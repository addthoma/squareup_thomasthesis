.class public Lcom/squareup/usb/UsbDiscoverer;
.super Ljava/lang/Object;
.source "UsbDiscoverer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;,
        Lcom/squareup/usb/UsbDiscoverer$DeviceListener;
    }
.end annotation


# static fields
.field private static final DELAYED_STOP_DELAY_TIME_MS:I = 0x1388

.field private static final PERMISSION_ACTION:Ljava/lang/String;


# instance fields
.field private final DELAYED_STOP_RUNNABLE:Ljava/lang/Runnable;

.field private final SEARCH_RUNNABLE:Ljava/lang/Runnable;

.field private final broadcastReceivers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Landroid/content/BroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private final deviceListeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/usb/UsbDiscoverer$DeviceListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final usbDevices:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final usbManager:Lcom/squareup/hardware/usb/UsbManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;

    .line 54
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".PERMISSION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/usb/UsbDiscoverer;->PERMISSION_ACTION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/executor/MainThread;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lcom/squareup/usb/-$$Lambda$pJMYjMYGH82a0-lqBoEIns7EXzQ;

    invoke-direct {v0, p0}, Lcom/squareup/usb/-$$Lambda$pJMYjMYGH82a0-lqBoEIns7EXzQ;-><init>(Lcom/squareup/usb/UsbDiscoverer;)V

    iput-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->DELAYED_STOP_RUNNABLE:Ljava/lang/Runnable;

    .line 65
    new-instance v0, Lcom/squareup/usb/-$$Lambda$UsbDiscoverer$_kTnGzxvkcmU6zT3FIxn-yrciv8;

    invoke-direct {v0, p0}, Lcom/squareup/usb/-$$Lambda$UsbDiscoverer$_kTnGzxvkcmU6zT3FIxn-yrciv8;-><init>(Lcom/squareup/usb/UsbDiscoverer;)V

    iput-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->SEARCH_RUNNABLE:Ljava/lang/Runnable;

    .line 74
    iput-object p1, p0, Lcom/squareup/usb/UsbDiscoverer;->context:Landroid/content/Context;

    .line 75
    iput-object p2, p0, Lcom/squareup/usb/UsbDiscoverer;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    .line 76
    iput-object p3, p0, Lcom/squareup/usb/UsbDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 78
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/usb/UsbDiscoverer;->deviceListeners:Ljava/util/Map;

    .line 79
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/usb/UsbDiscoverer;->broadcastReceivers:Ljava/util/Set;

    .line 80
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/usb/UsbDiscoverer;->usbDevices:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/usb/UsbDiscoverer;Landroid/content/Intent;)Landroid/hardware/usb/UsbDevice;
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/usb/UsbDiscoverer;->getDevice(Landroid/content/Intent;)Landroid/hardware/usb/UsbDevice;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/usb/UsbDiscoverer;I)Lcom/squareup/usb/UsbDiscoverer$DeviceListener;
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/usb/UsbDiscoverer;->getDeviceListener(I)Lcom/squareup/usb/UsbDiscoverer$DeviceListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/usb/UsbDiscoverer;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/usb/UsbDiscoverer;->usbDevices:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object p0
.end method

.method private attachBroadcastReceivers()V
    .locals 4

    .line 208
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->broadcastReceivers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 210
    :cond_0
    new-instance v0, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;-><init>(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/usb/UsbDiscoverer$1;)V

    .line 211
    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->context:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/squareup/usb/UsbDiscoverer;->PERMISSION_ACTION:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 213
    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->broadcastReceivers:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private findDevices(I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 183
    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    if-eqz v1, :cond_1

    .line 184
    invoke-interface {v1}, Lcom/squareup/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbDevice;

    .line 185
    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 186
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private getDevice(Landroid/content/Intent;)Landroid/hardware/usb/UsbDevice;
    .locals 1

    const-string v0, "device"

    .line 224
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/hardware/usb/UsbDevice;

    return-object p1
.end method

.method private getDeviceListener(I)Lcom/squareup/usb/UsbDiscoverer$DeviceListener;
    .locals 1

    .line 228
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->deviceListeners:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/usb/UsbDiscoverer$DeviceListener;

    return-object p1
.end method

.method public static synthetic lambda$_kTnGzxvkcmU6zT3FIxn-yrciv8(Lcom/squareup/usb/UsbDiscoverer;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/usb/UsbDiscoverer;->searchForUsbDevices()V

    return-void
.end method

.method private removeBroadcastReceivers()V
    .locals 3

    .line 217
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->broadcastReceivers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/BroadcastReceiver;

    .line 218
    iget-object v2, p0, Lcom/squareup/usb/UsbDiscoverer;->context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->broadcastReceivers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method private requestPermission(Landroid/hardware/usb/UsbDevice;)V
    .locals 3

    .line 201
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/squareup/usb/UsbDiscoverer;->PERMISSION_ACTION:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    invoke-interface {v1, p1, v0}, Lcom/squareup/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V

    return-void
.end method

.method private searchForUsbDevices()V
    .locals 5

    .line 144
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->broadcastReceivers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 149
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 150
    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->deviceListeners:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 151
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/squareup/usb/UsbDiscoverer;->findDevices(I)Ljava/util/List;

    move-result-object v2

    .line 152
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 155
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 156
    iget-object v2, p0, Lcom/squareup/usb/UsbDiscoverer;->usbDevices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/usb/UsbDevice;

    .line 157
    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 159
    invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v4

    .line 160
    invoke-direct {p0, v4}, Lcom/squareup/usb/UsbDiscoverer;->getDeviceListener(I)Lcom/squareup/usb/UsbDiscoverer$DeviceListener;

    move-result-object v4

    .line 161
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v4, :cond_2

    .line 163
    invoke-interface {v4, v3}, Lcom/squareup/usb/UsbDiscoverer$DeviceListener;->deviceUnavailable(Landroid/hardware/usb/UsbDevice;)V

    goto :goto_1

    .line 167
    :cond_3
    iget-object v2, p0, Lcom/squareup/usb/UsbDiscoverer;->usbDevices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 169
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    .line 170
    iget-object v2, p0, Lcom/squareup/usb/UsbDiscoverer;->usbDevices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 173
    invoke-direct {p0, v1}, Lcom/squareup/usb/UsbDiscoverer;->requestPermission(Landroid/hardware/usb/UsbDevice;)V

    goto :goto_2

    :cond_5
    return-void
.end method


# virtual methods
.method public removeDeviceListenerForVendorId(I)V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->deviceListeners:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public scheduleSearchForUsbDevices()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->SEARCH_RUNNABLE:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setDeviceListenerForVendorId(ILcom/squareup/usb/UsbDiscoverer$DeviceListener;)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->deviceListeners:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object p1, p0, Lcom/squareup/usb/UsbDiscoverer;->broadcastReceivers:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/squareup/usb/UsbDiscoverer;->scheduleSearchForUsbDevices()V

    :cond_0
    return-void
.end method

.method public start()V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->DELAYED_STOP_RUNNABLE:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 90
    invoke-direct {p0}, Lcom/squareup/usb/UsbDiscoverer;->attachBroadcastReceivers()V

    .line 91
    invoke-virtual {p0}, Lcom/squareup/usb/UsbDiscoverer;->scheduleSearchForUsbDevices()V

    return-void
.end method

.method public stop()V
    .locals 3

    .line 105
    invoke-direct {p0}, Lcom/squareup/usb/UsbDiscoverer;->removeBroadcastReceivers()V

    .line 108
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->usbDevices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    .line 109
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    .line 110
    invoke-direct {p0, v2}, Lcom/squareup/usb/UsbDiscoverer;->getDeviceListener(I)Lcom/squareup/usb/UsbDiscoverer$DeviceListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 111
    invoke-interface {v2, v1}, Lcom/squareup/usb/UsbDiscoverer$DeviceListener;->deviceUnavailable(Landroid/hardware/usb/UsbDevice;)V

    goto :goto_0

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->usbDevices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    return-void
.end method

.method public stopDelayed()V
    .locals 4

    .line 96
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->SEARCH_RUNNABLE:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer;->DELAYED_STOP_RUNNABLE:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
