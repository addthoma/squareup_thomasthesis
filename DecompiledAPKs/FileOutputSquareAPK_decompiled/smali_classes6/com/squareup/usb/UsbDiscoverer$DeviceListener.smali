.class public interface abstract Lcom/squareup/usb/UsbDiscoverer$DeviceListener;
.super Ljava/lang/Object;
.source "UsbDiscoverer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/usb/UsbDiscoverer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceListener"
.end annotation


# virtual methods
.method public abstract deviceAvailable(Landroid/hardware/usb/UsbDevice;)V
.end method

.method public abstract deviceUnavailable(Landroid/hardware/usb/UsbDevice;)V
.end method
