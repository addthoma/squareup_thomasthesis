.class public final Lcom/squareup/user/NotificationPresenter$showsNotification$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "NotificationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/user/NotificationPresenter;-><init>(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/BrowserLauncher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/server/account/protos/Notification;",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationPresenter.kt\ncom/squareup/user/NotificationPresenter$showsNotification$1\n*L\n1#1,156:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0003H\u0014\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/user/NotificationPresenter$showsNotification$1",
        "Lcom/squareup/mortar/PopupPresenter;",
        "Lcom/squareup/server/account/protos/Notification;",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        "onPopupResult",
        "",
        "result",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/user/NotificationPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/user/NotificationPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 38
    iput-object p1, p0, Lcom/squareup/user/NotificationPresenter$showsNotification$1;->this$0:Lcom/squareup/user/NotificationPresenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Lcom/squareup/server/account/protos/Notification$Button;)V
    .locals 1

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    sget-object v0, Lcom/squareup/user/NotificationPopup;->CANCELED:Lcom/squareup/server/account/protos/Notification$Button;

    if-ne p1, v0, :cond_1

    .line 41
    iget-object p1, p0, Lcom/squareup/user/NotificationPresenter$showsNotification$1;->this$0:Lcom/squareup/user/NotificationPresenter;

    invoke-virtual {p1}, Lcom/squareup/user/NotificationPresenter;->getCurrent()Lcom/squareup/server/account/protos/Notification;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter$showsNotification$1;->this$0:Lcom/squareup/user/NotificationPresenter;

    invoke-static {v0, p1}, Lcom/squareup/user/NotificationPresenter;->access$doNotShowAgain(Lcom/squareup/user/NotificationPresenter;Lcom/squareup/server/account/protos/Notification;)V

    .line 44
    iget-object p1, p0, Lcom/squareup/user/NotificationPresenter$showsNotification$1;->this$0:Lcom/squareup/user/NotificationPresenter;

    const/4 v0, 0x0

    check-cast v0, Lcom/squareup/server/account/protos/Notification;

    invoke-static {p1, v0}, Lcom/squareup/user/NotificationPresenter;->access$setCurrent$p(Lcom/squareup/user/NotificationPresenter;Lcom/squareup/server/account/protos/Notification;)V

    goto :goto_0

    .line 48
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The current non-lockout notification is null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter$showsNotification$1;->this$0:Lcom/squareup/user/NotificationPresenter;

    invoke-static {v0}, Lcom/squareup/user/NotificationPresenter;->access$getBrowserLauncher$p(Lcom/squareup/user/NotificationPresenter;)Lcom/squareup/util/BrowserLauncher;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/user/NotificationPresenter$showsNotification$1;->this$0:Lcom/squareup/user/NotificationPresenter;

    invoke-virtual {p1}, Lcom/squareup/user/NotificationPresenter;->check()V

    return-void
.end method

.method public bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/server/account/protos/Notification$Button;

    invoke-virtual {p0, p1}, Lcom/squareup/user/NotificationPresenter$showsNotification$1;->onPopupResult(Lcom/squareup/server/account/protos/Notification$Button;)V

    return-void
.end method
