.class public Lcom/squareup/util/ProtoTimes;
.super Ljava/lang/Object;
.source "ProtoTimes.java"


# static fields
.field private static final UTC:Ljava/util/TimeZone;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "UTC"

    .line 18
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/ProtoTimes;->UTC:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asCalendar(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Calendar;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 35
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 37
    sget-object v2, Lcom/squareup/util/ProtoTimes;->UTC:Ljava/util/TimeZone;

    invoke-static {v2, p1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p1

    .line 40
    iget-object v2, p0, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    const/16 v2, 0xc

    .line 41
    iget-object p0, p0, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {p1, v2, p0}, Ljava/util/Calendar;->add(II)V

    .line 43
    :cond_0
    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    return-object p1
.end method

.method public static asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;
    .locals 0

    .line 21
    invoke-static {p0, p1}, Lcom/squareup/util/ProtoTimes;->asCalendar(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method

.method public static asDateTime(J)Lcom/squareup/protos/common/time/DateTime;
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p0

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    return-object p0
.end method

.method public static asDateTime(Ljava/util/Date;)Lcom/squareup/protos/common/time/DateTime;
    .locals 2

    .line 25
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/util/ProtoTimes;->asDateTime(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    return-object p0
.end method
