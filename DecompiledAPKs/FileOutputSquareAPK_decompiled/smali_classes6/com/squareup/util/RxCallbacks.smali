.class public Lcom/squareup/util/RxCallbacks;
.super Ljava/lang/Object;
.source "RxCallbacks.java"


# static fields
.field private static final DO_NOTHING:Ljava/lang/Runnable;


# instance fields
.field private final computationScheduler:Lrx/Scheduler;

.field private final tasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/util/-$$Lambda$RxCallbacks$C6zECJxAAF7ue9aK6-3KIhWuEhg;->INSTANCE:Lcom/squareup/util/-$$Lambda$RxCallbacks$C6zECJxAAF7ue9aK6-3KIhWuEhg;

    sput-object v0, Lcom/squareup/util/RxCallbacks;->DO_NOTHING:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Lrx/Scheduler;)V
    .locals 1
    .param p1    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/util/RxCallbacks;->tasks:Ljava/util/List;

    .line 22
    iput-object p1, p0, Lcom/squareup/util/RxCallbacks;->computationScheduler:Lrx/Scheduler;

    return-void
.end method

.method static synthetic lambda$static$0()V
    .locals 0

    return-void
.end method


# virtual methods
.method public completion()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/util/RxCallbacks;->tasks:Ljava/util/List;

    invoke-static {v0}, Lrx/Observable;->merge(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->ignoreElements()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public createCallback()Ljava/lang/Runnable;
    .locals 3

    .line 31
    new-instance v0, Ljava/util/concurrent/FutureTask;

    sget-object v1, Lcom/squareup/util/RxCallbacks;->DO_NOTHING:Ljava/lang/Runnable;

    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 32
    invoke-static {v0}, Lrx/Observable;->from(Ljava/util/concurrent/Future;)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/util/RxCallbacks;->computationScheduler:Lrx/Scheduler;

    .line 37
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v1

    .line 39
    iget-object v2, p0, Lcom/squareup/util/RxCallbacks;->tasks:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
