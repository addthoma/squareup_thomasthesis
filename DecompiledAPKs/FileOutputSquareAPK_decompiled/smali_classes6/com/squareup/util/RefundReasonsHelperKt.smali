.class public final Lcom/squareup/util/RefundReasonsHelperKt;
.super Ljava/lang/Object;
.source "RefundReasonsHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\"\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "getReasonName",
        "",
        "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
        "reason",
        "res",
        "Lcom/squareup/util/Res;",
        "forPrintedReceipt",
        "",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getReasonName(Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/util/Res;Z)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getReasonName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    sget-object v0, Lcom/squareup/util/RefundReasonsHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 18
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    if-eqz p3, :cond_0

    .line 13
    sget p0, Lcom/squareup/proto_utilities/R$string;->refund_string:I

    invoke-interface {p2, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 15
    :cond_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->refund_reason_fraudulent_charge:I

    invoke-interface {p2, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 10
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->refund_reason_canceled_order:I

    invoke-interface {p2, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 9
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->refund_reason_accidental_charge:I

    invoke-interface {p2, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 8
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->refund_reason_returned_goods:I

    invoke-interface {p2, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    :pswitch_4
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
