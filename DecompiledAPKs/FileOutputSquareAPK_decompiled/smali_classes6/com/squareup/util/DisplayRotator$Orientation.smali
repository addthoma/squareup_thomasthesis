.class public final enum Lcom/squareup/util/DisplayRotator$Orientation;
.super Ljava/lang/Enum;
.source "DisplayRotator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/DisplayRotator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Orientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/util/DisplayRotator$Orientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/util/DisplayRotator$Orientation;

.field public static final enum LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

.field public static final enum PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

.field public static final enum REVERSE_LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

.field public static final enum REVERSE_PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;


# instance fields
.field private final landscapeDeviceIndex:I

.field private final portraitDeviceIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 94
    new-instance v0, Lcom/squareup/util/DisplayRotator$Orientation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "LANDSCAPE"

    invoke-direct {v0, v3, v2, v2, v1}, Lcom/squareup/util/DisplayRotator$Orientation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    .line 99
    new-instance v0, Lcom/squareup/util/DisplayRotator$Orientation;

    const-string v3, "PORTRAIT"

    invoke-direct {v0, v3, v1, v1, v2}, Lcom/squareup/util/DisplayRotator$Orientation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    .line 105
    new-instance v0, Lcom/squareup/util/DisplayRotator$Orientation;

    const/4 v3, 0x3

    const/4 v4, 0x2

    const-string v5, "REVERSE_LANDSCAPE"

    invoke-direct {v0, v5, v4, v4, v3}, Lcom/squareup/util/DisplayRotator$Orientation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    .line 111
    new-instance v0, Lcom/squareup/util/DisplayRotator$Orientation;

    const-string v5, "REVERSE_PORTRAIT"

    invoke-direct {v0, v5, v3, v3, v4}, Lcom/squareup/util/DisplayRotator$Orientation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/util/DisplayRotator$Orientation;

    .line 89
    sget-object v5, Lcom/squareup/util/DisplayRotator$Orientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    aput-object v5, v0, v2

    sget-object v2, Lcom/squareup/util/DisplayRotator$Orientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->$VALUES:[Lcom/squareup/util/DisplayRotator$Orientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    iput p3, p0, Lcom/squareup/util/DisplayRotator$Orientation;->landscapeDeviceIndex:I

    .line 127
    iput p4, p0, Lcom/squareup/util/DisplayRotator$Orientation;->portraitDeviceIndex:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/util/DisplayRotator$Orientation;)I
    .locals 0

    .line 89
    iget p0, p0, Lcom/squareup/util/DisplayRotator$Orientation;->landscapeDeviceIndex:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/util/DisplayRotator$Orientation;)I
    .locals 0

    .line 89
    iget p0, p0, Lcom/squareup/util/DisplayRotator$Orientation;->portraitDeviceIndex:I

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/util/DisplayRotator$Orientation;
    .locals 1

    .line 89
    const-class v0, Lcom/squareup/util/DisplayRotator$Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/util/DisplayRotator$Orientation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/util/DisplayRotator$Orientation;
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->$VALUES:[Lcom/squareup/util/DisplayRotator$Orientation;

    invoke-virtual {v0}, [Lcom/squareup/util/DisplayRotator$Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/util/DisplayRotator$Orientation;

    return-object v0
.end method
