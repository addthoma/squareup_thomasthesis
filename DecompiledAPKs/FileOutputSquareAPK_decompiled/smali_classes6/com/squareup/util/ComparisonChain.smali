.class public Lcom/squareup/util/ComparisonChain;
.super Ljava/lang/Object;
.source "ComparisonChain.java"


# static fields
.field private static EQUAL:Lcom/squareup/util/ComparisonChain;

.field private static GREATER_THAN:Lcom/squareup/util/ComparisonChain;

.field private static LESS_THAN:Lcom/squareup/util/ComparisonChain;


# instance fields
.field private final result:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 6
    new-instance v0, Lcom/squareup/util/ComparisonChain;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/util/ComparisonChain;-><init>(I)V

    sput-object v0, Lcom/squareup/util/ComparisonChain;->EQUAL:Lcom/squareup/util/ComparisonChain;

    .line 7
    new-instance v0, Lcom/squareup/util/ComparisonChain;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/squareup/util/ComparisonChain;-><init>(I)V

    sput-object v0, Lcom/squareup/util/ComparisonChain;->LESS_THAN:Lcom/squareup/util/ComparisonChain;

    .line 8
    new-instance v0, Lcom/squareup/util/ComparisonChain;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/util/ComparisonChain;-><init>(I)V

    sput-object v0, Lcom/squareup/util/ComparisonChain;->GREATER_THAN:Lcom/squareup/util/ComparisonChain;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/squareup/util/ComparisonChain;->result:I

    return-void
.end method

.method public static start()Lcom/squareup/util/ComparisonChain;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/util/ComparisonChain;->EQUAL:Lcom/squareup/util/ComparisonChain;

    return-object v0
.end method


# virtual methods
.method public compare(II)Lcom/squareup/util/ComparisonChain;
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/util/ComparisonChain;->result:I

    if-nez v0, :cond_1

    if-ge p1, p2, :cond_0

    .line 23
    sget-object p1, Lcom/squareup/util/ComparisonChain;->LESS_THAN:Lcom/squareup/util/ComparisonChain;

    return-object p1

    :cond_0
    if-le p1, p2, :cond_1

    .line 25
    sget-object p1, Lcom/squareup/util/ComparisonChain;->GREATER_THAN:Lcom/squareup/util/ComparisonChain;

    return-object p1

    :cond_1
    return-object p0
.end method

.method public compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable<",
            "TT;>;>(TT;TT;)",
            "Lcom/squareup/util/ComparisonChain;"
        }
    .end annotation

    .line 32
    iget v0, p0, Lcom/squareup/util/ComparisonChain;->result:I

    if-nez v0, :cond_1

    .line 33
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result p1

    if-gez p1, :cond_0

    .line 35
    sget-object p1, Lcom/squareup/util/ComparisonChain;->LESS_THAN:Lcom/squareup/util/ComparisonChain;

    return-object p1

    :cond_0
    if-lez p1, :cond_1

    .line 37
    sget-object p1, Lcom/squareup/util/ComparisonChain;->GREATER_THAN:Lcom/squareup/util/ComparisonChain;

    return-object p1

    :cond_1
    return-object p0
.end method

.method public compareWithNull(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable<",
            "TT;>;>(TT;TT;)",
            "Lcom/squareup/util/ComparisonChain;"
        }
    .end annotation

    .line 45
    iget v0, p0, Lcom/squareup/util/ComparisonChain;->result:I

    if-nez v0, :cond_1

    .line 46
    invoke-static {p1, p2}, Lcom/squareup/util/Objects;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    if-gez p1, :cond_0

    .line 48
    sget-object p1, Lcom/squareup/util/ComparisonChain;->LESS_THAN:Lcom/squareup/util/ComparisonChain;

    return-object p1

    :cond_0
    if-lez p1, :cond_1

    .line 50
    sget-object p1, Lcom/squareup/util/ComparisonChain;->GREATER_THAN:Lcom/squareup/util/ComparisonChain;

    return-object p1

    :cond_1
    return-object p0
.end method

.method public result()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/squareup/util/ComparisonChain;->result:I

    return v0
.end method
