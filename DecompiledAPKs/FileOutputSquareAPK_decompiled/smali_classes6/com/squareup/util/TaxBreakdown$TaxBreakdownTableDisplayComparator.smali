.class Lcom/squareup/util/TaxBreakdown$TaxBreakdownTableDisplayComparator;
.super Ljava/lang/Object;
.source "TaxBreakdown.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/TaxBreakdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TaxBreakdownTableDisplayComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final availableTaxInformations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$TaxInformation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$TaxInformation;",
            ">;)V"
        }
    .end annotation

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownTableDisplayComparator;->availableTaxInformations:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 126
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/util/TaxBreakdown$TaxBreakdownTableDisplayComparator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownTableDisplayComparator;->availableTaxInformations:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/TaxBreakdown$TaxInformation;

    .line 135
    iget-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownTableDisplayComparator;->availableTaxInformations:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/util/TaxBreakdown$TaxInformation;

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 141
    iget-object v0, p1, Lcom/squareup/util/TaxBreakdown$TaxInformation;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object v1, p2, Lcom/squareup/util/TaxBreakdown$TaxInformation;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Fee$InclusionType;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_1

    .line 142
    iget-object v0, p1, Lcom/squareup/util/TaxBreakdown$TaxInformation;->percentage:Lcom/squareup/util/Percentage;

    iget-object v1, p2, Lcom/squareup/util/TaxBreakdown$TaxInformation;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result v0

    if-nez v0, :cond_0

    .line 143
    iget-object p1, p1, Lcom/squareup/util/TaxBreakdown$TaxInformation;->name:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/util/TaxBreakdown$TaxInformation;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 145
    :cond_0
    iget-object p2, p2, Lcom/squareup/util/TaxBreakdown$TaxInformation;->percentage:Lcom/squareup/util/Percentage;

    iget-object p1, p1, Lcom/squareup/util/TaxBreakdown$TaxInformation;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {p2, p1}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result p1

    return p1

    .line 148
    :cond_1
    iget-object p1, p1, Lcom/squareup/util/TaxBreakdown$TaxInformation;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object p2, p2, Lcom/squareup/util/TaxBreakdown$TaxInformation;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/Fee$InclusionType;->compareTo(Ljava/lang/Enum;)I

    move-result p1

    return p1

    .line 138
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Specified tax not available in available taxes."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
