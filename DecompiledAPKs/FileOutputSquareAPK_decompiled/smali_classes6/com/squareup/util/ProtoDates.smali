.class public Lcom/squareup/util/ProtoDates;
.super Ljava/lang/Object;
.source "ProtoDates.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addDays(Lcom/squareup/protos/common/time/YearMonthDay;I)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 123
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p0

    const/4 v0, 0x5

    .line 124
    invoke-virtual {p0, v0, p1}, Ljava/util/Calendar;->add(II)V

    .line 125
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static addDays(Ljava/util/Date;I)Ljava/util/Date;
    .locals 1

    .line 100
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 101
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 p0, 0x5

    .line 102
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->add(II)V

    .line 103
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method

.method public static calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;
    .locals 3

    .line 67
    new-instance v0, Ljava/util/GregorianCalendar;

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object p0, p0, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    .line 68
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-direct {v0, v1, v2, p0}, Ljava/util/GregorianCalendar;-><init>(III)V

    return-object v0
.end method

.method public static calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/TimeZone;)Ljava/util/Calendar;
    .locals 2

    .line 32
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 33
    iget-object p1, p0, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object p0, p0, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    .line 34
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 33
    invoke-virtual {v0, p1, v1, p0}, Ljava/util/GregorianCalendar;->set(III)V

    .line 35
    invoke-static {v0}, Lcom/squareup/util/Times;->stripTime(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p0

    return-object p0
.end method

.method public static calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 3

    .line 57
    new-instance v0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object v0

    const/4 v2, 0x2

    .line 59
    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->month_of_year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object v0

    const/4 v1, 0x5

    .line 60
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->day_of_month(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object p0

    .line 61
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->build()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static compareYmd(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I
    .locals 1

    .line 81
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/time/YearMonthDay;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 84
    :cond_0
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p0

    .line 85
    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p1

    .line 87
    invoke-virtual {p0, p1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result p0

    return p0
.end method

.method public static countDaysBetweenAsUTC(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J
    .locals 1

    const-string v0, "UTC"

    .line 26
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 28
    invoke-static {p0, v0}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object p0

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object p1

    .line 27
    invoke-static {p0, p1}, Lcom/squareup/util/Times;->countDaysBetween(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide p0

    return-wide p0
.end method

.method public static dateIsAfterToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 92
    :cond_0
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getGregorianCalenderInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object p0, p0, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {v0, v1, v2, p0}, Ljava/util/Calendar;->set(III)V

    .line 96
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getGregorianCalenderInstance()Ljava/util/Calendar;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static dateToYmd(Ljava/util/Date;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 51
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 52
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 53
    invoke-static {v0}, Lcom/squareup/util/ProtoDates;->calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;
    .locals 0

    .line 18
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method

.method public static getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/TimeZone;)Ljava/util/Date;
    .locals 0

    .line 39
    invoke-static {p0, p1}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method

.method public static getLocalDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Lorg/threeten/bp/LocalDate;
    .locals 2

    .line 14
    iget-object v0, p0, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p0, p0, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-static {v0, v1, p0}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    return-object p0
.end method

.method public static getRelativeDate(Lcom/squareup/protos/common/time/YearMonthDay;I)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 72
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p0

    const/4 v0, 0x6

    .line 73
    invoke-virtual {p0, v0, p1}, Ljava/util/Calendar;->add(II)V

    .line 74
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static localDateToYmd(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    .line 43
    new-instance v0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;-><init>()V

    .line 44
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->month_of_year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object v0

    .line 46
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->day_of_month(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object p0

    .line 47
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->build()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static tryParseIso8601Date(Lcom/squareup/protos/client/ISO8601Date;)Ljava/util/Date;
    .locals 0

    if-eqz p0, :cond_0

    .line 108
    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method
