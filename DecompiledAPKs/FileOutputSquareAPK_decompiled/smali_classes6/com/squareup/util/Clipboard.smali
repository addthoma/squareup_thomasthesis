.class public interface abstract Lcom/squareup/util/Clipboard;
.super Ljava/lang/Object;
.source "Clipboard.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/Clipboard$DefaultImpls;,
        Lcom/squareup/util/Clipboard$RealClipboard;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\u0008f\u0018\u0000 \t2\u00020\u0001:\u0001\tJ$\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/util/Clipboard;",
        "",
        "copyPlainText",
        "",
        "context",
        "Landroid/content/Context;",
        "label",
        "",
        "text",
        "RealClipboard",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final RealClipboard:Lcom/squareup/util/Clipboard$RealClipboard;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/util/Clipboard$RealClipboard;->$$INSTANCE:Lcom/squareup/util/Clipboard$RealClipboard;

    sput-object v0, Lcom/squareup/util/Clipboard;->RealClipboard:Lcom/squareup/util/Clipboard$RealClipboard;

    return-void
.end method


# virtual methods
.method public abstract copyPlainText(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end method
