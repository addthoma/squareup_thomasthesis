.class Lcom/squareup/util/DisplayRotator$DefaultDisplay;
.super Ljava/lang/Object;
.source "DisplayRotator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/DisplayRotator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DefaultDisplay"
.end annotation


# instance fields
.field private final systemSettings:Lcom/squareup/util/SystemSettings;

.field private final windowManager:Landroid/view/WindowManager;


# direct methods
.method constructor <init>(Landroid/view/WindowManager;Lcom/squareup/util/SystemSettings;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->windowManager:Landroid/view/WindowManager;

    .line 41
    iput-object p2, p0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->systemSettings:Lcom/squareup/util/SystemSettings;

    return-void
.end method


# virtual methods
.method public getAccelerometerRotationEnabled()Z
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->systemSettings:Lcom/squareup/util/SystemSettings;

    invoke-virtual {v0}, Lcom/squareup/util/SystemSettings;->isAccelerometerRotationEnabled()Z

    move-result v0

    return v0
.end method

.method public getHeight()I
    .locals 2

    .line 59
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 61
    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method

.method public getRotation()I
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 2

    .line 53
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 54
    iget-object v1, p0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 55
    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method

.method public setAccelerometerRotationEnabled(Z)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->systemSettings:Lcom/squareup/util/SystemSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/util/SystemSettings;->setAccelerometerRotationEnabled(Z)V

    return-void
.end method

.method public setUserRotation(I)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->systemSettings:Lcom/squareup/util/SystemSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/util/SystemSettings;->setUserRotation(I)V

    return-void
.end method
