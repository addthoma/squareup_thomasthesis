.class public final Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;
.super Ljava/lang/Object;
.source "AndroidModule_ProvideStorageManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/os/storage/StorageManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;->contextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideStorageManager(Landroid/app/Application;)Landroid/os/storage/StorageManager;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/util/AndroidModule;->provideStorageManager(Landroid/app/Application;)Landroid/os/storage/StorageManager;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/storage/StorageManager;

    return-object p0
.end method


# virtual methods
.method public get()Landroid/os/storage/StorageManager;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;->provideStorageManager(Landroid/app/Application;)Landroid/os/storage/StorageManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/util/AndroidModule_ProvideStorageManagerFactory;->get()Landroid/os/storage/StorageManager;

    move-result-object v0

    return-object v0
.end method
