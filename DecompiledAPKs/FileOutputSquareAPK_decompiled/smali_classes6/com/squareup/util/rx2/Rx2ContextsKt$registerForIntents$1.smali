.class final Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1;
.super Ljava/lang/Object;
.source "Rx2Contexts.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2ContextsKt;->registerForIntents(Landroid/content/Context;[Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRx2Contexts.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Rx2Contexts.kt\ncom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,37:1\n11416#2,2:38\n*E\n*S KotlinDebug\n*F\n+ 1 Rx2Contexts.kt\ncom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1\n*L\n31#1,2:38\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "Landroid/content/Intent;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actions:[Ljava/lang/String;

.field final synthetic $this_registerForIntents:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1;->$this_registerForIntents:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1;->$actions:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1$receiver$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1$receiver$1;-><init>(Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1;Lio/reactivex/ObservableEmitter;)V

    .line 29
    new-instance v1, Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1$1;-><init>(Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1;Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1$receiver$1;)V

    check-cast v1, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 31
    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    .line 32
    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1;->$actions:[Ljava/lang/String;

    .line 38
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    .line 32
    invoke-virtual {p1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 34
    :cond_0
    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2ContextsKt$registerForIntents$1;->$this_registerForIntents:Landroid/content/Context;

    check-cast v0, Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v0, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
