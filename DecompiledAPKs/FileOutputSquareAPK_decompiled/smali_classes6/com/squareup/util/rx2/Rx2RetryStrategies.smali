.class public final Lcom/squareup/util/rx2/Rx2RetryStrategies;
.super Ljava/lang/Object;
.source "Rx2RetryStrategies.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002Jd\u0010\u0003\u001a\u001c\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00050\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000f2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00120\u000fH\u0002Jd\u0010\u0013\u001a\u001c\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00140\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000f2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00120\u000fH\u0002JX\u0010\u0015\u001a\u001c\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00050\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u00122\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000fH\u0007JX\u0010\u0017\u001a\u001c\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00140\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u00122\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000fH\u0007JV\u0010\u0018\u001a\u001c\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00050\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u00122\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000fJV\u0010\u0019\u001a\u001c\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00140\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u00122\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000fJ@\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005\"\u0008\u0008\u0000\u0010\u001b*\u00020\u0006*\n\u0012\u0006\u0008\u0001\u0012\u0002H\u001b0\u00052\u0006\u0010\u0008\u001a\u00020\t2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000fJ@\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0014\"\u0008\u0008\u0000\u0010\u001b*\u00020\u0006*\n\u0012\u0006\u0008\u0001\u0012\u0002H\u001b0\u00142\u0006\u0010\u0008\u001a\u00020\t2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000f\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/util/rx2/Rx2RetryStrategies;",
        "",
        "()V",
        "customBackoffThenError",
        "Lio/reactivex/functions/Function;",
        "Lio/reactivex/Observable;",
        "",
        "",
        "numberOfRetries",
        "",
        "unit",
        "Ljava/util/concurrent/TimeUnit;",
        "scheduler",
        "Lio/reactivex/Scheduler;",
        "isRetryableError",
        "Lkotlin/Function1;",
        "",
        "delayFunction",
        "",
        "customBackoffThenErrorFlowable",
        "Lio/reactivex/Flowable;",
        "exponentialBackoffThenError",
        "delay",
        "exponentialBackoffThenErrorFlowable",
        "noBackoffThenError",
        "noBackoffThenErrorFlowable",
        "retryCountThenError",
        "T",
        "retryCountThenErrorFlowable",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/util/rx2/Rx2RetryStrategies;

    invoke-direct {v0}, Lcom/squareup/util/rx2/Rx2RetryStrategies;-><init>()V

    sput-object v0, Lcom/squareup/util/rx2/Rx2RetryStrategies;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final customBackoffThenError(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;)",
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 71
    new-instance v6, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenError$2;

    move-object v0, v6

    move v1, p1

    move-object v2, p4

    move-object v3, p5

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenError$2;-><init>(ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V

    check-cast v6, Lio/reactivex/functions/Function;

    return-object v6
.end method

.method static synthetic customBackoffThenError$default(Lcom/squareup/util/rx2/Rx2RetryStrategies;ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;
    .locals 6

    and-int/lit8 p6, p6, 0x8

    if-eqz p6, :cond_0

    .line 68
    sget-object p4, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenError$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenError$1;

    check-cast p4, Lkotlin/jvm/functions/Function1;

    :cond_0
    move-object v4, p4

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->customBackoffThenError(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method private final customBackoffThenErrorFlowable(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;)",
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Flowable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 157
    new-instance v6, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;

    move-object v0, v6

    move v1, p1

    move-object v2, p4

    move-object v3, p5

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;-><init>(ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V

    check-cast v6, Lio/reactivex/functions/Function;

    return-object v6
.end method

.method static synthetic customBackoffThenErrorFlowable$default(Lcom/squareup/util/rx2/Rx2RetryStrategies;ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;
    .locals 6

    and-int/lit8 p6, p6, 0x8

    if-eqz p6, :cond_0

    .line 154
    sget-object p4, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$1;

    check-cast p4, Lkotlin/jvm/functions/Function1;

    :cond_0
    move-object v4, p4

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->customBackoffThenErrorFlowable(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static final exponentialBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/functions/Function;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v7}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenError$default(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static final exponentialBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isRetryableError"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget-object v1, Lcom/squareup/util/rx2/Rx2RetryStrategies;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies;

    new-instance v0, Lcom/squareup/util/rx2/Rx2RetryStrategies$exponentialBackoffThenError$2;

    invoke-direct {v0, p1, p2}, Lcom/squareup/util/rx2/Rx2RetryStrategies$exponentialBackoffThenError$2;-><init>(J)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    move v2, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->customBackoffThenError(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic exponentialBackoffThenError$default(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    .line 33
    sget-object p5, Lcom/squareup/util/rx2/Rx2RetryStrategies$exponentialBackoffThenError$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$exponentialBackoffThenError$1;

    check-cast p5, Lkotlin/jvm/functions/Function1;

    :cond_0
    move-object v5, p5

    move v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static final exponentialBackoffThenErrorFlowable(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/functions/Function;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Flowable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v7}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenErrorFlowable$default(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static final exponentialBackoffThenErrorFlowable(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Flowable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isRetryableError"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object v1, Lcom/squareup/util/rx2/Rx2RetryStrategies;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies;

    .line 121
    new-instance v0, Lcom/squareup/util/rx2/Rx2RetryStrategies$exponentialBackoffThenErrorFlowable$2;

    invoke-direct {v0, p1, p2}, Lcom/squareup/util/rx2/Rx2RetryStrategies$exponentialBackoffThenErrorFlowable$2;-><init>(J)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    move v2, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 119
    invoke-direct/range {v1 .. v6}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->customBackoffThenErrorFlowable(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic exponentialBackoffThenErrorFlowable$default(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    .line 117
    sget-object p5, Lcom/squareup/util/rx2/Rx2RetryStrategies$exponentialBackoffThenErrorFlowable$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$exponentialBackoffThenErrorFlowable$1;

    check-cast p5, Lkotlin/jvm/functions/Function1;

    :cond_0
    move-object v5, p5

    move v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenErrorFlowable(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic noBackoffThenError$default(Lcom/squareup/util/rx2/Rx2RetryStrategies;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;
    .locals 7

    and-int/lit8 p7, p7, 0x10

    if-eqz p7, :cond_0

    .line 59
    sget-object p6, Lcom/squareup/util/rx2/Rx2RetryStrategies$noBackoffThenError$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$noBackoffThenError$1;

    check-cast p6, Lkotlin/jvm/functions/Function1;

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->noBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic noBackoffThenErrorFlowable$default(Lcom/squareup/util/rx2/Rx2RetryStrategies;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;
    .locals 7

    and-int/lit8 p7, p7, 0x10

    if-eqz p7, :cond_0

    .line 145
    sget-object p6, Lcom/squareup/util/rx2/Rx2RetryStrategies$noBackoffThenErrorFlowable$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$noBackoffThenErrorFlowable$1;

    check-cast p6, Lkotlin/jvm/functions/Function1;

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->noBackoffThenErrorFlowable(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic retryCountThenError$default(Lcom/squareup/util/rx2/Rx2RetryStrategies;Lio/reactivex/Observable;ILkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/Observable;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    .line 84
    sget-object p3, Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenError$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenError$1;

    check-cast p3, Lkotlin/jvm/functions/Function1;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->retryCountThenError(Lio/reactivex/Observable;ILkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic retryCountThenErrorFlowable$default(Lcom/squareup/util/rx2/Rx2RetryStrategies;Lio/reactivex/Flowable;ILkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/Flowable;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    .line 170
    sget-object p3, Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenErrorFlowable$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenErrorFlowable$1;

    check-cast p3, Lkotlin/jvm/functions/Function1;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->retryCountThenErrorFlowable(Lio/reactivex/Flowable;ILkotlin/jvm/functions/Function1;)Lio/reactivex/Flowable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final noBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "unit"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isRetryableError"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/squareup/util/rx2/Rx2RetryStrategies$noBackoffThenError$2;

    invoke-direct {v0, p2, p3}, Lcom/squareup/util/rx2/Rx2RetryStrategies$noBackoffThenError$2;-><init>(J)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    move-object v1, p0

    move v2, p1

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v1 .. v6}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->customBackoffThenError(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p1

    return-object p1
.end method

.method public final noBackoffThenErrorFlowable(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Flowable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "unit"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isRetryableError"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    new-instance v0, Lcom/squareup/util/rx2/Rx2RetryStrategies$noBackoffThenErrorFlowable$2;

    invoke-direct {v0, p2, p3}, Lcom/squareup/util/rx2/Rx2RetryStrategies$noBackoffThenErrorFlowable$2;-><init>(J)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    move-object v1, p0

    move v2, p1

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v1 .. v6}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->customBackoffThenErrorFlowable(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p1

    return-object p1
.end method

.method public final retryCountThenError(Lio/reactivex/Observable;ILkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Throwable;",
            ">(",
            "Lio/reactivex/Observable<",
            "+TT;>;I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$retryCountThenError"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isRetryableError"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    add-int/lit8 v0, p2, 0x1

    const/4 v1, 0x1

    .line 87
    invoke-static {v1, v0}, Lio/reactivex/Observable;->range(II)Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 88
    new-instance v1, Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenError$2;

    invoke-direct {v1, p2, p3}, Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenError$2;-><init>(ILkotlin/jvm/functions/Function1;)V

    check-cast v1, Lio/reactivex/functions/BiFunction;

    .line 86
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Observable;->zipWith(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    .line 94
    sget-object p2, Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenError$3;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenError$3;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "zipWith(\n        Observa\u2026etryCount -> retryCount }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final retryCountThenErrorFlowable(Lio/reactivex/Flowable;ILkotlin/jvm/functions/Function1;)Lio/reactivex/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Throwable;",
            ">(",
            "Lio/reactivex/Flowable<",
            "+TT;>;I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/Flowable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$retryCountThenErrorFlowable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isRetryableError"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    add-int/lit8 v0, p2, 0x1

    const/4 v1, 0x1

    .line 173
    invoke-static {v1, v0}, Lio/reactivex/Flowable;->range(II)Lio/reactivex/Flowable;

    move-result-object v0

    check-cast v0, Lorg/reactivestreams/Publisher;

    .line 174
    new-instance v1, Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenErrorFlowable$2;

    invoke-direct {v1, p2, p3}, Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenErrorFlowable$2;-><init>(ILkotlin/jvm/functions/Function1;)V

    check-cast v1, Lio/reactivex/functions/BiFunction;

    .line 172
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Flowable;->zipWith(Lorg/reactivestreams/Publisher;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 180
    sget-object p2, Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenErrorFlowable$3;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$retryCountThenErrorFlowable$3;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Flowable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "zipWith(\n        Flowabl\u2026etryCount -> retryCount }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
