.class public abstract Lcom/squareup/util/AbstractReadOnlyCursorList;
.super Landroid/database/CursorWrapper;
.source "AbstractReadOnlyCursorList.java"

# interfaces
.implements Lcom/squareup/util/ReadOnlyCursorList;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/database/CursorWrapper;",
        "Lcom/squareup/util/ReadOnlyCursorList<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 22
    invoke-virtual {p0, p1}, Lcom/squareup/util/AbstractReadOnlyCursorList;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/util/AbstractReadOnlyCursorList;->getCurrent()Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 23
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Index out of bounds: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract getCurrent()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public isEmpty()Z
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/util/AbstractReadOnlyCursorList;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;-><init>(Lcom/squareup/util/AbstractReadOnlyCursorList;Lcom/squareup/util/AbstractReadOnlyCursorList$1;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/util/AbstractReadOnlyCursorList;->getCount()I

    move-result v0

    return v0
.end method
