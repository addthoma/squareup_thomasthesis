.class public final synthetic Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func1;


# instance fields
.field private final synthetic f$0:Lrx/Observable;

.field private final synthetic f$1:J

.field private final synthetic f$2:Ljava/util/concurrent/TimeUnit;

.field private final synthetic f$3:Lrx/Scheduler;


# direct methods
.method public synthetic constructor <init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;->f$0:Lrx/Observable;

    iput-wide p2, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;->f$1:J

    iput-object p4, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;->f$2:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;->f$3:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;->f$0:Lrx/Observable;

    iget-wide v1, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;->f$1:J

    iget-object v3, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;->f$2:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;->f$3:Lrx/Scheduler;

    move-object v5, p1

    check-cast v5, Lkotlin/Unit;

    invoke-static/range {v0 .. v5}, Lcom/squareup/util/rx/RxTransformers;->lambda$null$1(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lkotlin/Unit;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
