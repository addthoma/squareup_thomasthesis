.class public final synthetic Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/Observable$Transformer;


# instance fields
.field private final synthetic f$0:Lrx/Observable;

.field private final synthetic f$1:J

.field private final synthetic f$2:Ljava/util/concurrent/TimeUnit;

.field private final synthetic f$3:Lrx/Scheduler;


# direct methods
.method public synthetic constructor <init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;->f$0:Lrx/Observable;

    iput-wide p2, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;->f$1:J

    iput-object p4, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;->f$2:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;->f$3:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;->f$0:Lrx/Observable;

    iget-wide v1, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;->f$1:J

    iget-object v3, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;->f$2:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;->f$3:Lrx/Scheduler;

    move-object v5, p1

    check-cast v5, Lrx/Observable;

    invoke-static/range {v0 .. v5}, Lcom/squareup/util/rx/RxTransformers;->lambda$refreshWhen$8(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
