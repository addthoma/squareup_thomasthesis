.class public abstract Lcom/squareup/util/prefsbackedcontentprovider/BoolContractItem;
.super Ljava/lang/Object;
.source "PrefsBackedContentProvider.kt"

# interfaces
.implements Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008&\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0012\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0003\u001a\u00020\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/util/prefsbackedcontentprovider/BoolContractItem;",
        "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;",
        "",
        "default",
        "(Z)V",
        "getDefault",
        "()Ljava/lang/Boolean;",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final default:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/util/prefsbackedcontentprovider/BoolContractItem;-><init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/util/prefsbackedcontentprovider/BoolContractItem;->default:Z

    return-void
.end method

.method public synthetic constructor <init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 57
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/util/prefsbackedcontentprovider/BoolContractItem;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public getDefault()Ljava/lang/Boolean;
    .locals 1

    .line 57
    iget-boolean v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/BoolContractItem;->default:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefault()Ljava/lang/Object;
    .locals 1

    .line 57
    invoke-virtual {p0}, Lcom/squareup/util/prefsbackedcontentprovider/BoolContractItem;->getDefault()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 57
    invoke-static {p0}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem$DefaultImpls;->getPath(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPref()Ljava/lang/String;
    .locals 1

    .line 57
    invoke-static {p0}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem$DefaultImpls;->getPref(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
