.class final Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;
.super Ljava/lang/Object;
.source "RxBroadcastReceiver.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/RxBroadcastReceiver;->registerForIntents(Landroid/content/Context;Lrx/Emitter$BackpressureMode;[Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lrx/Emitter<",
        "TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lrx/Emitter;",
        "Landroid/content/Intent;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actions:[Ljava/lang/String;

.field final synthetic $this_registerForIntents:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;->$this_registerForIntents:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;->$actions:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lrx/Emitter;

    invoke-virtual {p0, p1}, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;->call(Lrx/Emitter;)V

    return-void
.end method

.method public final call(Lrx/Emitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Emitter<",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1;-><init>(Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;Lrx/Emitter;)V

    .line 55
    new-instance v1, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$1;-><init>(Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1;)V

    check-cast v1, Lrx/functions/Cancellable;

    invoke-interface {p1, v1}, Lrx/Emitter;->setCancellation(Lrx/functions/Cancellable;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;->$this_registerForIntents:Landroid/content/Context;

    check-cast v0, Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;->$actions:[Ljava/lang/String;

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Intents;->createFilter([Ljava/lang/String;)Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
