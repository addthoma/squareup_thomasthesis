.class final Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;
.super Ljava/lang/Object;
.source "Rx2Tuples.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Rx2Tuples;->toTripletFromPair()Lio/reactivex/functions/BiFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "Lkotlin/Pair<",
        "+TA;+TB;>;TC;",
        "Lcom/squareup/util/tuple/Triplet<",
        "TA;TB;TC;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u00052\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00072\u0006\u0010\u0008\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/tuple/Triplet;",
        "A",
        "B",
        "C",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "t",
        "apply",
        "(Lkotlin/Pair;Ljava/lang/Object;)Lcom/squareup/util/tuple/Triplet;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;

    invoke-direct {v0}, Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;-><init>()V

    sput-object v0, Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;Ljava/lang/Object;)Lcom/squareup/util/tuple/Triplet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+TA;+TB;>;TC;)",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "t"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    .line 77
    new-instance v1, Lcom/squareup/util/tuple/Triplet;

    invoke-direct {v1, v0, p1, p2}, Lcom/squareup/util/tuple/Triplet;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;->apply(Lkotlin/Pair;Ljava/lang/Object;)Lcom/squareup/util/tuple/Triplet;

    move-result-object p1

    return-object p1
.end method
