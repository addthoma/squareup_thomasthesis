.class public final Lcom/squareup/util/RxWatchdog_Factory;
.super Ljava/lang/Object;
.source "RxWatchdog_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/util/RxWatchdog<",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/util/RxWatchdog_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/util/RxWatchdog_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/util/RxWatchdog_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/util/RxWatchdog_Factory<",
            "TT;>;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/util/RxWatchdog_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/RxWatchdog_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/util/RxWatchdog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/util/RxWatchdog<",
            "TT;>;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/util/RxWatchdog;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/util/RxWatchdog;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/RxWatchdog<",
            "TT;>;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/util/RxWatchdog_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Scheduler;

    iget-object v1, p0, Lcom/squareup/util/RxWatchdog_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static {v0, v1}, Lcom/squareup/util/RxWatchdog_Factory;->newInstance(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/util/RxWatchdog;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/util/RxWatchdog_Factory;->get()Lcom/squareup/util/RxWatchdog;

    move-result-object v0

    return-object v0
.end method
