.class public final Lcom/squareup/updatecustomerapi/UpdateCustomerFlowKt;
.super Ljava/lang/Object;
.source "UpdateCustomerFlow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u001a\u000c\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0002*\u0004\u0018\u00010\u0001\u00a8\u0006\u0004"
    }
    d2 = {
        "toContactValidationType",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;",
        "",
        "toInt",
        "update-customer_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toContactValidationType(I)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;
    .locals 1

    if-ltz p0, :cond_0

    .line 14
    invoke-static {}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->values()[Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    move-result-object v0

    aget-object p0, v0, p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final toInt(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;)I
    .locals 0

    if-eqz p0, :cond_0

    .line 11
    invoke-virtual {p0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->ordinal()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, -0x1

    :goto_0
    return p0
.end method
