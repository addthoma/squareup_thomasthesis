.class public final Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;
.super Ljava/lang/Object;
.source "ScopedHeadset_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/ScopedHeadset;",
        ">;"
    }
.end annotation


# instance fields
.field private final headsetProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;->headsetProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/ScopedHeadset;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/wavpool/swipe/ScopedHeadset;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/ScopedHeadset;-><init>(Lcom/squareup/wavpool/swipe/Headset;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/ScopedHeadset;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;->headsetProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Headset;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;->newInstance(Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/ScopedHeadset;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/ScopedHeadset_Factory;->get()Lcom/squareup/wavpool/swipe/ScopedHeadset;

    move-result-object v0

    return-object v0
.end method
