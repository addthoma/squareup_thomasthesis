.class public final Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;
.super Ljava/lang/Object;
.source "DecoderModule_ProvideGen2SignalDecoderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory$InstanceHolder;->access$000()Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideGen2SignalDecoder()Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule;->provideGen2SignalDecoder()Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;->provideGen2SignalDecoder()Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;->get()Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;

    move-result-object v0

    return-object v0
.end method
