.class public final Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;
.super Ljava/lang/Object;
.source "HeadsetModule_HeadsetConnectionFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/HeadsetConnection;",
        ">;"
    }
.end annotation


# instance fields
.field private final headsetProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;->headsetProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static headsetConnection(Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnection;
    .locals 1

    .line 33
    invoke-static {p0}, Lcom/squareup/wavpool/swipe/HeadsetModule;->headsetConnection(Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnection;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/HeadsetConnection;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/HeadsetConnection;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;->headsetProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Headset;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;->headsetConnection(Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/HeadsetModule_HeadsetConnectionFactory;->get()Lcom/squareup/wavpool/swipe/HeadsetConnection;

    move-result-object v0

    return-object v0
.end method
