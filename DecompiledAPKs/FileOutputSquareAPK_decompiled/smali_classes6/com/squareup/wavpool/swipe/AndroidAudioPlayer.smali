.class public Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;
.super Ljava/lang/Object;
.source "AndroidAudioPlayer.java"

# interfaces
.implements Lcom/squareup/wavpool/swipe/AudioPlayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;,
        Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;
    }
.end annotation


# static fields
.field private static final AUDIO_TRACK_INIT_ATTEMPTS:I = 0x5

.field private static final NUM_AUDIO_CHANNELS:I = 0x2


# instance fields
.field private final audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

.field private final audioRunning:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final audioTrackFinisher:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private currentActiveTone:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;

.field private final lcrExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final outputSampleRate:Ljava/lang/Integer;

.field private final queuedTones:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;",
            ">;"
        }
    .end annotation
.end field

.field private final session:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
            ">;"
        }
    .end annotation
.end field

.field private warnedOnError:Z


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Ljavax/inject/Provider;Ljava/util/concurrent/ExecutorService;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ")V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->outputSampleRate:Ljava/lang/Integer;

    .line 42
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->audioTrackFinisher:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    .line 43
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->audioRunning:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    .line 45
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->session:Ljavax/inject/Provider;

    .line 46
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    .line 47
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->queuedTones:Ljava/util/Queue;

    .line 48
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 49
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;Z[S)Landroid/media/AudioTrack;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->buildAudioTrack(Z[S)Landroid/media/AudioTrack;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->checkAndPlay()V

    return-void
.end method

.method private blowUpSamples(Z[S)[S
    .locals 3

    if-nez p1, :cond_0

    return-object p2

    .line 172
    :cond_0
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->outputSampleRate:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->outputSampleRate:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    array-length v1, p2

    rem-int/2addr v0, v1

    sub-int/2addr p1, v0

    .line 173
    new-array p1, p1, [S

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 176
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 177
    array-length v2, p2

    invoke-static {p2, v0, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    array-length v2, p2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_1
    return-object p1
.end method

.method private buildAudioTrack(Z[S)Landroid/media/AudioTrack;
    .locals 6

    .line 104
    invoke-direct {p0, p1, p2}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->blowUpSamples(Z[S)[S

    move-result-object p2

    .line 105
    array-length v0, p2

    .line 106
    div-int/lit8 v1, v0, 0x2

    .line 108
    invoke-direct {p0, v0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->createAudioTrack(I)Landroid/media/AudioTrack;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_1

    sub-int v5, v0, v4

    .line 113
    invoke-virtual {v2, p2, v4, v5}, Landroid/media/AudioTrack;->write([SII)I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_3

    const/4 p2, -0x1

    .line 117
    invoke-virtual {v2, v3, v1, p2}, Landroid/media/AudioTrack;->setLoopPoints(III)I

    move-result p2

    if-nez p2, :cond_2

    goto :goto_1

    .line 119
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "couldn\'t loop the track: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    const/high16 p2, 0x3f800000    # 1.0f

    .line 124
    invoke-virtual {v2, p2, p2}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    if-nez p1, :cond_4

    .line 127
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->audioTrackFinisher:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    new-instance p2, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;

    invoke-direct {p2, p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;-><init>(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;)V

    invoke-virtual {p1, v1, v2, p2}, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->notifyWhenComplete(ILandroid/media/AudioTrack;Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;)V

    :cond_4
    return-object v2
.end method

.method private declared-synchronized checkAndPlay()V
    .locals 1

    monitor-enter p0

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->audioRunning:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 91
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->queuedTones:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    .line 93
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->currentActiveTone:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->currentActiveTone:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->access$000(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->currentActiveTone:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->stop()V

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->queuedTones:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;

    .line 98
    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->play()V

    .line 99
    iput-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->currentActiveTone:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private createAudioTrack(I)Landroid/media/AudioTrack;
    .locals 10

    mul-int/lit8 v7, p1, 0x2

    const/4 v0, 0x0

    const/4 v8, 0x0

    :goto_0
    const/4 v0, 0x5

    if-ge v8, v0, :cond_2

    .line 142
    new-instance v9, Landroid/media/AudioTrack;

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->outputSampleRate:Ljava/lang/Integer;

    .line 143
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0xc

    const/4 v4, 0x2

    const/4 v6, 0x0

    move-object v0, v9

    move v5, v7

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    .line 146
    invoke-virtual {v9}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    if-eqz v0, :cond_0

    return-object v9

    .line 150
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->warnedOnError:Z

    if-nez v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$AndroidAudioPlayer$x-M9ee_DBND1IKsvjXBuQXTM8QA;

    invoke-direct {v1, p0, v8, v9, p1}, Lcom/squareup/wavpool/swipe/-$$Lambda$AndroidAudioPlayer$x-M9ee_DBND1IKsvjXBuQXTM8QA;-><init>(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;ILandroid/media/AudioTrack;I)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    .line 158
    iput-boolean v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->warnedOnError:Z

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public enableTransmission()V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$AndroidAudioPlayer$p3e_OVTNXoSVyDsWbvbf4115hBY;

    invoke-direct {v1, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$AndroidAudioPlayer$p3e_OVTNXoSVyDsWbvbf4115hBY;-><init>(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public declared-synchronized forwardToReader(Z[S)V
    .locals 2

    monitor-enter p0

    .line 69
    :try_start_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->queuedTones:Ljava/util/Queue;

    new-instance v1, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;-><init>(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;Z[S)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->checkAndPlay()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public synthetic lambda$createAudioTrack$2$AndroidAudioPlayer(ILandroid/media/AudioTrack;I)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    .line 156
    invoke-virtual {p2}, Landroid/media/AudioTrack;->getState()I

    move-result p2

    invoke-static {p1, p2, p3}, Lcom/squareup/wavpool/swipe/AudioEventKt;->audiotrackNotCreatedEvent(III)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object p1

    .line 155
    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$enableTransmission$0$AndroidAudioPlayer()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->session:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->session:Ljavax/inject/Provider;

    .line 62
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    .line 61
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->cr_comms_backend_audio_enable_tx_for_connection(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$notifyTransmissionComplete$1$AndroidAudioPlayer()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->session:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->session:Ljavax/inject/Provider;

    .line 78
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    .line 77
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->cr_comms_backend_audio_notify_phy_tx_complete(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    return-void
.end method

.method public notifyTransmissionComplete()V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$AndroidAudioPlayer$Ulwez-0l2cEWY1Xc29PgXoTCWts;

    invoke-direct {v1, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$AndroidAudioPlayer$Ulwez-0l2cEWY1Xc29PgXoTCWts;-><init>(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public declared-synchronized reset()V
    .locals 1

    monitor-enter p0

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->queuedTones:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->stopSendingToReader()V

    const/4 v0, 0x0

    .line 55
    iput-boolean v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->warnedOnError:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopSendingToReader()V
    .locals 1

    monitor-enter p0

    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->currentActiveTone:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->currentActiveTone:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->stop()V

    const/4 v0, 0x0

    .line 85
    iput-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->currentActiveTone:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
