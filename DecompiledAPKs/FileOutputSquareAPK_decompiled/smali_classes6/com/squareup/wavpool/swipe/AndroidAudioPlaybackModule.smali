.class public abstract Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule;
.super Ljava/lang/Object;
.source "AndroidAudioPlaybackModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAudioGraphInitiailizer(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/wavpool/swipe/AudioStartAndStopper;)Lcom/squareup/wavpool/swipe/AudioGraphInitializer;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 67
    new-instance v0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;

    invoke-direct {v0, p0, p1}, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;-><init>(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/wavpool/swipe/AudioStartAndStopper;)V

    return-object v0
.end method

.method static provideAudioPlayer(Ljava/util/concurrent/ExecutorService;Ljava/lang/Integer;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)Lcom/squareup/wavpool/swipe/AudioPlayer;
    .locals 10
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/lang/Integer;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ")",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;"
        }
    .end annotation

    .line 49
    new-instance v9, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    move-object v0, v9

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move-object v4, p0

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;-><init>(Ljava/lang/Integer;Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Ljavax/inject/Provider;Ljava/util/concurrent/ExecutorService;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)V

    return-object v9
.end method

.method static provideAudioStartAndStopper(Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Landroid/media/AudioManager;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/loader/LibraryLoader;Ljavax/inject/Provider;Lcom/squareup/cardreader/R6CardReaderAwakener;FLcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/wavpool/swipe/AudioStartAndStopper;
    .locals 13
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Lcom/squareup/cardreader/CardReaderPauseAndResumer$Running;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadBus;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/media/AudioManager;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;",
            "Lcom/squareup/cardreader/R6CardReaderAwakener;",
            "F",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ")",
            "Lcom/squareup/wavpool/swipe/AudioStartAndStopper;"
        }
    .end annotation

    .line 33
    new-instance v12, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    move-object v0, v12

    move-object v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p5

    move-object/from16 v7, p4

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;-><init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Landroid/media/AudioManager;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/loader/LibraryLoader;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/R6CardReaderAwakener;FLcom/squareup/cardreader/CardReaderListeners;)V

    return-object v12
.end method

.method static provideAudioTrackFinisher(Lcom/squareup/thread/executor/MainThread;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioTrackFinisher;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    invoke-direct {v0, p0, p1}, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;-><init>(Lcom/squareup/thread/executor/MainThread;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method static provideCanPlayAudio(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Landroid/view/accessibility/AccessibilityManager;)Z
    .locals 0
    .param p2    # Ljavax/inject/Provider;
        .annotation runtime Lcom/squareup/cardreader/CardReaderPauseAndResumer$Running;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/view/accessibility/AccessibilityManager;",
            ")Z"
        }
    .end annotation

    .line 57
    invoke-interface {p0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 58
    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    .line 59
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateInProgress()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 60
    :cond_0
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
