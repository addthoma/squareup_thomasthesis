.class public Lcom/squareup/wavpool/swipe/SwipeBus;
.super Ljava/lang/Object;
.source "SwipeBus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/SwipeBus$Danger;
    }
.end annotation


# instance fields
.field private final failedSwipeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
            ">;"
        }
    .end annotation
.end field

.field private final successfulSwipeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/SwipeBus;->successfulSwipeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 17
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/SwipeBus;->failedSwipeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/wavpool/swipe/SwipeBus;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/SwipeBus;->successfulSwipeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/wavpool/swipe/SwipeBus;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/SwipeBus;->failedSwipeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method


# virtual methods
.method public DANGERdoNotUseDirectly()Lcom/squareup/wavpool/swipe/SwipeBus$Danger;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/wavpool/swipe/SwipeBus$1;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/SwipeBus$1;-><init>(Lcom/squareup/wavpool/swipe/SwipeBus;)V

    return-object v0
.end method

.method public post(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SwipeBus;->failedSwipeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public post(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SwipeBus;->successfulSwipeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
