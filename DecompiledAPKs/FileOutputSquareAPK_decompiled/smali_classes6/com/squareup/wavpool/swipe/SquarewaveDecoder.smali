.class public Lcom/squareup/wavpool/swipe/SquarewaveDecoder;
.super Ljava/lang/Object;
.source "SquarewaveDecoder.java"

# interfaces
.implements Lcom/squareup/squarewave/AudioFilter;
.implements Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;


# instance fields
.field private final bus:Lcom/squareup/badbus/BadEventSink;

.field private final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final crashnado:Lcom/squareup/crashnado/Crashnado;

.field private final decoder:Lcom/squareup/squarewave/SignalDecoder;

.field private final decoderExecutor:Ljava/util/concurrent/ExecutorService;

.field private delayedSwipeFailedPost:Ljava/lang/Runnable;

.field private final eventDataForwarder:Lcom/squareup/squarewave/EventDataForwarder;

.field private headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

.field private headsetDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final headsetListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

.field private final logger:Lcom/squareup/logging/SwipeEventLogger;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final pendingTasks:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/util/concurrent/Future<",
            "*>;>;"
        }
    .end annotation
.end field

.field private previousR4Signal:Lcom/squareup/wavpool/swipe/SqLinkSignal;

.field private final readerType:Lcom/squareup/wavpool/swipe/ReaderTypeProvider;

.field private final sampleFeeder:Lcom/squareup/squarewave/SampleFeeder;

.field private final sampleProcessor:Lcom/squareup/squarewave/gum/SampleProcessor;

.field private shouldSetLegacyReaderType:Z

.field private final swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;


# direct methods
.method public constructor <init>(Lcom/squareup/crashnado/Crashnado;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/squarewave/SampleFeeder;Lcom/squareup/squarewave/SignalDecoder;Ljava/util/concurrent/ExecutorService;Lcom/squareup/squarewave/gum/SampleProcessor;Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/logging/SwipeEventLogger;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V
    .locals 2

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;-><init>(ZZ)V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    .line 79
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->headsetDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 87
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->crashnado:Lcom/squareup/crashnado/Crashnado;

    .line 88
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->bus:Lcom/squareup/badbus/BadEventSink;

    .line 89
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    .line 90
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->sampleFeeder:Lcom/squareup/squarewave/SampleFeeder;

    .line 91
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->decoder:Lcom/squareup/squarewave/SignalDecoder;

    .line 92
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->decoderExecutor:Ljava/util/concurrent/ExecutorService;

    .line 93
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->sampleProcessor:Lcom/squareup/squarewave/gum/SampleProcessor;

    .line 94
    iput-object p9, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->eventDataForwarder:Lcom/squareup/squarewave/EventDataForwarder;

    .line 95
    iput-object p10, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->logger:Lcom/squareup/logging/SwipeEventLogger;

    .line 96
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 97
    iput-object p11, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->readerType:Lcom/squareup/wavpool/swipe/ReaderTypeProvider;

    .line 98
    iput-object p12, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 99
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->pendingTasks:Ljava/util/LinkedList;

    .line 100
    iput-object p13, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->headsetListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    return-void
.end method

.method private applyDecision(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;Lcom/squareup/squarewave/decode/DemodResult;)V
    .locals 2

    .line 321
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$SquarewaveDecoder$nAu1kXsMt6ddaTVx418oazHg3Jw;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/wavpool/swipe/-$$Lambda$SquarewaveDecoder$nAu1kXsMt6ddaTVx418oazHg3Jw;-><init>(Lcom/squareup/wavpool/swipe/SquarewaveDecoder;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;Lcom/squareup/squarewave/decode/DemodResult;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private blockAndFinishWork()V
    .locals 3

    .line 126
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->clearPendingAndDoneTasks()V

    .line 127
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->pendingTasks:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Future;

    .line 128
    invoke-interface {v1}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 131
    :try_start_0
    invoke-interface {v1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 133
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 139
    :cond_1
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->clearPendingAndDoneTasks()V

    return-void
.end method

.method private cancelDelayedSwipeFailedPost()V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->delayedSwipeFailedPost:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 144
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 145
    iput-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->delayedSwipeFailedPost:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private clearPendingAndDoneTasks()V
    .locals 3

    .line 160
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->pendingTasks:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 162
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 163
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Future;

    .line 164
    invoke-interface {v1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    :cond_1
    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private decide(JLcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Z)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;
    .locals 7

    .line 237
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->readerType:Lcom/squareup/wavpool/swipe/ReaderTypeProvider;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/ReaderTypeProvider;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p1

    .line 240
    :cond_0
    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->isProbablyNoise()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_2

    .line 244
    :cond_1
    iget-object v0, p4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->UNKNOWN_SIGNAL:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-eq v0, v1, :cond_2

    .line 245
    iget-object v0, p4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    goto :goto_0

    .line 247
    :cond_2
    iget-object v0, p4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    :goto_0
    move-object v6, v0

    .line 249
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-eq v6, v0, :cond_8

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-ne v6, v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    .line 252
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->previousR4Signal:Lcom/squareup/wavpool/swipe/SqLinkSignal;

    if-eqz p5, :cond_4

    .line 254
    iget-object p1, p4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    sget-object p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_GENERAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    if-ne p1, p2, :cond_4

    .line 256
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_2

    .line 257
    :cond_4
    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->isActivelyIgnored()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 259
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_2

    .line 260
    :cond_5
    iget-object p1, p4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    sget-object p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_FLASH_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    if-ne p1, p2, :cond_6

    .line 261
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DEAD_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_2

    .line 262
    :cond_6
    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result p1

    if-eqz p1, :cond_7

    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->hasCard()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 263
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_2

    .line 265
    :cond_7
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_2

    :cond_8
    :goto_1
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    .line 250
    invoke-direct/range {v1 .. v6}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->handleR4Signal(JLcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method private decodeInBackgroundThrowing(Lcom/squareup/squarewave/Signal;)V
    .locals 9

    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 207
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 209
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->cancelDelayedSwipeFailedPost()V

    .line 210
    invoke-virtual {p0, p1}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v8

    .line 212
    iget-object v0, p1, Lcom/squareup/squarewave/Signal;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 213
    iget-object v4, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    move-object v0, p0

    move-object v3, v8

    .line 215
    invoke-direct/range {v0 .. v5}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->decide(JLcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Z)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    move-result-object v0

    .line 216
    invoke-direct {p0, v0, v8}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->applyDecision(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;Lcom/squareup/squarewave/decode/DemodResult;)V

    .line 218
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    sub-long/2addr v1, v6

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    long-to-int v2, v1

    .line 219
    invoke-direct {p0, p1, v0, v2}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->logEvent(Lcom/squareup/squarewave/Signal;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;I)V

    return-void
.end method

.method private decodeSignalInBackground(Lcom/squareup/squarewave/Signal;)V
    .locals 3

    .line 174
    :try_start_0
    invoke-direct {p0, p1}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->decodeInBackgroundThrowing(Lcom/squareup/squarewave/Signal;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 177
    invoke-static {p1}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Error decoding: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "Error processing swipe"

    .line 178
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    move-object v1, p0

    :goto_0
    if-eqz v1, :cond_2

    .line 192
    instance-of v2, v1, Ljava/net/UnknownHostException;

    if-eqz v2, :cond_1

    return-object v0

    .line 195
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    goto :goto_0

    .line 198
    :cond_2
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 199
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 200
    invoke-virtual {p0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 201
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    .line 202
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private handleR4Signal(JLcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;
    .locals 7

    .line 275
    new-instance v6, Lcom/squareup/wavpool/swipe/SqLinkSignal;

    move-object v0, v6

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/wavpool/swipe/SqLinkSignal;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;J)V

    .line 277
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->previousR4Signal:Lcom/squareup/wavpool/swipe/SqLinkSignal;

    invoke-virtual {v6, p1}, Lcom/squareup/wavpool/swipe/SqLinkSignal;->shouldBeIgnored(Lcom/squareup/wavpool/swipe/SqLinkSignal;)Z

    move-result p1

    .line 278
    iput-object v6, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->previousR4Signal:Lcom/squareup/wavpool/swipe/SqLinkSignal;

    if-eqz p1, :cond_1

    .line 280
    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-ne p5, p1, :cond_0

    .line 283
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DELAYED_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_0

    .line 285
    :cond_0
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_0

    .line 288
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result p1

    if-nez p1, :cond_2

    .line 289
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_0

    .line 290
    :cond_2
    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->isActivelyIgnored()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 291
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_0

    .line 293
    :cond_3
    sget-object p1, Lcom/squareup/wavpool/swipe/SquarewaveDecoder$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$PacketType:[I

    iget-object p2, p4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x1

    if-eq p1, p2, :cond_8

    const/4 p2, 0x2

    if-eq p1, p2, :cond_7

    const/4 p2, 0x3

    if-eq p1, p2, :cond_6

    const/4 p2, 0x4

    if-ne p1, p2, :cond_5

    .line 305
    invoke-virtual {p3}, Lcom/squareup/squarewave/decode/DemodResult;->hasCard()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 306
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_0

    .line 308
    :cond_4
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_0

    .line 312
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unexpected packet type: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 302
    :cond_6
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DEAD_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_0

    .line 299
    :cond_7
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    goto :goto_0

    .line 296
    :cond_8
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    :goto_0
    return-object p1
.end method

.method private logEvent(Lcom/squareup/squarewave/Signal;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;I)V
    .locals 2

    .line 369
    iget-object v0, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_BLANK:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    if-ne v0, v1, :cond_0

    return-void

    .line 373
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->buildEventLog(Lcom/squareup/squarewave/Signal;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;I)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    move-result-object p1

    .line 374
    iget-object p2, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->logger:Lcom/squareup/logging/SwipeEventLogger;

    invoke-interface {p2, p1}, Lcom/squareup/logging/SwipeEventLogger;->logReaderCarrierDetectEvent(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V

    return-void
.end method


# virtual methods
.method protected buildEventLog(Lcom/squareup/squarewave/Signal;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;I)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;
    .locals 4

    .line 379
    iget-object v0, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    .line 380
    iget-object v1, p1, Lcom/squareup/squarewave/Signal;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    iget-object v1, v1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iget-object v1, v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 381
    iget-object v2, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->sample_rate:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v1, v1

    const v3, 0x49742400    # 1000000.0f

    mul-float v1, v1, v3

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x32

    add-int/2addr v1, p3

    .line 390
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    iput-object p3, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    .line 391
    iput-object p2, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 392
    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    .line 393
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    .line 394
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    move-result-object p1

    return-object p1
.end method

.method protected decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->decoder:Lcom/squareup/squarewave/SignalDecoder;

    invoke-interface {v0, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    .line 224
    invoke-virtual {p1}, Lcom/squareup/squarewave/Signal;->zeroize()V

    .line 226
    iget-boolean v1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->shouldSetLegacyReaderType:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->sampleProcessor:Lcom/squareup/squarewave/gum/SampleProcessor;

    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-interface {v1, p1}, Lcom/squareup/squarewave/gum/SampleProcessor;->setLegacyReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)V

    const/4 p1, 0x0

    .line 228
    iput-boolean p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->shouldSetLegacyReaderType:Z

    :cond_0
    return-object v0
.end method

.method public finish()V
    .locals 2

    .line 117
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->blockAndFinishWork()V

    .line 118
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->cancelDelayedSwipeFailedPost()V

    .line 119
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->sampleFeeder:Lcom/squareup/squarewave/SampleFeeder;

    invoke-virtual {v0}, Lcom/squareup/squarewave/SampleFeeder;->finish()V

    .line 120
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->eventDataForwarder:Lcom/squareup/squarewave/EventDataForwarder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/squarewave/EventDataForwarder;->setOnCarrierDetectedListener(Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->headsetDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method public synthetic lambda$applyDecision$3$SquarewaveDecoder(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;Lcom/squareup/squarewave/decode/DemodResult;)V
    .locals 4

    .line 327
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p2, v0

    const-string p1, "Demodulated signal with %s, but headset not attached. Ignoring."

    .line 328
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 332
    :cond_0
    sget-object v0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$Decision:[I

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 363
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown decision: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 359
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->bus:Lcom/squareup/badbus/BadEventSink;

    new-instance p2, Lcom/squareup/wavpool/swipe/SwipeEvents$ContactSupport;

    invoke-direct {p2}, Lcom/squareup/wavpool/swipe/SwipeEvents$ContactSupport;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 346
    :pswitch_1
    new-instance p1, Lcom/squareup/wavpool/swipe/-$$Lambda$SquarewaveDecoder$v_TsqvmeMS5IK8Hn3IZ1nz3mpnk;

    invoke-direct {p1, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$SquarewaveDecoder$v_TsqvmeMS5IK8Hn3IZ1nz3mpnk;-><init>(Lcom/squareup/wavpool/swipe/SquarewaveDecoder;)V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->delayedSwipeFailedPost:Ljava/lang/Runnable;

    .line 350
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object p2, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->delayedSwipeFailedPost:Ljava/lang/Runnable;

    const-wide/16 v0, 0x75d

    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    .line 351
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->bus:Lcom/squareup/badbus/BadEventSink;

    new-instance p2, Lcom/squareup/wavpool/swipe/SwipeEvents$WaitingForR4SlowSignal;

    invoke-direct {p2}, Lcom/squareup/wavpool/swipe/SwipeEvents$WaitingForR4SlowSignal;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 342
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-static {p2}, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->fromDemodResult(Lcom/squareup/squarewave/decode/DemodResult;)Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wavpool/swipe/SwipeBus;->post(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    goto :goto_0

    .line 334
    :pswitch_3
    invoke-virtual {p2}, Lcom/squareup/squarewave/decode/DemodResult;->getCard()Lcom/squareup/Card;

    move-result-object p1

    .line 335
    invoke-virtual {p2}, Lcom/squareup/squarewave/decode/DemodResult;->hasTrack1Data()Z

    move-result v0

    .line 336
    invoke-virtual {p2}, Lcom/squareup/squarewave/decode/DemodResult;->hasTrack2Data()Z

    move-result p2

    .line 337
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    new-instance v2, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    iget-object v3, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-direct {v2, v3, p1, v0, p2}, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;-><init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;ZZ)V

    invoke-virtual {v1, v2}, Lcom/squareup/wavpool/swipe/SwipeBus;->post(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    :goto_0
    :pswitch_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic lambda$null$2$SquarewaveDecoder()V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->fromDemodResult(Lcom/squareup/squarewave/decode/DemodResult;)Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/wavpool/swipe/SwipeBus;->post(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    return-void
.end method

.method public synthetic lambda$onCarrierDetect$1$SquarewaveDecoder(Lcom/squareup/squarewave/Signal;)V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->crashnado:Lcom/squareup/crashnado/Crashnado;

    invoke-interface {v0}, Lcom/squareup/crashnado/Crashnado;->prepareStack()V

    .line 154
    invoke-direct {p0, p1}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->decodeSignalInBackground(Lcom/squareup/squarewave/Signal;)V

    return-void
.end method

.method public synthetic lambda$start$0$SquarewaveDecoder(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 109
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    return-void
.end method

.method public onCarrierDetect(Lcom/squareup/squarewave/Signal;)V
    .locals 2

    .line 151
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->clearPendingAndDoneTasks()V

    .line 152
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->decoderExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$SquarewaveDecoder$AhY0bQ2Iud--r-Jsj7IjCdhEfc4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/wavpool/swipe/-$$Lambda$SquarewaveDecoder$AhY0bQ2Iud--r-Jsj7IjCdhEfc4;-><init>(Lcom/squareup/wavpool/swipe/SquarewaveDecoder;Lcom/squareup/squarewave/Signal;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    .line 156
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->pendingTasks:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public process(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->sampleFeeder:Lcom/squareup/squarewave/SampleFeeder;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/squarewave/SampleFeeder;->process(Ljava/nio/ByteBuffer;I)V

    return-void
.end method

.method public start(I)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->eventDataForwarder:Lcom/squareup/squarewave/EventDataForwarder;

    invoke-virtual {v0, p0}, Lcom/squareup/squarewave/EventDataForwarder;->setOnCarrierDetectedListener(Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->sampleFeeder:Lcom/squareup/squarewave/SampleFeeder;

    invoke-virtual {v0, p1}, Lcom/squareup/squarewave/SampleFeeder;->start(I)V

    const/4 p1, 0x1

    .line 106
    iput-boolean p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->shouldSetLegacyReaderType:Z

    .line 108
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->headsetDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->headsetListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;->onHeadsetStateChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$SquarewaveDecoder$rIeQp8zCVgUqUgku3xa-V_AX8E8;

    invoke-direct {v1, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$SquarewaveDecoder$rIeQp8zCVgUqUgku3xa-V_AX8E8;-><init>(Lcom/squareup/wavpool/swipe/SquarewaveDecoder;)V

    .line 109
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 108
    invoke-virtual {p1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method testApplyDecision(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;Lcom/squareup/squarewave/decode/DemodResult;)V
    .locals 0

    .line 398
    invoke-direct {p0, p1, p2}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;->applyDecision(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;Lcom/squareup/squarewave/decode/DemodResult;)V

    return-void
.end method
