.class public final Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;
.super Ljava/lang/Object;
.source "LegacyLcrAudioModule_ProvideAudioBackendSessionFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
        ">;"
    }
.end annotation


# instance fields
.field private final audioBackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;->audioBackendProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAudioBackendSession(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;)Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;
    .locals 0

    .line 38
    invoke-static {p0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule;->provideAudioBackendSession(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;)Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;->audioBackendProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;->provideAudioBackendSession(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;)Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;->get()Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    move-result-object v0

    return-object v0
.end method
