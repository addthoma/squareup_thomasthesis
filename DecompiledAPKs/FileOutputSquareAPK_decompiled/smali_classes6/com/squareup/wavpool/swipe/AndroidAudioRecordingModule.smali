.class public abstract Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule;
.super Ljava/lang/Object;
.source "AndroidAudioRecordingModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideMicRecorder(Lcom/squareup/crashnado/Crashnado;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AndroidDeviceParams;Lcom/squareup/thread/executor/MainThread;Landroid/telephony/TelephonyManager;Lcom/squareup/badbus/BadEventSink;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/MicRecorder;
    .locals 10
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crashnado/Crashnado;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Landroid/telephony/TelephonyManager;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/AudioFilter;",
            ">;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ")",
            "Lcom/squareup/wavpool/swipe/MicRecorder;"
        }
    .end annotation

    .line 29
    new-instance v9, Lcom/squareup/wavpool/swipe/MicRecorder;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p6

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/wavpool/swipe/MicRecorder;-><init>(Lcom/squareup/crashnado/Crashnado;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AndroidDeviceParams;Lcom/squareup/thread/executor/MainThread;Landroid/telephony/TelephonyManager;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V

    return-object v9
.end method


# virtual methods
.method abstract provideRecorder(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/wavpool/swipe/Recorder;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
