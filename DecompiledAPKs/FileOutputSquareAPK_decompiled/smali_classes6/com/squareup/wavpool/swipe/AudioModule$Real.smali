.class public abstract Lcom/squareup/wavpool/swipe/AudioModule$Real;
.super Ljava/lang/Object;
.source "AudioModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/AudioModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Real"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideEventDataListener(Lcom/squareup/squarewave/EventDataForwarder;)Lcom/squareup/squarewave/EventDataListener;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideHeadsetConnectionListener(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
