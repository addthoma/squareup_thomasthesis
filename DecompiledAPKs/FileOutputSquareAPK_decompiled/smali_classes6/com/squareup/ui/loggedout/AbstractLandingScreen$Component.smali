.class public interface abstract Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;
.super Ljava/lang/Object;
.source "AbstractLandingScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/AbstractLandingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/loggedout/LandingLandscapeView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/loggedout/LandingPortraitView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/loggedout/WorldLandingView;)V
.end method
