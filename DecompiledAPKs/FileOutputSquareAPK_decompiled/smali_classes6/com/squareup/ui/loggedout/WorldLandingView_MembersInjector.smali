.class public final Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;
.super Ljava/lang/Object;
.source "WorldLandingView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/loggedout/WorldLandingView;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/WorldLandingScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/WorldLandingScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/WorldLandingScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/loggedout/WorldLandingView;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectLocale(Lcom/squareup/ui/loggedout/WorldLandingView;Ljava/util/Locale;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/loggedout/WorldLandingView;->locale:Ljava/util/Locale;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/loggedout/WorldLandingView;Ljava/lang/Object;)V
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/ui/loggedout/WorldLandingScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/loggedout/WorldLandingView;->presenter:Lcom/squareup/ui/loggedout/WorldLandingScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/loggedout/WorldLandingView;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;->injectPresenter(Lcom/squareup/ui/loggedout/WorldLandingView;Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;->injectLocale(Lcom/squareup/ui/loggedout/WorldLandingView;Ljava/util/Locale;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/loggedout/WorldLandingView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/WorldLandingView_MembersInjector;->injectMembers(Lcom/squareup/ui/loggedout/WorldLandingView;)V

    return-void
.end method
