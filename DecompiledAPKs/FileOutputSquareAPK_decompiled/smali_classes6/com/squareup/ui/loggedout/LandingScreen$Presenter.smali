.class Lcom/squareup/ui/loggedout/LandingScreen$Presenter;
.super Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;
.source "LandingScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/LandingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter<",
        "Lcom/squareup/ui/loggedout/LandingScreen$PaymentLandingView;",
        ">;"
    }
.end annotation


# instance fields
.field private insertAnimPlayed:Z


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    sget-object v3, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->PAYMENT:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;Lcom/squareup/analytics/Analytics;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    return-void
.end method


# virtual methods
.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 62
    invoke-super {p0, p1}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 63
    iget-boolean p1, p0, Lcom/squareup/ui/loggedout/LandingScreen$Presenter;->insertAnimPlayed:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 64
    iput-boolean p1, p0, Lcom/squareup/ui/loggedout/LandingScreen$Presenter;->insertAnimPlayed:Z

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LandingScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/loggedout/LandingScreen$PaymentLandingView;

    invoke-interface {p1}, Lcom/squareup/ui/loggedout/LandingScreen$PaymentLandingView;->startReaderAnimation()V

    :cond_0
    return-void
.end method
