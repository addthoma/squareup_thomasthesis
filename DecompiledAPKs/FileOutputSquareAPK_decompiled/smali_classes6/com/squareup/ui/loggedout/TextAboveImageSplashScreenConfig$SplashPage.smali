.class public final Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;
.super Ljava/lang/Object;
.source "TextAboveImageSplashScreenConfig.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SplashPage"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B/\u0012\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J3\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00032\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;",
        "",
        "icon",
        "",
        "image",
        "text",
        "viewEvent",
        "Lcom/squareup/analytics/RegisterViewName;",
        "(IIILcom/squareup/analytics/RegisterViewName;)V",
        "getIcon",
        "()I",
        "getImage",
        "getText",
        "getViewEvent",
        "()Lcom/squareup/analytics/RegisterViewName;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final icon:I

.field private final image:I

.field private final text:I

.field private final viewEvent:Lcom/squareup/analytics/RegisterViewName;


# direct methods
.method public constructor <init>(IIILcom/squareup/analytics/RegisterViewName;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->icon:I

    iput p2, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->image:I

    iput p3, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->text:I

    iput-object p4, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->viewEvent:Lcom/squareup/analytics/RegisterViewName;

    return-void
.end method

.method public synthetic constructor <init>(IIILcom/squareup/analytics/RegisterViewName;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    const/4 p1, -0x1

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x0

    .line 73
    check-cast p4, Lcom/squareup/analytics/RegisterViewName;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;-><init>(IIILcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;IIILcom/squareup/analytics/RegisterViewName;ILjava/lang/Object;)Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->icon:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->image:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->text:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->viewEvent:Lcom/squareup/analytics/RegisterViewName;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->copy(IIILcom/squareup/analytics/RegisterViewName;)Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->icon:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->image:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->text:I

    return v0
.end method

.method public final component4()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->viewEvent:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public final copy(IIILcom/squareup/analytics/RegisterViewName;)Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;
    .locals 1

    new-instance v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;-><init>(IIILcom/squareup/analytics/RegisterViewName;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->icon:I

    iget v1, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->icon:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->image:I

    iget v1, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->image:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->text:I

    iget v1, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->text:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->viewEvent:Lcom/squareup/analytics/RegisterViewName;

    iget-object p1, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->viewEvent:Lcom/squareup/analytics/RegisterViewName;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getIcon()I
    .locals 1

    .line 70
    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->icon:I

    return v0
.end method

.method public final getImage()I
    .locals 1

    .line 71
    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->image:I

    return v0
.end method

.method public final getText()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->text:I

    return v0
.end method

.method public final getViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->viewEvent:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->icon:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->image:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->text:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->viewEvent:Lcom/squareup/analytics/RegisterViewName;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SplashPage(icon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->icon:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", image="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->image:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->text:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", viewEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->viewEvent:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
