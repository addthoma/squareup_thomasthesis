.class public final Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;
.super Ljava/lang/Object;
.source "LoggedOutActivity_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/loggedout/LoggedOutActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityResultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationChangeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final containerActivityDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;"
        }
    .end annotation
.end field

.field private final contentViewInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final createAccountStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/CreateAccountStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinkHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final focusedActivityScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final impersonationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ImpersonationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final locationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedOutFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaButtonDisablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentBundleManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ImpersonationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/CreateAccountStarter;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->containerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 125
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 126
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->containerActivityDelegateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 127
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->softInputPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 128
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->impersonationHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 129
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 130
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->deepLinkHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 131
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 132
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->browserLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 133
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->loggedOutFinisherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 134
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->createAccountStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ImpersonationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/CreateAccountStarter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivity;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 160
    new-instance v25, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v25
.end method

.method public static injectBrowserLauncher(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/util/BrowserLauncher;)V
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method public static injectContainer(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->container:Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    return-void
.end method

.method public static injectContainerActivityDelegate(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/container/ContainerActivityDelegate;)V
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    return-void
.end method

.method public static injectCreateAccountStarter(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/loggedout/CreateAccountStarter;)V
    .locals 0

    .line 252
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->createAccountStarter:Lcom/squareup/loggedout/CreateAccountStarter;

    return-void
.end method

.method public static injectDeepLinkHelper(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/analytics/DeepLinkHelper;)V
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    return-void
.end method

.method public static injectEncryptedEmailsFromReferrals(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    return-void
.end method

.method public static injectImpersonationHelper(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/account/ImpersonationHelper;)V
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->impersonationHelper:Lcom/squareup/account/ImpersonationHelper;

    return-void
.end method

.method public static injectLoggedOutFinisher(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutFinisher;)V
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->loggedOutFinisher:Lcom/squareup/ui/loggedout/LoggedOutFinisher;

    return-void
.end method

.method public static injectMainScheduler(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lrx/Scheduler;)V
    .locals 0
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .line 234
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->mainScheduler:Lrx/Scheduler;

    return-void
.end method

.method public static injectRunner(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    return-void
.end method

.method public static injectSoftInputPresenter(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/SoftInputPresenter;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/development/drawer/ContentViewInitializer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/MediaButtonDisabler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectContainer(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectRunner(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->containerActivityDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectContainerActivityDelegate(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/container/ContainerActivityDelegate;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/SoftInputPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectSoftInputPresenter(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/SoftInputPresenter;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->impersonationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/ImpersonationHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectImpersonationHelper(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/account/ImpersonationHelper;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectEncryptedEmailsFromReferrals(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->deepLinkHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/DeepLinkHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectDeepLinkHelper(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/analytics/DeepLinkHelper;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectMainScheduler(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lrx/Scheduler;)V

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/BrowserLauncher;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectBrowserLauncher(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/util/BrowserLauncher;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->loggedOutFinisherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutFinisher;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectLoggedOutFinisher(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutFinisher;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->createAccountStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/loggedout/CreateAccountStarter;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectCreateAccountStarter(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/loggedout/CreateAccountStarter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/loggedout/LoggedOutActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectMembers(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V

    return-void
.end method
