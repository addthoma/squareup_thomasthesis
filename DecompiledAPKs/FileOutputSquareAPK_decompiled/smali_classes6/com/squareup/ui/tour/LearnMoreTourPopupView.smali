.class public Lcom/squareup/ui/tour/LearnMoreTourPopupView;
.super Landroid/widget/RelativeLayout;
.source "LearnMoreTourPopupView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tour/LearnMoreTourPopupView$Component;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/tour/TourAdapter;

.field private callToActionButton:Landroid/widget/TextView;

.field private closeButton:Lcom/squareup/glyph/SquareGlyphView;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private nextButton:Landroid/widget/Button;

.field private pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

.field private pager:Landroidx/viewpager/widget/ViewPager;

.field presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const-class p2, Lcom/squareup/ui/tour/LearnMoreTourPopupView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tour/LearnMoreTourPopupView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView$Component;->inject(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V

    .line 46
    new-instance p1, Lcom/squareup/tour/TourAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->getContext()Landroid/content/Context;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    invoke-direct {p1, p2, v0}, Lcom/squareup/tour/TourAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Lcom/squareup/tour/TourAdapter;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroid/widget/Button;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->nextButton:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroid/widget/TextView;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->callToActionButton:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroidx/viewpager/widget/ViewPager;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pager:Landroidx/viewpager/widget/ViewPager;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 142
    sget v0, Lcom/squareup/loggedout/R$id;->tour_pager:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pager:Landroidx/viewpager/widget/ViewPager;

    .line 143
    sget v0, Lcom/squareup/loggedout/R$id;->tour_next_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->nextButton:Landroid/widget/Button;

    .line 144
    sget v0, Lcom/squareup/loggedout/R$id;->tour_close_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 145
    sget v0, Lcom/squareup/loggedout/R$id;->page_indicator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinPageIndicator;

    iput-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    .line 146
    sget v0, Lcom/squareup/loggedout/R$id;->tour_call_to_action:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->callToActionButton:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onFinishInflate$0$LearnMoreTourPopupView(Landroid/view/View;F)V
    .locals 2

    .line 116
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 117
    sget v1, Lcom/squareup/tour/common/R$id;->tour_image:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_1

    int-to-float v0, v0

    mul-float v0, v0, p2

    .line 121
    iget-object p2, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->device:Lcom/squareup/util/Device;

    invoke-interface {p2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p2

    if-eqz p2, :cond_0

    const p2, 0x3f4ccccd    # 0.8f

    mul-float v0, v0, p2

    .line 126
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->close()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 50
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->bindViews()V

    .line 53
    new-instance v0, Lcom/squareup/tour/TourAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/tour/TourAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->getItemsToShow()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->setItems(Ljava/util/List;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pager:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pager:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->hasCallToAction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->callToActionButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->getCallToActionText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    iget-object v0, v0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-le v0, v1, :cond_3

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v4, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    iget-object v4, v4, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v1

    if-ne v0, v4, :cond_2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->hasCallToAction()Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->nextButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->callToActionButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    iget-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    new-instance v1, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;-><init>(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    goto :goto_1

    .line 90
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->nextButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->hasCallToAction()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->callToActionButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->callToActionButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/tour/LearnMoreTourPopupView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView$2;-><init>(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->nextButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/tour/LearnMoreTourPopupView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView$3;-><init>(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/tour/LearnMoreTourPopupView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView$4;-><init>(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pager:Landroidx/viewpager/widget/ViewPager;

    new-instance v1, Lcom/squareup/ui/tour/-$$Lambda$LearnMoreTourPopupView$1R5FiVFtzViMXypBzs2_v_1OtcQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tour/-$$Lambda$LearnMoreTourPopupView$1R5FiVFtzViMXypBzs2_v_1OtcQ;-><init>(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V

    invoke-virtual {v0, v2, v1}, Landroidx/viewpager/widget/ViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;)V"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/tour/TourAdapter;->updatePages(Ljava/util/List;)V

    .line 133
    iget-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->nextButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    iget-object v1, v1, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->adapter:Lcom/squareup/tour/TourAdapter;

    iget-object v0, v0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
