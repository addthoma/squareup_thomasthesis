.class public Lcom/squareup/ui/permissions/EmployeeLockButtonWide;
.super Lcom/squareup/marketfont/MarketTextView;
.source "EmployeeLockButtonWide.java"

# interfaces
.implements Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;


# instance fields
.field presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/ui/permissions/EmployeeLockButtonWide;)V

    return-void
.end method

.method private getEmployeeButtonText()Ljava/lang/String;
    .locals 4

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getEmployeeFirstName()Ljava/lang/String;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getEmployeeInitials()Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/orderentry/R$string;->employee_management_lock_button_full:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "first_name"

    .line 76
    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    move-object v0, v1

    :cond_0
    return-object v0
.end method


# virtual methods
.method public asView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 35
    invoke-super {p0}, Lcom/squareup/marketfont/MarketTextView;->onAttachedToWindow()V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->dropView(Ljava/lang/Object;)V

    .line 41
    invoke-super {p0}, Lcom/squareup/marketfont/MarketTextView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 25
    invoke-super {p0}, Lcom/squareup/marketfont/MarketTextView;->onFinishInflate()V

    .line 27
    new-instance v0, Lcom/squareup/ui/permissions/EmployeeLockButtonWide$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide$1;-><init>(Lcom/squareup/ui/permissions/EmployeeLockButtonWide;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public showButton()V
    .locals 1

    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->setVisibility(I)V

    return-void
.end method

.method public showClock()V
    .locals 1

    .line 56
    sget v0, Lcom/squareup/ui/permissions/R$string;->employee_management_clock_in_out:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->setText(I)V

    return-void
.end method

.method public showLegacyGuest()V
    .locals 1

    .line 64
    sget v0, Lcom/squareup/orderentry/R$string;->employee_management_lock_button_guest:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->setText(I)V

    return-void
.end method

.method public showLegacyLoginOut(Ljava/lang/String;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->getEmployeeButtonText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showLoginOut(Ljava/lang/String;)V
    .locals 0

    .line 60
    invoke-virtual {p0, p1}, Lcom/squareup/ui/permissions/EmployeeLockButtonWide;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
