.class interface abstract Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;
.super Ljava/lang/Object;
.source "EmployeeLockButtonPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "LockButtonView"
.end annotation


# virtual methods
.method public abstract asView()Landroid/view/View;
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract setDescription(Ljava/lang/String;)V
.end method

.method public abstract showButton()V
.end method

.method public abstract showClock()V
.end method

.method public abstract showLegacyGuest()V
.end method

.method public abstract showLegacyLoginOut(Ljava/lang/String;)V
.end method

.method public abstract showLoginOut(Ljava/lang/String;)V
.end method
