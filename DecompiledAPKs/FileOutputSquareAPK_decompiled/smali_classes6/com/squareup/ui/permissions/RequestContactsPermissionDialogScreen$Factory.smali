.class public Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen$Factory;
.super Ljava/lang/Object;
.source "RequestContactsPermissionDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 28
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;

    .line 29
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;->systemPermissionsPresenter()Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/ui/permissions/SystemPermissionDialog;

    sget-object v2, Lcom/squareup/systempermissions/SystemPermission;->CONTACTS:Lcom/squareup/systempermissions/SystemPermission;

    sget v3, Lcom/squareup/utilities/R$string;->system_permission_contacts_body_invoices:I

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/squareup/ui/permissions/SystemPermissionDialog;-><init>(Landroid/content/Context;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;I)V

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
