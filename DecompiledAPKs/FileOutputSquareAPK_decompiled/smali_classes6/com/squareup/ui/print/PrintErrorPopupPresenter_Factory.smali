.class public final Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;
.super Ljava/lang/Object;
.source "PrintErrorPopupPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/print/PrintErrorPopupPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final printSpoolerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final routerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintTargetRouter;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintTargetRouter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->routerProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p10, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p11, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintTargetRouter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;)",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;"
        }
    .end annotation

    .line 78
    new-instance v12, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSpooler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/buyer/BuyerFlowStarter;)Lcom/squareup/ui/print/PrintErrorPopupPresenter;
    .locals 13

    .line 86
    new-instance v12, Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;-><init>(Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSpooler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/buyer/BuyerFlowStarter;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/print/PrintErrorPopupPresenter;
    .locals 12

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/print/PrintSpooler;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->routerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/print/PrintTargetRouter;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/util/ToastFactory;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->newInstance(Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSpooler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/buyer/BuyerFlowStarter;)Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter_Factory;->get()Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    move-result-object v0

    return-object v0
.end method
