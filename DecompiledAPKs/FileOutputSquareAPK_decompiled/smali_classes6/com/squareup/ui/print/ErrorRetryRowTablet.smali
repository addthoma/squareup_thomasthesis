.class public Lcom/squareup/ui/print/ErrorRetryRowTablet;
.super Landroid/view/ViewGroup;
.source "ErrorRetryRowTablet.java"


# instance fields
.field private final button:Landroid/widget/TextView;

.field private final contentContainer:Landroid/view/ViewGroup;

.field private final gapMedium:I

.field private final reprint:Ljava/lang/String;

.field private final spinner:Landroid/view/View;

.field private final tryAgain:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 43
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 44
    sget v0, Lcom/squareup/print/popup/error/impl/R$layout;->error_retry_row_tablet:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/print/ErrorRetryRowTablet;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 45
    sget p1, Lcom/squareup/print/popup/error/impl/R$id;->error_retry_row_spinner:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->spinner:Landroid/view/View;

    .line 46
    sget p1, Lcom/squareup/print/popup/error/impl/R$id;->error_retry_row_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    .line 47
    sget p1, Lcom/squareup/print/popup/error/impl/R$id;->error_retry_row_text_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->contentContainer:Landroid/view/ViewGroup;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/print/ErrorRetryRowTablet;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->gapMedium:I

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->reprint:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->tryAgain:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 2

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/print/ErrorRetryRowTablet;->getMeasuredWidth()I

    move-result p1

    .line 80
    iget-object p2, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result p2

    .line 81
    iget-object p3, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    invoke-virtual {p3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p3

    sub-int p2, p1, p2

    .line 84
    iget-object p4, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->contentContainer:Landroid/view/ViewGroup;

    iget p5, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->gapMedium:I

    sub-int p5, p2, p5

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p4, v1, v1, p5, v0}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 85
    iget-object p4, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    invoke-virtual {p4, p2, v1, p1, p3}, Landroid/widget/TextView;->layout(IIII)V

    .line 86
    iget-object p4, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->spinner:Landroid/view/View;

    invoke-virtual {p4, p2, v1, p1, p3}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 54
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 56
    iget-object p2, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object p2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->reprint:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->tryAgain:Ljava/lang/String;

    invoke-virtual {p2, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result p2

    invoke-static {v0, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    .line 58
    div-int/lit8 v0, p1, 0x3

    int-to-float v0, v0

    .line 59
    iget-object v1, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    .line 60
    invoke-static {v0, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    add-float/2addr v1, p2

    float-to-int p2, v1

    const/high16 v0, 0x40000000    # 2.0f

    .line 62
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/4 v2, 0x0

    .line 63
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 64
    iget-object v4, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    invoke-virtual {v4, v1, v3}, Landroid/widget/TextView;->measure(II)V

    .line 65
    iget-object v4, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->spinner:Landroid/view/View;

    invoke-virtual {v4, v1, v3}, Landroid/view/View;->measure(II)V

    .line 67
    iget v1, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->gapMedium:I

    sub-int v1, p1, v1

    sub-int/2addr v1, p2

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 68
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 69
    iget-object v1, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->contentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, p2, v0}, Landroid/view/ViewGroup;->measure(II)V

    .line 71
    iget-object p2, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->contentContainer:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result p2

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/print/ErrorRetryRowTablet;->button:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 73
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 75
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/print/ErrorRetryRowTablet;->setMeasuredDimension(II)V

    return-void
.end method
