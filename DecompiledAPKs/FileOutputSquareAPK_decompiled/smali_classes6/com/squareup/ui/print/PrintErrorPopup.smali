.class public Lcom/squareup/ui/print/PrintErrorPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "PrintErrorPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/Showing;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private contentView:Lcom/squareup/ui/print/PrintErrorPopupView;

.field private final presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopup;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    return-void
.end method

.method private createContentView()Lcom/squareup/ui/print/PrintErrorPopupView;
    .locals 3

    .line 40
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/print/popup/error/impl/R$layout;->print_error_popup_view:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/print/PrintErrorPopupView;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/Showing;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/print/PrintErrorPopup;->createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Showing;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Ljava/lang/Void;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopup;->createContentView()Lcom/squareup/ui/print/PrintErrorPopupView;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopup;->contentView:Lcom/squareup/ui/print/PrintErrorPopupView;

    .line 25
    new-instance p1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/print/-$$Lambda$PrintErrorPopup$Ir-AebSBPTXsV5Efo-xtUYivN3A;

    invoke-direct {p2, p0}, Lcom/squareup/ui/print/-$$Lambda$PrintErrorPopup$Ir-AebSBPTXsV5Efo-xtUYivN3A;-><init>(Lcom/squareup/ui/print/PrintErrorPopup;)V

    .line 26
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->clearWindowBackground()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopup;->contentView:Lcom/squareup/ui/print/PrintErrorPopupView;

    .line 28
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 30
    iget-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopup;->contentView:Lcom/squareup/ui/print/PrintErrorPopupView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/print/PrintErrorPopupView;->dialogUseFullScreenWidth(Landroid/app/Dialog;)V

    const/4 p2, 0x0

    .line 31
    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p1
.end method

.method getContentView()Lcom/squareup/ui/print/PrintErrorPopupView;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopup;->contentView:Lcom/squareup/ui/print/PrintErrorPopupView;

    return-object v0
.end method

.method public synthetic lambda$createDialog$0$PrintErrorPopup(Landroid/content/DialogInterface;)V
    .locals 0

    .line 26
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopup;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->dismiss()V

    return-void
.end method
