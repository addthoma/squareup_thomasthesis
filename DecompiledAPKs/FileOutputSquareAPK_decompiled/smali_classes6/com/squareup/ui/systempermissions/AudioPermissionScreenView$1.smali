.class Lcom/squareup/ui/systempermissions/AudioPermissionScreenView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "AudioPermissionScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->setupLayout(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;

    iget-object p1, p1, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->presenter:Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->askForMicPermission()V

    return-void
.end method
