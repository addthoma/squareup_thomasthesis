.class public Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;
.super Lmortar/ViewPresenter;
.source "EnableDeviceSettingsPresenter.java"

# interfaces
.implements Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;
.implements Lcom/squareup/pauses/PausesAndResumes;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;",
        ">;",
        "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;",
        "Lcom/squareup/pauses/PausesAndResumes;"
    }
.end annotation


# static fields
.field private static final REQUEST_CODE:I = 0x64


# instance fields
.field private final flow:Lflow/Flow;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private final permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/pauses/PauseAndResumeRegistrar;Lflow/Flow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->flow:Lflow/Flow;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method private hasAllCorePermissions()Z
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    sget-object v1, Lcom/squareup/ui/main/MainActivityPrimaryPermissions;->PRIMARY_SYSTEM_PERMISSIONS:[Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->hasAll([Lcom/squareup/systempermissions/SystemPermission;)Z

    move-result v0

    return v0
.end method

.method private permissionGranted(Lcom/squareup/systempermissions/SystemPermission;)V
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->markPermissionGranted(Lcom/squareup/systempermissions/SystemPermission;)V

    return-void
.end method

.method private updatePermissionsState()V
    .locals 1

    .line 124
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->checkPermission(Lcom/squareup/systempermissions/SystemPermission;)V

    .line 125
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->STORAGE:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->checkPermission(Lcom/squareup/systempermissions/SystemPermission;)V

    .line 126
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->PHONE:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->checkPermission(Lcom/squareup/systempermissions/SystemPermission;)V

    .line 127
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->hasAllCorePermissions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    :cond_0
    return-void
.end method


# virtual methods
.method checkPermission(Lcom/squareup/systempermissions/SystemPermission;)V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->GRANTED:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-direct {p0, p1}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionGranted(Lcom/squareup/systempermissions/SystemPermission;)V

    :cond_0
    return-void
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onDenied(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 42
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v0, p1, p0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    .line 44
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->addPermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->removePermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    .line 49
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method public onGranted(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 62
    sget-object p1, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    if-eq p2, p1, :cond_0

    sget-object p1, Lcom/squareup/systempermissions/SystemPermission;->STORAGE:Lcom/squareup/systempermissions/SystemPermission;

    if-eq p2, p1, :cond_0

    sget-object p1, Lcom/squareup/systempermissions/SystemPermission;->PHONE:Lcom/squareup/systempermissions/SystemPermission;

    if-ne p2, p1, :cond_1

    .line 63
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionGranted(Lcom/squareup/systempermissions/SystemPermission;)V

    :cond_1
    return-void
.end method

.method onLearnMoreClicked()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;

    invoke-direct {v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;-><init>()V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 53
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->updatePermissionsState()V

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method onRequestPermission(Lcom/squareup/systempermissions/SystemPermission;)V
    .locals 3

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object v0

    .line 85
    sget-object v1, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter$1;->$SwitchMap$com$squareup$ui$systempermissions$SystemPermissionsPresenter$Status:[I

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionGranted(Lcom/squareup/systempermissions/SystemPermission;)V

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->hasAllCorePermissions()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    goto :goto_0

    .line 101
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown permission status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->requestPermission(ILcom/squareup/systempermissions/SystemPermission;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 73
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->updatePermissionsState()V

    return-void
.end method
