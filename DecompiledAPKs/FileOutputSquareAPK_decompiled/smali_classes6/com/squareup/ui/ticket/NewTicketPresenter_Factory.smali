.class public final Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;
.super Ljava/lang/Object;
.source "NewTicketPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/NewTicketPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final availableTemplateCountCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/AvailableTemplateCountCache;",
            ">;"
        }
    .end annotation
.end field

.field private final editTicketControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final predefinedTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/AvailableTemplateCountCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->editTicketControllerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->availableTemplateCountCacheProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/AvailableTemplateCountCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Ljava/lang/Object;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/util/Res;Lcom/squareup/opentickets/AvailableTemplateCountCache;Lflow/Flow;)Lcom/squareup/ui/ticket/NewTicketPresenter;
    .locals 7

    .line 58
    new-instance v6, Lcom/squareup/ui/ticket/NewTicketPresenter;

    move-object v1, p0

    check-cast v1, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    move-object v0, v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/ticket/NewTicketPresenter;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/util/Res;Lcom/squareup/opentickets/AvailableTemplateCountCache;Lflow/Flow;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/NewTicketPresenter;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->editTicketControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/opentickets/PredefinedTickets;

    iget-object v2, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->availableTemplateCountCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/opentickets/AvailableTemplateCountCache;

    iget-object v4, p0, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflow/Flow;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->newInstance(Ljava/lang/Object;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/util/Res;Lcom/squareup/opentickets/AvailableTemplateCountCache;Lflow/Flow;)Lcom/squareup/ui/ticket/NewTicketPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter_Factory;->get()Lcom/squareup/ui/ticket/NewTicketPresenter;

    move-result-object v0

    return-object v0
.end method
