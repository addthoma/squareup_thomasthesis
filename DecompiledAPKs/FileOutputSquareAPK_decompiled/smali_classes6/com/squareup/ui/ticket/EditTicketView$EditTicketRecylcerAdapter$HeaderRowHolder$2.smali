.class Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;

.field final synthetic val$this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;->val$this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onTextChanged$0$EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2(Ljava/lang/CharSequence;)V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;

    iget-object v0, v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v0}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/EditTicketState;->setNote(Ljava/lang/String;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditTicketView;->onTicketNoteChanged()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 141
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$000(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/SafeRecyclerAdapterUpdater;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;

    iget-object p3, p3, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p3, p3, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    new-instance p4, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2$Kw--waHbGNV3UHP0oA7gHVbFM4k;

    invoke-direct {p4, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2$Kw--waHbGNV3UHP0oA7gHVbFM4k;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;Ljava/lang/CharSequence;)V

    invoke-virtual {p2, p3, p4}, Lcom/squareup/ui/SafeRecyclerAdapterUpdater;->doSafely(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method
