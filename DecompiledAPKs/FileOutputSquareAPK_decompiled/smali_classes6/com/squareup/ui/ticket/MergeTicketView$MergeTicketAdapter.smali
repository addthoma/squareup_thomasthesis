.class Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;
.super Lcom/squareup/ui/RangerRecyclerAdapter;
.source "MergeTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeTicketView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MergeTicketAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/MergeTicketView$RowType;",
        "Lcom/squareup/ui/ticket/MergeTicketView$HolderType;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

.field private ticketsInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;)V
    .locals 1

    .line 92
    const-class v0, Lcom/squareup/ui/ticket/MergeTicketView$HolderType;

    invoke-direct {p0, v0}, Lcom/squareup/ui/RangerRecyclerAdapter;-><init>(Ljava/lang/Class;)V

    .line 93
    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    .line 94
    new-instance p1, Lcom/squareup/ui/Ranger$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/Ranger$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->setRanger(Lcom/squareup/ui/Ranger;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;)Ljava/util/List;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->ticketsInfo:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;)Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    return-object p0
.end method

.method private buildRanger()V
    .locals 3

    .line 112
    new-instance v0, Lcom/squareup/ui/Ranger$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/Ranger$Builder;-><init>()V

    .line 113
    sget-object v1, Lcom/squareup/ui/ticket/MergeTicketView$RowType;->TICKET:Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->ticketsInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 114
    invoke-virtual {v0}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->setRanger(Lcom/squareup/ui/Ranger;)V

    return-void
.end method


# virtual methods
.method public onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/MergeTicketView$HolderType;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/ui/ticket/MergeTicketView$HolderType;",
            ")",
            "Lcom/squareup/ui/RangerRecyclerAdapter<",
            "Lcom/squareup/ui/ticket/MergeTicketView$RowType;",
            "Lcom/squareup/ui/ticket/MergeTicketView$HolderType;",
            ">.RangerHolder;"
        }
    .end annotation

    .line 98
    sget-object v0, Lcom/squareup/ui/ticket/MergeTicketView$1;->$SwitchMap$com$squareup$ui$ticket$MergeTicketView$HolderType:[I

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/MergeTicketView$HolderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 100
    new-instance p2, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;-><init>(Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 102
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal holder type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;Ljava/lang/Enum;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 0

    .line 56
    check-cast p2, Lcom/squareup/ui/ticket/MergeTicketView$HolderType;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/MergeTicketView$HolderType;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;

    move-result-object p1

    return-object p1
.end method

.method public setList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    goto :goto_0

    .line 107
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->ticketsInfo:Ljava/util/List;

    .line 108
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->buildRanger()V

    return-void
.end method
