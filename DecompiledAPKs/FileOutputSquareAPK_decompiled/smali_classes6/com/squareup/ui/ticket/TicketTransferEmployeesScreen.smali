.class public final Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;
.super Lcom/squareup/ui/ticket/InTicketActionScope;
.source "TicketTransferEmployeesScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/ticket/TicketScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Component;,
        Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;,
        Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final forTransactionTicket:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 315
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketTransferEmployeesScreen$TaDVtwuuTayo7MP5et5GT_-p1go;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketTransferEmployeesScreen$TaDVtwuuTayo7MP5et5GT_-p1go;

    .line 316
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketActionScope;-><init>()V

    .line 70
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;->forTransactionTicket:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;)Z
    .locals 0

    .line 54
    iget-boolean p0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;->forTransactionTicket:Z

    return p0
.end method

.method static forSelection()Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;
    .locals 2

    .line 59
    new-instance v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;-><init>(Z)V

    return-object v0
.end method

.method public static forTransaction()Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;
    .locals 2

    .line 64
    new-instance v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;-><init>(Z)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;
    .locals 1

    .line 317
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 318
    :goto_0
    new-instance p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;-><init>(Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 311
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketActionScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 312
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;->forTransactionTicket:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 82
    invoke-super {p0}, Lcom/squareup/ui/ticket/InTicketActionScope;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;->forTransactionTicket:Z

    .line 83
    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "%s-{forTransactionTicket=%s}"

    .line 82
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 78
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 74
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_transfer_employees_view:I

    return v0
.end method
