.class public Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Module;
.super Ljava/lang/Object;
.source "MasterDetailTicketScreen.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/ticket/GroupListPresenter$Module;,
        Lcom/squareup/ui/ticket/TicketListPresenter$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MasterDetailTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/MasterDetailTicketScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MasterDetailTicketScreen;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Module;->this$0:Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideDisplayMode()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 63
    sget-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;->MASTER_DETAIL:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    return-object v0
.end method

.method provideTicketMode()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Module;->this$0:Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    invoke-static {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->access$000(Lcom/squareup/ui/ticket/MasterDetailTicketScreen;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    move-result-object v0

    return-object v0
.end method
