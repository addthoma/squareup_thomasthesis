.class public Lcom/squareup/ui/ticket/TicketSelection;
.super Ljava/lang/Object;
.source "TicketSelection.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# instance fields
.field private final selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final selectedTicketsInfoKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/BundleKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfoKey:Lcom/squareup/BundleKey;

    .line 34
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method static synthetic lambda$onSelectedTicketsChanged$0(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 60
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private removeSelectedTickets(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;)V"
        }
    .end annotation

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 134
    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public static ticketInfoFromRow(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;)Lcom/squareup/ui/ticket/TicketInfo;
    .locals 4

    .line 147
    new-instance v0, Lcom/squareup/ui/ticket/TicketInfo;

    .line 148
    invoke-interface {p0}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getId()Ljava/lang/String;

    move-result-object v1

    .line 149
    invoke-interface {p0}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getName()Ljava/lang/String;

    move-result-object v2

    .line 150
    invoke-interface {p0}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getOpenTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {p0}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getOpenTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getNote()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/squareup/ui/ticket/TicketInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static ticketInfoFromTemplate(Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)Lcom/squareup/ui/ticket/TicketInfo;
    .locals 4

    .line 139
    new-instance v0, Lcom/squareup/ui/ticket/TicketInfo;

    .line 140
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getId()Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getName()Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/ui/ticket/TicketInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static ticketInfoFromTransaction(Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/ticket/TicketInfo;
    .locals 4

    .line 155
    new-instance v0, Lcom/squareup/ui/ticket/TicketInfo;

    .line 156
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTicketId()Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOpenTicketNote()Ljava/lang/String;

    move-result-object p0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/squareup/ui/ticket/TicketInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public addSelectedTicket(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 2

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 101
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public clearSelectedTickets()V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedCount()I
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedTicketsInfo()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public hasSelectedTickets()Z
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfoKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfoKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method onSelectedTicketsChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketSelection$-HuedC-fq4ne4oplHA7e_xS_pP0;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketSelection$-HuedC-fq4ne4oplHA7e_xS_pP0;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public pruneSelectedTicketsAgainst(Lcom/squareup/tickets/TicketRowCursorList;)V
    .locals 4

    .line 107
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v0

    .line 108
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 112
    :cond_0
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 113
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 114
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 115
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    invoke-interface {v2}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 118
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 119
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/ticket/TicketInfo;

    .line 120
    iget-boolean v3, v2, Lcom/squareup/ui/ticket/TicketInfo;->isTemplate:Z

    if-nez v3, :cond_2

    iget-object v3, v2, Lcom/squareup/ui/ticket/TicketInfo;->id:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 121
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 125
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketSelection;->removeSelectedTickets(Ljava/util/List;)V

    return-void
.end method

.method public toggleSelected(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 2

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 81
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public toggleUniqueSelected(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 2

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 95
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSelection;->selectedTicketsInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
