.class Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;
.super Ljava/lang/Object;
.source "GroupListPresenter.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GroupQueryCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        ">;"
    }
.end annotation


# instance fields
.field private canceled:Z

.field final synthetic this$0:Lcom/squareup/ui/ticket/GroupListPresenter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/ticket/GroupListPresenter;)V
    .locals 0

    .line 274
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;->this$0:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 276
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;->canceled:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/ticket/GroupListPresenter;Lcom/squareup/ui/ticket/GroupListPresenter$1;)V
    .locals 0

    .line 274
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ">;)V"
        }
    .end annotation

    .line 283
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_1

    .line 284
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;->canceled:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;->this$0:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->access$200(Lcom/squareup/ui/ticket/GroupListPresenter;)Lcom/squareup/tickets/TicketGroupsCache;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/tickets/TicketGroupsCache;->update(Lcom/squareup/shared/catalog/CatalogResult;)V

    .line 288
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;->this$0:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/GroupListPresenter;->access$300(Lcom/squareup/ui/ticket/GroupListPresenter;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 279
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;->canceled:Z

    return-void
.end method
