.class public abstract Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;
.super Ljava/lang/Object;
.source "TicketListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TicketsRefreshedCallback"
.end annotation


# instance fields
.field protected canceled:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 1

    .line 830
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;->canceled:Z

    if-nez v0, :cond_0

    .line 831
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;->onCompleted()V

    goto :goto_0

    .line 833
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;->onCanceled()V

    :goto_0
    return-void
.end method

.method public final cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 838
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;->canceled:Z

    return-void
.end method

.method protected onCanceled()V
    .locals 0

    return-void
.end method

.method protected abstract onCompleted()V
.end method
