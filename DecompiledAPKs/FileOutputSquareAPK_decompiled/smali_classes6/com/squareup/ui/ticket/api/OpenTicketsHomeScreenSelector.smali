.class public interface abstract Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;
.super Ljava/lang/Object;
.source "OpenTicketsHomeScreenSelector.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\u0005H&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
        "",
        "isOpenTicketsHomeScreen",
        "",
        "screen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "leavingSplitTicketScreen",
        "traversal",
        "Lflow/Traversal;",
        "updateHomeForOpenTickets",
        "Lflow/History;",
        "currentHistory",
        "openTicketsParentScreen",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z
.end method

.method public abstract leavingSplitTicketScreen(Lflow/Traversal;)Z
.end method

.method public abstract updateHomeForOpenTickets(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lflow/History;
.end method
