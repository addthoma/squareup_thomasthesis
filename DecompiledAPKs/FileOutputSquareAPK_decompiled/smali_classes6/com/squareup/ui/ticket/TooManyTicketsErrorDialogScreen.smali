.class public Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;
.super Lcom/squareup/ui/ticket/InTicketActionScope;
.source "TooManyTicketsErrorDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final limit:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$TooManyTicketsErrorDialogScreen$Fn7q2N-N1e-MIFXiBRXNiaj4HYg;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TooManyTicketsErrorDialogScreen$Fn7q2N-N1e-MIFXiBRXNiaj4HYg;

    .line 62
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketActionScope;-><init>()V

    .line 53
    iput p1, p0, Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;->limit:I

    return-void
.end method

.method public static forLimit(I)Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;-><init>(I)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;
    .locals 1

    .line 63
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    .line 64
    new-instance v0, Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 57
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketActionScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 58
    iget p2, p0, Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;->limit:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
