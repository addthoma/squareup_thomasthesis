.class public Lcom/squareup/ui/ticket/MergeTicketView;
.super Landroid/widget/LinearLayout;
.source "MergeTicketView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;,
        Lcom/squareup/ui/ticket/MergeTicketView$HolderType;,
        Lcom/squareup/ui/ticket/MergeTicketView$RowType;
    }
.end annotation


# instance fields
.field private final blockingPopup:Lcom/squareup/caller/BlockingPopup;

.field private final mergeInFlightPopup:Lcom/squareup/caller/ProgressPopup;

.field presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerAdapter:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 127
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 128
    new-instance p2, Lcom/squareup/caller/ProgressPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/MergeTicketView;->mergeInFlightPopup:Lcom/squareup/caller/ProgressPopup;

    .line 129
    new-instance p2, Lcom/squareup/caller/BlockingPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/BlockingPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/MergeTicketView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    .line 130
    const-class p2, Lcom/squareup/ui/ticket/MergeTicketScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MergeTicketScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$Component;->inject(Lcom/squareup/ui/ticket/MergeTicketView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 172
    sget v0, Lcom/squareup/orderentry/R$id;->merge_ticket_view_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 152
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 134
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 135
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MergeTicketView;->bindViews()V

    .line 136
    new-instance v0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketView;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    invoke-direct {v0, v1}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;-><init>(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;)V

    iput-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->recyclerAdapter:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketView;->recyclerAdapter:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketView;->mergeInFlightPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->dropView(Ljava/lang/Object;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketView;->mergeInFlightPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->presenter:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 148
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method refreshTicketList()V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->recyclerAdapter:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method setMergeButtonEnabled(Z)V
    .locals 1

    .line 160
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method setTicketList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;)V"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView;->recyclerAdapter:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->setList(Ljava/util/List;)V

    return-void
.end method
