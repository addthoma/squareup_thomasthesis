.class public final Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
.super Lcom/squareup/ui/ticket/InTicketActionScope;
.source "MergeCanceledDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Factory;,
        Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;,
        Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final action:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

.field private final toMoveCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 122
    new-instance v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$1;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;I)V
    .locals 0

    .line 66
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketActionScope;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->action:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    .line 68
    iput p2, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->toMoveCount:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;ILcom/squareup/ui/ticket/MergeCanceledDialogScreen$1;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;-><init>(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;I)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->action:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->toMoveCount:I

    return p0
.end method

.method public static forBulkMerge(I)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    sget-object v1, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->BULK_MERGE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;-><init>(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;I)V

    return-object v0
.end method

.method public static forMergeWithTransactionticket(I)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
    .locals 2

    .line 76
    new-instance v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    sget-object v1, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;-><init>(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;I)V

    return-object v0
.end method

.method public static forMove(I)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    sget-object v1, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->MOVE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;-><init>(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;I)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 117
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketActionScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 118
    iget-object p2, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->action:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    iget p2, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->toMoveCount:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
