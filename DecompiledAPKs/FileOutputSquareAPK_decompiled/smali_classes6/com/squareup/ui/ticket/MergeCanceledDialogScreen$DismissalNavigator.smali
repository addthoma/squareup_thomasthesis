.class Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;
.super Ljava/lang/Object;
.source "MergeCanceledDialogScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DismissalNavigator"
.end annotation


# instance fields
.field private final action:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;->flow:Lflow/Flow;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;->action:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .line 47
    sget-object p1, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$2;->$SwitchMap$com$squareup$ui$ticket$MergeCanceledDialogScreen$Action:[I

    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;->action:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->ordinal()I

    move-result v0

    aget p1, p1, v0

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v2, 0x2

    if-eq p1, v2, :cond_1

    const/4 v3, 0x3

    if-ne p1, v3, :cond_0

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;->flow:Lflow/Flow;

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/ticket/MoveTicketScreen;

    aput-object v3, v2, v0

    const-class v0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    aput-object v0, v2, v1

    invoke-static {p1, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 58
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;->action:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 52
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;->flow:Lflow/Flow;

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/ticket/MergeTicketScreen;

    aput-object v3, v2, v0

    const-class v0, Lcom/squareup/ui/ticket/TicketListScreen;

    aput-object v0, v2, v1

    invoke-static {p1, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 49
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;->flow:Lflow/Flow;

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/ticket/MergeTicketScreen;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method
