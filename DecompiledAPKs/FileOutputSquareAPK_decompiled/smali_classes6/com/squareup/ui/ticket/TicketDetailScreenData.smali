.class Lcom/squareup/ui/ticket/TicketDetailScreenData;
.super Ljava/lang/Object;
.source "TicketDetailScreenData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/TicketDetailScreenData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final group:Lcom/squareup/api/items/TicketGroup;

.field public final name:Ljava/lang/String;

.field public final note:Ljava/lang/String;

.field public final template:Lcom/squareup/api/items/TicketTemplate;

.field public final ticketId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/ui/ticket/TicketDetailScreenData$1;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/TicketDetailScreenData$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->ticketId:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->name:Ljava/lang/String;

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->note:Ljava/lang/String;

    .line 32
    const-class v0, Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->group:Lcom/squareup/api/items/TicketGroup;

    .line 33
    const-class v0, Lcom/squareup/api/items/TicketTemplate;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/TicketTemplate;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->template:Lcom/squareup/api/items/TicketTemplate;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/ui/ticket/TicketDetailScreenData$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketDetailScreenData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/tickets/OpenTicket;)V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->ticketId:Ljava/lang/String;

    .line 22
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->name:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getNote()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->note:Ljava/lang/String;

    .line 24
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->group:Lcom/squareup/api/items/TicketGroup;

    .line 25
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getTemplate()Lcom/squareup/api/items/TicketTemplate;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->template:Lcom/squareup/api/items/TicketTemplate;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 60
    instance-of v0, p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 64
    :cond_0
    check-cast p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->ticketId:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;->ticketId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;->name:Ljava/lang/String;

    .line 66
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->note:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;->note:Ljava/lang/String;

    .line 67
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->group:Lcom/squareup/api/items/TicketGroup;

    iget-object v2, p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;->group:Lcom/squareup/api/items/TicketGroup;

    .line 68
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/TicketGroup;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->template:Lcom/squareup/api/items/TicketTemplate;

    iget-object p1, p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;->template:Lcom/squareup/api/items/TicketTemplate;

    .line 69
    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TicketTemplate;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 73
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->ticketId:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->name:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->note:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->group:Lcom/squareup/api/items/TicketGroup;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->template:Lcom/squareup/api/items/TicketTemplate;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 41
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->ticketId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->note:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->group:Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 45
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->template:Lcom/squareup/api/items/TicketTemplate;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    return-void
.end method
