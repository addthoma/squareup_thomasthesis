.class public final Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;
.super Ljava/lang/Object;
.source "TicketActionScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final inEditModeKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final moveTicketListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final voidControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->ticketsLoaderProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->ticketSelectionProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->ticketsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->moveTicketListenerProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->voidControllerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p10, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->inEditModeKeyProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p12, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->ticketStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;"
        }
    .end annotation

    .line 87
    new-instance v13, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/ui/ticket/TicketActionScopeRunner;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/tickets/TicketStore;",
            ")",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;"
        }
    .end annotation

    .line 97
    new-instance v13, Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;-><init>(Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketActionScopeRunner;
    .locals 13

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->ticketsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/ticket/TicketsLoader;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->ticketSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/ticket/TicketSelection;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tickets/Tickets;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->moveTicketListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->voidControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/compvoidcontroller/VoidController;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->inEditModeKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->ticketStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/tickets/TicketStore;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->newInstance(Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner_Factory;->get()Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-result-object v0

    return-object v0
.end method
