.class public final Lcom/squareup/ui/ticket/TicketBulkVoidScreen;
.super Lcom/squareup/ui/ticket/InTicketActionScope;
.source "TicketBulkVoidScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/ticket/TicketScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/TicketBulkVoidScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketBulkVoidScreen$Component;,
        Lcom/squareup/ui/ticket/TicketBulkVoidScreen$Module;,
        Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/TicketBulkVoidScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final authorizedEmployeeToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 206
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$tu44dfZX7qwNn4hFTNIGjquIPmg;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$tu44dfZX7qwNn4hFTNIGjquIPmg;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketActionScope;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;->authorizedEmployeeToken:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketBulkVoidScreen;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;->authorizedEmployeeToken:Ljava/lang/String;

    return-object p0
.end method

.method static forAuthorizedEmployee(Ljava/lang/String;)Lcom/squareup/ui/ticket/TicketBulkVoidScreen;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/TicketBulkVoidScreen;
    .locals 1

    .line 207
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 208
    new-instance v0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 202
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketActionScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 203
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;->authorizedEmployeeToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 59
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 55
    sget v0, Lcom/squareup/configure/item/R$layout;->void_comp_view:I

    return v0
.end method
