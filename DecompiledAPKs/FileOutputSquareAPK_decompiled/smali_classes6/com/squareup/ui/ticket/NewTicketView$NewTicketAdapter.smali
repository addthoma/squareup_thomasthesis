.class Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;
.super Lcom/squareup/ui/RangerRecyclerAdapter;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NewTicketAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;,
        Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;,
        Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;,
        Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoResultsViewHolder;,
        Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$HeaderViewHolder;,
        Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;,
        Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/NewTicketView$RowType;",
        "Lcom/squareup/ui/ticket/NewTicketView$HolderType;",
        ">;"
    }
.end annotation


# instance fields
.field private isSearching:Z

.field private final mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

.field private showCustomTicketButton:Z

.field private showTicketGroups:Z

.field final synthetic this$0:Lcom/squareup/ui/ticket/NewTicketView;

.field private ticketGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private ticketTemplates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/NewTicketView;Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)V
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->this$0:Lcom/squareup/ui/ticket/NewTicketView;

    .line 238
    const-class p1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/RangerRecyclerAdapter;-><init>(Ljava/lang/Class;)V

    .line 231
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketGroups:Ljava/util/List;

    .line 232
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketTemplates:Ljava/util/List;

    .line 239
    iput-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 240
    new-instance p1, Lcom/squareup/ui/Ranger$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/Ranger$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->setRanger(Lcom/squareup/ui/Ranger;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;)Ljava/util/List;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketGroups:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;)Ljava/util/List;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketTemplates:Ljava/util/List;

    return-object p0
.end method

.method private resetRanger()V
    .locals 3

    .line 282
    new-instance v0, Lcom/squareup/ui/Ranger$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/Ranger$Builder;-><init>()V

    .line 283
    iget-boolean v1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->showCustomTicketButton:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->isSearching:Z

    if-nez v1, :cond_0

    .line 284
    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->CUSTOM_TICKET_BUTTON_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 286
    :cond_0
    iget-boolean v1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->showTicketGroups:Z

    if-eqz v1, :cond_2

    .line 287
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketGroups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 288
    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->NO_TICKETS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    goto :goto_1

    .line 290
    :cond_1
    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->SELECT_TICKET_GROUP_HEADER_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 291
    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    iget-object v2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketGroups:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    goto :goto_1

    .line 294
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 295
    iget-boolean v1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->isSearching:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->NO_RESULTS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->NO_TICKETS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    goto :goto_1

    .line 297
    :cond_4
    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->TICKET_TEMPLATE_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    iget-object v2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 300
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->setRanger(Lcom/squareup/ui/Ranger;)V

    return-void
.end method


# virtual methods
.method public onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/NewTicketView$HolderType;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/ui/ticket/NewTicketView$HolderType;",
            ")",
            "Lcom/squareup/ui/RangerRecyclerAdapter<",
            "Lcom/squareup/ui/ticket/NewTicketView$RowType;",
            "Lcom/squareup/ui/ticket/NewTicketView$HolderType;",
            ">.RangerHolder;"
        }
    .end annotation

    .line 244
    sget-object v0, Lcom/squareup/ui/ticket/NewTicketView$2;->$SwitchMap$com$squareup$ui$ticket$NewTicketView$HolderType:[I

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    .line 258
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal holder type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 256
    :pswitch_0
    new-instance p2, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;

    invoke-direct {p2, p0, p1, v1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/NewTicketView$1;)V

    return-object p2

    .line 254
    :pswitch_1
    new-instance p2, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;

    invoke-direct {p2, p0, p1, v1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/NewTicketView$1;)V

    return-object p2

    .line 252
    :pswitch_2
    new-instance p2, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;

    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    invoke-direct {p2, p0, p1, v0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)V

    return-object p2

    .line 250
    :pswitch_3
    new-instance p2, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoResultsViewHolder;

    invoke-direct {p2, p0, p1, v1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoResultsViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/NewTicketView$1;)V

    return-object p2

    .line 248
    :pswitch_4
    new-instance p2, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$HeaderViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$HeaderViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 246
    :pswitch_5
    new-instance p2, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;Ljava/lang/Enum;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 0

    .line 92
    check-cast p2, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/NewTicketView$HolderType;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;

    move-result-object p1

    return-object p1
.end method

.method setCustomTicketButtonRowVisible(Z)V
    .locals 0

    .line 263
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->showCustomTicketButton:Z

    .line 264
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->resetRanger()V

    return-void
.end method

.method setTicketGroups(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;)V"
        }
    .end annotation

    .line 268
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketGroups:Ljava/util/List;

    const/4 p1, 0x1

    .line 269
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->showTicketGroups:Z

    .line 270
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->resetRanger()V

    return-void
.end method

.method setTicketTemplates(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;Z)V"
        }
    .end annotation

    .line 275
    iput-boolean p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->isSearching:Z

    .line 276
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->ticketTemplates:Ljava/util/List;

    const/4 p1, 0x0

    .line 277
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->showTicketGroups:Z

    .line 278
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->resetRanger()V

    return-void
.end method
