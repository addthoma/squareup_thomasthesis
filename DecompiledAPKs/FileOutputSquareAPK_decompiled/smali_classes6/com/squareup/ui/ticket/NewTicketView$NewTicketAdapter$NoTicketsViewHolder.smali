.class Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;
.super Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NoTicketsViewHolder"
.end annotation


# instance fields
.field private final mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

.field final synthetic this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)V
    .locals 1

    .line 134
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    .line 135
    sget v0, Lcom/squareup/orderentry/R$layout;->new_ticket_no_predefined_tickets:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;I)V

    .line 136
    iput-object p3, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/NewTicketView$RowType;II)V
    .locals 3

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez p3, :cond_0

    const/4 p2, -0x1

    goto :goto_0

    :cond_0
    const/4 p2, -0x2

    .line 142
    :goto_0
    iput p2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 145
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->this$0:Lcom/squareup/ui/ticket/NewTicketView;

    iget-object p1, p1, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getSelectedTicketGroupName()Ljava/lang/String;

    move-result-object p1

    .line 148
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->itemView:Landroid/view/View;

    sget p3, Lcom/squareup/orderentry/R$id;->new_ticket_no_predefined_tickets_title:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 149
    iget-object p3, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->new_ticket_no_predefined_tickets_message:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/widgets/MessageView;

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x4

    .line 152
    invoke-virtual {p3, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :cond_1
    if-nez p1, :cond_2

    .line 156
    sget p1, Lcom/squareup/orderentry/R$string;->predefined_tickets_no_predefined_tickets_title:I

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    .line 157
    sget p1, Lcom/squareup/orderentry/R$string;->predefined_tickets_no_predefined_tickets_message:I

    invoke-virtual {p3, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->this$0:Lcom/squareup/ui/ticket/NewTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/NewTicketView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 162
    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_no_group_tickets_available_title:I

    .line 163
    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "group"

    .line 164
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 165
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 166
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    sget p2, Lcom/squareup/orderentry/R$string;->predefined_tickets_no_group_tickets_message:I

    .line 168
    invoke-static {v0, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 169
    invoke-virtual {p2, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 170
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 171
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 167
    invoke-virtual {p3, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 130
    check-cast p1, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoTicketsViewHolder;->bindRow(Lcom/squareup/ui/ticket/NewTicketView$RowType;II)V

    return-void
.end method
