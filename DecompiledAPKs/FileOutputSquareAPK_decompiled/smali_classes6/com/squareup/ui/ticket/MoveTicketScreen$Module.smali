.class public Lcom/squareup/ui/ticket/MoveTicketScreen$Module;
.super Ljava/lang/Object;
.source "MoveTicketScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MoveTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/MoveTicketScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->this$0:Lcom/squareup/ui/ticket/MoveTicketScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideConfiguration(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Ljava/util/List;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Device;",
            ")",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;"
        }
    .end annotation

    .line 118
    new-instance v7, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Ljava/util/List;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)V

    return-object v7
.end method

.method provideDisplayMode()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 88
    sget-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;->DETAIL_ONLY:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    return-object v0
.end method

.method provideInEditModeBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 168
    const-class v0, Ljava/lang/Boolean;

    const-string v1, "move-ticket-screen-in-edit-mode"

    invoke-static {p1, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p1

    return-object p1
.end method

.method provideSelectedTicketsInfoBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation

    .line 179
    new-instance v0, Lcom/squareup/ui/ticket/MoveTicketScreen$Module$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module$1;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)V

    .line 181
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "move-ticket-screen-selected-tickets-info"

    .line 179
    invoke-static {p1, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;

    move-result-object p1

    return-object p1
.end method

.method provideTicketActionSession(Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/ui/ticket/TicketActionScopeRunner;
    .locals 14
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/tickets/TicketStore;",
            ")",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;"
        }
    .end annotation

    .line 128
    new-instance v13, Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-object v0, v13

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;-><init>(Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)V

    return-object v13
.end method

.method provideTicketListListener()Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 145
    new-instance v0, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;-><init>()V

    return-object v0
.end method

.method provideTicketListPresenter(Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/ui/ticket/TicketListPresenter;
    .locals 16
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/ui/ticket/TicketListPresenter;"
        }
    .end annotation

    .line 110
    new-instance v15, Lcom/squareup/ui/ticket/TicketListPresenter;

    move-object v0, v15

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/ticket/TicketListPresenter;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V

    return-object v15
.end method

.method provideTicketMode()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->this$0:Lcom/squareup/ui/ticket/MoveTicketScreen;

    invoke-static {v0}, Lcom/squareup/ui/ticket/MoveTicketScreen;->access$000(Lcom/squareup/ui/ticket/MoveTicketScreen;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    move-result-object v0

    return-object v0
.end method

.method provideTicketSelectionSession(Lcom/squareup/BundleKey;)Lcom/squareup/ui/ticket/TicketSelection;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;)",
            "Lcom/squareup/ui/ticket/TicketSelection;"
        }
    .end annotation

    .line 135
    new-instance v0, Lcom/squareup/ui/ticket/TicketSelection;

    invoke-direct {v0, p1}, Lcom/squareup/ui/ticket/TicketSelection;-><init>(Lcom/squareup/BundleKey;)V

    return-object v0
.end method

.method provideTicketSort(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;"
        }
    .end annotation

    .line 157
    new-instance v0, Lcom/squareup/settings/EnumLocalSetting;

    const-class v1, Lcom/squareup/tickets/TicketSort;

    const-string v2, "move-ticket-screen-sort-style"

    invoke-direct {v0, p1, v2, v1}, Lcom/squareup/settings/EnumLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method

.method provideTicketsLoader(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)Lcom/squareup/ui/ticket/TicketsLoader;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 141
    new-instance v0, Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/ticket/TicketsLoader;-><init>(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    return-object v0
.end method

.method provideTicketsToMove()Ljava/util/List;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->this$0:Lcom/squareup/ui/ticket/MoveTicketScreen;

    invoke-static {v0}, Lcom/squareup/ui/ticket/MoveTicketScreen;->access$100(Lcom/squareup/ui/ticket/MoveTicketScreen;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
