.class public final Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;
.super Ljava/lang/Object;
.source "TicketScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final permittedToViewAllTicketsKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final x2TicketRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/X2TicketRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/X2TicketRunner;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->permittedToViewAllTicketsKeyProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->x2TicketRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/X2TicketRunner;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/BundleKey;Lcom/squareup/ui/ticket/X2TicketRunner;)Lcom/squareup/ui/ticket/TicketScopeRunner;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/ticket/X2TicketRunner;",
            ")",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;"
        }
    .end annotation

    .line 59
    new-instance v6, Lcom/squareup/ui/ticket/TicketScopeRunner;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/ticket/TicketScopeRunner;-><init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/BundleKey;Lcom/squareup/ui/ticket/X2TicketRunner;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketScopeRunner;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->permittedToViewAllTicketsKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/BundleKey;

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->x2TicketRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/ticket/X2TicketRunner;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/BundleKey;Lcom/squareup/ui/ticket/X2TicketRunner;)Lcom/squareup/ui/ticket/TicketScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketScopeRunner_Factory;->get()Lcom/squareup/ui/ticket/TicketScopeRunner;

    move-result-object v0

    return-object v0
.end method
