.class final Lcom/squareup/ui/ticket/EditTicketState$1;
.super Ljava/lang/Object;
.source "EditTicketState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/ticket/EditTicketState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/EditTicketState;
    .locals 1

    .line 224
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketState;

    invoke-direct {v0, p1}, Lcom/squareup/ui/ticket/EditTicketState;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 222
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/EditTicketState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/ticket/EditTicketState;
    .locals 0

    .line 228
    new-array p1, p1, [Lcom/squareup/ui/ticket/EditTicketState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 222
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/EditTicketState$1;->newArray(I)[Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p1

    return-object p1
.end method
