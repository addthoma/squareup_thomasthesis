.class public Lcom/squareup/ui/ticket/SplitTicketView;
.super Landroid/widget/LinearLayout;
.source "SplitTicketView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# instance fields
.field private final dividerWidth:I

.field presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private scrollView:Lcom/squareup/ui/NullStateHorizontalScrollView;

.field private final ticketViews:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/ticket/TicketView;",
            ">;"
        }
    .end annotation
.end field

.field private ticketsContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    .line 34
    const-class p2, Lcom/squareup/ui/ticket/SplitTicketScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/SplitTicketScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/SplitTicketScreen$Component;->inject(Lcom/squareup/ui/ticket/SplitTicketView;)V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/ticket/SplitTicketView;->dividerWidth:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/SplitTicketView;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->dividerWidth:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/SplitTicketView;)Lcom/squareup/ui/NullStateHorizontalScrollView;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->scrollView:Lcom/squareup/ui/NullStateHorizontalScrollView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/SplitTicketView;)Ljava/util/Map;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 159
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_scroll_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/NullStateHorizontalScrollView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->scrollView:Lcom/squareup/ui/NullStateHorizontalScrollView;

    .line 160
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketsContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method static doScrollTicketIntoView(ILcom/squareup/ui/NullStateHorizontalScrollView;Ljava/util/Map;IIIII)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/ui/NullStateHorizontalScrollView;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/ticket/TicketView;",
            ">;IIIII)V"
        }
    .end annotation

    sub-int/2addr p4, p0

    add-int/2addr p5, p0

    .line 133
    invoke-virtual {p1}, Lcom/squareup/ui/NullStateHorizontalScrollView;->getWidth()I

    move-result p0

    .line 134
    invoke-virtual {p1}, Lcom/squareup/ui/NullStateHorizontalScrollView;->getScrollX()I

    move-result v0

    add-int v1, v0, p0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-le p5, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge p4, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v1, :cond_2

    sub-int p4, p5, p0

    goto :goto_2

    :cond_2
    if-eqz v0, :cond_5

    :goto_2
    if-nez p6, :cond_3

    if-nez p7, :cond_3

    .line 150
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result p0

    sub-int/2addr p0, v2

    if-ne p3, p0, :cond_3

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_4

    .line 152
    invoke-virtual {p1, p4, v3}, Lcom/squareup/ui/NullStateHorizontalScrollView;->scrollTo(II)V

    goto :goto_4

    .line 154
    :cond_4
    invoke-virtual {p1, p4, v3}, Lcom/squareup/ui/NullStateHorizontalScrollView;->smoothScrollTo(II)V

    :cond_5
    :goto_4
    return-void
.end method


# virtual methods
.method addSplitTicket(Ljava/lang/String;Ljava/lang/String;IZZZZZ)Lcom/squareup/ui/ticket/TicketView;
    .locals 10

    move-object v0, p0

    .line 65
    sget v1, Lcom/squareup/orderentry/R$layout;->ticket_view:I

    iget-object v2, v0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketsContainer:Landroid/widget/LinearLayout;

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/TicketView;

    move-object v2, v1

    move-object v3, p1

    move-object v4, p2

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    .line 66
    invoke-virtual/range {v2 .. v9}, Lcom/squareup/ui/ticket/TicketView;->initializeTicket(Ljava/lang/String;Ljava/lang/String;ZZZZZ)V

    .line 68
    iget-object v2, v0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketsContainer:Landroid/widget/LinearLayout;

    move v3, p3

    invoke-virtual {v2, v1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 69
    iget-object v2, v0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    move-object v3, p1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method

.method closeDropDown(Ljava/lang/String;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketView;->closeDropDown()V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 59
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 45
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->onCancelClicked()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->dropView(Ljava/lang/Object;)V

    .line 51
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketView;->bindViews()V

    return-void
.end method

.method refreshTicketView(Ljava/lang/String;Ljava/lang/String;ZZZZZ)V
    .locals 7

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/ticket/TicketView;

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/ui/ticket/TicketView;->updateTicket(Ljava/lang/String;ZZZZZ)V

    return-void
.end method

.method removeAllTicketViews()V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method removeTicket(Ljava/lang/String;)V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method scrollTicketIntoView(ILjava/lang/String;)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/ticket/TicketView;

    .line 99
    new-instance v0, Lcom/squareup/ui/ticket/SplitTicketView$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/ticket/SplitTicketView$1;-><init>(Lcom/squareup/ui/ticket/SplitTicketView;Lcom/squareup/ui/ticket/TicketView;I)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/ticket/TicketView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method showDismissButton(Ljava/lang/String;)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketView;->showDismissButton()V

    return-void
.end method

.method showMoveItemsButton(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketView;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/TicketView;->showMoveItemsButton(Ljava/lang/String;)V

    return-void
.end method

.method showNewTicketButton(Ljava/lang/String;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketView;->showNewTicketButton()V

    return-void
.end method

.method showSaveButton(Ljava/lang/String;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketView;->ticketViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketView;->showSaveButton()V

    return-void
.end method
