.class public final Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;
.super Ljava/lang/Object;
.source "TicketListPresenter_Module_ProvideConfigurationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final displayModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->displayModeProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->ticketModeProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->transactionProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideConfiguration(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Ljava/lang/Object;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/ui/ticket/TicketListPresenter$Module;->provideConfiguration(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->displayModeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->ticketModeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/Device;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->provideConfiguration(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Ljava/lang/Object;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideConfigurationFactory;->get()Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-result-object v0

    return-object v0
.end method
