.class Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager;
.super Landroidx/recyclerview/widget/LinearLayoutManager;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SmoothScrollLayoutManager"
.end annotation


# static fields
.field private static final MILLISECONDS_PER_INCH:F = 100.0f


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 304
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 305
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public smoothScrollToPosition(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;I)V
    .locals 0

    .line 311
    new-instance p1, Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager$1;

    iget-object p2, p0, Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager;->context:Landroid/content/Context;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager$1;-><init>(Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager;Landroid/content/Context;)V

    .line 316
    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/LinearSmoothScroller;->setTargetPosition(I)V

    .line 317
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager;->startSmoothScroll(Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;)V

    return-void
.end method
