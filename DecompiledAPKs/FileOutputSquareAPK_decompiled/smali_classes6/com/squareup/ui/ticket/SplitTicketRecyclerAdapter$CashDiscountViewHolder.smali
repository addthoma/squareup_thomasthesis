.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;
.super Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CashDiscountViewHolder"
.end annotation


# instance fields
.field private final cashDiscountView:Lcom/squareup/ui/cart/CartEntryView;

.field private final checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

.field private final discountRowView:Landroid/widget/LinearLayout;

.field final synthetic this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 233
    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 234
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_item_row:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->discountRowView:Landroid/widget/LinearLayout;

    .line 235
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_item:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->cashDiscountView:Lcom/squareup/ui/cart/CartEntryView;

    .line 236
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_check_box:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinCheckBox;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;)Lcom/squareup/marin/widgets/MarinCheckBox;
    .locals 0

    .line 227
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    return-object p0
.end method


# virtual methods
.method public bind()V
    .locals 7

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->getPosition()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$600(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;I)I

    move-result v0

    .line 241
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->ticketMutable(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    .line 243
    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v3}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v4}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getCartLevelCashDiscount(Ljava/lang/String;I)Lcom/squareup/checkout/Discount;

    move-result-object v3

    .line 244
    iget-object v4, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->cashDiscountView:Lcom/squareup/ui/cart/CartEntryView;

    iget-object v5, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 245
    invoke-static {v5}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$500(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object v5

    iget-object v6, v3, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    iget-object v3, v3, Lcom/squareup/checkout/Discount;->amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v5, v6, v3, v1}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->discountItem(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v3

    .line 244
    invoke-virtual {v4, v3}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v3}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v4}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->isEntrySelected(Ljava/lang/String;IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinCheckBox;->setChecked(Z)V

    .line 249
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->discountRowView:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$300()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 250
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->discountRowView:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;I)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinCheckBox;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->discountRowView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method
