.class public final Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;
.super Ljava/lang/Object;
.source "NewTicketView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/ticket/NewTicketView;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/NewTicketPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/NewTicketPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/NewTicketPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/ticket/NewTicketView;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectEmployeeManagementModeDecider(Lcom/squareup/ui/ticket/NewTicketView;Lcom/squareup/permissions/EmployeeManagementModeDecider;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/ticket/NewTicketView;Lcom/squareup/ui/ticket/NewTicketPresenter;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/ticket/NewTicketView;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/NewTicketPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;->injectPresenter(Lcom/squareup/ui/ticket/NewTicketView;Lcom/squareup/ui/ticket/NewTicketPresenter;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;->injectEmployeeManagementModeDecider(Lcom/squareup/ui/ticket/NewTicketView;Lcom/squareup/permissions/EmployeeManagementModeDecider;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/ticket/NewTicketView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/NewTicketView_MembersInjector;->injectMembers(Lcom/squareup/ui/ticket/NewTicketView;)V

    return-void
.end method
