.class public interface abstract Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;
.super Ljava/lang/Object;
.source "MasterDetailTicketPresenter.java"

# interfaces
.implements Lcom/squareup/ui/ticket/TicketListPresenter$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/ticket/GroupListView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/ticket/TicketListView;)V
.end method
