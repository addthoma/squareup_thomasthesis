.class public Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "O1ReminderDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen$Factory;,
        Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderDialogScreen$XaDqEAvWTUlsk_XQBipDPzH2EJM;->INSTANCE:Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderDialogScreen$XaDqEAvWTUlsk_XQBipDPzH2EJM;

    .line 46
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;
    .locals 0

    .line 46
    new-instance p0, Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;

    invoke-direct {p0}, Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;-><init>()V

    return-object p0
.end method
