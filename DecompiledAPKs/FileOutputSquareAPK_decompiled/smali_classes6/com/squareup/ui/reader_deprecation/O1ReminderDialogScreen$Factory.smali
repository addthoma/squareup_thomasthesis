.class public Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen$Factory;
.super Ljava/lang/Object;
.source "O1ReminderDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/analytics/Analytics;Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 34
    new-instance p2, Lcom/squareup/ui/reader_deprecation/O1ReminderEvent;

    const-string p3, "Order a Reader"

    invoke-direct {p2, p3}, Lcom/squareup/ui/reader_deprecation/O1ReminderEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p0, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 35
    sget p0, Lcom/squareup/cardreader/ui/R$string;->o1_reminder_ja_url:I

    invoke-virtual {p1, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/analytics/Analytics;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 38
    new-instance p1, Lcom/squareup/ui/reader_deprecation/O1ReminderEvent;

    const-string p2, "OK"

    invoke-direct {p1, p2}, Lcom/squareup/ui/reader_deprecation/O1ReminderEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method static synthetic lambda$create$2(Lcom/squareup/analytics/Analytics;Landroid/content/DialogInterface;)V
    .locals 1

    .line 40
    new-instance p1, Lcom/squareup/ui/reader_deprecation/O1ReminderEvent;

    const-string v0, "OK"

    invoke-direct {p1, v0}, Lcom/squareup/ui/reader_deprecation/O1ReminderEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 27
    const-class v0, Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen$ParentComponent;

    .line 28
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen$ParentComponent;

    .line 29
    invoke-interface {v0}, Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen$ParentComponent;->analytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/cardreader/ui/R$string;->o1_reminder_ja_title:I

    .line 31
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->o1_reminder_ja_body:I

    .line 32
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->o1_reminder_ja_confirm:I

    new-instance v3, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderDialogScreen$Factory$aAdAZm6-WGFC9A78QTi4zNya7p0;

    invoke-direct {v3, v0, p1}, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderDialogScreen$Factory$aAdAZm6-WGFC9A78QTi4zNya7p0;-><init>(Lcom/squareup/analytics/Analytics;Landroid/content/Context;)V

    .line 33
    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/cardreader/ui/R$string;->gen2_denial_cancel:I

    new-instance v2, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderDialogScreen$Factory$Obc_dHxHeQHVFVFHuwgvVZPXy8E;

    invoke-direct {v2, v0}, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderDialogScreen$Factory$Obc_dHxHeQHVFVFHuwgvVZPXy8E;-><init>(Lcom/squareup/analytics/Analytics;)V

    .line 37
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 39
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderDialogScreen$Factory$rPlmLbHWxr-sDoDMUVJO2-Okh40;

    invoke-direct {v1, v0}, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderDialogScreen$Factory$rPlmLbHWxr-sDoDMUVJO2-Okh40;-><init>(Lcom/squareup/analytics/Analytics;)V

    .line 40
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 30
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
