.class public final Lcom/squareup/ui/login/CreateAccountHelper;
.super Ljava/lang/Object;
.source "CreateAccountHelper.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/login/CreateAccountScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/CreateAccountHelper$AccountCreationEvent;,
        Lcom/squareup/ui/login/CreateAccountHelper$NextStep;,
        Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\u0008\u0007\u0018\u00002\u00020\u0001:\u0003./0BO\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u001eH\u0007J\u001e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H\u0002J\u0012\u0010)\u001a\u00020\u00192\u0008\u0010*\u001a\u0004\u0018\u00010\u0001H\u0002J&\u0010+\u001a\u0008\u0012\u0004\u0012\u00020$0#2\u0006\u0010,\u001a\u00020&2\u0006\u0010-\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001f\u0010\u0017\u001a\u0010\u0012\u000c\u0012\n \u001a*\u0004\u0018\u00010\u00190\u00190\u0018\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/ui/login/CreateAccountHelper;",
        "",
        "accountService",
        "Lcom/squareup/account/PersistentAccountService;",
        "accountStatusService",
        "Lcom/squareup/server/accountstatus/AccountStatusService;",
        "authenticationService",
        "Lcom/squareup/server/account/AuthenticationService;",
        "authenticator",
        "Lcom/squareup/account/LegacyAuthenticator;",
        "runner",
        "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
        "res",
        "Lcom/squareup/util/Res;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "adAnalytics",
        "Lcom/squareup/adanalytics/AdAnalytics;",
        "finalCreateAccountLogInCheck",
        "Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;",
        "(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/server/accountstatus/AccountStatusService;Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;)V",
        "createAccountDisposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "progressPresenter",
        "Lcom/squareup/caller/ProgressAndFailurePresenter;",
        "Lcom/squareup/server/SimpleResponse;",
        "kotlin.jvm.PlatformType",
        "getProgressPresenter",
        "()Lcom/squareup/caller/ProgressAndFailurePresenter;",
        "requestBody",
        "Lcom/squareup/server/account/CreateBody;",
        "attemptCreateAccount",
        "Lio/reactivex/disposables/Disposable;",
        "createBody",
        "finalizeLogIn",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
        "sessionToken",
        "",
        "existingMerchant",
        "",
        "parseClientError",
        "body",
        "postCreateLogin",
        "email",
        "password",
        "AccountCreationEvent",
        "NextStep",
        "NextStepResponse",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountService:Lcom/squareup/account/PersistentAccountService;

.field private final accountStatusService:Lcom/squareup/server/accountstatus/AccountStatusService;

.field private final adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final authenticationService:Lcom/squareup/server/account/AuthenticationService;

.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final createAccountDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final finalCreateAccountLogInCheck:Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

.field private final progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/caller/ProgressAndFailurePresenter<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation
.end field

.field private requestBody:Lcom/squareup/server/account/CreateBody;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/server/accountstatus/AccountStatusService;Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticator"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runner"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adAnalytics"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "finalCreateAccountLogInCheck"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper;->accountService:Lcom/squareup/account/PersistentAccountService;

    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountHelper;->accountStatusService:Lcom/squareup/server/accountstatus/AccountStatusService;

    iput-object p3, p0, Lcom/squareup/ui/login/CreateAccountHelper;->authenticationService:Lcom/squareup/server/account/AuthenticationService;

    iput-object p4, p0, Lcom/squareup/ui/login/CreateAccountHelper;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    iput-object p5, p0, Lcom/squareup/ui/login/CreateAccountHelper;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iput-object p6, p0, Lcom/squareup/ui/login/CreateAccountHelper;->res:Lcom/squareup/util/Res;

    iput-object p7, p0, Lcom/squareup/ui/login/CreateAccountHelper;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p8, p0, Lcom/squareup/ui/login/CreateAccountHelper;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    iput-object p9, p0, Lcom/squareup/ui/login/CreateAccountHelper;->finalCreateAccountLogInCheck:Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    .line 61
    new-instance p1, Lcom/squareup/caller/ProgressAndFailurePresenter;

    .line 62
    new-instance p2, Lcom/squareup/request/RequestMessages;

    iget-object p3, p0, Lcom/squareup/ui/login/CreateAccountHelper;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/loggedout/R$string;->signing_up:I

    sget p5, Lcom/squareup/loggedout/R$string;->signing_up_fail:I

    invoke-direct {p2, p3, p4, p5}, Lcom/squareup/request/RequestMessages;-><init>(Lcom/squareup/util/Res;II)V

    .line 63
    new-instance p3, Lcom/squareup/ui/login/CreateAccountHelper$progressPresenter$1;

    invoke-direct {p3, p0}, Lcom/squareup/ui/login/CreateAccountHelper$progressPresenter$1;-><init>(Lcom/squareup/ui/login/CreateAccountHelper;)V

    check-cast p3, Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;

    const-string p4, "createCall"

    .line 61
    invoke-direct {p1, p4, p2, p3}, Lcom/squareup/caller/ProgressAndFailurePresenter;-><init>(Ljava/lang/String;Lcom/squareup/request/RequestMessages;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    .line 74
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper;->createAccountDisposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method public static final synthetic access$finalizeLogIn(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/String;Z)Lio/reactivex/Single;
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/login/CreateAccountHelper;->finalizeLogIn(Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAdAnalytics$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/adanalytics/AdAnalytics;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getAuthenticator$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/account/LegacyAuthenticator;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    return-object p0
.end method

.method public static final synthetic access$getCreateAccountDisposables$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lio/reactivex/disposables/CompositeDisposable;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->createAccountDisposables:Lio/reactivex/disposables/CompositeDisposable;

    return-object p0
.end method

.method public static final synthetic access$getFinalCreateAccountLogInCheck$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->finalCreateAccountLogInCheck:Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    return-object p0
.end method

.method public static final synthetic access$getRequestBody$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/server/account/CreateBody;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->requestBody:Lcom/squareup/server/account/CreateBody;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/util/Res;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$parseClientError(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/CreateAccountHelper;->parseClientError(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$postCreateLogin(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/String;Ljava/lang/String;Z)Lio/reactivex/Single;
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/CreateAccountHelper;->postCreateLogin(Ljava/lang/String;Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setRequestBody$p(Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/server/account/CreateBody;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper;->requestBody:Lcom/squareup/server/account/CreateBody;

    return-void
.end method

.method private final finalizeLogIn(Ljava/lang/String;Z)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
            ">;"
        }
    .end annotation

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->accountStatusService:Lcom/squareup/server/accountstatus/AccountStatusService;

    .line 190
    invoke-static {p1}, Lcom/squareup/server/account/AuthorizationHeaders;->authorizationHeaderValueFrom(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/server/accountstatus/AccountStatusServiceKt;->getAccountStatusWithDefaults(Lcom/squareup/server/accountstatus/AccountStatusService;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 191
    new-instance v1, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;-><init>(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/String;Z)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "accountStatusService\n   \u2026re)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final parseClientError(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 3

    .line 138
    instance-of v0, p1, Lcom/squareup/protos/register/api/LoginResponse;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x0

    check-cast p1, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v2, p1, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/register/api/LoginResponse;->error_message:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    :cond_0
    instance-of v0, p1, Lcom/squareup/server/SimpleResponse;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/squareup/server/SimpleResponse;

    :goto_0
    return-object v0

    .line 140
    :cond_1
    new-instance v0, Lkotlin/NotImplementedError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown error body type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final postCreateLogin(Ljava/lang/String;Ljava/lang/String;Z)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
            ">;"
        }
    .end annotation

    .line 148
    new-instance v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/LoginRequest$Builder;-><init>()V

    .line 149
    invoke-virtual {v0, p1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->email(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object p1

    .line 150
    invoke-virtual {p1, p2}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->password(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object p1

    .line 151
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->build()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object p1

    .line 152
    iget-object p2, p0, Lcom/squareup/ui/login/CreateAccountHelper;->authenticationService:Lcom/squareup/server/account/AuthenticationService;

    const-string v0, "loginRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/account/AuthenticationService;->login(Lcom/squareup/protos/register/api/LoginRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 153
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->receivedResponse()Lio/reactivex/Single;

    move-result-object p1

    .line 154
    sget-object p2, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;->INSTANCE:Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 160
    new-instance p2, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;

    invoke-direct {p2, p0, p3}, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;-><init>(Lcom/squareup/ui/login/CreateAccountHelper;Z)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationService.lo\u2026istingMerchant)\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final attemptCreateAccount(Lcom/squareup/server/account/CreateBody;)Lio/reactivex/disposables/Disposable;
    .locals 3

    const-string v0, "createBody"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper;->requestBody:Lcom/squareup/server/account/CreateBody;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->createAccountDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountHelper;->accountService:Lcom/squareup/account/PersistentAccountService;

    invoke-virtual {v1, p1}, Lcom/squareup/account/PersistentAccountService;->create(Lcom/squareup/server/account/CreateBody;)Lio/reactivex/Single;

    move-result-object v1

    .line 81
    new-instance v2, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;-><init>(Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/server/account/CreateBody;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 93
    new-instance v1, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$2;-><init>(Lcom/squareup/ui/login/CreateAccountHelper;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    .line 94
    new-instance v1, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;-><init>(Lcom/squareup/ui/login/CreateAccountHelper;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v1, "accountService.create(cr\u2026  }\n          }\n        }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper;->createAccountDisposables:Lio/reactivex/disposables/CompositeDisposable;

    check-cast p1, Lio/reactivex/disposables/Disposable;

    return-object p1
.end method

.method public final getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/caller/ProgressAndFailurePresenter<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    return-object v0
.end method
