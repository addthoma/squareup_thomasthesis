.class public final Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;
.super Lcom/squareup/ui/login/AuthenticatorScreenRendering;
.source "AuthenticatorRendering.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticatorScreenRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnrollGoogleAuthQrRendering"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u0016J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;",
        "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
        "data",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
        "input",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "getData",
        "()Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
        "getInput",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "toString",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

.field private final input:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering$Companion;

    .line 188
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering$Companion;

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 183
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 182
    sget-object p2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 180
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->copy(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    iget-object p1, p1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getData()Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    return-object v0
.end method

.method public final getInput()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final toScreen()Lcom/squareup/workflow/legacy/Screen;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation

    .line 185
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    iget-object v3, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnrollGoogleAuthQrRendering(data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->data:Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
