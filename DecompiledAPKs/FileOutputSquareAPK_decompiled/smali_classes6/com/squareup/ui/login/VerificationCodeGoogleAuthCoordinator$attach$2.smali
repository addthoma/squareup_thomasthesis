.class final Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;
.super Lkotlin/jvm/internal/Lambda;
.source "VerificationCodeGoogleAuthCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerificationCodeGoogleAuthCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerificationCodeGoogleAuthCoordinator.kt\ncom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,151:1\n1550#2,3:152\n*E\n*S KotlinDebug\n*F\n+ 1 VerificationCodeGoogleAuthCoordinator.kt\ncom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2\n*L\n94#1,3:152\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->$view:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 68
    new-instance v1, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;-><init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 76
    iget-object v2, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->access$getVerificationCodeField$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$3;

    invoke-direct {v3, p0, v1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$3;-><init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;)V

    check-cast v3, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 92
    iget-object v2, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    invoke-static {v2, v0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->access$setScreen$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;)V

    .line 93
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->getAllTwoFactorDetails()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 152
    instance-of v2, v0, Ljava/util/Collection;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 153
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 94
    iget-object v2, v2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    const/4 v4, 0x1

    .line 109
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 98
    iget-object v3, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verify:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 99
    new-instance v3, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, v1, p1, v4}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;Lcom/squareup/workflow/legacy/WorkflowInput;Z)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    new-instance v5, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$sam$i$java_lang_Runnable$0;

    invoke-direct {v5, v3}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$sam$i$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v5, Ljava/lang/Runnable;

    invoke-virtual {v2, v5}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 100
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v5, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v5}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 101
    new-instance v3, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$2;

    invoke-direct {v3, p0, v1, p1, v4}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$2;-><init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;Lcom/squareup/workflow/legacy/WorkflowInput;Z)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    if-eqz v4, :cond_4

    .line 105
    iget-object v3, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_use_sms:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 106
    new-instance v3, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$3;

    invoke-direct {v3, p0, v1, p1, v4}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$3;-><init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;Lcom/squareup/workflow/legacy/WorkflowInput;Z)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 109
    :cond_4
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->access$getContactSupport$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->access$buildContactSupportLinkText(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    invoke-virtual {p1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->updateEnabledState()V

    return-void
.end method
