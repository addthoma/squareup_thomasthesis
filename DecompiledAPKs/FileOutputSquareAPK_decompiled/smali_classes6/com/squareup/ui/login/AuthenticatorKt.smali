.class public final Lcom/squareup/ui/login/AuthenticatorKt;
.super Ljava/lang/Object;
.source "Authenticator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\"\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\"\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007*.\u0010\u0008\"\u0014\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\t2\u0014\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\t\u00a8\u0006\r"
    }
    d2 = {
        "DEVICE_CODE_MAX_LENGTH",
        "",
        "getDEVICE_CODE_MAX_LENGTH",
        "()I",
        "DEVICE_CODE_SUPPORTED_LENGTH",
        "",
        "getDEVICE_CODE_SUPPORTED_LENGTH",
        "()Ljava/util/Set;",
        "Authenticator",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthenticatorRendering;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final DEVICE_CODE_MAX_LENGTH:I

.field private static final DEVICE_CODE_SUPPORTED_LENGTH:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x6

    .line 5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorKt;->DEVICE_CODE_SUPPORTED_LENGTH:Ljava/util/Set;

    .line 6
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorKt;->DEVICE_CODE_SUPPORTED_LENGTH:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->max(Ljava/lang/Iterable;)Ljava/lang/Comparable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    sput v0, Lcom/squareup/ui/login/AuthenticatorKt;->DEVICE_CODE_MAX_LENGTH:I

    return-void
.end method

.method public static final getDEVICE_CODE_MAX_LENGTH()I
    .locals 1

    .line 6
    sget v0, Lcom/squareup/ui/login/AuthenticatorKt;->DEVICE_CODE_MAX_LENGTH:I

    return v0
.end method

.method public static final getDEVICE_CODE_SUPPORTED_LENGTH()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 5
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorKt;->DEVICE_CODE_SUPPORTED_LENGTH:Ljava/util/Set;

    return-object v0
.end method
