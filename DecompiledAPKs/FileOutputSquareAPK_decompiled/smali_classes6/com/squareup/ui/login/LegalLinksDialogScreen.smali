.class public Lcom/squareup/ui/login/LegalLinksDialogScreen;
.super Lcom/squareup/container/ContainerTreeKey;
.source "LegalLinksDialogScreen.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/login/LegalLinksDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/LegalLinksDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/login/LegalLinksDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final countryCode:Lcom/squareup/CountryCode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/ui/login/-$$Lambda$LegalLinksDialogScreen$KqUylQeSbcm2jkg7910cG5Y2ue8;->INSTANCE:Lcom/squareup/ui/login/-$$Lambda$LegalLinksDialogScreen$KqUylQeSbcm2jkg7910cG5Y2ue8;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/login/LegalLinksDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/CountryCode;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/login/LegalLinksDialogScreen;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/login/LegalLinksDialogScreen;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/ui/login/LegalLinksDialogScreen;

    invoke-static {}, Lcom/squareup/CountryCode;->values()[Lcom/squareup/CountryCode;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    aget-object p0, v1, p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/LegalLinksDialogScreen;-><init>(Lcom/squareup/CountryCode;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 58
    iget-object p2, p0, Lcom/squareup/ui/login/LegalLinksDialogScreen;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {p2}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
