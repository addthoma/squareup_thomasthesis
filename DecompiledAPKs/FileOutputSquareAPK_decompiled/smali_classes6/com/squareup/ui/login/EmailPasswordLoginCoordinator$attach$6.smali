.class final Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;
.super Lkotlin/jvm/internal/Lambda;
.source "EmailPasswordLoginCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmailPasswordLoginCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmailPasswordLoginCoordinator.kt\ncom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6\n*L\n1#1,305:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;

    invoke-direct {v0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;->INSTANCE:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;->invoke(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;)Ljava/lang/String;
    .locals 2

    .line 152
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getEmailForPrefill()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    move-object v0, p1

    :cond_1
    return-object v0
.end method
