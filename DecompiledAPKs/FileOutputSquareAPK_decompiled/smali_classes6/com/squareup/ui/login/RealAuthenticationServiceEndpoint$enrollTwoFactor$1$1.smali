.class final Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AuthenticationServiceEndpoint.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1;->apply(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/ui/login/AuthenticationCallResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;",
        "response",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1$1;

    invoke-direct {v0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;
    .locals 4

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    new-instance v0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;

    new-instance v1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;

    iget-object v2, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    const-string v3, "response.error_title"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    const-string v3, "response.error_message"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, p1}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1$1;->invoke(Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;

    move-result-object p1

    return-object p1
.end method
