.class final Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DeviceCodeLoginCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res:Landroid/content/res/Resources;

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;Landroid/content/res/Resources;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->$res:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 73
    iget-object v2, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->$res:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/common/authenticatorviews/R$string;->sign_in:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 74
    new-instance v2, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$1;-><init>(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 75
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->$res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 76
    new-instance v2, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$2;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->access$getDeviceCodeField$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$3;-><init>(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$4;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$4;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
