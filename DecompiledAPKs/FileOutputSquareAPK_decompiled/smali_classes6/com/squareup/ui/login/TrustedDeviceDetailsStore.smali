.class public interface abstract Lcom/squareup/ui/login/TrustedDeviceDetailsStore;
.super Ljava/lang/Object;
.source "TrustedDeviceDetailsStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001R$\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003X\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/login/TrustedDeviceDetailsStore;",
        "",
        "personTokenToDeviceDetails",
        "",
        "",
        "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
        "getPersonTokenToDeviceDetails",
        "()Ljava/util/Map;",
        "setPersonTokenToDeviceDetails",
        "(Ljava/util/Map;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getPersonTokenToDeviceDetails()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setPersonTokenToDeviceDetails(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;)V"
        }
    .end annotation
.end method
