.class public final Lcom/squareup/ui/login/RealAuthenticator;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealAuthenticator.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthenticatorRendering;",
        ">;",
        "Lcom/squareup/workflow/Workflow<",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthenticatorRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAuthenticator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAuthenticator.kt\ncom/squareup/ui/login/RealAuthenticator\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 RealAuthenticator.kt\ncom/squareup/ui/login/RealAuthenticatorKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 7 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,1721:1\n180#2:1722\n1588#3,15:1723\n1588#3,15:1756\n1588#3,15:1782\n1588#3,15:1797\n41#4:1738\n56#4,2:1739\n85#4:1742\n85#4:1745\n276#5:1741\n276#5:1744\n276#5:1747\n240#6:1743\n240#6:1746\n1360#7:1748\n1429#7,3:1749\n1360#7:1752\n1429#7,3:1753\n601#7,11:1771\n*E\n*S KotlinDebug\n*F\n+ 1 RealAuthenticator.kt\ncom/squareup/ui/login/RealAuthenticator\n*L\n182#1:1722\n203#1,15:1723\n496#1,15:1756\n697#1,15:1782\n1005#1,15:1797\n220#1:1738\n220#1,2:1739\n225#1:1742\n238#1:1745\n220#1:1741\n225#1:1744\n238#1:1747\n225#1:1743\n238#1:1746\n251#1:1748\n251#1,3:1749\n310#1:1752\n310#1,3:1753\n587#1,11:1771\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00aa\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001j\u0002`\u00052\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0006:\u0002\u008d\u0001BY\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u00a2\u0006\u0002\u0010\u001cJ\u0018\u0010!\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020#2\u0006\u0010\u0016\u001a\u00020#H\u0002J\u0008\u0010$\u001a\u00020\u0007H\u0002J\u001a\u0010%\u001a\u00020\u00072\u0006\u0010&\u001a\u00020\u00022\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J \u0010)\u001a\u00020\u00072\u0006\u0010*\u001a\u00020\u00022\u0006\u0010+\u001a\u00020\u00022\u0006\u0010,\u001a\u00020\u0007H\u0016J\u0010\u0010-\u001a\u00020\u00072\u0006\u0010.\u001a\u00020/H\u0002J,\u00100\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\u00022\u0006\u0010,\u001a\u00020\u00072\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000302H\u0016J3\u00103\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`52\u0006\u00106\u001a\u00020/2\u000c\u00107\u001a\u0008\u0012\u0004\u0012\u00020908H\u0000\u00a2\u0006\u0002\u0008:J*\u0010;\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`52\u0008\u0010<\u001a\u0004\u0018\u00010/2\u0006\u00106\u001a\u00020/H\u0002J\u0010\u0010=\u001a\u00020(2\u0006\u0010,\u001a\u00020\u0007H\u0016J7\u0010>\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u0006\u0010?\u001a\u00020@2\u000c\u0010A\u001a\u0008\u0012\u0004\u0012\u00020C0BH\u0000\u00a2\u0006\u0002\u0008DJC\u0010E\u001a$\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020C0B\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`50F*\u00020\u00072\u0006\u0010?\u001a\u00020@2\u0006\u0010G\u001a\u00020\u0002H\u0000\u00a2\u0006\u0002\u0008HJ\u001a\u0010I\u001a\u00020\u0007*\u00020\u00072\u000c\u0010A\u001a\u0008\u0012\u0004\u0012\u00020J0BH\u0002J,\u0010K\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`50L*\u00020\u00072\u0006\u0010G\u001a\u00020\u0002H\u0002J\u001c\u0010M\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u0007H\u0002J1\u0010N\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u0006\u0010G\u001a\u00020\u00022\u0006\u0010O\u001a\u00020PH\u0000\u00a2\u0006\u0002\u0008QJ!\u0010R\u001a\u00020\u0007*\u00020\u00072\u0006\u0010S\u001a\u00020T2\u0006\u0010U\u001a\u00020VH\u0000\u00a2\u0006\u0002\u0008WJ!\u0010X\u001a\u00020\u0007*\u00020\u00072\u0006\u00106\u001a\u00020/2\u0006\u0010Y\u001a\u00020/H\u0000\u00a2\u0006\u0002\u0008ZJ2\u0010[\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u0006\u0010G\u001a\u00020\u00022\u000c\u0010\\\u001a\u0008\u0012\u0004\u0012\u00020^0]H\u0002J\u001b\u0010_\u001a\u00020\u0007*\u00020\u00072\u0008\u0010`\u001a\u0004\u0018\u00010/H\u0000\u00a2\u0006\u0002\u0008aJ\u001c\u0010b\u001a\u00020\u0007*\u00020\u00072\u0006\u0010c\u001a\u00020d2\u0006\u0010e\u001a\u00020fH\u0002J\u000c\u0010g\u001a\u00020\u0007*\u00020\u0007H\u0002J\u000c\u0010h\u001a\u00020\u0007*\u00020\u0007H\u0002J$\u0010i\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u0006\u0010G\u001a\u00020\u0002H\u0002J/\u0010j\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u000c\u0010A\u001a\u0008\u0012\u0004\u0012\u00020k0BH\u0000\u00a2\u0006\u0002\u0008lJ!\u0010m\u001a\u00020\u0007*\u00020\u00072\u000e\u0010A\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010n0BH\u0000\u00a2\u0006\u0002\u0008oJ<\u0010p\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u0006\u0010G\u001a\u00020\u00022\u000c\u0010\\\u001a\u0008\u0012\u0004\u0012\u00020^0]2\u0008\u0008\u0001\u0010q\u001a\u00020rH\u0002J$\u0010s\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u0006\u0010G\u001a\u00020\u0002H\u0002JA\u0010t\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u0006\u0010G\u001a\u00020\u00022\u000c\u0010\\\u001a\u0008\u0012\u0004\u0012\u00020^0]2\u0008\u0008\u0003\u0010q\u001a\u00020rH\u0000\u00a2\u0006\u0002\u0008uJ2\u0010v\u001a\u00020\u0007*\u00020\u00072\u000c\u0010w\u001a\u0008\u0012\u0004\u0012\u00020d0]2\u0006\u0010x\u001a\u00020#2\u0006\u0010y\u001a\u00020#2\u0006\u0010z\u001a\u00020#H\u0002J\u000c\u0010{\u001a\u00020\u0007*\u00020\u0007H\u0002J\u000c\u0010|\u001a\u00020\u0007*\u00020\u0007H\u0002J\u000c\u0010}\u001a\u00020\u0007*\u00020\u0002H\u0002J\u0015\u0010~\u001a\u00020\u0007*\u00020\u00072\u0007\u0010\u007f\u001a\u00030\u0080\u0001H\u0002J$\u0010\u0081\u0001\u001a\u00020\u0007*\u00020\u00072\u0006\u0010?\u001a\u00020@2\u0007\u0010\u0082\u0001\u001a\u00020/H\u0000\u00a2\u0006\u0003\u0008\u0083\u0001J5\u0010\u0084\u0001\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5*\u00020\u00072\u0006\u0010S\u001a\u00020T2\u0008\u0010\u0085\u0001\u001a\u00030\u0086\u0001H\u0000\u00a2\u0006\u0003\u0008\u0087\u0001JX\u0010\u0088\u0001\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000304j\u0002`5\"\u0005\u0008\u0000\u0010\u0089\u0001*\u00020\u00072\u0006\u0010G\u001a\u00020\u00022\r\u0010A\u001a\t\u0012\u0005\u0012\u0003H\u0089\u00010B2\u0015\u0010\u008a\u0001\u001a\u0010\u0012\u0005\u0012\u0003H\u0089\u0001\u0012\u0005\u0012\u00030\u008b\u00010FH\u0000\u00a2\u0006\u0003\u0008\u008c\u0001R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u001d\u001a&\u0012\u000c\u0012\n  *\u0004\u0018\u00010\u001f0\u001f  *\u0012\u0012\u000c\u0012\n  *\u0004\u0018\u00010\u001f0\u001f\u0018\u00010\u001e0\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u008e\u0001"
    }
    d2 = {
        "Lcom/squareup/ui/login/RealAuthenticator;",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthenticatorRendering;",
        "Lcom/squareup/ui/login/Authenticator;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "trustedDeviceDetailsStore",
        "Lcom/squareup/ui/login/TrustedDeviceDetailsStore;",
        "authenticationServiceEndpoint",
        "Lcom/squareup/ui/login/AuthenticationServiceEndpoint;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "countryGuesser",
        "Lcom/squareup/location/CountryGuesser;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "toastFactory",
        "Lcom/squareup/util/ToastFactory;",
        "supportsDeviceCodeLogin",
        "Lcom/squareup/ui/login/SupportsDeviceCodeLogin;",
        "persistentAccountCache",
        "Lcom/squareup/account/accountservice/AppAccountCache;",
        "safetyNetCaptchaVerifier",
        "Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;",
        "(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lio/reactivex/Scheduler;Lcom/squareup/badbus/BadBus;Lcom/squareup/location/CountryGuesser;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/login/SupportsDeviceCodeLogin;Lcom/squareup/account/accountservice/AppAccountCache;Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;)V",
        "sessionExpiredBusEvents",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/account/AccountEvents$SessionExpired;",
        "kotlin.jvm.PlatformType",
        "defaultStartState",
        "asLandingScreen",
        "",
        "deviceCodeStartState",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "providedDeviceCodeAuthStartState",
        "deviceCode",
        "",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "resetPasswordCallback",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/login/AuthUpdate;",
        "email",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/ForgotPasswordResponse;",
        "resetPasswordCallback$impl_release",
        "showForgotPasswordToastThenGoBack",
        "message",
        "snapshotState",
        "deviceCodeLoginCallback",
        "request",
        "Lcom/squareup/protos/register/api/LoginRequest;",
        "response",
        "Lcom/squareup/ui/login/AuthenticationCallResult;",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "deviceCodeLoginCallback$impl_release",
        "emailLoginCallback",
        "Lkotlin/Function1;",
        "input",
        "emailLoginCallback$impl_release",
        "enrollTwoFactorCallback",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "getObservableForCall",
        "Lio/reactivex/Single;",
        "goBackToStart",
        "handleEvent",
        "event",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "handleEvent$impl_release",
        "loginWithCaptcha",
        "requestTemplate",
        "Lcom/squareup/protos/register/api/LoginRequest$Builder;",
        "captcha",
        "Lcom/squareup/safetynetrecaptcha/CaptchaResult$Success;",
        "loginWithCaptcha$impl_release",
        "loginWithEmail",
        "password",
        "loginWithEmail$impl_release",
        "multiUnitSingleMerchantEmployeeLoginFlow",
        "unitList",
        "",
        "Lcom/squareup/protos/register/api/Unit;",
        "promptToResetPassword",
        "emailForPrefill",
        "promptToResetPassword$impl_release",
        "promptToVerifySmsCode",
        "twoFactorDetails",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "login",
        "Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;",
        "resendSmsCode",
        "resetSessionState",
        "retryResetPassword",
        "selectUnitCallback",
        "Lcom/squareup/protos/register/api/SelectUnitResponse;",
        "selectUnitCallback$impl_release",
        "sendVerificationCodeTwoFactorCallback",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        "sendVerificationCodeTwoFactorCallback$impl_release",
        "singleUnitEmployeeLoginFlow",
        "loadingMsg",
        "",
        "skipTwoFactorEnrollment",
        "startEmployeeLoginFlow",
        "startEmployeeLoginFlow$impl_release",
        "startTwoFactorFlow",
        "twoFactorDetailsList",
        "canUseGoogleAuth",
        "canUseSms",
        "canSkipEnroll",
        "switchLoginPromptToDeviceCode",
        "switchVerificationPromptToSms",
        "toStartState",
        "updatePassword",
        "newPassword",
        "Lcom/squareup/account/SecretString;",
        "verifyLoginByCaptcha",
        "apiKey",
        "verifyLoginByCaptcha$impl_release",
        "verifyLoginByCaptchaCallback",
        "result",
        "Lcom/squareup/safetynetrecaptcha/CaptchaResult;",
        "verifyLoginByCaptchaCallback$impl_release",
        "verifyTwoFactorCodeCallback",
        "T",
        "parseTokenAndDeviceDetails",
        "Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;",
        "verifyTwoFactorCodeCallback$impl_release",
        "TokenAndDeviceDetails",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

.field private final countryGuesser:Lcom/squareup/location/CountryGuesser;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final persistentAccountCache:Lcom/squareup/account/accountservice/AppAccountCache;

.field private final safetyNetCaptchaVerifier:Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;

.field private final sessionExpiredBusEvents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/account/AccountEvents$SessionExpired;",
            ">;"
        }
    .end annotation
.end field

.field private final supportsDeviceCodeLogin:Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;

.field private final trustedDeviceDetailsStore:Lcom/squareup/ui/login/TrustedDeviceDetailsStore;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lio/reactivex/Scheduler;Lcom/squareup/badbus/BadBus;Lcom/squareup/location/CountryGuesser;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/login/SupportsDeviceCodeLogin;Lcom/squareup/account/accountservice/AppAccountCache;Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "trustedDeviceDetailsStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationServiceEndpoint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryGuesser"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toastFactory"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "supportsDeviceCodeLogin"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistentAccountCache"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "safetyNetCaptchaVerifier"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator;->trustedDeviceDetailsStore:Lcom/squareup/ui/login/TrustedDeviceDetailsStore;

    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    iput-object p3, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p5, p0, Lcom/squareup/ui/login/RealAuthenticator;->countryGuesser:Lcom/squareup/location/CountryGuesser;

    iput-object p6, p0, Lcom/squareup/ui/login/RealAuthenticator;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p7, p0, Lcom/squareup/ui/login/RealAuthenticator;->toastFactory:Lcom/squareup/util/ToastFactory;

    iput-object p8, p0, Lcom/squareup/ui/login/RealAuthenticator;->supportsDeviceCodeLogin:Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    iput-object p9, p0, Lcom/squareup/ui/login/RealAuthenticator;->persistentAccountCache:Lcom/squareup/account/accountservice/AppAccountCache;

    iput-object p10, p0, Lcom/squareup/ui/login/RealAuthenticator;->safetyNetCaptchaVerifier:Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;

    .line 176
    const-class p1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-virtual {p4, p1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator;->sessionExpiredBusEvents:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$enrollTwoFactorCallback(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 157
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator;->enrollTwoFactorCallback(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$startTwoFactorFlow(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;ZZZ)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 157
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/login/RealAuthenticator;->startTwoFactorFlow(Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;ZZZ)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method private final defaultStartState(ZZ)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 13

    .line 275
    invoke-static {}, Lcom/squareup/ui/login/RealAuthenticatorKt;->getSTART_STATE()Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    .line 277
    new-instance v4, Lcom/squareup/ui/login/AuthenticatorStack;

    .line 278
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1c

    const/4 v12, 0x0

    move-object v5, v1

    move v6, p1

    move v7, p2

    invoke-direct/range {v5 .. v12}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 277
    invoke-direct {v4, p1}, Lcom/squareup/ui/login/AuthenticatorStack;-><init>(Ljava/util/List;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x17

    const/4 v7, 0x0

    .line 276
    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    .line 281
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->resetSessionState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method private final deviceCodeStartState()Lcom/squareup/ui/login/AuthenticatorState;
    .locals 2

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->supportsDeviceCodeLogin:Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    invoke-interface {v0}, Lcom/squareup/ui/login/SupportsDeviceCodeLogin;->getEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 288
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/login/RealAuthenticator;->defaultStartState(ZZ)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    .line 291
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/RealAuthenticator;->switchLoginPromptToDeviceCode(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    return-object v0

    .line 285
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Device code login not supported, but device code login start state requested."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final enrollTwoFactorCallback(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorState;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 608
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 611
    instance-of v0, p2, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    .line 614
    iget-object v0, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v0, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    const-string v1, "two_factor_details"

    if-eqz v0, :cond_0

    .line 615
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    iget-object p2, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p2}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 614
    invoke-static {p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    goto :goto_0

    .line 617
    :cond_0
    iget-object v0, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v0, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    if-eqz v0, :cond_1

    iget-object p2, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;->ENROLL:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/login/RealAuthenticator;->promptToVerifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    goto :goto_0

    .line 618
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Expected either googleauth or sms to be non-null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 624
    :cond_2
    instance-of v0, p2, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    if-eqz v0, :cond_3

    check-cast p2, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final getObservableForCall(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)Lio/reactivex/Single;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;>;"
        }
    .end annotation

    .line 387
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/login/Operation;->getType()Lcom/squareup/ui/login/OperationType;

    move-result-object v0

    .line 389
    sget-object v1, Lcom/squareup/ui/login/OperationType$None;->INSTANCE:Lcom/squareup/ui/login/OperationType$None;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lio/reactivex/Single;->never()Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.never()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 390
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/login/OperationType$ResetPassword;

    if-eqz v1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    move-object p2, v0

    check-cast p2, Lcom/squareup/ui/login/OperationType$ResetPassword;

    invoke-virtual {p2}, Lcom/squareup/ui/login/OperationType$ResetPassword;->getRequest()Lcom/squareup/server/account/ForgotPasswordBody;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;->resetPassword(Lcom/squareup/server/account/ForgotPasswordBody;)Lio/reactivex/Single;

    move-result-object p1

    .line 391
    iget-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 392
    new-instance p2, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$1;

    invoke-direct {p2, p0, v0}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$1;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/OperationType;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationServiceEnd\u2026Type.request.email, it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 393
    :cond_1
    instance-of v1, v0, Lcom/squareup/ui/login/OperationType$VerifyLoginByCaptcha;

    if-eqz v1, :cond_2

    iget-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator;->safetyNetCaptchaVerifier:Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/login/OperationType$VerifyLoginByCaptcha;

    invoke-virtual {v1}, Lcom/squareup/ui/login/OperationType$VerifyLoginByCaptcha;->getApiKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;->verifyWithRecaptcha(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p2

    .line 394
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p2, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p2

    .line 395
    new-instance v1, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$2;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$2;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/OperationType;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "safetyNetCaptchaVerifier\u2026pe.requestTemplate, it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 396
    :cond_2
    instance-of v1, v0, Lcom/squareup/ui/login/OperationType$LoginWithEmail;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    check-cast v0, Lcom/squareup/ui/login/OperationType$LoginWithEmail;

    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$LoginWithEmail;->getRequest()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;->login(Lcom/squareup/protos/register/api/LoginRequest;)Lio/reactivex/Single;

    move-result-object v1

    .line 397
    iget-object v2, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    .line 398
    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$LoginWithEmail;->getRequest()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/squareup/ui/login/RealAuthenticator;->emailLoginCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Lcom/squareup/ui/login/AuthenticatorInput;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    if-eqz p1, :cond_3

    new-instance p2, Lcom/squareup/ui/login/RealAuthenticatorKt$sam$io_reactivex_functions_Function$0;

    invoke-direct {p2, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, p2

    :cond_3
    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {v1, p1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationServiceEnd\u2026tionType.request, input))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 399
    :cond_4
    instance-of v1, v0, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;

    if-eqz v1, :cond_5

    iget-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;

    invoke-virtual {v1}, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;->getRequest()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;->login(Lcom/squareup/protos/register/api/LoginRequest;)Lio/reactivex/Single;

    move-result-object p2

    .line 400
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p2, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p2

    .line 401
    new-instance v1, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/OperationType;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationServiceEnd\u2026rationType.request, it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 402
    :cond_5
    instance-of v1, v0, Lcom/squareup/ui/login/OperationType$SelectUnit;

    if-eqz v1, :cond_6

    .line 403
    iget-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    .line 404
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object v1

    check-cast v0, Lcom/squareup/ui/login/OperationType$SelectUnit;

    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$SelectUnit;->getRequest()Lcom/squareup/protos/register/api/SelectUnitRequest;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;->selectUnit(Lcom/squareup/account/SecretString;Lcom/squareup/protos/register/api/SelectUnitRequest;)Lio/reactivex/Single;

    move-result-object p2

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p2

    .line 406
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$4;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationServiceEnd\u2026 selectUnitCallback(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 407
    :cond_6
    instance-of v1, v0, Lcom/squareup/ui/login/OperationType$ResumeAutomaticUnitSelection;

    if-eqz v1, :cond_7

    .line 408
    sget-object p2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object p1

    sget-object v1, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;->EMAIL_PASSWORD:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;-><init>(Lcom/squareup/account/SecretString;Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;)V

    invoke-virtual {p2, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(emitOutput(S\u2026nToken, EMAIL_PASSWORD)))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 409
    :cond_7
    instance-of v1, v0, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByEnroll;

    const-string v2, "authenticationServiceEnd\u2026          }\n            }"

    if-eqz v1, :cond_8

    .line 410
    iget-object v3, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    .line 411
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object v4

    check-cast v0, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByEnroll;

    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByEnroll;->getRequest()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint$DefaultImpls;->enrollTwoFactor$default(Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    .line 412
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 413
    new-instance v1, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$5;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 418
    :cond_8
    instance-of v1, v0, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByUpgradeSession;

    if-eqz v1, :cond_9

    .line 419
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    .line 420
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object v3

    check-cast v0, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByUpgradeSession;

    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByUpgradeSession;->getRequest()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;->upgradeSession(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lio/reactivex/Single;

    move-result-object v0

    .line 421
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 422
    new-instance v1, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 427
    :cond_9
    instance-of p2, v0, Lcom/squareup/ui/login/OperationType$EnrollTwoFactor;

    if-eqz p2, :cond_a

    .line 428
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    .line 429
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object v2

    check-cast v0, Lcom/squareup/ui/login/OperationType$EnrollTwoFactor;

    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$EnrollTwoFactor;->getRequest()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint$DefaultImpls;->enrollTwoFactor$default(Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p2

    .line 430
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p2

    .line 431
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$7;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$7;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationServiceEnd\u2026lTwoFactorCallback(it)) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 432
    :cond_a
    instance-of p2, v0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;

    if-eqz p2, :cond_b

    .line 433
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->authenticationServiceEndpoint:Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    .line 435
    check-cast v0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;

    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->getSessionToken()Lcom/squareup/account/SecretString;

    move-result-object v2

    .line 436
    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v3

    .line 437
    invoke-virtual {v0}, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->getThrottle()Z

    move-result v4

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 434
    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint$DefaultImpls;->sendVerificationCode$default(Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;ZLjava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p2

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p2

    .line 440
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$8;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationServiceEnd\u2026          )\n            }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final goBackToStart(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1153
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "Authenticator popping back to start"

    .line 1155
    invoke-static {v3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1158
    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1159
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->resetSessionState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    .line 1162
    new-instance v8, Lcom/squareup/ui/login/AuthenticatorStack;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorStack;->getScreens()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2, v0}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/squareup/ui/login/AuthenticatorStack;-><init>(Ljava/util/List;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x17

    const/4 v11, 0x0

    move-object v4, p1

    invoke-static/range {v4 .. v11}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string v0, "goBackToStart"

    const/4 v2, 0x4

    .line 1160
    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final multiUnitSingleMerchantEmployeeLoginFlow(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    .line 1134
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    invoke-static {p3}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->allSameMerchant(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    if-eqz v2, :cond_3

    .line 1137
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/register/api/Unit;

    iget-object p3, p3, Lcom/squareup/protos/register/api/Unit;->merchant_token:Ljava/lang/String;

    const-string v1, "unitList[0].merchant_token"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {v0, p3, v2, v1, v2}, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;-><init>(Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p3

    .line 1139
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorInput;->getUnitSelectionFlow()Lcom/squareup/ui/login/UnitSelectionFlow;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    const-string v0, "startEmpLoginFlow singlemerchant"

    if-eqz p2, :cond_2

    const/4 p1, 0x4

    .line 1140
    invoke-static {v0, p3, v2, p1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1146
    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorState;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-static/range {v6 .. v11}, Lcom/squareup/ui/login/Operation;->copy$default(Lcom/squareup/ui/login/Operation;Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILjava/lang/Object;)Lcom/squareup/ui/login/Operation;

    move-result-object v6

    const/16 v7, 0xf

    move-object v1, p1

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p2

    .line 1147
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;

    new-instance v2, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithMerchant;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object p1

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithMerchant;-><init>(Lcom/squareup/account/SecretString;)V

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    invoke-direct {v1, v2, p3}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;-><init>(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)V

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorOutput;

    .line 1144
    invoke-static {v0, p2, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1

    .line 1134
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "There should be more than one unit to select, and all units should belong to the same merchant."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final promptToVerifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 11

    .line 669
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;->NONE:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    .line 670
    sget-object v1, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object v1

    .line 671
    sget-object v2, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;->LOGIN:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    if-ne p3, v2, :cond_0

    .line 673
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;->AUTO:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    .line 674
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object v1

    .line 675
    new-instance v2, Lcom/squareup/ui/login/Operation;

    const/4 v3, 0x0

    .line 677
    new-instance v4, Lcom/squareup/ui/login/OperationType$SendVerificationCode;

    const/4 v6, 0x0

    invoke-direct {v4, v1, p2, v6}, Lcom/squareup/ui/login/OperationType$SendVerificationCode;-><init>(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Z)V

    check-cast v4, Lcom/squareup/ui/login/OperationType;

    .line 675
    invoke-direct {v2, v3, v4, v6}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V

    move-object v8, v0

    move-object v1, v2

    goto :goto_0

    :cond_0
    move-object v8, v0

    .line 684
    :goto_0
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    const/4 v6, 0x1

    const/4 v2, 0x0

    const/16 v9, 0x8

    const/4 v10, 0x0

    move-object v3, v0

    move-object v4, p2

    move-object v5, p3

    move v7, v2

    invoke-direct/range {v3 .. v10}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 683
    invoke-static {p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    .line 690
    invoke-static {v0, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    return-object v0
.end method

.method private final providedDeviceCodeAuthStartState(Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 8

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->supportsDeviceCodeLogin:Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    invoke-interface {v0}, Lcom/squareup/ui/login/SupportsDeviceCodeLogin;->getEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    new-instance v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/LoginRequest$Builder;-><init>()V

    .line 300
    invoke-virtual {v0, p1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->device_code(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object p1

    .line 301
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->build()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object p1

    .line 303
    invoke-static {}, Lcom/squareup/ui/login/RealAuthenticatorKt;->getSTART_STATE()Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v4, Lcom/squareup/ui/login/AuthenticatorStack;

    sget-object v5, Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/squareup/ui/login/AuthenticatorStack;-><init>(Ljava/util/List;)V

    const/4 v5, 0x0

    const/16 v6, 0x17

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    .line 304
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/RealAuthenticator;->resetSessionState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    .line 305
    new-instance v7, Lcom/squareup/ui/login/Operation;

    sget v1, Lcom/squareup/common/authenticator/impl/R$string;->sign_in_signing_in:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v1, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;

    const-string v3, "loginRequest"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;-><init>(Lcom/squareup/protos/register/api/LoginRequest;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/login/OperationType;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {v0, v7}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1

    .line 295
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Device code login not supported, but device code login start state requested."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final resendSmsCode(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 15

    .line 694
    sget-object v0, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1782
    invoke-static {v2, v1, v2}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1785
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v3

    .line 1787
    invoke-virtual {v3}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v4

    instance-of v5, v4, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    if-nez v5, :cond_0

    move-object v4, v2

    :cond_0
    check-cast v4, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    check-cast v4, Lcom/squareup/ui/login/AuthenticatorScreen;

    if-eqz v4, :cond_4

    .line 1792
    move-object v5, v4

    check-cast v5, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    .line 698
    invoke-virtual {v5}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getRequestedVerificationCodeStatus()Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    move-result-object v6

    .line 699
    sget-object v7, Lcom/squareup/ui/login/RealAuthenticator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v6}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;->ordinal()I

    move-result v8

    aget v7, v7, v8

    const/4 v13, 0x0

    const/4 v14, 0x2

    if-eq v7, v1, :cond_3

    if-eq v7, v14, :cond_2

    const/4 v2, 0x3

    if-eq v7, v2, :cond_1

    move-object v10, v6

    goto :goto_0

    .line 713
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Resending a verification code should be disabled while a manual request is in flight."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 712
    :cond_2
    sget-object v2, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;->MANUAL:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    move-object v10, v2

    goto :goto_0

    .line 701
    :cond_3
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;->MANUAL:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    .line 702
    new-instance v6, Lcom/squareup/ui/login/Operation;

    .line 704
    new-instance v7, Lcom/squareup/ui/login/OperationType$SendVerificationCode;

    .line 705
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object v8

    .line 706
    invoke-virtual {v5}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getSmsTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v9

    .line 704
    invoke-direct {v7, v8, v9, v1}, Lcom/squareup/ui/login/OperationType$SendVerificationCode;-><init>(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Z)V

    check-cast v7, Lcom/squareup/ui/login/OperationType;

    .line 702
    invoke-direct {v6, v2, v7, v13}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V

    move-object v10, v0

    move-object v0, v6

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0xb

    const/4 v12, 0x0

    .line 718
    invoke-static/range {v5 .. v12}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen;

    new-array v5, v14, [Ljava/lang/Object;

    aput-object v4, v5, v13

    aput-object v2, v5, v1

    const-string v1, "Authenticator updating screen: %s -> %s"

    .line 1794
    invoke-static {v1, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 1796
    invoke-virtual {v3}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x17

    const/4 v13, 0x0

    move-object/from16 v6, p1

    invoke-static/range {v6 .. v13}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v1

    .line 722
    invoke-static {v1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    return-object v0

    .line 1788
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 1789
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected to be on screen "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v4, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", was "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1790
    invoke-virtual {v3}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    :cond_5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1788
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final resetSessionState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 9

    .line 774
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->persistentAccountCache:Lcom/squareup/account/accountservice/AppAccountCache;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/account/accountservice/AppAccountCache$DefaultImpls;->clearCache$default(Lcom/squareup/account/accountservice/AppAccountCache;Lcom/squareup/account/accountservice/ClearCacheReason;ILjava/lang/Object;)V

    .line 777
    sget-object v0, Lcom/squareup/account/SecretString;->Companion:Lcom/squareup/account/SecretString$Companion;

    invoke-virtual {v0}, Lcom/squareup/account/SecretString$Companion;->getEMPTY()Lcom/squareup/account/SecretString;

    move-result-object v3

    .line 778
    sget-object v4, Lcom/squareup/ui/login/UnitsAndMerchants;->EMPTY:Lcom/squareup/ui/login/UnitsAndMerchants;

    .line 779
    sget-object v0, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object v6

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x9

    const/4 v8, 0x0

    move-object v1, p1

    .line 776
    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method private final retryResetPassword(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 451
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 452
    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 455
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    if-eqz v1, :cond_3

    .line 458
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    invoke-static {p1, p2, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$goBackFrom(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p2

    .line 460
    invoke-static {p2, p1}, Lcom/squareup/workflow/WorkflowActionKt;->applyTo(Lcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorOutput;

    if-nez p1, :cond_2

    .line 466
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    .line 467
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;->getEmailForPrefill()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 471
    invoke-static {v1, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$resetPassword(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const/4 p2, 0x4

    const-string v1, "resetPassword"

    invoke-static {v1, p1, v0, p2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 467
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Expected ForgotPassword screen to have an email address."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 466
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.login.AuthenticatorScreen.ForgotPassword"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return-object p2

    .line 456
    :cond_3
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected to be on ForgotPasswordFailed screen, was on "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 455
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method private final showForgotPasswordToastThenGoBack(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 900
    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 901
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->toastFactory:Lcom/squareup/util/ToastFactory;

    invoke-interface {v0, p1, v1}, Lcom/squareup/util/ToastFactory;->showText(Ljava/lang/CharSequence;I)V

    .line 904
    :cond_1
    new-instance p1, Lcom/squareup/ui/login/RealAuthenticator$showForgotPasswordToastThenGoBack$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/login/RealAuthenticator$showForgotPasswordToastThenGoBack$1;-><init>(Ljava/lang/String;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x2

    const/4 v0, 0x0

    const-string v1, "resetPassword: success"

    invoke-static {v1, v0, p1, p2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final singleUnitEmployeeLoginFlow(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;I)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;I)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    .line 1068
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p3, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    .line 1070
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorInput;->getUnitSelectionFlow()Lcom/squareup/ui/login/UnitSelectionFlow;

    move-result-object p2

    .line 1071
    instance-of p3, p2, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    if-eqz p3, :cond_2

    const/4 p2, -0x1

    if-eq p4, p2, :cond_1

    .line 1084
    new-instance p2, Lcom/squareup/ui/login/Operation;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object p3, Lcom/squareup/ui/login/OperationType$None;->INSTANCE:Lcom/squareup/ui/login/OperationType$None;

    move-object v2, p3

    check-cast v2, Lcom/squareup/ui/login/OperationType;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p2

    goto :goto_1

    :cond_1
    move-object p2, p1

    .line 1088
    :goto_1
    new-instance p3, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object p1

    sget-object p4, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;->EMAIL_PASSWORD:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;

    invoke-direct {p3, p1, p4}, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;-><init>(Lcom/squareup/account/SecretString;Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;)V

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorOutput;

    const-string p1, "startEmpLoginFlow singleunit"

    .line 1086
    invoke-static {p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 1098
    :cond_2
    instance-of p2, p2, Lcom/squareup/ui/login/UnitSelectionFlow$Pause;

    if-eqz p2, :cond_3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1101
    new-instance v7, Lcom/squareup/ui/login/Operation;

    .line 1103
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/login/Operation;->getType()Lcom/squareup/ui/login/OperationType;

    move-result-object p2

    const/4 p3, 0x0

    .line 1101
    invoke-direct {v7, p3, p2, v1}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V

    const/16 v8, 0xf

    const/4 v9, 0x0

    move-object v2, p1

    .line 1100
    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p2

    .line 1107
    new-instance p4, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;

    .line 1108
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithMerchant;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithMerchant;-><init>(Lcom/squareup/account/SecretString;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    .line 1110
    new-instance v7, Lcom/squareup/ui/login/Operation;

    .line 1112
    sget-object v2, Lcom/squareup/ui/login/OperationType$ResumeAutomaticUnitSelection;->INSTANCE:Lcom/squareup/ui/login/OperationType$ResumeAutomaticUnitSelection;

    check-cast v2, Lcom/squareup/ui/login/OperationType;

    .line 1110
    invoke-direct {v7, p3, v2, v1}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V

    move-object v2, p1

    .line 1109
    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    .line 1107
    invoke-direct {p4, v0, p1}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;-><init>(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)V

    check-cast p4, Lcom/squareup/ui/login/AuthenticatorOutput;

    const-string p1, "pauseAutomaticUnitSelection singleunit"

    .line 1098
    invoke-static {p1, p2, p4}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 1068
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "There should only be a single unit available to select. "

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final skipTwoFactorEnrollment(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 563
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 564
    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 566
    invoke-static {p1}, Lcom/squareup/ui/login/AuthenticatorStateKt;->getCanSkipEnroll(Lcom/squareup/ui/login/AuthenticatorState;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object v0

    .line 572
    invoke-virtual {v0}, Lcom/squareup/ui/login/UnitsAndMerchants;->getUnits()Ljava/util/List;

    move-result-object v0

    .line 573
    sget v1, Lcom/squareup/common/authenticator/impl/R$string;->two_factor_spinner_title_signing_in:I

    .line 570
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/squareup/ui/login/RealAuthenticator;->startEmployeeLoginFlow$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;I)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 567
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Expected runnerDetails.canSkipEnroll to be true."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static synthetic startEmployeeLoginFlow$impl_release$default(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;IILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p4, -0x1

    .line 1044
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/login/RealAuthenticator;->startEmployeeLoginFlow$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;I)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final startTwoFactorFlow(Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;ZZZ)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;ZZZ)",
            "Lcom/squareup/ui/login/AuthenticatorState;"
        }
    .end annotation

    .line 645
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    invoke-static {p1, p3, p4, p5}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$startTwoFactorEnrollment(Lcom/squareup/ui/login/AuthenticatorState;ZZZ)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1

    .line 650
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 651
    iget-object p5, p4, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    if-eqz p5, :cond_1

    .line 652
    new-instance p3, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    sget-object p5, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;->LOGIN:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-direct {p3, p2, p4, p5}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;-><init>(Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)V

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1

    .line 657
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p3

    const/4 p4, 0x1

    if-ne p3, p4, :cond_3

    const/4 p3, 0x0

    .line 659
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    sget-object p3, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;->LOGIN:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator;->promptToVerifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    goto :goto_0

    .line 661
    :cond_3
    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->promptToPickSmsNumber(Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final switchLoginPromptToDeviceCode(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 4

    .line 475
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->resetSessionState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    .line 476
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method private final switchVerificationPromptToSms(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 9

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 578
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 579
    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 581
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v2

    instance-of v3, v2, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    if-nez v3, :cond_0

    move-object v2, v1

    :cond_0
    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    if-eqz v2, :cond_7

    .line 586
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->getAllTwoFactorDetails()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 1773
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, 0x0

    move-object v6, v1

    const/4 v5, 0x0

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 1774
    move-object v8, v7

    check-cast v8, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 587
    iget-object v8, v8, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    if-eqz v8, :cond_2

    const/4 v8, 0x1

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    :goto_1
    if-eqz v8, :cond_1

    if-eqz v5, :cond_3

    goto :goto_2

    :cond_3
    move-object v6, v7

    const/4 v5, 0x1

    goto :goto_0

    :cond_4
    if-nez v5, :cond_5

    goto :goto_2

    :cond_5
    move-object v1, v6

    .line 586
    :goto_2
    check-cast v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-eqz v1, :cond_6

    .line 591
    sget-object v0, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;->LOGIN:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/ui/login/RealAuthenticator;->promptToVerifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    goto :goto_3

    .line 594
    :cond_6
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->getAllTwoFactorDetails()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->promptToPickSmsNumber(Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    :goto_3
    return-object p1

    .line 582
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 583
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected to be in VerifyCodeGoogleAuth state, but is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 582
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final toStartState(Lcom/squareup/ui/login/AuthenticatorInput;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 3

    .line 259
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorInput;->getUnitSelectionFlow()Lcom/squareup/ui/login/UnitSelectionFlow;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/ui/login/UnitSelectionFlow$Go;->getResumeState()Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v2

    :cond_1
    if-eqz v2, :cond_2

    goto :goto_0

    .line 260
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorInput;->getLaunchMode()Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    move-result-object v0

    .line 261
    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorLaunchMode$ProvidedDeviceCodeAuthOnly;

    if-eqz v1, :cond_3

    .line 262
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorInput;->getLaunchMode()Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorLaunchMode$ProvidedDeviceCodeAuthOnly;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorLaunchMode$ProvidedDeviceCodeAuthOnly;->getDeviceCode()Lcom/squareup/account/SecretString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/account/SecretString;->getSecretValue()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->providedDeviceCodeAuthStartState(Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v2

    goto :goto_0

    .line 263
    :cond_3
    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorLaunchMode$DeviceCodeEntry;

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/squareup/ui/login/RealAuthenticator;->deviceCodeStartState()Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v2

    goto :goto_0

    .line 264
    :cond_4
    instance-of v0, v0, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;

    if-eqz v0, :cond_5

    .line 265
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorInput;->getLaunchMode()Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;->getAsLandingScreen()Z

    move-result p1

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->supportsDeviceCodeLogin:Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    invoke-interface {v0}, Lcom/squareup/ui/login/SupportsDeviceCodeLogin;->getEnabled()Z

    move-result v0

    .line 264
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/login/RealAuthenticator;->defaultStartState(ZZ)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final updatePassword(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/account/SecretString;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 12

    .line 310
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1752
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1753
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1754
    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 311
    instance-of v3, v2, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    if-eqz v3, :cond_0

    move-object v4, v2

    check-cast v4, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0xf

    const/4 v11, 0x0

    move-object v9, p2

    invoke-static/range {v4 .. v11}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen;

    :cond_0
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1755
    :cond_1
    check-cast v1, Ljava/util/List;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 313
    new-instance v6, Lcom/squareup/ui/login/AuthenticatorStack;

    invoke-direct {v6, v1}, Lcom/squareup/ui/login/AuthenticatorStack;-><init>(Ljava/util/List;)V

    const/4 v7, 0x0

    const/16 v8, 0x17

    const/4 v9, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final deviceCodeLoginCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/protos/register/api/LoginRequest;",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$deviceCodeLoginCallback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 924
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 927
    instance-of v1, p3, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    const/4 v2, 0x4

    if-eqz v1, :cond_2

    .line 928
    move-object v1, p3

    check-cast v1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v3, v3, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    goto :goto_0

    .line 929
    :cond_0
    sget-object v3, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_CAPTCHA_REQUIRED:Ljava/lang/Boolean;

    :goto_0
    const-string v4, "requiresCaptcha"

    .line 931
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 934
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object p3, p3, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    const-string v1, "response.response.captcha_api_key"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator;->verifyLoginByCaptcha$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "verifyLoginByCaptcha"

    .line 932
    invoke-static {p2, p1, v0, v2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 939
    :cond_1
    sget-object p2, Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$1;

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 940
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;

    .line 941
    new-instance v2, Lcom/squareup/account/SecretString;

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v1, v1, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    const-string v3, "response.response.session_token"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1}, Lcom/squareup/account/SecretString;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;->DEVICE_CODE:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;

    .line 940
    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;-><init>(Lcom/squareup/account/SecretString;Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorOutput;

    .line 942
    new-instance v1, Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$2;

    invoke-direct {v1, p1, p3}, Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$2;-><init>(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 938
    invoke-static {p2, v0, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState(Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1

    .line 948
    :cond_2
    instance-of p2, p3, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    if-eqz p2, :cond_4

    .line 952
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;

    if-eqz p2, :cond_3

    .line 955
    new-instance p2, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    sget-object p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ProvidedDeviceCodeAuthFailedText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ProvidedDeviceCodeAuthFailedText;

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    invoke-direct {p2, p3}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast p2, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "show deviceCodeAuth failure"

    .line 953
    invoke-static {p2, p1, v0, v2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 958
    :cond_3
    check-cast p3, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    invoke-static {p1, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "show deviceCodeLogin failure"

    invoke-static {p2, p1, v0, v2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    .line 952
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final emailLoginCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Lcom/squareup/ui/login/AuthenticatorInput;)Lkotlin/jvm/functions/Function1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/protos/register/api/LoginRequest;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            ")",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;>;"
        }
    .end annotation

    const-string v0, "$this$emailLoginCallback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 807
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 809
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Lcom/squareup/ui/login/AuthenticatorInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final handleEvent$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$handleEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 321
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithEmail;

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 322
    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithEmail;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithEmail;->getEmail()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithEmail;->getPassword()Lcom/squareup/account/SecretString;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/account/SecretString;->getSecretValue()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator;->loginWithEmail$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "loginWithEmail"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 323
    :cond_0
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$PasswordTextChanged;

    if-eqz v0, :cond_1

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$PasswordTextChanged;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$PasswordTextChanged;->getPassword()Lcom/squareup/account/SecretString;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator;->updatePassword(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/account/SecretString;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "updatePassword"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 324
    :cond_1
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;

    if-eqz v0, :cond_2

    .line 325
    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;->getDeviceCode()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->loginWithDeviceCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "loginWithDeviceCode"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 326
    :cond_2
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$CreateAccount;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$CreateAccount;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object p2, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$CreateAccount;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$CreateAccount;

    const-string p3, "CreateAccount event"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 327
    :cond_3
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToResetPassword;

    if-eqz v0, :cond_4

    .line 328
    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToResetPassword;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToResetPassword;->getEmailForPrefill()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator;->promptToResetPassword$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "promptToResetPassword"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 329
    :cond_4
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$ResetPassword;

    if-eqz v0, :cond_5

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$ResetPassword;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$ResetPassword;->getEmail()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$resetPassword(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "resetPassword"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 330
    :cond_5
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$RetryResetPassword;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$RetryResetPassword;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator;->retryResetPassword(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 331
    :cond_6
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$SwitchLoginPromptToDeviceCode;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$SwitchLoginPromptToDeviceCode;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 332
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->switchLoginPromptToDeviceCode(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "switchLoginPromptToDeviceCode"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 333
    :cond_7
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$SkipTwoFactorEnrollment;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$SkipTwoFactorEnrollment;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator;->skipTwoFactorEnrollment(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 334
    :cond_8
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInGoogleAuth;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInGoogleAuth;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$enrollInGoogleAuth(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "enrollInGoogleAuth"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 335
    :cond_9
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInSms;

    if-eqz v0, :cond_a

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInSms;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInSms;->getPhoneNumber()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$enrollInSms(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "enrollInSms"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 336
    :cond_a
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToEnrollSms;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$PromptToEnrollSms;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$promptToEnrollSms(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "promptToEnrollSms"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 337
    :cond_b
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifyGoogleAuthCode;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifyGoogleAuthCode;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 338
    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->promptToVerifyGoogleAuthCode(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "promptToVerifyGoogleAuthCode"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 339
    :cond_c
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$ResendSmsCode;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$ResendSmsCode;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->resendSmsCode(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "resendSmsCode"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 340
    :cond_d
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$SwitchVerificationPromptToSms;

    if-eqz v0, :cond_e

    .line 341
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->switchVerificationPromptToSms(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "switchVerificationPromptToSms"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 342
    :cond_e
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$VerifyGoogleAuthCode;

    if-eqz v0, :cond_f

    .line 343
    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$VerifyGoogleAuthCode;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$VerifyGoogleAuthCode;->getCode()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$VerifyGoogleAuthCode;->getRememberDevice()Z

    move-result p3

    invoke-static {p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$verifyGoogleAuthCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Z)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "verifyGoogleAuthCode"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 344
    :cond_f
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$VerifySmsCode;

    if-eqz v0, :cond_10

    .line 345
    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$VerifySmsCode;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$VerifySmsCode;->getCode()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$VerifySmsCode;->getRememberDevice()Z

    move-result p3

    invoke-static {p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$verifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Z)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "verifySmsCode"

    .line 344
    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 347
    :cond_10
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$SelectMerchant;

    if-eqz v0, :cond_11

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$SelectMerchant;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$SelectMerchant;->getMerchantToken()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->selectMerchant(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 348
    :cond_11
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$SelectUnit;

    if-eqz v0, :cond_12

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$SelectUnit;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$SelectUnit;->getUnitToken()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->selectUnit(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "selectUnit"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 349
    :cond_12
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$ShowGoogleAuthCodeInsteadOfQr;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$ShowGoogleAuthCodeInsteadOfQr;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 350
    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$showGoogleAuthCodeInsteadOfQr(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "showGoogleAuthCodeInsteadOfQr"

    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 351
    :cond_13
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifySmsCode;

    if-eqz v0, :cond_14

    .line 354
    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifySmsCode;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifySmsCode;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p2

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifySmsCode;->getLogin()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator;->promptToVerifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "promptToVerifySmsCode"

    .line 352
    invoke-static {p2, p1, v2, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 356
    :cond_14
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;

    if-eqz v0, :cond_15

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;->getScreen()Ljava/lang/Class;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$goBackFrom(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 357
    :cond_15
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorEvent$WarningDialogDismissed;

    if-eqz v0, :cond_18

    .line 358
    check-cast p3, Lcom/squareup/ui/login/AuthenticatorEvent$WarningDialogDismissed;

    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorEvent$WarningDialogDismissed;->getWarning()Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    move-result-object p3

    .line 359
    instance-of v0, p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ProvidedDeviceCodeAuthFailedText;

    if-eqz v0, :cond_16

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object p2, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$Canceled;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$Canceled;

    const-string p3, "deviceCodeAuthCanceled"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 360
    :cond_16
    instance-of p3, p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$SessionExpired;

    if-eqz p3, :cond_17

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->goBackToStart(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 361
    :cond_17
    const-class p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    invoke-static {p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$goBackFrom(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 358
    :cond_18
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public initialState(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 182
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 1722
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p2}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p2

    check-cast p2, Lokio/BufferedSource;

    .line 182
    invoke-static {p2}, Lcom/squareup/ui/login/AuthenticatorStateSerializationKt;->readAuthenticatorState(Lokio/BufferedSource;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->toStartState(Lcom/squareup/ui/login/AuthenticatorInput;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p2

    :goto_0
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator;->initialState(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method public final loginWithCaptcha$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest$Builder;Lcom/squareup/safetynetrecaptcha/CaptchaResult$Success;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 6

    const-string v0, "$this$loginWithCaptcha"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestTemplate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "captcha"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 548
    invoke-virtual {p3}, Lcom/squareup/safetynetrecaptcha/CaptchaResult$Success;->getToken()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->captcha_response(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object p2

    .line 549
    invoke-virtual {p2}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->build()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object p2

    .line 551
    iget-object p3, p2, Lcom/squareup/protos/register/api/LoginRequest;->device_code:Ljava/lang/String;

    const-string v0, "loginRequest"

    if-eqz p3, :cond_0

    .line 552
    new-instance p3, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p3, p2}, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;-><init>(Lcom/squareup/protos/register/api/LoginRequest;)V

    check-cast p3, Lcom/squareup/ui/login/OperationType;

    goto :goto_0

    .line 554
    :cond_0
    new-instance p3, Lcom/squareup/ui/login/OperationType$LoginWithEmail;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p3, p2}, Lcom/squareup/ui/login/OperationType$LoginWithEmail;-><init>(Lcom/squareup/protos/register/api/LoginRequest;)V

    check-cast p3, Lcom/squareup/ui/login/OperationType;

    :goto_0
    move-object v2, p3

    .line 558
    new-instance p2, Lcom/squareup/ui/login/Operation;

    sget p3, Lcom/squareup/common/authenticator/impl/R$string;->sign_in_signing_in:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 557
    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method public final loginWithEmail$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 18

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    const-string v0, "$this$loginWithEmail"

    move-object/from16 v10, p1

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "email"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "password"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v15, 0x1

    const/4 v14, 0x0

    .line 492
    invoke-static {v14, v15, v14}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 493
    invoke-static/range {p1 .. p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1756
    invoke-static {v14, v15, v14}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1759
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v11

    .line 1761
    invoke-virtual {v11}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    if-nez v1, :cond_0

    move-object v0, v14

    :cond_0
    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-object v12, v0

    check-cast v12, Lcom/squareup/ui/login/AuthenticatorScreen;

    if-eqz v12, :cond_1

    .line 1766
    move-object v0, v12

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1b

    const/4 v7, 0x0

    move-object/from16 v3, p2

    .line 496
    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v12, v1, v2

    aput-object v0, v1, v15

    const-string v2, "Authenticator updating screen: %s -> %s"

    .line 1768
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 1770
    invoke-virtual {v11}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    const/4 v2, 0x0

    const/16 v16, 0x17

    const/16 v17, 0x0

    move-object/from16 v10, p1

    move v11, v1

    move-object v1, v14

    move-object v14, v0

    const/4 v0, 0x1

    move-object v15, v2

    invoke-static/range {v10 .. v17}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v2

    .line 498
    new-instance v3, Lcom/squareup/protos/register/api/LoginRequest$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/register/api/LoginRequest$Builder;-><init>()V

    .line 499
    invoke-virtual {v3, v8}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->email(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object v3

    .line 500
    invoke-virtual {v3, v9}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->password(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object v3

    move-object/from16 v4, p0

    .line 502
    iget-object v5, v4, Lcom/squareup/ui/login/RealAuthenticator;->trustedDeviceDetailsStore:Lcom/squareup/ui/login/TrustedDeviceDetailsStore;

    const-wide/16 v6, 0x0

    invoke-static {v5, v6, v7, v0, v1}, Lcom/squareup/ui/login/TrustedDeviceDetailsStoreKt;->removeExpiredDeviceDetails$default(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;JILjava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 505
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast v1, Ljava/util/List;

    invoke-virtual {v3, v1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->trusted_device_details(Ljava/util/List;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    .line 508
    new-instance v0, Lcom/squareup/ui/login/Operation;

    sget v1, Lcom/squareup/common/authenticator/impl/R$string;->sign_in_signing_in:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v1, Lcom/squareup/ui/login/OperationType$LoginWithEmail;

    invoke-virtual {v3}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->build()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object v3

    const-string v5, "loginRequestBuilder.build()"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Lcom/squareup/ui/login/OperationType$LoginWithEmail;-><init>(Lcom/squareup/protos/register/api/LoginRequest;)V

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/login/OperationType;

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v5, v0

    invoke-direct/range {v5 .. v10}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 507
    invoke-static {v2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    return-object v0

    :cond_1
    move-object/from16 v4, p0

    move-object v1, v14

    .line 1762
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 1763
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected to be on screen "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v3, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1764
    invoke-virtual {v11}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v14

    goto :goto_0

    :cond_2
    move-object v14, v1

    :goto_0
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1762
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onPropsChanged(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 11

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;-><init>(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 197
    new-instance v1, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;

    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;-><init>(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorInput;)V

    .line 202
    invoke-virtual {v0}, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->invoke()Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 p1, 0x1

    const/4 v0, 0x0

    .line 1723
    invoke-static {v0, p1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1726
    invoke-virtual {p3}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    .line 1728
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v2

    instance-of v3, v2, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    if-nez v3, :cond_0

    move-object v2, v0

    :cond_0
    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen;

    if-eqz v2, :cond_2

    .line 1733
    move-object v3, v2

    check-cast v3, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    .line 204
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorInput;->getLaunchMode()Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    move-result-object p2

    if-eqz p2, :cond_1

    check-cast p2, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;->getAsLandingScreen()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/login/AuthenticatorScreen;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    aput-object p2, v0, p1

    const-string p1, "Authenticator updating screen: %s -> %s"

    .line 1735
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1737
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x17

    const/4 v9, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p3

    goto :goto_0

    .line 204
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.login.AuthenticatorLaunchMode.Start"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1729
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 1730
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Expected to be on screen "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class p3, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    invoke-virtual {p3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", was "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1731
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object p3

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    :cond_3
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1729
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 207
    :cond_4
    invoke-virtual {v1, p3}, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;->invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    if-eqz p1, :cond_5

    move-object p3, p1

    :cond_5
    :goto_0
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorInput;

    check-cast p2, Lcom/squareup/ui/login/AuthenticatorInput;

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator;->onPropsChanged(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method public final promptToResetPassword$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 4

    const-string v0, "$this$promptToResetPassword"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->resetSessionState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    .line 369
    invoke-static {p2}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 370
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorStack;->getScreens()Ljava/util/List;

    move-result-object p2

    .line 371
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->asReversed(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 372
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 373
    sget-object v0, Lcom/squareup/ui/login/RealAuthenticator$promptToResetPassword$actualEmailForPrefill$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$promptToResetPassword$actualEmailForPrefill$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v0}, Lkotlin/sequences/SequencesKt;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 374
    invoke-static {p2}, Lkotlin/sequences/SequencesKt;->firstOrNull(Lkotlin/sequences/Sequence;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 376
    :goto_0
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, p2, v1, v2, v3}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;-><init>(Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 377
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    if-eqz p2, :cond_1

    .line 378
    const-class p2, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, p2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$replaceScreen(Lcom/squareup/ui/login/AuthenticatorState;Lkotlin/reflect/KClass;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    goto :goto_1

    .line 380
    :cond_1
    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public render(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/login/AuthenticatorRendering;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorRendering;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-static {p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->populateScreenPropertiesFromState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    .line 220
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->sessionExpiredBusEvents:Lio/reactivex/Observable;

    const-string v2, "sessionExpiredBusEvents"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1738
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v1

    const-string v2, "this.toFlowable(BUFFER)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lorg/reactivestreams/Publisher;

    if-eqz v1, :cond_2

    .line 1740
    invoke-static {v1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 1741
    const-class v2, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 221
    sget-object v1, Lcom/squareup/ui/login/RealAuthenticator$render$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$render$1;

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    .line 219
    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 225
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator;->countryGuesser:Lcom/squareup/location/CountryGuesser;

    invoke-interface {v1}, Lcom/squareup/location/CountryGuesser;->getCountryGuess()Lio/reactivex/Single;

    move-result-object v1

    .line 1742
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/ui/login/RealAuthenticator$render$$inlined$asWorker$1;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/squareup/ui/login/RealAuthenticator$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 1743
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 1744
    const-class v2, Lcom/squareup/location/CountryGuesser$Result;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v6, v4

    check-cast v6, Lcom/squareup/workflow/Worker;

    const/4 v7, 0x0

    .line 225
    new-instance v1, Lcom/squareup/ui/login/RealAuthenticator$render$2;

    invoke-direct {v1, v0}, Lcom/squareup/ui/login/RealAuthenticator$render$2;-><init>(Lcom/squareup/ui/login/AuthenticatorState;)V

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v5, p3

    invoke-static/range {v5 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 236
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->getObservableForCall(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)Lio/reactivex/Single;

    move-result-object v1

    .line 237
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorState;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/login/Operation;->getType()Lcom/squareup/ui/login/OperationType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/login/OperationType;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1745
    sget-object v4, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v4, Lcom/squareup/ui/login/RealAuthenticator$render$$inlined$asWorker$2;

    invoke-direct {v4, v1, v3}, Lcom/squareup/ui/login/RealAuthenticator$render$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 1746
    invoke-static {v4}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 1747
    const-class v3, Lcom/squareup/workflow/WorkflowAction;

    sget-object v4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v5, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v4

    sget-object v5, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v6, Lcom/squareup/ui/login/AuthenticatorOutput;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 238
    sget-object v1, Lcom/squareup/ui/login/RealAuthenticator$render$3;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$render$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4, v2, v1}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 242
    invoke-static {p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$isLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 244
    sget-object p1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    goto :goto_0

    .line 246
    :cond_0
    new-instance p2, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;

    invoke-direct {p2, p0, v0, p1}, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;-><init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p3, p2}, Lcom/squareup/workflow/legacyintegration/RenderContextsKt;->makeLegacyWorkflowInput(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 251
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 1748
    new-instance p3, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p3, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p3, Ljava/util/Collection;

    .line 1749
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1750
    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 251
    invoke-static {v1, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$toScreen(Lcom/squareup/ui/login/AuthenticatorScreen;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1751
    :cond_1
    check-cast p3, Ljava/util/List;

    .line 252
    new-instance p2, Lcom/squareup/ui/login/AuthenticatorRendering;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorState;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object v0

    invoke-direct {p2, p3, v0, p1}, Lcom/squareup/ui/login/AuthenticatorRendering;-><init>(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p2

    .line 1740
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorInput;

    check-cast p2, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator;->render(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/login/AuthenticatorRendering;

    move-result-object p1

    return-object p1
.end method

.method public final resetPasswordCallback$impl_release(Ljava/lang/String;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/ForgotPasswordResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "email"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "successOrFailure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 876
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 879
    instance-of v1, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_0

    .line 880
    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/server/account/ForgotPasswordResponse;

    invoke-virtual {p2}, Lcom/squareup/server/account/ForgotPasswordResponse;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/login/RealAuthenticator;->showForgotPasswordToastThenGoBack(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 883
    :cond_0
    instance-of p1, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    .line 884
    iget-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    .line 885
    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/common/authenticator/impl/R$string;->forgot_password_failed:I

    .line 886
    sget-object v2, Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$failureMessage$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 884
    invoke-virtual {p1, p2, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 887
    new-instance p2, Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$1;

    invoke-direct {p2, p1}, Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$1;-><init>(Lcom/squareup/receiving/FailureMessage;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x2

    const-string v1, "resetPassword: failure"

    invoke-static {v1, v0, p2, p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final selectUnitCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/SelectUnitResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$selectUnitCallback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 972
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 975
    instance-of v1, p2, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    if-eqz v1, :cond_0

    .line 977
    sget-object v0, Lcom/squareup/ui/login/RealAuthenticator$selectUnitCallback$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$selectUnitCallback$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 978
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;

    new-instance v2, Lcom/squareup/account/SecretString;

    move-object v3, p2

    check-cast v3, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    invoke-virtual {v3}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/register/api/SelectUnitResponse;

    iget-object v3, v3, Lcom/squareup/protos/register/api/SelectUnitResponse;->session_token:Ljava/lang/String;

    const-string v4, "response.response.session_token"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/squareup/account/SecretString;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;->EMAIL_PASSWORD:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;-><init>(Lcom/squareup/account/SecretString;Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched$AuthenticationMethod;)V

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorOutput;

    .line 979
    new-instance v2, Lcom/squareup/ui/login/RealAuthenticator$selectUnitCallback$2;

    invoke-direct {v2, p1, p2}, Lcom/squareup/ui/login/RealAuthenticator$selectUnitCallback$2;-><init>(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 976
    invoke-static {v0, v1, v2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState(Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 983
    :cond_0
    instance-of v1, p2, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    if-eqz v1, :cond_1

    check-cast p2, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const/4 p2, 0x4

    const-string v1, "show selectUnit failure"

    invoke-static {v1, p1, v0, p2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final sendVerificationCodeTwoFactorCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorState;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "$this$sendVerificationCodeTwoFactorCallback"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "response"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1000
    invoke-static {v3, v2, v3}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1004
    instance-of v4, v1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    if-eqz v4, :cond_4

    .line 1797
    invoke-static {v3, v2, v3}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1800
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    .line 1802
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v4

    instance-of v5, v4, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    if-nez v5, :cond_0

    move-object v4, v3

    :cond_0
    check-cast v4, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    check-cast v4, Lcom/squareup/ui/login/AuthenticatorScreen;

    if-eqz v4, :cond_2

    .line 1807
    move-object v5, v4

    check-cast v5, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1b

    const/4 v12, 0x0

    .line 1006
    invoke-static/range {v5 .. v12}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    move-result-object v13

    .line 1008
    invoke-virtual {v13}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getRequestedVerificationCodeStatus()Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    move-result-object v3

    sget-object v5, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;->MANUAL:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    if-ne v3, v5, :cond_1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 1011
    invoke-virtual {v13}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getShowResendSuccessToastCounter()I

    move-result v3

    add-int/lit8 v17, v3, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x17

    const/16 v20, 0x0

    .line 1010
    invoke-static/range {v13 .. v20}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    move-result-object v13

    :cond_1
    move-object v5, v13

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 1015
    sget-object v10, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;->NONE:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    const/16 v11, 0xf

    const/4 v12, 0x0

    invoke-static/range {v5 .. v12}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    move-result-object v3

    .line 1016
    check-cast v3, Lcom/squareup/ui/login/AuthenticatorScreen;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    aput-object v3, v5, v2

    const-string v2, "Authenticator updating screen: %s -> %s"

    .line 1809
    invoke-static {v2, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1811
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v6

    const/16 v8, 0x17

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move v1, v2

    move-object v2, v4

    move-object v3, v5

    move-object v4, v6

    move-object v5, v7

    move v6, v8

    move-object v7, v9

    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 1017
    sget-object v0, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object v15

    const/16 v16, 0xf

    const/16 v17, 0x0

    invoke-static/range {v10 .. v17}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    goto :goto_0

    .line 1803
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 1804
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected to be on screen "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v4, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", was "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1805
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    :cond_3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1803
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 1019
    :cond_4
    instance-of v2, v1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    if-eqz v2, :cond_5

    check-cast v1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    invoke-static {v0, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_5
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/ui/login/RealAuthenticator$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/RealAuthenticator$snapshotState$1;-><init>(Lcom/squareup/ui/login/AuthenticatorState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator;->snapshotState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public final startEmployeeLoginFlow$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;I)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;I)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$startEmployeeLoginFlow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitList"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1047
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/login/RealAuthenticator;->singleUnitEmployeeLoginFlow(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;I)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 1049
    :cond_0
    invoke-static {p3}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->allSameMerchant(Ljava/util/List;)Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator;->multiUnitSingleMerchantEmployeeLoginFlow(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 1051
    :cond_1
    new-instance p2, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;

    const/4 p3, 0x0

    invoke-direct {p2, p3, v1, p3}, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;-><init>(Lcom/squareup/ui/login/UnitsAndMerchants;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const/4 p2, 0x4

    const-string p4, "startEmpLoginFlow"

    invoke-static {p4, p1, p3, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final verifyLoginByCaptcha$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 4

    const-string v0, "$this$verifyLoginByCaptcha"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 525
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 528
    new-instance v0, Lcom/squareup/ui/login/Operation;

    .line 529
    sget v1, Lcom/squareup/common/authenticator/impl/R$string;->sign_in_signing_in:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 530
    new-instance v2, Lcom/squareup/ui/login/OperationType$VerifyLoginByCaptcha;

    invoke-virtual {p2}, Lcom/squareup/protos/register/api/LoginRequest;->newBuilder()Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object p2

    const-string v3, "request.newBuilder()"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p2, p3}, Lcom/squareup/ui/login/OperationType$VerifyLoginByCaptcha;-><init>(Lcom/squareup/protos/register/api/LoginRequest$Builder;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/ui/login/OperationType;

    const/4 p2, 0x0

    .line 528
    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V

    .line 527
    invoke-static {p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method public final verifyLoginByCaptchaCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest$Builder;Lcom/squareup/safetynetrecaptcha/CaptchaResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/protos/register/api/LoginRequest$Builder;",
            "Lcom/squareup/safetynetrecaptcha/CaptchaResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$verifyLoginByCaptchaCallback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestTemplate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 861
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 864
    instance-of v1, p3, Lcom/squareup/safetynetrecaptcha/CaptchaResult$Success;

    const/4 v2, 0x4

    if-eqz v1, :cond_0

    check-cast p3, Lcom/squareup/safetynetrecaptcha/CaptchaResult$Success;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticator;->loginWithCaptcha$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest$Builder;Lcom/squareup/safetynetrecaptcha/CaptchaResult$Success;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const-string p2, "login"

    invoke-static {p2, p1, v0, v2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 865
    :cond_0
    instance-of p2, p3, Lcom/squareup/safetynetrecaptcha/CaptchaResult$NetworkError;

    const-string v1, "show emailLoginCaptcha failure"

    if-eqz p2, :cond_1

    .line 866
    new-instance p2, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    sget-object p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$NetworkErrorText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$NetworkErrorText;

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    invoke-direct {p2, p3}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast p2, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    invoke-static {v1, p1, v0, v2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 867
    :cond_1
    instance-of p2, p3, Lcom/squareup/safetynetrecaptcha/CaptchaResult$Error;

    if-eqz p2, :cond_2

    .line 868
    new-instance p2, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    sget-object p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ServerErrorText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ServerErrorText;

    check-cast p3, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    invoke-direct {p2, p3}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast p2, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    invoke-static {v1, p1, v0, v2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final verifyTwoFactorCodeCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticationCallResult;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v3, "$this$verifyTwoFactorCodeCallback"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "input"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "response"

    invoke-static {p3, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "parseTokenAndDeviceDetails"

    invoke-static {p4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 746
    invoke-static {v4, v3, v4}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 749
    instance-of v5, p3, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    if-eqz v5, :cond_2

    .line 750
    move-object v1, p3

    check-cast v1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p4, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;

    invoke-virtual {v1}, Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;->component1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;->component2()Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    move-result-object v7

    .line 753
    move-object v1, v2

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v8, 0x0

    move-object v0, p1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move v4, v5

    move-object v5, v8

    .line 754
    invoke-static/range {v0 .. v5}, Lcom/squareup/ui/login/RealAuthenticatorKt;->withTemporarySessionData$default(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v5

    if-eqz v7, :cond_0

    .line 758
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator;->trustedDeviceDetailsStore:Lcom/squareup/ui/login/TrustedDeviceDetailsStore;

    invoke-static {v0, v7}, Lcom/squareup/ui/login/TrustedDeviceDetailsStoreKt;->update(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)V

    .line 761
    :cond_0
    invoke-virtual {v5}, Lcom/squareup/ui/login/AuthenticatorState;->getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object v0

    .line 763
    invoke-virtual {v0}, Lcom/squareup/ui/login/UnitsAndMerchants;->getUnits()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v4, p0

    move-object v6, p2

    invoke-static/range {v4 .. v10}, Lcom/squareup/ui/login/RealAuthenticator;->startEmployeeLoginFlow$impl_release$default(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;IILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 753
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing session token or server error."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 766
    :cond_2
    instance-of v3, p3, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    if-eqz v3, :cond_3

    move-object v1, p3

    check-cast v1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    invoke-static {p1, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    const/4 v1, 0x4

    const-string v3, "show verify 2fa failure"

    invoke-static {v3, v0, v4, v1, v4}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
