.class public abstract Lcom/squareup/ui/login/CreateAccountScreen$Module;
.super Ljava/lang/Object;
.source "CreateAccountScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/CreateAccountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/login/CreateAccountScreen;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountScreen$Module;->this$0:Lcom/squareup/ui/login/CreateAccountScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideCreateAccountScreenRunner(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/ui/login/CreateAccountScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
