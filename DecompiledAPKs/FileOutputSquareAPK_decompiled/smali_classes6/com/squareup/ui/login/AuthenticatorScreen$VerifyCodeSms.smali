.class public final Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;
.super Lcom/squareup/ui/login/AuthenticatorScreen;
.source "AuthenticatorScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticatorScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VerifyCodeSms"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001#B3\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u000bH\u00c6\u0003J;\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u00072\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u00d6\u0003J\t\u0010 \u001a\u00020\tH\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;",
        "Lcom/squareup/ui/login/AuthenticatorScreen;",
        "smsTwoFactorDetails",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "login",
        "Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;",
        "resendCodeLinkEnabled",
        "",
        "showResendSuccessToastCounter",
        "",
        "requestedVerificationCodeStatus",
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;",
        "(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;)V",
        "getLogin",
        "()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;",
        "getRequestedVerificationCodeStatus",
        "()Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;",
        "getResendCodeLinkEnabled",
        "()Z",
        "getShowResendSuccessToastCounter",
        "()I",
        "getSmsTwoFactorDetails",
        "()Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "RequestedVerificationCode",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

.field private final requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

.field private final resendCodeLinkEnabled:Z

.field private final showResendSuccessToastCounter:I

.field private final smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;Z)V
    .locals 8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZI)V
    .locals 8

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;)V
    .locals 1

    const-string v0, "smsTwoFactorDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "login"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestedVerificationCodeStatus"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 250
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/AuthenticatorScreen;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    iput-boolean p3, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->resendCodeLinkEnabled:Z

    iput p4, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->showResendSuccessToastCounter:I

    iput-object p5, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p7, p6, 0x8

    if-eqz p7, :cond_0

    const/4 p4, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    move v4, p4

    :goto_0
    and-int/lit8 p4, p6, 0x10

    if-eqz p4, :cond_1

    .line 249
    sget-object p5, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;->NONE:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    :cond_1
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->resendCodeLinkEnabled:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->showResendSuccessToastCounter:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->copy(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->resendCodeLinkEnabled:Z

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->showResendSuccessToastCounter:I

    return v0
.end method

.method public final component5()Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;
    .locals 7

    const-string v0, "smsTwoFactorDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "login"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestedVerificationCodeStatus"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->resendCodeLinkEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->resendCodeLinkEnabled:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->showResendSuccessToastCounter:I

    iget v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->showResendSuccessToastCounter:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    iget-object p1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLogin()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    return-object v0
.end method

.method public final getRequestedVerificationCodeStatus()Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    return-object v0
.end method

.method public final getResendCodeLinkEnabled()Z
    .locals 1

    .line 247
    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->resendCodeLinkEnabled:Z

    return v0
.end method

.method public final getShowResendSuccessToastCounter()I
    .locals 1

    .line 248
    iget v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->showResendSuccessToastCounter:I

    return v0
.end method

.method public final getSmsTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->resendCodeLinkEnabled:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->showResendSuccessToastCounter:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VerifyCodeSms(smsTwoFactorDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->smsTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", login="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", resendCodeLinkEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->resendCodeLinkEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showResendSuccessToastCounter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->showResendSuccessToastCounter:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", requestedVerificationCodeStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->requestedVerificationCodeStatus:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
