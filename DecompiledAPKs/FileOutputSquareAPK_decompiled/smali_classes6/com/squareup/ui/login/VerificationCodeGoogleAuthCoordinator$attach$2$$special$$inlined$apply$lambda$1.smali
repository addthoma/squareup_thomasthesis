.class final synthetic Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "VerificationCodeGoogleAuthCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$4$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $hasSmsTwoFactorDetails$inlined:Z

.field final synthetic $verifyCode$2$inlined:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;

.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;Lcom/squareup/workflow/legacy/WorkflowInput;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;

    iput-object p2, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;->$verifyCode$2$inlined:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;

    iput-object p3, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    iput-boolean p4, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;->$hasSmsTwoFactorDetails$inlined:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "verifyCode"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "invoke()V"

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$1;->$verifyCode$2$inlined:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;

    .line 99
    invoke-virtual {v0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;->invoke()V

    return-void
.end method
