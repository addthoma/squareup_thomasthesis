.class public Lcom/squareup/ui/login/LegalLinksDialogScreen$Factory;
.super Ljava/lang/Object;
.source "LegalLinksDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/LegalLinksDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Landroid/content/res/Resources;Lcom/squareup/ui/login/LegalLinksDialogScreen;Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 40
    iget-object p3, p1, Lcom/squareup/ui/login/LegalLinksDialogScreen;->countryCode:Lcom/squareup/CountryCode;

    invoke-static {p3}, Lcom/squareup/address/CountryResources;->legalUrlsId(Lcom/squareup/CountryCode;)I

    move-result p3

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    .line 41
    aget-object p0, p0, p4

    const-string p3, "{country_code}"

    .line 43
    invoke-virtual {p0, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 44
    invoke-static {p0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    iget-object p1, p1, Lcom/squareup/ui/login/LegalLinksDialogScreen;->countryCode:Lcom/squareup/CountryCode;

    .line 45
    invoke-virtual {p1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object p1

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string p3, "country_code"

    invoke-virtual {p0, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 46
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 47
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    .line 49
    :cond_0
    invoke-static {p2, p0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 33
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/LegalLinksDialogScreen;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 36
    iget-object v2, v0, Lcom/squareup/ui/login/LegalLinksDialogScreen;->countryCode:Lcom/squareup/CountryCode;

    invoke-static {v2}, Lcom/squareup/address/CountryResources;->legalUrlNamesId(Lcom/squareup/CountryCode;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 38
    new-instance v3, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/squareup/ui/login/-$$Lambda$LegalLinksDialogScreen$Factory$-cKzQUXDTxCEjg5zgWUPmqIIJl8;

    invoke-direct {v4, v1, v0, p1}, Lcom/squareup/ui/login/-$$Lambda$LegalLinksDialogScreen$Factory$-cKzQUXDTxCEjg5zgWUPmqIIJl8;-><init>(Landroid/content/res/Resources;Lcom/squareup/ui/login/LegalLinksDialogScreen;Landroid/content/Context;)V

    .line 39
    invoke-virtual {v3, v2, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/loggedout/R$string;->activation_legal_dialog_title:I

    .line 51
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
