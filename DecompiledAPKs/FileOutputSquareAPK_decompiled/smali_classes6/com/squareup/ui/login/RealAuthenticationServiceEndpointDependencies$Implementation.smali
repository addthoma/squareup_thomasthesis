.class public final Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;
.super Ljava/lang/Object;
.source "AuthenticationServiceEndpoint.kt"

# interfaces
.implements Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Implementation"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u00002\u00020\u0001B)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0008\u001a\u00020\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;",
        "Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;",
        "authenticationService",
        "Lcom/squareup/server/account/AuthenticationService;",
        "passwordService",
        "Lcom/squareup/server/account/PasswordService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/server/account/PasswordService;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;)V",
        "getAuthenticationService",
        "()Lcom/squareup/server/account/AuthenticationService;",
        "getMainScheduler",
        "()Lio/reactivex/Scheduler;",
        "getPasswordService",
        "()Lcom/squareup/server/account/PasswordService;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final authenticationService:Lcom/squareup/server/account/AuthenticationService;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final passwordService:Lcom/squareup/server/account/PasswordService;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/server/account/PasswordService;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticationService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passwordService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;->authenticationService:Lcom/squareup/server/account/AuthenticationService;

    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;->passwordService:Lcom/squareup/server/account/PasswordService;

    iput-object p3, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;->authenticationService:Lcom/squareup/server/account/AuthenticationService;

    return-object v0
.end method

.method public getMainScheduler()Lio/reactivex/Scheduler;
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;->mainScheduler:Lio/reactivex/Scheduler;

    return-object v0
.end method

.method public getPasswordService()Lcom/squareup/server/account/PasswordService;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;->passwordService:Lcom/squareup/server/account/PasswordService;

    return-object v0
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;->res:Lcom/squareup/util/Res;

    return-object v0
.end method
