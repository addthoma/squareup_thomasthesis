.class final Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$2;
.super Ljava/lang/Object;
.source "RealPostInstallEncryptedEmail.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->decryptEmail(Ljava/lang/String;)Lio/reactivex/Maybe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPostInstallEncryptedEmail.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPostInstallEncryptedEmail.kt\ncom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$2\n*L\n1#1,66:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "decryptEmailResponse",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$2;->this$0:Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/protos/client/multipass/DecryptEmailResponse;)Z
    .locals 1

    const-string v0, "decryptEmailResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$2;->this$0:Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;

    invoke-static {v0, p1}, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->access$isValid(Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;Lcom/squareup/protos/client/multipass/DecryptEmailResponse;)Z

    move-result p1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$2;->this$0:Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;

    invoke-static {v0, p1}, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->access$logAnalytics(Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;Z)V

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$2;->test(Lcom/squareup/protos/client/multipass/DecryptEmailResponse;)Z

    move-result p1

    return p1
.end method
