.class public final Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TwoFactorDetailsPickerCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0016\u0010\u0016\u001a\u00020\u00122\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "googleAuthButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "instructions",
        "Lcom/squareup/widgets/MessageView;",
        "smsButton",
        "subtitle",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "displayTwoFactorDetailsMethods",
        "twoFactorDetailsMethods",
        "",
        "Lcom/squareup/ui/login/TwoFactorDetailsMethod;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private googleAuthButton:Lcom/squareup/marketfont/MarketButton;

.field private instructions:Lcom/squareup/widgets/MessageView;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private smsButton:Lcom/squareup/marketfont/MarketButton;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->screenData:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$displayTwoFactorDetailsMethods(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;Ljava/util/List;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->displayTwoFactorDetailsMethods(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getGoogleAuthButton$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;)Lcom/squareup/marketfont/MarketButton;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->googleAuthButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p0, :cond_0

    const-string v0, "googleAuthButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSmsButton$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;)Lcom/squareup/marketfont/MarketButton;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->smsButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p0, :cond_0

    const-string v0, "smsButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setGoogleAuthButton$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;Lcom/squareup/marketfont/MarketButton;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->googleAuthButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method public static final synthetic access$setSmsButton$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;Lcom/squareup/marketfont/MarketButton;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->smsButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 86
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 87
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 88
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 89
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->instructions:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->instructions:Lcom/squareup/widgets/MessageView;

    .line 90
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->google_auth_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->googleAuthButton:Lcom/squareup/marketfont/MarketButton;

    .line 91
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->sms_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->smsButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final displayTwoFactorDetailsMethods(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/login/TwoFactorDetailsMethod;",
            ">;)V"
        }
    .end annotation

    .line 74
    sget-object v0, Lcom/squareup/ui/login/TwoFactorDetailsMethod;->GOOGLE_AUTH:Lcom/squareup/ui/login/TwoFactorDetailsMethod;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->googleAuthButton:Lcom/squareup/marketfont/MarketButton;

    const-string v2, "googleAuthButton"

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v3, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_details_picker_authentication_app:I

    invoke-virtual {v0, v3}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->googleAuthButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 79
    :cond_2
    sget-object v0, Lcom/squareup/ui/login/TwoFactorDetailsMethod;->SMS:Lcom/squareup/ui/login/TwoFactorDetailsMethod;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->smsButton:Lcom/squareup/marketfont/MarketButton;

    const-string v0, "smsButton"

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v2, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_details_picker_sms:I

    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->smsButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    :cond_5
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->bindViews(Landroid/view/View;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "subtitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->instructions:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_2

    const-string v1, "instructions"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_hint:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
