.class public final Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EnrollGoogleAuthCodeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0001\u001dB\'\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u000e\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u0012H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "clipboard",
        "Lcom/squareup/util/Clipboard;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Clipboard;)V",
        "accountName",
        "Lcom/squareup/widgets/MessageView;",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "authenticationKey",
        "Landroid/widget/TextView;",
        "copyCode",
        "googleAuthTwoFactorDetails",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "subtitle",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "onCopyCode",
        "setTwoFactorDetails",
        "twoFactorDetails",
        "Factory",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private accountName:Lcom/squareup/widgets/MessageView;

.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private authenticationKey:Landroid/widget/TextView;

.field private final clipboard:Lcom/squareup/util/Clipboard;

.field private copyCode:Landroid/widget/TextView;

.field private googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Clipboard;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;",
            "Lcom/squareup/util/Clipboard;",
            ")V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clipboard"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->screenData:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->clipboard:Lcom/squareup/util/Clipboard;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getAuthenticationKey$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Landroid/widget/TextView;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->authenticationKey:Landroid/widget/TextView;

    if-nez p0, :cond_0

    const-string v0, "authenticationKey"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCopyCode$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Landroid/widget/TextView;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->copyCode:Landroid/widget/TextView;

    if-nez p0, :cond_0

    const-string v0, "copyCode"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSubtitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez p0, :cond_0

    const-string v0, "subtitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getTitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez p0, :cond_0

    const-string v0, "title"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method public static final synthetic access$setAuthenticationKey$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Landroid/widget/TextView;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->authenticationKey:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic access$setCopyCode$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Landroid/widget/TextView;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->copyCode:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic access$setSubtitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Lcom/squareup/widgets/MessageView;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$setTitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Lcom/squareup/widgets/MessageView;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->title:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$setTwoFactorDetails(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->setTwoFactorDetails(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 87
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById<Action\u2026>(R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "view.findViewById<Action\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 89
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 90
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 91
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->authentication_key:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->authenticationKey:Landroid/widget/TextView;

    .line 92
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->copy_code:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->copyCode:Landroid/widget/TextView;

    .line 93
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->account_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->accountName:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final setTwoFactorDetails(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V
    .locals 3

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->authenticationKey:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const-string v0, "authenticationKey"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    const-string v1, "googleAuthTwoFactorDetails"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinatorKt;->access$formatAuthKey(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->accountName:Lcom/squareup/widgets/MessageView;

    const-string v0, "accountName"

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v2, "accountName.context"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 83
    iget-object v2, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->accountName:Lcom/squareup/widgets/MessageView;

    if-nez v2, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 80
    :cond_3
    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_google_auth_code_hint:I

    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v0, v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iget-object v0, v0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->account_name:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "account_name"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->bindViews(Landroid/view/View;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final onCopyCode(Landroid/view/View;)V
    .locals 7

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->clipboard:Lcom/squareup/util/Clipboard;

    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string p1, "view.context"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-nez p1, :cond_0

    const-string v0, "googleAuthTwoFactorDetails"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iget-object p1, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->authenticator_key_base32:Ljava/lang/String;

    const-string v0, "googleAuthTwoFactorDetai\u2026.authenticator_key_base32"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Ljava/lang/CharSequence;

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 70
    invoke-static/range {v1 .. v6}, Lcom/squareup/util/Clipboard$DefaultImpls;->copyPlainText$default(Lcom/squareup/util/Clipboard;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Object;)V

    return-void
.end method
