.class public Lcom/squareup/ui/onboarding/SilentExitScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "SilentExitScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/SilentExitScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/SilentExitScreen$Component;,
        Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/SilentExitScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/SilentExitScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ui/onboarding/SilentExitScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/SilentExitScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/SilentExitScreen;->INSTANCE:Lcom/squareup/ui/onboarding/SilentExitScreen;

    .line 23
    sget-object v0, Lcom/squareup/ui/onboarding/SilentExitScreen;->INSTANCE:Lcom/squareup/ui/onboarding/SilentExitScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/SilentExitScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 46
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_silent_exit:I

    return v0
.end method
