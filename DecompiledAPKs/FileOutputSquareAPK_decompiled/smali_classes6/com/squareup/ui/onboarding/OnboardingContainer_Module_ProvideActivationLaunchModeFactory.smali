.class public final Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;
.super Ljava/lang/Object;
.source "OnboardingContainer_Module_ProvideActivationLaunchModeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;",
        ">;"
    }
.end annotation


# instance fields
.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;->modelProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;)",
            "Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideActivationLaunchMode(Lcom/squareup/ui/onboarding/OnboardingModel;)Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer$Module;->provideActivationLaunchMode(Lcom/squareup/ui/onboarding/OnboardingModel;)Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;->provideActivationLaunchMode(Lcom/squareup/ui/onboarding/OnboardingModel;)Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideActivationLaunchModeFactory;->get()Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    move-result-object v0

    return-object v0
.end method
