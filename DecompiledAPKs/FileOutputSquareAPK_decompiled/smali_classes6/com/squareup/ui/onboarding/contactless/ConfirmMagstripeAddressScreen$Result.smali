.class Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;
.super Ljava/lang/Object;
.source "ConfirmMagstripeAddressScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Result"
.end annotation


# instance fields
.field final correction:Lcom/squareup/protos/common/location/GlobalAddress;

.field private final errorMessage:Ljava/lang/String;

.field private final errorTitle:Ljava/lang/String;

.field uncorrectable:Z


# direct methods
.method private constructor <init>(Lcom/squareup/protos/common/location/GlobalAddress;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->correction:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 296
    iput-boolean p2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->uncorrectable:Z

    .line 297
    iput-object p3, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->errorTitle:Ljava/lang/String;

    .line 298
    iput-object p4, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->errorMessage:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;)Ljava/lang/String;
    .locals 0

    .line 287
    iget-object p0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->errorTitle:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;)Ljava/lang/String;
    .locals 0

    .line 287
    iget-object p0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->errorMessage:Ljava/lang/String;

    return-object p0
.end method

.method static corrected(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;
    .locals 3

    .line 310
    new-instance v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    sget v1, Lcom/squareup/common/strings/R$string;->order_validation_modified_title:I

    .line 311
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->order_validation_modified_text:I

    .line 312
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2, v1, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static error(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;
    .locals 3

    .line 332
    new-instance v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static serviceError(Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;
    .locals 4

    .line 322
    new-instance v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    sget v1, Lcom/squareup/common/strings/R$string;->order_reader_magstripe_validation_system_error_title:I

    .line 323
    invoke-interface {p0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->order_reader_magstripe_validation_system_error_text:I

    .line 326
    invoke-interface {p0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3, v1, p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static success()Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;
    .locals 3

    .line 306
    new-instance v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v1, v1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static uncorrectable(Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;
    .locals 4

    .line 316
    new-instance v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    sget v1, Lcom/squareup/common/strings/R$string;->order_validation_uncorrected_title:I

    .line 317
    invoke-interface {p0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->order_validation_uncorrected_text:I

    .line 318
    invoke-interface {p0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3, v1, p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method isSuccessful()Z
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->correction:Lcom/squareup/protos/common/location/GlobalAddress;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->errorTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
