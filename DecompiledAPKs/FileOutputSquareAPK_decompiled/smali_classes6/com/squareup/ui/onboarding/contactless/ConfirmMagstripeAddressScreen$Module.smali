.class public Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Module;
.super Ljava/lang/Object;
.source "ConfirmMagstripeAddressScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static providePostalValidator(Lcom/squareup/CountryCode;)Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 352
    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne p0, v0, :cond_0

    new-instance p0, Lcom/squareup/ui/onboarding/postalvalidation/USPostalValidator;

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/postalvalidation/USPostalValidator;-><init>()V

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;->NONE:Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;

    :goto_0
    return-object p0
.end method
