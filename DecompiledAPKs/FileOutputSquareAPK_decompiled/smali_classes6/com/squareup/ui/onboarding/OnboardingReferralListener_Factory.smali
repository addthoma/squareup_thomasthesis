.class public final Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;
.super Ljava/lang/Object;
.source "OnboardingReferralListener_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/OnboardingReferralListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;)",
            "Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)Lcom/squareup/ui/onboarding/OnboardingReferralListener;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingReferralListener;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingReferralListener;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/OnboardingReferralListener;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;->newInstance(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)Lcom/squareup/ui/onboarding/OnboardingReferralListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingReferralListener_Factory;->get()Lcom/squareup/ui/onboarding/OnboardingReferralListener;

    move-result-object v0

    return-object v0
.end method
