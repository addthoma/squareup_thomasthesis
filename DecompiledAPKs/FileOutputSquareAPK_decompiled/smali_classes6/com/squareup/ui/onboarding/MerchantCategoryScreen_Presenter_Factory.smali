.class public final Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "MerchantCategoryScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationResourcesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationResourcesService;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final spinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationResourcesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->activationResourcesServiceProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->modelProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->spinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationResourcesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationResourcesService;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;
    .locals 8

    .line 59
    new-instance v7, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationResourcesService;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->activationResourcesServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/onboarding/ActivationResourcesService;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->spinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationResourcesService;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen_Presenter_Factory;->get()Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
