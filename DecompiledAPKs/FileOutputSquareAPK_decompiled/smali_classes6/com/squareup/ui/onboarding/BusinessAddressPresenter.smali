.class public Lcom/squareup/ui/onboarding/BusinessAddressPresenter;
.super Lmortar/ViewPresenter;
.source "BusinessAddressPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/BusinessAddressView;",
        ">;"
    }
.end annotation


# instance fields
.field private analytics:Lcom/squareup/analytics/Analytics;

.field private final businessAddressMonitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

.field private final features:Lcom/squareup/settings/server/Features;

.field private mainScheduler:Lio/reactivex/Scheduler;

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;

.field final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 44
    new-instance v0, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->res:Lcom/squareup/util/Res;

    .line 59
    iput-object p2, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 60
    iput-object p3, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 61
    iput-object p4, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 62
    iput-object p5, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 63
    iput-object p6, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->businessAddressMonitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    .line 64
    iput-object p7, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 65
    iput-object p8, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private maybeShowValidationPopup()Z
    .locals 6

    .line 126
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/BusinessAddressView;

    .line 128
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->isBusinessAddressSet()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v1}, Lcom/squareup/ui/onboarding/BusinessAddressAnalytics;->logMissingInformation(Lcom/squareup/analytics/Analytics;)V

    .line 130
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v3, Lcom/squareup/widgets/warning/WarningIds;

    sget v4, Lcom/squareup/onboarding/flow/R$string;->missing_required_field:I

    sget v5, Lcom/squareup/onboarding/flow/R$string;->missing_address_fields:I

    invoke-direct {v3, v4, v5}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 133
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->focusBusinessAddress()V

    return v2

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 138
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->getBusinessAddress()Lcom/squareup/address/Address;

    move-result-object v1

    .line 139
    invoke-static {v1}, Lcom/squareup/address/PoBoxChecker;->isPoBox(Lcom/squareup/address/Address;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 140
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v1}, Lcom/squareup/ui/onboarding/BusinessAddressAnalytics;->logInvalidAddress(Lcom/squareup/analytics/Analytics;)V

    .line 141
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v3, Lcom/squareup/widgets/warning/WarningIds;

    sget v4, Lcom/squareup/onboarding/flow/R$string;->business_address_warning_no_po_box_allowed_title:I

    sget v5, Lcom/squareup/onboarding/flow/R$string;->business_address_warning_no_po_box_allowed_message:I

    invoke-direct {v3, v4, v5}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 144
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->focusBusinessAddress()V

    return v2

    :cond_1
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/onboarding/BusinessAddressView;)V
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 86
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 87
    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    .line 89
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/ui/onboarding/BusinessAddressView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->dropView(Lcom/squareup/ui/onboarding/BusinessAddressView;)V

    return-void
.end method

.method protected getPersonalAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalAddress()Lcom/squareup/address/Address;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$BusinessAddressPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 109
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->businessAddressMonitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    invoke-interface {p1}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->setBusinessAddressVerified()V

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->afterBusinessAddressSet()V

    goto :goto_0

    .line 114
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->business_address_warning_try_again:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->business_address_warning_try_again_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {p1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onAdvanced$1$BusinessAddressPresenter(Lio/reactivex/Single;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 105
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 106
    invoke-virtual {v0}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2()Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 107
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessAddressPresenter$rfN_US42lwmKApN5p8Eguj24PDY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessAddressPresenter$rfN_US42lwmKApN5p8Eguj24PDY;-><init>(Lcom/squareup/ui/onboarding/BusinessAddressPresenter;)V

    .line 108
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onAdvanced()V
    .locals 4

    .line 93
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/BusinessAddressView;

    .line 95
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->isMobileBusiness()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->setMobileBusiness()Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0

    .line 100
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->maybeShowValidationPopup()Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    .line 101
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->getBusinessAddress()Lcom/squareup/address/Address;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->isSameAsPersonal()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->setBusinessAddress(Lcom/squareup/address/Address;Z)Lio/reactivex/Single;

    move-result-object v1

    .line 104
    :goto_0
    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessAddressPresenter$Rhg0rqmEPefOcUsLeD7bt9OXZmU;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessAddressPresenter$Rhg0rqmEPefOcUsLeD7bt9OXZmU;-><init>(Lcom/squareup/ui/onboarding/BusinessAddressPresenter;Lio/reactivex/Single;)V

    invoke-static {v0, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 69
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 71
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 73
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    .line 74
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$ZpJDlEnjNPa1Ztplq6pjqdmsJJQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ZpJDlEnjNPa1Ztplq6pjqdmsJJQ;-><init>(Lcom/squareup/ui/onboarding/BusinessAddressPresenter;)V

    .line 75
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/BusinessAddressView;

    .line 78
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/BusinessAddressView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method
