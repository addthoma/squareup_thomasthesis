.class Lcom/squareup/ui/onboarding/PersonalInfoView$10;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "PersonalInfoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/PersonalInfoView;->bindAdvanceOnDone(Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;

.field final synthetic val$view:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/PersonalInfoView;Landroid/widget/EditText;)V
    .locals 0

    .line 301
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView$10;->this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoView$10;->val$view:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x2

    if-ne p2, p1, :cond_0

    .line 304
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView$10;->val$view:Landroid/widget/EditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 305
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView$10;->this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;

    iget-object p1, p1, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->onAdvanced()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
