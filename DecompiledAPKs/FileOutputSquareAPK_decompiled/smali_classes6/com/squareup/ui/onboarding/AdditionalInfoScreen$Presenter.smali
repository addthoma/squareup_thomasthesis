.class public Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "AdditionalInfoScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/AdditionalInfoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/AdditionalInfoView;",
        ">;"
    }
.end annotation


# static fields
.field private static final SSN_LENGTH:I = 0xb


# instance fields
.field protected final dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/SquareDate;",
            "Lcom/squareup/ui/SquareDate;",
            ">;"
        }
    .end annotation
.end field

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final receivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field protected final serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            ">;"
        }
    .end annotation
.end field

.field protected final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/text/DateFormat;Lcom/squareup/ui/onboarding/ActivationStatusPresenter;Lcom/squareup/onboarding/ShareableReceivedResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            ">;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->receivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;

    .line 61
    new-instance p1, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 62
    new-instance p1, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;

    invoke-direct {p1, p0, p2, p3}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;-><init>(Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/text/DateFormat;)V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$onAdvanced$0$AdditionalInfoScreen$Presenter()V
    .locals 4

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->receivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;

    new-instance v1, Lcom/squareup/server/activation/ApplyMoreInfoBody;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 115
    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/SquareDate;->apiDate:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v3}, Lcom/squareup/ui/onboarding/OnboardingModel;->getLastSsn()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/server/activation/ApplyMoreInfoBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-virtual {v0, v1}, Lcom/squareup/onboarding/ShareableReceivedResponse;->send(Ljava/lang/Object;)V

    return-void
.end method

.method protected onAdvanced()V
    .locals 4

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/AdditionalInfoView;

    .line 98
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/AdditionalInfoView;->getSsn()Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xb

    if-eq v2, v3, :cond_0

    .line 100
    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/onboarding/flow/R$string;->invalid_ssn:I

    sget v3, Lcom/squareup/onboarding/flow/R$string;->invalid_ssn_length:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 101
    iget-object v2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v2, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 102
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/AdditionalInfoView;->requestSsnFocus()V

    return-void

    .line 105
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setLastSsn(Ljava/lang/String;)V

    .line 106
    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object v1

    if-nez v1, :cond_1

    .line 107
    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v3, Lcom/squareup/onboarding/flow/R$string;->missing_date_of_birth:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 110
    iget-object v2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v2, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 111
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/AdditionalInfoView;->requestBirthDateViewFocus()V

    return-void

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$AdditionalInfoScreen$Presenter$GFKLXcqtoFXeZuEU9ALwpfB2Hpk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$AdditionalInfoScreen$Presenter$GFKLXcqtoFXeZuEU9ALwpfB2Hpk;-><init>(Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->call(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 74
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/AdditionalInfoView;

    .line 77
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/AdditionalInfoView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    .line 78
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$InUkygubGbbVqMXeaAo2fjCgMXw;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$InUkygubGbbVqMXeaAo2fjCgMXw;-><init>(Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;)V

    .line 79
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 80
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/AdditionalInfoView;->setBirthDateViewGone()V

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/AdditionalInfoView;->setBirthDateViewVisible()V

    :goto_0
    return-void
.end method

.method protected showBirthDatePicker()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object v0

    if-nez v0, :cond_0

    .line 92
    invoke-static {}, Lcom/squareup/ui/SquareDate;->defaultBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object v0

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method
