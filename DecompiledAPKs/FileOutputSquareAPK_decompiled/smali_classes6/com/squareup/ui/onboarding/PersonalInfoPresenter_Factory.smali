.class public final Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;
.super Ljava/lang/Object;
.source "PersonalInfoPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/PersonalInfoPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumbersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final serverCallPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyBody;",
            ">;>;"
        }
    .end annotation
.end field

.field private final shareableReceivedResponseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ssnStringsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyBody;",
            ">;>;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->modelProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->ssnStringsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->deviceIdProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->installationIdProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->shareableReceivedResponseProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p11, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p12, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->serverCallPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyBody;",
            ">;>;)",
            "Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;"
        }
    .end annotation

    .line 83
    new-instance v13, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingContainer;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Ljava/text/DateFormat;Lcom/squareup/ui/onboarding/SSNStrings;Lcom/squareup/settings/DeviceIdProvider;Ljavax/inject/Provider;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/analytics/Analytics;Lcom/squareup/text/PhoneNumberHelper;Ljava/lang/Object;)Lcom/squareup/ui/onboarding/PersonalInfoPresenter;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            "Lcom/squareup/settings/DeviceIdProvider;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/squareup/ui/onboarding/PersonalInfoPresenter;"
        }
    .end annotation

    .line 92
    new-instance v13, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    move-object/from16 v12, p11

    check-cast v12, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingContainer;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Ljava/text/DateFormat;Lcom/squareup/ui/onboarding/SSNStrings;Lcom/squareup/settings/DeviceIdProvider;Ljavax/inject/Provider;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/analytics/Analytics;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/PersonalInfoPresenter;
    .locals 13

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/onboarding/OnboardingContainer;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->ssnStringsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/onboarding/SSNStrings;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->deviceIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/DeviceIdProvider;

    iget-object v8, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->installationIdProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->shareableReceivedResponseProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/onboarding/ShareableReceivedResponse;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->serverCallPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingContainer;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Ljava/text/DateFormat;Lcom/squareup/ui/onboarding/SSNStrings;Lcom/squareup/settings/DeviceIdProvider;Ljavax/inject/Provider;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/analytics/Analytics;Lcom/squareup/text/PhoneNumberHelper;Ljava/lang/Object;)Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->get()Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    move-result-object v0

    return-object v0
.end method
