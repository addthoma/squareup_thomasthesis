.class public Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
.super Ljava/lang/Object;
.source "OnboardingActivityRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;
.implements Lcom/squareup/onboarding/OnboardingExiter;


# instance fields
.field private final adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final freeReaderStatus:Lcom/squareup/ui/onboarding/FreeReaderStatus;

.field private hasCompletedBusinessInfo:Z

.field private final lazyFlow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingFinisher:Lcom/squareup/ui/onboarding/OnboardingFinisher;

.field private final onboardingModelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingScopedServices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end field

.field private final profileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final setupGuideIntegrationRunner:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final subscriptions:Lio/reactivex/disposables/CompositeDisposable;

.field private treeKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final verticalSelectionRunner:Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/ui/onboarding/FreeReaderStatus;Ljava/util/Set;Ldagger/Lazy;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Lcom/squareup/ui/onboarding/OnboardingFinisher;)V
    .locals 1
    .param p10    # Ljava/util/Set;
        .annotation runtime Lcom/squareup/onboarding/ForOnboarding;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/analytics/Analytics;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tour/WhatsNewSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/ui/onboarding/FreeReaderStatus;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
            "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
            "Lcom/squareup/ui/onboarding/OnboardingFinisher;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v0, 0x0

    .line 102
    iput-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->hasCompletedBusinessInfo:Z

    .line 113
    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    .line 115
    iput-object p3, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->lazyFlow:Ldagger/Lazy;

    .line 116
    iput-object p4, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    .line 117
    iput-object p5, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 118
    iput-object p7, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->features:Lcom/squareup/settings/server/Features;

    .line 119
    iput-object p8, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    .line 120
    iput-object p9, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->freeReaderStatus:Lcom/squareup/ui/onboarding/FreeReaderStatus;

    .line 121
    iput-object p10, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingScopedServices:Ljava/util/Set;

    .line 122
    iput-object p11, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->setupGuideIntegrationRunner:Ldagger/Lazy;

    .line 123
    iput-object p12, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->verticalSelectionRunner:Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    .line 124
    iput-object p13, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->profileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    .line 125
    iput-object p14, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingFinisher:Lcom/squareup/ui/onboarding/OnboardingFinisher;

    .line 126
    invoke-virtual {p6}, Lcom/squareup/tour/WhatsNewSettings;->shouldDisplayAfterOnboarding()Z

    move-result p1

    if-nez p1, :cond_0

    .line 128
    invoke-virtual {p6}, Lcom/squareup/tour/WhatsNewSettings;->markAllPagesAsSeen()V

    :cond_0
    return-void
.end method

.method private afterActivationApprovalNoProductIntent()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 189
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->bankScreensOrAfter()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private assertOffersMagStripe()V
    .locals 2

    .line 481
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->mayOfferMagstripe()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 482
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This device should offer mag-stripe readers."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private bankScreensOrAfter()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 199
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$lPH0_ZFoO-60TWT23iijhvsqKB4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$lPH0_ZFoO-60TWT23iijhvsqKB4;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V

    .line 200
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private getFirstNativeOnboardingScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 3

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 488
    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v1

    .line 490
    invoke-virtual {v1}, Lcom/squareup/settings/server/OnboardingSettings;->showInAppActivationPostSignup()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 491
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->isRestart()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    sget-object v0, Lcom/squareup/ui/onboarding/LoadingScreen;->INSTANCE:Lcom/squareup/ui/onboarding/LoadingScreen;

    return-object v0

    .line 494
    :cond_0
    sget-object v0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/MerchantCategoryScreen;

    return-object v0

    .line 499
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/settings/server/OnboardingSettings;->showBankLinkingAfterActivation()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 500
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setBankingOnly()V

    .line 501
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;

    return-object v0

    .line 504
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Got to onboarding but unable to do anything"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 505
    sget-object v0, Lcom/squareup/ui/onboarding/SilentExitScreen;->INSTANCE:Lcom/squareup/ui/onboarding/SilentExitScreen;

    return-object v0
.end method

.method private goFromTo(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;",
            "Lcom/squareup/container/ContainerTreeKey;",
            ")V"
        }
    .end annotation

    .line 517
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->lazyFlow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-static {v0, p1, p2}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private goTo(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 513
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->lazyFlow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final varargs goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/ContainerTreeKey;",
            "[",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .line 522
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->lazyFlow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-static {v0, p1, p2}, Lcom/squareup/container/Flows;->goToFromOneOf(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method private goToHomeScreenFrom(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 509
    sget-object v0, Lcom/squareup/ui/onboarding/OnboardingContainer;->EXIT:Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goFromTo(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private goToNextScreenAfterActivationNoProductIntent([Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    .line 356
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->afterActivationApprovalNoProductIntent()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$8PfgtPQI7fqKoV3UYwCt6cGGkpw;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$8PfgtPQI7fqKoV3UYwCt6cGGkpw;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;[Ljava/lang/Class;)V

    .line 357
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->switchMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object p1

    .line 366
    invoke-virtual {p1}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 355
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private goToNextScreenAfterBusinessAddress([Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 333
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 334
    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->verticalSelectionRunner:Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    .line 335
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBusinessCategory()Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->key:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;->onActivationCompleted(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;

    invoke-direct {v3, p0, v0, p1}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/ui/onboarding/OnboardingModel;[Ljava/lang/Class;)V

    .line 336
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 349
    invoke-virtual {p1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 334
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private mayOfferMagstripe()Z
    .locals 2

    .line 477
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_FREE_READER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private refreshFlags()V
    .locals 1

    .line 463
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->refresh()V

    return-void
.end method

.method private screenAfterBank(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->isBankingOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    sget-object p1, Lcom/squareup/ui/onboarding/OnboardingContainer;->EXIT:Lcom/squareup/container/ContainerTreeKey;

    return-object p1

    .line 224
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->shouldProceedToFreeMagstripe()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    sget-object p1, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;->INSTANCE:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;

    return-object p1

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->showReferralAfterActivation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    sget-object p1, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;->INSTANCE:Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;

    invoke-static {p1}, Lcom/squareup/referrals/ReferralScreen;->forOnboarding(Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/referrals/ReferralScreen;

    move-result-object p1

    :cond_2
    return-object p1
.end method

.method private shouldProceedToFreeMagstripe()Z
    .locals 1

    .line 456
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->mayOfferMagstripe()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->freeReaderStatus:Lcom/squareup/ui/onboarding/FreeReaderStatus;

    .line 457
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/FreeReaderStatus;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    .line 458
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->isBankingOnly()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method afterBusinessAddressSet()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 271
    const-class v1, Lcom/squareup/ui/onboarding/BusinessAddressScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 274
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToNextScreenAfterBusinessAddress([Ljava/lang/Class;)V

    return-void
.end method

.method public afterIntentQuestions()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 193
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->bankScreensOrAfter()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public exit()V
    .locals 1

    .line 468
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->refreshFlags()V

    .line 469
    sget-object v0, Lcom/squareup/ui/onboarding/OnboardingContainer;->EXIT:Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public getFirstScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 444
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 445
    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/OnboardingSettings;->shouldActivateOnTheWeb()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 446
    sget-object v0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ActivateViaWebScreen;

    return-object v0

    .line 448
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->shouldPromptActivate()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    sget-object v0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;

    return-object v0

    .line 452
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->getFirstNativeOnboardingScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$bankScreensOrAfter$1$OnboardingActivityRunner(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 201
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->mayOfferMagstripe()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->freeReaderStatus:Lcom/squareup/ui/onboarding/FreeReaderStatus;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/FreeReaderStatus;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->freeReaderStatus:Lcom/squareup/ui/onboarding/FreeReaderStatus;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/FreeReaderStatus;->refresh()V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->showBankLinkingAfterActivation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->hasBankAccount()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->isSuccessful()Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    .line 211
    :cond_1
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;

    return-object p1

    .line 209
    :cond_2
    :goto_0
    sget-object p1, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;->INSTANCE:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;

    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->screenAfterBank(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$goToNextScreenAfterActivationNoProductIntent$4$OnboardingActivityRunner([Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)Lio/reactivex/CompletableSource;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->setupGuideIntegrationRunner:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->FINISH_ACTIVATING:Lcom/squareup/protos/checklist/common/ActionItemName;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->treeKey:Lcom/squareup/ui/main/RegisterTreeKey;

    new-instance v3, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$7Pl6Hgs5pozzi0Rt9KAA9ROD_F8;

    invoke-direct {v3, p0, p2, p1}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$7Pl6Hgs5pozzi0Rt9KAA9ROD_F8;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;->handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$goToNextScreenAfterBusinessAddress$2$OnboardingActivityRunner(Lcom/squareup/ui/onboarding/OnboardingModel;[Ljava/lang/Class;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 337
    invoke-virtual {p3}, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;->getShouldLogoutOnComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    sget-object v0, Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;->LOGIN:Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setDestinationAfterOnboarding(Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)V

    .line 341
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_ASK_INTENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 342
    invoke-virtual {p3}, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;->getShouldSkipProductIntentScreens()Z

    move-result p1

    if-nez p1, :cond_1

    .line 343
    sget-object p1, Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;->INSTANCE:Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    goto :goto_0

    .line 345
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToNextScreenAfterActivationNoProductIntent([Ljava/lang/Class;)V

    .line 347
    :goto_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public synthetic lambda$null$3$OnboardingActivityRunner(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)Lkotlin/Unit;
    .locals 0

    .line 361
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    .line 362
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public synthetic lambda$onDepositOptionsResult$0$OnboardingActivityRunner()Lkotlin/Unit;
    .locals 1

    .line 156
    sget-object v0, Lcom/squareup/ui/onboarding/OnboardingContainer;->EXIT:Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->screenAfterBank(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    .line 157
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method onActivateAccountContinued()V
    .locals 2

    .line 388
    const-class v0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->getFirstNativeOnboardingScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goFromTo(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method onActivateAccountLater()V
    .locals 1

    .line 392
    const-class v0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToHomeScreenFrom(Ljava/lang/Class;)V

    return-void
.end method

.method onActivateViaWebLater()V
    .locals 1

    .line 384
    const-class v0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToHomeScreenFrom(Ljava/lang/Class;)V

    return-void
.end method

.method onActivationCallFailed()V
    .locals 2

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->DECLINED:Lcom/squareup/server/activation/ActivationStatus$State;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setActivationErrorState(Lcom/squareup/server/activation/ActivationStatus$State;)V

    .line 372
    sget-object v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen;->INSTANCE:Lcom/squareup/ui/onboarding/OnboardingErrorScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method onActivationStatus(Lcom/squareup/server/activation/ActivationStatus;)V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Class;

    .line 278
    const-class v1, Lcom/squareup/ui/onboarding/PersonalInfoScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Lcom/squareup/ui/onboarding/AdditionalInfoScreen;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-class v1, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 282
    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 284
    invoke-virtual {p1}, Lcom/squareup/server/activation/ActivationStatus;->getState()Lcom/squareup/server/activation/ActivationStatus$State;

    move-result-object v2

    .line 285
    sget-object v3, Lcom/squareup/ui/onboarding/OnboardingActivityRunner$1;->$SwitchMap$com$squareup$server$activation$ActivationStatus$State:[I

    invoke-virtual {v2}, Lcom/squareup/server/activation/ActivationStatus$State;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 327
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingModel;

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->DECLINED:Lcom/squareup/server/activation/ActivationStatus$State;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setActivationErrorState(Lcom/squareup/server/activation/ActivationStatus$State;)V

    .line 328
    sget-object p1, Lcom/squareup/ui/onboarding/OnboardingErrorScreen;->INSTANCE:Lcom/squareup/ui/onboarding/OnboardingErrorScreen;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    goto :goto_0

    .line 318
    :pswitch_0
    invoke-virtual {p1}, Lcom/squareup/server/activation/ActivationStatus;->isRetryable()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 319
    sget-object p1, Lcom/squareup/ui/onboarding/ActivationRetryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ActivationRetryScreen;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void

    .line 322
    :cond_0
    invoke-virtual {v1, v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->setActivationErrorState(Lcom/squareup/server/activation/ActivationStatus$State;)V

    .line 323
    sget-object p1, Lcom/squareup/ui/onboarding/OnboardingErrorScreen;->INSTANCE:Lcom/squareup/ui/onboarding/OnboardingErrorScreen;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    goto :goto_0

    .line 311
    :pswitch_1
    invoke-virtual {p1}, Lcom/squareup/server/activation/ActivationStatus;->getQuestions()[Lcom/squareup/server/activation/QuizQuestion;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setQuizQuestions([Lcom/squareup/server/activation/QuizQuestion;)V

    .line 312
    sget-object p1, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    goto :goto_0

    .line 307
    :pswitch_2
    sget-object p1, Lcom/squareup/ui/onboarding/AdditionalInfoScreen;->INSTANCE:Lcom/squareup/ui/onboarding/AdditionalInfoScreen;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    goto :goto_0

    .line 303
    :pswitch_3
    sget-object p1, Lcom/squareup/ui/onboarding/MerchantCategoryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/MerchantCategoryScreen;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    goto :goto_0

    .line 287
    :pswitch_4
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->refreshFlags()V

    .line 288
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->freeReaderStatus:Lcom/squareup/ui/onboarding/FreeReaderStatus;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/FreeReaderStatus;->refresh()V

    .line 289
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p1

    .line 290
    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v3

    .line 291
    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object p1

    .line 290
    invoke-interface {v1, v2, v3, p1}, Lcom/squareup/adanalytics/AdAnalytics;->recordActivation(Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/lang/String;)V

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/onboarding/common/ActivationEvent;

    invoke-direct {v1}, Lcom/squareup/onboarding/common/ActivationEvent;-><init>()V

    invoke-interface {p1, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 294
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 295
    sget-object p1, Lcom/squareup/ui/onboarding/BusinessAddressScreen;->INSTANCE:Lcom/squareup/ui/onboarding/BusinessAddressScreen;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    goto :goto_0

    .line 297
    :cond_1
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToNextScreenAfterBusinessAddress([Ljava/lang/Class;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method onBusinessCategorySelected()V
    .locals 2

    .line 236
    const-class v0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen;

    sget-object v1, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goFromTo(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method onBusinessInfoSet()V
    .locals 2

    .line 250
    const-class v0, Lcom/squareup/ui/onboarding/BusinessInfoScreen;

    invoke-static {}, Lcom/squareup/ui/onboarding/PersonalInfoScreen;->firstTry()Lcom/squareup/ui/onboarding/PersonalInfoScreen;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goFromTo(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method onBusinessSubcategorySelected()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->skipBusinessInfo()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->hasCompletedBusinessInfo:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 245
    :cond_0
    const-class v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;

    sget-object v1, Lcom/squareup/ui/onboarding/BusinessInfoScreen;->INSTANCE:Lcom/squareup/ui/onboarding/BusinessInfoScreen;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goFromTo(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_1

    .line 243
    :cond_1
    :goto_0
    const-class v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;

    invoke-static {}, Lcom/squareup/ui/onboarding/PersonalInfoScreen;->firstTry()Lcom/squareup/ui/onboarding/PersonalInfoScreen;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goFromTo(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    :goto_1
    return-void
.end method

.method public onDepositOptionsResult(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;Lcom/squareup/ui/main/RegisterTreeKey;)Lio/reactivex/Completable;
    .locals 2

    .line 149
    instance-of p1, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$GoBack;

    if-eqz p1, :cond_0

    .line 150
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingFinisher:Lcom/squareup/ui/onboarding/OnboardingFinisher;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingFinisher;->finish()V

    .line 151
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object p1

    return-object p1

    .line 153
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->setupGuideIntegrationRunner:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    sget-object p2, Lcom/squareup/protos/checklist/common/ActionItemName;->LINK_YOUR_BANK_ACCOUNT:Lcom/squareup/protos/checklist/common/ActionItemName;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->treeKey:Lcom/squareup/ui/main/RegisterTreeKey;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$_JdNbRAPa3EFpzH4_oqeGiXAtwA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$_JdNbRAPa3EFpzH4_oqeGiXAtwA;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V

    .line 154
    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;->handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 133
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->treeKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingScopedServices:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    .line 137
    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method onLoadingFinished()V
    .locals 4

    .line 427
    sget-object v0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/MerchantCategoryScreen;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/onboarding/LoadingScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToFromOneOf(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method onMerchantCategoryLater()V
    .locals 2

    .line 396
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_LATER_IN_MCC:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 397
    const-class v0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToHomeScreenFrom(Ljava/lang/Class;)V

    return-void
.end method

.method onOnboardingError()V
    .locals 0

    .line 376
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->exit()V

    return-void
.end method

.method onOnboardingFinished()V
    .locals 1

    .line 380
    const-class v0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goToHomeScreenFrom(Ljava/lang/Class;)V

    return-void
.end method

.method onPersonalInfoLater()V
    .locals 0

    .line 415
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->exit()V

    return-void
.end method

.method public onReferralLaterOrLoadFailure()V
    .locals 0

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->exit()V

    return-void
.end method

.method onRetryLater()V
    .locals 0

    .line 411
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->exit()V

    return-void
.end method

.method onRetrySucceeded()V
    .locals 2

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onboardingModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    const/4 v1, 0x0

    .line 402
    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setPersonalFirstName(Ljava/lang/String;)V

    .line 403
    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setPersonalLastName(Ljava/lang/String;)V

    .line 404
    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setPhoneNumber(Ljava/lang/String;)V

    .line 405
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setShowLaterInPersonalInfo()V

    .line 407
    const-class v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen;

    invoke-static {}, Lcom/squareup/ui/onboarding/PersonalInfoScreen;->retry()Lcom/squareup/ui/onboarding/PersonalInfoScreen;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goFromTo(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public onSelectorScreenCompleted()V
    .locals 1

    .line 436
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->getFirstScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public onShippedOrSkippedMagstripeReader()V
    .locals 1

    .line 162
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->assertOffersMagStripe()V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->showReferralAfterActivation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    sget-object v0, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;->INSTANCE:Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;

    invoke-static {v0}, Lcom/squareup/referrals/ReferralScreen;->forOnboarding(Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/referrals/ReferralScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void

    .line 169
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->exit()V

    return-void
.end method

.method onSilentExit()V
    .locals 0

    .line 419
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->exit()V

    return-void
.end method

.method setBusinessAddress(Lcom/squareup/address/Address;Z)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/address/Address;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 263
    iget-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p2}, Lcom/squareup/ui/onboarding/BusinessAddressAnalytics;->logContinueSame(Lcom/squareup/analytics/Analytics;)V

    goto :goto_0

    .line 265
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p2}, Lcom/squareup/ui/onboarding/BusinessAddressAnalytics;->logContinueDifferent(Lcom/squareup/analytics/Analytics;)V

    .line 267
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->profileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    const/4 v0, 0x0

    invoke-interface {p2, v0, p1}, Lcom/squareup/merchantprofile/MerchantProfileUpdater;->updateBusinessAddress(ZLcom/squareup/address/Address;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public setHasCompletedBusinessInfo(Z)V
    .locals 0

    .line 185
    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->hasCompletedBusinessInfo:Z

    return-void
.end method

.method setMobileBusiness()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/BusinessAddressAnalytics;->logContinueMobile(Lcom/squareup/analytics/Analytics;)V

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->profileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    sget-object v1, Lcom/squareup/address/Address;->EMPTY:Lcom/squareup/address/Address;

    const/4 v2, 0x1

    invoke-interface {v0, v2, v1}, Lcom/squareup/merchantprofile/MerchantProfileUpdater;->updateBusinessAddress(ZLcom/squareup/address/Address;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method warn(Lcom/squareup/widgets/warning/Warning;)V
    .locals 1

    .line 440
    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v0, p1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method
