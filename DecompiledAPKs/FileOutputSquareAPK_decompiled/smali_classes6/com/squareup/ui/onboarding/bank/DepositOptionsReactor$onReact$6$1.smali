.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnCancelClickedFromDepositOptionsConfirmationDialog;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;",
        "it",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnCancelClickedFromDepositOptionsConfirmationDialog;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnCancelClickedFromDepositOptionsConfirmationDialog;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnCancelClickedFromDepositOptionsConfirmationDialog;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 289
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;

    iget-object v1, v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->prepareToLinkBankAccountState$default(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/protos/client/bankaccount/BankAccountType;ILjava/lang/Object;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnCancelClickedFromDepositOptionsConfirmationDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6$1;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnCancelClickedFromDepositOptionsConfirmationDialog;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
