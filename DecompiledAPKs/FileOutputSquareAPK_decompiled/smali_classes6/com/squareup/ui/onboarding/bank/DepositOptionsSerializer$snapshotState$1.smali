.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsSerializer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->snapshotState(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state.javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    .line 57
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone;

    if-eqz v1, :cond_0

    goto/16 :goto_1

    .line 58
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet;

    if-eqz v1, :cond_1

    goto/16 :goto_1

    .line 59
    :cond_1
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;

    if-eqz v1, :cond_2

    goto/16 :goto_1

    .line 61
    :cond_2
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    if-eqz v1, :cond_3

    goto :goto_0

    .line 62
    :cond_3
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;

    if-eqz v1, :cond_4

    goto :goto_0

    .line 63
    :cond_4
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;

    if-eqz v1, :cond_5

    :goto_0
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->access$writeDepositConfig(Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lokio/BufferedSink;

    goto/16 :goto_1

    .line 65
    :cond_5
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    if-eqz v1, :cond_6

    .line 66
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->access$writeDepositConfig(Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lokio/BufferedSink;

    .line 67
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    check-cast v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;->getInvalidInput()Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->writeInvalidInput(Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;)Lokio/BufferedSink;

    goto :goto_1

    .line 70
    :cond_6
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    if-eqz v1, :cond_7

    .line 71
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->access$writeDepositConfig(Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lokio/BufferedSink;

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 75
    :cond_7
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    if-eqz v1, :cond_8

    .line 76
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->access$writeDepositConfig(Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lokio/BufferedSink;

    .line 77
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    check-cast v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->writeFailureMessage(Lokio/BufferedSink;Lcom/squareup/receiving/FailureMessage;)Lokio/BufferedSink;

    goto :goto_1

    .line 80
    :cond_8
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    if-eqz v1, :cond_9

    .line 81
    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;->getBankSuccess()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;->getCardData()Lcom/squareup/protos/client/bills/CardData;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 85
    :cond_9
    instance-of v1, v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking;

    if-eqz v1, :cond_a

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking;->getBankSuccess()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    :cond_a
    :goto_1
    return-void
.end method
