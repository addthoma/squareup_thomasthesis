.class final Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$2;
.super Ljava/lang/Object;
.source "SetupGuideDepositOptionsResultHandler.kt"

# interfaces
.implements Lio/reactivex/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;->onDepositOptionsResult(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;Lcom/squareup/ui/main/RegisterTreeKey;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$2;->this$0:Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$2;->this$0:Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;->access$getFlow$p(Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;)Lflow/Flow;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;->INSTANCE:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;

    .line 31
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
