.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;
.super Ljava/lang/Object;
.source "DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final rateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final reactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->reactorProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->modelProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->rateFormatterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;"
        }
    .end annotation

    .line 54
    new-instance v7, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;
    .locals 8

    .line 60
    new-instance v7, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/Features;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->reactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->rateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/text/RateFormatter;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->newInstance(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter_OnboardingFactory_Factory;->get()Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;

    move-result-object v0

    return-object v0
.end method
