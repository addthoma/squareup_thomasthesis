.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->onReact(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "+",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositOptionsReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositOptionsReactor.kt\ncom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7\n+ 2 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder\n*L\n1#1,413:1\n85#2,2:414\n85#2,2:416\n*E\n*S KotlinDebug\n*F\n+ 1 DepositOptionsReactor.kt\ncom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7\n*L\n294#1,2:414\n297#1,2:416\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 414
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7$$special$$inlined$onEvent$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7$$special$$inlined$onEvent$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 297
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 416
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7$$special$$inlined$onEvent$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7$$special$$inlined$onEvent$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
