.class public Lcom/squareup/ui/onboarding/OnboardingContainer;
.super Lcom/squareup/container/ContainerPresenter;
.source "OnboardingContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/OnboardingContainer$Module;,
        Lcom/squareup/ui/onboarding/OnboardingContainer$ExitKey;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/ContainerPresenter<",
        "Lcom/squareup/ui/onboarding/OnboardingContainerView;",
        ">;"
    }
.end annotation


# static fields
.field static final EXIT:Lcom/squareup/container/ContainerTreeKey;

.field private static final MODEL_BUNDLE_KEY:Ljava/lang/String; = "onboarding"

.field private static final SESSION_EXPIRED:Ljava/lang/String; = "SESSION_EXPIRED"


# instance fields
.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

.field private final modelHolder:Lcom/squareup/ui/onboarding/ModelHolder;

.field private final navigationListener:Lcom/squareup/navigation/NavigationListener;

.field private final nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lflow/History;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

.field private final selector:Lcom/squareup/onboarding/OnboardingScreenSelector;

.field final sessionExpired:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

.field private final traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation
.end field

.field private final verticalSelectionRunner:Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 89
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingContainer$ExitKey;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/OnboardingContainer$ExitKey;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/OnboardingContainer;->EXIT:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method

.method constructor <init>(Lcom/squareup/navigation/NavigationListener;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/badbus/BadBus;Lcom/squareup/onboarding/OnboardingScreenSelector;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/ModelHolder;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 126
    invoke-direct {p0}, Lcom/squareup/container/ContainerPresenter;-><init>()V

    .line 112
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 115
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 118
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 127
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->navigationListener:Lcom/squareup/navigation/NavigationListener;

    .line 128
    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

    .line 129
    iput-object p3, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    .line 130
    iput-object p5, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    .line 131
    iput-object p6, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->bus:Lcom/squareup/badbus/BadBus;

    .line 132
    iput-object p7, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->selector:Lcom/squareup/onboarding/OnboardingScreenSelector;

    .line 133
    iput-object p8, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->verticalSelectionRunner:Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    .line 134
    iput-object p9, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 135
    iput-object p10, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    .line 136
    iput-object p11, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->res:Lcom/squareup/util/Res;

    .line 137
    iput-object p12, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->modelHolder:Lcom/squareup/ui/onboarding/ModelHolder;

    .line 138
    new-instance p1, Lcom/squareup/ui/onboarding/OnboardingContainer$1;

    const-string p2, "SESSION_EXPIRED"

    invoke-direct {p1, p0, p2, p4, p5}, Lcom/squareup/ui/onboarding/OnboardingContainer$1;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;Ljava/lang/String;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;)V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->sessionExpired:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/OnboardingContainer;)Lflow/Flow;
    .locals 0

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->getFlow()Lflow/Flow;

    move-result-object p0

    return-object p0
.end method

.method private onAccountServiceSessionExpired()V
    .locals 5

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->sessionExpired:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/widgets/warning/WarningStrings;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v3, Lcom/squareup/common/strings/R$string;->session_expired_title:I

    .line 262
    invoke-interface {v2, v3}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 263
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v4, Lcom/squareup/common/strings/R$string;->session_expired_message:I

    .line 264
    invoke-interface {v3, v4}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 265
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method private performCompletionTasksAndShowLogin()V
    .locals 3

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->verticalSelectionRunner:Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    invoke-interface {v1}, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;->onOnboardingCompleted()Lio/reactivex/Completable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$QJHP1q-ZAQwvwyx21m1L1kLjINY;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$QJHP1q-ZAQwvwyx21m1L1kLjINY;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    .line 253
    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->doFinally(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object v1

    .line 254
    invoke-virtual {v1}, Lio/reactivex/Completable;->toObservable()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 255
    invoke-virtual {v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2()Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 256
    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 252
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method protected buildDispatchPipeline(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/DispatchStep;",
            ">;)V"
        }
    .end annotation

    .line 171
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$iLL1urXCPO0sIrKczqUKuptEE58;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$iLL1urXCPO0sIrKczqUKuptEE58;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$DCPNpq_ovYf6GUIaBEXJd3SEKz0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$DCPNpq_ovYf6GUIaBEXJd3SEKz0;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$Dn5RXSABDtm1Bzic__OB9yfPbkY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$Dn5RXSABDtm1Bzic__OB9yfPbkY;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerPresenter;->buildDispatchPipeline(Ljava/util/List;)V

    .line 222
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$CU-4y5uLXBoCAKEn9N8xHf-XRAE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$CU-4y5uLXBoCAKEn9N8xHf-XRAE;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected buildRedirectPipeline()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/container/RedirectStep;",
            ">;"
        }
    .end annotation

    .line 158
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 160
    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$8R2n6RkjSCqx3anT0bwUTlsgp54;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$8R2n6RkjSCqx3anT0bwUTlsgp54;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    invoke-super {p0}, Lcom/squareup/container/ContainerPresenter;->buildRedirectPipeline()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method protected bridge synthetic extractBundleService(Lcom/squareup/container/ContainerView;)Lmortar/bundler/BundleService;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingContainerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/OnboardingContainer;->extractBundleService(Lcom/squareup/ui/onboarding/OnboardingContainerView;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected extractBundleService(Lcom/squareup/ui/onboarding/OnboardingContainerView;)Lmortar/bundler/BundleService;
    .locals 0

    .line 150
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingContainerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingContainerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/OnboardingContainer;->extractBundleService(Lcom/squareup/ui/onboarding/OnboardingContainerView;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected getDefaultHistory()Lflow/History;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->selector:Lcom/squareup/onboarding/OnboardingScreenSelector;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingScreenSelector;->getFirstScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-static {v0}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object v0

    return-object v0
.end method

.method public goBackPast(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 244
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->getFlow()Lflow/Flow;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$buildDispatchPipeline$1$OnboardingContainer(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 0

    .line 172
    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/onboarding/OnboardingContainer;->EXIT:Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 173
    sget-object p1, Lcom/squareup/ui/onboarding/OnboardingContainer$2;->$SwitchMap$com$squareup$onboarding$OnboardingStarter$DestinationAfterOnboarding:[I

    iget-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->modelHolder:Lcom/squareup/ui/onboarding/ModelHolder;

    invoke-virtual {p2}, Lcom/squareup/ui/onboarding/ModelHolder;->getModel()Lcom/squareup/ui/onboarding/OnboardingModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/onboarding/OnboardingModel;->getDestinationAfterOnboarding()Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x1

    if-eq p1, p2, :cond_2

    const/4 p2, 0x2

    if-eq p1, p2, :cond_1

    const/4 p2, 0x3

    if-ne p1, p2, :cond_0

    .line 199
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->performCompletionTasksAndShowLogin()V

    goto :goto_0

    .line 202
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected destination type."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 196
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingContainerView;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingContainerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Contexts;->getActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 175
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingContainerView;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingContainerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/PaymentActivity;->reset(Landroid/content/Context;)V

    :goto_0
    const-string p1, "exiting Onboarding"

    .line 204
    invoke-static {p1}, Lcom/squareup/container/DispatchStep$Result;->stop(Ljava/lang/String;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1

    .line 207
    :cond_3
    invoke-static {}, Lcom/squareup/container/DispatchStep$Result;->go()Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$buildDispatchPipeline$2$OnboardingContainer(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 1

    .line 211
    iget-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->RESIZE:Lcom/squareup/workflow/SoftInputMode;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/SoftInputPresenter;->prepKeyboardForScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/SoftInputMode;)V

    .line 212
    invoke-static {}, Lcom/squareup/container/DispatchStep$Result;->go()Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$buildDispatchPipeline$3$OnboardingContainer(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 0

    .line 216
    iget-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 217
    invoke-static {}, Lcom/squareup/container/DispatchStep$Result;->go()Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$buildDispatchPipeline$5$OnboardingContainer(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 1

    .line 223
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$l9dhtXkonrWPNWcJvYo-DBII4pU;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$l9dhtXkonrWPNWcJvYo-DBII4pU;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;Lflow/Traversal;Lflow/TraversalCallback;)V

    const-string p1, "LoggedInOnboardingContainer: traversalCompleting"

    invoke-static {p1, v0}, Lcom/squareup/container/DispatchStep$Result;->wrap(Ljava/lang/String;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$buildRedirectPipeline$0$OnboardingContainer(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->navigationListener:Lcom/squareup/navigation/NavigationListener;

    invoke-virtual {v0, p1}, Lcom/squareup/navigation/NavigationListener;->onDispatch(Lflow/Traversal;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public synthetic lambda$null$4$OnboardingContainer(Lflow/Traversal;Lflow/TraversalCallback;)V
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 225
    invoke-interface {p2}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$6$OnboardingContainer(Lcom/squareup/account/AccountEvents$SessionExpired;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 235
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->onAccountServiceSessionExpired()V

    return-void
.end method

.method public synthetic lambda$performCompletionTasksAndShowLogin$7$OnboardingContainer()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/onboarding/OnboardingContainerView;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingContainerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/loggedout/LoggedOutStarter;->showLogin(Landroid/content/Context;)V

    return-void
.end method

.method public nextHistory()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;"
        }
    .end annotation

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 230
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const-class v2, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;

    .line 232
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 231
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/navigation/ScreenNavigationLogger;->init(Lmortar/MortarScope;Lio/reactivex/Observable;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->bus:Lcom/squareup/badbus/BadBus;

    const-class v2, Lcom/squareup/account/AccountEvents$SessionExpired;

    .line 235
    invoke-virtual {v1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$0ReTZNInehL1zTaKgWBG7-0yGbg;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingContainer$0ReTZNInehL1zTaKgWBG7-0yGbg;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 234
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public pushStack(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 240
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->getFlow()Lflow/Flow;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/container/Flows;->pushStack(Lflow/Flow;Ljava/util/List;)V

    return-void
.end method
