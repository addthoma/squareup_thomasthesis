.class public final enum Lcom/squareup/ui/onboarding/SSNStrings;
.super Ljava/lang/Enum;
.source "SSNStrings.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/onboarding/SSNStrings;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/onboarding/SSNStrings;

.field public static final enum SSN:Lcom/squareup/ui/onboarding/SSNStrings;

.field public static final enum SSN_WITH_ITIN:Lcom/squareup/ui/onboarding/SSNStrings;


# instance fields
.field public final invalid_last4_ssn_message:I

.field public final missing_personal_fields:I

.field public final uppercase_header_last_4_ssn:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 12
    new-instance v6, Lcom/squareup/ui/onboarding/SSNStrings;

    sget v3, Lcom/squareup/onboarding/flow/R$string;->invalid_last4_ssn_message:I

    sget v4, Lcom/squareup/onboarding/flow/R$string;->missing_personal_fields:I

    sget v5, Lcom/squareup/onboarding/flow/R$string;->uppercase_header_last_4_ssn:I

    const-string v1, "SSN"

    const/4 v2, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/SSNStrings;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lcom/squareup/ui/onboarding/SSNStrings;->SSN:Lcom/squareup/ui/onboarding/SSNStrings;

    .line 15
    new-instance v0, Lcom/squareup/ui/onboarding/SSNStrings;

    sget v10, Lcom/squareup/onboarding/flow/R$string;->invalid_last4_ssn_message_with_itin:I

    sget v11, Lcom/squareup/onboarding/flow/R$string;->missing_personal_fields_with_itin:I

    sget v12, Lcom/squareup/onboarding/flow/R$string;->uppercase_header_last_4_ssn_with_itin:I

    const-string v8, "SSN_WITH_ITIN"

    const/4 v9, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/onboarding/SSNStrings;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/squareup/ui/onboarding/SSNStrings;->SSN_WITH_ITIN:Lcom/squareup/ui/onboarding/SSNStrings;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/onboarding/SSNStrings;

    .line 11
    sget-object v1, Lcom/squareup/ui/onboarding/SSNStrings;->SSN:Lcom/squareup/ui/onboarding/SSNStrings;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/onboarding/SSNStrings;->SSN_WITH_ITIN:Lcom/squareup/ui/onboarding/SSNStrings;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/onboarding/SSNStrings;->$VALUES:[Lcom/squareup/ui/onboarding/SSNStrings;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/squareup/ui/onboarding/SSNStrings;->invalid_last4_ssn_message:I

    .line 26
    iput p4, p0, Lcom/squareup/ui/onboarding/SSNStrings;->missing_personal_fields:I

    .line 27
    iput p5, p0, Lcom/squareup/ui/onboarding/SSNStrings;->uppercase_header_last_4_ssn:I

    return-void
.end method

.method public static getFor(Lcom/squareup/CountryCode;Ljava/util/Locale;)Lcom/squareup/ui/onboarding/SSNStrings;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne p0, v0, :cond_0

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p0

    const-string p1, "es"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget-object p0, Lcom/squareup/ui/onboarding/SSNStrings;->SSN_WITH_ITIN:Lcom/squareup/ui/onboarding/SSNStrings;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/ui/onboarding/SSNStrings;->SSN:Lcom/squareup/ui/onboarding/SSNStrings;

    :goto_0
    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/onboarding/SSNStrings;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/ui/onboarding/SSNStrings;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/onboarding/SSNStrings;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/onboarding/SSNStrings;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/ui/onboarding/SSNStrings;->$VALUES:[Lcom/squareup/ui/onboarding/SSNStrings;

    invoke-virtual {v0}, [Lcom/squareup/ui/onboarding/SSNStrings;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/onboarding/SSNStrings;

    return-object v0
.end method
