.class final Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;
.super Ljava/lang/Object;
.source "FreeReaderStatus.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/FreeReaderStatus;->refresh()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;->INSTANCE:Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;->status:Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Status;

    sget-object v0, Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Status;->NOT_FOUND:Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Status;

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 52
    :cond_1
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_2

    :goto_0
    return v1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
