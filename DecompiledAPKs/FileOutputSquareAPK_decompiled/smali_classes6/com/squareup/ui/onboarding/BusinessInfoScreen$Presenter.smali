.class public Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "BusinessInfoScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/BusinessInfoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/BusinessInfoView;",
        ">;"
    }
.end annotation


# static fields
.field private static final EIN_LENGTH:I = 0xa


# instance fields
.field private final activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final container:Lcom/squareup/ui/onboarding/OnboardingContainer;

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final res:Lcom/squareup/util/Res;

.field private revenueContainer:Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

.field private final revenueStorage:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingContainer;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/onboarding/ActivationResourcesService;Lcom/squareup/BundleKey;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            "Lcom/squareup/onboarding/ActivationResourcesService;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 65
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 59
    new-instance v0, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 67
    iput-object p2, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 68
    iput-object p3, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    .line 69
    iput-object p4, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 70
    iput-object p5, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;

    .line 71
    iput-object p6, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueStorage:Lcom/squareup/BundleKey;

    .line 72
    iput-object p7, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private setSelectedIncome(Lcom/squareup/ui/onboarding/BusinessInfoView;)V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getRevenue()Lcom/squareup/server/activation/ActivationResources$RevenueEntry;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueContainer:Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    iget-object v1, v1, Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;->entries:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/BusinessInfoView;->setSelectedIncome(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$BusinessInfoScreen$Presenter(Lcom/squareup/server/activation/ActivationResources;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/BusinessInfoView;

    if-nez v0, :cond_0

    return-void

    .line 82
    :cond_0
    new-instance v1, Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    .line 83
    invoke-virtual {p1}, Lcom/squareup/server/activation/ActivationResources;->getEstimatedRevenueEntries()Ljava/util/List;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;-><init>(Ljava/util/List;)V

    iput-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueContainer:Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueContainer:Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    iget-object p1, p1, Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;->entries:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/BusinessInfoView;->setIncomeOptions(Ljava/util/List;)V

    .line 85
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->setSelectedIncome(Lcom/squareup/ui/onboarding/BusinessInfoView;)V

    return-void
.end method

.method onAdvanced()V
    .locals 4

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/onboarding/BusinessInfoView;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/BusinessInfoView;->getBusinessName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setBusinessName(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/BusinessInfoView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessInfoView;->getEinCheckedIndex()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 119
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->missing_ein:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->missing_ein_body:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 120
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 122
    :cond_0
    sget v1, Lcom/squareup/onboarding/flow/R$id;->has_ein:I

    if-ne v0, v1, :cond_2

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/BusinessInfoView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessInfoView;->getEin()Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_1

    .line 125
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->invalid_ein:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->invalid_ein_length:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 126
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 129
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setEin(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :cond_2
    sget v1, Lcom/squareup/onboarding/flow/R$id;->no_ein:I

    if-ne v0, v1, :cond_3

    .line 133
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onBusinessInfoSet()V

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BUSINESS_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void

    .line 131
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown radio index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method onBusinessNameChanged()V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BUSINESS_NAME:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method onEinChanged()V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_EIN:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method onEinRadioTapped()V
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_EIN_RADIO:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;

    .line 77
    invoke-virtual {v0}, Lcom/squareup/onboarding/ActivationResourcesService;->getActivationResources()Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    .line 78
    invoke-static {}, Lcom/squareup/receiving/StandardReceiver;->filterSuccess()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/observables/ConnectableObservable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessInfoScreen$Presenter$BixMevyECs9t74cPWd5JNhWBUhA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessInfoScreen$Presenter$BixMevyECs9t74cPWd5JNhWBUhA;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;)V

    .line 79
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 76
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method onIncomeRangeSelected(I)V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_INCOME_RADIO:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setRevenue(Lcom/squareup/server/activation/ActivationResources$RevenueEntry;)V

    goto :goto_0

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueContainer:Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    iget-object v1, v1, Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;->entries:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/activation/ActivationResources$RevenueEntry;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setRevenue(Lcom/squareup/server/activation/ActivationResources$RevenueEntry;)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/BusinessInfoView;

    .line 91
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/BusinessInfoView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/onboarding/-$$Lambda$5ZQ0I9OVAgq3phbZJSSrfxeiYdE;

    invoke-direct {v4, v3}, Lcom/squareup/ui/onboarding/-$$Lambda$5ZQ0I9OVAgq3phbZJSSrfxeiYdE;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    .line 92
    invoke-virtual {v2, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 93
    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->updateUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    .line 94
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/onboarding/-$$Lambda$bsfg6doCW-CXM6YmNuA_swKlsRA;

    invoke-direct {v3, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$bsfg6doCW-CXM6YmNuA_swKlsRA;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;)V

    .line 95
    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 96
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    .line 91
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    if-eqz p1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueStorage:Lcom/squareup/BundleKey;

    invoke-virtual {v1, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    if-eqz p1, :cond_0

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueContainer:Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    .line 105
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBusinessName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/BusinessInfoView;->setBusinessName(Ljava/lang/String;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueContainer:Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    if-eqz v0, :cond_0

    .line 110
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->revenueStorage:Lcom/squareup/BundleKey;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
