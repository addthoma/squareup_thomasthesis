.class Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "ActivationStatusPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<REQ:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field static final POLLS_BEFORE_TIMEOUT:I = 0x14

.field static final POLL_INTERVAL_MILLIS:I = 0xbb8

.field private static final TIMEOUT_MILLIS:I = 0xea60


# instance fields
.field private final activationService:Lcom/squareup/server/activation/ActivationService;

.field private applyServerCallAction:Ljava/lang/Runnable;

.field private final expectedState:Lcom/squareup/server/activation/ActivationStatus$State;

.field private final failureMessageId:I

.field private final firstReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "TREQ;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final firstRequestRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final glassSpinnerRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isFirstStatusCall:Z

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private pollCount:I

.field private final progressMessageId:I

.field private progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final statusReceivedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private validationSuccess:Z


# direct methods
.method constructor <init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/server/activation/ActivationStatus$State;Lcom/squareup/util/Res;IILcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            "Lcom/squareup/server/activation/ActivationService;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "TREQ;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/server/activation/ActivationStatus$State;",
            "Lcom/squareup/util/Res;",
            "II",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")V"
        }
    .end annotation

    .line 82
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    .line 61
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->statusReceivedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 62
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->firstRequestRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 66
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->glassSpinnerRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    const/4 v0, 0x0

    .line 70
    iput v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->pollCount:I

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 84
    iput-object p6, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->res:Lcom/squareup/util/Res;

    .line 85
    iput p7, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->progressMessageId:I

    .line 86
    iput p8, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->failureMessageId:I

    .line 87
    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 88
    iput-object p3, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    .line 89
    iput-object p4, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->firstReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;

    .line 90
    iput-object p5, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->expectedState:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 91
    iput-object p9, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method private sendStatusCall(Z)V
    .locals 1

    .line 220
    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->isFirstStatusCall:Z

    .line 221
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->statusReceivedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    const/4 p1, 0x1

    .line 222
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showSpinner(Z)V

    return-void
.end method

.method private showFailure(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 230
    invoke-direct {p0, p1, v0, v0}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showFailure(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showFailure(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 235
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/common/strings/R$string;->network_error_title:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/common/strings/R$string;->network_error_message:I

    .line 236
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 237
    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->retry:I

    .line 238
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 235
    invoke-static {p1, p2, p3, v0}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 240
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->res:Lcom/squareup/util/Res;

    iget v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->failureMessageId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->server_error_message:I

    .line 242
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 241
    invoke-static {p3, p2}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 243
    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 240
    invoke-static {p1, p2, p3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->show(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method private showSpinner(Z)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->glassSpinnerRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Runnable;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->applyServerCallAction:Ljava/lang/Runnable;

    const/4 p1, 0x1

    .line 216
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->sendStatusCall(Z)V

    return-void
.end method

.method public dropView(Lcom/squareup/mortar/Popup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 203
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 204
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 205
    iput-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    .line 207
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lmortar/bundler/BundleService;"
        }
    .end annotation

    .line 108
    invoke-interface {p1}, Lcom/squareup/mortar/Popup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$2$ActivationStatusPresenter()V
    .locals 1

    .line 150
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->isFirstStatusCall:Z

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->sendStatusCall(Z)V

    return-void
.end method

.method public synthetic lambda$null$3$ActivationStatusPresenter(Lcom/squareup/server/activation/ActivationStatus;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 126
    invoke-virtual {p1}, Lcom/squareup/server/activation/ActivationStatus;->getState()Lcom/squareup/server/activation/ActivationStatus$State;

    move-result-object v0

    .line 127
    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->UNRECOGNIZED:Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 128
    invoke-direct {p0, v2}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showSpinner(Z)V

    const/4 v0, 0x0

    .line 129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unrecognized state: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/server/activation/ActivationStatus;->getRawState()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v2, v0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showFailure(ZLjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 133
    :cond_0
    iput-boolean v2, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->isFirstStatusCall:Z

    .line 134
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->expectedState:Lcom/squareup/server/activation/ActivationStatus$State;

    if-ne v0, v1, :cond_1

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->firstRequestRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->applyServerCallAction:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void

    .line 138
    :cond_1
    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->PENDING:Lcom/squareup/server/activation/ActivationStatus$State;

    if-eq v0, v1, :cond_2

    .line 139
    invoke-direct {p0, v2}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showSpinner(Z)V

    .line 140
    iput v2, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->pollCount:I

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onActivationStatus(Lcom/squareup/server/activation/ActivationStatus;)V

    return-void

    .line 144
    :cond_2
    iget p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->pollCount:I

    const/4 v0, 0x1

    add-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->pollCount:I

    const/16 v1, 0x14

    if-le p1, v1, :cond_3

    .line 145
    invoke-direct {p0, v2}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showSpinner(Z)V

    .line 146
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showFailure(Z)V

    return-void

    .line 150
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$yfW3gwcIW0zCUPSfkqxY0EHmzjA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$yfW3gwcIW0zCUPSfkqxY0EHmzjA;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    const-wide/16 v1, 0xbb8

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public synthetic lambda$null$4$ActivationStatusPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 152
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showSpinner(Z)V

    .line 155
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showFailure(Z)V

    return-void
.end method

.method public synthetic lambda$null$7$ActivationStatusPresenter(Lcom/squareup/server/SimpleResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p1, 0x0

    .line 164
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showSpinner(Z)V

    const/4 p1, 0x1

    .line 165
    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->validationSuccess:Z

    .line 166
    iget-boolean p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->isFirstStatusCall:Z

    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->sendStatusCall(Z)V

    return-void
.end method

.method public synthetic lambda$null$8$ActivationStatusPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 169
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showSpinner(Z)V

    .line 170
    iput-boolean v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->validationSuccess:Z

    .line 173
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 177
    instance-of v1, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 178
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    .line 179
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/SimpleResponse;

    if-eqz p1, :cond_1

    .line 181
    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 182
    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->getMessage()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 184
    :cond_0
    instance-of v1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v1, :cond_1

    .line 185
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    .line 186
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/SimpleResponse;

    if-eqz p1, :cond_1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 189
    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->getMessage()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    move-object p1, v2

    .line 192
    :goto_0
    invoke-direct {p0, v0, v2, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->showFailure(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$ActivationStatusPresenter(Ljava/lang/Boolean;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1

    .line 115
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iget v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->progressMessageId:I

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/GlassSpinnerState;->showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1

    .line 118
    :cond_0
    invoke-static {}, Lcom/squareup/register/widgets/GlassSpinnerState;->hideSpinner()Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$1$ActivationStatusPresenter(Lkotlin/Unit;)Lio/reactivex/SingleSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    invoke-interface {p1}, Lcom/squareup/server/activation/ActivationService;->status()Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$5$ActivationStatusPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 125
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$lWwZ9XBWwQVVBChsBRw_x6fRyZk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$lWwZ9XBWwQVVBChsBRw_x6fRyZk;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$VuOuD5TcX_bKpmH0IIs5yON2ujU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$VuOuD5TcX_bKpmH0IIs5yON2ujU;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$6$ActivationStatusPresenter(Lkotlin/Unit;)Lio/reactivex/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->firstReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;

    invoke-virtual {p1}, Lcom/squareup/onboarding/ShareableReceivedResponse;->getResponses()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$9$ActivationStatusPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 160
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$DbXH2CE4_3UtNOjaQ7ndrbcEXmE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$DbXH2CE4_3UtNOjaQ7ndrbcEXmE;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$G-UheLpC26V7iK-xRXdtGU4MyNM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$G-UheLpC26V7iK-xRXdtGU4MyNM;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->glassSpinnerRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 113
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$WQJ_OgFt4ZMTX1DV7Y5y9CRQs7I;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$WQJ_OgFt4ZMTX1DV7Y5y9CRQs7I;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    .line 114
    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 112
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->statusReceivedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$RZ9qUGsTZkkX_YnpaEPJrdxChiI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$RZ9qUGsTZkkX_YnpaEPJrdxChiI;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    .line 124
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$gO2EFCpHtxPjNiu5b_fPuqEYPw0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$gO2EFCpHtxPjNiu5b_fPuqEYPw0;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    .line 125
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 123
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->firstRequestRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$pFvs7mP2uByiWLMrMsgyL1kQ9yM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$pFvs7mP2uByiWLMrMsgyL1kQ9yM;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$wjCAHymiP03TPe_iiocShSd8LyE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationStatusPresenter$wjCAHymiP03TPe_iiocShSd8LyE;-><init>(Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V

    .line 160
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 158
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 197
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 198
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 199
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mortar/Popup;

    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 1

    const/4 v0, 0x0

    .line 95
    iput v0, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->pollCount:I

    .line 96
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->sendStatusCall(Z)V

    goto :goto_0

    .line 100
    :cond_0
    iget-boolean p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->validationSuccess:Z

    if-eqz p1, :cond_1

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onActivationCallFailed()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 39
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
