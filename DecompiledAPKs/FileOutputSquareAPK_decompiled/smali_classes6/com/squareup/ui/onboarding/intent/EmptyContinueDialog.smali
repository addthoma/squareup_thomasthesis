.class public Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "EmptyContinueDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;",
            ">;"
        }
    .end annotation
.end field

.field static final EMPTY_CONTINUE_FROM_HOW:Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

.field static final EMPTY_CONTINUE_FROM_WHERE:Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;


# instance fields
.field private final fromWhere:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 20
    new-instance v0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;-><init>(Z)V

    sput-object v0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->EMPTY_CONTINUE_FROM_WHERE:Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

    .line 21
    new-instance v0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;-><init>(Z)V

    sput-object v0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->EMPTY_CONTINUE_FROM_HOW:Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

    .line 59
    sget-object v0, Lcom/squareup/ui/onboarding/intent/-$$Lambda$EmptyContinueDialog$jPHsXlA1cJfmLfypKgF8MoIKEJY;->INSTANCE:Lcom/squareup/ui/onboarding/intent/-$$Lambda$EmptyContinueDialog$jPHsXlA1cJfmLfypKgF8MoIKEJY;

    .line 60
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    .line 26
    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->fromWhere:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;)Z
    .locals 0

    .line 19
    iget-boolean p0, p0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->fromWhere:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;
    .locals 1

    .line 61
    new-instance v0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    move-result p0

    invoke-static {p0}, Lcom/squareup/util/Booleans;->toBoolean(B)Z

    move-result p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 56
    iget-boolean p2, p0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->fromWhere:Z

    invoke-static {p2}, Lcom/squareup/util/Booleans;->toByte(Z)B

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method
