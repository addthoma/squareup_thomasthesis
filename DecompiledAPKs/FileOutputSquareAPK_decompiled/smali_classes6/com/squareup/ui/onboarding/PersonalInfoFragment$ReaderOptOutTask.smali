.class public Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;
.super Ljava/lang/Object;
.source "PersonalInfoFragment.java"

# interfaces
.implements Lcom/squareup/queue/retrofit/RetrofitTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/PersonalInfoFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReaderOptOutTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/retrofit/RetrofitTask<",
        "Lcom/squareup/ui/onboarding/OnboardingLoggedInComponent;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x42f7c1c2bae01e7L


# instance fields
.field transient accountService:Lcom/squareup/account/PersistentAccountService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;->accountService:Lcom/squareup/account/PersistentAccountService;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/account/PersistentAccountService;->reportOutOfBandReader(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$H1FKsbcnpoGiVi8VW-et63b2_bI;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/-$$Lambda$H1FKsbcnpoGiVi8VW-et63b2_bI;-><init>(Lcom/squareup/server/SquareCallback;)V

    .line 25
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public inject(Lcom/squareup/ui/onboarding/OnboardingLoggedInComponent;)V
    .locals 0

    .line 33
    invoke-interface {p1, p0}, Lcom/squareup/ui/onboarding/OnboardingLoggedInComponent;->inject(Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingLoggedInComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;->inject(Lcom/squareup/ui/onboarding/OnboardingLoggedInComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method
