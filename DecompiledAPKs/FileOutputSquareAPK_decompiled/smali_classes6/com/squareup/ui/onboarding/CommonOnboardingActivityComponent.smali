.class public interface abstract Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;
.super Ljava/lang/Object;
.source "CommonOnboardingActivityComponent.java"

# interfaces
.implements Lcom/squareup/caller/ProgressPopup$Component;
.implements Lcom/squareup/register/widgets/card/CardEditor$ParentComponent;
.implements Lcom/squareup/register/widgets/card/PanEditor$Component;
.implements Lcom/squareup/address/StatePickerScreen$Component;
.implements Lcom/squareup/referrals/ReferralScreen$ParentComponent;
.implements Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ParentComponent;
.implements Lcom/squareup/ui/LinkSpan$Component;


# annotations
.annotation runtime Lcom/squareup/banklinking/RealBankAccountSettings$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/camerahelper/CameraHelperScope;
.end annotation

.annotation runtime Lcom/squareup/depositschedule/RealDepositScheduleSettings$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/onboarding/OnboardingContainer$Module;,
        Lcom/squareup/ui/onboarding/SimpleOnboardingScreenSelectorModule;,
        Lcom/squareup/ui/onboarding/bank/OnboardingDepositOptionsModule;,
        Lcom/squareup/setupguide/NoSetupGuideOnboardingModule;,
        Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;
    }
.end annotation


# virtual methods
.method public abstract activateViaWeb()Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Component;
.end method

.method public abstract activateYourAccount()Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Component;
.end method

.method public abstract activationRetry()Lcom/squareup/ui/onboarding/ActivationRetryScreen$Component;
.end method

.method public abstract additionalInfo()Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Component;
.end method

.method public abstract businessAddress()Lcom/squareup/ui/onboarding/BusinessAddressScreen$Component;
.end method

.method public abstract businessInfo()Lcom/squareup/ui/onboarding/BusinessInfoScreen$Component;
.end method

.method public abstract confirmIdentity()Lcom/squareup/ui/onboarding/ConfirmIdentityScreen$Component;
.end method

.method public abstract confirmMagstripeAddress()Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Component;
.end method

.method public abstract getRunner()Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
.end method

.method public abstract inject(Lcom/squareup/ui/onboarding/OnboardingActivity;)V
.end method

.method public abstract inject(Lcom/squareup/ui/onboarding/OnboardingContainerView;)V
.end method

.method public abstract intentHowCoordinator()Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;
.end method

.method public abstract intentWhereCoordinator()Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;
.end method

.method public abstract loading()Lcom/squareup/ui/onboarding/LoadingScreen$Component;
.end method

.method public abstract merchantCategory()Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Component;
.end method

.method public abstract merchantSubcategory()Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Component;
.end method

.method public abstract onboardingError()Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Component;
.end method

.method public abstract onboardingFinished()Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Component;
.end method

.method public abstract onboardingIntentData()Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;
.end method

.method public abstract personalInfo()Lcom/squareup/ui/onboarding/PersonalInfoScreen$Component;
.end method

.method public abstract silentExit()Lcom/squareup/ui/onboarding/SilentExitScreen$Component;
.end method
