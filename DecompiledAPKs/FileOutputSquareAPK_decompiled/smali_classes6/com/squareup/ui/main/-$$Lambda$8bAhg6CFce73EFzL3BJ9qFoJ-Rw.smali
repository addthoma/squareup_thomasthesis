.class public final synthetic Lcom/squareup/ui/main/-$$Lambda$8bAhg6CFce73EFzL3BJ9qFoJ-Rw;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/container/RedirectStep;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/main/HomeScreenSelector;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/main/HomeScreenSelector;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/-$$Lambda$8bAhg6CFce73EFzL3BJ9qFoJ-Rw;->f$0:Lcom/squareup/ui/main/HomeScreenSelector;

    return-void
.end method


# virtual methods
.method public final maybeRedirect(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/main/-$$Lambda$8bAhg6CFce73EFzL3BJ9qFoJ-Rw;->f$0:Lcom/squareup/ui/main/HomeScreenSelector;

    invoke-interface {v0, p1}, Lcom/squareup/ui/main/HomeScreenSelector;->redirectForOpenTicketsHomeScreen(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p1

    return-object p1
.end method
