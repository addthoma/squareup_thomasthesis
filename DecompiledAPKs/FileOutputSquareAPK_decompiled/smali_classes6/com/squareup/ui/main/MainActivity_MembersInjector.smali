.class public final Lcom/squareup/ui/main/MainActivity_MembersInjector;
.super Ljava/lang/Object;
.source "MainActivity_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/main/MainActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityResultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final activityVisibilityPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalActivityDelegatesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;>;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final apiRequestControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;"
        }
    .end annotation
.end field

.field private final badKeyboardHiderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final cameraHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationChangeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final containerActivityDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final contentViewInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinkHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final focusedActivityScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final locationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final locationPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LocationPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaButtonDisablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseNarcPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentBundleManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;"
        }
    .end annotation
.end field

.field private final rootViewSetupProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;"
        }
    .end annotation
.end field

.field private final secureScopeManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final statusBarHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final usbDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LocationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 165
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 166
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->containerActivityDelegateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->cameraHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->locationPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->pauseNarcPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 175
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->activityVisibilityPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->permissionsPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->mainContainerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->softInputPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 181
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->featuresProvider2:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 182
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->statusBarHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 183
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 184
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->apiRequestControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 185
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->deepLinkHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 186
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->nfcStateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->browserLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 188
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->rootViewSetupProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 189
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->secureScopeManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 190
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p34

    .line 191
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->usbDiscovererProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p35

    .line 192
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->additionalActivityDelegatesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p36

    .line 193
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->badKeyboardHiderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 38
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LocationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/main/MainActivity;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    .line 230
    new-instance v37, Lcom/squareup/ui/main/MainActivity_MembersInjector;

    move-object/from16 v0, v37

    invoke-direct/range {v0 .. v36}, Lcom/squareup/ui/main/MainActivity_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v37
.end method

.method public static injectActivityVisibilityPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)V
    .locals 0

    .line 304
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    return-void
.end method

.method public static injectAdditionalActivityDelegates(Lcom/squareup/ui/main/MainActivity;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/MainActivity;",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;)V"
        }
    .end annotation

    .line 392
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->additionalActivityDelegates:Ljava/util/Set;

    return-void
.end method

.method public static injectApiRequestController(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/api/ApiRequestController;)V
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    return-void
.end method

.method public static injectBadKeyboardHider(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/BadKeyboardHider;)V
    .locals 0

    .line 398
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->badKeyboardHider:Lcom/squareup/ui/main/BadKeyboardHider;

    return-void
.end method

.method public static injectBrowserLauncher(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/util/BrowserLauncher;)V
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method public static injectCameraHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/camerahelper/CameraHelper;)V
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    return-void
.end method

.method public static injectContainerActivityDelegate(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/container/ContainerActivityDelegate;)V
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    return-void
.end method

.method public static injectDeepLinkHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/analytics/DeepLinkHelper;)V
    .locals 0

    .line 353
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 332
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectInvoiceShareUrlLauncher(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/url/InvoiceShareUrlLauncher;)V
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    return-void
.end method

.method public static injectLocationPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/LocationPresenter;)V
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->locationPresenter:Lcom/squareup/ui/main/LocationPresenter;

    return-void
.end method

.method public static injectMainContainer(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/PosContainer;)V
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method public static injectNfcState(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/AndroidNfcState;)V
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->nfcState:Lcom/squareup/ui/AndroidNfcState;

    return-void
.end method

.method public static injectOfflineModeMonitor(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/payment/OfflineModeMonitor;)V
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    return-void
.end method

.method public static injectPasscodeEmployeeManagement(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    return-void
.end method

.method public static injectPauseNarcPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/pauses/PauseAndResumePresenter;)V
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->pauseNarcPresenter:Lcom/squareup/pauses/PauseAndResumePresenter;

    return-void
.end method

.method public static injectPermissionsPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;)V
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    return-void
.end method

.method public static injectRootViewSetup(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/rootview/RootViewSetup;)V
    .locals 0

    .line 368
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->rootViewSetup:Lcom/squareup/rootview/RootViewSetup;

    return-void
.end method

.method public static injectSecureScopeManager(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/secure/SecureScopeManager;)V
    .locals 0

    .line 374
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    return-void
.end method

.method public static injectSoftInputPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/SoftInputPresenter;)V
    .locals 0

    .line 327
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    return-void
.end method

.method public static injectStatusBarHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/StatusBarHelper;)V
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->statusBarHelper:Lcom/squareup/ui/StatusBarHelper;

    return-void
.end method

.method public static injectToastFactory(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/util/ToastFactory;)V
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->toastFactory:Lcom/squareup/util/ToastFactory;

    return-void
.end method

.method public static injectUsbDiscoverer(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/usb/UsbDiscoverer;)V
    .locals 0

    .line 385
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/main/MainActivity;)V
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/development/drawer/ContentViewInitializer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/MediaButtonDisabler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->containerActivityDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectContainerActivityDelegate(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/container/ContainerActivityDelegate;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->cameraHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/camerahelper/CameraHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectCameraHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/camerahelper/CameraHelper;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->locationPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/LocationPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectLocationPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/LocationPresenter;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->pauseNarcPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectPauseNarcPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/pauses/PauseAndResumePresenter;)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectPasscodeEmployeeManagement(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->activityVisibilityPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectActivityVisibilityPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)V

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/OfflineModeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectOfflineModeMonitor(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/payment/OfflineModeMonitor;)V

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->permissionsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectPermissionsPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;)V

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectMainContainer(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/PosContainer;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/SoftInputPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectSoftInputPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/SoftInputPresenter;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->featuresProvider2:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/settings/server/Features;)V

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->statusBarHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/StatusBarHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectStatusBarHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/StatusBarHelper;)V

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/ToastFactory;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectToastFactory(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/util/ToastFactory;)V

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->apiRequestControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiRequestController;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectApiRequestController(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/api/ApiRequestController;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->deepLinkHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/DeepLinkHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectDeepLinkHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/analytics/DeepLinkHelper;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->nfcStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/AndroidNfcState;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectNfcState(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/AndroidNfcState;)V

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/BrowserLauncher;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectBrowserLauncher(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/util/BrowserLauncher;)V

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->rootViewSetupProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/rootview/RootViewSetup;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectRootViewSetup(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/rootview/RootViewSetup;)V

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->secureScopeManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/secure/SecureScopeManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectSecureScopeManager(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/secure/SecureScopeManager;)V

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/url/InvoiceShareUrlLauncher;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectInvoiceShareUrlLauncher(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/url/InvoiceShareUrlLauncher;)V

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->usbDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectUsbDiscoverer(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/usb/UsbDiscoverer;)V

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->additionalActivityDelegatesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectAdditionalActivityDelegates(Lcom/squareup/ui/main/MainActivity;Ljava/util/Set;)V

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity_MembersInjector;->badKeyboardHiderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/BadKeyboardHider;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectBadKeyboardHider(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/BadKeyboardHider;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/ui/main/MainActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectMembers(Lcom/squareup/ui/main/MainActivity;)V

    return-void
.end method
