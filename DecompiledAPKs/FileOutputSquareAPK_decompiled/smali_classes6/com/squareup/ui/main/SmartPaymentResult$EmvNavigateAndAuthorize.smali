.class public final Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;
.super Ljava/lang/Object;
.source "SmartPaymentResult.java"

# interfaces
.implements Lcom/squareup/ui/main/SmartPaymentResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SmartPaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmvNavigateAndAuthorize"
.end annotation


# instance fields
.field public final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method


# virtual methods
.method public dispatch(Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;)V
    .locals 0

    .line 88
    invoke-interface {p1, p0}, Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;->onResult(Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;)V

    return-void
.end method
