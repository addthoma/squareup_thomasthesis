.class public final Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;
.super Ljava/lang/Object;
.source "SmartPaymentResult.java"

# interfaces
.implements Lcom/squareup/ui/main/SmartPaymentResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SmartPaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DoNothing"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;

    invoke-direct {v0}, Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatch(Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;)V
    .locals 0

    .line 29
    invoke-interface {p1, p0}, Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;->onResult(Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;)V

    return-void
.end method
