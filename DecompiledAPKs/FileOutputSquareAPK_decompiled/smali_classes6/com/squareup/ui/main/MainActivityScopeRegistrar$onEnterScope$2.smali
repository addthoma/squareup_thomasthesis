.class final Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "MainActivityScopeRegistrar.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityScopeRegistrar;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/account/AccountEvents$SessionExpired;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/account/AccountEvents$SessionExpired;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$2;->this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$2;->invoke(Lcom/squareup/account/AccountEvents$SessionExpired;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/account/AccountEvents$SessionExpired;)V
    .locals 0

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$2;->this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    invoke-static {p1}, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->access$getSessionExpiredListener$p(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)Lcom/squareup/account/SessionExpiredListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/account/SessionExpiredListener;->onSessionExpired()V

    return-void
.end method
