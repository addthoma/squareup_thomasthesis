.class public interface abstract Lcom/squareup/ui/main/SmartPaymentResult;
.super Ljava/lang/Object;
.source "SmartPaymentResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;,
        Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;,
        Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;,
        Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;,
        Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;,
        Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;,
        Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;
    }
.end annotation


# virtual methods
.method public abstract dispatch(Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;)V
.end method
