.class final enum Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$4;
.super Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const/4 v0, 0x0

    .line 141
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;-><init>(Ljava/lang/String;IILcom/squareup/ui/main/r12education/R12EducationView$1;)V

    return-void
.end method


# virtual methods
.method buildContent(Landroid/view/ViewGroup;Lcom/squareup/CountryCode;)Landroid/view/View;
    .locals 2

    .line 143
    sget p2, Lcom/squareup/readertutorial/R$layout;->r12_education_panel_dip:I

    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->access$100(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 148
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/readertutorial/R$drawable;->r12_education_chip_card:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 149
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    .line 150
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_dip_matte_container:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    .line 151
    invoke-virtual {v0, v1, v1, p1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    return-object p2
.end method

.method getLogDetailFromPanel()Ljava/lang/String;
    .locals 1

    const-string v0, "Third Page (Dip a Card)"

    return-object v0
.end method
