.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$9;
.super Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateStandardPanelTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V
    .locals 0

    .line 417
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$9;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;-><init>()V

    return-void
.end method


# virtual methods
.method tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 1

    .line 421
    sget-object p4, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$14;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$Element:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->ordinal()I

    move-result p1

    aget p1, p4, p1

    const/4 p4, 0x0

    const/high16 v0, 0x3e800000    # 0.25f

    packed-switch p1, :pswitch_data_0

    .line 453
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->hide(Landroid/view/View;)V

    goto :goto_0

    :pswitch_0
    const p1, 0x3ea8f5c3    # 0.33f

    const/high16 p5, 0x3f800000    # 1.0f

    .line 445
    invoke-static {p3, p1, p5}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->fadeIn(Landroid/view/View;F)V

    .line 446
    invoke-static {p2, p7}, Lcom/squareup/ui/main/r12education/Tweens;->centerY(Landroid/view/View;Landroid/view/View;)V

    .line 447
    invoke-static {p2, p7, p6, p4, p3}, Lcom/squareup/ui/main/r12education/Tweens;->slideInRightToAlignCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    goto :goto_0

    :pswitch_1
    const/4 p1, 0x0

    .line 435
    invoke-static {p3, p1, v0}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 437
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 438
    invoke-static {p2, p5}, Lcom/squareup/ui/main/r12education/Tweens;->alignBottomEdgeToBottomY(Landroid/view/View;Landroid/view/View;)V

    .line 439
    invoke-static {p2, p5, p4, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutLeftFromAlignCenter(Landroid/view/View;Landroid/view/View;IF)V

    goto :goto_0

    :pswitch_2
    const/high16 p1, 0x3f400000    # 0.75f

    .line 425
    invoke-static {p3, v0, p1}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 427
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 428
    invoke-static {p2, p5}, Lcom/squareup/ui/main/r12education/Tweens;->centerX(Landroid/view/View;Landroid/view/View;)V

    .line 429
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$9;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneR4BottomMargin:I

    invoke-static {p2, p5, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutFromAlignBottom(Landroid/view/View;Landroid/view/View;IF)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
