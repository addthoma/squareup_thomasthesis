.class final enum Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$3;
.super Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const/4 v0, 0x0

    .line 121
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;-><init>(Ljava/lang/String;IILcom/squareup/ui/main/r12education/R12EducationView$1;)V

    return-void
.end method


# virtual methods
.method buildContent(Landroid/view/ViewGroup;Lcom/squareup/CountryCode;)Landroid/view/View;
    .locals 0

    .line 123
    sget p2, Lcom/squareup/readertutorial/R$layout;->r12_education_panel_charge:I

    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->access$100(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method getLogDetailFromPanel()Ljava/lang/String;
    .locals 1

    const-string v0, "Second Page (Charge Your Reader)"

    return-object v0
.end method

.method overTweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Lcom/squareup/ui/main/r12education/TransitionView;F)V
    .locals 1

    .line 132
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->DOTS_ORANGE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    if-ne p1, v0, :cond_0

    const p1, 0x3ea8f5c3    # 0.33f

    const/high16 v0, 0x3f800000    # 1.0f

    .line 133
    invoke-static {p3, p1, v0}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/main/r12education/TransitionView;->setProgress(F)V

    :cond_0
    return-void
.end method

.method shouldOverTween()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
