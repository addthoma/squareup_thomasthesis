.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;
.super Ljava/lang/Object;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;,
        Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
    }
.end annotation


# static fields
.field private static final easeIn:Landroid/view/animation/Interpolator;

.field private static final easeInOut:Landroid/view/animation/Interpolator;

.field private static final easeOut:Landroid/view/animation/Interpolator;


# instance fields
.field private knownTransitions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lkotlin/Pair<",
            "Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;",
            "Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;",
            ">;",
            "Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;",
            ">;"
        }
    .end annotation
.end field

.field private values:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 58
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->easeOut:Landroid/view/animation/Interpolator;

    .line 59
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->easeIn:Landroid/view/animation/Interpolator;

    .line 60
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->easeInOut:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->knownTransitions:Ljava/util/Map;

    .line 67
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;-><init>(Landroid/content/res/Resources;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$1;)V

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->values:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateStandardPanelTransitions()V

    .line 69
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateCountryPrefersContactlessCardsTransitions()V

    return-void
.end method

.method static synthetic access$100()Landroid/view/animation/Interpolator;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->easeInOut:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$200()Landroid/view/animation/Interpolator;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->easeOut:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->values:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    return-object p0
.end method

.method static synthetic access$400()Landroid/view/animation/Interpolator;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->easeIn:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method private addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V
    .locals 3

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->knownTransitions:Ljava/util/Map;

    new-instance v1, Lkotlin/Pair;

    const-string v2, "old R12 education panel"

    invoke-static {p1, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    const-string v2, "new R12 education panel"

    .line 79
    invoke-static {p2, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    invoke-direct {v1, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 78
    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
    .locals 1

    .line 83
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$1;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    return-object v0
.end method

.method private populateCountryPrefersContactlessCardsTransitions()V
    .locals 4

    .line 489
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$11;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$11;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 549
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 550
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 552
    new-instance v1, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$12;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$12;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 583
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 584
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v1

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 586
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$13;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$13;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 636
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 637
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    return-void
.end method

.method private populateStandardPanelTransitions()V
    .locals 4

    .line 94
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$2;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 135
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 136
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 137
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME_DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 138
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME_DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 140
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$3;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 193
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 194
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 196
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$4;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 248
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 249
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 251
    new-instance v1, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$5;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$5;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 283
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 284
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v1

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 286
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$6;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$6;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 343
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 344
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 346
    new-instance v1, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$7;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$7;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 377
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 378
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v1

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 380
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$8;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 414
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->VIDEO:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 415
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->VIDEO:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 417
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$9;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$9;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 459
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->START_SELLING:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 460
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->START_SELLING:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 462
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$10;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$10;-><init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V

    .line 484
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->VIDEO:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->START_SELLING:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    .line 485
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->START_SELLING:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->VIDEO:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->buildReverseTransition(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->addTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V

    return-void
.end method


# virtual methods
.method getTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->knownTransitions:Ljava/util/Map;

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    return-object p1
.end method
