.class public final Lcom/squareup/ui/main/r12education/workflow/R12TutorialViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "R12TutorialViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/main/r12education/workflow/R12TutorialViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "reader-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 9
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 10
    sget-object v2, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->Companion:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 11
    sget v3, Lcom/squareup/readertutorial/R$layout;->r12_speed_test:I

    .line 12
    new-instance v4, Lcom/squareup/workflow/ScreenHint;

    invoke-direct {v4}, Lcom/squareup/workflow/ScreenHint;-><init>()V

    .line 13
    sget-object v5, Lcom/squareup/ui/main/r12education/workflow/R12TutorialViewFactory$1;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/R12TutorialViewFactory$1;

    move-object v6, v5

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    .line 9
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 8
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
