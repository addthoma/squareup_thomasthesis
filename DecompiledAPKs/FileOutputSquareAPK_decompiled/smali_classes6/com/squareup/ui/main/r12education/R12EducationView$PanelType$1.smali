.class final enum Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$1;
.super Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const/4 v0, 0x0

    .line 86
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;-><init>(Ljava/lang/String;IILcom/squareup/ui/main/r12education/R12EducationView$1;)V

    return-void
.end method


# virtual methods
.method buildContent(Landroid/view/ViewGroup;Lcom/squareup/CountryCode;)Landroid/view/View;
    .locals 0

    .line 88
    sget p2, Lcom/squareup/readertutorial/R$layout;->r12_education_panel_welcome:I

    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->access$100(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method getButtonText()I
    .locals 1

    .line 92
    sget v0, Lcom/squareup/readertutorial/R$string;->r12_education_welcome_button:I

    return v0
.end method

.method getLogDetailFromPanel()Ljava/lang/String;
    .locals 1

    const-string v0, "First Page (Reader Connected)"

    return-object v0
.end method
