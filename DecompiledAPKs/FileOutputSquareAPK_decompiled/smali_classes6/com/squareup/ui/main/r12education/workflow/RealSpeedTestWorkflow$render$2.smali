.class final Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSpeedTestWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->render(Lkotlin/Unit;Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;",
        "+",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        "duration",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

.field final synthetic this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->$state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(J)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->$state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

    check-cast v0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;

    invoke-virtual {v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;->getDurations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x6

    if-ge v0, v3, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->access$getSpeedTestLogger$p(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;->add(J)V

    .line 85
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v3, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;

    iget-object v4, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->$state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

    check-cast v4, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;

    invoke-virtual {v4}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;->getDurations()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {v4, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v3, p1, v2, v1, v2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;-><init>(Ljava/util/List;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {v0, v3, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 87
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    iget-object p2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->$state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

    check-cast p2, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;

    invoke-virtual {p2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;->getDurations()Ljava/util/List;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->access$getAverageDuration(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;Ljava/util/List;)J

    move-result-wide p1

    const-wide/16 v3, 0xa0

    cmp-long v0, p1, v3

    if-gez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 89
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    invoke-static {v3}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->access$getSpeedTestLogger$p(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;->log(J)V

    .line 90
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance p2, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Finished;

    invoke-direct {p2, v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Finished;-><init>(Z)V

    invoke-static {p1, p2, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 40
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;->invoke(J)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
