.class public abstract Lcom/squareup/ui/main/PosMainActivityComponent$Module;
.super Ljava/lang/Object;
.source "PosMainActivityComponent.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/activity/ActivityAppletModule;,
        Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponModule;,
        Lcom/squareup/applet/AppletsModule;,
        Lcom/squareup/balance/activity/BalanceActivityMainModule;,
        Lcom/squareup/brandaudio/BrandAudioModule;,
        Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutModule;,
        Lcom/squareup/ui/buyer/BuyerFlowModule;,
        Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowModule;,
        Lcom/squareup/cardreader/dipper/CardReaderUIDipperModule;,
        Lcom/squareup/ui/cart/CartFeesModel$Module;,
        Lcom/squareup/checkoutflow/CheckoutflowDefaultTenderOptionsModule;,
        Lcom/squareup/checkoutflow/CheckoutflowModule;,
        Lcom/squareup/connectedscalesdata/ConnectedScalesModule;,
        Lcom/squareup/register/tutorial/CommonTutorialModule;,
        Lcom/squareup/items/tutorial/CreateItemTutorialModule;,
        Lcom/squareup/depositschedule/DepositScheduleModule;,
        Lcom/squareup/ui/crm/edit/EditCustomerModule;,
        Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowModule;,
        Lcom/squareup/permissions/ui/EmployeesScreensModule;,
        Lcom/squareup/feetutorial/FeeTutorialModule;,
        Lcom/squareup/filepicker/FilePickerModule;,
        Lcom/squareup/invoices/image/FileViewerModule;,
        Lcom/squareup/checkoutflow/installments/InstallmentsModule;,
        Lcom/squareup/instantdeposit/InstantDepositModule;,
        Lcom/squareup/invoices/InvoicesAppletModule;,
        Lcom/squareup/invoices/tutorial/InvoiceTutorialModule;,
        Lcom/squareup/banklinking/LinkBankAccountModule;,
        Lcom/squareup/debitcard/LinkDebitCardModule;,
        Lcom/squareup/ui/onboarding/bank/MainActivityDepositOptionsModule;,
        Lcom/squareup/api/multipassauth/MultipassMainActivityScopeModule;,
        Lcom/squareup/notificationcenter/applet/NotificationCenterAppletModule;,
        Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeModule;,
        Lcom/squareup/notificationcenterlauncher/NotificationCenterLauncherModule;,
        Lcom/squareup/notificationcenter/NotificationCenterModule;,
        Lcom/squareup/notificationcenterdata/NotificationResolverModule;,
        Lcom/squareup/notificationcenterdata/db/NotificationStateStoreModule;,
        Lcom/squareup/ui/onboarding/OnboardingModule;,
        Lcom/squareup/onboarding/flow/OnboardingWorkflowModule;,
        Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutModule;,
        Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsModule;,
        Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderBillPaymentForkModule;,
        Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowModule;,
        Lcom/squareup/ui/orderhub/OrderHubMainActivityScopeModule;,
        Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpModule;,
        Lcom/squareup/checkoutflow/payother/PayOtherModule;,
        Lcom/squareup/payment/pending/PendingPaymentsModule;,
        Lcom/squareup/permissionworkflow/PermissionWorkflowModule;,
        Lcom/squareup/ui/main/PosMainWorkflowModule;,
        Lcom/squareup/print/PrintModule$MainActivity;,
        Lcom/squareup/quickamounts/settings/QuickAmountsSettingsModule;,
        Lcom/squareup/crm/RealEmailCollectionSettingsModule;,
        Lcom/squareup/print/popup/error/RealPrintErrorPopupModule;,
        Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeModule;,
        Lcom/squareup/crm/RolodexLoaders$GroupLoaderModule;,
        Lcom/squareup/crm/RolodexLoaders$MerchantLoaderModule;,
        Lcom/squareup/scales/ScalesModule;,
        Lcom/squareup/scales/ScalesWorkflowModule;,
        Lcom/squareup/ui/settings/SettingsAppletMainActivityModule;,
        Lcom/squareup/sonicbranding/SonicBrandModule;,
        Lcom/squareup/ui/tender/TenderPaymentLegacyModule;,
        Lcom/squareup/tmn/TmnModule;,
        Lcom/squareup/ui/timecards/TimecardsModule;,
        Lcom/squareup/checkoutflow/core/tip/TipModule;,
        Lcom/squareup/balance/transferout/TransferOutModule;,
        Lcom/squareup/transferreports/TransferReportsModule;,
        Lcom/squareup/tour/TourModule;,
        Lcom/squareup/tutorialv2/TutorialV2Module;,
        Lcom/squareup/ui/crm/flow/UpdateCustomerModule;,
        Lcom/squareup/debitcard/VerifyCardChangeModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/PosMainActivityComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAuthorizingEmployeeBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;"
        }
    .end annotation

    .line 404
    const-class v0, Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/protos/client/Employee;

    invoke-static {p0, v0, v1}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideWorkingDiscountBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;"
        }
    .end annotation

    .line 400
    const-class v0, Lcom/squareup/configure/item/WorkingDiscount;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/configure/item/WorkingDiscount;

    invoke-static {p0, v0, v1}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static registerTutorials(Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)Ljava/util/List;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 p0, 0x1

    aput-object p1, v0, p0

    const/4 p0, 0x2

    aput-object p2, v0, p0

    const/4 p0, 0x3

    aput-object p3, v0, p0

    .line 318
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract bindBarcodeScannerHudToaster(Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindCartFeesModelSessionBundler(Lcom/squareup/ui/cart/CartFeesModel$Session;)Lmortar/bundler/Bundler;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindCashDrawerHudToasterScoped(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindCheckoutScopeParentProvider(Lcom/squareup/ui/main/CommonPosMainWorkflowCheckoutScopeParentProvider;)Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindEditInvoiceGateway(Lcom/squareup/invoices/edit/EditInvoiceGateway;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindEnabledTaxCounterAsScoped(Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindIntentParser(Lcom/squareup/ui/main/PosIntentParser;)Lcom/squareup/ui/main/IntentParser;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindOrderEntryPagesRootScoped(Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindRedirectPipelineDecorator(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;)Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindRolodexMerchantLoader(Lcom/squareup/crm/RolodexMerchantLoader;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindScaleHudToaster(Lcom/squareup/scales/ScaleHudToaster;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindSerialScaleDiscoverer(Lcom/squareup/scales/SerialUsbScaleDiscoverer;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindShareUrlLauncher(Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;)Lcom/squareup/url/InvoiceShareUrlLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindStarScaleDiscoverer(Lcom/squareup/scales/StarScaleDiscoverer;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract goBackAfterWarning(Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;)Lcom/squareup/ui/main/errors/GoBackAfterWarning;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideActivateEGiftCardWorkflowRunner(Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideActivityAppletGateway(Lcom/squareup/ui/activity/RealActivityAppletGateway;)Lcom/squareup/ui/activity/ActivityAppletGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideActivityAppletStarter(Lcom/squareup/ui/activity/RealActivityAppletStarter;)Lcom/squareup/ui/activity/ActivityAppletStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAdjustInventoryGateway(Lcom/squareup/ui/inventory/RealAdjustInventoryStarter;)Lcom/squareup/ui/inventory/AdjustInventoryStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCardPresentRefundFactory(Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;)Lcom/squareup/ui/activity/CardPresentRefundFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideDuplicateSkuResultController(Lcom/squareup/sku/RealDuplicateSkuResultController;)Lcom/squareup/sku/DuplicateSkuResultController;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEditInvoiceInTenderRunner(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEditItemGateway(Lcom/squareup/ui/items/RealEditItemGateway;)Lcom/squareup/ui/items/EditItemGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEditWorkflowRunner(Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideHelpAppletGateway(Lcom/squareup/ui/help/RealHelpAppletGateway;)Lcom/squareup/ui/help/HelpAppletGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideHelpAppletHistoryBuilder(Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;)Lcom/squareup/ui/help/HelpAppletHistoryBuilder;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideItemsAppletScreenGateway(Lcom/squareup/ui/items/RealItemsAppletGateway;)Lcom/squareup/ui/items/ItemsAppletGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSettingsAppletGateway(Lcom/squareup/ui/settings/RealSettingsAppletGateway;)Lcom/squareup/ui/settings/SettingsAppletGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTutorialHomeIdentifier(Lcom/squareup/orderentry/RealV1TutorialHomeIdentifier;)Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providesPaymentRequestV2Workflow(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract sessionExpiredListener(Lcom/squareup/ui/main/ShowDialogOnSessionExpired;)Lcom/squareup/account/SessionExpiredListener;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
