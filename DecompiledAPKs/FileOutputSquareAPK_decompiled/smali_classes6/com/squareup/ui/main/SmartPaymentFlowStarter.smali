.class public Lcom/squareup/ui/main/SmartPaymentFlowStarter;
.super Ljava/lang/Object;
.source "SmartPaymentFlowStarter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/payment/Transaction;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/BuyerFlowStarter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 91
    iput-object p2, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    .line 92
    iput-object p3, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 93
    iput-object p4, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

    .line 94
    iput-object p5, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 95
    iput-object p6, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    .line 96
    iput-object p7, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 97
    iput-object p8, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 98
    iput-object p9, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 99
    iput-object p10, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 100
    iput-object p11, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    .line 101
    iput-object p12, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 102
    iput-object p13, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/buyer/BuyerFlowStarter;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    return-object p0
.end method

.method private checkCanStartContactlessSplitTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
    .locals 1

    .line 429
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkSplitTenderPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 430
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_SPLIT_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 432
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkRegister()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    return-object v0
.end method

.method private checkCardReader()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
    .locals 2

    .line 480
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 481
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_ACTIVE_CARD_READER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    .line 486
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v1

    if-nez v1, :cond_1

    .line 487
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_SECURE_SESSION:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 490
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result v1

    if-nez v1, :cond_2

    .line 491
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_VALID_TMS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 494
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isBatteryDead()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 495
    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastIfBatteryDead(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 496
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->BATTERY_DEAD:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 499
    :cond_3
    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_INSERTED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkFirmwareUpdateNotBlocking(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 500
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->BLOCKING_FIRMWARE_UPDATE:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 503
    :cond_4
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0
.end method

.method private checkRegister()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
    .locals 4

    .line 440
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v0}, Lcom/squareup/cardreader/DippedCardTracker;->mustReinsertDippedCard()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReinsertChipCardToCharge()Z

    .line 448
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->REINSERT_CHIP:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 451
    :cond_1
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 441
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->unable_to_process_chip_cards:I

    sget v3, Lcom/squareup/cardreader/ui/R$string;->please_restore_internet_connectivity:I

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastLongHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    .line 443
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_ONLINE:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0
.end method

.method private checkSingleTenderPayment()Z
    .locals 1

    .line 465
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/BillPayment;

    .line 469
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v0

    return v0
.end method

.method private checkSplitTenderPayment()Z
    .locals 1

    .line 458
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private getSingleTenderInBuyerFlowResult(Z)Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->maybeCancelAndRemoveGlass()Z

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v1}, Lcom/squareup/payment/tender/TenderFactory;->createSmartCard()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    .line 196
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->setStartedWithChargeButton(Z)V

    .line 199
    iget-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1, v1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 200
    iget-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cancelPaymentsOnNonActiveCardReaders()V

    .line 202
    sget-object p1, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;

    return-object p1
.end method

.method private prepareContactlessSingleTenderInBuyerFlow()V
    .locals 4

    .line 391
    invoke-virtual {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartContactlessSingleTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    .line 392
    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot start contactless single. Result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 395
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->prepareSingleTenderInBuyerFlow()V

    .line 396
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    .line 398
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method private prepareContactlessSplitTenderInBuyerFlow()V
    .locals 1

    .line 373
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->updateTenderInEditAndStartPaymentOnReader(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    return-void
.end method

.method private prepareSingleTenderInBuyerFlow()V
    .locals 2

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 379
    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v1

    if-nez v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v1}, Lcom/squareup/payment/tender/TenderFactory;->createSmartCard()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    .line 381
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 382
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v1, v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0, v1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    goto :goto_0

    .line 386
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->updateTenderInEditAndStartPaymentOnReader(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    :goto_0
    return-void
.end method

.method private updateTenderInEditAndStartPaymentOnReader(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 2

    .line 402
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 403
    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 404
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 405
    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    .line 406
    iget-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method


# virtual methods
.method public checkCanStartContactlessSingleTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
    .locals 1

    .line 416
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkSingleTenderPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_SINGLE_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 419
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkRegister()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    return-object v0
.end method

.method public checkCanStartSingleTenderEmvInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
    .locals 2

    .line 131
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCardReader()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    .line 132
    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-eq v0, v1, :cond_0

    return-object v0

    .line 136
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkSingleTenderPayment()Z

    move-result v0

    if-nez v0, :cond_1

    .line 137
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_SINGLE_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0

    .line 140
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkRegister()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    return-object v0
.end method

.method public checkFirmwareUpdateNotBlocking(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;)Z
    .locals 3

    .line 113
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastCrucialFirmwareUpdate(Lcom/squareup/cardreader/CardReaderInfo;)Z

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v2, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 116
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 115
    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public getBuyerFlowWithOfflineApprovalResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 6

    .line 218
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkRegister()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    .line 219
    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Register cannot start a smart card payment. Result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    xor-int/2addr v0, v3

    const-string v1, "We have TC from the reader, there shouldn\'t be any readers in the middle of a payment"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    const-string v1, "Should already have a tenderInEdit by the time we get auth bytes"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v2, 0x1

    :cond_1
    const-string v0, "Can only set cardReaderInfo if it was not previously set."

    invoke-static {v2, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 230
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 231
    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1, p2}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardAuthRequestData(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;[B)V

    .line 232
    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setTenderApprovedOffline()V

    .line 234
    iget-object p2, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p2}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    .line 236
    iget-object p2, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 238
    new-instance p2, Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;

    invoke-direct {p2, p1}, Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-object p2
.end method

.method public getContactlessTenderHardwarePinRequest(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/securetouch/SecureTouchPinRequestData;)Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;
    .locals 2

    .line 364
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartContactlessSplitTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    .line 365
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->prepareContactlessSplitTenderInBuyerFlow()V

    goto :goto_0

    .line 367
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->prepareContactlessSingleTenderInBuyerFlow()V

    .line 369
    :goto_0
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;-><init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-object v0
.end method

.method public getContactlessTenderInBuyerFlowResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 2

    .line 250
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartContactlessSplitTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p1

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 253
    invoke-virtual {p1, v0, p2}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardAuthRequestData(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;[B)V

    .line 254
    iget-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    .line 256
    iget-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    iget-object p2, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 257
    sget-object p1, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;

    return-object p1

    .line 260
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartContactlessSingleTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne p1, v0, :cond_1

    .line 261
    invoke-virtual {p0, p2}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->setContactlessSingleTenderAuthDataOnTransaction([B)V

    .line 262
    sget-object p1, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;

    return-object p1

    .line 265
    :cond_1
    sget-object p1, Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;

    return-object p1
.end method

.method public getContactlessTenderPinRequest(Lcom/squareup/cardreader/PinRequestData;)Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;
    .locals 2

    .line 353
    invoke-static {p1}, Lcom/squareup/ui/buyer/ContactlessPinRequest;->create(Lcom/squareup/cardreader/PinRequestData;)Lcom/squareup/ui/buyer/ContactlessPinRequest;

    move-result-object p1

    .line 354
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartContactlessSplitTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    .line 355
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->prepareContactlessSplitTenderInBuyerFlow()V

    goto :goto_0

    .line 357
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->prepareContactlessSingleTenderInBuyerFlow()V

    .line 359
    :goto_0
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;

    invoke-direct {v0, p1}, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;-><init>(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V

    return-object v0
.end method

.method public getContinueContactlessTenderMaybeAfterErrorResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 3

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getContactlessTenderInBuyerFlowResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    return-object p1

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 335
    sget-object p1, Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;

    return-object p1

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasAuthData()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    const-string v2, "We already have auth data for this tender, we should not be re-authing"

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 342
    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1, p2}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardAuthRequestData(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;[B)V

    .line 344
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 345
    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    .line 346
    iget-object p2, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 348
    new-instance p2, Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;

    invoke-direct {p2, p1}, Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-object p2
.end method

.method public getOrContinueEmvTenderMaybeAfterErrorResult(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    invoke-virtual {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getSingleTenderDipWithoutChargeButtonResult()Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    return-object p1

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 314
    sget-object p1, Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;

    return-object p1

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 319
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 320
    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 323
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;

    invoke-direct {v0, p1}, Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-object v0
.end method

.method public getPreAuthContactlessSingleTenderInBuyerFlowResult()Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 4

    .line 206
    invoke-virtual {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartContactlessSingleTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    .line 207
    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Single tender contactless payment cannot start. Result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 210
    invoke-direct {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->prepareSingleTenderInBuyerFlow()V

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->maybeCancelAndRemoveGlass()Z

    .line 213
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;

    return-object v0
.end method

.method public getPreAuthContactlessSingleTenderPinRequest(Lcom/squareup/cardreader/PinRequestData;)Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 299
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 302
    :cond_0
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;

    invoke-static {p1}, Lcom/squareup/ui/buyer/ContactlessPinRequest;->create(Lcom/squareup/cardreader/PinRequestData;)Lcom/squareup/ui/buyer/ContactlessPinRequest;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;-><init>(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V

    return-object v0
.end method

.method public getSingleTenderDipWithChargeButtonResult()Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 5

    .line 185
    invoke-virtual {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartSingleTenderEmvInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    .line 186
    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Single tender dip charge cannot start. Result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 188
    invoke-direct {p0, v2}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getSingleTenderInBuyerFlowResult(Z)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object v0

    return-object v0
.end method

.method public getSingleTenderDipWithoutChargeButtonResult()Lcom/squareup/ui/main/SmartPaymentResult;
    .locals 2

    .line 144
    invoke-virtual {p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartSingleTenderEmvInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 145
    invoke-direct {p0, v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getSingleTenderInBuyerFlowResult(Z)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object v0

    return-object v0

    .line 148
    :cond_0
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;

    return-object v0
.end method

.method public navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 1

    .line 152
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;-><init>(Lcom/squareup/ui/main/SmartPaymentFlowStarter;)V

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/SmartPaymentResult;->dispatch(Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;)V

    return-void
.end method

.method public setContactlessSingleTenderAuthDataOnTransaction([B)V
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-nez v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v1}, Lcom/squareup/payment/tender/TenderFactory;->createSmartCard()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 282
    iget-object v1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    .line 284
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 285
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardAuthRequestData(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;[B)V

    .line 286
    invoke-virtual {v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    .line 288
    iget-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method
