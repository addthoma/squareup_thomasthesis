.class public abstract Lcom/squareup/ui/main/AppletsDrawerActionBarNavigationHelperModule;
.super Ljava/lang/Object;
.source "AppletsDrawerActionBarNavigationHelperModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindAppletsDrawerActionBarNavigationHelper(Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;)Lcom/squareup/applet/ActionBarNavigationHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
