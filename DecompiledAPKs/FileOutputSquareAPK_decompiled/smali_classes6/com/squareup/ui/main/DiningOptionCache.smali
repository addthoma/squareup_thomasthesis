.class public Lcom/squareup/ui/main/DiningOptionCache;
.super Ljava/lang/Object;
.source "DiningOptionCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/DiningOptionCache$Update;
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final diningOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/main/DiningOptionCache;->cogsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/main/DiningOptionCache;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 31
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    return-void
.end method

.method private updateAndAnnounceChanges(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;)V"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/DiningOptionCache;->setDiningOptions(Ljava/util/List;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/main/DiningOptionCache;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/ui/main/DiningOptionCache$Update;

    invoke-direct {v0}, Lcom/squareup/ui/main/DiningOptionCache$Update;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public chooseNextDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/DiningOption;
    .locals 1

    const-string v0, "CurrentDiningOption"

    .line 64
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rem-int/2addr p1, v0

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/DiningOption;

    return-object p1
.end method

.method public getDefaultDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/DiningOption;

    :goto_0
    return-object v0
.end method

.method public getDiningOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$loadAndPost$0$DiningOptionCache(Ljava/lang/Runnable;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 40
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-static {p2}, Lcom/squareup/checkout/DiningOption;->diningOptionsFrom(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/main/DiningOptionCache;->updateAndAnnounceChanges(Ljava/util/List;)V

    if-eqz p1, :cond_0

    .line 41
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method public loadAndPost()V
    .locals 1

    const/4 v0, 0x0

    .line 35
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/DiningOptionCache;->loadAndPost(Ljava/lang/Runnable;)V

    return-void
.end method

.method public loadAndPost(Ljava/lang/Runnable;)V
    .locals 3

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    sget-object v1, Lcom/squareup/ui/main/-$$Lambda$D-nvUdnK8FJfHVpChL5gz0r_M14;->INSTANCE:Lcom/squareup/ui/main/-$$Lambda$D-nvUdnK8FJfHVpChL5gz0r_M14;

    new-instance v2, Lcom/squareup/ui/main/-$$Lambda$DiningOptionCache$WKxGSOx5FndYQBdJ6PeCbhby-uI;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/main/-$$Lambda$DiningOptionCache$WKxGSOx5FndYQBdJ6PeCbhby-uI;-><init>(Lcom/squareup/ui/main/DiningOptionCache;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public setDiningOptions(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;)V"
        }
    .end annotation

    const-string v0, "ListOfDiningOptions"

    .line 50
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache;->diningOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method
