.class Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ButtonFlowStarter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/ButtonFlowStarter;->killTenderAndTurnOffReader(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

.field final synthetic val$reason:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;->this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    iput-object p2, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;->val$reason:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;->this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-static {v0}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->access$100(Lcom/squareup/ui/main/errors/ButtonFlowStarter;)Lcom/squareup/ui/NfcProcessorInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/NfcProcessorInterface;->cancelPaymentOnAllContactlessReaders()V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;->this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-static {v0}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->access$000(Lcom/squareup/ui/main/errors/ButtonFlowStarter;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;->val$reason:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;->this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;->val$reason:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->goHome(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method
