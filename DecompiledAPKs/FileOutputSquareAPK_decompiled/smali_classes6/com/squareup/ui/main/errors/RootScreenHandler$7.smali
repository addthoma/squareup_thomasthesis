.class Lcom/squareup/ui/main/errors/RootScreenHandler$7;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;->goToSettingScreen(ILjava/lang/String;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

.field final synthetic val$setting:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/RootScreenHandler;Ljava/lang/String;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$7;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    iput-object p2, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$7;->val$setting:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 179
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 180
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$7;->val$setting:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
