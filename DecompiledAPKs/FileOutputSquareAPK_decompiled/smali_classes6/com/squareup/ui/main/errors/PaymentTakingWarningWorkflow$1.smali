.class Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$1;
.super Ljava/lang/Object;
.source "PaymentTakingWarningWorkflow.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->register(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$1;->this$0:Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$1;->this$0:Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    invoke-static {p1}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->access$000(Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;)Lcom/squareup/ui/main/errors/PaymentInputHandler;

    move-result-object p1

    const-wide/16 v0, 0xfa0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithNfcFieldAutoRestart(J)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$1;->this$0:Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->access$000(Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;)Lcom/squareup/ui/main/errors/PaymentInputHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->destroy()V

    return-void
.end method
