.class public Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;
.super Lcom/squareup/ui/main/errors/RootScreenHandler;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TalkBackEnabledScreenHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method constructor <init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 656
    invoke-direct/range {p0 .. p12}, Lcom/squareup/ui/main/errors/RootScreenHandler;-><init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 2

    .line 681
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 682
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->talk_back_warning_header:I

    .line 683
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->talk_back_warning_message:I

    .line 684
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 686
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;->goToHomeScreenIfTalkbackDisabled(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    .line 685
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->talk_back_go_to_accessibility_setting:I

    const-string v1, "android.settings.ACCESSIBILITY_SETTINGS"

    .line 687
    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;->goToSettingScreen(ILjava/lang/String;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->secondButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 689
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method

.method protected goToHomeScreenIfTalkbackDisabled(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 2

    .line 669
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    new-instance v1, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler$1;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method protected toastMustCancelTalkback()V
    .locals 4

    .line 662
    invoke-static {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler;->access$100(Lcom/squareup/ui/main/errors/RootScreenHandler;)Lcom/squareup/hudtoaster/HudToaster;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_120:I

    sget v2, Lcom/squareup/cardreader/ui/R$string;->talk_back_please_close_header:I

    sget v3, Lcom/squareup/cardreader/ui/R$string;->talk_back_please_close:I

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(III)Z

    return-void
.end method
