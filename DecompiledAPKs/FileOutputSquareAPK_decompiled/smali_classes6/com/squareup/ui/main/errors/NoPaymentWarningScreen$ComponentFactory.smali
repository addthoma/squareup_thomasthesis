.class public Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "NoPaymentWarningScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 41
    const-class v0, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ParentComponent;

    .line 42
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ParentComponent;

    .line 43
    check-cast p2, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;

    .line 44
    new-instance v0, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Module;-><init>(Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;)V

    .line 45
    invoke-interface {p1, v0}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ParentComponent;->noPaymentWarningScreen(Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Module;)Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Component;

    move-result-object p1

    return-object p1
.end method
