.class final Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;
.super Ljava/lang/Object;
.source "MainActivityContainer.kt"

# interfaces
.implements Lcom/squareup/container/DispatchStep;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityContainer;->buildDispatchPipeline(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMainActivityContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MainActivityContainer.kt\ncom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2\n*L\n1#1,532:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/DispatchStep$Result;",
        "kotlin.jvm.PlatformType",
        "traversal",
        "Lflow/Traversal;",
        "callback",
        "Lflow/TraversalCallback;",
        "dispatch"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispatch(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 5

    .line 355
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 357
    iget-object v1, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    .line 358
    iget-object v2, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v2}, Lcom/squareup/ui/main/MainActivityContainer;->access$getView(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/main/MainActivityScope$View;

    move-result-object v2

    .line 359
    sget-object v3, Lcom/squareup/container/spot/ContainerSpots;->INSTANCE:Lcom/squareup/container/spot/ContainerSpots;

    const-string v4, "newScreen"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    const-string v4, "traversal.direction"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1, v0, p1}, Lcom/squareup/container/spot/ContainerSpots;->isEnteringFullScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    .line 360
    invoke-interface {v2}, Lcom/squareup/ui/main/MainActivityScope$View;->startEnterFullScreen()V

    move-object v1, v2

    .line 362
    :cond_1
    new-instance p1, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;

    invoke-direct {p1, p0, v1, p2, v0}, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;-><init>(Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;Lcom/squareup/ui/main/MainActivityScope$View;Lflow/TraversalCallback;Lcom/squareup/container/ContainerTreeKey;)V

    check-cast p1, Lflow/TraversalCallback;

    const-string p2, "MainActivityScope: transition cleanup"

    invoke-static {p2, p1}, Lcom/squareup/container/DispatchStep$Result;->wrap(Ljava/lang/String;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1
.end method
