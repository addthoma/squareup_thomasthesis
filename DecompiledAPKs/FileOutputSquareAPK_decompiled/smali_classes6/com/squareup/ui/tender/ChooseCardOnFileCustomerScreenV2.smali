.class public Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "ChooseCardOnFileCustomerScreenV2.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Component;,
        Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;
    }
.end annotation


# instance fields
.field private final cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;->cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;->cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    return-object p0
.end method

.method public static create(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/InTenderScope;
    .locals 1

    .line 64
    new-instance v0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)V

    return-object v0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CHOOSE_COF_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 281
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->payment_choose_card_on_file_customer_view_crm_v2:I

    return v0
.end method
