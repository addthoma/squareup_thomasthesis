.class public final Lcom/squareup/ui/tender/TenderScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "TenderScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/register/tutorial/R6VideoLauncherAllowed;
.implements Lcom/squareup/ui/tender/TenderScopeTreeKey;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/TenderScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/TenderScope$Component;,
        Lcom/squareup/ui/tender/TenderScope$ParentComponent;,
        Lcom/squareup/ui/tender/TenderScope$RunnerModule;,
        Lcom/squareup/ui/tender/TenderScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/TenderScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/tender/TenderScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {v0}, Lcom/squareup/ui/tender/TenderScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    .line 106
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/TenderScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getRegisterTreeKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 0

    return-object p0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 2

    .line 39
    const-class v0, Lcom/squareup/ui/tender/TenderScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderScope$Component;

    .line 40
    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderScope$Component;->scopeRunner()Lcom/squareup/tenderpayment/TenderScopeRunner;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 41
    new-instance v1, Lcom/squareup/ui/tender/TenderScope$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/tender/TenderScope$1;-><init>(Lcom/squareup/ui/tender/TenderScope;Lcom/squareup/ui/tender/TenderScope$Component;)V

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
