.class public Lcom/squareup/ui/tender/PayThirdPartyCardView;
.super Landroid/widget/LinearLayout;
.source "PayThirdPartyCardView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/PayThirdPartyCardView$Component;
    }
.end annotation


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private helpText:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recordButton:Landroid/widget/Button;

.field private tenderAmount:Lcom/squareup/noho/NohoEditText;

.field tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const-class p2, Lcom/squareup/ui/tender/PayThirdPartyCardView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/PayThirdPartyCardView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/PayThirdPartyCardView$Component;->inject(Lcom/squareup/ui/tender/PayThirdPartyCardView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 119
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 120
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->pay_third_party_helper_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->helpText:Lcom/squareup/widgets/MessageView;

    .line 121
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->pay_third_party_tender_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->recordButton:Landroid/widget/Button;

    .line 122
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->pay_third_party_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    return-void
.end method


# virtual methods
.method configureAmount(Lcom/squareup/money/MoneyLocaleHelper;)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1, v0}, Lcom/squareup/money/MoneyLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    return-void
.end method

.method enableRecordPayment(Z)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->recordButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method getInputText()Ljava/lang/String;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method hideSoftKeyboard()V
    .locals 0

    .line 111
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$requestInitialFocus$0$PayThirdPartyCardView()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->requestFocus()Z

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->bindViews()V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/ui/tender/legacy/R$string;->record_payment_action_label:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    new-instance v1, Lcom/squareup/ui/tender/PayThirdPartyCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/PayThirdPartyCardView$1;-><init>(Lcom/squareup/ui/tender/PayThirdPartyCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {v1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$drawable;->marin_white_border_bottom_light_gray_1px:I

    .line 59
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setBackground(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->presenter:Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->hideSoftKeyboard()V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->presenter:Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->presenter:Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->dropView(Ljava/lang/Object;)V

    .line 67
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method requestInitialFocus()V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$PayThirdPartyCardView$74BaXv5rzbz2D-LEDVKx1ST2LM0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$PayThirdPartyCardView$74BaXv5rzbz2D-LEDVKx1ST2LM0;-><init>(Lcom/squareup/ui/tender/PayThirdPartyCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setUpTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showButton()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hidePrimaryButton()V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->recordButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->recordButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/tender/PayThirdPartyCardView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/PayThirdPartyCardView$2;-><init>(Lcom/squareup/ui/tender/PayThirdPartyCardView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method updateAmountDue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderAmount:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->helpText:Lcom/squareup/widgets/MessageView;

    sget v0, Lcom/squareup/payment/R$string;->any_amount_over:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void
.end method
