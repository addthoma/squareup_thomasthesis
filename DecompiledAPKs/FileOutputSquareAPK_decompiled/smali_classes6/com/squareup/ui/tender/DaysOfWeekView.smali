.class public Lcom/squareup/ui/tender/DaysOfWeekView;
.super Landroid/widget/LinearLayout;
.source "DaysOfWeekView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/DaysOfWeekView$ParentComponent;
    }
.end annotation


# instance fields
.field locale:Ljava/util/Locale;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const-class p2, Lcom/squareup/ui/tender/DaysOfWeekView$ParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/DaysOfWeekView$ParentComponent;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/DaysOfWeekView$ParentComponent;->inject(Lcom/squareup/ui/tender/DaysOfWeekView;)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/tender/DaysOfWeekView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 33
    iget-object p2, p0, Lcom/squareup/ui/tender/DaysOfWeekView;->locale:Ljava/util/Locale;

    invoke-static {p1, p2}, Lcom/squareup/ui/tender/DaysOfWeekHelper;->getDaysOfWeek(Landroid/content/res/Resources;Ljava/util/Locale;)Ljava/util/List;

    move-result-object p2

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/tender/DaysOfWeekView;->removeAllViews()V

    .line 36
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 37
    new-instance v1, Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/tender/DaysOfWeekView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    .line 38
    new-instance v2, Landroid/widget/TableLayout$LayoutParams;

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, -0x2

    invoke-direct {v2, v4, v4, v3}, Landroid/widget/TableLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v2, 0x1

    .line 40
    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 41
    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 42
    sget v2, Lcom/squareup/widgets/R$dimen;->text_android_small:I

    .line 43
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 42
    invoke-virtual {v1, v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    .line 44
    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 45
    sget v0, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 46
    invoke-virtual {p0, v1}, Lcom/squareup/ui/tender/DaysOfWeekView;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method
