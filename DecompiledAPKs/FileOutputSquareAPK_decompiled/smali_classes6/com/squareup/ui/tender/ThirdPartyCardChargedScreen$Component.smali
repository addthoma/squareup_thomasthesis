.class public interface abstract Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen$Component;
.super Ljava/lang/Object;
.source "ThirdPartyCardChargedScreen.java"

# interfaces
.implements Lcom/squareup/ui/tender/PayThirdPartyCardView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation
