.class public final Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "PayCardCnpDisabledScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;,
        Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;

    invoke-direct {v0}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;->INSTANCE:Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;

    .line 38
    sget-object v0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;->INSTANCE:Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;

    .line 39
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 42
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->pay_card_cnp_disabled_view:I

    return v0
.end method
