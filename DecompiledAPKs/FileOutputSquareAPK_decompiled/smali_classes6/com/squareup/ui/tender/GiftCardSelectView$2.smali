.class Lcom/squareup/ui/tender/GiftCardSelectView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "GiftCardSelectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/GiftCardSelectView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/GiftCardSelectView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/GiftCardSelectView;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/tender/GiftCardSelectView$2;->this$0:Lcom/squareup/ui/tender/GiftCardSelectView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/tender/GiftCardSelectView$2;->this$0:Lcom/squareup/ui/tender/GiftCardSelectView;

    iget-object p1, p1, Lcom/squareup/ui/tender/GiftCardSelectView;->presenter:Lcom/squareup/ui/tender/GiftCardSelectPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->scanOrEnterGiftCardClicked()V

    return-void
.end method
