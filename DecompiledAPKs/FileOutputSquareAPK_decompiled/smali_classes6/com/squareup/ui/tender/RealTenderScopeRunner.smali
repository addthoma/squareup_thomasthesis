.class public Lcom/squareup/ui/tender/RealTenderScopeRunner;
.super Ljava/lang/Object;
.source "RealTenderScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/tenderpayment/TenderScopeRunner;


# instance fields
.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

.field private final context:Landroid/content/Context;

.field private final flow:Lflow/Flow;

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final res:Lcom/squareup/util/Res;

.field private final tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/util/Res;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lflow/Flow;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/text/Formatter;Landroid/app/Application;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/ui/NfcProcessor;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/TouchEventMonitor;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Landroid/app/Application;",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    move-object v1, p2

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object v1, p3

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p4

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    move-object v1, p5

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p6

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    move-object v1, p7

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object/from16 v1, p16

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->context:Landroid/content/Context;

    move-object v1, p8

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-object v1, p9

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    move-object v1, p10

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->flow:Lflow/Flow;

    move-object v1, p11

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    move-object v1, p12

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    move-object v1, p13

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    move-object/from16 v1, p14

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    move-object/from16 v1, p15

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->formatter:Lcom/squareup/text/Formatter;

    move-object/from16 v1, p17

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    return-void
.end method

.method private buildTenderActionBarConfigBuilder(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    if-eqz p2, :cond_0

    .line 272
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 273
    :goto_0
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 274
    invoke-virtual {v0, p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/tender/-$$Lambda$frK-Cn9c9FpR4EquTvcyQR_pF1c;

    invoke-direct {v0, p2}, Lcom/squareup/ui/tender/-$$Lambda$frK-Cn9c9FpR4EquTvcyQR_pF1c;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    .line 275
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    return-object p1
.end method

.method private static cancelSplitTenderScreenData()Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;
    .locals 7

    .line 300
    new-instance v6, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    sget v1, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_confirm:I

    sget v2, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_cancel:I

    sget v3, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_title:I

    sget v4, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_message_split_tender:I

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;-><init>(IIIILjava/lang/String;)V

    return-object v6
.end method

.method private gatedCancelSplitTender()V
    .locals 3

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/tender/RealTenderScopeRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner$1;-><init>(Lcom/squareup/ui/tender/RealTenderScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method private getFormattedGrandTotal()Ljava/lang/CharSequence;
    .locals 2

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->formatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getTenderActionBarTitle()Ljava/lang/CharSequence;
    .locals 4

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasOrderTicketName()Z

    move-result v0

    if-nez v0, :cond_1

    .line 284
    invoke-virtual {p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->getFormattedTotal()Ljava/lang/CharSequence;

    move-result-object v0

    .line 285
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_amount:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "amount"

    .line 287
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 288
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    :cond_0
    return-object v0

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/tender/legacy/R$string;->kitchen_printing_price_name_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 294
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->getFormattedGrandTotal()Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "price"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 295
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrderTicketName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->context:Landroid/content/Context;

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v2, v3}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    const-string v2, "name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 296
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$onEnterScope$1(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 125
    const-class v0, Lcom/squareup/ui/tender/TenderScope;

    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$onEnterScope$3(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 133
    const-class v0, Lcom/squareup/ui/tender/TenderScope;

    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    return p0
.end method

.method private resetTransactionAndGoToInvoiceDetail()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireInvoiceId()Ljava/lang/String;

    move-result-object v0

    .line 266
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->reset()V

    .line 267
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v1, v0}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->invoicePaymentCanceled(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0, p1}, Lcom/squareup/ui/tender/TenderStarter;->advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z

    move-result p1

    return p1
.end method

.method public asCancelSplitTenderDialogScreen()Lcom/squareup/workflow/legacy/Screen;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
            ">;"
        }
    .end annotation

    .line 175
    invoke-static {}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->cancelSplitTenderScreenData()Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$jOPKeXMhbpLjPgO7cOLdiPS65DQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$jOPKeXMhbpLjPgO7cOLdiPS65DQ;-><init>(Lcom/squareup/ui/tender/RealTenderScopeRunner;)V

    .line 176
    invoke-static {v1}, Lcom/squareup/ui/tender/WorkflowInputs;->forJava(Lio/reactivex/functions/Consumer;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 174
    invoke-static {v0, v1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog;->forJava(Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    return-object v0
.end method

.method public availabilityOffline()V
    .locals 5

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 244
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;->INSTANCE:Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen;->INSTANCE:Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen;

    .line 247
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->flow:Lflow/Flow;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;

    aput-object v4, v2, v3

    invoke-static {v1, v0, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public availabilityOnline()V
    .locals 5

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;

    sget-object v2, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    iget-object v3, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    .line 233
    invoke-interface {v3}, Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;->getScreenData()Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 230
    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public buildActionBarConfigSplittable(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 6

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 155
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->getTenderActionBarTitle()Ljava/lang/CharSequence;

    move-result-object v1

    .line 156
    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->buildTenderActionBarConfigBuilder(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 157
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/tender/legacy/R$string;->split_tender_secondary_button:I

    .line 158
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 159
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 160
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method public buildTenderActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 1

    const/4 v0, 0x0

    .line 141
    invoke-virtual {p0, v0}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->buildTenderActionBarConfig(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method public buildTenderActionBarConfig(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 0

    .line 150
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->buildTenderActionBarConfigBuilder(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method public buildTenderActionBarConfig(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 1

    .line 145
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->getTenderActionBarTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->buildTenderActionBarConfig(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method cancelPaymentAndExit(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    move-result-object p1

    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_API_TRANSACTION:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    if-eq p1, v0, :cond_0

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {p1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    :cond_0
    return-void
.end method

.method public completeTenderAndAdvance(Z)Z
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0, p1}, Lcom/squareup/ui/tender/TenderStarter;->completeTenderAndAdvance(Z)Z

    move-result p1

    return p1
.end method

.method public getFormattedTotal()Ljava/lang/CharSequence;
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPaymentWithAtLeastOneTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->formatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 168
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->getFormattedGrandTotal()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$asCancelSplitTenderDialogScreen$5$RealTenderScopeRunner(Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 177
    sget-object v0, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$cancelsplit$CancelSplitTenderTransactionDialog$Event:[I

    invoke-virtual {p1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 179
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->gatedCancelSplitTender()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$0$RealTenderScopeRunner(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {p1}, Lcom/squareup/log/CheckoutInformationEventLogger;->tap()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$RealTenderScopeRunner(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/NfcProcessor;->stopMonitoringSoon()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$4$RealTenderScopeRunner(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {p1}, Lcom/squareup/log/CheckoutInformationEventLogger;->showScreen()V

    return-void
.end method

.method public onBackPressedOnFirstTenderScreen()Z
    .locals 4

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 198
    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderCompleter;->cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    move-result-object v0

    .line 200
    sget-object v1, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CancelPaymentResult:[I

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    return v0

    .line 210
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown cancel result :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    return v2

    .line 202
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->resetTransactionAndGoToInvoiceDetail()V

    return v2
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->startCheckoutIfHasNot()V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static {p1}, Lcom/squareup/mortar/MortarScopes;->completeOnExit(Lmortar/MortarScope;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimer(Lio/reactivex/Completable;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    .line 111
    invoke-interface {v0}, Lcom/squareup/ui/TouchEventMonitor;->observeTouchEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$2rnYSaXteF95lCYpbVI0KQ4seRE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$2rnYSaXteF95lCYpbVI0KQ4seRE;-><init>(Lcom/squareup/ui/tender/RealTenderScopeRunner;)V

    .line 112
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 110
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 124
    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$DMX8L5PVV7MqQ1tzIsqzyY2GUBA;->INSTANCE:Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$DMX8L5PVV7MqQ1tzIsqzyY2GUBA;

    .line 125
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    .line 127
    invoke-interface {v1}, Lcom/squareup/ui/main/CheckoutWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->skipUntil(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$JSa1yY6hDtSjdxgwnQWj8ZX-XY8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$JSa1yY6hDtSjdxgwnQWj8ZX-XY8;-><init>(Lcom/squareup/ui/tender/RealTenderScopeRunner;)V

    .line 128
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 123
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 132
    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$Bxo0lWYsgjSvM_kcrU7b49v2Tgw;->INSTANCE:Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$Bxo0lWYsgjSvM_kcrU7b49v2Tgw;

    .line 133
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$rGz49yYtruEt-cRig53A6_7hGmg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$RealTenderScopeRunner$rGz49yYtruEt-cRig53A6_7hGmg;-><init>(Lcom/squareup/ui/tender/RealTenderScopeRunner;)V

    .line 134
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 131
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onGiftCardOnFileSelected()V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->GIFT_CARD:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    invoke-static {v1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;->create(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/InTenderScope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onOrderNameCommitted()V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    return-void
.end method
