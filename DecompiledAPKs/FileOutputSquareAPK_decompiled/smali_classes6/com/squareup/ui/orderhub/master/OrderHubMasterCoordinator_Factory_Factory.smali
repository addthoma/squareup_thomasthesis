.class public final Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "OrderHubMasterCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubBackButtonConfigProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->badgePresenterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->orderHubBackButtonConfigProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 59
    new-instance v7, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;)Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;
    .locals 8

    .line 66
    new-instance v7, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;
    .locals 7

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/applet/BadgePresenter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->orderHubBackButtonConfigProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/applet/AppletSelection;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;)Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator_Factory_Factory;->get()Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
