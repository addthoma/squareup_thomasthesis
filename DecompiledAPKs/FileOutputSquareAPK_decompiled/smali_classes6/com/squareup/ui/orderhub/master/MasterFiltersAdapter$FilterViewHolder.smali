.class public final Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;
.super Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;
.source "MasterFiltersAdapter.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "FilterViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMasterFiltersAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MasterFiltersAdapter.kt\ncom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,132:1\n1103#2,7:133\n*E\n*S KotlinDebug\n*F\n+ 1 MasterFiltersAdapter.kt\ncom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder\n*L\n95#1,7:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0080\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0007R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;",
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;",
        "Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;",
        "itemView",
        "Landroid/view/View;",
        "(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;Landroid/view/View;)V",
        "edges",
        "",
        "getEdges",
        "()I",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "getRow",
        "()Lcom/squareup/noho/NohoRow;",
        "bind",
        "",
        "position",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final edges:I

.field private final row:Lcom/squareup/noho/NohoRow;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string v0, "itemView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->this$0:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    const/16 p1, 0x8

    .line 88
    iput p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->edges:I

    .line 89
    sget p1, Lcom/squareup/orderhub/applet/R$id;->orderhub_master_filter_row:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->row:Lcom/squareup/noho/NohoRow;

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.noho.NohoRow"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final bind(I)V
    .locals 3

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->this$0:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->access$getFilterList$p(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "filterList[position]"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;

    .line 93
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->row:Lcom/squareup/noho/NohoRow;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->this$0:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->access$getRes$p(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;)Lcom/squareup/util/Res;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->row:Lcom/squareup/noho/NohoRow;

    check-cast v1, Landroid/view/View;

    .line 133
    new-instance v2, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;Lcom/squareup/ui/orderhub/master/Filter;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->row:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->isSelected()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setActivated(Z)V

    .line 99
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->getOrderCount()I

    move-result p1

    if-lez p1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->row:Lcom/squareup/noho/NohoRow;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 103
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->row:Lcom/squareup/noho/NohoRow;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public getEdges()I
    .locals 1

    .line 88
    iget v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->edges:I

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)V
    .locals 1

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges$DefaultImpls;->getPadding(Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;Landroid/graphics/Rect;)V

    return-void
.end method

.method public final getRow()Lcom/squareup/noho/NohoRow;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->row:Lcom/squareup/noho/NohoRow;

    return-object v0
.end method
