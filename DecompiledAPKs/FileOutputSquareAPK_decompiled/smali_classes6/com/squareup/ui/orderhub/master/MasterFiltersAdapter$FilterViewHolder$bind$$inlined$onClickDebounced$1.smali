.class public final Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->bind(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 MasterFiltersAdapter.kt\ncom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder\n*L\n1#1,1322:1\n96#2,2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $filter$inlined:Lcom/squareup/ui/orderhub/master/Filter;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;Lcom/squareup/ui/orderhub/master/Filter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;->$filter$inlined:Lcom/squareup/ui/orderhub/master/Filter;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->this$0:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->access$getScreen$p(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;)Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event$FilterSelected;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;->$filter$inlined:Lcom/squareup/ui/orderhub/master/Filter;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event$FilterSelected;-><init>(Lcom/squareup/ui/orderhub/master/Filter;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
