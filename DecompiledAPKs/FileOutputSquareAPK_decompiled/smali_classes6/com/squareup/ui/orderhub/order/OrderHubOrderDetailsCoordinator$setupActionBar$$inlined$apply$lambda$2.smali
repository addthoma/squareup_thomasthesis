.class final Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubOrderDetailsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->setupActionBar(ZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$2$2$1",
        "com/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$$special$$inlined$also$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $isReadOnly$inlined:Z

.field final synthetic $it:Lcom/squareup/protos/client/orders/Action;

.field final synthetic $order$inlined:Lcom/squareup/orders/model/Order;

.field final synthetic $screen$inlined:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

.field final synthetic $showOrderIdInActionBar$inlined:Z

.field final synthetic $this_apply$inlined:Lcom/squareup/noho/NohoActionBar$Config$Builder;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/orders/Action;Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;ZLcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->$it:Lcom/squareup/protos/client/orders/Action;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->$this_apply$inlined:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->$showOrderIdInActionBar$inlined:Z

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->$order$inlined:Lcom/squareup/orders/model/Order;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->$screen$inlined:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    iput-boolean p7, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->$isReadOnly$inlined:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->$screen$inlined:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;->$it:Lcom/squareup/protos/client/orders/Action;

    invoke-direct {v1, v2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;-><init>(Lcom/squareup/protos/client/orders/Action;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
