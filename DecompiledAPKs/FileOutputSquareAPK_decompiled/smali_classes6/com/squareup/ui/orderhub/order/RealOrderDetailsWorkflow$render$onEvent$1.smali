.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "event",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "event"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 337
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-eqz v2, :cond_3

    .line 338
    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v6, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v2}, Lcom/squareup/protos/client/orders/Action$Type;->ordinal()I

    move-result v2

    aget v2, v6, v2

    if-eq v2, v3, :cond_2

    if-eq v2, v4, :cond_1

    .line 360
    :goto_0
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 361
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 362
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v7

    .line 363
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v8

    .line 364
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v9

    .line 365
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v12

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 367
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getPickupTimeOverride()Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x10

    const/4 v15, 0x0

    move-object v6, v3

    .line 361
    invoke-direct/range {v6 .. v15}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 360
    invoke-static {v2, v3, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_2

    .line 349
    :cond_1
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 350
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    .line 351
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v7

    .line 352
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v8

    .line 353
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v9

    .line 354
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v11

    const/4 v10, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xe0

    const/16 v16, 0x0

    move-object v6, v3

    .line 350
    invoke-direct/range {v6 .. v16}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 349
    invoke-static {v2, v3, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_2

    .line 339
    :cond_2
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 340
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    .line 341
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v7

    .line 342
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v8

    .line 343
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v9

    .line 344
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v12

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/16 v13, 0x10

    const/4 v14, 0x0

    move-object v6, v3

    .line 340
    invoke-direct/range {v6 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 339
    invoke-static {v2, v3, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_2

    .line 372
    :cond_3
    sget-object v2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickAdjustPickupTime;->INSTANCE:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickAdjustPickupTime;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 373
    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;

    .line 374
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    .line 375
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v6

    .line 376
    iget-object v7, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v7, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    invoke-virtual {v7}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getPickupTimeOverride()Ljava/lang/String;

    move-result-object v7

    .line 373
    invoke-direct {v2, v3, v6, v7}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;-><init>(Lcom/squareup/orders/model/Order;ZLjava/lang/String;)V

    .line 372
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_2

    .line 379
    :cond_4
    sget-object v2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ViewTransactionDetail;->INSTANCE:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ViewTransactionDetail;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 380
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v3, "state.order.id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_VIEW_TRANSACTION_DETAIL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    .line 381
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 382
    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    .line 383
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v7

    .line 384
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v8

    .line 385
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xf8

    const/16 v16, 0x0

    move-object v6, v2

    .line 382
    invoke-direct/range {v6 .. v16}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 381
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_2

    .line 389
    :cond_5
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;

    if-eqz v2, :cond_7

    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 390
    new-instance v15, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    .line 391
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v7

    .line 392
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v8

    .line 393
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 394
    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;->getFulfillment()Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v12

    .line 395
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v14

    .line 396
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v1

    if-eqz v1, :cond_6

    const/4 v13, 0x1

    goto :goto_1

    :cond_6
    const/4 v3, 0x0

    const/4 v13, 0x0

    :goto_1
    const/16 v1, 0x18

    const/16 v16, 0x0

    move-object v6, v15

    move-object v3, v15

    move v15, v1

    .line 390
    invoke-direct/range {v6 .. v16}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/orders/model/Order$Fulfillment;ZLcom/squareup/ordermanagerdata/TrackingInfo;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 389
    invoke-static {v2, v3, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_2

    .line 399
    :cond_7
    sget-object v2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$GoBackFromOrderDetails;->INSTANCE:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$GoBackFromOrderDetails;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CloseOrder;

    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CloseOrder;-><init>(Lcom/squareup/orders/model/Order;)V

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_2
    return-object v1

    :cond_8
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;->invoke(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
