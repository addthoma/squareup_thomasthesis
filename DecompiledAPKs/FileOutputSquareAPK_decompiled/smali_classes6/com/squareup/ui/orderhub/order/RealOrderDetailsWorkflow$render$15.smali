.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderDetailsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderDetailsWorkflow.kt\ncom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15\n*L\n1#1,885:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "event",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "event"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 568
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryBillRetrieval;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryBillRetrieval;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x0

    if-eqz v2, :cond_3

    .line 569
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getRetryCancellation()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 570
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 572
    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    .line 573
    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v6

    .line 574
    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v7

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 579
    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v2

    const-string v5, "Required value was null."

    if-eqz v2, :cond_1

    .line 580
    iget-object v13, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v13, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    invoke-virtual {v13}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 578
    new-instance v14, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    invoke-direct {v14, v2, v13}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;-><init>(Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V

    const/16 v2, 0x60

    const/4 v15, 0x0

    .line 571
    new-instance v13, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    move-object v5, v13

    move-object v3, v13

    move-object v13, v14

    move v14, v2

    invoke-direct/range {v5 .. v15}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v2, 0x2

    .line 570
    invoke-static {v1, v3, v4, v2, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 580
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 579
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 585
    :cond_2
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 586
    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-object v5, v2

    check-cast v5, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x37

    const/4 v15, 0x0

    invoke-static/range {v5 .. v15}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    move-result-object v2

    const/4 v3, 0x2

    .line 585
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 592
    :cond_3
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelBillRetrieval;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelBillRetrieval;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 593
    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 594
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v6

    .line 595
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 596
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x78

    const/4 v14, 0x0

    move-object v5, v2

    .line 593
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v3, 0x2

    .line 592
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_0
    return-object v1

    .line 599
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected error dialog event "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;->invoke(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
