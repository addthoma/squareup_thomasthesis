.class public final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInputKt;
.super Ljava/lang/Object;
.source "OrderEditTrackingInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0014\u0010\u0004\u001a\u00020\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0002H\u0000\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0000\u0010\u0003\u00a8\u0006\u0008"
    }
    d2 = {
        "isNonUSLocale",
        "",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;)Z",
        "initialState",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;",
        "accountStatusSettings",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final initialState(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;
    .locals 10

    const-string v0, "$this$initialState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    .line 24
    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInputKt;->isNonUSLocale(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result v4

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getShouldShowRemoveTracking()Z

    move-result v5

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v6

    .line 27
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getDisplayTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getTrackingInfo(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    :goto_0
    move-object v7, p1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getDisplayTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getTrackingNumber()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_1

    .line 29
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getTrackingInfo(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getTrackingNumber()Ljava/lang/String;

    move-result-object p1

    :goto_1
    move-object v8, p1

    goto :goto_2

    :cond_2
    move-object v8, v1

    .line 30
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getDisplayTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getCarrierName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    move-object v9, p1

    goto :goto_3

    .line 31
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getTrackingInfo(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getCarrierName()Ljava/lang/String;

    move-result-object p0

    move-object v9, p0

    goto :goto_3

    :cond_4
    move-object v9, v1

    :goto_3
    const/4 v3, 0x0

    move-object v1, v0

    .line 21
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;-><init>(Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final isNonUSLocale(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 1

    const-string v0, "$this$isNonUSLocale"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p0

    const-string v0, "userSettings"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p0

    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
