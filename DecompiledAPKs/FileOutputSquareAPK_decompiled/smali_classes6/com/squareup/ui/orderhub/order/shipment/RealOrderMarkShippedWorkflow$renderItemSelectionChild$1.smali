.class final Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$renderItemSelectionChild$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderMarkShippedWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->renderItemSelectionChild(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        "it",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$renderItemSelectionChild$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 227
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$renderItemSelectionChild$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 228
    sget-object v7, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;->EDIT_TRACKING:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    const/4 v6, 0x0

    .line 229
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v5

    const/4 v8, 0x0

    const/16 v9, 0x57

    const/4 v10, 0x0

    .line 227
    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->copy$default(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    move-result-object p1

    const/4 v1, 0x2

    .line 226
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 232
    :cond_0
    instance-of p1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$GoBackFromMarkShipped;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$GoBackFromMarkShipped;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$renderItemSelectionChild$1;->invoke(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
