.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderDetailsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderDetailsWorkflow.kt\ncom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9\n*L\n1#1,885:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "event",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "event"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 415
    instance-of v2, v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$ResetOrderDetailsScreen;

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 416
    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 417
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v6

    .line 418
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 419
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x78

    const/4 v14, 0x0

    move-object v5, v2

    .line 416
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 415
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_1

    .line 422
    :cond_0
    instance-of v2, v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CurrentOrderUpdated;

    if-eqz v2, :cond_1

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 423
    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    .line 424
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v5

    .line 425
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v6

    .line 426
    iget-object v7, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v7}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v7

    .line 423
    invoke-direct {v2, v5, v6, v7}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;-><init>(ZZLcom/squareup/orders/model/Order;)V

    .line 422
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_1

    .line 429
    :cond_1
    instance-of v2, v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;

    if-eqz v2, :cond_5

    .line 430
    check-cast v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    sget-object v5, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v2}, Lcom/squareup/protos/client/orders/Action$Type;->ordinal()I

    move-result v2

    aget v2, v5, v2

    const/4 v5, 0x1

    if-eq v2, v5, :cond_4

    if-eq v2, v3, :cond_3

    .line 450
    :goto_0
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 451
    new-instance v15, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 452
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v6

    .line 453
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 454
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    .line 455
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v11

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 457
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getPickupTimeOverride()Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x10

    const/4 v14, 0x0

    move-object v5, v15

    .line 451
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 450
    invoke-static {v2, v15, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_1

    .line 441
    :cond_3
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 442
    new-instance v15, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    .line 443
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v6

    .line 444
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 445
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    const/4 v9, 0x0

    .line 446
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xe8

    const/4 v1, 0x0

    move-object v5, v15

    move-object v3, v15

    move-object v15, v1

    .line 442
    invoke-direct/range {v5 .. v15}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v1, 0x2

    .line 441
    invoke-static {v2, v3, v4, v1, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_1

    .line 432
    :cond_4
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 433
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    .line 434
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v6

    .line 435
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 436
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 437
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v11

    const/16 v12, 0x18

    const/4 v13, 0x0

    move-object v5, v3

    .line 433
    invoke-direct/range {v5 .. v13}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v1, 0x2

    .line 432
    invoke-static {v2, v3, v4, v1, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_1

    .line 462
    :cond_5
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$GoBackFromOrderDetails;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$GoBackFromOrderDetails;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 463
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CloseOrder;

    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CloseOrder;-><init>(Lcom/squareup/orders/model/Order;)V

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_1
    return-object v1

    .line 464
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected error dialog event "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;->invoke(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
