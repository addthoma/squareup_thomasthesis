.class public final Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOrderMarkShippedWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderMarkShippedWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderMarkShippedWorkflow.kt\ncom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,258:1\n85#2:259\n240#3:260\n276#4:261\n149#5,5:262\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderMarkShippedWorkflow.kt\ncom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow\n*L\n116#1:259\n116#1:260\n116#1:261\n172#1,5:262\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B\'\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J \u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u001a\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u00032\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J \u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u00032\u0006\u0010!\u001a\u00020\u00032\u0006\u0010\"\u001a\u00020\u0004H\u0016JN\u0010#\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001c\u001a\u00020\u00032\u0006\u0010\"\u001a\u00020\u00042\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050%H\u0016JF\u0010&\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\"\u001a\u00020\u00042\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050%H\u0002JF\u0010\'\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\"\u001a\u00020\u00042\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050%H\u0002J\u0010\u0010(\u001a\u00020\u001e2\u0006\u0010\"\u001a\u00020\u0004H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "orderItemSelectionWorkflow",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
        "orderEditTrackingWorkflow",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;)V",
        "getInitialState",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "markShippedAction",
        "Lcom/squareup/protos/client/orders/Action;",
        "canShowErrorDialog",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "renderEditTrackingChild",
        "renderItemSelectionChild",
        "snapshotState",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final orderEditTrackingWorkflow:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;

.field private final orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

.field private final orderItemSelectionWorkflow:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderItemSelectionWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEditTrackingWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderRepository"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderItemSelectionWorkflow:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderEditTrackingWorkflow:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    return-void
.end method

.method private final getInitialState(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Z)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;
    .locals 9

    .line 242
    iget-object v0, p2, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    sget-object v1, Lcom/squareup/protos/client/orders/Action$Type;->MARK_SHIPPED:Lcom/squareup/protos/client/orders/Action$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Landroidx/core/util/Preconditions;->checkState(Z)V

    .line 243
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    const/4 v4, 0x0

    .line 247
    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->getAvailableLineItemsToRowIdentifiers(Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object v5

    .line 248
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/ActionsKt;->getCanApplyToLineItems(Lcom/squareup/protos/client/orders/Action;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 249
    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;->ITEM_SELECTION:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    goto :goto_1

    .line 251
    :cond_1
    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;->EDIT_TRACKING:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    :goto_1
    move-object v7, v1

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    move v3, p3

    move-object v8, p2

    .line 243
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;-><init>(Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;)V

    return-object v0
.end method

.method private final renderEditTrackingChild(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 181
    new-instance v2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;

    .line 182
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 184
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x0

    .line 181
    invoke-direct {v2, v0, v4, v1, v3}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;-><init>(Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderEditTrackingWorkflow:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    .line 190
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$renderEditTrackingChild$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$renderEditTrackingChild$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p2

    .line 187
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method

.method private final renderItemSelectionChild(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 216
    new-instance v2, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    .line 217
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 218
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getMarkShippedAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    .line 219
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v3

    .line 216
    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderItemSelectionWorkflow:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 224
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$renderItemSelectionChild$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$renderItemSelectionChild$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p2

    .line 221
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;
    .locals 1

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p2

    .line 79
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;->getMarkShippedAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v0

    .line 80
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;->getCanShowErrorDialog()Z

    move-result p1

    .line 77
    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->getInitialState(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Z)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->initialState(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-object p3

    .line 93
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    .line 94
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;->getMarkShippedAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object p3

    .line 95
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;->getCanShowErrorDialog()Z

    move-result p2

    .line 92
    invoke-direct {p0, p1, p3, p2}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->getInitialState(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Z)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;

    check-cast p3, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->onPropsChanged(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->render(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getNextShipmentAction()Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;->MARK_SHIPPED:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    if-ne p1, v0, :cond_1

    .line 106
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    const-string v0, "state.order.id"

    if-nez p1, :cond_0

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_SKIP_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    .line 109
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_SHIPPED:Lcom/squareup/protos/client/orders/Action$Type;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logAction$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/protos/client/orders/Action$Type;)V

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 112
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 113
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v1

    .line 114
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->selectedLineItemQuantities(Lcom/squareup/orders/model/Order;Ljava/util/Map;)Ljava/util/List;

    move-result-object v2

    .line 111
    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/ordermanagerdata/OrderRepository;->shipOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    .line 259
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 260
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 261
    const-class v0, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/orders/model/Order;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    .line 120
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "shipping_order "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ".order.id "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ".order.version"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 121
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 118
    invoke-interface {p3, v1, p1, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 148
    :cond_1
    sget-object p1, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$spinningScreenEventHandler$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$spinningScreenEventHandler$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 152
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getNextShipmentAction()Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 p3, 0x3

    if-ne v0, p3, :cond_2

    .line 168
    sget-object p3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 169
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen;

    invoke-direct {v0, p2, p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen;-><init>(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 263
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 264
    const-class p2, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 265
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 263
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 168
    invoke-virtual {p3, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 161
    :cond_3
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->renderEditTrackingChild(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    .line 162
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 163
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/legacy/Screen;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 164
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lcom/squareup/workflow/legacy/Screen;

    const/16 v6, 0xe

    const/4 v7, 0x0

    .line 162
    invoke-static/range {v0 .. v7}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 154
    :cond_4
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->renderItemSelectionChild(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    .line 155
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 156
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/legacy/Screen;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 157
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lcom/squareup/workflow/legacy/Screen;

    const/16 v6, 0xe

    const/4 v7, 0x0

    .line 155
    invoke-static/range {v0 .. v7}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public snapshotState(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->snapshotState(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
