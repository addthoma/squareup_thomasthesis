.class public final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;
.super Ljava/lang/Object;
.source "OrderHubCancellationProcessingCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final screensProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;>;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;->screensProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;>;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lio/reactivex/Observable;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator;-><init>(Lio/reactivex/Observable;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;->screensProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Observable;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;->newInstance(Lio/reactivex/Observable;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator_Factory;->get()Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingCoordinator;

    move-result-object v0

    return-object v0
.end method
