.class final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubTrackingRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->configureActionBar(Ljava/lang/String;Ljava/lang/String;ZZZLcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubTrackingRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubTrackingRunner.kt\ncom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2\n*L\n1#1,328:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $canSaveTrackingInfo:Z

.field final synthetic $carrierName:Ljava/lang/String;

.field final synthetic $isReadOnly:Z

.field final synthetic $rendering:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

.field final synthetic $showSkipTracking:Z

.field final synthetic $trackingNumber:Ljava/lang/String;


# direct methods
.method constructor <init>(ZZLcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$isReadOnly:Z

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$showSkipTracking:Z

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$rendering:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$canSaveTrackingInfo:Z

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$trackingNumber:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$carrierName:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    .line 253
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$isReadOnly:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$showSkipTracking:Z

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$rendering:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$SubmitTracking;

    invoke-direct {v2, v1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$SubmitTracking;-><init>(Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    invoke-interface {v0, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 255
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$isReadOnly:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$canSaveTrackingInfo:Z

    if-eqz v0, :cond_3

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$rendering:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 259
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$trackingNumber:Ljava/lang/String;

    const-string v3, "Required value was null."

    if-eqz v2, :cond_2

    .line 260
    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;->$carrierName:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 258
    new-instance v3, Lcom/squareup/ordermanagerdata/TrackingInfo;

    invoke-direct {v3, v4, v2, v1}, Lcom/squareup/ordermanagerdata/TrackingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    new-instance v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$SubmitTracking;

    invoke-direct {v1, v3}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$SubmitTracking;-><init>(Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 260
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 259
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    :goto_0
    return-void
.end method
