.class public final Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOrderEditTrackingWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderEditTrackingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderEditTrackingWorkflow.kt\ncom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,141:1\n41#2:142\n56#2,2:143\n276#3:145\n149#4,5:146\n149#4,5:151\n149#4,5:156\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderEditTrackingWorkflow.kt\ncom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow\n*L\n76#1:142\n76#1,2:143\n76#1:145\n123#1,5:146\n127#1,5:151\n136#1,5:156\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001a\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00032\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J \u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u0004H\u0016JN\u0010\u0018\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0004H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInputKt;->initialState(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->initialState(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 66
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p2, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInputKt;->initialState(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-result-object p3

    :goto_0
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;

    check-cast p3, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->onPropsChanged(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->render(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {p1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "connectivityMonitor.internetState()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string v0, "this.toFlowable(BUFFER)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_1

    .line 144
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 145
    const-class v0, Lcom/squareup/connectivity/InternetState;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 77
    new-instance p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 75
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 82
    new-instance p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$screenEventHandler$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$screenEventHandler$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 91
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$errorDialogEventHandler$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$errorDialogEventHandler$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 103
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->getUnsubmittedTrackingNumber()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$editTrackingNumberEditableText$1;

    invoke-direct {v2, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$editTrackingNumberEditableText$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "tracking_number"

    invoke-static {p3, v1, v3, v2}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    .line 110
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->getUnsubmittedCarrierName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$editOtherCarrierNameEditableText$1;

    invoke-direct {v3, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$editOtherCarrierNameEditableText$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const-string v4, "carrier_name"

    invoke-static {p3, v2, v4, v3}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p3

    .line 116
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v2

    const-string v3, ""

    if-eqz v2, :cond_0

    .line 117
    sget-object v4, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 118
    new-instance v2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    .line 119
    invoke-static {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingStateKt;->getTrackingScreenData(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object v5

    .line 118
    invoke-direct {v2, v5, v1, p3, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 147
    new-instance v5, Lcom/squareup/workflow/legacy/Screen;

    .line 148
    const-class p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {p1, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 149
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 147
    invoke-direct {v5, p1, v2, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 124
    new-instance p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    .line 125
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object p2

    .line 124
    invoke-direct {p1, p2, v0}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 152
    new-instance v9, Lcom/squareup/workflow/legacy/Screen;

    .line 153
    const-class p2, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 154
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 152
    invoke-direct {v9, p2, p1, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/16 v10, 0xe

    const/4 v11, 0x0

    .line 117
    invoke-static/range {v4 .. v11}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 130
    :cond_0
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 131
    new-instance v2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    .line 132
    invoke-static {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingStateKt;->getTrackingScreenData(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object p2

    .line 131
    invoke-direct {v2, p2, v1, p3, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 157
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 158
    const-class p2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 159
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 157
    invoke-direct {p1, p2, v2, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 130
    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    .line 144
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->snapshotState(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
