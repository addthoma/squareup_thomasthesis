.class final Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;
.super Ljava/lang/Object;
.source "OrderHubItemSelectionCoordinator.kt"

# interfaces
.implements Lcom/squareup/cycler/ItemComparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/ItemComparator<",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u00c2\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016J\u0018\u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;",
        "Lcom/squareup/cycler/ItemComparator;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
        "()V",
        "areSameContent",
        "",
        "a",
        "b",
        "areSameIdentity",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 230
    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;->INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public areSameContent(Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;)Z
    .locals 4

    const-string v0, "a"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "b"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    instance-of v3, p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    if-eqz v3, :cond_1

    .line 256
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;->getAction()Lcom/squareup/protos/client/orders/Action$Type;

    move-result-object p1

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;->getAction()Lcom/squareup/protos/client/orders/Action$Type;

    move-result-object p2

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 257
    :cond_1
    instance-of v3, p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    if-eq v0, v3, :cond_2

    return v2

    .line 261
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    if-eqz v0, :cond_4

    instance-of v3, p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    if-eqz v3, :cond_4

    .line 262
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;->getSelected()Z

    move-result p1

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;->getSelected()Z

    move-result p2

    if-ne p1, p2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 263
    :cond_4
    instance-of v3, p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    if-eq v0, v3, :cond_5

    return v2

    .line 267
    :cond_5
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;

    .line 268
    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;

    .line 270
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getIdentifier()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_6

    return v2

    .line 274
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getSelected()Z

    move-result v0

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getSelected()Z

    move-result v1

    if-eq v0, v1, :cond_7

    return v2

    .line 278
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic areSameContent(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 230
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;->areSameContent(Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;)Z

    move-result p1

    return p1
.end method

.method public areSameIdentity(Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;)Z
    .locals 4

    const-string v0, "a"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "b"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    instance-of v3, p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    if-eqz v3, :cond_1

    .line 237
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;->getAction()Lcom/squareup/protos/client/orders/Action$Type;

    move-result-object p1

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;->getAction()Lcom/squareup/protos/client/orders/Action$Type;

    move-result-object p2

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 238
    :cond_1
    instance-of v3, p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    if-eq v0, v3, :cond_2

    return v2

    .line 242
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    if-eqz v0, :cond_3

    instance-of v3, p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    if-eqz v3, :cond_3

    return v1

    .line 244
    :cond_3
    instance-of v1, p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    if-eq v0, v1, :cond_4

    return v2

    .line 248
    :cond_4
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getIdentifier()Ljava/lang/String;

    move-result-object p1

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getIdentifier()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic areSameIdentity(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 230
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;->areSameIdentity(Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;)Z

    move-result p1

    return p1
.end method

.method public getChangePayload(Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;)Ljava/lang/Object;
    .locals 1

    const-string v0, "oldItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    invoke-static {p0, p1, p2}, Lcom/squareup/cycler/ItemComparator$DefaultImpls;->getChangePayload(Lcom/squareup/cycler/ItemComparator;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getChangePayload(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 230
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;->getChangePayload(Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
