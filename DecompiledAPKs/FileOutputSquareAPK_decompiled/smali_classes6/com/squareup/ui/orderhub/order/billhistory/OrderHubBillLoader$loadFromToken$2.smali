.class final Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$2;
.super Ljava/lang/Object;
.source "OrderHubBillLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->loadFromToken$orderhub_applet_release(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$2;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$2;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->access$getBillHistoryLoadedState$p(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$2;->accept(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;)V

    return-void
.end method
