.class final Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubItemSelectionCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubItemSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubItemSelectionCoordinator.kt\ncom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$1$3\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,281:1\n1103#2,7:282\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubItemSelectionCoordinator.kt\ncom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$1$3\n*L\n162#1,7:282\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "index",
        "",
        "item",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;",
        "row",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "invoke",
        "com/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$1$3"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;->this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;

    check-cast p3, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;->invoke(ILcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;Lcom/squareup/noho/NohoCheckableRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;Lcom/squareup/noho/NohoCheckableRow;)V
    .locals 3

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;->this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->access$getRes$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->displayName(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;->this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->access$getLineItemSubtitleFormatter$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    move-result-object p1

    .line 148
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v0

    const/4 v1, 0x1

    .line 147
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->createOrderLineItemSubtitle(Lcom/squareup/orders/model/Order$LineItem;Z)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->isUnitPriced(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 152
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;->this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->access$getMoneyFormatter$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object p1

    .line 154
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->getBasePricePerQuantity(Lcom/squareup/orders/model/Order$LineItem;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 155
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    const-string v2, "item.lineItem.quantity"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 153
    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->multiplyByItemQuantity(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 152
    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 159
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;->this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->access$getMoneyFormatter$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->getBasePricePerQuantity(Lcom/squareup/orders/model/Order$LineItem;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 151
    :goto_0
    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setValue(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->getSelected()Z

    move-result p1

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 162
    move-object p1, p3

    check-cast p1, Landroid/view/View;

    .line 282
    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3$1;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3$1;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;Lcom/squareup/noho/NohoCheckableRow;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
