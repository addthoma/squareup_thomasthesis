.class public abstract Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;
.super Ljava/lang/Object;
.source "OrderHubOrderDetailsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;,
        Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickAdjustPickupTime;,
        Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ViewTransactionDetail;,
        Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;,
        Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$GoBackFromOrderDetails;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u0003\u0004\u0005\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0005\u0008\t\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;",
        "",
        "()V",
        "ClickAdjustPickupTime",
        "ClickFulfillmentAction",
        "EditTracking",
        "GoBackFromOrderDetails",
        "ViewTransactionDetail",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickFulfillmentAction;",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ClickAdjustPickupTime;",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$ViewTransactionDetail;",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$GoBackFromOrderDetails;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;-><init>()V

    return-void
.end method
