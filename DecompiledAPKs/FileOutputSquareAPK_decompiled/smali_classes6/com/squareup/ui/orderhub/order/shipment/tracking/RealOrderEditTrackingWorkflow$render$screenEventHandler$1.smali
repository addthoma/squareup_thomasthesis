.class final Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$screenEventHandler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderEditTrackingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->render(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        "event",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$screenEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$UpdateCarrier;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$screenEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$UpdateCarrier;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$UpdateCarrier;->getCarrier()Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x7f

    const/4 v11, 0x0

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->copy$default(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-result-object p1

    const/4 v1, 0x2

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 85
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$SubmitTracking;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;

    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$SubmitTracking;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$SubmitTracking;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;-><init>(Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 86
    :cond_1
    instance-of p1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$GoBackFromEditingTracking;

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$GoBackFromEditTracking;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$GoBackFromEditTracking;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$screenEventHandler$1;->invoke(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
