.class final Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderItemSelectionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;->render(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderItemSelectionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderItemSelectionWorkflow.kt\ncom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1\n*L\n1#1,107:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
        "event",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$SubmitItemSelection;

    if-eqz v0, :cond_0

    .line 77
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;-><init>(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 78
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$GoBackFromItemSelection;

    if-eqz v0, :cond_1

    .line 79
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;->INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 80
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$UpdateItemSelection;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    .line 81
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$UpdateItemSelection;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$UpdateItemSelection;->getItemIdentifier()Ljava/lang/String;

    move-result-object v0

    .line 82
    sget-object v3, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 83
    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 84
    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v9

    invoke-static {v9}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    .line 85
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$UpdateItemSelection;->getShouldSelectItem()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getAvailableLineItems()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_3

    .line 87
    invoke-interface {v9, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 90
    :cond_2
    invoke-interface {v9, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_0
    const/16 v10, 0xf

    const/4 v11, 0x0

    .line 83
    invoke-static/range {v4 .. v11}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->copy$default(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-result-object p1

    .line 82
    invoke-static {v3, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 96
    :cond_4
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$SelectAllItems;

    if-eqz v0, :cond_6

    .line 97
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 98
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$SelectAllItems;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event$SelectAllItems;->getShouldSelectAll()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getAvailableLineItems()Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    :cond_5
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    :goto_1
    move-object v8, p1

    const/16 v9, 0xf

    const/4 v10, 0x0

    .line 97
    invoke-static/range {v3 .. v10}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->copy$default(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-result-object p1

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;->invoke(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
