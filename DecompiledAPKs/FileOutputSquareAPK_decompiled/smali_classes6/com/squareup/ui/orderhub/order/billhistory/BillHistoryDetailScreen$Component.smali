.class public interface abstract Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;
.super Ljava/lang/Object;
.source "BillHistoryDetailScreen.kt"

# interfaces
.implements Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/ErrorsBarPresenter$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/ui/activity/billhistory/BillHistoryView$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;",
        "Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;",
        "Lcom/squareup/ui/ErrorsBarView$Component;",
        "inject",
        "",
        "view",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;)V
.end method
