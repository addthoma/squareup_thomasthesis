.class public final Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;
.super Ljava/lang/Object;
.source "OrderLineItemSelections.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderLineItemSelections.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderLineItemSelections.kt\ncom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,139:1\n75#2,9:140\n151#2,2:149\n84#2:151\n75#2,9:159\n151#2,2:168\n84#2:170\n347#3,7:152\n1587#4,3:171\n*E\n*S KotlinDebug\n*F\n+ 1 OrderLineItemSelections.kt\ncom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt\n*L\n40#1,9:140\n40#1,2:149\n40#1:151\n97#1,9:159\n97#1,2:168\n97#1:170\n63#1,7:152\n120#1,3:171\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u001e\n\u0002\u0008\u0005\u001a\u001c\u0010\n\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u000c\u001a\u00020\rH\u0002\u001a@\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000f0\u0001j\u0002`\u0010*&\u0012\u0004\u0012\u00020\u0002\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0001j\u0002`\u00050\u0001j\u0002`\u0006H\u0000\u001a>\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012*\u00020\u00072*\u0010\u0014\u001a&\u0012\u0004\u0012\u00020\u0002\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0001j\u0002`\u00050\u0001j\u0002`\u0006H\u0000\u001a\u001a\u0010\u0015\u001a\u00020\u000f*\u0008\u0012\u0004\u0012\u00020\u00040\u00162\u0006\u0010\u0017\u001a\u00020\rH\u0002\"<\u0010\u0000\u001a&\u0012\u0004\u0012\u00020\u0002\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0001j\u0002`\u00050\u0001j\u0002`\u0006*\u00020\u00078@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t*\n\u0010\u0018\"\u00020\u00022\u00020\u0002*6\u0010\u0019\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002`\u00050\u00012\"\u0012\u0004\u0012\u00020\u0002\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0001j\u0002`\u00050\u0001*&\u0010\u001a\"\u000e\u0012\u0004\u0012\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u00012\u0012\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0001\u00a8\u0006\u001b"
    }
    d2 = {
        "availableLineItemsToRowIdentifiers",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "Lcom/squareup/orders/model/Order;",
        "getAvailableLineItemsToRowIdentifiers",
        "(Lcom/squareup/orders/model/Order;)Ljava/util/Map;",
        "lineItemRowIdentifier",
        "uid",
        "quantityIndex",
        "",
        "asLineItemQuantitiesByUid",
        "Ljava/math/BigDecimal;",
        "Lcom/squareup/ui/orderhub/inventory/LineItemQuantitiesByUid;",
        "selectedLineItemQuantities",
        "",
        "Lcom/squareup/protos/client/orders/LineItemQuantity;",
        "enabledLineItems",
        "totalLineItemSelectionQuantity",
        "",
        "precision",
        "LineItemRowIdentifier",
        "LineItemSelectionsByUid",
        "LineItemWithQuantityByIdentifier",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asLineItemQuantitiesByUid(Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$asLineItemQuantitiesByUid"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 149
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 41
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_2

    .line 44
    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 45
    move-object v3, v2

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$LineItem;

    iget-object v3, v3, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v3, :cond_2

    iget-object v3, v3, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    .line 46
    :goto_1
    invoke-static {v2, v3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->totalLineItemSelectionQuantity(Ljava/util/Collection;I)Ljava/math/BigDecimal;

    move-result-object v2

    .line 47
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_0

    .line 148
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 151
    :cond_4
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 50
    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMap(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final getAvailableLineItemsToRowIdentifiers(Lcom/squareup/orders/model/Order;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    const-string v0, "$this$availableLineItemsToRowIdentifiers"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 61
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getAvailableLineItemsByUid(Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 63
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 152
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    .line 63
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v3, Ljava/util/Map;

    .line 155
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_1
    check-cast v3, Ljava/util/Map;

    .line 64
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orders/model/Order$LineItem;

    .line 66
    invoke-static {v2}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->isUnitPriced(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result v4

    const/4 v5, 0x0

    if-nez v4, :cond_2

    .line 68
    iget-object v4, v2, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    const-string v6, "lineItem.quantity"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    :goto_1
    if-ge v5, v4, :cond_0

    .line 69
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->lineItemRowIdentifier(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 70
    invoke-interface {v3, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 75
    :cond_2
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->lineItemRowIdentifier(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 80
    :cond_3
    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private static final lineItemRowIdentifier(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x2d

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final selectedLineItemQuantities(Lcom/squareup/orders/model/Order;Ljava/util/Map;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$selectedLineItemQuantities"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enabledLineItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 92
    invoke-static {p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->getAvailableLineItemsToRowIdentifiers(Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object p0

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 94
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    return-object p0

    .line 159
    :cond_0
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    check-cast p0, Ljava/util/Collection;

    .line 168
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 98
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_2

    .line 101
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 102
    move-object v2, v1

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orders/model/Order$LineItem;

    iget-object v2, v2, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    .line 103
    :goto_1
    invoke-static {v1, v2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->totalLineItemSelectionQuantity(Ljava/util/Collection;I)Ljava/math/BigDecimal;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v1

    .line 106
    new-instance v2, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;-><init>()V

    .line 107
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;->line_item_uid(Ljava/lang/String;)Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/LineItemQuantity$Builder;->build()Lcom/squareup/protos/client/orders/LineItemQuantity;

    move-result-object v0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_1

    .line 167
    invoke-interface {p0, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 170
    :cond_5
    check-cast p0, Ljava/util/List;

    return-object p0
.end method

.method private static final totalLineItemSelectionQuantity(Ljava/util/Collection;I)Ljava/math/BigDecimal;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;I)",
            "Ljava/math/BigDecimal;"
        }
    .end annotation

    .line 120
    check-cast p0, Ljava/lang/Iterable;

    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 172
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orders/model/Order$LineItem;

    .line 121
    invoke-static {v1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->isUnitPriced(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result v2

    const-string v3, "this.add(other)"

    const-string v4, "initial"

    if-eqz v2, :cond_0

    .line 122
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v1, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    const-string v2, "lineItem.quantity"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_0
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    const-string v2, "BigDecimal.ONE"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string p0, "fold(BigDecimal.ZERO) { \u2026BigDecimal.ONE)\n    }\n  }"

    .line 173
    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-static {v0, p1}, Lcom/squareup/quantity/ItemQuantities;->applyQuantityPrecision(Ljava/math/BigDecimal;I)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method
