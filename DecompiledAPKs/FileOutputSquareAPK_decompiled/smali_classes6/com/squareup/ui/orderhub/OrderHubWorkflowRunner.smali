.class public final Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner;
.source "OrderHubWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner<",
        "Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;",
        "Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 #2\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001#B\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u000fH\u0014JS\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0017\u001a\u00020\u00182*\u0010\u0019\u001a&\u0012\u0004\u0012\u00020\u001b\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u001d0\u001aj\u0002`\u001e0\u001aj\u0002`\u001fH\u0000\u00a2\u0006\u0002\u0008 J\r\u0010!\u001a\u00020\rH\u0000\u00a2\u0006\u0002\u0008\"R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner;",
        "Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;",
        "Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "orderHubRefundFlowState",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
        "orderHubApplet",
        "Lcom/squareup/ui/orderhub/OrderHubApplet;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/OrderHubApplet;)V",
        "handleResults",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onEnterScope",
        "newScope",
        "restartWorkflowAfterCanceledOrder",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "bill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "cancelReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "restartWorkflowAfterCanceledOrder$orderhub_applet_release",
        "startWorkflow",
        "startWorkflow$orderhub_applet_release",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final orderHubApplet:Lcom/squareup/ui/orderhub/OrderHubApplet;

.field private final orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->Companion:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;

    .line 80
    const-class v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderHubWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/OrderHubApplet;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubRefundFlowState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubApplet"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object v2, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->NAME:Ljava/lang/String;

    .line 28
    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 29
    const-class v4, Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;

    .line 30
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 31
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$2;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$2;

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x60

    const/4 v10, 0x0

    move-object v1, p0

    .line 26
    invoke-direct/range {v1 .. v10}, Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->orderHubApplet:Lcom/squareup/ui/orderhub/OrderHubApplet;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getOrderHubRefundFlowState$p(Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    return-object p0
.end method

.method private final handleResults(Lmortar/MortarScope;)V
    .locals 2

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1;-><init>(Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method


# virtual methods
.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->handleResults(Lmortar/MortarScope;)V

    .line 40
    iget-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->orderHubApplet:Lcom/squareup/ui/orderhub/OrderHubApplet;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubApplet;->select()V

    .line 41
    iget-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->orderHubApplet:Lcom/squareup/ui/orderhub/OrderHubApplet;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubApplet;->resetBadgeCount()V

    return-void
.end method

.method public final restartWorkflowAfterCanceledOrder$orderhub_applet_release(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelReason"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->ensureWorkflow()V

    .line 70
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->setProps(Ljava/lang/Object;)V

    return-void
.end method

.method public final startWorkflow$orderhub_applet_release()V
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->ensureWorkflow()V

    .line 76
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$NoInput;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$NoInput;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->setProps(Ljava/lang/Object;)V

    return-void
.end method
