.class public final Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "OrderHubAppletScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/OrderHubAppletScope$ParentComponent;,
        Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubAppletScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubAppletScope.kt\ncom/squareup/ui/orderhub/OrderHubAppletScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,57:1\n35#2:58\n35#2:59\n24#3,4:60\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubAppletScope.kt\ncom/squareup/ui/orderhub/OrderHubAppletScope\n*L\n22#1:58\n31#1:59\n55#1,4:60\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\nH\u0016R\u001c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0006\u0010\u0003\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubAppletScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "CREATOR$annotations",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "register",
        "",
        "scope",
        "Component",
        "ParentComponent",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/orderhub/OrderHubAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/orderhub/OrderHubAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubAppletScope;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/OrderHubAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubAppletScope;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubAppletScope;

    .line 60
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubAppletScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/OrderHubAppletScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 63
    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method public static synthetic CREATOR$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 58
    const-class v1, Lcom/squareup/ui/orderhub/OrderHubAppletScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 22
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubAppletScope$ParentComponent;

    .line 24
    invoke-interface {p1}, Lcom/squareup/ui/orderhub/OrderHubAppletScope$ParentComponent;->orderHubWorkflowRunner()Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    move-result-object p1

    const-string v1, "scopeBuilder"

    .line 25
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    const-class v0, Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 31
    check-cast v0, Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;

    .line 32
    invoke-interface {v0}, Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;->salesHistoryRefundHelper()Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
