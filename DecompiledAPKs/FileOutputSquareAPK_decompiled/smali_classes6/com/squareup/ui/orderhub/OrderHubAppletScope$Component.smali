.class public interface abstract Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;
.super Ljava/lang/Object;
.source "OrderHubAppletScope.kt"

# interfaces
.implements Lcom/squareup/ui/activity/IssueRefundScope$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/applet/BadgePresenter$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/orderhub/OrderHubAppletScopeModule;,
        Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;",
        "Lcom/squareup/ui/activity/IssueRefundScope$ParentComponent;",
        "billHistoryScope",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$Component;",
        "salesHistoryRefundHelper",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;",
        "viewFactory",
        "Lcom/squareup/ui/orderhub/OrderHubViewFactory;",
        "workflow",
        "Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract billHistoryScope()Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$Component;
.end method

.method public abstract salesHistoryRefundHelper()Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;
.end method

.method public abstract viewFactory()Lcom/squareup/ui/orderhub/OrderHubViewFactory;
.end method

.method public abstract workflow()Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;
.end method
