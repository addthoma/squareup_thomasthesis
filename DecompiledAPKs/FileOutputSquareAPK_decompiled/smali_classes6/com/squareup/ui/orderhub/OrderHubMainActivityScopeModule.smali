.class public abstract Lcom/squareup/ui/orderhub/OrderHubMainActivityScopeModule;
.super Ljava/lang/Object;
.source "OrderHubMainActivityScopeModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ordermanagerdata/OrderManagerDataModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000cJ\u0015\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000fH!\u00a2\u0006\u0002\u0008\u0010J\u0015\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0013H!\u00a2\u0006\u0002\u0008\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H!\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubMainActivityScopeModule;",
        "",
        "()V",
        "bindOrderHubBadgingMonitor",
        "Lmortar/Scoped;",
        "orderHubBadgingMonitor",
        "Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;",
        "bindOrderHubBadgingMonitor$orderhub_applet_release",
        "bindOrderHubClientActionTranslator",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
        "orderHubClientActionTranslator",
        "Lcom/squareup/ui/orderhub/linking/OrderHubClientActionTranslator;",
        "bindOrderHubClientActionTranslator$orderhub_applet_release",
        "bindOrderHubNewOrdersDialogMonitor",
        "orderHubNewOrdersDialogMonitor",
        "Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;",
        "bindOrderHubNewOrdersDialogMonitor$orderhub_applet_release",
        "bindOrderHubPrintingMonitor",
        "orderHubPrintingMonitor",
        "Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;",
        "bindOrderHubPrintingMonitor$orderhub_applet_release",
        "bindUnknownOrdersNotificationsSource",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "unknownOrdersNotificationsSource",
        "Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindOrderHubBadgingMonitor$orderhub_applet_release(Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindOrderHubClientActionTranslator$orderhub_applet_release(Lcom/squareup/ui/orderhub/linking/OrderHubClientActionTranslator;)Lcom/squareup/clientactiontranslation/ClientActionTranslator;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindOrderHubNewOrdersDialogMonitor$orderhub_applet_release(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindOrderHubPrintingMonitor$orderhub_applet_release(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindUnknownOrdersNotificationsSource(Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;)Lcom/squareup/notificationcenterdata/NotificationsSource;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
