.class final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderHubWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderDetailChildRendering(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "+",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "it",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/OrderHubState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/OrderDetailsResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 529
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CloseOrder;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iget-object v4, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CloseOrder;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CloseOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    invoke-static {v3, v4, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getStateAfterOrderViewFinished(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 530
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$IssuedInventoryAdjustment;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 531
    iget-object v3, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 532
    new-instance v9, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    .line 533
    new-instance v10, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrder;

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$IssuedInventoryAdjustment;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$IssuedInventoryAdjustment;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    invoke-direct {v10, p1}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrder;-><init>(Lcom/squareup/orders/model/Order;)V

    check-cast v10, Lcom/squareup/ui/orderhub/ViewedOrderInput;

    .line 532
    invoke-direct {v9, v10}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;-><init>(Lcom/squareup/ui/orderhub/ViewedOrderInput;)V

    check-cast v9, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    const/4 v10, 0x0

    const/16 v11, 0x5f

    const/4 v12, 0x0

    .line 531
    invoke-static/range {v3 .. v12}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    .line 530
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 537
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CancelOrder;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 538
    new-instance v1, Lcom/squareup/ui/orderhub/OrderHubResult$CanceledOrder;

    .line 539
    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CancelOrder;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CancelOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    .line 540
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CancelOrder;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v3

    .line 541
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CancelOrder;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v4

    .line 542
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CancelOrder;->getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object p1

    .line 538
    invoke-direct {v1, v2, v3, v4, p1}, Lcom/squareup/ui/orderhub/OrderHubResult$CanceledOrder;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;)V

    .line 537
    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;->invoke(Lcom/squareup/ui/orderhub/order/OrderDetailsResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
