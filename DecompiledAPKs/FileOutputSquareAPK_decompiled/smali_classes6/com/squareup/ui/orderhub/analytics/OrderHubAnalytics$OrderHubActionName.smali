.class public final enum Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;
.super Ljava/lang/Enum;
.source "OrderHubAnalytics.kt"

# interfaces
.implements Lcom/squareup/analytics/EventNamedAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OrderHubActionName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;",
        ">;",
        "Lcom/squareup/analytics/EventNamedAction;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u001c\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001ej\u0002\u0008\u001f\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;",
        "",
        "Lcom/squareup/analytics/EventNamedAction;",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getName",
        "ORDER_HUB_ACCEPT",
        "ORDER_HUB_CANCEL",
        "ORDER_HUB_COMPLETE",
        "ORDER_HUB_MARK_IN_PROGRESS",
        "ORDER_HUB_MARK_PICKED_UP",
        "ORDER_HUB_MARK_READY",
        "ORDER_HUB_MARK_SHIPPED",
        "ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_LATER",
        "ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_NOW",
        "ORDER_HUB_TAP_ORDER_DETAIL",
        "ORDER_HUB_ORDER_DEEP_LINK",
        "ORDER_HUB_VIEW_TRANSACTION_DETAIL",
        "ORDER_HUB_ADJUST_PICKUP_TIME",
        "ORDER_HUB_QUICK_ACTIONS",
        "ORDER_HUB_ADD_TRACKING_INFORMATION",
        "ORDER_HUB_EDIT_TRACKING_INFORMATION",
        "ORDER_HUB_DELETE_TRACKING_INFORMATION",
        "ORDER_HUB_SKIP_TRACKING_INFORMATION",
        "ORDER_HUB_CANCEL_CONTINUE_TO_REFUND",
        "ORDER_HUB_CANCEL_SKIP_REFUND",
        "ORDER_HUB_AUTOMATIC_PRINT",
        "ORDER_HUB_MARKED_ORDER_AS_PRINTED",
        "ORDER_HUB_SKIPPED_AUTOMATIC_PRINT",
        "ORDER_HUB_SKIPPED_AUTOMATIC_PRINT_INITIAL_ATTEMPT",
        "ORDER_HUB_MANUAL_PRINT",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_ACCEPT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_ADD_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_ADJUST_PICKUP_TIME:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_AUTOMATIC_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_CANCEL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_CANCEL_CONTINUE_TO_REFUND:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_CANCEL_SKIP_REFUND:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_COMPLETE:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_DELETE_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_EDIT_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_MANUAL_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_MARKED_ORDER_AS_PRINTED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_MARK_IN_PROGRESS:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_MARK_PICKED_UP:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_MARK_READY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_MARK_SHIPPED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_LATER:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_NOW:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_ORDER_DEEP_LINK:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_QUICK_ACTIONS:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_SKIPPED_AUTOMATIC_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_SKIPPED_AUTOMATIC_PRINT_INITIAL_ATTEMPT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_SKIP_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_TAP_ORDER_DETAIL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

.field public static final enum ORDER_HUB_VIEW_TRANSACTION_DETAIL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x19

    new-array v0, v0, [Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/4 v2, 0x0

    const-string v3, "ORDER_HUB_ACCEPT"

    const-string v4, "Order Hub: Accepted Order"

    .line 87
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_ACCEPT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/4 v2, 0x1

    const-string v3, "ORDER_HUB_CANCEL"

    const-string v4, "Order Hub: Canceled Order"

    .line 88
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_CANCEL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/4 v2, 0x2

    const-string v3, "ORDER_HUB_COMPLETE"

    const-string v4, "Order Hub: Completed Order"

    .line 89
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_COMPLETE:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/4 v2, 0x3

    const-string v3, "ORDER_HUB_MARK_IN_PROGRESS"

    const-string v4, "Order Hub: Marked Order In Progress"

    .line 90
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARK_IN_PROGRESS:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/4 v2, 0x4

    const-string v3, "ORDER_HUB_MARK_PICKED_UP"

    const-string v4, "Order Hub: Marked Order As Picked Up"

    .line 91
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARK_PICKED_UP:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/4 v2, 0x5

    const-string v3, "ORDER_HUB_MARK_READY"

    const-string v4, "Order Hub: Marked Order As Ready"

    .line 92
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARK_READY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/4 v2, 0x6

    const-string v3, "ORDER_HUB_MARK_SHIPPED"

    const-string v4, "Order Hub: Marked Order As Shipped"

    .line 93
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARK_SHIPPED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/4 v2, 0x7

    const-string v3, "ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_LATER"

    const-string v4, "Order Hub: New Order Notification - View Later"

    .line 96
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_LATER:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x8

    const-string v3, "ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_NOW"

    const-string v4, "Order Hub: New Order Notification - View Now"

    .line 97
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_NOW:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x9

    const-string v3, "ORDER_HUB_TAP_ORDER_DETAIL"

    const-string v4, "Order Hub: Selected Order Detail"

    .line 100
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_TAP_ORDER_DETAIL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0xa

    const-string v3, "ORDER_HUB_ORDER_DEEP_LINK"

    const-string v4, "Order Hub: Selected Deep Link"

    .line 101
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_ORDER_DEEP_LINK:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0xb

    const-string v3, "ORDER_HUB_VIEW_TRANSACTION_DETAIL"

    const-string v4, "Order Hub: Viewed Transaction Detail"

    .line 102
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_VIEW_TRANSACTION_DETAIL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0xc

    const-string v3, "ORDER_HUB_ADJUST_PICKUP_TIME"

    const-string v4, "Order Hub: Adjusted Order Pickup Time"

    .line 103
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_ADJUST_PICKUP_TIME:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0xd

    const-string v3, "ORDER_HUB_QUICK_ACTIONS"

    const-string v4, "Order Hub: Selected Quick Action"

    .line 104
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_QUICK_ACTIONS:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0xe

    const-string v3, "ORDER_HUB_ADD_TRACKING_INFORMATION"

    const-string v4, "Order Hub: Added Tracking Information"

    .line 106
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_ADD_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_EDIT_TRACKING_INFORMATION"

    const/16 v3, 0xf

    const-string v4, "Order Hub: Edited Tracking Information"

    .line 107
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_EDIT_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_DELETE_TRACKING_INFORMATION"

    const/16 v3, 0x10

    const-string v4, "Order Hub: Deleted Tracking Information"

    .line 108
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_DELETE_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_SKIP_TRACKING_INFORMATION"

    const/16 v3, 0x11

    const-string v4, "Order Hub: Skipped Tracking Information"

    .line 109
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_SKIP_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_CANCEL_CONTINUE_TO_REFUND"

    const/16 v3, 0x12

    const-string v4, "Order Hub: Continued To Refund"

    .line 111
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_CANCEL_CONTINUE_TO_REFUND:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_CANCEL_SKIP_REFUND"

    const/16 v3, 0x13

    const-string v4, "Order Hub: Skipped Refund"

    .line 112
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_CANCEL_SKIP_REFUND:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_AUTOMATIC_PRINT"

    const/16 v3, 0x14

    const-string v4, "Order Hub: Automatically Printed Ticket"

    .line 115
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_AUTOMATIC_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_MARKED_ORDER_AS_PRINTED"

    const/16 v3, 0x15

    const-string v4, "Order Hub: Marked Order as Printed"

    .line 116
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARKED_ORDER_AS_PRINTED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_SKIPPED_AUTOMATIC_PRINT"

    const/16 v3, 0x16

    const-string v4, "Order Hub: Skipped Auto Print Ticket"

    .line 117
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_SKIPPED_AUTOMATIC_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_SKIPPED_AUTOMATIC_PRINT_INITIAL_ATTEMPT"

    const/16 v3, 0x17

    const-string v4, "Order Hub: Skipped Auto Print Ticket Due to Initial Attempt"

    .line 118
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_SKIPPED_AUTOMATIC_PRINT_INITIAL_ATTEMPT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const-string v2, "ORDER_HUB_MANUAL_PRINT"

    const/16 v3, 0x18

    const-string v4, "Order Hub: Manually Printed Ticket"

    .line 121
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MANUAL_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->$VALUES:[Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;
    .locals 1

    const-class v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;
    .locals 1

    sget-object v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->$VALUES:[Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v0}, [Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->value:Ljava/lang/String;

    return-object v0
.end method
