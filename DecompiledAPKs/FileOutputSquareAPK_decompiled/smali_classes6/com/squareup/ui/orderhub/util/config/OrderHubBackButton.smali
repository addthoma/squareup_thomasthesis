.class public final Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;
.super Ljava/lang/Object;
.source "OrderHubBackButton.kt"

# interfaces
.implements Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;",
        "Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;",
        "()V",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "showBackButton",
        "",
        "getShowBackButton",
        "()Z",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;

.field private static final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private static final showBackButton:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;->INSTANCE:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;

    .line 10
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sput-object v0, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public getShowBackButton()Z
    .locals 1

    .line 9
    sget-boolean v0, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButton;->showBackButton:Z

    return v0
.end method
