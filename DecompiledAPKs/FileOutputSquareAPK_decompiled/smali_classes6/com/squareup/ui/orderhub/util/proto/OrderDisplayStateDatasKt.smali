.class public final Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt;
.super Ljava/lang/Object;
.source "OrderDisplayStateDatas.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0005\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\u0007\u001a\u0012\u0010\u0008\u001a\u00020\t*\u00020\u00022\u0006\u0010\n\u001a\u00020\u000b\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028G\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u000c"
    }
    d2 = {
        "updatedAtPatternRes",
        "",
        "Lcom/squareup/protos/client/orders/OrderDisplayStateData;",
        "getUpdatedAtPatternRes",
        "(Lcom/squareup/protos/client/orders/OrderDisplayStateData;)I",
        "getColor",
        "currentDate",
        "Lorg/threeten/bp/ZonedDateTime;",
        "getDisplayName",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getColor(Lcom/squareup/protos/client/orders/OrderDisplayStateData;Lorg/threeten/bp/ZonedDateTime;)I
    .locals 1

    const-string v0, "$this$getColor"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->overdue_at:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/threeten/bp/ZonedDateTime;->parse(Ljava/lang/CharSequence;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    .line 53
    check-cast v0, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/ZonedDateTime;->isAfter(Lorg/threeten/bp/chrono/ChronoZonedDateTime;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 54
    sget p0, Lcom/squareup/orderhub/applet/R$color;->orderhub_text_color_overdue:I

    return p0

    .line 57
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-nez p0, :cond_1

    goto :goto_1

    :cond_1
    sget-object p1, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result p0

    aget p0, p1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_1

    .line 59
    :pswitch_0
    sget p0, Lcom/squareup/orderhub/applet/R$color;->orderhub_text_color_overdue:I

    goto :goto_2

    .line 58
    :pswitch_1
    sget p0, Lcom/squareup/orderhub/applet/R$color;->orderhub_text_color_progress:I

    goto :goto_2

    .line 60
    :goto_1
    sget p0, Lcom/squareup/orderhub/applet/R$color;->orderhub_text_color_completed:I

    :goto_2
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static final getDisplayName(Lcom/squareup/protos/client/orders/OrderDisplayStateData;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getDisplayName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 26
    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    const-string p1, "display_name"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 29
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-nez p0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 37
    :pswitch_0
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_failed:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 36
    :pswitch_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_rejected:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 35
    :pswitch_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_canceled:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 34
    :pswitch_3
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_completed:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 33
    :pswitch_4
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_ready:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 32
    :pswitch_5
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_in_progress:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 31
    :pswitch_6
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_new:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 30
    :pswitch_7
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_upcoming:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 38
    :goto_0
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_unknown:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_1
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final getUpdatedAtPatternRes(Lcom/squareup/protos/client/orders/OrderDisplayStateData;)I
    .locals 1

    const-string v0, "$this$updatedAtPatternRes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 75
    :pswitch_0
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_placed_at:I

    goto :goto_1

    .line 74
    :pswitch_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_rejected_at:I

    goto :goto_1

    .line 73
    :pswitch_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_canceled_at:I

    goto :goto_1

    .line 72
    :pswitch_3
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_completed_at:I

    goto :goto_1

    .line 71
    :pswitch_4
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_placed_at:I

    goto :goto_1

    .line 70
    :pswitch_5
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_placed_at:I

    goto :goto_1

    .line 69
    :pswitch_6
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_placed_at:I

    goto :goto_1

    .line 68
    :pswitch_7
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_placed_at:I

    goto :goto_1

    .line 76
    :goto_0
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_placed_at:I

    :goto_1
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
