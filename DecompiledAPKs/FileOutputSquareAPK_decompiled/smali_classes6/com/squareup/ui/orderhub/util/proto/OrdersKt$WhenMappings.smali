.class public final synthetic Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->values()[Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->READY:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->COMPLETED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/4 v5, 0x5

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->CANCELED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/4 v5, 0x6

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->REJECTED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/4 v5, 0x7

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->FAILED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/16 v5, 0x8

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->DO_NOT_USE:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v1

    const/16 v5, 0x9

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->values()[Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ALL:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ENTRY_LIST:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->UNKNOWN_APPLICATION:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$FulfillmentType;->values()[Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
