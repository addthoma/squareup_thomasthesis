.class public abstract Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;
.super Ljava/lang/Object;
.source "SearchWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;,
        Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$DispatchSearch;,
        Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$ForceSearch;,
        Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;,
        Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;,
        Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$ChangeSearchFocus;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0006\u0005\u0006\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004\u0082\u0001\u0006\u000b\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "",
        "()V",
        "ChangeSearchFocus",
        "DispatchSearch",
        "EnterSearchText",
        "ForceSearch",
        "SearchError",
        "UpdateSearchResults",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$DispatchSearch;",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$ForceSearch;",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$ChangeSearchFocus;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 191
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 191
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Void;

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            "*>;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
