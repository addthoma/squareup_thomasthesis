.class public final Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderKt;
.super Ljava/lang/Object;
.source "PrintableOrderHubOrder.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintableOrderHubOrder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintableOrderHubOrder.kt\ncom/squareup/ui/orderhub/print/PrintableOrderHubOrderKt\n*L\n1#1,168:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001e\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0001\u001a\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0008*\u00020\t2\u0006\u0010\u0003\u001a\u00020\u0004H\u0002\u001a,\u0010\n\u001a\u00020\u000b*\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000fH\u0000\u001a\u0014\u0010\u0010\u001a\u00020\u0008*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0001\u00a8\u0006\u0011"
    }
    d2 = {
        "diningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "Lcom/squareup/orders/model/Order;",
        "res",
        "Lcom/squareup/util/Res;",
        "dateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "name",
        "",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "printOrder",
        "",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        "order",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "ticketName",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final diningOption(Lcom/squareup/orders/model/Order;Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;)Lcom/squareup/checkout/DiningOption;
    .locals 4

    const-string v0, "$this$diningOption"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateAndTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderKt;->name(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 136
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getPickupAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 138
    invoke-virtual {p2, v1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    .line 139
    invoke-virtual {v1}, Lorg/threeten/bp/ZonedDateTime;->toInstant()Lorg/threeten/bp/Instant;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 143
    invoke-virtual {p2, v1, v2}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->formatDateAndTime(Lorg/threeten/bp/ZonedDateTime;Z)Ljava/lang/String;

    move-result-object p2

    .line 148
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_print_fulfillment_name_with_pickup_at:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 149
    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "fulfillment_name"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 150
    check-cast p2, Ljava/lang/CharSequence;

    const-string v0, "pickup_at"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 151
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 152
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 155
    :cond_0
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p0

    invoke-static {p0, v0}, Lcom/squareup/checkout/DiningOption;->of(Lcom/squareup/orders/model/Order$Fulfillment;Ljava/lang/String;)Lcom/squareup/checkout/DiningOption;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private static final name(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 159
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz p0, :cond_0

    sget-object v0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    const/4 p0, 0x0

    goto :goto_0

    .line 164
    :pswitch_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_print_digital:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 163
    :pswitch_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_print_shipment:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 162
    :pswitch_3
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_print_delivery:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 161
    :pswitch_4
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_print_delivery:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 160
    :pswitch_5
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_print_pickup:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    .line 165
    :cond_0
    :goto_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static final printOrder(Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/orders/model/Order;Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)V
    .locals 1

    const-string v0, "$this$printOrder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateAndTimeFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;

    invoke-direct {v0, p1, p3, p4}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)V

    check-cast v0, Lcom/squareup/print/PrintableOrder;

    .line 107
    invoke-static {p1, p2}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderKt;->ticketName(Lcom/squareup/orders/model/Order;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    .line 105
    invoke-virtual {p0, v0, p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Z)V

    return-void
.end method

.method public static final ticketName(Lcom/squareup/orders/model/Order;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 10

    const-string v0, "$this$ticketName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 119
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_0
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getDisplayId(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_1
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->recipientNameOrDefault(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    const-string p0, "\n"

    move-object v2, p0

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
