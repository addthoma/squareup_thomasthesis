.class public final Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;
.super Ljava/lang/Object;
.source "PrintableOrderHubOrderItem.kt"

# interfaces
.implements Lcom/squareup/print/PrintableOrderItem;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintableOrderHubOrderItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintableOrderHubOrderItem.kt\ncom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,79:1\n704#2:80\n777#2,2:81\n1360#2:83\n1429#2,3:84\n1587#2,3:87\n1360#2:90\n1429#2,3:91\n*E\n*S KotlinDebug\n*F\n+ 1 PrintableOrderHubOrderItem.kt\ncom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem\n*L\n70#1:80\n70#1,2:81\n71#1:83\n71#1,3:84\n72#1,3:87\n27#1:90\n27#1,3:91\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u00108\u001a\u00020\u00062\u0006\u00109\u001a\u00020:H\u0016J\u0010\u0010;\u001a\u00020\u001e2\u0006\u00109\u001a\u00020:H\u0016J\u0010\u0010<\u001a\u00020\u00062\u0006\u00109\u001a\u00020:H\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0016\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u0012X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013R\u0014\u0010\u0015\u001a\u00020\u0012X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0013R\u0014\u0010\u0016\u001a\u00020\u0012X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0013R\u0014\u0010\u0017\u001a\u00020\u0012X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0013R\u0014\u0010\u0018\u001a\u00020\u0012X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0013R\u0016\u0010\u0019\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001b\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0008R\u0014\u0010\u001d\u001a\u00020\u001eX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0014\u0010!\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u0008R\u0016\u0010#\u001a\u0004\u0018\u00010$X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&R\u001a\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020)0(X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010+R\u0016\u0010,\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010\u0008R\u0014\u0010.\u001a\u00020\u0012X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u0010\u0013R\u0014\u00100\u001a\u000201X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u00103R\u0014\u00104\u001a\u000201X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u00103R\u0014\u00106\u001a\u0002018VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u00087\u00103\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;",
        "Lcom/squareup/print/PrintableOrderItem;",
        "lineItem",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "(Lcom/squareup/orders/model/Order$LineItem;)V",
        "categoryId",
        "",
        "getCategoryId",
        "()Ljava/lang/String;",
        "destination",
        "Lcom/squareup/checkout/OrderDestination;",
        "getDestination",
        "()Lcom/squareup/checkout/OrderDestination;",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "getIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "isComped",
        "",
        "()Z",
        "isDownConvertedCustomAmount",
        "isTaxed",
        "isUncategorized",
        "isUnitPriced",
        "isVoided",
        "itemName",
        "getItemName",
        "notes",
        "getNotes",
        "quantityAsInt",
        "",
        "getQuantityAsInt",
        "()I",
        "quantityAsString",
        "getQuantityAsString",
        "selectedDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "getSelectedDiningOption",
        "()Lcom/squareup/checkout/DiningOption;",
        "selectedModifiers",
        "",
        "Lcom/squareup/print/PrintableOrderItemModifier;",
        "getSelectedModifiers",
        "()Ljava/util/List;",
        "selectedVariationDisplayName",
        "getSelectedVariationDisplayName",
        "shouldShowVariationName",
        "getShouldShowVariationName",
        "total",
        "Lcom/squareup/protos/common/Money;",
        "getTotal",
        "()Lcom/squareup/protos/common/Money;",
        "unitPrice",
        "getUnitPrice",
        "unitPriceWithModifiers",
        "getUnitPriceWithModifiers",
        "quantityEntrySuffix",
        "res",
        "Lcom/squareup/util/Res;",
        "quantityPrecision",
        "unitAbbreviation",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final categoryId:Ljava/lang/String;

.field private final destination:Lcom/squareup/checkout/OrderDestination;

.field private final idPair:Lcom/squareup/protos/client/IdPair;

.field private final isComped:Z

.field private final isDownConvertedCustomAmount:Z

.field private final isTaxed:Z

.field private final isUncategorized:Z

.field private final isUnitPriced:Z

.field private final isVoided:Z

.field private final itemName:Ljava/lang/String;

.field private final lineItem:Lcom/squareup/orders/model/Order$LineItem;

.field private final notes:Ljava/lang/String;

.field private final quantityAsInt:I

.field private final quantityAsString:Ljava/lang/String;

.field private final selectedDiningOption:Lcom/squareup/checkout/DiningOption;

.field private final selectedModifiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItemModifier;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedVariationDisplayName:Ljava/lang/String;

.field private final shouldShowVariationName:Z

.field private final total:Lcom/squareup/protos/common/Money;

.field private final unitPrice:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order$LineItem;)V
    .locals 5

    const-string v0, "lineItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    .line 20
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isUncategorized:Z

    .line 24
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->selectedVariationDisplayName:Ljava/lang/String;

    .line 27
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    const-string v1, "lineItem.modifiers"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 90
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 91
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 92
    check-cast v2, Lcom/squareup/orders/model/Order$LineItem$Modifier;

    .line 28
    new-instance v3, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;

    const-string v4, "it"

    .line 29
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {v3, v2}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;-><init>(Lcom/squareup/orders/model/Order$LineItem$Modifier;)V

    .line 30
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 93
    :cond_2
    check-cast v1, Ljava/util/List;

    iput-object v1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->selectedModifiers:Ljava/util/List;

    .line 33
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->itemName:Ljava/lang/String;

    .line 35
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    const-string v1, "lineItem.quantity"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->quantityAsString:Ljava/lang/String;

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->isUnitPriced(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    :goto_3
    iput p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->quantityAsInt:I

    .line 39
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->notes:Ljava/lang/String;

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->applied_taxes:Ljava/util/List;

    const-string v1, "lineItem.applied_taxes"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v0

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isTaxed:Z

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->categoryId:Ljava/lang/String;

    .line 53
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->getShouldShowVariationName(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->shouldShowVariationName:Z

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->isUnitPriced(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isUnitPriced:Z

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v0, "lineItem.base_price_money"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->unitPrice:Lcom/squareup/protos/common/Money;

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v0, "lineItem.total_money"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->total:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public getCategoryId()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->categoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getDestination()Lcom/squareup/checkout/OrderDestination;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->destination:Lcom/squareup/checkout/OrderDestination;

    return-object v0
.end method

.method public getIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getItemName()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public getNotes()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->notes:Ljava/lang/String;

    return-object v0
.end method

.method public getQuantityAsInt()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->quantityAsInt:I

    return v0
.end method

.method public getQuantityAsString()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->quantityAsString:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    return-object v0
.end method

.method public bridge synthetic getSelectedDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    check-cast v0, Lcom/squareup/itemsorter/SortableDiningOption;

    return-object v0
.end method

.method public getSelectedModifiers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItemModifier;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->selectedModifiers:Ljava/util/List;

    return-object v0
.end method

.method public getSelectedVariationDisplayName()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->selectedVariationDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getShouldShowVariationName()Z
    .locals 1

    .line 53
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->shouldShowVariationName:Z

    return v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->total:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getUnitPrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->unitPrice:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getUnitPriceWithModifiers()Lcom/squareup/protos/common/Money;
    .locals 5

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->getUnitPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->getSelectedModifiers()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 80
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 81
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/print/PrintableOrderItemModifier;

    .line 70
    invoke-interface {v4}, Lcom/squareup/print/PrintableOrderItemModifier;->getBasePriceTimesModifierQuantity()Lcom/squareup/protos/common/Money;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    :cond_2
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 83
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 84
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 85
    check-cast v3, Lcom/squareup/print/PrintableOrderItemModifier;

    .line 71
    invoke-interface {v3}, Lcom/squareup/print/PrintableOrderItemModifier;->getBasePriceTimesModifierQuantity()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 86
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 88
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 72
    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_3

    :cond_4
    const-string v1, "selectedModifiers\n      \u2026talPrice, MoneyMath::sum)"

    .line 89
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public isComped()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isComped:Z

    return v0
.end method

.method public isDownConvertedCustomAmount()Z
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isDownConvertedCustomAmount:Z

    return v0
.end method

.method public isTaxed()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isTaxed:Z

    return v0
.end method

.method public isUncategorized()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isUncategorized:Z

    return v0
.end method

.method public isUnitPriced()Z
    .locals 1

    .line 55
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isUnitPriced:Z

    return v0
.end method

.method public isVoided()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->isVoided:Z

    return v0
.end method

.method public quantityEntrySuffix(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, ""

    return-object p1
.end method

.method public quantityPrecision(Lcom/squareup/util/Res;)I
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    invoke-static {v0, p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->unitDisplayData(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecision()I

    move-result p1

    return p1
.end method

.method public bridge synthetic setSelectedDiningOption(Lcom/squareup/itemsorter/SortableDiningOption;)Lcom/squareup/itemsorter/SortableItem;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/checkout/DiningOption;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;

    move-result-object p1

    check-cast p1, Lcom/squareup/itemsorter/SortableItem;

    return-object p1
.end method

.method public setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;
    .locals 1

    const-string v0, "selectedDiningOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {p0, p1}, Lcom/squareup/print/PrintableOrderItem$DefaultImpls;->setSelectedDiningOption(Lcom/squareup/print/PrintableOrderItem;Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;

    move-result-object p1

    return-object p1
.end method

.method public unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    invoke-static {v0, p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->unitDisplayData(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
