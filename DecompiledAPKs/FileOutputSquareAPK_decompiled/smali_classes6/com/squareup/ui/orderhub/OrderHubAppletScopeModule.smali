.class public abstract Lcom/squareup/ui/orderhub/OrderHubAppletScopeModule;
.super Ljava/lang/Object;
.source "OrderHubAppletScopeModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008!\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000cJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H!\u00a2\u0006\u0002\u0008\u0011J\u0015\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H!\u00a2\u0006\u0002\u0008\u0016J\u0015\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH!\u00a2\u0006\u0002\u0008\u001bJ\u0015\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u000f\u001a\u00020\u001eH!\u00a2\u0006\u0002\u0008\u001fJ\u0015\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H!\u00a2\u0006\u0002\u0008$J\u0015\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H!\u00a2\u0006\u0002\u0008)\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubAppletScopeModule;",
        "",
        "()V",
        "bindOrderCancellationReasonWorkflow",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;",
        "realOrderCancellationReasonWorkflow",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;",
        "bindOrderCancellationReasonWorkflow$orderhub_applet_release",
        "bindOrderDetailsWorkflow",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;",
        "realOrderDetailsWorkflow",
        "Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;",
        "bindOrderDetailsWorkflow$orderhub_applet_release",
        "bindOrderEditTrackingWorkflow",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
        "realOrderMarkShippedWorkflow",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;",
        "bindOrderEditTrackingWorkflow$orderhub_applet_release",
        "bindOrderItemSelectionWorkflow",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
        "realOrderItemSelectionWorkflow",
        "Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;",
        "bindOrderItemSelectionWorkflow$orderhub_applet_release",
        "bindOrderMarkCanceledWorkflow",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;",
        "realOrderMarkCanceledWorkflow",
        "Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;",
        "bindOrderMarkCanceledWorkflow$orderhub_applet_release",
        "bindOrderMarkShippedWorkflow",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
        "Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;",
        "bindOrderMarkShippedWorkflow$orderhub_applet_release",
        "bindOrderSearchWorkflow",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
        "realOrderSearchWorkflow",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;",
        "bindOrderSearchWorkflow$orderhub_applet_release",
        "bindTransactionsHistoryRefundHelper",
        "Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;",
        "orderHubTransactionsHistoryRefundHelper",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;",
        "bindTransactionsHistoryRefundHelper$orderhub_applet_release",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindOrderCancellationReasonWorkflow$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderDetailsWorkflow$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;)Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderEditTrackingWorkflow$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderItemSelectionWorkflow$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderMarkCanceledWorkflow$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderMarkShippedWorkflow$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderSearchWorkflow$orderhub_applet_release(Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindTransactionsHistoryRefundHelper$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;)Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
