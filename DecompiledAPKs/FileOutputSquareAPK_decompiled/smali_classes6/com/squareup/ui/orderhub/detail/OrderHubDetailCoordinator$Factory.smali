.class public final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;
.super Ljava/lang/Object;
.source "OrderHubDetailCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001BY\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J*\u0010\u0017\u001a\u00020\u00182\"\u0010\u0019\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001bj\u0008\u0012\u0004\u0012\u00020\u001c`\u001e0\u001aR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "relativeDateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "locale",
        "Ljava/util/Locale;",
        "dateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "badgePresenter",
        "Lcom/squareup/applet/BadgePresenter;",
        "actionBarNavigationHelper",
        "Lcom/squareup/applet/ActionBarNavigationHelper;",
        "appletSelection",
        "Lcom/squareup/applet/AppletSelection;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/time/CurrentTime;Ljava/util/Locale;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;)V",
        "create",
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final badgePresenter:Lcom/squareup/applet/BadgePresenter;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private final locale:Ljava/util/Locale;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/time/CurrentTime;Ljava/util/Locale;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .param p9    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "relativeDateAndTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateAndTimeFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badgePresenter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBarNavigationHelper"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletSelection"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->locale:Ljava/util/Locale;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->appletSelection:Lcom/squareup/applet/AppletSelection;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p10, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    new-instance v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    .line 168
    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->res:Lcom/squareup/util/Res;

    .line 169
    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    .line 170
    iget-object v4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 171
    iget-object v5, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->locale:Ljava/util/Locale;

    .line 172
    iget-object v6, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    .line 173
    iget-object v7, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    .line 174
    iget-object v8, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    .line 175
    iget-object v9, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->appletSelection:Lcom/squareup/applet/AppletSelection;

    .line 176
    iget-object v10, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    .line 177
    iget-object v11, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    move-object v1, v0

    move-object v12, p1

    .line 167
    invoke-direct/range {v1 .. v12}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/time/CurrentTime;Ljava/util/Locale;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V

    return-object v0
.end method
