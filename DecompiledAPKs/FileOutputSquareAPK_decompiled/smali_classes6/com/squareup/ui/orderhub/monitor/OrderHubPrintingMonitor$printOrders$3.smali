.class final Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3;
.super Ljava/lang/Object;
.source "OrderHubPrintingMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->printOrders()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubPrintingMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubPrintingMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,142:1\n1642#2,2:143\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubPrintingMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3\n*L\n110#1,2:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u0014\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "it",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/Set;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 143
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 110
    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v2

    const-string v3, "orderId"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logOrderMarkedAsPrinted$orderhub_applet_release(Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getOrderRepository$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ordermanagerdata/OrderRepository;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ordermanagerdata/OrderRepository;->markOrdersAsPrinted(Ljava/util/Set;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3;->apply(Ljava/util/Set;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
