.class public final Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;
.super Ljava/lang/Object;
.source "OrderHubBadgingMonitor.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u000eH\u0016J\u0010\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;",
        "Lmortar/Scoped;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "statusBarEventManager",
        "Lcom/squareup/statusbar/event/StatusBarEventManager;",
        "orderHubApplet",
        "Lcom/squareup/ui/orderhub/OrderHubApplet;",
        "appletSelection",
        "Lcom/squareup/applet/AppletSelection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/applet/AppletSelection;Lcom/squareup/settings/server/Features;)V",
        "clearNewOrdersBadge",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "updateNewOrdersBadge",
        "count",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderHubApplet:Lcom/squareup/ui/orderhub/OrderHubApplet;

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

.field private final statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;


# direct methods
.method public constructor <init>(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/applet/AppletSelection;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statusBarEventManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubApplet"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletSelection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->orderHubApplet:Lcom/squareup/ui/orderhub/OrderHubApplet;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->appletSelection:Lcom/squareup/applet/AppletSelection;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$clearNewOrdersBadge(Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->clearNewOrdersBadge()V

    return-void
.end method

.method public static final synthetic access$updateNewOrdersBadge(Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;I)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->updateNewOrdersBadge(I)V

    return-void
.end method

.method private final clearNewOrdersBadge()V
    .locals 1

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->updateNewOrdersBadge(I)V

    return-void
.end method

.method private final updateNewOrdersBadge(I)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->orderHubApplet:Lcom/squareup/ui/orderhub/OrderHubApplet;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/OrderHubApplet;->setBadgeCount(I)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    const-string v1, "ORDER_HUB"

    invoke-interface {v0, v1, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager;->onBadgesChanged(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 30
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/OrderRepository;->unknownOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "features.featureEnabled(\u2026.ORDERHUB_APPLET_ROLLOUT)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;->appletSelection:Lcom/squareup/applet/AppletSelection;

    invoke-interface {v1}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "appletSelection.selectedApplet()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor$onEnterScope$1;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
