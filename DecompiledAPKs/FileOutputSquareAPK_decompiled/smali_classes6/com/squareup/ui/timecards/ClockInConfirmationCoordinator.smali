.class public Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "ClockInConfirmationCoordinator.java"


# instance fields
.field private clockInConfirmationButton:Lcom/squareup/marketfont/MarketButton;

.field private clockInConfirmationContainer:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 29
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 31
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInConfirmationCoordinator$Y-IAiPC5RJXQBRMrc1Mc9CTYtTM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInConfirmationCoordinator$Y-IAiPC5RJXQBRMrc1Mc9CTYtTM;-><init>(Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->clockInConfirmationButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInConfirmationCoordinator$SzNFOLCFj25i2lvLInHQZZoDZFM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInConfirmationCoordinator$SzNFOLCFj25i2lvLInHQZZoDZFM;-><init>(Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;)V

    .line 37
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 36
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 62
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 63
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clockin_confirmation:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->clockInConfirmationContainer:Landroid/widget/LinearLayout;

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->clockInConfirmationContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 65
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clockin_confirmation_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->clockInConfirmationButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$ClockInConfirmationCoordinator()Lrx/Subscription;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->clockInConfirmationScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 32
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$rrpZ5DfXd_13nFXWzFFlAFDb_wY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$rrpZ5DfXd_13nFXWzFFlAFDb_wY;-><init>(Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;)V

    .line 33
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$pQhzfGOjGDzyAExVNWA4K1X1Yto;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$pQhzfGOjGDzyAExVNWA4K1X1Yto;-><init>(Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$Nq1xdWAND_pipRSpy6eOKaYriM4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Nq1xdWAND_pipRSpy6eOKaYriM4;-><init>(Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;)V

    .line 34
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$ClockInConfirmationCoordinator(Landroid/view/View;)V
    .locals 1

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onClockInConfirmationClicked(Z)V

    return-void
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 2

    .line 53
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 54
    check-cast p1, Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_clock_in_confirmation_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 58
    iget-object p1, p1, Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;->hoursMinutesWorkedToday:Lcom/squareup/ui/timecards/HoursMinutes;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;->setTimeWorkedTodayInSubHeader(Lcom/squareup/ui/timecards/HoursMinutes;)V

    return-void
.end method
