.class synthetic Lcom/squareup/ui/timecards/TimecardsCoordinator$1;
.super Ljava/lang/Object;
.source "TimecardsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/TimecardsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$timecards$ADD_OR_EDIT_NOTES_BUTTON_CONFIG:[I

.field static final synthetic $SwitchMap$com$squareup$ui$timecards$Timecards$TimecardRequestState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 177
    invoke-static {}, Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;->values()[Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/timecards/TimecardsCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$ADD_OR_EDIT_NOTES_BUTTON_CONFIG:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/timecards/TimecardsCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$ADD_OR_EDIT_NOTES_BUTTON_CONFIG:[I

    sget-object v2, Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;->SHOW_ADD_NOTES:Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    invoke-virtual {v2}, Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/timecards/TimecardsCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$ADD_OR_EDIT_NOTES_BUTTON_CONFIG:[I

    sget-object v3, Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;->SHOW_EDIT_NOTES:Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    invoke-virtual {v3}, Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    .line 81
    :catch_1
    invoke-static {}, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->values()[Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/ui/timecards/TimecardsCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$Timecards$TimecardRequestState:[I

    :try_start_2
    sget-object v2, Lcom/squareup/ui/timecards/TimecardsCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$Timecards$TimecardRequestState:[I

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v3}, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/timecards/TimecardsCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$Timecards$TimecardRequestState:[I

    sget-object v2, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v2}, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
