.class public interface abstract Lcom/squareup/ui/timecards/TimecardsScope$Component;
.super Ljava/lang/Object;
.source "TimecardsScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/TimecardsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract addNotesCoordinator()Lcom/squareup/ui/timecards/AddNotesCoordinator;
.end method

.method public abstract clockInConfirmationCoordinator()Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;
.end method

.method public abstract clockInCoordinator()Lcom/squareup/ui/timecards/ClockInCoordinator;
.end method

.method public abstract clockInModalCoordinator()Lcom/squareup/ui/timecards/ClockInModalCoordinator;
.end method

.method public abstract clockInOrContinueScreen()Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Component;
.end method

.method public abstract clockInOut()Lcom/squareup/ui/timecards/ClockInOutScreen$Component;
.end method

.method public abstract clockOutConfirmationCoordinator()Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;
.end method

.method public abstract clockOutCoordinator()Lcom/squareup/ui/timecards/ClockOutCoordinator;
.end method

.method public abstract editNotesCoordinator()Lcom/squareup/ui/timecards/EditNotesCoordinator;
.end method

.method public abstract employeeJobsListCoordinator()Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;
.end method

.method public abstract employeeJobsListModalCoordinator()Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;
.end method

.method public abstract endBreakAfterLoginConfirmationCoordinator()Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;
.end method

.method public abstract endBreakAfterLoginCoordinator()Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;
.end method

.method public abstract endBreakConfirmationCoordinator()Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;
.end method

.method public abstract endBreakCoordinator()Lcom/squareup/ui/timecards/EndBreakCoordinator;
.end method

.method public abstract inject(Lcom/squareup/ui/timecards/TimecardsActionBarView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;)V
.end method

.method public abstract listBreaksCoordinator()Lcom/squareup/ui/timecards/ListBreaksCoordinator;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/timecards/TimecardsScopeRunner;
.end method

.method public abstract startBreakCoordinator()Lcom/squareup/ui/timecards/StartBreakCoordinator;
.end method

.method public abstract startBreakOrClockOutCoordinator()Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;
.end method

.method public abstract timecardsLoadingCoordinator()Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;
.end method

.method public abstract viewNotesCoordinator()Lcom/squareup/ui/timecards/ViewNotesCoordinator;
.end method
