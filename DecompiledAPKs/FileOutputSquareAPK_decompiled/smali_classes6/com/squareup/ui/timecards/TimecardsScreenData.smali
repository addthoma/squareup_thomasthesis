.class public Lcom/squareup/ui/timecards/TimecardsScreenData;
.super Ljava/lang/Object;
.source "TimecardsScreenData.java"


# instance fields
.field public final addOrEditNotesButtonConfig:Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

.field public final currentTimeMillis:J

.field public final device:Lcom/squareup/util/Device;

.field public final employeeName:Ljava/lang/String;

.field public final internetState:Lcom/squareup/connectivity/InternetState;

.field public final timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScreenData;->device:Lcom/squareup/util/Device;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/timecards/TimecardsScreenData;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    .line 22
    iput-object p3, p0, Lcom/squareup/ui/timecards/TimecardsScreenData;->employeeName:Ljava/lang/String;

    .line 23
    iput-wide p4, p0, Lcom/squareup/ui/timecards/TimecardsScreenData;->currentTimeMillis:J

    .line 24
    iput-object p6, p0, Lcom/squareup/ui/timecards/TimecardsScreenData;->internetState:Lcom/squareup/connectivity/InternetState;

    .line 25
    iput-object p7, p0, Lcom/squareup/ui/timecards/TimecardsScreenData;->addOrEditNotesButtonConfig:Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    return-void
.end method
