.class public Lcom/squareup/ui/timecards/TimecardsLoadingScreen;
.super Lcom/squareup/ui/timecards/InTimecardsScope;
.source "TimecardsLoadingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/timecards/TimecardsLoadingScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/timecards/TimecardsLoadingScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/TimecardsLoadingScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen;->INSTANCE:Lcom/squareup/ui/timecards/TimecardsLoadingScreen;

    .line 57
    sget-object v0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen;->INSTANCE:Lcom/squareup/ui/timecards/TimecardsLoadingScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/squareup/ui/timecards/InTimecardsScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 53
    const-class v0, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    .line 54
    invoke-interface {p1}, Lcom/squareup/ui/timecards/TimecardsScope$Component;->timecardsLoadingCoordinator()Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 49
    sget v0, Lcom/squareup/ui/timecards/R$layout;->timecards_loading_modal:I

    return v0
.end method
