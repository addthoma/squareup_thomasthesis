.class Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;
.super Ljava/lang/Object;
.source "ClockInOutPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/ClockInOutPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BreakTrackingHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;)V
    .locals 0

    .line 343
    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;Lcom/squareup/ui/timecards/ClockInOutPresenter$1;)V
    .locals 0

    .line 343
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;Lcom/squareup/permissions/Employee;)V
    .locals 0

    .line 343
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->getOpenTimecardAndBreak(Lcom/squareup/permissions/Employee;)V

    return-void
.end method

.method private getOpenTimecardAndBreak(Lcom/squareup/permissions/Employee;)V
    .locals 3

    .line 345
    new-instance v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    .line 347
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;

    move-result-object v0

    .line 348
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->build()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$400(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/server/employees/TimecardsService;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/server/employees/TimecardsService;->getOpenTimecardAndBreakResponse(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    .line 350
    invoke-static {v1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$300(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lrx/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$QTcPApWkdYlqmjDAu5ThnuvFhmI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$QTcPApWkdYlqmjDAu5ThnuvFhmI;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    .line 351
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    .line 352
    invoke-static {v1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$200(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/receiving/StandardReceiver;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$6MLVejq7lZGMgva5OjHz_zOoJcU;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$6MLVejq7lZGMgva5OjHz_zOoJcU;

    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$pFLQPlR-QhwyIkjiZcPwzvpUwYk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$pFLQPlR-QhwyIkjiZcPwzvpUwYk;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;Lcom/squareup/permissions/Employee;)V

    .line 353
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method static synthetic lambda$getOpenTimecardAndBreak$1(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)Ljava/lang/Boolean;
    .locals 0

    const/4 p0, 0x1

    .line 352
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onGetOpenTimecardAndBreakSuccess$5(Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)Ljava/lang/Boolean;
    .locals 0

    const/4 p0, 0x1

    .line 365
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$processTimecardAndJobResponses$12(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;)Ljava/lang/Boolean;
    .locals 0

    const/4 p0, 0x1

    .line 434
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private onGetOpenTimecardAndBreakSuccess(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/permissions/Employee;)V
    .locals 2

    .line 362
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$500(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_JOBS_SELECT:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$400(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/server/employees/TimecardsService;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;

    iget-object p2, p2, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-direct {v1, p2}, Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/server/employees/TimecardsService;->getEmployeeJobInfos(Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;)Lrx/Observable;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    .line 364
    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$300(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lrx/Scheduler;

    move-result-object v0

    invoke-virtual {p2, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    .line 365
    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$200(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/receiving/StandardReceiver;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$uMrx52s6Ov2RnBIAwcKrI7DuFF8;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$uMrx52s6Ov2RnBIAwcKrI7DuFF8;

    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$8qemOrCDR8S5MkYl1Ej3yfW6BiA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$8qemOrCDR8S5MkYl1Ej3yfW6BiA;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)V

    .line 366
    invoke-virtual {p2, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 374
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->processTimecardAndJobResponses(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V

    :goto_0
    return-void
.end method

.method private onTimecardBreakDefinitionSuccess(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V
    .locals 0

    .line 446
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->showStartBreakOrClockOutScreen(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 448
    iget-object p2, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$600(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/Timecards;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;->timecard_break_definition:Ljava/util/List;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/timecards/Timecards;->setTimecardBreakDefinitions(Ljava/util/List;)V

    .line 450
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$800(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/thread/executor/MainThread;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$JSyEFANCPV17A7t_IjF4CVlvAyk;

    invoke-direct {p2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$JSyEFANCPV17A7t_IjF4CVlvAyk;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    invoke-interface {p1, p2}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 452
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$800(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/thread/executor/MainThread;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$3CC_Z7hHU-haa2cSdJFsWp4fRw0;

    invoke-direct {p2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$3CC_Z7hHU-haa2cSdJFsWp4fRw0;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    invoke-interface {p1, p2}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private processTimecardAndJobResponses(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V
    .locals 9

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$600(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/Timecards;

    move-result-object v0

    if-eqz p2, :cond_0

    iget-object v1, p2, Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;->ordered_employee_job_info:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/Timecards;->setJobs(Ljava/util/List;)V

    .line 384
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 385
    :goto_1
    iget-object v3, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    .line 389
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$700(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOutView;

    invoke-virtual {p1, v2}, Lcom/squareup/ui/timecards/ClockInOutView;->showError(Z)V

    return-void

    :cond_3
    if-nez v0, :cond_4

    if-nez v1, :cond_4

    .line 394
    iget-object p2, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$600(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/Timecards;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/timecards/Timecards;->initRequestState(Ljava/lang/Long;)V

    .line 396
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$800(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/thread/executor/MainThread;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$3G11svErzpOe9bNvxRfc9KGzfWA;

    invoke-direct {p2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$3G11svErzpOe9bNvxRfc9KGzfWA;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    invoke-interface {p1, p2}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void

    :cond_4
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 403
    iget-object p2, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$600(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/Timecards;

    move-result-object v0

    iget-object p2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iget-object p2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, p2, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    new-instance v3, Ljava/util/Date;

    iget-object p2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object p2, p2, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 405
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    iget-object v4, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    iget-object p2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v5, p2, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v6, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    iget-object p2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v7, p2, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    .line 403
    invoke-virtual/range {v0 .. v7}, Lcom/squareup/ui/timecards/Timecards;->setTimecard(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)V

    .line 410
    iget-object p2, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$600(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/Timecards;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v0, v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 411
    invoke-static {v1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    .line 410
    invoke-virtual {p2, v0, v1, v2, p1}, Lcom/squareup/ui/timecards/Timecards;->setTimecardBreak(Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 415
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$800(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/thread/executor/MainThread;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$dk-FgxTE7St5-uh3EKPRJM1Nw64;

    invoke-direct {p2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$dk-FgxTE7St5-uh3EKPRJM1Nw64;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    invoke-interface {p1, p2}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void

    :cond_5
    if-eqz v0, :cond_6

    if-nez v1, :cond_6

    .line 420
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$600(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/Timecards;

    move-result-object v1

    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, v0, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, v0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    new-instance v4, Ljava/util/Date;

    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v0, v0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 422
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    iget-object v5, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v6, v0, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v7, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v8, v0, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    .line 420
    invoke-virtual/range {v1 .. v8}, Lcom/squareup/ui/timecards/Timecards;->setTimecard(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$900(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v0

    .line 428
    iget-object p1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    .line 429
    new-instance v1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsRequest;

    invoke-direct {v1, v0, p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$400(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/server/employees/TimecardsService;

    move-result-object p1

    invoke-interface {p1, v1}, Lcom/squareup/server/employees/TimecardsService;->listTimecardBreakDefinitions(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    .line 432
    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$300(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lrx/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$77jHTkZZfIcT6vXoEUSRhLvJgXI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$77jHTkZZfIcT6vXoEUSRhLvJgXI;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    .line 433
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    .line 434
    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$200(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/receiving/StandardReceiver;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$QytLqwIoJFbmNclXQHeR7soSy_s;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$QytLqwIoJFbmNclXQHeR7soSy_s;

    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$J9YC-1Y-EWkh1ck3mlGJ0KLXtlo;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$J9YC-1Y-EWkh1ck3mlGJ0KLXtlo;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V

    .line 435
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    :cond_6
    return-void
.end method

.method private showStartBreakOrClockOutScreen(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)Z
    .locals 3

    .line 471
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;->timecard_break_definition:Ljava/util/List;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;->timecard_break_definition:Ljava/util/List;

    .line 473
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 475
    iget-object p2, p2, Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;->ordered_employee_job_info:Ljava/util/List;

    .line 477
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-le p2, v2, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    if-nez p1, :cond_2

    .line 479
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    .line 480
    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$500(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_SWITCH_JOBS:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method


# virtual methods
.method public synthetic lambda$getOpenTimecardAndBreak$0$ClockInOutPresenter$BreakTrackingHelper()V
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1200(Lcom/squareup/ui/timecards/ClockInOutPresenter;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$getOpenTimecardAndBreak$4$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/permissions/Employee;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1

    .line 353
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$4PHhCmxSae6wUu_fx8ABWCPljW0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$4PHhCmxSae6wUu_fx8ABWCPljW0;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;Lcom/squareup/permissions/Employee;)V

    new-instance p1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$EvG0K2i5m3g1ynl1LqRDH3S2J7c;

    invoke-direct {p1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$EvG0K2i5m3g1ynl1LqRDH3S2J7c;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$13$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 436
    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->onTimecardBreakDefinitionSuccess(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V

    return-void
.end method

.method public synthetic lambda$null$14$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 438
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1100(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOutView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->showError(Z)V

    return-void
.end method

.method public synthetic lambda$null$2$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/permissions/Employee;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 354
    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->onGetOpenTimecardAndBreakSuccess(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/permissions/Employee;)V

    return-void
.end method

.method public synthetic lambda$null$3$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 355
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1400(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOutView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->showError(Z)V

    return-void
.end method

.method public synthetic lambda$null$6$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 368
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->processTimecardAndJobResponses(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V

    return-void
.end method

.method public synthetic lambda$null$7$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 371
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1300(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOutView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->showError(Z)V

    return-void
.end method

.method public synthetic lambda$onGetOpenTimecardAndBreakSuccess$8$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1

    .line 366
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$Fydd7MQF-8tS_YBh9mLCI8SEqsQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$Fydd7MQF-8tS_YBh9mLCI8SEqsQ;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)V

    new-instance p1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$fIQvpuYqB3boB5A8xNL4jtdUv0g;

    invoke-direct {p1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$fIQvpuYqB3boB5A8xNL4jtdUv0g;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onTimecardBreakDefinitionSuccess$16$ClockInOutPresenter$BreakTrackingHelper()V
    .locals 2

    .line 450
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1000(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen;->INSTANCE:Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onTimecardBreakDefinitionSuccess$17$ClockInOutPresenter$BreakTrackingHelper()V
    .locals 2

    .line 452
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1000(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->FIRST_SCREEN_IN_LAYER:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$processTimecardAndJobResponses$10$ClockInOutPresenter$BreakTrackingHelper()V
    .locals 2

    .line 415
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1000(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen;->INSTANCE:Lcom/squareup/ui/timecards/EndBreakConfirmationScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$processTimecardAndJobResponses$11$ClockInOutPresenter$BreakTrackingHelper()V
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1200(Lcom/squareup/ui/timecards/ClockInOutPresenter;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$processTimecardAndJobResponses$15$ClockInOutPresenter$BreakTrackingHelper(Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1

    .line 435
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$nHby49DyLKVgYOP7-iqDDLLvN_c;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$nHby49DyLKVgYOP7-iqDDLLvN_c;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V

    new-instance p1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$CzIVwyJvkyRjA8bL_falyotLVww;

    invoke-direct {p1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BreakTrackingHelper$CzIVwyJvkyRjA8bL_falyotLVww;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$processTimecardAndJobResponses$9$ClockInOutPresenter$BreakTrackingHelper()V
    .locals 2

    .line 396
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->this$0:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->access$1000(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/ClockInConfirmationScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInConfirmationScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
