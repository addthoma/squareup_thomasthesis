.class public Lcom/squareup/ui/timecards/TimecardsScopeRunner;
.super Ljava/lang/Object;
.source "TimecardsScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private application:Landroid/app/Application;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final currentTimeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

.field private final flow:Lflow/Flow;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;

.field private final res:Lcom/squareup/util/Res;

.field private final timecards:Lcom/squareup/ui/timecards/Timecards;

.field private final timecardsPrintingDispatcher:Lcom/squareup/print/TimecardsPrintingDispatcher;

.field private final unitToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/Timecards;Lflow/Flow;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Lcom/squareup/print/TimecardsPrintingDispatcher;Lcom/squareup/permissions/PermissionGatekeeper;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/timecards/Timecards;",
            "Lflow/Flow;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            "Lcom/squareup/print/TimecardsPrintingDispatcher;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->application:Landroid/app/Application;

    .line 102
    iput-object p3, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->res:Lcom/squareup/util/Res;

    .line 103
    iput-object p4, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    .line 104
    iput-object p5, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    .line 105
    iput-object p6, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 106
    iput-object p7, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    .line 107
    iput-object p8, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 108
    invoke-direct {p0, p2}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getCurrentTimeObservable(Lcom/squareup/time/CurrentTime;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    .line 109
    invoke-virtual {p9}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->unitToken:Ljava/lang/String;

    .line 110
    iput-object p10, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->posContainer:Lcom/squareup/ui/main/PosContainer;

    .line 111
    iput-object p11, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 112
    iput-object p12, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    .line 113
    iput-object p13, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecardsPrintingDispatcher:Lcom/squareup/print/TimecardsPrintingDispatcher;

    .line 114
    iput-object p14, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 115
    iput-object p15, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakConfirmationClicked(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Ljava/lang/String;Z)V
    .locals 0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakAfterLoginClicked(Ljava/lang/String;Z)V

    return-void
.end method

.method private endBreakAfterLoginClicked(Ljava/lang/String;Z)V
    .locals 2

    .line 797
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->setLoadingRequestState()V

    .line 798
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen;->INSTANCE:Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 799
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getAuthorization(Ljava/lang/String;Z)Lcom/squareup/protos/client/timecards/Authorization;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/Timecards;->stopBreak(Lcom/squareup/protos/client/timecards/Authorization;)V

    return-void
.end method

.method private endBreakConfirmationClicked(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    .line 790
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->setLoadingRequestState()V

    .line 791
    sget-object v0, Lcom/squareup/ui/timecards/EndBreakScreen;->INSTANCE:Lcom/squareup/ui/timecards/EndBreakScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->resetHistoryAndGoToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    .line 792
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getAuthorization(Ljava/lang/String;Z)Lcom/squareup/protos/client/timecards/Authorization;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/Timecards;->stopBreak(Lcom/squareup/protos/client/timecards/Authorization;)V

    return-void
.end method

.method private getAuthorization(Ljava/lang/String;Z)Lcom/squareup/protos/client/timecards/Authorization;
    .locals 2

    .line 804
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 805
    new-instance v0, Lcom/squareup/protos/client/timecards/Authorization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/Authorization$Builder;-><init>()V

    .line 806
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/Authorization$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/Authorization$Builder;

    move-result-object p1

    .line 807
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/timecards/Authorization$Builder;->using_team_passcode(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/Authorization$Builder;

    move-result-object p1

    .line 808
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/Authorization$Builder;->build()Lcom/squareup/protos/client/timecards/Authorization;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private getCurrentTimeObservable(Lcom/squareup/time/CurrentTime;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/time/CurrentTime;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.intent.action.TIME_TICK"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/RxBroadcastReceiver;->registerForIntents(Landroid/content/Context;[Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$Thrafqj_TTMPfQcC-2juuNLFe98;

    invoke-direct {v1, p1}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$Thrafqj_TTMPfQcC-2juuNLFe98;-><init>(Lcom/squareup/time/CurrentTime;)V

    .line 124
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 125
    invoke-interface {p1}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object p1

    invoke-virtual {p1}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private getFilteredJobInfoList(Lcom/squareup/ui/timecards/Timecards$State;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/timecards/Timecards$State;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;"
        }
    .end annotation

    .line 484
    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-nez v0, :cond_0

    .line 485
    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    return-object p1

    .line 487
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 488
    iget-object v1, p1, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    .line 489
    iget-object v3, v2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_token:Ljava/lang/String;

    .line 490
    iget-object v4, p1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v4, v4, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v4, v4, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_token:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 492
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 493
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;
    .locals 1

    .line 513
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p1

    .line 514
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/EmployeeInfo;->getFullName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getHrsMinsWorkedToday(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/HoursMinutes;
    .locals 5

    .line 782
    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 785
    :cond_0
    new-instance v0, Lcom/squareup/ui/timecards/HoursMinutes;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long v1, v1, v3

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/timecards/HoursMinutes;-><init>(J)V

    return-object v0
.end method

.method private getJobTitle(Lcom/squareup/protos/client/timecards/Timecard;)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    .line 773
    iget-object p1, p1, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    if-eqz p1, :cond_0

    .line 775
    iget-object p1, p1, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private getNotesButtonConfig(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;
    .locals 0

    .line 814
    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 815
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 818
    :cond_0
    sget-object p1, Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;->SHOW_EDIT_NOTES:Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    return-object p1

    .line 816
    :cond_1
    :goto_0
    sget-object p1, Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;->SHOW_ADD_NOTES:Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    return-object p1
.end method

.method static synthetic lambda$getCurrentTimeObservable$0(Lcom/squareup/time/CurrentTime;Landroid/content/Intent;)Ljava/lang/Long;
    .locals 0

    .line 124
    invoke-interface {p0}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object p0

    invoke-virtual {p0}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$timecardsLoadingScreenData$13(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;
    .locals 5

    .line 502
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    .line 503
    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v0, v1, :cond_0

    goto :goto_1

    .line 507
    :cond_0
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, p0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    iget-object p0, p0, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    if-eqz p0, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;-><init>(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;ZZ)V

    return-object v0

    .line 504
    :cond_3
    :goto_1
    new-instance p0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;

    invoke-direct {p0, v0, v2, v2}, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;-><init>(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;ZZ)V

    return-object p0
.end method

.method private resetHistoryAndGoToScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 4

    .line 710
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    .line 711
    const-class v2, Lcom/squareup/ui/timecards/InTimecardsScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Histories;->popWhile(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    .line 712
    invoke-virtual {v0, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 714
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-virtual {p1, v0, v1}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method


# virtual methods
.method public clockInConfirmationScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;",
            ">;"
        }
    .end annotation

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 324
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$BO8C9jzCpCJEu4Xj1dIMOOhSMDQ;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$BO8C9jzCpCJEu4Xj1dIMOOhSMDQ;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 323
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public clockInScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/ClockInScreen$Data;",
            ">;"
        }
    .end annotation

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 372
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$bnVxZnId3KdPdQG6qnHVd5woV4c;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$bnVxZnId3KdPdQG6qnHVd5woV4c;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 371
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public clockOutConfirmationClicked()V
    .locals 2

    .line 643
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_CLOCK_OUT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 644
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->setLoadingRequestState()V

    .line 645
    sget-object v0, Lcom/squareup/ui/timecards/ClockOutScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockOutScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->resetHistoryAndGoToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    .line 646
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->stopTimecard()V

    return-void
.end method

.method public clockOutConfirmationScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;",
            ">;"
        }
    .end annotation

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 348
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$33G30BQK3sgHyDzr7Oez2CKtNbc;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$33G30BQK3sgHyDzr7Oez2CKtNbc;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 347
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public clockOutScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/ClockOutScreen$Data;",
            ">;"
        }
    .end annotation

    .line 393
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 394
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$Iq374QAnzzcAEXHgTIFACbGfoKY;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$Iq374QAnzzcAEXHgTIFACbGfoKY;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 393
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public editNotesScreenData()Lcom/squareup/ui/timecards/EditNotesScreen$Data;
    .locals 2

    .line 446
    new-instance v0, Lcom/squareup/ui/timecards/EditNotesScreen$Data;

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v1}, Lcom/squareup/ui/timecards/Timecards;->getLatestState()Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/EditNotesScreen$Data;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public employeeJobsListScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;",
            ">;"
        }
    .end annotation

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 423
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$2ulbWQZUxo3UAAON1a88SZnFRng;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$2ulbWQZUxo3UAAON1a88SZnFRng;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 422
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public endBreakAfterLoginConfirmationScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;",
            ">;"
        }
    .end annotation

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 304
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$84OH_-lVfSIHmTc-6YpAgFfRIK0;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$84OH_-lVfSIHmTc-6YpAgFfRIK0;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 303
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public endBreakAfterLoginScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;",
            ">;"
        }
    .end annotation

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 274
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$-dDwmMO5eEA1ro2tt1eZbSwdTcw;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$-dDwmMO5eEA1ro2tt1eZbSwdTcw;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 273
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public endBreakAuthorizationNeeded(Lcom/squareup/ui/timecards/Timecards$State;J)Z
    .locals 7

    .line 733
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$State;->minBreakDurationSeconds:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 736
    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 737
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v3, p1

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    add-long/2addr v1, v3

    cmp-long p1, p2, v1

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public endBreakConfirmationScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;",
            ">;"
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 218
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$qCXbHdG-fqBz4D0RKOfby0QhxNo;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$qCXbHdG-fqBz4D0RKOfby0QhxNo;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 217
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public endBreakScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/EndBreakScreen$Data;",
            ">;"
        }
    .end annotation

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 253
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$MbXVGs7L5B9cW4KNvTea1dIxtww;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$MbXVGs7L5B9cW4KNvTea1dIxtww;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 252
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public exitFlow()V
    .locals 4

    .line 528
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_EXIT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 529
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->noPasscode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 530
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onlyRequireForRestrictedActions()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 533
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->posContainer:Lcom/squareup/ui/main/PosContainer;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/squareup/ui/timecards/InTimecardsScope;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    goto :goto_1

    .line 531
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->attemptScreenLock()V

    :goto_1
    return-void
.end method

.method public finish()V
    .locals 4

    .line 543
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_FINISH:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 544
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->noPasscode()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 545
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onlyRequireForRestrictedActions()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->posContainer:Lcom/squareup/ui/main/PosContainer;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/timecards/InTimecardsScope;

    aput-object v3, v2, v1

    invoke-interface {v0, v2}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    goto :goto_1

    .line 546
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->attemptScreenLock(Z)V

    :goto_1
    return-void
.end method

.method public finishAndLogOut()V
    .locals 5

    .line 558
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_FINISH:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 559
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->posContainer:Lcom/squareup/ui/main/PosContainer;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/timecards/InTimecardsScope;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-interface {v0, v2}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    .line 560
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->attemptScreenLock(Z)V

    return-void
.end method

.method public synthetic lambda$clockInConfirmationScreenData$8$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 326
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 327
    new-instance v2, Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 329
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    move-object v4, v2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v11}, Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/HoursMinutes;)V

    return-object v2

    .line 332
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v2, :cond_1

    goto :goto_0

    .line 339
    :cond_1
    new-instance v2, Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;

    iget-object v4, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v5, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 341
    invoke-direct {v0, v3}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 342
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getHrsMinsWorkedToday(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/HoursMinutes;

    move-result-object v10

    move-object v3, v2

    move-object/from16 v9, p3

    invoke-direct/range {v3 .. v10}, Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/HoursMinutes;)V

    return-object v2

    .line 334
    :cond_2
    :goto_0
    new-instance v2, Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;

    iget-object v12, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v13, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 336
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    const/16 v18, 0x0

    move-object v11, v2

    move-object/from16 v17, p3

    invoke-direct/range {v11 .. v18}, Lcom/squareup/ui/timecards/ClockInConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/HoursMinutes;)V

    return-object v2
.end method

.method public synthetic lambda$clockInScreenData$10$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/ClockInScreen$Data;
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 374
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 375
    new-instance v2, Lcom/squareup/ui/timecards/ClockInScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 376
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v4, v2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v12}, Lcom/squareup/ui/timecards/ClockInScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/String;)V

    return-object v2

    .line 379
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-nez v2, :cond_1

    goto :goto_0

    .line 385
    :cond_1
    new-instance v2, Lcom/squareup/ui/timecards/ClockInScreen$Data;

    iget-object v4, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v5, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 386
    invoke-direct {v0, v3}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 387
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getNotesButtonConfig(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    move-result-object v10

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 388
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getJobTitle(Lcom/squareup/protos/client/timecards/Timecard;)Ljava/lang/String;

    move-result-object v11

    move-object v3, v2

    move-object/from16 v9, p3

    invoke-direct/range {v3 .. v11}, Lcom/squareup/ui/timecards/ClockInScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/String;)V

    return-object v2

    .line 381
    :cond_2
    :goto_0
    new-instance v2, Lcom/squareup/ui/timecards/ClockInScreen$Data;

    iget-object v13, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v14, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 382
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v12, v2

    move-object/from16 v18, p3

    invoke-direct/range {v12 .. v20}, Lcom/squareup/ui/timecards/ClockInScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/String;)V

    return-object v2
.end method

.method public synthetic lambda$clockOutConfirmationScreenData$9$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 350
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 351
    new-instance v2, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 352
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v7

    .line 353
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v4, v2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v13}, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Lcom/squareup/ui/timecards/HoursMinutes;Ljava/lang/String;)V

    return-object v2

    .line 355
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    if-nez v2, :cond_1

    goto :goto_0

    .line 363
    :cond_1
    new-instance v2, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;

    iget-object v4, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v5, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 364
    invoke-direct {v0, v3}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v6

    .line 365
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getNotesButtonConfig(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    move-result-object v10

    .line 366
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getHrsMinsWorkedToday(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/HoursMinutes;

    move-result-object v11

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getJobTitle(Lcom/squareup/protos/client/timecards/Timecard;)Ljava/lang/String;

    move-result-object v12

    move-object v3, v2

    move-object/from16 v9, p3

    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Lcom/squareup/ui/timecards/HoursMinutes;Ljava/lang/String;)V

    return-object v2

    .line 358
    :cond_2
    :goto_0
    new-instance v2, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;

    iget-object v14, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v15, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 359
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v16

    .line 360
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object v13, v2

    move-object/from16 v19, p3

    invoke-direct/range {v13 .. v22}, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Lcom/squareup/ui/timecards/HoursMinutes;Ljava/lang/String;)V

    return-object v2
.end method

.method public synthetic lambda$clockOutScreenData$11$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/ClockOutScreen$Data;
    .locals 28

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 396
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 397
    new-instance v2, Lcom/squareup/ui/timecards/ClockOutScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 398
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v7

    .line 399
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v4, v2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v14}, Lcom/squareup/ui/timecards/ClockOutScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/HoursMinutes;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;ZZ)V

    return-object v2

    .line 401
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_3

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-nez v2, :cond_1

    goto :goto_1

    .line 407
    :cond_1
    new-instance v2, Ljava/util/Date;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, v3, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 408
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    new-instance v3, Ljava/util/Date;

    iget-object v4, v1, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v4, v4, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    .line 409
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 408
    invoke-static {v2, v3}, Lcom/squareup/ui/timecards/HoursMinutes;->getDiff(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/ui/timecards/HoursMinutes;

    move-result-object v13

    .line 411
    iget-object v2, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v3, Lcom/squareup/ui/timecards/Feature;->RECEIPT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecardsPrintingDispatcher:Lcom/squareup/print/TimecardsPrintingDispatcher;

    .line 412
    invoke-virtual {v2}, Lcom/squareup/print/TimecardsPrintingDispatcher;->canPrintTimecardsSummary()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    const/4 v15, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    const/4 v15, 0x0

    .line 414
    :goto_0
    new-instance v2, Lcom/squareup/ui/timecards/ClockOutScreen$Data;

    iget-object v7, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v8, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 415
    invoke-direct {v0, v3}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v9

    .line 416
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-object v14, v1, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    .line 417
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->showViewNotesButton(Lcom/squareup/ui/timecards/Timecards$State;)Z

    move-result v16

    move-object v6, v2

    move-object/from16 v12, p3

    invoke-direct/range {v6 .. v16}, Lcom/squareup/ui/timecards/ClockOutScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/HoursMinutes;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;ZZ)V

    return-object v2

    .line 403
    :cond_3
    :goto_1
    new-instance v2, Lcom/squareup/ui/timecards/ClockOutScreen$Data;

    iget-object v3, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v19, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 404
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v20

    .line 405
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v21

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move-object/from16 v23, p3

    invoke-direct/range {v17 .. v27}, Lcom/squareup/ui/timecards/ClockOutScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/HoursMinutes;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;ZZ)V

    return-object v2
.end method

.method public synthetic lambda$employeeJobsListScreenData$12$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;
    .locals 10

    .line 425
    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 426
    :goto_0
    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v0, v1, :cond_1

    .line 427
    new-instance v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 428
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v4

    .line 429
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const/4 v8, 0x0

    move-object v1, v0

    move-object v7, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/util/List;Z)V

    return-object v0

    .line 431
    :cond_1
    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v0, v1, :cond_3

    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    if-nez v0, :cond_2

    goto :goto_1

    .line 438
    :cond_2
    new-instance v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, p1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 439
    invoke-direct {p0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v4

    .line 440
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 441
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFilteredJobInfoList(Lcom/squareup/ui/timecards/Timecards$State;)Ljava/util/List;

    move-result-object v8

    move-object v1, v0

    move-object v7, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/util/List;Z)V

    return-object v0

    .line 433
    :cond_3
    :goto_1
    new-instance v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 434
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v4

    .line 435
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const/4 v8, 0x0

    move-object v1, v0

    move-object v7, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/util/List;Z)V

    return-object v0
.end method

.method public synthetic lambda$endBreakAfterLoginConfirmationScreenData$7$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p1

    .line 306
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 307
    new-instance v1, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/4 v7, 0x0

    .line 308
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-object v4, v1

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;)V

    return-object v1

    .line 311
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    if-eqz v1, :cond_1

    goto :goto_0

    .line 317
    :cond_1
    new-instance v1, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;

    iget-object v3, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v4, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/4 v5, 0x0

    .line 318
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v2, v1

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v8}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;)V

    return-object v1

    .line 313
    :cond_2
    :goto_0
    new-instance v1, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;

    iget-object v9, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v10, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/4 v11, 0x0

    .line 314
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-object v8, v1

    move-object/from16 v14, p3

    invoke-direct/range {v8 .. v14}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;)V

    return-object v1
.end method

.method public synthetic lambda$endBreakAfterLoginScreenData$6$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 276
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 277
    new-instance v1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/4 v7, 0x0

    .line 278
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v4, v1

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v12}, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/lang/Long;Z)V

    return-object v1

    .line 281
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    if-nez v2, :cond_1

    goto :goto_0

    .line 289
    :cond_1
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    .line 290
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, v1, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    .line 291
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    mul-long v4, v4, v6

    add-long/2addr v2, v4

    .line 293
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakAuthorizationNeeded(Lcom/squareup/ui/timecards/Timecards$State;J)Z

    move-result v14

    .line 295
    new-instance v1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;

    iget-object v7, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v8, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/4 v9, 0x0

    .line 296
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 297
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    move-object v6, v1

    move-object/from16 v12, p3

    invoke-direct/range {v6 .. v14}, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/lang/Long;Z)V

    return-object v1

    .line 285
    :cond_2
    :goto_0
    new-instance v1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;

    iget-object v2, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v17, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/16 v18, 0x0

    .line 286
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object v15, v1

    move-object/from16 v16, v2

    move-object/from16 v21, p3

    invoke-direct/range {v15 .. v23}, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/lang/Long;Z)V

    return-object v1
.end method

.method public synthetic lambda$endBreakConfirmationScreenData$4$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 220
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    if-ne v2, v3, :cond_0

    .line 221
    new-instance v2, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;

    iget-object v6, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v7, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 223
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v5, v2

    move-object/from16 v11, p3

    .line 224
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/Long;Ljava/lang/Boolean;)V

    return-object v2

    .line 227
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    if-nez v2, :cond_1

    goto :goto_0

    .line 237
    :cond_1
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    .line 238
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, v1, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    .line 239
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    mul-long v4, v4, v6

    add-long/2addr v2, v4

    .line 241
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakAuthorizationNeeded(Lcom/squareup/ui/timecards/Timecards$State;J)Z

    move-result v4

    .line 243
    new-instance v15, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;

    iget-object v6, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v7, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v5, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 245
    invoke-direct {v0, v5}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    .line 246
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getNotesButtonConfig(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    move-result-object v12

    .line 247
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    move-object v5, v15

    move-object/from16 v11, p3

    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/Long;Ljava/lang/Boolean;)V

    return-object v15

    .line 231
    :cond_2
    :goto_0
    new-instance v2, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;

    iget-object v6, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v7, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 233
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v5, v2

    move-object/from16 v11, p3

    .line 234
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/Long;Ljava/lang/Boolean;)V

    return-object v2
.end method

.method public synthetic lambda$endBreakScreenData$5$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/EndBreakScreen$Data;
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 255
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 256
    new-instance v2, Lcom/squareup/ui/timecards/EndBreakScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 257
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    move-object v4, v2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v11}, Lcom/squareup/ui/timecards/EndBreakScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    return-object v2

    .line 260
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    if-eqz v2, :cond_1

    goto :goto_0

    .line 266
    :cond_1
    new-instance v2, Lcom/squareup/ui/timecards/EndBreakScreen$Data;

    iget-object v4, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v5, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 267
    invoke-direct {v0, v3}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 268
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getNotesButtonConfig(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    move-result-object v10

    move-object v3, v2

    move-object/from16 v9, p3

    invoke-direct/range {v3 .. v10}, Lcom/squareup/ui/timecards/EndBreakScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    return-object v2

    .line 262
    :cond_2
    :goto_0
    new-instance v2, Lcom/squareup/ui/timecards/EndBreakScreen$Data;

    iget-object v12, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v13, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 263
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    const/16 v18, 0x0

    move-object v11, v2

    move-object/from16 v17, p3

    invoke-direct/range {v11 .. v18}, Lcom/squareup/ui/timecards/EndBreakScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    return-object v2
.end method

.method public synthetic lambda$listBreaksScreenData$2$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/ListBreaksScreen$Data;
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 174
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 175
    new-instance v2, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 176
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v7

    .line 177
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    move-object v4, v2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v11}, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/util/List;)V

    return-object v2

    .line 179
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    .line 181
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 186
    :cond_1
    new-instance v2, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;

    iget-object v4, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v5, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 187
    invoke-direct {v0, v3}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v6

    .line 188
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-object v10, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    move-object v3, v2

    move-object/from16 v9, p3

    invoke-direct/range {v3 .. v10}, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/util/List;)V

    return-object v2

    .line 182
    :cond_2
    :goto_0
    new-instance v2, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;

    iget-object v12, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v13, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 183
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v14

    .line 184
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    const/16 v18, 0x0

    move-object v11, v2

    move-object/from16 v17, p3

    invoke-direct/range {v11 .. v18}, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/util/List;)V

    return-object v2
.end method

.method public synthetic lambda$onClockInConfirmationClicked$14$TimecardsScopeRunner(ZLcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 627
    iget-object v0, p2, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 631
    :cond_0
    iget-object v0, p2, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 632
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->setLoadingRequestState()V

    .line 633
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/ui/timecards/ClockInModalScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInModalScreen;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/squareup/ui/timecards/ClockInScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInScreen;

    :goto_0
    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 634
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->unitToken:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object p2, p2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_token:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/timecards/Timecards;->startTimecard(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 636
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;->INSTANCE:Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;

    goto :goto_1

    :cond_3
    sget-object p1, Lcom/squareup/ui/timecards/EmployeeJobsListScreen;->INSTANCE:Lcom/squareup/ui/timecards/EmployeeJobsListScreen;

    :goto_1
    invoke-virtual {p2, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_4

    .line 628
    :cond_4
    :goto_2
    iget-object p2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {p2}, Lcom/squareup/ui/timecards/Timecards;->setLoadingRequestState()V

    .line 629
    iget-object p2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    if-eqz p1, :cond_5

    sget-object p1, Lcom/squareup/ui/timecards/ClockInModalScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInModalScreen;

    goto :goto_3

    :cond_5
    sget-object p1, Lcom/squareup/ui/timecards/ClockInScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInScreen;

    :goto_3
    invoke-virtual {p2, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 630
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    iget-object p2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->unitToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/timecards/Timecards;->startTimecard(Ljava/lang/String;)V

    :goto_4
    return-void
.end method

.method public synthetic lambda$printReceiptClicked$15$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 691
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecardsPrintingDispatcher:Lcom/squareup/print/TimecardsPrintingDispatcher;

    iget-object v1, p1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/print/TimecardsPrintingDispatcher;->printTimecardsSummary(Lcom/squareup/permissions/Employee;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;)V

    return-void
.end method

.method public synthetic lambda$startBreakOrClockOutScreenData$1$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;
    .locals 29

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 143
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 144
    new-instance v2, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 145
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    .line 146
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v4, v2

    move-object/from16 v11, p3

    invoke-direct/range {v4 .. v15}, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;Lcom/squareup/ui/timecards/HoursMinutes;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/String;ZZ)V

    return-object v2

    .line 149
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_4

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    if-nez v2, :cond_1

    goto :goto_2

    .line 155
    :cond_1
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    .line 156
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v15, 0x1

    goto :goto_0

    :cond_2
    const/4 v15, 0x0

    .line 157
    :goto_0
    iget-object v2, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v5, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_SWITCH_JOBS:Lcom/squareup/ui/timecards/Feature;

    .line 158
    invoke-virtual {v2, v5}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    .line 159
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v4, :cond_3

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v5, v1, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    if-ne v2, v5, :cond_3

    const/16 v16, 0x1

    goto :goto_1

    :cond_3
    const/16 v16, 0x0

    .line 161
    :goto_1
    new-instance v2, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;

    iget-object v6, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v7, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 162
    invoke-direct {v0, v3}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v8

    .line 163
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getHrsMinsWorkedToday(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/HoursMinutes;

    move-result-object v9

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 164
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getNotesButtonConfig(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    move-result-object v13

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 165
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getJobTitle(Lcom/squareup/protos/client/timecards/Timecard;)Ljava/lang/String;

    move-result-object v14

    move-object v5, v2

    move-object/from16 v12, p3

    invoke-direct/range {v5 .. v16}, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;Lcom/squareup/ui/timecards/HoursMinutes;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/String;ZZ)V

    return-object v2

    .line 151
    :cond_4
    :goto_2
    new-instance v2, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;

    iget-object v3, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v19, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 152
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    .line 153
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move-object/from16 v24, p3

    invoke-direct/range {v17 .. v28}, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;Lcom/squareup/ui/timecards/HoursMinutes;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/String;ZZ)V

    return-object v2
.end method

.method public synthetic lambda$startBreakScreenData$3$TimecardsScopeRunner(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/timecards/StartBreakScreen$Data;
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 196
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v2, v3, :cond_0

    .line 197
    new-instance v2, Lcom/squareup/ui/timecards/StartBreakScreen$Data;

    iget-object v5, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v6, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 198
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v7

    .line 199
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v4, v2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v12}, Lcom/squareup/ui/timecards/StartBreakScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/Long;)V

    return-object v2

    .line 201
    :cond_0
    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    .line 203
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 208
    :cond_1
    new-instance v2, Lcom/squareup/ui/timecards/StartBreakScreen$Data;

    iget-object v4, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v5, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 209
    invoke-direct {v0, v3}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v6

    .line 210
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getNotesButtonConfig(Lcom/squareup/ui/timecards/Timecards$State;)Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    move-result-object v10

    iget-object v3, v1, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    .line 211
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v13, v1

    add-long/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object v3, v2

    move-object/from16 v9, p3

    invoke-direct/range {v3 .. v11}, Lcom/squareup/ui/timecards/StartBreakScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/Long;)V

    return-object v2

    .line 204
    :cond_2
    :goto_0
    new-instance v2, Lcom/squareup/ui/timecards/StartBreakScreen$Data;

    iget-object v13, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->device:Lcom/squareup/util/Device;

    sget-object v14, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v1, v1, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 205
    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->getFullDisplayName(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object v15

    .line 206
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v12, v2

    move-object/from16 v18, p3

    invoke-direct/range {v12 .. v20}, Lcom/squareup/ui/timecards/StartBreakScreen$Data;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/Long;)V

    return-object v2
.end method

.method public listBreaksScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/ListBreaksScreen$Data;",
            ">;"
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 172
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$X0Aor-p2ezbzAGOlH0jwGU7quWE;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$X0Aor-p2ezbzAGOlH0jwGU7quWE;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 171
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public navigateToClockInOrContinue()V
    .locals 3

    .line 696
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/ClockInOrContinueScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInOrContinueScreen;

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public navigateToEndBreakAfterLogin()V
    .locals 3

    .line 700
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen;->INSTANCE:Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen;

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public navigateToHomeScreen()V
    .locals 2

    .line 718
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 721
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    return-void
.end method

.method public onAddNotesClicked()V
    .locals 2

    .line 747
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_ADD_NOTES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 748
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/AddNotesScreen;->INSTANCE:Lcom/squareup/ui/timecards/AddNotesScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackClicked()V
    .locals 2

    .line 518
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_BACK:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 519
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public onBreakSelected(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2

    .line 580
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_LIST_BREAKS_SELECT_BREAK:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 581
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->setLoadingRequestState()V

    .line 582
    sget-object v0, Lcom/squareup/ui/timecards/StartBreakScreen;->INSTANCE:Lcom/squareup/ui/timecards/StartBreakScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->resetHistoryAndGoToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    .line 583
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->startBreak(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public onClockInConfirmationClicked(Z)V
    .locals 2

    .line 621
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz p1, :cond_0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_CLOCK_IN:Lcom/squareup/analytics/RegisterTapName;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_CLOCK_IN:Lcom/squareup/analytics/RegisterTapName;

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 626
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$Kyklhn9TesD9wPIF-RgZXMH9krI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$Kyklhn9TesD9wPIF-RgZXMH9krI;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Z)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public onClockInOutClicked()V
    .locals 2

    .line 725
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_SUCCESS_CLOCK_IN_OUT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 726
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/ClockInOutScreen;->FROM_PASSCODE:Lcom/squareup/ui/timecards/ClockInOutScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onClockOutClicked()V
    .locals 2

    .line 564
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_CLOCK_OUT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 565
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->NOT_FIRST_SCREEN_IN_LAYER:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onEditNotesClicked()V
    .locals 2

    .line 752
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_EDIT_NOTES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 753
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/EditNotesScreen;->INSTANCE:Lcom/squareup/ui/timecards/EditNotesScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onEndBreakAfterLoginClicked()V
    .locals 2

    .line 650
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_END_BREAK:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 651
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->getEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/permissions/Employee;->token()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakAfterLoginClicked(Ljava/lang/String;Z)V

    return-void
.end method

.method public onEndBreakAfterLoginCloseClicked(Z)V
    .locals 1

    if-eqz p1, :cond_1

    .line 681
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    .line 682
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/Timecards;->getEmployee()Lcom/squareup/permissions/Employee;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/Permission;->END_TIMECARD_BREAK_EARLY:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 685
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->finishAndLogOut()V

    goto :goto_1

    .line 683
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->navigateToHomeScreen()V

    :goto_1
    return-void
.end method

.method public onEndBreakConfirmationClicked()V
    .locals 2

    .line 601
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_END_BREAK:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 602
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->getEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakConfirmationClicked(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method public onEndBreakEarlyAfterLoginClicked()V
    .locals 3

    .line 655
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_END_BREAK_EARLY:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 656
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->getEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    .line 657
    sget-object v1, Lcom/squareup/permissions/Permission;->END_TIMECARD_BREAK_EARLY:Lcom/squareup/permissions/Permission;

    invoke-static {v1}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 658
    invoke-virtual {v0}, Lcom/squareup/permissions/Employee;->token()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakAfterLoginClicked(Ljava/lang/String;Z)V

    goto :goto_0

    .line 660
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->END_TIMECARD_BREAK_EARLY:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/timecards/TimecardsScopeRunner$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner$2;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    :goto_0
    return-void
.end method

.method public onEndBreakEarlyConfirmationClicked()V
    .locals 3

    .line 606
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_END_BREAK_EARLY:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 607
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->getEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    .line 608
    sget-object v1, Lcom/squareup/permissions/Permission;->END_TIMECARD_BREAK_EARLY:Lcom/squareup/permissions/Permission;

    invoke-static {v1}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    invoke-virtual {v0}, Lcom/squareup/permissions/Employee;->token()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakConfirmationClicked(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 611
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->END_TIMECARD_BREAK_EARLY:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/timecards/TimecardsScopeRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner$1;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/Timecards;->loadCurrentEmployeeData(Lcom/squareup/permissions/Employee;)V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onJobSelected(Ljava/lang/String;Z)V
    .locals 2

    .line 587
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->EMPLOYEE_JOBS_LIST_JOBS_SELECT_JOB:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 588
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->setLoadingRequestState()V

    .line 589
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/ui/timecards/ClockInModalScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInModalScreen;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/ui/timecards/ClockInScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInScreen;

    :goto_0
    invoke-virtual {v0, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 590
    iget-object p2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->unitToken:Ljava/lang/String;

    invoke-virtual {p2, v0, p1}, Lcom/squareup/ui/timecards/Timecards;->startTimecard(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onJobSelectedToSwitchJobs(Ljava/lang/String;)V
    .locals 2

    .line 594
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->EMPLOYEE_JOBS_LIST_JOBS_SELECT_JOB:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 595
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->setLoadingRequestState()V

    .line 596
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/ClockInScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 597
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/Timecards;->switchJobs(Ljava/lang/String;)V

    return-void
.end method

.method public onNotesSaved(Ljava/lang/String;)V
    .locals 2

    .line 762
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_NOTES_SAVED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 763
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/Timecards;->saveNotes(Ljava/lang/String;)V

    .line 764
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public onSwitchJobsClicked()V
    .locals 2

    .line 569
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_SWITCH_JOBS:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 570
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/EmployeeJobsListScreen;->INSTANCE:Lcom/squareup/ui/timecards/EmployeeJobsListScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onTakeBreakClicked()V
    .locals 2

    .line 574
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_START_BREAK:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 575
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/ListBreaksScreen;->INSTANCE:Lcom/squareup/ui/timecards/ListBreaksScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onViewNotesClicked()V
    .locals 2

    .line 757
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_VIEW_NOTES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 758
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/timecards/ViewNotesScreen;->INSTANCE:Lcom/squareup/ui/timecards/ViewNotesScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public printReceiptClicked()V
    .locals 2

    .line 690
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$Qq3QsRthtyeKsSjH-WGZi41zf9U;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$Qq3QsRthtyeKsSjH-WGZi41zf9U;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public showAddOrEditNotesButton()Z
    .locals 2

    .line 768
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->TIMECARD_NOTES:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v0

    return v0
.end method

.method public showViewNotesButton(Lcom/squareup/ui/timecards/Timecards$State;)Z
    .locals 2

    .line 826
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->TIMECARD_NOTES:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 827
    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    if-eqz p1, :cond_1

    .line 829
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    if-eqz v0, :cond_0

    .line 830
    iget-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->timecard_notes:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->timecard_notes:Ljava/lang/String;

    .line 831
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public startBreakOrClockOutScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;",
            ">;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 141
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$rZxj2VNwdu1JEQ4OaVMIPdoc7mc;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$rZxj2VNwdu1JEQ4OaVMIPdoc7mc;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 140
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public startBreakScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/StartBreakScreen$Data;",
            ">;"
        }
    .end annotation

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->currentTimeObservable:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 194
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$TD1MoB9acrllGzFj_6teXdUOBHc;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$TD1MoB9acrllGzFj_6teXdUOBHc;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 193
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public timecardsLoadingScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;",
            ">;"
        }
    .end annotation

    .line 500
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->state()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$G9W-DA2BOE8iTg1p28vDDklI_go;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$TimecardsScopeRunner$G9W-DA2BOE8iTg1p28vDDklI_go;

    .line 501
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public viewNotesScreenData()Lcom/squareup/ui/timecards/ViewNotesScreen$Data;
    .locals 6

    .line 455
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/Timecards;->getLatestState()Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v0, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    .line 456
    iget-object v2, v1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v2, v2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v1, v1, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 462
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 463
    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v2}, Lcom/squareup/ui/timecards/Timecards;->getLatestState()Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    .line 464
    iget-object v4, v3, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->timecard_notes:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v3, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->timecard_notes:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x0

    if-eqz v0, :cond_5

    .line 467
    iget-object v4, v3, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v4, v4, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 468
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 469
    :cond_3
    iget-object v4, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/ui/timecards/R$string;->timecard_no_job_title:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 471
    :cond_4
    iget-object v5, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 473
    :cond_5
    new-instance v5, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;

    iget-object v3, v3, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->timecard_notes:Ljava/lang/String;

    invoke-direct {v5, v4, v3}, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 476
    :cond_6
    new-instance v0, Lcom/squareup/ui/timecards/ViewNotesScreen$Data;

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/ViewNotesScreen$Data;-><init>(Ljava/util/List;)V

    return-object v0
.end method
