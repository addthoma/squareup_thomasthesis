.class public Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "StartBreakOrClockOutCoordinator.java"


# instance fields
.field private clockOutButton:Lcom/squareup/noho/NohoButton;

.field private clockedInGreenIndicator:Landroid/view/View;

.field private startBreakButton:Lcom/squareup/noho/NohoButton;

.field private startBreakOrClockOutLinearLayout:Landroid/widget/LinearLayout;

.field private switchJobsButton:Lcom/squareup/noho/NohoButton;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method private getHeader(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_1

    .line 76
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_header_job:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "job_title"

    .line 81
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 77
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_header:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 32
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 33
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakOrClockOutCoordinator$FQ0MCKEwrkxZWzw9LHhQB2bIQEY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakOrClockOutCoordinator$FQ0MCKEwrkxZWzw9LHhQB2bIQEY;-><init>(Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->startBreakButton:Lcom/squareup/noho/NohoButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakOrClockOutCoordinator$2huOwhvZNpslu0-jhuAQRb13n-I;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakOrClockOutCoordinator$2huOwhvZNpslu0-jhuAQRb13n-I;-><init>(Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->clockOutButton:Lcom/squareup/noho/NohoButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakOrClockOutCoordinator$rgZoPH1xy3KPiLuMmW0d7bS4ong;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakOrClockOutCoordinator$rgZoPH1xy3KPiLuMmW0d7bS4ong;-><init>(Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->switchJobsButton:Lcom/squareup/noho/NohoButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakOrClockOutCoordinator$ZhBLEQjZDT67TdFsM2rAk5YaUqk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakOrClockOutCoordinator$ZhBLEQjZDT67TdFsM2rAk5YaUqk;-><init>(Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 86
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 87
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clocked_in_green_indicator:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->clockedInGreenIndicator:Landroid/view/View;

    .line 88
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_start_break_or_clockout:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->startBreakOrClockOutLinearLayout:Landroid/widget/LinearLayout;

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->startBreakOrClockOutLinearLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 90
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_start_break_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->startBreakButton:Lcom/squareup/noho/NohoButton;

    .line 91
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clockout_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->clockOutButton:Lcom/squareup/noho/NohoButton;

    .line 92
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_switch_jobs_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->switchJobsButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_START_BREAK_OR_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$StartBreakOrClockOutCoordinator()Lrx/Subscription;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->startBreakOrClockOutScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 34
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$7vQYVFA_v1UB-7_3q5Nu5e8TUa0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$7vQYVFA_v1UB-7_3q5Nu5e8TUa0;-><init>(Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;)V

    .line 35
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Pzo7aF3pQPcWBpTVqT6xMppHu7Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Pzo7aF3pQPcWBpTVqT6xMppHu7Q;-><init>(Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$dTPO6qVgoiFjnsqP7QX0DtBbRBs;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$dTPO6qVgoiFjnsqP7QX0DtBbRBs;-><init>(Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;)V

    .line 36
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$StartBreakOrClockOutCoordinator(Landroid/view/View;)V
    .locals 0

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onTakeBreakClicked()V

    return-void
.end method

.method public synthetic lambda$attach$2$StartBreakOrClockOutCoordinator(Landroid/view/View;)V
    .locals 0

    .line 38
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onClockOutClicked()V

    return-void
.end method

.method public synthetic lambda$attach$3$StartBreakOrClockOutCoordinator(Landroid/view/View;)V
    .locals 0

    .line 39
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onSwitchJobsClicked()V

    return-void
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 4

    .line 55
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 56
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;

    .line 59
    iget-object v1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->clockedInGreenIndicator:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 60
    iget-object v1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    iget-object v3, v0, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;->jobTitle:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 63
    sget v1, Lcom/squareup/ui/timecards/R$id;->start_break_or_clockout_notes_button:I

    invoke-virtual {p0, p1, v1}, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->showOrHideNotesButton(Lcom/squareup/ui/timecards/TimecardsScreenData;I)V

    .line 65
    iget-object p1, v0, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;->hoursMinutesWorkedToday:Lcom/squareup/ui/timecards/HoursMinutes;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->setTimeWorkedTodayInSubHeader(Lcom/squareup/ui/timecards/HoursMinutes;)V

    .line 67
    iget-boolean p1, v0, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;->showStartBreakButton:Z

    if-eqz p1, :cond_0

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->startBreakButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 70
    :cond_0
    iget-boolean p1, v0, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;->showSwitchJobsButton:Z

    if-eqz p1, :cond_1

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;->switchJobsButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    :cond_1
    return-void
.end method
