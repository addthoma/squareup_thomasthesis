.class public Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;
.super Landroid/widget/LinearLayout;
.source "TimecardsSuccessButtonsView.java"


# instance fields
.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private buttonSeparator:Landroid/view/View;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private primaryButton:Lcom/squareup/marketfont/MarketButton;

.field private secondaryButton:Lcom/squareup/marketfont/MarketButton;

.field timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/timecards/TimecardsScope$Component;->inject(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;)V

    .line 27
    sget p2, Lcom/squareup/ui/timecards/R$layout;->timecards_success_buttons_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 43
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_button_separator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->buttonSeparator:Landroid/view/View;

    .line 44
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 45
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_secondary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsSuccessButtonsView$vkYk-nRFA_VocFPcNncqAwBwANA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsSuccessButtonsView$vkYk-nRFA_VocFPcNncqAwBwANA;-><init>(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private updateView()V
    .locals 3

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTabletLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    .line 52
    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->setVisibility(I)V

    goto :goto_0

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->buttonSeparator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 56
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_success_continue_button_text:I

    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$bindViews$0$TimecardsSuccessButtonsView(Landroid/view/View;)V
    .locals 0

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->finish()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->bindViews()V

    .line 33
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->updateView()V

    return-void
.end method

.method public showSecondaryButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
