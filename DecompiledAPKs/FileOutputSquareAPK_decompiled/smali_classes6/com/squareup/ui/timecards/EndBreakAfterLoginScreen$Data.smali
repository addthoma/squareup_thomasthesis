.class public final Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;
.super Lcom/squareup/ui/timecards/TimecardsScreenData;
.source "EndBreakAfterLoginScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field public final endAuthorizationNeeded:Z

.field public final expectedBreakEndTimeMillis:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/lang/Long;Z)V
    .locals 9

    move-object v8, p0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    .line 27
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/timecards/TimecardsScreenData;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    move-object/from16 v0, p7

    .line 29
    iput-object v0, v8, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->expectedBreakEndTimeMillis:Ljava/lang/Long;

    move/from16 v0, p8

    .line 30
    iput-boolean v0, v8, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->endAuthorizationNeeded:Z

    return-void
.end method
