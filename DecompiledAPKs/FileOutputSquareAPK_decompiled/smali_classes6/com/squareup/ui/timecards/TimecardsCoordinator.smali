.class public abstract Lcom/squareup/ui/timecards/TimecardsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TimecardsCoordinator.java"


# static fields
.field protected static final MOBILE_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;


# instance fields
.field protected analytics:Lcom/squareup/analytics/Analytics;

.field protected final currentTime:Lcom/squareup/time/CurrentTime;

.field protected currentTimeView:Lcom/squareup/marketfont/MarketTextView;

.field private errorContainer:Landroid/view/View;

.field private errorMessage:Landroid/widget/TextView;

.field private errorTitle:Landroid/widget/TextView;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field protected final mainScheduler:Lrx/Scheduler;

.field private progressBar:Landroid/widget/LinearLayout;

.field private progressBarTitle:Lcom/squareup/marketfont/MarketTextView;

.field protected res:Lcom/squareup/util/Res;

.field private timecardContainer:Landroid/widget/LinearLayout;

.field protected timecardHeader:Lcom/squareup/marketfont/MarketTextView;

.field protected timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

.field protected timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

.field protected final timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

.field protected view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 55
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    sput-object v0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->MOBILE_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    .line 59
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    .line 61
    iput-object p2, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    .line 62
    iput-object p3, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 63
    iput-object p4, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 64
    iput-object p5, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private getErrorViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 115
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_DEFAULT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method private setEmployeeName(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 3

    .line 284
    iget-object v0, p1, Lcom/squareup/ui/timecards/TimecardsScreenData;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->getConfig()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object v1

    .line 286
    invoke-virtual {v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->buildUpon()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/ui/timecards/TimecardsScreenData;->employeeName:Ljava/lang/String;

    .line 287
    invoke-virtual {v1, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonText(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object p1

    .line 288
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object p1

    .line 285
    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    goto :goto_0

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->getConfig()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object v1

    .line 291
    invoke-virtual {v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->buildUpon()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 292
    invoke-virtual {v1, v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setTitleTextVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/ui/timecards/TimecardsScreenData;->employeeName:Ljava/lang/String;

    .line 293
    invoke-virtual {v1, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setTitleTextPrimary(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object p1

    .line 294
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object p1

    .line 290
    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    :goto_0
    return-void
.end method

.method private setUpButtonGlyph(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 263
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->getConfig()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object v0

    .line 264
    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->buildUpon()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 265
    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/containerconstants/R$string;->content_description_back:I

    .line 267
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 266
    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonContentDescription(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$MOYUUrKzSMg0Y34APo3KcSKJTiw;

    invoke-direct {v2, v1}, Lcom/squareup/ui/timecards/-$$Lambda$MOYUUrKzSMg0Y34APo3KcSKJTiw;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 268
    invoke-virtual {v0, v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonCommand(Ljava/lang/Runnable;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object v0

    .line 263
    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    .line 270
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->view:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$MOYUUrKzSMg0Y34APo3KcSKJTiw;

    invoke-direct {v1, v0}, Lcom/squareup/ui/timecards/-$$Lambda$MOYUUrKzSMg0Y34APo3KcSKJTiw;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 272
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->getConfig()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->buildUpon()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 274
    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/containerconstants/R$string;->content_description_close:I

    .line 276
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 275
    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonContentDescription(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$RVpyFHjuJ4BLxci8KmFEvMvgEg0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/timecards/-$$Lambda$RVpyFHjuJ4BLxci8KmFEvMvgEg0;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 277
    invoke-virtual {v0, v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonCommand(Ljava/lang/Runnable;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object v0

    .line 272
    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    .line 279
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->view:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$RVpyFHjuJ4BLxci8KmFEvMvgEg0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/timecards/-$$Lambda$RVpyFHjuJ4BLxci8KmFEvMvgEg0;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private showError(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->errorContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->errorTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->errorMessage:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    new-instance p2, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    invoke-direct {p2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;-><init>()V

    .line 246
    invoke-virtual {p2, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setTitleTextVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object p2

    .line 247
    invoke-virtual {p2, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setPrimaryButtonVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object p2

    .line 248
    invoke-virtual {p2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object p2

    .line 245
    invoke-virtual {p1, p2}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    .line 249
    invoke-direct {p0, v1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->setUpButtonGlyph(Z)V

    if-eqz p3, :cond_0

    .line 251
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {p2}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object p2

    invoke-virtual {p2}, Lorg/threeten/bp/ZonedDateTime;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 253
    invoke-interface {p3}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object p3

    invoke-virtual {p3}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide v0

    .line 252
    invoke-static {p2, v0, v1}, Lcom/squareup/ui/timecards/TimeFormatter;->getTitleTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 256
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 257
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->progressBar:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method private showNoInternetConnectionError(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 3

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_NO_INTERNET:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_message:I

    .line 208
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 207
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showError(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->view:Landroid/view/View;

    .line 69
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 70
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$RVpyFHjuJ4BLxci8KmFEvMvgEg0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/timecards/-$$Lambda$RVpyFHjuJ4BLxci8KmFEvMvgEg0;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->layoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 1

    .line 119
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    .line 120
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_current_time:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    .line 121
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_header:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    .line 122
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_sub_header:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    .line 123
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardContainer:Landroid/widget/LinearLayout;

    .line 124
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->progressBar:Landroid/widget/LinearLayout;

    .line 125
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_progress_bar_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->progressBarTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 126
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_error:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->errorContainer:Landroid/view/View;

    .line 127
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_error_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->errorTitle:Landroid/widget/TextView;

    .line 128
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_error_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->errorMessage:Landroid/widget/TextView;

    return-void
.end method

.method protected abstract getErrorMessage()Ljava/lang/String;
.end method

.method protected abstract getErrorTitle()Ljava/lang/String;
.end method

.method protected getProgressBarTitle()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method protected abstract getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
.end method

.method protected isCardScreen()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic lambda$showOrHideNotesButton$0$TimecardsCoordinator(Landroid/view/View;)V
    .locals 0

    .line 180
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onAddNotesClicked()V

    return-void
.end method

.method public synthetic lambda$showOrHideNotesButton$1$TimecardsCoordinator(Landroid/view/View;)V
    .locals 0

    .line 185
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onEditNotesClicked()V

    return-void
.end method

.method protected setTimeWorkedTodayInSubHeader(Lcom/squareup/ui/timecards/HoursMinutes;)V
    .locals 2

    if-nez p1, :cond_0

    .line 222
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void

    .line 228
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/HoursMinutes;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_clock_in_confirmation_sub_header:I

    .line 230
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_shift_summary:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    .line 233
    invoke-static {v1, p1}, Lcom/squareup/ui/timecards/TimeFormatter;->getFormattedTimePeriod(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/HoursMinutes;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "hrs_mins"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 234
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 236
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method protected showBackArrowOnSuccess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected showData(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 2

    .line 77
    iget-object v0, p1, Lcom/squareup/ui/timecards/TimecardsScreenData;->internetState:Lcom/squareup/connectivity/InternetState;

    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq v0, v1, :cond_0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showNoInternetConnectionError(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    return-void

    .line 81
    :cond_0
    sget-object v0, Lcom/squareup/ui/timecards/TimecardsCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$Timecards$TimecardRequestState:[I

    iget-object v1, p1, Lcom/squareup/ui/timecards/TimecardsScreenData;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v1}, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 89
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showError(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    goto :goto_0

    .line 86
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showProgressBar(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    :goto_0
    return-void
.end method

.method protected showError(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->getErrorViewEvent()Lcom/squareup/analytics/RegisterViewName;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 214
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->getErrorTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showError(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    return-void
.end method

.method protected showGeneralError(Ljava/lang/Throwable;)V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_DEFAULT:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Timecards error"

    .line 199
    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 201
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    .line 202
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 201
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showError(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    return-void
.end method

.method protected showOrHideNotesButton(Lcom/squareup/ui/timecards/TimecardsScreenData;I)V
    .locals 3

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->view:Landroid/view/View;

    invoke-static {v0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    .line 174
    iget-object p1, p1, Lcom/squareup/ui/timecards/TimecardsScreenData;->addOrEditNotesButtonConfig:Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->showAddOrEditNotesButton()Z

    move-result v0

    const/16 v1, 0x8

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 177
    sget-object v0, Lcom/squareup/ui/timecards/TimecardsCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$ADD_OR_EDIT_NOTES_BUTTON_CONFIG:[I

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 189
    invoke-virtual {p2, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto :goto_0

    .line 184
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->timecard_edit_notes:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    new-instance p1, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsCoordinator$XRCy-5D7qyMDj_aCFOE6op2_-5Y;

    invoke-direct {p1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsCoordinator$XRCy-5D7qyMDj_aCFOE6op2_-5Y;-><init>(Lcom/squareup/ui/timecards/TimecardsCoordinator;)V

    invoke-static {p1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    invoke-virtual {p2, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto :goto_0

    .line 179
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->timecard_add_notes:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    new-instance p1, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsCoordinator$LyRbIce0g7YBJQssXAVw9AyQ2nA;

    invoke-direct {p1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsCoordinator$LyRbIce0g7YBJQssXAVw9AyQ2nA;-><init>(Lcom/squareup/ui/timecards/TimecardsCoordinator;)V

    invoke-static {p1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    invoke-virtual {p2, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto :goto_0

    .line 192
    :cond_2
    invoke-virtual {p2, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method protected showProgressBar()V
    .locals 1

    const/4 v0, 0x0

    .line 132
    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showProgressBar(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    return-void
.end method

.method protected showProgressBar(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 4

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_LOADING:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->progressBar:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->progressBarTitle:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->getProgressBarTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    new-instance v2, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;-><init>()V

    .line 140
    invoke-virtual {v2, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setActionBarVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v2

    .line 141
    invoke-virtual {v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object v2

    .line 139
    invoke-virtual {v0, v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    const/16 v0, 0x8

    if-eqz p1, :cond_0

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZonedDateTime;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 145
    invoke-interface {v2}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide v2

    .line 144
    invoke-static {v1, v2, v3}, Lcom/squareup/ui/timecards/TimeFormatter;->getTitleTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 147
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, ""

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 152
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 153
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->errorContainer:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 4

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    new-instance v1, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    .line 159
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showBackArrowOnSuccess()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->setUpButtonGlyph(Z)V

    .line 160
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->setEmployeeName(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZonedDateTime;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 163
    invoke-interface {v2}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide v2

    .line 162
    invoke-static {v1, v2, v3}, Lcom/squareup/ui/timecards/TimeFormatter;->getTitleTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 166
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->timecardContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 167
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->progressBar:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 168
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsCoordinator;->errorContainer:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
