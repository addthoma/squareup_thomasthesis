.class public Lcom/squareup/ui/widgets/GlyphRadioRow;
.super Landroid/widget/LinearLayout;
.source "GlyphRadioRow.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private button:Landroid/widget/CompoundButton;

.field private description:Landroid/widget/TextView;

.field private glyph:Lcom/squareup/glyph/SquareGlyphView;

.field private name:Lcom/squareup/widgets/ShorteningTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->GlyphRadioRow:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 32
    sget v0, Lcom/squareup/widgets/pos/R$styleable;->GlyphRadioRow_android_text:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 33
    sget v1, Lcom/squareup/widgets/pos/R$styleable;->GlyphRadioRow_shortText:I

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 35
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 37
    sget p2, Lcom/squareup/widgets/pos/R$layout;->glyph_radio_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/widgets/GlyphRadioRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/widgets/GlyphRadioRow;->bindViews()V

    .line 40
    iget-object p1, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    iget-object p1, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/widgets/GlyphRadioRow;->isClickable()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 44
    new-instance p1, Lcom/squareup/ui/widgets/GlyphRadioRow$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/widgets/GlyphRadioRow$1;-><init>(Lcom/squareup/ui/widgets/GlyphRadioRow;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/widgets/GlyphRadioRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/widgets/GlyphRadioRow;)Landroid/widget/CompoundButton;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->button:Landroid/widget/CompoundButton;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 89
    sget v0, Lcom/squareup/widgets/pos/R$id;->button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->button:Landroid/widget/CompoundButton;

    .line 90
    sget v0, Lcom/squareup/widgets/pos/R$id;->description:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->description:Landroid/widget/TextView;

    .line 91
    sget v0, Lcom/squareup/widgets/pos/R$id;->glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 92
    sget v0, Lcom/squareup/widgets/pos/R$id;->name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ShorteningTextView;

    iput-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method public setDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->description:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->description:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setGlyphColor(I)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    return-void
.end method

.method public setNameColor(I)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setTextColor(I)V

    return-void
.end method

.method public setRadioBackgroundRes(I)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/widgets/GlyphRadioRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setBackgroundResource(I)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/widgets/GlyphRadioRow;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/ui/widgets/GlyphRadioRow;->setChecked(Z)V

    return-void
.end method
