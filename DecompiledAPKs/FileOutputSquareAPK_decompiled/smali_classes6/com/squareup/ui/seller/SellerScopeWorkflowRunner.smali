.class public final Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "SellerScopeWorkflowRunner.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowProps;",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 \u00112\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u0011B\u001f\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowProps;",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowOutput;",
        "workflow",
        "Lcom/squareup/ui/seller/SellerScopeWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "viewFactory",
        "Lcom/squareup/ui/seller/SellerScopeViewFactory;",
        "(Lcom/squareup/ui/seller/SellerScopeWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/seller/SellerScopeViewFactory;)V",
        "getWorkflow",
        "()Lcom/squareup/ui/seller/SellerScopeWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "Companion",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/ui/seller/SellerScopeWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->Companion:Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;

    .line 31
    const-class v0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SellerScopeWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/seller/SellerScopeWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/seller/SellerScopeViewFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    const-class v0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v0, "SellerScopeWorkflowRunner::class.java.name"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-interface {p2}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 20
    move-object v4, p3

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 17
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->workflow:Lcom/squareup/ui/seller/SellerScopeWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;)V
    .locals 0

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getProps$p(Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;)Lcom/squareup/ui/seller/SellerScopeWorkflowProps;
    .locals 0

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->getProps()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/seller/SellerScopeWorkflowProps;

    return-object p0
.end method

.method public static final synthetic access$setProps$p(Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;Lcom/squareup/ui/seller/SellerScopeWorkflowProps;)V
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->setProps(Ljava/lang/Object;)V

    return-void
.end method

.method public static final startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/ui/seller/SellerScopeWorkflowProps;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->Companion:Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/ui/seller/SellerScopeWorkflowProps;)V

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/ui/seller/SellerScopeWorkflow;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->workflow:Lcom/squareup/ui/seller/SellerScopeWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->getWorkflow()Lcom/squareup/ui/seller/SellerScopeWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method
