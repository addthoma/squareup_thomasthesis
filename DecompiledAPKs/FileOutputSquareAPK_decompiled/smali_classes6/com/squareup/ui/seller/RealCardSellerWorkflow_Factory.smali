.class public final Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealCardSellerWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/seller/RealCardSellerWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeInputTypeTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final tipDeterminerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p2, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p3, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p4, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p5, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p6, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p7, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p8, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p9, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->swipeInputTypeTrackerProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p10, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p11, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p12, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p13, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;"
        }
    .end annotation

    .line 97
    new-instance v14, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/seller/RealCardSellerWorkflow;
    .locals 15

    .line 107
    new-instance v14, Lcom/squareup/ui/seller/RealCardSellerWorkflow;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/seller/RealCardSellerWorkflow;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/settings/server/Features;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/ui/seller/RealCardSellerWorkflow;
    .locals 14

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/analytics/StoreAndForwardAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/giftcard/GiftCards;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->swipeInputTypeTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/payment/TipDeterminerFactory;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/onboarding/OnboardingDiverter;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/api/ApiTransactionState;

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->newInstance(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/seller/RealCardSellerWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/seller/RealCardSellerWorkflow_Factory;->get()Lcom/squareup/ui/seller/RealCardSellerWorkflow;

    move-result-object v0

    return-object v0
.end method
