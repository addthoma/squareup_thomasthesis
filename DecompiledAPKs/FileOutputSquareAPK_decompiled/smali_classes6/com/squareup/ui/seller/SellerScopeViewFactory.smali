.class public final Lcom/squareup/ui/seller/SellerScopeViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "SellerScopeViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/seller/SellerScopeViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "separatedPrintoutsViewFactory",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
        "(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;)V",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "separatedPrintoutsViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 9
    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
