.class public final enum Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;
.super Ljava/lang/Enum;
.source "SellerScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/seller/SellerScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClearOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

.field public static final enum CLEAR_CART:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

.field public static final enum CLEAR_NON_LOCKED_ITEMS:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 401
    new-instance v0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    const/4 v1, 0x0

    const-string v2, "CLEAR_CART"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->CLEAR_CART:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    new-instance v0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    const/4 v2, 0x1

    const-string v3, "CLEAR_NON_LOCKED_ITEMS"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->CLEAR_NON_LOCKED_ITEMS:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    .line 400
    sget-object v3, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->CLEAR_CART:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->CLEAR_NON_LOCKED_ITEMS:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->$VALUES:[Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 400
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;
    .locals 1

    .line 400
    const-class v0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;
    .locals 1

    .line 400
    sget-object v0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->$VALUES:[Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    invoke-virtual {v0}, [Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    return-object v0
.end method
