.class public final Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;
.super Ljava/lang/Object;
.source "SwipeChipCardsSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final r12HasConnectedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final swipeChipCardsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/SwipeChipCardsSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/SwipeChipCardsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;->r12HasConnectedProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;->swipeChipCardsSettingsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/SwipeChipCardsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/server/SwipeChipCardsSettings;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;->r12HasConnectedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;->swipeChipCardsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/SwipeChipCardsSettings;

    iget-object v2, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;->newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection_Factory;->get()Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;

    move-result-object v0

    return-object v0
.end method
