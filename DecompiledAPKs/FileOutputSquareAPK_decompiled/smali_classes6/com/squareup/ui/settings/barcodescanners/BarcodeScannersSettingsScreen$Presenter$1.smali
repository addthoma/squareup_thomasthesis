.class Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter$1;
.super Ljava/lang/Object;
.source "BarcodeScannersSettingsScreen.java"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/barcodescanners/BarcodeScannerTracker;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public barcodeScannerConnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 0

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->access$000(Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;)V

    return-void
.end method

.method public barcodeScannerDisconnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 0

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->access$000(Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;)V

    return-void
.end method
