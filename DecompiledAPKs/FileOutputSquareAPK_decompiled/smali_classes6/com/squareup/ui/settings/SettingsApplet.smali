.class public Lcom/squareup/ui/settings/SettingsApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "SettingsApplet.java"


# static fields
.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "SETTINGS"


# instance fields
.field private final bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

.field private final depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

.field private final settingsBadge:Lcom/squareup/ui/settings/SettingsBadge;

.field private final settingsEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;


# direct methods
.method constructor <init>(Ldagger/Lazy;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/SettingsBadge;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/settings/SettingsAppletEntryPoint;",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            "Lcom/squareup/ui/settings/SettingsBadge;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;Lcom/squareup/applet/AppletEntryPoint;)V

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsApplet;->settingsEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/settings/SettingsApplet;->bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    .line 56
    iput-object p4, p0, Lcom/squareup/ui/settings/SettingsApplet;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/settings/SettingsApplet;->settingsBadge:Lcom/squareup/ui/settings/SettingsBadge;

    return-void
.end method

.method private getHistoryFactory(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/applet/AppletSection;)Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    .line 126
    new-instance v0, Lcom/squareup/ui/settings/SettingsApplet$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/SettingsApplet$1;-><init>(Lcom/squareup/ui/settings/SettingsApplet;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/applet/AppletSection;)V

    return-object v0
.end method

.method static isEnteringApplet(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 2

    .line 94
    instance-of v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;

    const/4 v1, 0x0

    if-nez v0, :cond_3

    instance-of v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpScreen;

    if-nez v0, :cond_3

    instance-of v0, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 99
    :cond_0
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope;

    invoke-static {p0, p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isEnteringScope(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 102
    instance-of p1, p0, Lcom/squareup/address/StatePickerScreen;

    const/4 v0, 0x1

    if-nez p1, :cond_1

    instance-of p0, p0, Lcom/squareup/register/widgets/WarningDialogScreen;

    if-eqz p0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    xor-int/lit8 p0, v1, 0x1

    return p0

    :cond_3
    :goto_0
    return v1
.end method

.method private resetHistory(Lcom/squareup/ui/main/HistoryFactory;)V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsApplet;->container:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-interface {v0, p1, v1}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method


# virtual methods
.method public activateBankAccountSettings()V
    .locals 2

    .line 110
    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsApplet;->bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/settings/SettingsApplet;->getHistoryFactory(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/applet/AppletSection;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/SettingsApplet;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;)V

    return-void
.end method

.method public activateDepositsSettings()V
    .locals 2

    .line 114
    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsApplet;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/settings/SettingsApplet;->getHistoryFactory(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/applet/AppletSection;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/SettingsApplet;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;)V

    return-void
.end method

.method public badge()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsApplet;->settingsBadge:Lcom/squareup/ui/settings/SettingsBadge;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsBadge;->count()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/-$$Lambda$JyM0djAmbcEiFah4MVar-ihOwZ0;->INSTANCE:Lcom/squareup/ui/settings/-$$Lambda$JyM0djAmbcEiFah4MVar-ihOwZ0;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    const-string v0, "settings"

    return-object v0
.end method

.method public getEntryPoint(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/applet/AppletEntryPoint;
    .locals 0

    .line 82
    invoke-static {p1, p2}, Lcom/squareup/ui/settings/SettingsApplet;->isEnteringApplet(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsApplet;->settingsEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 69
    sget-object p1, Lcom/squareup/ui/settings/SettingsSectionsScreen;->INSTANCE:Lcom/squareup/ui/settings/SettingsSectionsScreen;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getInitialDetailScreen()Lflow/path/Path;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsApplet;->settingsEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "SETTINGS"

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 65
    sget v0, Lcom/squareup/settingsapplet/R$string;->titlecase_settings:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
