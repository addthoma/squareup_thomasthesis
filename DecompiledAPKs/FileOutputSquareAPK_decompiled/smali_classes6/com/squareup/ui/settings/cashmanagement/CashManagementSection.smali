.class public Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;
.super Lcom/squareup/applet/AppletSection;
.source "CashManagementSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$Access;,
        Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    sget v0, Lcom/squareup/settings/cashmanagement/R$string;->cash_management:I

    sput v0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$Access;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$Access;-><init>(Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;

    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletScope;->INSTANCE:Lcom/squareup/ui/settings/SettingsAppletScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method
