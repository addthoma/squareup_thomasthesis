.class public interface abstract Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;
.super Ljava/lang/Object;
.source "EmailCollectionSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract emailCollectionEnabled()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract enableEmailCollectionToggleClicked()V
.end method
