.class public final Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;
.super Ljava/lang/Object;
.source "CustomerManagementSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/crm/CustomerManagementSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/crm/CustomerManagementSection;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/crm/CustomerManagementSection;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/crm/CustomerManagementSection;-><init>(Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/crm/CustomerManagementSection;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;->newInstance(Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/crm/CustomerManagementSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSection_Factory;->get()Lcom/squareup/ui/settings/crm/CustomerManagementSection;

    move-result-object v0

    return-object v0
.end method
