.class public final Lcom/squareup/ui/settings/crm/CustomerManagementSection$Access;
.super Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.source "CustomerManagementSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/crm/CustomerManagementSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 55
    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSection$Access;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSection$Access;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSection$Access;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 62
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
