.class public Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "ShareTeamPasscodeDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Factory;,
        Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;,
        Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;",
            ">;"
        }
    .end annotation
.end field

.field private static final DAYS_UNTIL_EXPIRATION:I = 0x1e


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$ShareTeamPasscodeDialogScreen$yrgyR830P4jUsk59-SJw6PaAE_0;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$ShareTeamPasscodeDialogScreen$yrgyR830P4jUsk59-SJw6PaAE_0;

    .line 57
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;
    .locals 0

    .line 57
    new-instance p0, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;-><init>()V

    return-object p0
.end method
