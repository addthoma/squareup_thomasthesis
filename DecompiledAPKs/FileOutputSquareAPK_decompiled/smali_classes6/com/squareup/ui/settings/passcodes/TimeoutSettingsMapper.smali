.class public Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;
.super Ljava/lang/Object;
.source "TimeoutSettingsMapper.java"


# static fields
.field private static settingToDisplayStringIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/permissions/PasscodesSettings$Timeout;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static settingToViewIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/permissions/PasscodesSettings$Timeout;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToViewIdMap:Ljava/util/Map;

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToDisplayStringIdMap:Ljava/util/Map;

    .line 13
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToViewIdMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->NEVER:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget v2, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_timeout_never_radio:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToViewIdMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->THIRTY_SECONDS:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget v2, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_timeout_30_seconds_radio:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToViewIdMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->ONE_MINUTE:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget v2, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_timeout_1_minute_radio:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToViewIdMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->FIVE_MINUTES:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget v2, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_timeout_5_minutes_radio:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToDisplayStringIdMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->NEVER:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_after_timeout_never:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToDisplayStringIdMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->THIRTY_SECONDS:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_after_timeout_30_seconds:I

    .line 20
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 19
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToDisplayStringIdMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->ONE_MINUTE:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_after_timeout_1_minute:I

    .line 22
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 21
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToDisplayStringIdMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->FIVE_MINUTES:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_after_timeout_5_minutes:I

    .line 24
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 23
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDisplayStringId(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Ljava/lang/Integer;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToDisplayStringIdMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    return-object p0
.end method

.method public static getTimeout(I)Lcom/squareup/permissions/PasscodesSettings$Timeout;
    .locals 4

    .line 36
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToViewIdMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 37
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/permissions/PasscodesSettings$Timeout;

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getViewId(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Ljava/lang/Integer;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->settingToViewIdMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    return-object p0
.end method
