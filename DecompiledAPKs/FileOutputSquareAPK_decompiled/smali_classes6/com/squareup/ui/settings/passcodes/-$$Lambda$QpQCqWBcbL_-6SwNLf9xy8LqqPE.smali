.class public final synthetic Lcom/squareup/ui/settings/passcodes/-$$Lambda$QpQCqWBcbL_-6SwNLf9xy8LqqPE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/permissions/PasscodesSettings;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/permissions/PasscodesSettings;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$QpQCqWBcbL_-6SwNLf9xy8LqqPE;->f$0:Lcom/squareup/permissions/PasscodesSettings;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$QpQCqWBcbL_-6SwNLf9xy8LqqPE;->f$0:Lcom/squareup/permissions/PasscodesSettings;

    check-cast p1, Lcom/squareup/permissions/Employee;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->setAccountOwnerEmployee(Lcom/squareup/permissions/Employee;)V

    return-void
.end method
