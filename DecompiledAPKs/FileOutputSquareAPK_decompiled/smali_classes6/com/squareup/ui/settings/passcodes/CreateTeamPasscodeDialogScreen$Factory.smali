.class public Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Factory;
.super Ljava/lang/Object;
.source "CreateTeamPasscodeDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lflow/Flow;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 44
    sget-object p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen;

    invoke-virtual {p0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 35
    const-class v0, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Component;

    invoke-interface {v0}, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Component;->res()Lcom/squareup/util/Res;

    move-result-object v0

    .line 36
    const-class v1, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Component;

    invoke-interface {v1}, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Component;->flow()Lflow/Flow;

    move-result-object v1

    .line 38
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_dialog_title:I

    .line 39
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_dialog_message:I

    .line 40
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 41
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 40
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_dialog_positive_button_text:I

    .line 42
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateTeamPasscodeDialogScreen$Factory$YpLxUsOJAp4Ffj5S3r6SHkUITQM;

    invoke-direct {v3, v1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateTeamPasscodeDialogScreen$Factory$YpLxUsOJAp4Ffj5S3r6SHkUITQM;-><init>(Lflow/Flow;)V

    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 45
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 47
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_dialog_negative_button_text:I

    .line 49
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 38
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
