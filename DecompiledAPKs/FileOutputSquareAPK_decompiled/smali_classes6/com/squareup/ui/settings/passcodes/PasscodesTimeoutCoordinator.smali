.class public Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "PasscodesTimeoutCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private flow:Lflow/Flow;

.field private mainScheduler:Lio/reactivex/Scheduler;

.field private scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

.field private timeoutGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Lflow/Flow;Lio/reactivex/Scheduler;)V
    .locals 0
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->flow:Lflow/Flow;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 53
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcode_settings_timeout_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 54
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcode_settings_timeout_radio_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->timeoutGroup:Lcom/squareup/noho/NohoCheckableGroup;

    return-void
.end method

.method private getActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 5

    .line 65
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesTimeoutCoordinator$3HQA3EKQU-zlafql2wp0lmXw_Ec;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesTimeoutCoordinator$3HQA3EKQU-zlafql2wp0lmXw_Ec;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;)V

    .line 66
    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_timeout_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 70
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/settingsapplet/R$string;->passcode_settings_timeout_save_button:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    new-instance v3, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesTimeoutCoordinator$_uwGSgTpMgNcrS0NUQWRrOB9ol8;

    invoke-direct {v3, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesTimeoutCoordinator$_uwGSgTpMgNcrS0NUQWRrOB9ol8;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;)V

    const/4 v4, 0x1

    .line 71
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic lambda$z_Dc7fnaX2BZFbzZ9Hjp0lDOtHg(Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->showScreenData(Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen$Data;)V

    return-void
.end method

.method private showScreenData(Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen$Data;)V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->getActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->timeoutGroup:Lcom/squareup/noho/NohoCheckableGroup;

    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen$Data;->timeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    invoke-static {p1}, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->getViewId(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->view:Landroid/view/View;

    .line 43
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->bindViews(Landroid/view/View;)V

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesTimeoutCoordinator$uJjhB012UbmHdJqG_ktzONTuZEs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesTimeoutCoordinator$uJjhB012UbmHdJqG_ktzONTuZEs;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$PasscodesTimeoutCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->timeoutScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesTimeoutCoordinator$z_Dc7fnaX2BZFbzZ9Hjp0lDOtHg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesTimeoutCoordinator$z_Dc7fnaX2BZFbzZ9Hjp0lDOtHg;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;)V

    .line 49
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$getActionBarConfig$1$PasscodesTimeoutCoordinator()Lkotlin/Unit;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 68
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$getActionBarConfig$2$PasscodesTimeoutCoordinator()Lkotlin/Unit;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->timeoutGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 75
    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableGroup;->getCheckedId()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->getTimeout(I)Lcom/squareup/permissions/PasscodesSettings$Timeout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onTimeoutChanged(Lcom/squareup/permissions/PasscodesSettings$Timeout;)V

    goto :goto_0

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid passcode settings timeout option selected - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->timeoutGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 80
    invoke-virtual {v1}, Lcom/squareup/noho/NohoCheckableGroup;->getCheckedId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 79
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 83
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method
