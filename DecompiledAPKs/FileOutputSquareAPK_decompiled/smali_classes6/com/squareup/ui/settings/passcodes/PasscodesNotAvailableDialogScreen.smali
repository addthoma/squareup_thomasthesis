.class public Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "PasscodesNotAvailableDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$Factory;,
        Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$Component;,
        Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;

    .line 51
    sget-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;

    .line 52
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method
