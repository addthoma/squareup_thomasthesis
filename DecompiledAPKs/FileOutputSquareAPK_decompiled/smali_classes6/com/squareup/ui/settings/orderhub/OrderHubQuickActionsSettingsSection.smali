.class public final Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;
.super Lcom/squareup/applet/AppletSection;
.source "OrderHubQuickActionsSettingsSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;,
        Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;,
        Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u000c2\u00020\u0001:\u0003\u000b\u000c\rB\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;",
        "Lcom/squareup/applet/AppletSection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "device",
        "Lcom/squareup/util/Device;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "getInitialScreen",
        "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsScreen;",
        "Access",
        "Companion",
        "ListEntry",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Companion;

.field public static final TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;->Companion:Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Companion;

    .line 56
    sget v0, Lcom/squareup/settingsapplet/R$string;->orderhub_quick_actions_settings_section_label:I

    sput v0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;)V

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    .line 24
    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;->getInitialScreen()Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsScreen;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsScreen;->Companion:Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsScreen;

    move-result-object v0

    return-object v0
.end method
