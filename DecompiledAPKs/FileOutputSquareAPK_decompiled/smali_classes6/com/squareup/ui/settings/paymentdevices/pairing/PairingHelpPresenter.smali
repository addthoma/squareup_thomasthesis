.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;
.super Lmortar/ViewPresenter;
.source "PairingHelpPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->res:Lcom/squareup/util/Res;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public devicesClicked()V
    .locals 3

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->check_compatibility_url:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 37
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->pairing_help:I

    .line 39
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 38
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 40
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public supportClicked()V
    .locals 3

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->pairing_fallback_help_url:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public videoClicked()V
    .locals 3

    .line 55
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 57
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getVideoUrlSettings()Lcom/squareup/settings/server/VideoUrlSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/VideoUrlSettings;->getR12GettingStartedVideoYoutubeId()Ljava/lang/String;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->launchYouTubeMovie(Ljava/lang/String;)V

    return-void
.end method
