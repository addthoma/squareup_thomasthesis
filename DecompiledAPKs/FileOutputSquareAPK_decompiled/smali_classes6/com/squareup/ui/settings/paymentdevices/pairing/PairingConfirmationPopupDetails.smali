.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;
.super Ljava/lang/Object;
.source "PairingConfirmationPopupDetails.java"

# interfaces
.implements Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final content:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;->title:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;->content:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getContent(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 0

    .line 22
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;->content:Ljava/lang/String;

    return-object p1
.end method

.method public getLayoutResId()I
    .locals 1

    .line 34
    sget v0, Lcom/squareup/common/tutorial/R$layout;->tutorial_dialog:I

    return v0
.end method

.method public getPrimaryButton()I
    .locals 1

    .line 26
    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    return v0
.end method

.method public getSecondaryButton()I
    .locals 1

    .line 30
    sget v0, Lcom/squareup/cardreader/ui/R$string;->pairing_confirmation_wrong_reader:I

    return v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 0

    .line 18
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;->title:Ljava/lang/String;

    return-object p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 42
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;->content:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
