.class public Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;
.super Ljava/lang/Object;
.source "CardReaderMessages.java"


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private getNameSerialNumMessage(Ljava/lang/String;Lcom/squareup/cardreader/WirelessConnection;I)Ljava/lang/String;
    .locals 0

    .line 167
    invoke-interface {p2}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/LastFourDigits;->getLastDigitsAsSerialNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameSerialNumMessage(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getNameSerialNumMessage(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    const-string v0, ""

    if-nez p1, :cond_0

    move-object p1, v0

    :cond_0
    if-nez p2, :cond_1

    move-object p2, v0

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    const-string v0, "name"

    .line 175
    invoke-virtual {p3, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string p3, "serial_number"

    .line 176
    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 178
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getNameString(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;
    .locals 3

    .line 183
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 185
    sget p1, Lcom/squareup/cardreader/R$string;->smart_reader_contactless_and_chip_name:I

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 188
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown reader type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getAcceptedPaymentTypes(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/CharSequence;
    .locals 3

    .line 93
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const-string p1, "T2"

    return-object p1

    .line 106
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown reader type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_accepts_legacy:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 97
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_accepts_r6_value:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 95
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_accepts_r12_value:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAlreadyBondedMessage(Lcom/squareup/cardreader/WirelessConnection;)Ljava/lang/String;
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->smart_reader_contactless_and_chip_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 138
    sget v1, Lcom/squareup/cardreader/R$string;->ble_pairing_already_bonded_message:I

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameSerialNumMessage(Ljava/lang/String;Lcom/squareup/cardreader/WirelessConnection;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getCardReaderConnectionType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/CharSequence;
    .locals 3

    .line 111
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const-string p1, "Built-in"

    return-object p1

    .line 123
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown reader type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_connection_headset:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 113
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_connection_bluetooth:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getCardReaderRowName(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Ljava/lang/CharSequence;
    .locals 3

    .line 25
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 50
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown ReaderType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string p1, "T2"

    return-object p1

    .line 45
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->square_reader_magstripe:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 38
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->square_reader_chip:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 27
    :pswitch_3
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->square_reader_contactless_chip:I

    .line 28
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    .line 30
    :goto_0
    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    if-nez v1, :cond_1

    return-object v0

    .line 33
    :cond_1
    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    sget v1, Lcom/squareup/cardreader/R$string;->pairing_list_row_text:I

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameSerialNumMessage(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getCardReaderStatusDescription(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)Ljava/lang/CharSequence;
    .locals 3

    .line 74
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 88
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown card reader state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_status_ready_value:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 84
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_status_connecting_value:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 82
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_status_permission_needed:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 80
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_status_failed_value:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 78
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_status_updating_value:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 76
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->reader_detail_status_unavailable_value:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getConfirmationContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->pairing_confirmation_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 160
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/LastFourDigits;->getLastDigitsAsSerialNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "serial_number"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 161
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 162
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getConfirmationTitle(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 155
    sget v0, Lcom/squareup/cardreader/R$string;->pairing_confirmation_title:I

    invoke-direct {p0, p2, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameSerialNumMessage(Ljava/lang/String;Lcom/squareup/cardreader/WirelessConnection;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getDefaultCardReaderName(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;
    .locals 3

    .line 56
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const-string p1, "T2"

    return-object p1

    .line 69
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown ReaderType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->square_reader_magstripe:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 60
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->square_reader_chip:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 58
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/R$string;->square_reader_bluetooth:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getPairingFailureMessage(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/WirelessConnection;)Ljava/lang/String;
    .locals 3

    .line 144
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameString(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;

    move-result-object v0

    .line 145
    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 147
    sget p1, Lcom/squareup/cardreader/R$string;->ble_pairing_failed_message:I

    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameSerialNumMessage(Ljava/lang/String;Lcom/squareup/cardreader/WirelessConnection;I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 150
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown reader type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public getProgressTitle(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/WirelessConnection;)Ljava/lang/String;
    .locals 1

    .line 132
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameString(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/R$string;->pairing_progress_title:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameSerialNumMessage(Ljava/lang/String;Lcom/squareup/cardreader/WirelessConnection;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getScanResultRowTitle(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 128
    sget v0, Lcom/squareup/cardreader/R$string;->pairing_list_row_text:I

    invoke-direct {p0, p2, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getNameSerialNumMessage(Ljava/lang/String;Lcom/squareup/cardreader/WirelessConnection;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
