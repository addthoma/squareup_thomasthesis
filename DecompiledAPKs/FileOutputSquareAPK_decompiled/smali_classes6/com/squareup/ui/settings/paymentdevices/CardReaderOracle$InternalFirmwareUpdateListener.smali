.class Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;
.super Ljava/lang/Object;
.source "CardReaderOracle.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalFirmwareUpdateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$1;)V
    .locals 0

    .line 356
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    return-void
.end method


# virtual methods
.method public onFirmwareManifestServerResponseFailure(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$400(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 369
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$500(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onFirmwareManifestServerResponseSuccess(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$400(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 364
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$500(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onFirmwareUpdateAborted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    .line 411
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 1

    .line 399
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$700(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->hasRecentRebootAssetCompleted(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 400
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_IS_FWUP_REBOOTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    goto :goto_0

    .line 404
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    :goto_0
    return-void
.end method

.method public onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;ZLcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 0

    if-nez p2, :cond_0

    .line 417
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 420
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object p3, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_FWUP_ERROR:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {p2, p1, p3}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    :goto_0
    return-void
.end method

.method public onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;ZIIZ)V
    .locals 0

    if-nez p2, :cond_0

    return-void

    .line 387
    :cond_0
    sget-object p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-eqz p5, :cond_1

    .line 389
    sget-object p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_AND_REBOOTING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 392
    :cond_1
    iget-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p5, p1, p2, p4, p3}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$600(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;II)V

    return-void
.end method

.method public onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 1

    if-nez p2, :cond_0

    return-void

    .line 379
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    return-void
.end method

.method public onReaderFailedToConnectAfterRebootingFwup(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 426
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onSendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$400(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 359
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$500(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
