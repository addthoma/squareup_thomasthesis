.class public Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;
.super Ljava/lang/Object;
.source "MovieLauncher.java"


# static fields
.field private static final YOUTUBE_URI_TEMPLATE:Ljava/lang/String; = "vnd.youtube:"


# instance fields
.field private final context:Landroid/content/Context;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/util/Res;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->context:Landroid/content/Context;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method getYouTubeIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .line 44
    new-instance v0, Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "vnd.youtube:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 45
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->youtube_url_template:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "video_id"

    .line 49
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 51
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 52
    new-instance v0, Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-direct {v0, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method public launchYouTubeMovie(Ljava/lang/String;)V
    .locals 3

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->getYouTubeIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "vnd.youtube:"

    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->context:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->youtube_url_template:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "video_id"

    .line 31
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 33
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->context:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
