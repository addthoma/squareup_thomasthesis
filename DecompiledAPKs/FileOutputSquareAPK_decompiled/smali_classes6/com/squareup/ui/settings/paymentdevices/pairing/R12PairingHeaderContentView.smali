.class public Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingHeaderContentView;
.super Landroid/widget/LinearLayout;
.source "R12PairingHeaderContentView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .line 18
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 19
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_screen_reader_r12:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    return-void
.end method
