.class Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CardReaderDetailCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->identifyReader()V

    return-void
.end method
