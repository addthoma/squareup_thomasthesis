.class public final synthetic Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->values()[Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->SUCCESS:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/CountryCode;->values()[Lcom/squareup/CountryCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    return-void
.end method
