.class public final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;
.super Landroid/widget/LinearLayout;
.source "HardwarePrinterSelectView.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;,
        Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHardwarePrinterSelectView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HardwarePrinterSelectView.kt\ncom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,222:1\n49#2:223\n50#2,3:229\n53#2:244\n599#3,4:224\n601#3:228\n310#4,6:232\n355#4,6:238\n1360#5:245\n1429#5,3:246\n310#5,7:249\n*E\n*S KotlinDebug\n*F\n+ 1 HardwarePrinterSelectView.kt\ncom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView\n*L\n114#1:223\n114#1,3:229\n114#1:244\n114#1,4:224\n114#1:228\n114#1,6:232\n114#1,6:238\n189#1:245\n189#1,3:246\n201#1,7:249\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000c\u0018\u00002\u00020\u00012\u00020\u0002:\u0002?@B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J \u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\r2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u0018H\u0002J\u0018\u0010+\u001a\u00020&2\u0006\u0010(\u001a\u00020,2\u0006\u0010*\u001a\u00020$H\u0002J\u000e\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u001aH\u0002J\u0012\u0010.\u001a\u00020&2\u0008\u0010/\u001a\u0004\u0018\u000100H\u0002J\u001c\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u0002030\u0017H\u0002J\u0008\u00104\u001a\u000205H\u0016J\u0008\u00106\u001a\u00020&H\u0014J\u0008\u00107\u001a\u00020&H\u0014J\u001b\u00108\u001a\u00020&2\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u0002030\u0017H\u0000\u00a2\u0006\u0002\u00089J\u0010\u0010:\u001a\u00020&2\u0006\u0010;\u001a\u000205H\u0002J\u0008\u0010<\u001a\u00020&H\u0002J\r\u0010=\u001a\u00020&H\u0000\u00a2\u0006\u0002\u0008>R\u0014\u0010\u0008\u001a\u00020\t8@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0010\u001a\u00020\u00118\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u001b\u001a\u00020\u001c8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u000e\u0010!\u001a\u00020\"X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;",
        "Landroid/widget/LinearLayout;",
        "Lcom/squareup/workflow/ui/HandlesBack;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "getActionBar$settings_applet_release",
        "()Lcom/squareup/marin/widgets/MarinActionBar;",
        "checkedRowPosition",
        "",
        "emptyView",
        "Lcom/squareup/ui/EmptyView;",
        "presenter",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;",
        "getPresenter$settings_applet_release",
        "()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;",
        "setPresenter$settings_applet_release",
        "(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;)V",
        "printerRowList",
        "",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "getRecyclerFactory$settings_applet_release",
        "()Lcom/squareup/recycler/RecyclerFactory;",
        "setRecyclerFactory$settings_applet_release",
        "(Lcom/squareup/recycler/RecyclerFactory;)V",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "testPrintButtonRow",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;",
        "bindPrinterRow",
        "",
        "pos",
        "view",
        "Lcom/squareup/ui/RadioButtonListRow;",
        "item",
        "bindTestPrintButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "createRecycler",
        "findAndSetCheckedRowPosition",
        "hardwarePrinterId",
        "",
        "getPrinterRows",
        "hardwarePrinterIdList",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;",
        "onBackPressed",
        "",
        "onDetachedFromWindow",
        "onFinishInflate",
        "refresh",
        "refresh$settings_applet_release",
        "showEmptyView",
        "isVisible",
        "updatePrintButton",
        "updatePrinterList",
        "updatePrinterList$settings_applet_release",
        "PrinterRow",
        "TestPrintButtonRow",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private checkedRowPosition:I

.field private emptyView:Lcom/squareup/ui/EmptyView;

.field public presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private printerRowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
            ">;"
        }
    .end annotation
.end field

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
            ">;"
        }
    .end annotation
.end field

.field public recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private testPrintButtonRow:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, -0x1

    .line 42
    iput p2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->checkedRowPosition:I

    .line 47
    const-class p2, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Component;

    .line 48
    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Component;->inject(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V

    return-void
.end method

.method public static final synthetic access$bindPrinterRow(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;ILcom/squareup/ui/RadioButtonListRow;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->bindPrinterRow(ILcom/squareup/ui/RadioButtonListRow;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;)V

    return-void
.end method

.method public static final synthetic access$bindTestPrintButton(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/marketfont/MarketButton;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->bindTestPrintButton(Lcom/squareup/marketfont/MarketButton;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;)V

    return-void
.end method

.method public static final synthetic access$getCheckedRowPosition$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)I
    .locals 0

    .line 27
    iget p0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->checkedRowPosition:I

    return p0
.end method

.method public static final synthetic access$getRecycler$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)Lcom/squareup/cycler/Recycler;
    .locals 1

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p0, :cond_0

    const-string v0, "recycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setCheckedRowPosition$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;I)V
    .locals 0

    .line 27
    iput p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->checkedRowPosition:I

    return-void
.end method

.method public static final synthetic access$setRecycler$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/cycler/Recycler;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    return-void
.end method

.method private final bindPrinterRow(ILcom/squareup/ui/RadioButtonListRow;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;)V
    .locals 4

    .line 147
    invoke-virtual {p3}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;->getStationName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;->getAppliedStations()Ljava/lang/String;

    move-result-object v1

    .line 148
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    if-nez v2, :cond_0

    const-string v3, "presenter"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p3}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isPrinterAtPositionSelected$settings_applet_release(Ljava/lang/String;)Z

    move-result v2

    .line 146
    invoke-virtual {p2, v0, v1, v2}, Lcom/squareup/ui/RadioButtonListRow;->showItem(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 151
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;-><init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;Lcom/squareup/ui/RadioButtonListRow;I)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/RadioButtonListRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final bindTestPrintButton(Lcom/squareup/marketfont/MarketButton;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;)V
    .locals 2

    .line 171
    invoke-virtual {p2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    const-string v1, "presenter"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isPrinterSelected$settings_applet_release()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isSelectedPrinterConnected$settings_applet_release()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 175
    :cond_2
    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_test_print_printer_disconnected:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    goto :goto_1

    .line 173
    :cond_3
    :goto_0
    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_test_print:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 178
    :goto_1
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindTestPrintButton$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindTestPrintButton$1;-><init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final createRecycler()Lcom/squareup/cycler/Recycler;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
            ">;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    if-nez v0, :cond_0

    const-string v1, "recyclerFactory"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v1, :cond_1

    const-string v2, "recyclerView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 223
    :cond_1
    sget-object v2, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 224
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 225
    new-instance v2, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v2}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 229
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 230
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 115
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$1;->INSTANCE:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 118
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$2;->INSTANCE:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$2;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->compareItemsContent(Lkotlin/jvm/functions/Function2;)V

    .line 233
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$$special$$inlined$row$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 122
    new-instance v3, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 233
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 232
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 239
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$$special$$inlined$extraItem$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 129
    new-instance v3, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v3, p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2;-><init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 239
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 238
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 227
    invoke-virtual {v2, v1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    return-object v0

    .line 224
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final findAndSetCheckedRowPosition(Ljava/lang/String;)V
    .locals 3

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->printerRowList:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "printerRowList"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    .line 250
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 251
    check-cast v2, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

    .line 201
    invoke-virtual {v2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    .line 255
    :goto_1
    iput v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->checkedRowPosition:I

    return-void
.end method

.method private final getPrinterRows(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
            ">;"
        }
    .end annotation

    .line 189
    check-cast p1, Ljava/lang/Iterable;

    .line 245
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 246
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 247
    check-cast v1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;

    .line 189
    new-instance v2, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;->getStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;->getAppliedStations()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 248
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 190
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    .line 192
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

    .line 193
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/settingsapplet/R$string;->printer_stations_no_hardware_printer:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resources.getString(R.st\u2026ions_no_hardware_printer)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    sget-object v2, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    const-string v3, ""

    .line 192
    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method private final showEmptyView(Z)V
    .locals 4

    const-string v0, "emptyView"

    const/16 v1, 0x8

    const-string v2, "recyclerView"

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    .line 206
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 207
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v3}, Lcom/squareup/ui/EmptyView;->setVisibility(I)V

    goto :goto_0

    .line 209
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, v3}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 210
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1, v1}, Lcom/squareup/ui/EmptyView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final updatePrintButton()V
    .locals 3

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    const-string v1, "recycler"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/cycler/Recycler;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->refresh(I)V

    return-void
.end method


# virtual methods
.method public final getActionBar$settings_applet_release()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 2

    .line 33
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getPresenter$settings_applet_release()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getRecyclerFactory$settings_applet_release()Lcom/squareup/recycler/RecyclerFactory;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    if-nez v0, :cond_0

    const-string v1, "recyclerFactory"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public onBackPressed()Z
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->commit$settings_applet_release()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->dropView(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V

    .line 78
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 52
    sget v0, Lcom/squareup/settingsapplet/R$id;->hardware_printer_list_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->createRecycler()Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    .line 55
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    const-string v2, "presenter"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isPrintTestEnabled$settings_applet_release()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;-><init>(Z)V

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->testPrintButtonRow:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_1

    const-string v1, "recycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->testPrintButtonRow:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;

    if-nez v1, :cond_2

    const-string v3, "testPrintButtonRow"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->setExtraItem(Ljava/lang/Object;)V

    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 59
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->empty_view:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/EmptyView;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->emptyView:Lcom/squareup/ui/EmptyView;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->emptyView:Lcom/squareup/ui/EmptyView;

    const-string v1, "emptyView"

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 62
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 63
    sget v4, Lcom/squareup/settingsapplet/R$string;->printer_stations_no_hardware_printers_available_title:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 61
    invoke-virtual {v0, v3}, Lcom/squareup/ui/EmptyView;->setTitle(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 66
    :cond_5
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 67
    sget v3, Lcom/squareup/settingsapplet/R$string;->printer_stations_no_hardware_printers_available_text:I

    const-string v4, "link"

    invoke-virtual {v1, v3, v4}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 68
    sget v3, Lcom/squareup/registerlib/R$string;->printer_stations_set_up_printers_url:I

    invoke-virtual {v1, v3}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 69
    sget v3, Lcom/squareup/cardreader/ui/R$string;->support_center:I

    invoke-virtual {v1, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setMessage(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    if-nez v0, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public final refresh$settings_applet_release(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;",
            ">;)V"
        }
    .end annotation

    const-string v0, "hardwarePrinterIdList"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->showEmptyView(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 92
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->showEmptyView(Z)V

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getPrinterRows(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->printerRowList:Ljava/util/List;

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    if-nez p1, :cond_1

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getSelectedHardwarePrinterId$settings_applet_release()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->findAndSetCheckedRowPosition(Ljava/lang/String;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_2

    const-string v0, "recycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->printerRowList:Ljava/util/List;

    if-nez v0, :cond_3

    const-string v1, "printerRowList"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->updatePrinterList$settings_applet_release()V

    return-void
.end method

.method public final setPresenter$settings_applet_release(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    return-void
.end method

.method public final setRecyclerFactory$settings_applet_release(Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method

.method public final updatePrinterList$settings_applet_release()V
    .locals 4

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->testPrintButtonRow:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;

    if-nez v0, :cond_0

    const-string v1, "testPrintButtonRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    if-nez v1, :cond_1

    const-string v2, "presenter"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isPrintTestEnabled$settings_applet_release()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;->setEnabled(Z)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    const-string v1, "recycler"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v3, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3}, Lcom/squareup/cycler/Recycler;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cycler/Recycler;->refresh(II)V

    .line 107
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->updatePrintButton()V

    return-void
.end method
