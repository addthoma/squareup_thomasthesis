.class public Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;
.source "PrinterStationsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final printerStations:Lcom/squareup/print/PrinterStations;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/print/PrinterStations;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    sget-object v5, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->HARDWARE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;-><init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 8

    .line 42
    sget v2, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;->TITLE_ID:I

    sget v5, Lcom/squareup/settingsapplet/R$string;->peripheral_configured_one:I

    sget v6, Lcom/squareup/settingsapplet/R$string;->peripheral_configured_many:I

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;Lcom/squareup/util/Device;IILcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    .line 44
    iput-object p4, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;->printerStations:Lcom/squareup/print/PrinterStations;

    return-void
.end method

.method static synthetic lambda$connectedCount$0(Lcom/squareup/print/PrinterStation;)Ljava/lang/Boolean;
    .locals 0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/print/PrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object p0

    iget-boolean p0, p0, Lcom/squareup/print/PrinterStationConfiguration;->internal:Z

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected connectedCount()I
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 49
    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->getAllStations()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->from(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/printerstations/-$$Lambda$PrinterStationsSection$ListEntry$l_4bOjNhz7PEWTPHHs0i206aTQw;->INSTANCE:Lcom/squareup/ui/settings/printerstations/-$$Lambda$PrinterStationsSection$ListEntry$l_4bOjNhz7PEWTPHHs0i206aTQw;

    .line 50
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lrx/Observable;->count()Lrx/Observable;

    move-result-object v0

    .line 48
    invoke-static {v0}, Lcom/squareup/util/rx/RxBlockingSupport;->getValueOrThrow(Lrx/Observable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
