.class final Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;
.super Ljava/lang/Object;
.source "PrinterStationScope.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "PrinterStationState"
.end annotation


# static fields
.field static final PRINTER_STATION_BUILDER_BUNDLER_KEY:Ljava/lang/String; = "PRINTER_STATION_BUNDLER_KEY"

.field static final PRINTER_STATION_BUILDER_STATE_KEY:Ljava/lang/String; = "PRINTER_STATION_STATE_KEY"


# instance fields
.field builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

.field private final builderBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/print/PrinterStationConfiguration$Builder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/BundleKey;Lcom/squareup/print/PrinterStationConfiguration$Builder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/print/PrinterStationConfiguration$Builder;",
            ">;",
            "Lcom/squareup/print/PrinterStationConfiguration$Builder;",
            ")V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builderBundleKey:Lcom/squareup/BundleKey;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    const-string v0, "PRINTER_STATION_STATE_KEY"

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builderBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/PrinterStationConfiguration$Builder;

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builderBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method
