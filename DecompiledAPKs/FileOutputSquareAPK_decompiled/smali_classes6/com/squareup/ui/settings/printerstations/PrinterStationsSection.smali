.class public Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;
.super Lcom/squareup/applet/AppletSection;
.source "PrinterStationsSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_settings_label:I

    sput v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/permissions/Permission;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;->INSTANCE:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;

    return-object v0
.end method
