.class public Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;
.super Landroid/widget/LinearLayout;
.source "PrinterStationDetailView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;
    }
.end annotation


# instance fields
.field private automaticItemizedReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private categoriesList:Landroid/widget/LinearLayout;

.field private categoriesProgress:Landroid/view/View;

.field private contents:Landroid/view/ViewGroup;

.field private hardwarePrinterSelect:Lcom/squareup/ui/account/view/LineRow;

.field private includeOnTicketContainer:Landroid/view/View;

.field private noCategoriesMessage:Landroid/view/View;

.field final optionsTransition:Landroid/animation/LayoutTransition;

.field private paperSignatureOptionsContainer:Landroid/view/View;

.field presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private printATicketForEachItemSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private printCompactTicketsSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private printOrderTicketsMessage:Lcom/squareup/widgets/MessageView;

.field private printReceiptHint:Lcom/squareup/widgets/MessageView;

.field private printReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private printReceiptUnavailable:Lcom/squareup/ui/account/view/LineRow;

.field private printTicketStubSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private printTicketStubUnavailable:Lcom/squareup/ui/account/view/LineRow;

.field private printTicketSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private printerStationName:Lcom/squareup/ui/XableEditText;

.field private removePrinterStationButton:Lcom/squareup/ui/ConfirmButton;

.field private testPrintButton:Lcom/squareup/marketfont/MarketButton;

.field private ticketAutoNameSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private ticketCustomNameSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private ticketNamingOptionsContainer:Landroid/view/View;

.field private topDivider:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 101
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    new-instance p2, Landroid/animation/LayoutTransition;

    invoke-direct {p2}, Landroid/animation/LayoutTransition;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->optionsTransition:Landroid/animation/LayoutTransition;

    .line 102
    const-class p2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Component;->inject(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    return-void
.end method

.method private buildCategoryView(I)Landroid/view/View;
    .locals 3

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getCategories()Ljava/util/List;

    move-result-object v0

    .line 365
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 366
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 367
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    .line 369
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isPrintCategoryIdEnabled(Ljava/lang/String;)Z

    move-result v1

    new-instance v2, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$THFngkZzbUCil7dJ1mxKM9U-KUk;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$THFngkZzbUCil7dJ1mxKM9U-KUk;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;Ljava/lang/String;)V

    invoke-direct {p0, p1, v1, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->createAndBindRow(Ljava/lang/String;ZLandroid/widget/CompoundButton$OnCheckedChangeListener;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 372
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$string;->print_uncategorized_items:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    .line 373
    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getPrintsUncategorizedItems()Z

    move-result v0

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$4fraWQ2fcKR8YAO7aJzj3MZj0xk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$4fraWQ2fcKR8YAO7aJzj3MZj0xk;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    .line 372
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->createAndBindRow(Ljava/lang/String;ZLandroid/widget/CompoundButton$OnCheckedChangeListener;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method private createAndBindRow(Ljava/lang/String;ZLandroid/widget/CompoundButton$OnCheckedChangeListener;)Landroid/view/View;
    .locals 3

    .line 380
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->categoriesList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$layout;->printer_detail_category_row:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 383
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    .line 384
    invoke-virtual {v0, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 385
    invoke-virtual {v0, p3}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-object v0
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 358
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getCheckedRoles()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterStation$Role;",
            ">;"
        }
    .end annotation

    .line 196
    const-class v0, Lcom/squareup/print/PrinterStation$Role;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 203
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketStubSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 204
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printerStationName:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hideKeyboard()V
    .locals 0

    .line 239
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method hidePrintCompactTicketsSwitch()V
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printCompactTicketsSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printCompactTicketsSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public synthetic lambda$buildCategoryView$6$PrinterStationDetailView(Ljava/lang/String;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 370
    iget-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {p2, p1, p3}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->setPrintCategoryIdEnabled(Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic lambda$buildCategoryView$7$PrinterStationDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 374
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->setPrintsUncategorizedItems(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$PrinterStationDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 126
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onAutomaticItemizedReceiptChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$PrinterStationDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onPrintATicketForEachItemChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$PrinterStationDetailView()V
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onRemoveButtonClicked()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$3$PrinterStationDetailView()V
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onTestPrintClicked()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$4$PrinterStationDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onRolesChanged()V

    return-void
.end method

.method public synthetic lambda$showPrintCompactTicketsSwitch$5$PrinterStationDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 288
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onPrintCompactTicketsChanged(Z)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 176
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 106
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 107
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 109
    sget v1, Lcom/squareup/settingsapplet/R$id;->content:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->contents:Landroid/view/ViewGroup;

    .line 110
    sget v1, Lcom/squareup/settingsapplet/R$id;->printer_station_name:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    iput-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printerStationName:Lcom/squareup/ui/XableEditText;

    .line 111
    sget v1, Lcom/squareup/settingsapplet/R$id;->print_receipts_switch:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 112
    sget v1, Lcom/squareup/settingsapplet/R$id;->print_receipts_hint:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    iput-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptHint:Lcom/squareup/widgets/MessageView;

    .line 113
    sget v1, Lcom/squareup/settingsapplet/R$id;->print_receipts_unavailable:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/account/view/LineRow;

    iput-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptUnavailable:Lcom/squareup/ui/account/view/LineRow;

    .line 114
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptUnavailable:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/account/view/LineRow;->setValueColor(I)V

    .line 115
    sget v1, Lcom/squareup/settingsapplet/R$id;->print_order_tickets_switch:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 116
    sget v1, Lcom/squareup/settingsapplet/R$id;->print_order_ticket_stubs_unavailable:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/account/view/LineRow;

    iput-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketStubUnavailable:Lcom/squareup/ui/account/view/LineRow;

    .line 117
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketStubUnavailable:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/account/view/LineRow;->setValueColor(I)V

    .line 118
    sget v0, Lcom/squareup/settingsapplet/R$id;->print_order_ticket_stubs_switch:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketStubSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 119
    sget v0, Lcom/squareup/settingsapplet/R$id;->print_order_tickets_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printOrderTicketsMessage:Lcom/squareup/widgets/MessageView;

    .line 120
    sget v0, Lcom/squareup/settingsapplet/R$id;->print_order_tickets_no_categories_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->noCategoriesMessage:Landroid/view/View;

    .line 122
    sget v0, Lcom/squareup/settingsapplet/R$id;->ticket_name_options_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->ticketNamingOptionsContainer:Landroid/view/View;

    .line 123
    sget v0, Lcom/squareup/settingsapplet/R$id;->paper_signature_available_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->paperSignatureOptionsContainer:Landroid/view/View;

    .line 124
    sget v0, Lcom/squareup/settingsapplet/R$id;->automatic_itemized_receipt_switch:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->automaticItemizedReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->automaticItemizedReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$7eb6A84OP0rpCMkfe1o_t_6MW-c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$7eb6A84OP0rpCMkfe1o_t_6MW-c;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 128
    sget v0, Lcom/squareup/settingsapplet/R$id;->order_ticket_custom_name_switch:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->ticketCustomNameSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 129
    sget v0, Lcom/squareup/settingsapplet/R$id;->order_ticket_autonumber_switch:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->ticketAutoNameSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 130
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$1;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    .line 135
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->ticketCustomNameSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->ticketAutoNameSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    sget v0, Lcom/squareup/settingsapplet/R$id;->print_a_ticket_for_each_item_switch:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printATicketForEachItemSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printATicketForEachItemSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$aw4pdL-H8XPAGKI8lp6-je6_aYc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$aw4pdL-H8XPAGKI8lp6-je6_aYc;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 142
    sget v0, Lcom/squareup/settingsapplet/R$id;->print_compact_tickets_switch:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printCompactTicketsSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 144
    sget v0, Lcom/squareup/settingsapplet/R$id;->include_on_ticket_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->includeOnTicketContainer:Landroid/view/View;

    .line 145
    sget v0, Lcom/squareup/settingsapplet/R$id;->categories_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->categoriesProgress:Landroid/view/View;

    .line 146
    sget v0, Lcom/squareup/settingsapplet/R$id;->categories_list_layout:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->categoriesList:Landroid/widget/LinearLayout;

    .line 148
    sget v0, Lcom/squareup/settingsapplet/R$id;->hardware_printer_select:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->hardwarePrinterSelect:Lcom/squareup/ui/account/view/LineRow;

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->hardwarePrinterSelect:Lcom/squareup/ui/account/view/LineRow;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$2;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->hardwarePrinterSelect:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getHardwarePrinterDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 156
    sget v0, Lcom/squareup/settingsapplet/R$id;->remove_printer_station:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->removePrinterStationButton:Lcom/squareup/ui/ConfirmButton;

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->removePrinterStationButton:Lcom/squareup/ui/ConfirmButton;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$TvFpswWCQ-N4j6eN4dN_bjDFF28;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$TvFpswWCQ-N4j6eN4dN_bjDFF28;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 159
    sget v0, Lcom/squareup/settingsapplet/R$id;->test_print:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->testPrintButton:Lcom/squareup/marketfont/MarketButton;

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->testPrintButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$54gzk1hjwKNuSUAY_0LHtk5cwZM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$54gzk1hjwKNuSUAY_0LHtk5cwZM;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    sget v0, Lcom/squareup/settingsapplet/R$id;->printer_station_top_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->topDivider:Landroid/view/View;

    .line 164
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$P0Q7E1pR-7wAzWBC9pmkqevcaGA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$P0Q7E1pR-7wAzWBC9pmkqevcaGA;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    .line 167
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 168
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 169
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketStubSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setAutomaticItemizedReceiptChecked(Z)V
    .locals 1

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->automaticItemizedReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public setAutomaticItemizedReceiptSwitchLabel(I)V
    .locals 2

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->automaticItemizedReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setCategoriesProgressVisible(Z)V
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->categoriesProgress:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setCheckedRoles(ZZZ)V
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 211
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 212
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketStubSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public setIncludeOnTicketVisible(Z)V
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->includeOnTicketContainer:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setIsInternalPrinter(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 336
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printerStationName:Lcom/squareup/ui/XableEditText;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 337
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->hardwarePrinterSelect:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    .line 338
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->topDivider:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 339
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->removePrinterStationButton:Lcom/squareup/ui/ConfirmButton;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method setIsNewPrinterStation(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 345
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->removePrinterStationButton:Lcom/squareup/ui/ConfirmButton;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method setLayoutTransitionEnabled(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->contents:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    if-nez v0, :cond_0

    .line 302
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->contents:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->optionsTransition:Landroid/animation/LayoutTransition;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    .line 304
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->contents:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    :cond_1
    :goto_0
    return-void
.end method

.method setName(Ljava/lang/String;)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printerStationName:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setNameHint(Ljava/lang/String;)V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printerStationName:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method public setNoCategoriesMessageVisible(Z)V
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->noCategoriesMessage:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setPaperSigOptionsVisible(Z)V
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->paperSignatureOptionsContainer:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setPrintATicketForEachItemSwitchChecked(Z)V
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printATicketForEachItemSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public setPrintATicketForEachItemVisible(Z)V
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printATicketForEachItemSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setPrintReceiptHint(I)V
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptHint:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptHint:Lcom/squareup/widgets/MessageView;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setPrintReceiptText(I)V
    .locals 2

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptUnavailable:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setReceiptSwitchEnabled(Z)V
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setReceiptSwitchSupported(Z)V
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printReceiptUnavailable:Lcom/squareup/ui/account/view/LineRow;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setSelectedNamingOption(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;)V
    .locals 4

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->ticketCustomNameSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    sget-object v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->CUSTOM_NAME:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->ticketAutoNameSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    sget-object v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    if-ne p1, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setShowOrderTicketsMessage(Z)V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printOrderTicketsMessage:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setShowTestPrintButton(Z)V
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->testPrintButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setTicketNamingOptionsVisible(Z)V
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->ticketNamingOptionsContainer:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setTicketStubSwitchEnabled(Z)V
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketStubSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setTicketStubSwitchSupported(Z)V
    .locals 1

    .line 314
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setTicketStubSwitchEnabled(Z)V

    .line 315
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketStubUnavailable:Lcom/squareup/ui/account/view/LineRow;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setTicketSwitchEnabled(Z)V
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printTicketSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showPrintCompactTicketsSwitch(Z)V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printCompactTicketsSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printCompactTicketsSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 287
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->printCompactTicketsSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$2F2zWf7iT-elSSw8CFszzWQHnOw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailView$2F2zWf7iT-elSSw8CFszzWQHnOw;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public updateCategoriesList()V
    .locals 7

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->categoriesList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getCategories()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    .line 228
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_0

    .line 229
    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->buildCategoryView(I)Landroid/view/View;

    move-result-object v2

    .line 230
    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->categoriesList:Landroid/widget/LinearLayout;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
