.class Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$4;
.super Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;
.source "EmployeeManagementSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onEmployeeManagementEnabledChanged(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

.field final synthetic val$newSetting:Z


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Lcom/squareup/permissions/Permission;Z)V
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$4;->this$0:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    iput-boolean p3, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$4;->val$newSetting:Z

    invoke-direct {p0, p2}, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;-><init>(Lcom/squareup/permissions/Permission;)V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$4;->this$0:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$4;->val$newSetting:Z

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->access$100(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Z)V

    return-void
.end method
