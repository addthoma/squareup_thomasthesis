.class Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "EmployeeManagementSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 80
    iput-object p2, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 81
    iput-object p3, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 82
    iput-object p4, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 83
    iput-object p5, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 84
    iput-object p6, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 85
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->flow:Lflow/Flow;

    .line 86
    iput-object p7, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onUpdatedGuessModeSuccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Z)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onEmployeeManagementEnabledSuccess(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Z)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onGuestModeSuccess(Z)V

    return-void
.end method

.method private onEmployeeManagementEnabledSuccess(Z)V
    .locals 3

    .line 264
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setPasscodeEmployeeManagementEnabledToggle(Z)V

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    .line 269
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->updateView()V

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    .line 272
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 271
    invoke-static {p1, v1}, Lcom/squareup/permissions/EmployeeManagementEvent;->forPasscodeEmployeeManagementEnabledToggle(ZZ)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private onGuestModeSuccess(Z)V
    .locals 3

    .line 276
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;

    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setGuestModeEnabled(Z)V

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    .line 281
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->updateView()V

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    .line 283
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 282
    invoke-static {p1, v1}, Lcom/squareup/permissions/EmployeeManagementEvent;->forGuestModeToggle(ZZ)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private onUpdatedGuessModeSuccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V
    .locals 3

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setPasscodeAccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    .line 289
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 288
    invoke-static {p1, v1}, Lcom/squareup/permissions/EmployeeManagementEvent;->forPasscodeAccessToggle(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;Z)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 290
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->updateView()V

    return-void
.end method


# virtual methods
.method canSeeLearnMore()Z
    .locals 1

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->canSeePayrollUpsell()Z

    move-result v0

    return v0
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onEmployeeManagementEnabledChanged(Z)V
    .locals 3

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isPasscodeEmployeeManagementEnabled()Z

    move-result v0

    .line 164
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setPasscodeEmployeeManagementEnabledToggle(Z)V

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$4;

    sget-object v2, Lcom/squareup/permissions/Permission;->MANAGE_EMPLOYEES:Lcom/squareup/permissions/Permission;

    invoke-direct {v1, p0, v2, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$4;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Lcom/squareup/permissions/Permission;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->loginEmployee(Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_EMPLOYEES:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$5;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$5;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    :goto_0
    return-void
.end method

.method onGuestModeChanged(Z)V
    .locals 3

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isGuestModeEnabled()Z

    move-result v0

    .line 187
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setGuestModeEnabled(Z)V

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$6;

    sget-object v2, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-direct {v1, p0, v2, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$6;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Lcom/squareup/permissions/Permission;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->loginEmployee(Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$7;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$7;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 90
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->updateView()V

    return-void
.end method

.method onPasscodeAccessToggleChanged(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V
    .locals 3

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getPasscodeAccess()Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    move-result-object v0

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setUpdatedPasscodeOption(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    if-ne v0, p1, :cond_0

    return-void

    .line 144
    :cond_0
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->NO_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-ne p1, v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_EMPLOYEES:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$1;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$2;

    sget-object v2, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-direct {v1, p0, v2, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$2;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Lcom/squareup/permissions/Permission;Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->loginEmployee(Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$3;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter$3;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    :goto_0
    return-void
.end method

.method onPayrollUpsellClicked()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYROLL_LEARN_MORE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->FROM_LEARN_MORE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onTimeTrackingChanged(Z)V
    .locals 3

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->canSeePayrollUpsell()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 110
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->canShowEmployeeManagementSettingsSection()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setTimeTrackingToggle(Z)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->EMPLOYEE_MANAGEMENT_TRACK_TIME_UPSELL:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->FROM_TIME_TRACK_TOGGLE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TIME_TRACKING:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->updateView()V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    .line 122
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 121
    invoke-static {p1, v1}, Lcom/squareup/permissions/EmployeeManagementEvent;->forTimeTrackingToggle(ZZ)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V
    .locals 3

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setTimeout(Lcom/squareup/settings/server/PasscodeTimeout;)V

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->updateView()V

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onUserInteraction()V

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/permissions/EmployeeManagementEvent;->forTimeoutChange(Lcom/squareup/settings/server/PasscodeTimeout;Z)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method onTransactionLockModeChanged(Z)V
    .locals 3

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TRANSACTION_LOCK:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    .line 216
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->updateView()V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    .line 219
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 218
    invoke-static {p1, v1}, Lcom/squareup/permissions/EmployeeManagementEvent;->forTransactionLockModeChange(ZZ)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public saveSettings()V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->saveSettingsToServer()V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 95
    const-class v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;

    return-object v0
.end method

.method updateView()V
    .locals 5

    .line 232
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;

    if-nez v0, :cond_0

    return-void

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTimecardEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setTimeTrackingToggle(Z)V

    .line 238
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setPasscodeEmployeeManagementEnabledToggle(Z)V

    .line 239
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isAllowed()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    .line 240
    invoke-interface {v1, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 239
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setPasscodeEmployeeManagementContainerVisibility(Z)V

    .line 241
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setPasscodeEmployeeManagementSettingsVisibility(Z)V

    .line 242
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isGuestModeEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setGuestModeEnabled(Z)V

    .line 244
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getTimeout()Lcom/squareup/settings/server/PasscodeTimeout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setTimeoutOption(Lcom/squareup/settings/server/PasscodeTimeout;)V

    .line 245
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTransactionLockModeEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setTransactionLockToggle(Z)V

    .line 247
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->canSeeLearnMore()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setLearnMoreVisibility(Z)V

    .line 250
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 251
    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isAllowed()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 250
    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setUpdatedPasscodeEmployeeManagementContainerVisibility(Z)V

    .line 253
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getPasscodeAccess()Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    move-result-object v1

    .line 254
    sget-object v4, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->NO_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-eq v1, v4, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v0, v4}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setUpdatedTimeoutContainerVisibility(Z)V

    .line 255
    sget-object v4, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ALWAYS_REQUIRE_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-ne v1, v4, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v0, v2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setUpdatedTransactionLockToggleVisibilty(Z)V

    .line 256
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setUpdatedPasscodeOption(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    .line 258
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getTimeout()Lcom/squareup/settings/server/PasscodeTimeout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setUpdatedTimeoutOption(Lcom/squareup/settings/server/PasscodeTimeout;)V

    .line 259
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 260
    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTransactionLockModeEnabled()Z

    move-result v1

    .line 259
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->setUpdatedTransactionLockToggle(Z)V

    return-void
.end method
