.class public Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;
.super Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;
.source "OpenTicketsSettingsRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "BasicViewHolder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method public static inflate(Landroid/view/ViewGroup;ILcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    if-eq p1, v0, :cond_3

    const/4 v2, 0x2

    if-eq p1, v2, :cond_2

    const/4 v2, 0x3

    if-eq p1, v2, :cond_1

    const/4 v2, 0x5

    if-eq p1, v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$layout;->open_tickets_settings_create_ticket_group_button:I

    .line 47
    invoke-virtual {p1, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 48
    move-object p1, p0

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->onCreateTicketGroupButtonRowInflated(Lcom/squareup/marketfont/MarketButton;)V

    goto :goto_0

    .line 42
    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget p2, Lcom/squareup/settingsapplet/R$layout;->open_tickets_settings_ticket_group_header:I

    .line 43
    invoke-virtual {p1, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    goto :goto_0

    .line 37
    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$layout;->open_tickets_settings_toggle_predefined_tickets:I

    .line 38
    invoke-virtual {p1, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 39
    invoke-virtual {p2, p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->onPredefinedTicketsEnabledRowInflated(Landroid/view/View;)V

    goto :goto_0

    .line 32
    :cond_3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$layout;->open_tickets_settings_toggle_as_home_screen:I

    .line 33
    invoke-virtual {p1, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 34
    invoke-virtual {p2, p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->onOpenTicketsAsHomeScreenEnabledRowInflated(Landroid/view/View;)V

    goto :goto_0

    .line 27
    :cond_4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$layout;->open_tickets_settings_toggle_open_tickets:I

    .line 28
    invoke-virtual {p1, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 29
    invoke-virtual {p2, p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->onOpenTicketsEnabledRowInflated(Landroid/view/View;)V

    :goto_0
    if-eqz p0, :cond_5

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    .line 50
    :goto_1
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 51
    new-instance p1, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;-><init>(Landroid/view/View;)V

    return-object p1
.end method


# virtual methods
.method public bindData(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/util/Res;)V
    .locals 0

    return-void
.end method

.method public getDragHandle()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
