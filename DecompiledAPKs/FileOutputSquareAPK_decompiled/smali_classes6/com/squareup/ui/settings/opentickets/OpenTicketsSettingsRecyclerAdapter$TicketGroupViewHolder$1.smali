.class Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "OpenTicketsSettingsRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;

.field final synthetic val$presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder$1;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder$1;->val$presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder$1;->val$presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder$1;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;

    iget-object v0, v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->ticketGroupEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->onEditTicketGroupClicked(Ljava/lang/String;)V

    return-void
.end method
