.class public Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "OpenTicketsSettingsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field final blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final flow:Lflow/Flow;

.field private final groupCache:Lcom/squareup/tickets/TicketGroupsCache;

.field private final nextScreen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private queryCallback:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

.field private subs:Lio/reactivex/disposables/CompositeDisposable;

.field private ticketGroupEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field final unsyncedTicketsPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/Tickets;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/cogs/Cogs;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;Lcom/squareup/ui/main/PosContainer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 114
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 105
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    .line 107
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 115
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getBadBus()Lcom/squareup/badbus/BadBus;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    .line 116
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->res:Lcom/squareup/util/Res;

    .line 117
    iput-object p3, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 118
    iput-object p4, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    .line 119
    iput-object p5, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    .line 120
    iput-object p7, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 121
    iput-object p10, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->runner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    .line 122
    new-instance p2, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {p2}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->unsyncedTicketsPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 123
    new-instance p2, Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-direct {p2, p6}, Lcom/squareup/caller/BlockingPopupPresenter;-><init>(Lcom/squareup/thread/executor/MainThread;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    .line 124
    iput-object p8, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 125
    iput-object p9, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 126
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->flow:Lflow/Flow;

    .line 127
    invoke-static {p11}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->nextScreen:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Lcom/squareup/tickets/TicketGroupsCache;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$202(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->queryCallback:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Ljava/util/List;
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->findTicketGroupsWithUpdatedOrdinals()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Ljava/util/List;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->saveTicketGroupOrdinals(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Z
    .locals 0

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->hasView()Z

    move-result p0

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->updateView()V

    return-void
.end method

.method private cancelQueryCallback()V
    .locals 1

    .line 368
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->hasPendingQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->queryCallback:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->cancel()V

    const/4 v0, 0x0

    .line 370
    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->queryCallback:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

    :cond_0
    return-void
.end method

.method private ensureDeviceProfilesDisabled()V
    .locals 1

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isUsingDeviceProfiles()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method

.method private findTicketGroupsWithUpdatedOrdinals()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketGroup;",
            ">;"
        }
    .end annotation

    .line 335
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 336
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 337
    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 338
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getOrdinal()I

    move-result v4

    if-eq v4, v2, :cond_0

    .line 339
    new-instance v4, Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-direct {v4}, Lcom/squareup/api/items/TicketGroup$Builder;-><init>()V

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/api/items/TicketGroup$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v4

    .line 340
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/api/items/TicketGroup$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v4

    .line 341
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method(Lcom/squareup/api/items/TicketGroup$NamingMethod;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v3

    .line 342
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v3

    .line 343
    invoke-virtual {v3}, Lcom/squareup/api/items/TicketGroup$Builder;->build()Lcom/squareup/api/items/TicketGroup;

    move-result-object v3

    .line 339
    invoke-static {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->create(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private hasPendingQuery()Z
    .locals 1

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->queryCallback:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isMobileRegisterEmployee()Z
    .locals 2

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 314
    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isOpenTicketsAsHomeScreenSwitchVisible()Z
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsAllowed()Z

    move-result v0

    return v0
.end method

.method private isPredefinedTicketsSwitchVisible()Z
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsAllowed()Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$Y7xd3yqAPOxHJWhkC-CG3w135Lk(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->onCogsUpdated(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method static synthetic lambda$loadTicketGroupsFromCogs$6(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 1

    .line 361
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 362
    invoke-interface {p0, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 363
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllTicketGroups()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p0

    return-object p0
.end method

.method private onCogsUpdated(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 179
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_TEMPLATE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->isDragging()Z

    move-result p1

    if-nez p1, :cond_0

    .line 181
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->loadTicketGroupsFromCogs()V

    :cond_0
    return-void
.end method

.method private saveTicketGroupOrdinals(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketGroup;",
            ">;)V"
        }
    .end annotation

    .line 326
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->updateTicketEntryOrdinals()V

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$1;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Ljava/util/List;)V

    .line 331
    invoke-static {}, Lcom/squareup/shared/catalog/CatalogTasks;->explodeOnError()Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object p1

    .line 327
    invoke-interface {v0, v1, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private shouldShowTicketGroups()Z
    .locals 1

    .line 304
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isMobileRegisterEmployee()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private updateTicketEntryOrdinals()V
    .locals 7

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 351
    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 352
    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v4

    .line 353
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getTicketTemplateCount()I

    move-result v6

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object v2

    .line 352
    invoke-static {v4, v5, v6, v1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->forTicketGroup(Ljava/lang/String;Ljava/lang/String;IILcom/squareup/api/items/TicketGroup$NamingMethod;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v2

    invoke-interface {v3, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateTicketGroupEntries()V
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->setTicketGroupEntries(Ljava/util/List;)V

    goto :goto_0

    .line 321
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->setTicketGroupEntries(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method private updateView()V
    .locals 4

    .line 289
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

    .line 290
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isOpenTicketsEnabled()Z

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->setEnabledSwitch(ZZ)V

    .line 292
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isOpenTicketsAsHomeScreenSwitchVisible()Z

    move-result v1

    .line 293
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v2

    .line 292
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->setOpenTicketsAsHomeScreenSwitch(ZZ)V

    .line 295
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isPredefinedTicketsSwitchVisible()Z

    move-result v1

    .line 296
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isPredefinedTicketsEnabled()Z

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isPredefinedTicketsSwitchEnabled()Z

    move-result v3

    .line 295
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->setPredefinedTicketsSwitch(ZZZ)V

    .line 298
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->shouldShowTicketGroups()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->setPredefinedTicketGroupVisible(Z)V

    .line 299
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isCreateTicketGroupButtonEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->setCreateTicketGroupButtonEnabled(Z)V

    .line 300
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->updateTicketGroupEntries()V

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method isCreateTicketGroupButtonEnabled()Z
    .locals 1

    .line 285
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->hasPendingQuery()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected isOpenTicketsAsHomeScreenEnabled()Z
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    return v0
.end method

.method protected isOpenTicketsEnabled()Z
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->runner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->isOpenTicketsEnabled()Z

    move-result v0

    return v0
.end method

.method protected isPredefinedTicketsEnabled()Z
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    return v0
.end method

.method isPredefinedTicketsSwitchEnabled()Z
    .locals 1

    .line 279
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->hasPendingQuery()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public synthetic lambda$null$4$OpenTicketsSettingsPresenter(Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 247
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 248
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->findTicketGroupsWithUpdatedOrdinals()Ljava/util/List;

    move-result-object p1

    .line 249
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->saveTicketGroupOrdinals(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$observeDragState$5$OpenTicketsSettingsPresenter(Lio/reactivex/Observable;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 246
    new-instance v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$eaeN_nAx7ofaGz5ijY1QgTfZjBU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$eaeN_nAx7ofaGz5ijY1QgTfZjBU;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnableSwitchChecked$2$OpenTicketsSettingsPresenter()V
    .locals 4

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->unsyncedTicketsPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/opentickets/R$string;->open_tickets_checking:I

    .line 201
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method public synthetic lambda$onEnableSwitchChecked$3$OpenTicketsSettingsPresenter(Lcom/squareup/tickets/TicketsResult;)V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/caller/BlockingPopupPresenter;->dismiss()V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->unsyncedTicketsPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dismiss()V

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->runner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->maybeDisableTickets(I)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$OpenTicketsSettingsPresenter(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 145
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 146
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->updateView()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$OpenTicketsSettingsPresenter(Ljava/lang/Boolean;)V
    .locals 0

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    .line 156
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->updateView()V

    return-void
.end method

.method loadTicketGroupsFromCogs()V
    .locals 3

    .line 358
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->cancelQueryCallback()V

    .line 359
    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$1;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->queryCallback:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    sget-object v1, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$q68cBccf_7uaH0WzHp4k5df017A;->INSTANCE:Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$q68cBccf_7uaH0WzHp4k5df017A;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->queryCallback:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method observeDragState(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 246
    new-instance v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$mN8ynMZB0MAiBvm0P7DOm4btrpM;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$mN8ynMZB0MAiBvm0P7DOm4btrpM;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Lio/reactivex/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onCreateTicketButtonClicked()V
    .locals 3

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->getMaxTicketGroupCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->GROUP_LIMIT_REACHED:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;-><init>(I)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method onEditTicketGroupClicked(Ljava/lang/String;)V
    .locals 3

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, p1, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onEnableSwitchChecked(Z)V
    .locals 2

    .line 187
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ensureDeviceProfilesDisabled()V

    if-eqz p1, :cond_0

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->runner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->setTicketsEnabled(Z)V

    :cond_0
    if-nez p1, :cond_1

    .line 195
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {p1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 198
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$MKtMCC-HktumkauXit4Y1VJJqxw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$MKtMCC-HktumkauXit4Y1VJJqxw;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/caller/BlockingPopupPresenter;->show(Ljava/lang/Runnable;)V

    .line 204
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$0wQwbf84ZPiCbfDq_jbh1l0-PNo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$0wQwbf84ZPiCbfDq_jbh1l0-PNo;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    invoke-interface {p1, v0}, Lcom/squareup/tickets/Tickets;->getUnsyncedTicketCount(Lcom/squareup/tickets/TicketsCallback;)V

    :cond_1
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 139
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$Y7xd3yqAPOxHJWhkC-CG3w135Lk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$Y7xd3yqAPOxHJWhkC-CG3w135Lk;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 142
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->loadTicketGroupsFromCogs()V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->nextScreen:Lio/reactivex/Observable;

    new-instance v2, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$ujo-rOCiHHSPf507mjM74QTbQ_0;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$ujo-rOCiHHSPf507mjM74QTbQ_0;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->runner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsEnabled()Lrx/Observable;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$5gTQ6uO8-PRH5Xvhv_hr7kf1fxU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsPresenter$5gTQ6uO8-PRH5Xvhv_hr7kf1fxU;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    .line 154
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 152
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 162
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->cancelQueryCallback()V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    .line 165
    invoke-super {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onExitScope()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 169
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 170
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->updateView()V

    return-void
.end method

.method onOpenTicketsAsHomeScreenEnabledSwitchToggled(Z)V
    .locals 1

    .line 213
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ensureDeviceProfilesDisabled()V

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/OpenTicketsSettings;->setOpenTicketsAsHomeScreenEnabled(Z)V

    return-void
.end method

.method onPredefinedTicketsEnabledSwitchToggled(Z)V
    .locals 1

    .line 219
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ensureDeviceProfilesDisabled()V

    if-eqz p1, :cond_1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->ticketGroupEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 222
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isMobileRegisterEmployee()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen;

    invoke-direct {p1}, Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen;-><init>()V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;

    .line 225
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/OpenTicketsSettings;->setPredefinedTicketsEnabled(Z)V

    .line 228
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->updateView()V

    :goto_1
    return-void
.end method

.method protected saveSettings()V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 131
    const-class v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;

    return-object v0
.end method
