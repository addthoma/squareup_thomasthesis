.class public interface abstract Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Component;
.super Ljava/lang/Object;
.source "EditTicketGroupScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract editTicketGroup()Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen$Component;
.end method

.method public abstract inject(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;)V
.end method
