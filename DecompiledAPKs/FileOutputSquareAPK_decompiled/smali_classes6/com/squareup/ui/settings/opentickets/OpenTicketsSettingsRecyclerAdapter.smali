.class public Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;
.super Lcom/squareup/ui/ReorderableRecyclerViewAdapter;
.source "OpenTicketsSettingsRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;,
        Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/ReorderableRecyclerViewAdapter<",
        "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        ">;"
    }
.end annotation


# static fields
.field static final OPEN_TICKETS_AS_HOME_SCREEN_SWITCH_ROW:I = 0x1

.field static final OPEN_TICKETS_SWITCH_ROW:I = 0x0

.field static final PREDEFINED_TICKETS_CREATE_GROUP_BUTTON_ROW:I = 0x5

.field static final PREDEFINED_TICKETS_GROUP_HEADER_ROW:I = 0x3

.field static final PREDEFINED_TICKETS_GROUP_ROW:I = 0x4

.field static final PREDEFINED_TICKETS_SWITCH_ROW:I = 0x2


# instance fields
.field private isAsHomeScreenSwitchVisible:Z

.field private isPredefinedTicketsSwitchVisible:Z

.field private isTicketGroupVisible:Z

.field private final openTicketsSettingsView:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

.field private final presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Lcom/squareup/util/Res;)V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;-><init>()V

    .line 129
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->openTicketsSettingsView:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

    .line 130
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    .line 131
    iput-object p3, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private isCreateGroupButtonRow(I)Z
    .locals 2

    .line 234
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isTicketGroupVisible:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isOpenTicketsAsHomeScreenSwitchRow(I)Z
    .locals 2

    .line 211
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isAsHomeScreenSwitchVisible:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isPredefinedTicketsGroupHeaderRow(I)Z
    .locals 3

    .line 221
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isAsHomeScreenSwitchVisible:Z

    const/4 v1, 0x1

    add-int/2addr v0, v1

    .line 222
    iget-boolean v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isPredefinedTicketsSwitchVisible:Z

    add-int/2addr v0, v2

    .line 223
    iget-boolean v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isTicketGroupVisible:Z

    if-eqz v2, :cond_0

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isPredefinedTicketsGroupRow(I)Z
    .locals 3

    .line 228
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isAsHomeScreenSwitchVisible:Z

    const/4 v1, 0x1

    add-int/2addr v0, v1

    .line 229
    iget-boolean v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isPredefinedTicketsSwitchVisible:Z

    add-int/2addr v0, v2

    .line 230
    iget-boolean v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isTicketGroupVisible:Z

    if-eqz v2, :cond_0

    if-le p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->getDraggableItemCount()I

    move-result v2

    add-int/2addr v0, v2

    if-gt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isPredefinedTicketsSwitchRow(I)Z
    .locals 2

    .line 215
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isPredefinedTicketsSwitchVisible:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isAsHomeScreenSwitchVisible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_0
    if-ne p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method public getDraggableItemId(I)J
    .locals 2

    .line 192
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->getDraggableItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemCount()I
    .locals 1

    .line 184
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isTicketGroupVisible:Z

    if-eqz v0, :cond_0

    .line 185
    invoke-super {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getItemCount()I

    move-result v0

    return v0

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->getStaticTopRowsCount()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 156
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isOpenTicketsAsHomeScreenSwitchRow(I)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    return v2

    .line 158
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isPredefinedTicketsSwitchRow(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p1, 0x2

    return p1

    .line 160
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isPredefinedTicketsGroupHeaderRow(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 p1, 0x3

    return p1

    .line 162
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isPredefinedTicketsGroupRow(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 p1, 0x4

    return p1

    .line 164
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isCreateGroupButtonRow(I)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 p1, 0x5

    return p1

    .line 167
    :cond_5
    new-instance v1, Ljava/lang/AssertionError;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v0

    const-string p1, "No view type for position %d"

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public getStaticBottomRowsCount()I
    .locals 1

    .line 180
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isTicketGroupVisible:Z

    return v0
.end method

.method public getStaticTopRowsCount()I
    .locals 2

    .line 173
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isAsHomeScreenSwitchVisible:Z

    add-int/lit8 v0, v0, 0x1

    .line 174
    iget-boolean v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isPredefinedTicketsSwitchVisible:Z

    add-int/2addr v0, v1

    .line 175
    iget-boolean v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isTicketGroupVisible:Z

    add-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;I)V
    .locals 1

    .line 150
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->getDraggableItemAtPosition(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;->bindData(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic onCreateReorderableViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;
    .locals 0

    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->onCreateReorderableViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateReorderableViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;
    .locals 2

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v1, 0x2

    if-eq p2, v1, :cond_2

    const/4 v1, 0x3

    if-eq p2, v1, :cond_2

    const/4 v1, 0x4

    if-eq p2, v1, :cond_1

    const/4 v1, 0x5

    if-ne p2, v1, :cond_0

    goto :goto_0

    .line 145
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v0, v1

    const-string p2, "Unknown view type %d"

    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 143
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-static {p1, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->inflate(Landroid/view/ViewGroup;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;

    move-result-object p1

    return-object p1

    .line 141
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->openTicketsSettingsView:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

    invoke-static {p1, p2, v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;->inflate(Landroid/view/ViewGroup;ILcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public setAsHomeScreenSwitchVisible(Z)V
    .locals 0

    .line 201
    iput-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isAsHomeScreenSwitchVisible:Z

    .line 202
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setPredefinedTicketsSwitchVisible(Z)V
    .locals 0

    .line 206
    iput-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isPredefinedTicketsSwitchVisible:Z

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setTicketGroupsVisible(Z)V
    .locals 0

    .line 196
    iput-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isTicketGroupVisible:Z

    .line 197
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method
