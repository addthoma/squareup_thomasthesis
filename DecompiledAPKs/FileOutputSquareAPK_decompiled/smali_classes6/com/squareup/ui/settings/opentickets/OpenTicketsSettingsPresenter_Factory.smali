.class public final Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;
.super Ljava/lang/Object;
.source "OpenTicketsSettingsPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final coreParametersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final groupCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final sidebarRefresherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->ticketsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->groupCacheProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;"
        }
    .end annotation

    .line 84
    new-instance v12, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/Tickets;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/cogs/Cogs;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;
    .locals 13

    .line 93
    new-instance v12, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/Tickets;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/cogs/Cogs;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;Lcom/squareup/ui/main/PosContainer;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tickets/Tickets;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->groupCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tickets/TicketGroupsCache;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/settings/SidebarRefresher;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/main/PosContainer;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/Tickets;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/cogs/Cogs;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter_Factory;->get()Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    move-result-object v0

    return-object v0
.end method
