.class public final Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;
.super Ljava/lang/Object;
.source "SettingsSectionsPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/SettingsSectionsPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final bankAccountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final bannerButtonResolverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/BannerButtonResolver;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsAppletSectionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsList;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final sidebarRefresherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;"
        }
    .end annotation
.end field

.field private final signOutRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/signout/SignOutRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final starterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/signout/SignOutRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/BannerButtonResolver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->settingsAppletSectionsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->starterProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->badgePresenterProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->deepLinksProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->signOutRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->bannerButtonResolverProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/signout/SignOutRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/BannerButtonResolver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    .line 133
    new-instance v21, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;

    move-object/from16 v0, v21

    invoke-direct/range {v0 .. v20}, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v21
.end method

.method public static newInstance(Lcom/squareup/ui/settings/SettingsAppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Ljavax/inject/Provider;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lcom/squareup/signout/SignOutRunner;Lcom/squareup/onboarding/BannerButtonResolver;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/SettingsSectionsPresenter;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsList;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/applet/BadgePresenter;",
            "Lcom/squareup/ui/main/DeepLinks;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            "Lcom/squareup/applet/AppletSelection;",
            "Lcom/squareup/signout/SignOutRunner;",
            "Lcom/squareup/onboarding/BannerButtonResolver;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/settings/SettingsSectionsPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    .line 145
    new-instance v21, Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    move-object/from16 v0, v21

    invoke-direct/range {v0 .. v20}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;-><init>(Lcom/squareup/ui/settings/SettingsAppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Ljavax/inject/Provider;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lcom/squareup/signout/SignOutRunner;Lcom/squareup/onboarding/BannerButtonResolver;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V

    return-object v21
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/SettingsSectionsPresenter;
    .locals 22

    move-object/from16 v0, p0

    .line 114
    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->settingsAppletSectionsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/settings/SettingsAppletSectionsList;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/settings/SidebarRefresher;

    iget-object v8, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->starterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/onboarding/OnboardingStarter;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/applet/BadgePresenter;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->deepLinksProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/main/DeepLinks;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/applet/AppletSelection;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->signOutRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/signout/SignOutRunner;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->bannerButtonResolverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/onboarding/BannerButtonResolver;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/util/BrowserLauncher;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/settings/server/Features;

    invoke-static/range {v2 .. v21}, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->newInstance(Lcom/squareup/ui/settings/SettingsAppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Ljavax/inject/Provider;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lcom/squareup/signout/SignOutRunner;Lcom/squareup/onboarding/BannerButtonResolver;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter_Factory;->get()Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    move-result-object v0

    return-object v0
.end method
