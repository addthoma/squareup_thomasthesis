.class public Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "LoyaltySettingsScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;->INSTANCE:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;

    .line 32
    sget-object v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;->INSTANCE:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;

    .line 33
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 20
    const-class v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;

    .line 21
    invoke-interface {v0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;->scopeRunner()Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
