.class public Lcom/squareup/ui/settings/tiles/TileAppearanceView;
.super Landroid/widget/LinearLayout;
.source "TileAppearanceView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private final IMAGE_CLICKED:Landroid/view/View$OnClickListener;

.field private final TEXT_CLICKED:Landroid/view/View$OnClickListener;

.field private imageRadioRow:Landroid/view/View;

.field private imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

.field presenter:Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private textRadioRow:Landroid/view/View;

.field private textTile:Lcom/squareup/orderentry/TextTile;

.field private tileChoice:Lcom/squareup/widgets/CheckableGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance p2, Lcom/squareup/ui/settings/tiles/TileAppearanceView$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceView$1;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceView;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->IMAGE_CLICKED:Landroid/view/View$OnClickListener;

    .line 35
    new-instance p2, Lcom/squareup/ui/settings/tiles/TileAppearanceView$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceView$2;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceView;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->TEXT_CLICKED:Landroid/view/View$OnClickListener;

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->isInEditMode()Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 50
    :cond_0
    const-class p2, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Component;->inject(Lcom/squareup/ui/settings/tiles/TileAppearanceView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 95
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->presenter:Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 83
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 56
    sget v0, Lcom/squareup/settingsapplet/R$id;->image_tile:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    iput-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$string;->printer_test_print_item_example:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showAbbreviation()V

    .line 59
    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-static {v0}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v1, v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setName(Ljava/lang/String;)V

    .line 61
    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->disableAbbreviationEditing()V

    .line 62
    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setLabelColor(Ljava/lang/String;)V

    .line 64
    sget v1, Lcom/squareup/settingsapplet/R$id;->text_tile:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/TextTile;

    iput-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->textTile:Lcom/squareup/orderentry/TextTile;

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->textTile:Lcom/squareup/orderentry/TextTile;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    sget v0, Lcom/squareup/settingsapplet/R$id;->tile_choice:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->tileChoice:Lcom/squareup/widgets/CheckableGroup;

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->tileChoice:Lcom/squareup/widgets/CheckableGroup;

    sget v1, Lcom/squareup/settingsapplet/R$id;->text_tile_radio:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->textRadioRow:Landroid/view/View;

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->tileChoice:Lcom/squareup/widgets/CheckableGroup;

    sget v1, Lcom/squareup/settingsapplet/R$id;->image_tile_radio:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageRadioRow:Landroid/view/View;

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->textRadioRow:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->TEXT_CLICKED:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->textTile:Lcom/squareup/orderentry/TextTile;

    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->TEXT_CLICKED:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/TextTile;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageRadioRow:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->IMAGE_CLICKED:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->IMAGE_CLICKED:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->presenter:Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method updateViews(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;)V
    .locals 4

    .line 87
    sget-object v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->TEXT:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 88
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->tileChoice:Lcom/squareup/widgets/CheckableGroup;

    if-eqz p1, :cond_1

    sget v1, Lcom/squareup/settingsapplet/R$id;->text_tile_radio:I

    goto :goto_1

    :cond_1
    sget v1, Lcom/squareup/settingsapplet/R$id;->image_tile_radio:I

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Colors;->toHex(I)Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->imageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    const-string v2, ""

    if-nez p1, :cond_2

    move-object v3, v0

    goto :goto_2

    :cond_2
    move-object v3, v2

    :goto_2
    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setLabelColor(Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->textTile:Lcom/squareup/orderentry/TextTile;

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    move-object v0, v2

    :goto_3
    invoke-virtual {v1, v0}, Lcom/squareup/orderentry/TextTile;->setColor(Ljava/lang/String;)V

    return-void
.end method
