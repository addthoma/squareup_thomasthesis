.class Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$ImageTileDialogCanceled;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "TileAppearanceAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageTileDialogCanceled"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->TILE_APPEARANCE_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    return-void
.end method
