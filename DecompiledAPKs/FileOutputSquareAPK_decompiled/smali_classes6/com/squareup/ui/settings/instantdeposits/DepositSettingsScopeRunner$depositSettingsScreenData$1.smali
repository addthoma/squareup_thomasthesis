.class final Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositSettingsScreenData$1;
.super Ljava/lang/Object;
.source "DepositSettingsScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositSettingsScreenData()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0018\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/util/tuple/Triplet;",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositSettingsScreenData$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Triplet<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;)",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component3()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    .line 106
    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositSettingsScreenData$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    .line 107
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    const-string v3, "settings.instantDepositsSettings"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {v2, v0, v1, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->access$toDepositSettingsScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/settings/server/InstantDepositsSettings;Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/util/tuple/Triplet;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositSettingsScreenData$1;->apply(Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method
