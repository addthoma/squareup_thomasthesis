.class public final Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;
.super Ljava/lang/Object;
.source "DepositSettingsScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$LinkDebitCardWorkflowResultHandler;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ba\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u001a\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002BW\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\u0008\u0010\u001a\u001a\u00020\u001bH\u0002J\n\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0002J\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001fJ\u0018\u0010!\u001a\u0008\u0012\u0004\u0012\u00020#0\"2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u0002J\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0\u001fJ\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020)0\u001fJ\u0006\u0010*\u001a\u00020+J\u0006\u0010,\u001a\u00020+J\u0006\u0010-\u001a\u00020+J\u0006\u0010.\u001a\u00020+J\u000e\u0010/\u001a\u00020+2\u0006\u00100\u001a\u00020\u001bJ\u0006\u00101\u001a\u00020+J\u001c\u00102\u001a\u00020+2\u0008\u0008\u0001\u00103\u001a\u00020\u001b2\u0008\u0008\u0001\u00104\u001a\u00020\u001bH\u0002J\u0006\u00105\u001a\u00020+J\u0006\u00106\u001a\u00020+J\u0008\u00107\u001a\u000208H\u0002J\u0008\u00109\u001a\u000208H\u0002J\u000e\u0010:\u001a\u0008\u0012\u0004\u0012\u00020#0\"H\u0002J\n\u0010;\u001a\u0004\u0018\u00010<H\u0002J\u000e\u0010=\u001a\u0008\u0012\u0004\u0012\u00020#0\"H\u0002J\u000e\u0010>\u001a\u00020+2\u0006\u0010?\u001a\u000208J\u0008\u0010@\u001a\u00020\u001bH\u0002J\u0010\u0010A\u001a\u00020+2\u0006\u0010B\u001a\u00020CH\u0002J\u0010\u0010D\u001a\u00020+2\u0006\u0010B\u001a\u00020CH\u0002J\u0006\u0010E\u001a\u00020+J\u0010\u0010F\u001a\u00020+2\u0006\u0010G\u001a\u00020HH\u0016J\u0008\u0010I\u001a\u00020+H\u0016J\u0008\u0010J\u001a\u00020+H\u0016J\u0006\u0010K\u001a\u00020+J\u0006\u0010L\u001a\u00020+J\u0016\u0010M\u001a\u0008\u0012\u0004\u0012\u00020#0\"2\u0006\u0010N\u001a\u00020OH\u0007J\u000e\u0010P\u001a\u00020+2\u0006\u0010Q\u001a\u000208J\u0006\u0010R\u001a\u00020+J\u0016\u0010R\u001a\u00020+2\u0006\u0010S\u001a\u00020\u001b2\u0006\u0010T\u001a\u00020\u001bJ\u0006\u0010U\u001a\u00020+J\u0006\u0010V\u001a\u00020+J\u000e\u0010W\u001a\u00020+2\u0006\u0010X\u001a\u000208J\u000e\u0010Y\u001a\u00020+2\u0006\u0010Z\u001a\u000208J\u0010\u0010[\u001a\u0002082\u0006\u00100\u001a\u00020\u001bH\u0002J\u0010\u0010\\\u001a\u0002082\u0006\u0010]\u001a\u000208H\u0002J\u0008\u0010^\u001a\u000208H\u0002J\u0008\u0010_\u001a\u000208H\u0002J\u0008\u0010`\u001a\u000208H\u0002J\u0008\u0010a\u001a\u000208H\u0002J\u0010\u0010b\u001a\u0002082\u0006\u0010c\u001a\u000208H\u0002J\u0010\u0010d\u001a\u0002082\u0006\u0010]\u001a\u000208H\u0002J\u0010\u0010e\u001a\u00020 2\u0006\u0010B\u001a\u00020CH\u0002J\u0018\u0010f\u001a\u00020\'2\u0006\u0010N\u001a\u00020O2\u0006\u0010g\u001a\u00020CH\u0002J \u0010h\u001a\u00020)2\u0006\u0010N\u001a\u00020O2\u0006\u0010i\u001a\u00020j2\u0006\u0010g\u001a\u00020CH\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006k"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
        "Lmortar/Scoped;",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$LinkDebitCardWorkflowResultHandler;",
        "flow",
        "Lflow/Flow;",
        "depositScheduleSettings",
        "Lcom/squareup/depositschedule/DepositScheduleSettings;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "rateFormatter",
        "Lcom/squareup/text/RateFormatter;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "sidebarRefresher",
        "Lcom/squareup/ui/settings/SidebarRefresher;",
        "analytics",
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "instantDepositRunner",
        "Lcom/squareup/instantdeposit/InstantDepositRunner;",
        "(Lflow/Flow;Lcom/squareup/depositschedule/DepositScheduleSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/text/RateFormatter;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/util/Clock;Lcom/squareup/instantdeposit/InstantDepositRunner;)V",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "cardDrawable",
        "",
        "cardStatus",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;",
        "closeOfDayDialogScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;",
        "depositScheduleHint",
        "Lcom/squareup/resources/TextModel;",
        "",
        "defaultCutoffTime",
        "Lcom/squareup/protos/common/time/LocalTime;",
        "depositScheduleScreenData",
        "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;",
        "depositSettingsScreenData",
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;",
        "enableAutomaticDeposits",
        "",
        "enableManualDeposits",
        "goBack",
        "goToCloseOfDayDialog",
        "goToDepositScheduleScreen",
        "dayOfWeek",
        "goToDepositSettingsScreen",
        "goToDepositsDialog",
        "title",
        "body",
        "goToLinkDebitCardEntryScreen",
        "goToLinkDebitCardResultScreen",
        "hasActivatedAccount",
        "",
        "hasLinkedCard",
        "instantTransfersHint",
        "linkedCard",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;",
        "linkedCardText",
        "logInstantDepositToggled",
        "checked",
        "manualDepositsEnabledBodyResId",
        "maybeGoToAutomaticDepositsEnabledDialog",
        "state",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "maybeGoToManualDepositsEnabledDialog",
        "onChangeDebitCard",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLinkDebitCardResult",
        "onRefresh",
        "refreshDepositSchedule",
        "sameDayDepositFee",
        "instantDepositsSettings",
        "Lcom/squareup/settings/server/InstantDepositsSettings;",
        "saveSettings",
        "isInstantDepositsChecked",
        "setCustom",
        "day",
        "hour",
        "setOneToTwoBusinessDays",
        "setSameDay",
        "setSameDayDeposit",
        "sameDay",
        "setWeekendBalanceEnabled",
        "enabled",
        "shouldShowWeekendBalance",
        "showAutomaticDeposits",
        "pausedSettlement",
        "showDepositSchedule",
        "showDepositSpeed",
        "showInstantTransfersInstrument",
        "showInstantTransfersSection",
        "showInstantTransfersSetUpButton",
        "instantDepositAllowed",
        "showInstantTransfersToggle",
        "toCloseOfDayDialogScreenData",
        "toDepositScheduleScreenData",
        "depositScheduleState",
        "toDepositSettingsScreenData",
        "bankSettingsState",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final clock:Lcom/squareup/util/Clock;

.field private final depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

.field private final rateFormatter:Lcom/squareup/text/RateFormatter;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/depositschedule/DepositScheduleSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/text/RateFormatter;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/util/Clock;Lcom/squareup/instantdeposit/InstantDepositRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositScheduleSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rateFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankAccountSettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sidebarRefresher"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instantDepositRunner"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    iput-object p3, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p4, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->rateFormatter:Lcom/squareup/text/RateFormatter;

    iput-object p5, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    iput-object p6, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    iput-object p7, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    iput-object p8, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iput-object p9, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->clock:Lcom/squareup/util/Clock;

    iput-object p10, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    .line 74
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method public static final synthetic access$maybeGoToAutomaticDepositsEnabledDialog(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->maybeGoToAutomaticDepositsEnabledDialog(Lcom/squareup/depositschedule/DepositScheduleSettings$State;)V

    return-void
.end method

.method public static final synthetic access$maybeGoToManualDepositsEnabledDialog(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->maybeGoToManualDepositsEnabledDialog(Lcom/squareup/depositschedule/DepositScheduleSettings$State;)V

    return-void
.end method

.method public static final synthetic access$toCloseOfDayDialogScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->toCloseOfDayDialogScreenData(Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toDepositScheduleScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/settings/server/InstantDepositsSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->toDepositScheduleScreenData(Lcom/squareup/settings/server/InstantDepositsSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toDepositSettingsScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/settings/server/InstantDepositsSettings;Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->toDepositSettingsScreenData(Lcom/squareup/settings/server/InstantDepositsSettings;Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method private final cardDrawable()I
    .locals 2

    .line 329
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showInstantTransfersInstrument()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    .line 332
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->linkedCard()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-static {v0}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1}, Lcom/squareup/debitcard/CardDrawables;->cardDrawable(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    :goto_0
    return v0
.end method

.method private final cardStatus()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;
    .locals 1

    .line 337
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showInstantTransfersInstrument()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 340
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->linkedCard()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v0, v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    :goto_0
    return-object v0
.end method

.method private final depositScheduleHint(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/resources/TextModel;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/LocalTime;",
            ")",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 381
    sget-object p1, Lcom/squareup/resources/TextModel;->Companion:Lcom/squareup/resources/TextModel$Companion;

    invoke-virtual {p1}, Lcom/squareup/resources/TextModel$Companion;->getEmpty()Lcom/squareup/resources/FixedText;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    return-object p1

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    .line 386
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    .line 387
    invoke-static {}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt;->getNO_DAY_OF_WEEK()Ljava/lang/Integer;

    move-result-object v3

    .line 388
    iget-object v0, p1, Lcom/squareup/protos/common/time/LocalTime;->hour_of_day:Ljava/lang/Integer;

    const-string v4, "defaultCutoffTime.hour_of_day"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 389
    iget-object p1, p1, Lcom/squareup/protos/common/time/LocalTime;->minute_of_hour:Ljava/lang/Integer;

    const-string v0, "defaultCutoffTime.minute_of_hour"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const-string v6, "hh:mm a z"

    .line 384
    invoke-static/range {v1 .. v6}, Lcom/squareup/util/Times;->currentTimeZoneRepresentation(Ljava/util/TimeZone;Ljava/util/TimeZone;Ljava/lang/Integer;IILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 393
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showDepositSpeed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394
    sget v0, Lcom/squareup/settingsapplet/R$string;->instant_deposits_deposit_schedule_hint:I

    goto :goto_0

    .line 396
    :cond_1
    sget v0, Lcom/squareup/settingsapplet/R$string;->instant_deposits_deposit_schedule_hint_without_same_day_deposit:I

    .line 399
    :goto_0
    new-instance v7, Lcom/squareup/ui/LinkSpanModel;

    .line 400
    sget v2, Lcom/squareup/common/strings/R$string;->learn_more:I

    .line 401
    sget v3, Lcom/squareup/registerlib/R$string;->instant_deposits_deposit_schedule_url:I

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v7

    .line 399
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/LinkSpanModel;-><init>(IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 404
    new-instance v1, Lcom/squareup/resources/PhraseModel;

    invoke-direct {v1, v0}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    const-string v0, "closeOfDay"

    .line 405
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "close_of_day"

    invoke-virtual {v1, v0, p1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object p1

    .line 406
    check-cast v7, Lcom/squareup/resources/TextModel;

    const-string v0, "learn_more"

    invoke-virtual {p1, v0, v7}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Lcom/squareup/resources/TextModel;)Lcom/squareup/resources/PhraseModel;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    return-object p1
.end method

.method private final goToDepositsDialog(II)V
    .locals 3

    .line 429
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    invoke-direct {v2, p1, p2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v2, Lcom/squareup/widgets/warning/Warning;

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final hasActivatedAccount()Z
    .locals 1

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->showInAppActivationPostSignup()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final hasLinkedCard()Z
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->hasLinkedCard()Z

    move-result v0

    return v0
.end method

.method private final instantTransfersHint()Lcom/squareup/resources/TextModel;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 292
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showInstantTransfersSection()Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    sget-object v0, Lcom/squareup/resources/TextModel;->Companion:Lcom/squareup/resources/TextModel$Companion;

    invoke-virtual {v0}, Lcom/squareup/resources/TextModel$Companion;->getEmpty()Lcom/squareup/resources/FixedText;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    return-object v0

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "settings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 297
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v1

    .line 298
    invoke-virtual {v1}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositFeeAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 299
    invoke-virtual {v1}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositFeePercentage()Lcom/squareup/util/Percentage;

    move-result-object v1

    .line 300
    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->rateFormatter:Lcom/squareup/text/RateFormatter;

    invoke-virtual {v3, v2, v1}, Lcom/squareup/text/RateFormatter;->format(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 301
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 302
    new-instance v2, Lcom/squareup/ui/LinkSpanModel;

    .line 303
    sget v3, Lcom/squareup/settingsapplet/R$string;->instant_transfer_terms:I

    .line 304
    sget v4, Lcom/squareup/settingsapplet/R$string;->instant_transfer_terms_url:I

    .line 305
    sget v5, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    .line 302
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/ui/LinkSpanModel;-><init>(III)V

    .line 308
    sget-object v3, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    const-string v4, "fee"

    if-ne v0, v3, :cond_1

    .line 309
    new-instance v0, Lcom/squareup/resources/PhraseModel;

    sget v3, Lcom/squareup/settingsapplet/R$string;->instant_transfers_description_uk:I

    invoke-direct {v0, v3}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 310
    invoke-virtual {v0, v4, v1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    .line 311
    check-cast v2, Lcom/squareup/resources/TextModel;

    const-string v1, "instant_transfer_terms"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Lcom/squareup/resources/TextModel;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    goto :goto_0

    .line 313
    :cond_1
    new-instance v0, Lcom/squareup/resources/PhraseModel;

    sget v2, Lcom/squareup/settingsapplet/R$string;->instant_transfers_description:I

    invoke-direct {v0, v2}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 314
    invoke-virtual {v0, v4, v1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    :goto_0
    return-object v0
.end method

.method private final linkedCard()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
    .locals 2

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    const-string v1, "settings.instantDepositsSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->getLinkedCard()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v0

    return-object v0
.end method

.method private final linkedCardText()Lcom/squareup/resources/TextModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 319
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showInstantTransfersInstrument()Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    sget-object v0, Lcom/squareup/resources/TextModel;->Companion:Lcom/squareup/resources/TextModel$Companion;

    invoke-virtual {v0}, Lcom/squareup/resources/TextModel$Companion;->getEmpty()Lcom/squareup/resources/FixedText;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    return-object v0

    .line 323
    :cond_0
    new-instance v0, Lcom/squareup/resources/PhraseModel;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_linked_card_hint:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 324
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->linkedCard()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v1, v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    iget-object v1, v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-static {v1}, Lcom/squareup/card/CardBrands;->brandNameId(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)I

    move-result v1

    const-string v2, "card_brand"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;I)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    .line 325
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->linkedCard()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    iget-object v1, v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    if-nez v1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    iget-object v1, v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    if-nez v1, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    const-string v2, "linkedCard()!!.card_details!!.pan_suffix!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "card_suffix"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method private final manualDepositsEnabledBodyResId()I
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    sget v0, Lcom/squareup/settingsapplet/R$string;->manual_deposits_enabled_body_with_id:I

    goto :goto_0

    .line 436
    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$string;->manual_deposits_enabled_body_without_id:I

    :goto_0
    return v0
.end method

.method private final maybeGoToAutomaticDepositsEnabledDialog(Lcom/squareup/depositschedule/DepositScheduleSettings$State;)V
    .locals 1

    .line 410
    iget-object p1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    sget-object v0, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->SUCCEEDED:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    if-ne p1, v0, :cond_0

    .line 412
    sget p1, Lcom/squareup/settingsapplet/R$string;->automatic_deposits_enabled_title:I

    sget v0, Lcom/squareup/settingsapplet/R$string;->automatic_deposits_enabled_body:I

    .line 411
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->goToDepositsDialog(II)V

    :cond_0
    return-void
.end method

.method private final maybeGoToManualDepositsEnabledDialog(Lcom/squareup/depositschedule/DepositScheduleSettings$State;)V
    .locals 1

    .line 418
    iget-object p1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    sget-object v0, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->SUCCEEDED:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    if-ne p1, v0, :cond_0

    .line 420
    sget p1, Lcom/squareup/settingsapplet/R$string;->manual_deposits_enabled_title:I

    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->manualDepositsEnabledBodyResId()I

    move-result v0

    .line 419
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->goToDepositsDialog(II)V

    :cond_0
    return-void
.end method

.method private final shouldShowWeekendBalance(I)Z
    .locals 2

    .line 372
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_WEEKEND_BALANCE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final showAutomaticDeposits(Z)Z
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_PAUSE_NIGHTLY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final showDepositSchedule()Z
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings;->isVisible()Z

    move-result v0

    return v0
.end method

.method private final showDepositSpeed()Z
    .locals 2

    .line 367
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SAME_DAY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final showInstantTransfersInstrument()Z
    .locals 2

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_DEPOSIT_SETTINGS_CARD_LINKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->hasLinkedCard()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final showInstantTransfersSection()Z
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final showInstantTransfersSetUpButton(Z)Z
    .locals 2

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->hasLinkedCard()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    .line 351
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final showInstantTransfersToggle(Z)Z
    .locals 2

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final toCloseOfDayDialogScreenData(Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;
    .locals 4

    .line 284
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;

    .line 285
    iget v1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    .line 286
    invoke-virtual {p1}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositInterval()Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/depositsettings/DepositInterval;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    const-string v3, "state.depositInterval.cutoff_at"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 287
    invoke-virtual {p1}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->timeZone()Ljava/util/TimeZone;

    move-result-object p1

    .line 284
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;-><init>(ILcom/squareup/protos/common/time/DayTime;Ljava/util/TimeZone;)V

    return-object v0
.end method

.method private final toDepositScheduleScreenData(Lcom/squareup/settings/server/InstantDepositsSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;
    .locals 8

    .line 271
    new-instance v7, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;

    .line 273
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->sameDayDepositFee(Lcom/squareup/settings/server/InstantDepositsSettings;)Lcom/squareup/resources/TextModel;

    move-result-object v2

    .line 274
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->hasLinkedCard()Z

    move-result v3

    .line 275
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showDepositSpeed()Z

    move-result v4

    .line 276
    iget p1, p2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->shouldShowWeekendBalance(I)Z

    move-result v5

    .line 277
    iget-object p1, p2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleHint(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/resources/TextModel;

    move-result-object v6

    move-object v0, v7

    move-object v1, p2

    .line 271
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;-><init>(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/resources/TextModel;ZZZLcom/squareup/resources/TextModel;)V

    return-object v7
.end method

.method private final toDepositSettingsScreenData(Lcom/squareup/settings/server/InstantDepositsSettings;Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v3, p3

    .line 245
    iget-object v1, v3, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    sget-object v2, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->SUCCEEDED:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    if-ne v1, v2, :cond_0

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->pausedSettlement()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 246
    :goto_0
    new-instance v19, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;

    move-object/from16 v1, v19

    .line 249
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->linkedCardText()Lcom/squareup/resources/TextModel;

    move-result-object v4

    .line 250
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->cardDrawable()I

    move-result v5

    .line 251
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->cardStatus()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    move-result-object v6

    .line 252
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositAllowed()Z

    move-result v7

    .line 253
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->hasActivatedAccount()Z

    move-result v8

    .line 254
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showInstantTransfersSection()Z

    move-result v9

    .line 255
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showInstantTransfersInstrument()Z

    move-result v10

    .line 256
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositAllowed()Z

    move-result v11

    invoke-direct {v0, v11}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showInstantTransfersSetUpButton(Z)Z

    move-result v11

    .line 257
    invoke-direct {v0, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showInstantTransfersToggle(Z)Z

    move-result v12

    .line 258
    invoke-direct {v0, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showAutomaticDeposits(Z)Z

    move-result v13

    .line 259
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showDepositSpeed()Z

    move-result v14

    .line 260
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->showDepositSchedule()Z

    move-result v15

    .line 261
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->instantTransfersHint()Lcom/squareup/resources/TextModel;

    move-result-object v16

    .line 262
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->sameDayDepositFee(Lcom/squareup/settings/server/InstantDepositsSettings;)Lcom/squareup/resources/TextModel;

    move-result-object v17

    .line 263
    iget-object v2, v3, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    invoke-direct {v0, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleHint(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/resources/TextModel;

    move-result-object v18

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    .line 246
    invoke-direct/range {v1 .. v18}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;-><init>(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/resources/TextModel;ILcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;ZZZZZZZZZLcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;)V

    return-object v19
.end method


# virtual methods
.method public final closeOfDayDialogScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;",
            ">;"
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    .line 124
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$closeOfDayDialogScreenData$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$closeOfDayDialogScreenData$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$sam$io_reactivex_functions_Function$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "depositScheduleSettings.\u2026oseOfDayDialogScreenData)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final depositScheduleScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 113
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 114
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailableRx2()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "settings.settingsAvailableRx2()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v2}, Lcom/squareup/depositschedule/DepositScheduleSettings;->state()Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositScheduleScreenData$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositScheduleScreenData$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026ate\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final depositSettingsScreenData()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailableRx2()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 99
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v1}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 100
    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v2}, Lcom/squareup/depositschedule/DepositScheduleSettings;->state()Lio/reactivex/Observable;

    move-result-object v2

    check-cast v2, Lio/reactivex/ObservableSource;

    .line 101
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toTriplet()Lio/reactivex/functions/Function3;

    move-result-object v3

    .line 97
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object v0

    .line 105
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositSettingsScreenData$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositSettingsScreenData$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n        s\u2026ate\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final enableAutomaticDeposits()V
    .locals 4

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setAutomaticDepositsEnabled(Z)Lio/reactivex/Single;

    move-result-object v1

    .line 166
    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$enableAutomaticDeposits$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v2, v3}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$enableAutomaticDeposits$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v3, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "depositScheduleSettings.\u2026ticDepositsEnabledDialog)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final enableManualDeposits()V
    .locals 4

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setAutomaticDepositsEnabled(Z)Lio/reactivex/Single;

    move-result-object v1

    .line 171
    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$enableManualDeposits$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v2, v3}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$enableManualDeposits$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v3, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "depositScheduleSettings.\u2026ualDepositsEnabledDialog)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final goBack()V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public final goToCloseOfDayDialog()V
    .locals 3

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public final goToDepositScheduleScreen(I)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v0, p1}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setDayOfWeek(I)V

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final goToDepositSettingsScreen()V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final goToLinkDebitCardEntryScreen()V
    .locals 4

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/LinkDebitCardBootstrapScreen;

    new-instance v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;

    sget-object v3, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;->SETTINGS_APPLET:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;

    invoke-direct {v2, v3}, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;)V

    check-cast v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/LinkDebitCardBootstrapScreen;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final goToLinkDebitCardResultScreen()V
    .locals 3

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/LinkDebitCardBootstrapScreen;

    sget-object v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;

    check-cast v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/LinkDebitCardBootstrapScreen;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final logInstantDepositToggled(Z)V
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->instantDepositToggled(Z)V

    return-void
.end method

.method public final onChangeDebitCard()V
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tapLinkDifferentDebitCard()V

    .line 219
    invoke-virtual {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->goToLinkDebitCardEntryScreen()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings;->getDepositSchedule()Lio/reactivex/Single;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "depositScheduleSettings.\u2026             .subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onLinkDebitCardResult()V
    .locals 3

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner;->checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "instantDepositRunner.che\u2026   )\n        .subscribe()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final onRefresh()V
    .locals 3

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v1}, Lcom/squareup/banklinking/BankAccountSettings;->refresh()Lio/reactivex/Single;

    move-result-object v1

    .line 160
    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "bankAccountSettings.refresh()\n        .subscribe()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 161
    invoke-virtual {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->refreshDepositSchedule()V

    return-void
.end method

.method public final refreshDepositSchedule()V
    .locals 3

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings;->getDepositSchedule()Lio/reactivex/Single;

    move-result-object v1

    .line 206
    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "depositScheduleSettings.\u2026le()\n        .subscribe()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final sameDayDepositFee(Lcom/squareup/settings/server/InstantDepositsSettings;)Lcom/squareup/resources/TextModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/InstantDepositsSettings;",
            ")",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    const-string v0, "instantDepositsSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    invoke-virtual {p1}, Lcom/squareup/settings/server/InstantDepositsSettings;->sameDayDepositFeeAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 229
    invoke-virtual {p1}, Lcom/squareup/settings/server/InstantDepositsSettings;->sameDayDepositFeePercentage()Lcom/squareup/util/Percentage;

    move-result-object p1

    .line 231
    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->isZero()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    sget-object p1, Lcom/squareup/resources/TextModel;->Companion:Lcom/squareup/resources/TextModel$Companion;

    invoke-virtual {p1}, Lcom/squareup/resources/TextModel$Companion;->getEmpty()Lcom/squareup/resources/FixedText;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_0

    .line 234
    :cond_0
    new-instance v1, Lcom/squareup/resources/PhraseModel;

    sget v2, Lcom/squareup/settingsapplet/R$string;->deposit_speed_same_day_fee:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 235
    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->rateFormatter:Lcom/squareup/text/RateFormatter;

    invoke-virtual {v2, v0, p1}, Lcom/squareup/text/RateFormatter;->format(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "percent_or_amount"

    invoke-virtual {v1, v0, p1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    :goto_0
    return-object p1
.end method

.method public final saveSettings(Z)V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    .line 154
    invoke-virtual {v0, p1}, Lcom/squareup/settings/server/InstantDepositsSettings;->allowInstantDeposit(Z)V

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    return-void
.end method

.method public final setCustom()V
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setAllToCustom()V

    return-void
.end method

.method public final setCustom(II)V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v1, p1, p2}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setCustom(II)Lio/reactivex/Single;

    move-result-object p1

    .line 214
    invoke-virtual {p1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string p2, "depositScheduleSettings.\u2026our)\n        .subscribe()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final setOneToTwoBusinessDays()V
    .locals 3

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setAllToOneToTwoBusinessDays()Lio/reactivex/Single;

    move-result-object v1

    .line 187
    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "depositScheduleSettings.\u2026ys()\n        .subscribe()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final setSameDay()V
    .locals 3

    .line 175
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->hasLinkedCard()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    invoke-virtual {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->goToLinkDebitCardEntryScreen()V

    goto :goto_0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setAllToSameDay()Lio/reactivex/Single;

    move-result-object v1

    .line 181
    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "depositScheduleSettings.\u2026()\n          .subscribe()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    :goto_0
    return-void
.end method

.method public final setSameDayDeposit(Z)V
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v1, p1}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setSameDayDeposit(Z)Lio/reactivex/Single;

    move-result-object p1

    .line 196
    invoke-virtual {p1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v1, "depositScheduleSettings.\u2026Day)\n        .subscribe()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final setWeekendBalanceEnabled(Z)V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v1, p1}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setWeekendBalanceEnabled(Z)Lio/reactivex/Single;

    move-result-object p1

    .line 201
    invoke-virtual {p1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v1, "depositScheduleSettings.\u2026led)\n        .subscribe()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
