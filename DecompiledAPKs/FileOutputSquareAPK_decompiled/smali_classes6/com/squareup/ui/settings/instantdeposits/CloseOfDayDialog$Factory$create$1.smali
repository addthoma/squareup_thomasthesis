.class final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;
.super Ljava/lang/Object;
.source "CloseOfDayDialog.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/Dialog;",
        "screenData",
        "Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic $days:[Ljava/lang/String;

.field final synthetic $scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

.field final synthetic this$0:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;[Ljava/lang/String;Landroid/content/Context;Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->$days:[Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->$scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;)Landroid/app/Dialog;
    .locals 7

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 49
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->$days:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->getDayOfWeek()I

    move-result v2

    aget-object v1, v1, v2

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->$days:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->getDayOfWeek()I

    move-result v2

    const/4 v3, 0x1

    add-int/2addr v2, v3

    const/4 v4, 0x7

    rem-int/2addr v2, v4

    aget-object v1, v1, v2

    aput-object v1, v0, v3

    .line 50
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->getCutoffAt()Lcom/squareup/protos/common/time/DayTime;

    move-result-object v1

    .line 51
    iget-object v2, v1, Lcom/squareup/protos/common/time/DayTime;->time_at:Lcom/squareup/protos/common/time/LocalTime;

    .line 53
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    .line 54
    iget-object v1, v1, Lcom/squareup/protos/common/time/DayTime;->day_of_week:Ljava/lang/Integer;

    .line 55
    iget-object v5, v2, Lcom/squareup/protos/common/time/LocalTime;->hour_of_day:Ljava/lang/Integer;

    const-string v6, "timeAt.hour_of_day"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 56
    iget-object v2, v2, Lcom/squareup/protos/common/time/LocalTime;->minute_of_hour:Ljava/lang/Integer;

    const-string v6, "timeAt.minute_of_hour"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 52
    invoke-static {v3, v1, v5, v2}, Lcom/squareup/util/Times;->currentTimeZoneCalendar(Ljava/util/TimeZone;Ljava/lang/Integer;II)Ljava/util/Calendar;

    move-result-object v1

    .line 58
    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 59
    invoke-virtual {v2, v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekRange([Ljava/lang/String;)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    move-result-object v0

    .line 60
    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->getDayOfWeek()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;->access$currentOrNextDay(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;II)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeek(I)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    move-result-object v0

    const/16 v2, 0xb

    .line 61
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hour(I)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->$scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->getDayOfWeek()I

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->onConfirmed(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;I)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->build()Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;->apply(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method
