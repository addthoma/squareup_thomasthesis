.class public final Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;
.super Lcom/squareup/ui/settings/instantdeposits/InDepositSettingsScope;
.source "DepositSettingsScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositSettingsScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositSettingsScreen.kt\ncom/squareup/ui/settings/instantdeposits/DepositSettingsScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,60:1\n43#2:61\n24#3,4:62\n*E\n*S KotlinDebug\n*F\n+ 1 DepositSettingsScreen.kt\ncom/squareup/ui/settings/instantdeposits/DepositSettingsScreen\n*L\n32#1:61\n58#1,4:62\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001\u0014B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016R\u0016\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;",
        "Lcom/squareup/ui/settings/instantdeposits/InDepositSettingsScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "ScreenData",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    .line 62
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 65
    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/InDepositSettingsScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_INSTANT_DEPOSIT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 27
    const-class v0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    const-class v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;

    .line 33
    invoke-interface {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;->instantDepositsCoordinator()Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 36
    sget v0, Lcom/squareup/settingsapplet/R$layout;->deposit_settings_view:I

    return v0
.end method
