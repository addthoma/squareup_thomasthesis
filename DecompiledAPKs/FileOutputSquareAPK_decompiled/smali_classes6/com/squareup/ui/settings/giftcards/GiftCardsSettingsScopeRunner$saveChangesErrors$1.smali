.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->saveChangesErrors()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "kotlin.jvm.PlatformType",
        "newSetting",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
            ">;"
        }
    .end annotation

    const-string v0, "newSetting"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->access$getGiftCardServiceHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/giftcard/GiftCardServiceHelper;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/giftcard/GiftCardServiceHelper;->setEGiftCardOrderConfig(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lio/reactivex/Single;

    move-result-object v0

    .line 117
    sget-object v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 118
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1$2;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1$2;-><init>(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lio/reactivex/Maybe;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1;->apply(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
