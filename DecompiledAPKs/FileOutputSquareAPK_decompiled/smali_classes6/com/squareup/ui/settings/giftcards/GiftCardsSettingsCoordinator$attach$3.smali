.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$3;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardsSettingsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/settings/giftcards/ScreenData;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/settings/giftcards/ScreenData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$3;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/ui/settings/giftcards/ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$3;->invoke(Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/settings/giftcards/ScreenData;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$3;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    invoke-static {v0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$onScreenData(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    return-void
.end method
