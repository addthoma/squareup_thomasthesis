.class public final Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;
.super Ljava/lang/Object;
.source "EGiftCardImageUploader.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u001dB?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u001e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u001c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0016J\u0012\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u000e*\u00020\u0016H\u0002J\u0012\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0019*\u00020\u001bH\u0002J\u0012\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000e*\u00020\u0016H\u0002R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;",
        "",
        "giftCardServiceHelper",
        "Lcom/squareup/giftcard/GiftCardServiceHelper;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "computationScheduler",
        "fileThreadScheduler",
        "tempPhotoDir",
        "Ljava/io/File;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/giftcard/GiftCardServiceHelper;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/io/File;Lcom/squareup/util/Res;)V",
        "saveNewThemeRequest",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
        "response",
        "Lcom/squareup/server/payment/GiftCardImageUploadResponse;",
        "configSoFar",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
        "uploadImage",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "toNewBitmapCorrectSize",
        "toTheme",
        "",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "",
        "writeToTempFile",
        "UploadResult",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final computationScheduler:Lio/reactivex/Scheduler;

.field private final fileThreadScheduler:Lio/reactivex/Scheduler;

.field private final giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final tempPhotoDir:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/GiftCardServiceHelper;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/io/File;Lcom/squareup/util/Res;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p5    # Ljava/io/File;
        .annotation runtime Lcom/squareup/util/TempPhotoDir;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "giftCardServiceHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "computationScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileThreadScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tempPhotoDir"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->computationScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->fileThreadScheduler:Lio/reactivex/Scheduler;

    iput-object p5, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->tempPhotoDir:Ljava/io/File;

    iput-object p6, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getGiftCardServiceHelper$p(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;)Lcom/squareup/giftcard/GiftCardServiceHelper;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    return-object p0
.end method

.method public static final synthetic access$getTempPhotoDir$p(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;)Ljava/io/File;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->tempPhotoDir:Ljava/io/File;

    return-object p0
.end method

.method public static final synthetic access$saveNewThemeRequest(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lcom/squareup/server/payment/GiftCardImageUploadResponse;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lio/reactivex/Single;
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->saveNewThemeRequest(Lcom/squareup/server/payment/GiftCardImageUploadResponse;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toNewBitmapCorrectSize(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Landroid/graphics/Bitmap;)Lio/reactivex/Single;
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->toNewBitmapCorrectSize(Landroid/graphics/Bitmap;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$writeToTempFile(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Landroid/graphics/Bitmap;)Lio/reactivex/Single;
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->writeToTempFile(Landroid/graphics/Bitmap;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final saveNewThemeRequest(Lcom/squareup/server/payment/GiftCardImageUploadResponse;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/payment/GiftCardImageUploadResponse;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
            ">;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    iget-object p1, p1, Lcom/squareup/server/payment/GiftCardImageUploadResponse;->url:Ljava/lang/String;

    const-string v1, "response.url"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->toTheme(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p2, p1}, Lcom/squareup/giftcard/GiftCardServiceHelper;->setEGiftCardOrderConfig(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    .line 86
    sget-object p2, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$saveNewThemeRequest$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$saveNewThemeRequest$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "giftCardServiceHelper.se\u2026led\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toNewBitmapCorrectSize(Landroid/graphics/Bitmap;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 60
    new-instance v0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$toNewBitmapCorrectSize$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$toNewBitmapCorrectSize$1;-><init>(Landroid/graphics/Bitmap;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->computationScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.fromCallable {\n  \u2026eOn(computationScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toTheme(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    .line 96
    new-instance v0, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;-><init>()V

    .line 97
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->image_url(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;

    move-result-object p1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/giftcard/R$color;->egiftcard_default_bg_color:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    .line 99
    invoke-static {v0}, Lcom/squareup/util/Colors;->toRGBHex(I)Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->background_color(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;

    move-result-object p1

    .line 103
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->read_only_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;

    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->build()Lcom/squareup/protos/client/giftcards/EGiftTheme;

    move-result-object p1

    .line 95
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final writeToTempFile(Landroid/graphics/Bitmap;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 67
    new-instance v0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1;-><init>(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Landroid/graphics/Bitmap;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->fileThreadScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.fromCallable {\n  \u2026beOn(fileThreadScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final uploadImage(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Landroid/graphics/Bitmap;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
            ">;"
        }
    .end annotation

    const-string v0, "configSoFar"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bitmap"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {p2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p2

    .line 45
    new-instance v0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$1;-><init>(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p2

    .line 46
    new-instance v0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$2;-><init>(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p2

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$3;-><init>(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p2

    .line 48
    new-instance v0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$4;-><init>(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 55
    sget-object p2, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$5;->INSTANCE:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$5;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 56
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(bitmap)\n    \u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
