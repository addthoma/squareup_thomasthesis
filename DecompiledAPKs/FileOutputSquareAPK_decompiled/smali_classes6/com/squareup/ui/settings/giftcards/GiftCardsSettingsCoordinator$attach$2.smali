.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardsSettingsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardsSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardsSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2\n*L\n1#1,260:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "showBackButton",
        "",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2;->invoke(Ljava/lang/Boolean;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Boolean;)V
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getActionBar$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 112
    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getRes$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/util/Res;

    move-result-object v2

    sget v3, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_title:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    const-string v2, "showBackButton"

    .line 113
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    new-instance v2, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2$$special$$inlined$run$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2$$special$$inlined$run$lambda$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2;Ljava/lang/Boolean;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 116
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 118
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 111
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
