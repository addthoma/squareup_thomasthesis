.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;
.super Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;
.source "GiftCardsSettingsScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Runner;,
        Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardsSettingsScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardsSettingsScreen.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,82:1\n43#2:83\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardsSettingsScreen.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen\n*L\n28#1:83\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u0000 \u00102\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0002\u0010\u0011B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0007H\u0016J\u0008\u0010\u0008\u001a\u00020\tH\u0016J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "getParentKey",
        "",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Companion",
        "Runner",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Companion;

.field private static final INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;->Companion:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Companion;

    .line 37
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;

    .line 40
    sget-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(INSTANCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;-><init>()V

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;

    return-object v0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_GIFTCARDS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 2

    .line 25
    sget-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;

    const-string v1, "GiftCardsSettingsScope.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    const-class v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;

    .line 29
    invoke-interface {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;->giftCardsSettingsCoordinator()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 34
    sget v0, Lcom/squareup/settingsapplet/R$layout;->giftcards_settings_screen:I

    return v0
.end method
