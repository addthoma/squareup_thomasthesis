.class public Lcom/squareup/ui/settings/PosSettingsAppletSections;
.super Ljava/lang/Object;
.source "PosSettingsAppletSections.java"

# interfaces
.implements Lcom/squareup/ui/settings/SettingsAppletSections;


# instance fields
.field private final bankAccountEntry:Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;

.field private final barcodeScannersEntry:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;

.field private final cardReadersEntry:Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;

.field private final cashDrawerEntry:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;

.field private final cashManagementEntry:Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;

.field private final customerManagementEntry:Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;

.field private final depositsEntry:Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;

.field private final deviceEntry:Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;

.field private final emailCollectionEntry:Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;

.field private final employeeManagementEntry:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;

.field private final giftCardsEntry:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;

.field private final loyaltyEntry:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;

.field private final offlineEntry:Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;

.field private final onlineCheckoutEntry:Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;

.field private final openTicketsEntry:Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;

.field private final orderHubNotificationsEntry:Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;

.field private final orderHubPrintingEntry:Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;

.field private final orderHubQuickActionsEntry:Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;

.field private final passcodesSettingsEntry:Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;

.field private final paymentTypesEntry:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;

.field private final printerStationsEntry:Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;

.field private final publicProfileEntry:Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;

.field private final quickAmountsSettingsEntry:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;

.field private final scalesEntry:Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;

.field private final sharedSettingsEntry:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;

.field private final signatureAndReceiptEntry:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;

.field private final swipeChipCardsEntry:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;

.field private final taxesEntry:Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;

.field private final tileAppearanceEntry:Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;

.field private final timeTrackingSettingsEntry:Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;

.field private final tippingEntry:Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->sharedSettingsEntry:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;

    move-object v1, p2

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->paymentTypesEntry:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;

    move-object v1, p3

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->taxesEntry:Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;

    move-object v1, p4

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->signatureAndReceiptEntry:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;

    move-object v1, p5

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->quickAmountsSettingsEntry:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;

    move-object v1, p6

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->tippingEntry:Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;

    move-object v1, p7

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->cashManagementEntry:Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;

    move-object v1, p8

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->offlineEntry:Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;

    move-object v1, p9

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->employeeManagementEntry:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;

    move-object v1, p10

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->openTicketsEntry:Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;

    move-object v1, p11

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->swipeChipCardsEntry:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;

    move-object v1, p12

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->customerManagementEntry:Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;

    move-object v1, p13

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->emailCollectionEntry:Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;

    move-object/from16 v1, p14

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->loyaltyEntry:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;

    move-object/from16 v1, p15

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->giftCardsEntry:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;

    move-object/from16 v1, p16

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->onlineCheckoutEntry:Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;

    move-object/from16 v1, p17

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->tileAppearanceEntry:Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;

    move-object/from16 v1, p18

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->passcodesSettingsEntry:Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;

    move-object/from16 v1, p19

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->timeTrackingSettingsEntry:Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;

    move-object/from16 v1, p20

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->cardReadersEntry:Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;

    move-object/from16 v1, p21

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->printerStationsEntry:Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;

    move-object/from16 v1, p22

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->cashDrawerEntry:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;

    move-object/from16 v1, p23

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->barcodeScannersEntry:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;

    move-object/from16 v1, p24

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->scalesEntry:Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;

    move-object/from16 v1, p25

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->deviceEntry:Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;

    move-object/from16 v1, p26

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->orderHubNotificationsEntry:Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;

    move-object/from16 v1, p27

    .line 181
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->orderHubPrintingEntry:Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;

    move-object/from16 v1, p28

    .line 182
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->orderHubQuickActionsEntry:Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;

    move-object/from16 v1, p31

    .line 185
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->depositsEntry:Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;

    move-object/from16 v1, p29

    .line 186
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->publicProfileEntry:Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;

    move-object/from16 v1, p30

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->bankAccountEntry:Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;

    return-void
.end method

.method private hardwareSectionEntries()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    .line 257
    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->cardReadersEntry:Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->printerStationsEntry:Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->cashDrawerEntry:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->barcodeScannersEntry:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->scalesEntry:Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public accountSectionEntries()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    .line 281
    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->publicProfileEntry:Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->bankAccountEntry:Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->depositsEntry:Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public checkoutSectionEntries()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    .line 223
    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->sharedSettingsEntry:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->paymentTypesEntry:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->taxesEntry:Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->signatureAndReceiptEntry:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->quickAmountsSettingsEntry:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->tippingEntry:Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->cashManagementEntry:Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->offlineEntry:Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->employeeManagementEntry:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->openTicketsEntry:Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->swipeChipCardsEntry:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->customerManagementEntry:Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->emailCollectionEntry:Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->loyaltyEntry:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->giftCardsEntry:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->onlineCheckoutEntry:Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->tileAppearanceEntry:Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public deviceSectionEntries()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    .line 267
    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->deviceEntry:Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBarcodeScannersEntry()Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->barcodeScannersEntry:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;

    return-object v0
.end method

.method public getCardReadersEntry()Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->cardReadersEntry:Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;

    return-object v0
.end method

.method public getCashDrawerEntry()Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->cashDrawerEntry:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;

    return-object v0
.end method

.method public getPrinterStationsEntry()Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->printerStationsEntry:Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;

    return-object v0
.end method

.method public getScalesEntry()Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->scalesEntry:Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;

    return-object v0
.end method

.method public orderHubSectionEntries()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    .line 273
    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->orderHubNotificationsEntry:Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->orderHubPrintingEntry:Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->orderHubQuickActionsEntry:Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public orderedEntries()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 192
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PosSettingsAppletSections;->checkoutSectionEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 193
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PosSettingsAppletSections;->securitySectionEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 194
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PosSettingsAppletSections;->teamManagementSectionEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 195
    invoke-direct {p0}, Lcom/squareup/ui/settings/PosSettingsAppletSections;->hardwareSectionEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 196
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PosSettingsAppletSections;->deviceSectionEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 197
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PosSettingsAppletSections;->orderHubSectionEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 198
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PosSettingsAppletSections;->accountSectionEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public securitySectionEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->passcodesSettingsEntry:Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public teamManagementSectionEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/settings/PosSettingsAppletSections;->timeTrackingSettingsEntry:Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
