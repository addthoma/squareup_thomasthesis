.class public Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "SharedSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private analytics:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;->analytics:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onDashboardLinkClicked(Landroid/view/View;)V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;->analytics:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;->dashboardLinkClicked()V

    .line 64
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->shared_settings_empty_state_dashboard_link:I

    .line 65
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 51
    const-class v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;

    return-object v0
.end method
