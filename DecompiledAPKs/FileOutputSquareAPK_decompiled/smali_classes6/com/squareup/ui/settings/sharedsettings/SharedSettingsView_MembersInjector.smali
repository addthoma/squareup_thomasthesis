.class public final Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView_MembersInjector;
.super Ljava/lang/Object;
.source "SharedSettingsView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;->presenter:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;)V

    return-void
.end method
