.class public Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;
.super Lcom/squareup/applet/AppletSection;
.source "SharedSettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$Access;,
        Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$ListEntry;,
        Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    sget v0, Lcom/squareup/settingsapplet/R$string;->shared_settings_title:I

    sput v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    new-instance v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$Access;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$Access;-><init>(Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;

    return-object v0
.end method
