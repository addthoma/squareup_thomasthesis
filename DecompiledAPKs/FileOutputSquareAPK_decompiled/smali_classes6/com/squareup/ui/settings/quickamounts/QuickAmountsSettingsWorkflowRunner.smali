.class public final Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;
.super Lcom/squareup/container/WorkflowV2Runner;
.source "QuickAmountsSettingsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/WorkflowV2Runner<",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00172\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0017B/\u0008\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0017J\u0015\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0002H\u0001\u00a2\u0006\u0002\u0008\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0006X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;",
        "Lcom/squareup/container/WorkflowV2Runner;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "workflow",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;",
        "createItemTutorialRunner",
        "Lcom/squareup/items/tutorial/CreateItemTutorialRunner;",
        "quickAmountsSettings",
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "viewFactory",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;Lcom/squareup/items/tutorial/CreateItemTutorialRunner;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;)V",
        "getWorkflow",
        "()Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "onResult",
        "output",
        "onResult$settings_applet_release",
        "Companion",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final createItemTutorialRunner:Lcom/squareup/items/tutorial/CreateItemTutorialRunner;

.field private final quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

.field private final workflow:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->Companion:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$Companion;

    .line 78
    const-class v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "QuickAmountsSettingsWork\u2026owRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;Lcom/squareup/items/tutorial/CreateItemTutorialRunner;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createItemTutorialRunner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quickAmountsSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v2, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    .line 31
    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 32
    move-object v4, p5

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 29
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p2, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->workflow:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->createItemTutorialRunner:Lcom/squareup/items/tutorial/CreateItemTutorialRunner;

    iput-object p4, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;)V
    .locals 0

    .line 23
    invoke-virtual {p0}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->workflow:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->getWorkflow()Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-super {p0, p1}, Lcom/squareup/container/WorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final onResult$settings_applet_release(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;)V
    .locals 1

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    instance-of v0, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$Exit;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    sget-object v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPastWorkflow(Ljava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_0
    instance-of v0, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateSetAmounts;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    check-cast p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateSetAmounts;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateSetAmounts;->getQuickAmounts()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/quickamounts/QuickAmountsSettings;->updateSetAmounts(Lcom/squareup/quickamounts/QuickAmounts;)V

    goto :goto_0

    .line 54
    :cond_1
    instance-of v0, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateStatus;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    check-cast p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateStatus;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateStatus;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/quickamounts/QuickAmountsSettings;->setFeatureStatus(Lcom/squareup/quickamounts/QuickAmountsStatus;)V

    goto :goto_0

    .line 55
    :cond_2
    instance-of v0, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateSetAmountsAndExit;

    if-eqz v0, :cond_3

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    check-cast p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateSetAmountsAndExit;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateSetAmountsAndExit;->getQuickAmounts()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/quickamounts/QuickAmountsSettings;->updateSetAmounts(Lcom/squareup/quickamounts/QuickAmounts;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    sget-object v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPastWorkflow(Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_3
    instance-of p1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$StartItemsTutorial;

    if-eqz p1, :cond_4

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    sget-object v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPastWorkflow(Ljava/lang/String;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;->createItemTutorialRunner:Lcom/squareup/items/tutorial/CreateItemTutorialRunner;

    invoke-interface {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialRunner;->showCreateItemTutorial()V

    :cond_4
    :goto_0
    return-void
.end method
