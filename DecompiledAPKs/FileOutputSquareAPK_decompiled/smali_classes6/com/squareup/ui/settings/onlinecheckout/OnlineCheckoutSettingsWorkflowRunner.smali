.class public final Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;
.super Lcom/squareup/container/WorkflowV2Runner;
.source "OnlineCheckoutSettingsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$ParentComponent;,
        Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Scope;,
        Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$BootstrapScreen;,
        Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/WorkflowV2Runner<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00112\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u0010\u0011\u0012\u0013B\u001f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000fH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u0006X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;",
        "Lcom/squareup/container/WorkflowV2Runner;",
        "",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "onlineCheckoutSettingsWorkflow",
        "Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;",
        "onlineCheckoutSettingsWorkflowViewFactory",
        "Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflowViewFactory;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflowViewFactory;)V",
        "workflow",
        "getWorkflow",
        "()Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "BootstrapScreen",
        "Companion",
        "ParentComponent",
        "Scope",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->Companion:Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;

    .line 70
    const-class v0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OnlineCheckoutSettingsWo\u2026owRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflowViewFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineCheckoutSettingsWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineCheckoutSettingsWorkflowViewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v2, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    .line 24
    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 25
    move-object v4, p3

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 22
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->workflow:Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;)V
    .locals 0

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->workflow:Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->getWorkflow()Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-super {p0, p1}, Lcom/squareup/container/WorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
