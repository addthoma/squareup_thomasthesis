.class public final Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "OnlineCheckoutSettingsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "get",
        "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;",
        "scope",
        "Lmortar/MortarScope;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$get(Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;Lmortar/MortarScope;)Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;

    move-result-object p0

    return-object p0
.end method

.method private final get(Lmortar/MortarScope;)Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;
    .locals 1

    .line 73
    invoke-static {}, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;->access$getNAME$cp()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsWorkflowRunner;

    return-object p1
.end method
