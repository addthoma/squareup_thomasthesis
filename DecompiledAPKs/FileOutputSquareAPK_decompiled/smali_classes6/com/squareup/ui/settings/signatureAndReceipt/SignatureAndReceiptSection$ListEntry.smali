.class public Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "SignatureAndReceiptSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 78
    sget-object v2, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    .line 79
    invoke-static {p4, p5}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->getSectionNameResId(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;)I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    .line 78
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    .line 80
    iput-object p4, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 81
    iput-object p5, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    .line 82
    iput-object p6, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 83
    iput-object p7, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method private hideValue()Z
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->canSkipReceiptScreen()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 114
    invoke-static {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->shouldShowSignatureSettings(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public getShortValueText()Ljava/lang/String;
    .locals 2

    .line 103
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->hideValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 107
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/SignatureSettings;->merchantOptedInToSkipSignaturesForSmallPayments()Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/squareup/settingsapplet/R$string;->signature_optional_on_short:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/squareup/settingsapplet/R$string;->signature_optional_off_short:I

    .line 106
    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValueText()Ljava/lang/String;
    .locals 4

    .line 87
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->hideValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SignatureSettings;->merchantOptedInToSkipSignaturesForSmallPayments()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 92
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getSkipSignatureMaxCents()J

    move-result-wide v1

    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v2, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 91
    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 93
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->signature_optional_on:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "amount"

    .line 95
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->signature_optional_off_short:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
