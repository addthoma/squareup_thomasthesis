.class public Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;
.super Landroid/widget/LinearLayout;
.source "SignatureAndReceiptSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private additionalAuthSlipContainer:Landroid/view/View;

.field private additionalAuthSlipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private alwaysSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private alwaysSkipSignatureWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private lastSelectedSignatureOption:Landroid/widget/Checkable;

.field private legacySignatureAvailableOptions:Landroid/view/View;

.field private neverSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private noSignatureTooltip:Lcom/squareup/widgets/MessageView;

.field private paperSignatureDivider:Landroid/view/View;

.field private paperSignatureGroup:Lcom/squareup/widgets/CheckableGroup;

.field private paperSignatureOptions:Landroid/view/View;

.field private paperSignatureReceiptOptions:Landroid/view/View;

.field presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private quickTipHint:Lcom/squareup/widgets/MessageView;

.field private quickTipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private signOnDeviceHint:Lcom/squareup/widgets/MessageView;

.field private signOnPrintedReceiptSwitch:Lcom/squareup/ui/widgets/GlyphRadioRow;

.field private signOnPrintedReceiptWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field private signOnPrintedReceiptWithoutPrinterPopup:Lcom/squareup/caller/FailurePopup;

.field private signatureAvailableOptions:Landroid/view/View;

.field private skipReceiptScreenDivider:Landroid/view/View;

.field private skipReceiptScreenHint:Lcom/squareup/widgets/MessageView;

.field private skipReceiptScreenSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private skipSignatureUnderAmountTooltip:Lcom/squareup/marketfont/MarketTextView;

.field private skipSignatureWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field private traditionalReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 66
    iput-object p2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->lastSelectedSignatureOption:Landroid/widget/Checkable;

    .line 70
    const-class p2, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->alwaysSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->neverSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->quickTipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->traditionalReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 327
    sget v0, Lcom/squareup/settingsapplet/R$id;->signature_available_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signatureAvailableOptions:Landroid/view/View;

    .line 328
    sget v0, Lcom/squareup/settingsapplet/R$id;->always_skip_signature:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->alwaysSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 329
    sget v0, Lcom/squareup/settingsapplet/R$id;->under_amount_skip_signature:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 330
    sget v0, Lcom/squareup/settingsapplet/R$id;->never_skip_signature:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->neverSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 331
    sget v0, Lcom/squareup/settingsapplet/R$id;->no_signature_tooltip:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->noSignatureTooltip:Lcom/squareup/widgets/MessageView;

    .line 332
    sget v0, Lcom/squareup/settingsapplet/R$id;->paper_signature_group:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureGroup:Lcom/squareup/widgets/CheckableGroup;

    .line 333
    sget v0, Lcom/squareup/settingsapplet/R$id;->sign_on_printed_receipt:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/widgets/GlyphRadioRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptSwitch:Lcom/squareup/ui/widgets/GlyphRadioRow;

    .line 334
    sget v0, Lcom/squareup/settingsapplet/R$id;->quick_tip:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->quickTipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 335
    sget v0, Lcom/squareup/settingsapplet/R$id;->traditional_receipt:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->traditionalReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 336
    sget v0, Lcom/squareup/settingsapplet/R$id;->additional_auth_slip_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->additionalAuthSlipContainer:Landroid/view/View;

    .line 337
    sget v0, Lcom/squareup/settingsapplet/R$id;->aditional_auth_slip:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->additionalAuthSlipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 338
    sget v0, Lcom/squareup/settingsapplet/R$id;->paper_signature_available_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureOptions:Landroid/view/View;

    .line 339
    sget v0, Lcom/squareup/settingsapplet/R$id;->paper_signature_receipt_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureReceiptOptions:Landroid/view/View;

    .line 340
    sget v0, Lcom/squareup/settingsapplet/R$id;->legacy_signature_available_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->legacySignatureAvailableOptions:Landroid/view/View;

    .line 341
    sget v0, Lcom/squareup/settingsapplet/R$id;->sign_skip_under_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 342
    sget v0, Lcom/squareup/settingsapplet/R$id;->skip_receipt_screen:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 343
    sget v0, Lcom/squareup/settingsapplet/R$id;->skip_receipt_screen_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenHint:Lcom/squareup/widgets/MessageView;

    .line 344
    sget v0, Lcom/squareup/settingsapplet/R$id;->sign_skip_under_amount_tooltip:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountTooltip:Lcom/squareup/marketfont/MarketTextView;

    .line 345
    sget v0, Lcom/squareup/settingsapplet/R$id;->paper_signature_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureDivider:Landroid/view/View;

    .line 346
    sget v0, Lcom/squareup/settingsapplet/R$id;->skip_receipt_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenDivider:Landroid/view/View;

    .line 347
    sget v0, Lcom/squareup/settingsapplet/R$id;->sign_on_device_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnDeviceHint:Lcom/squareup/widgets/MessageView;

    .line 348
    sget v0, Lcom/squareup/settingsapplet/R$id;->quick_tip_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->quickTipHint:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private setChecked(Landroid/widget/Checkable;)V
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->lastSelectedSignatureOption:Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_0
    const/4 v0, 0x1

    .line 388
    invoke-interface {p1, v0}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 389
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->lastSelectedSignatureOption:Landroid/widget/Checkable;

    return-void
.end method

.method private setSignOnDeviceHint(I)V
    .locals 3

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnDeviceHint:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "learn_more"

    .line 298
    invoke-virtual {v1, p1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/registerlib/R$string;->paper_signature_help_url:I

    .line 299
    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->learn_more:I

    .line 300
    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 301
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    .line 297
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showOrHidePrinterConfigurationWarning(Z)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 305
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->WARNING_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    move-object v1, v0

    .line 307
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->res:Lcom/squareup/util/Res;

    if-eqz p1, :cond_1

    sget v3, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_1

    :cond_1
    sget v3, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray:I

    :goto_1
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    if-eqz p1, :cond_2

    .line 311
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$string;->paper_signature_sign_on_printed_receipt_description:I

    .line 312
    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    if-eqz p1, :cond_3

    .line 315
    sget p1, Lcom/squareup/marin/R$drawable;->marin_selector_button_radio_red:I

    goto :goto_2

    :cond_3
    sget p1, Lcom/squareup/marin/R$drawable;->marin_selector_button_radio:I

    .line 319
    :goto_2
    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptSwitch:Lcom/squareup/ui/widgets/GlyphRadioRow;

    invoke-virtual {v3, v1}, Lcom/squareup/ui/widgets/GlyphRadioRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 320
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptSwitch:Lcom/squareup/ui/widgets/GlyphRadioRow;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/widgets/GlyphRadioRow;->setGlyphColor(I)V

    .line 321
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptSwitch:Lcom/squareup/ui/widgets/GlyphRadioRow;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/widgets/GlyphRadioRow;->setNameColor(I)V

    .line 322
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptSwitch:Lcom/squareup/ui/widgets/GlyphRadioRow;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/widgets/GlyphRadioRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptSwitch:Lcom/squareup/ui/widgets/GlyphRadioRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/widgets/GlyphRadioRow;->setRadioBackgroundRes(I)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 383
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method hideSignOnDeviceHint()V
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnDeviceHint:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method hideSkipSignUnderAmount()V
    .locals 2

    .line 228
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountTooltip:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 234
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->updateDividers()V

    return-void
.end method

.method isAdditionalAuthSlipChecked()Z
    .locals 1

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->additionalAuthSlipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method isAlwaysSkipSignatureChecked()Z
    .locals 1

    .line 359
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->alwaysSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isAlwaysSkipSignatureEnabled()Z
    .locals 2

    .line 379
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_ALWAYS_SKIP_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method isQuickTipChecked()Z
    .locals 1

    .line 367
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->quickTipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method isSignOnPrintedReceiptChecked()Z
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptSwitch:Lcom/squareup/ui/widgets/GlyphRadioRow;

    invoke-virtual {v0}, Lcom/squareup/ui/widgets/GlyphRadioRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method isSkipReceiptScreenChecked()Z
    .locals 1

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method isSkipSignUnderAmountChecked()Z
    .locals 1

    .line 352
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$SignatureAndReceiptSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->onSkipSignatureUnderAmountToggled()V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$SignatureAndReceiptSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->onSkipReceiptScreenToggled(Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$SignatureAndReceiptSettingsView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    const/4 p1, -0x1

    if-eq p3, p1, :cond_1

    if-eq p2, p3, :cond_1

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    sget p3, Lcom/squareup/settingsapplet/R$id;->sign_on_printed_receipt:I

    if-ne p2, p3, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->onSignOnPrintedReceiptToggled(Z)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$3$SignatureAndReceiptSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 150
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->onAdditionalAuthSlipToggled(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .line 74
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->bindViews()V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->legacySignatureAvailableOptions:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->alwaysSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$1;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$2;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->neverSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$3;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->noSignatureTooltip:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/settingsapplet/R$string;->signature_never_collect_hint:I

    const-string v3, "square_support"

    .line 105
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/settingsapplet/R$string;->never_collect_signature_help_url:I

    .line 106
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->square_support:I

    .line 107
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 108
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 104
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signatureAvailableOptions:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/-$$Lambda$SignatureAndReceiptSettingsView$pf4eF-7G5jEVI5pq9uPVWKYKrRo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/-$$Lambda$SignatureAndReceiptSettingsView$pf4eF-7G5jEVI5pq9uPVWKYKrRo;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 116
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/-$$Lambda$SignatureAndReceiptSettingsView$RFf0oxiDptnqdJDwVdu2H6S9sP0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/-$$Lambda$SignatureAndReceiptSettingsView$RFf0oxiDptnqdJDwVdu2H6S9sP0;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->showSignOnDeviceHint()V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureGroup:Lcom/squareup/widgets/CheckableGroup;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/-$$Lambda$SignatureAndReceiptSettingsView$LfdSUmPRPya_HUjffJB9H25xjTM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/-$$Lambda$SignatureAndReceiptSettingsView$LfdSUmPRPya_HUjffJB9H25xjTM;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->quickTipHint:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/settingsapplet/R$string;->paper_signature_quick_tip_hint:I

    const-string v3, "learn_more"

    .line 128
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->paper_signature_help_url:I

    .line 129
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->learn_more:I

    .line 130
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 131
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 127
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->quickTipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$4;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->traditionalReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$5;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->additionalAuthSlipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/-$$Lambda$SignatureAndReceiptSettingsView$2TU7ixuDJ4wDT9FnoZHu948EuE0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/-$$Lambda$SignatureAndReceiptSettingsView$2TU7ixuDJ4wDT9FnoZHu948EuE0;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 152
    new-instance v0, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->signOnPrintedReceiptWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 155
    new-instance v0, Lcom/squareup/caller/FailurePopup;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/caller/FailurePopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptWithoutPrinterPopup:Lcom/squareup/caller/FailurePopup;

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->signOnPrintedReceiptWithoutPrinterPopup:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnPrintedReceiptWithoutPrinterPopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 159
    new-instance v0, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 162
    new-instance v0, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->alwaysSkipSignatureWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->alwaysSkipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->alwaysSkipSignatureWarningPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 170
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setAdditionalAuthSlipEnabled(Z)V
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->additionalAuthSlipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setAlwaysSkipSignatureChecked()V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->alwaysSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setChecked(Landroid/widget/Checkable;)V

    return-void
.end method

.method setNeverSkipSignatureChecked()V
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->neverSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setChecked(Landroid/widget/Checkable;)V

    return-void
.end method

.method public setQuickTipEnabled(Z)V
    .locals 2

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->traditionalReceiptSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->quickTipSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public setSignOnPrintedReceipt(ZZZ)V
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureGroup:Lcom/squareup/widgets/CheckableGroup;

    if-eqz p1, :cond_0

    sget v1, Lcom/squareup/settingsapplet/R$id;->sign_on_printed_receipt:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$id;->sign_on_device:I

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureReceiptOptions:Landroid/view/View;

    invoke-static {v0, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 274
    iget-object p2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->additionalAuthSlipContainer:Landroid/view/View;

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 275
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->showOrHidePrinterConfigurationWarning(Z)V

    return-void
.end method

.method public setSignatureDetailsSettingsVisible(Z)V
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureOptions:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 290
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->updateDividers()V

    return-void
.end method

.method public setSignatureSettingsVisible(Z)V
    .locals 1

    .line 279
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signatureAvailableOptions:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountTooltip:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 285
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->updateDividers()V

    return-void
.end method

.method setSkipReceiptScreen(Z)V
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setSkipReceiptScreenEnabled(Z)V
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenHint:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 253
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->updateDividers()V

    return-void
.end method

.method setSkipReceiptScreenHintText(I)V
    .locals 3

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenHint:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "learn_more"

    .line 208
    invoke-virtual {v1, p1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->skip_receipt_screen_url:I

    .line 209
    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->learn_more:I

    .line 210
    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 211
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    .line 207
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSkipSignUnderAmount(Z)V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setUnderAmountSkipSignatureChecked()V
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setChecked(Landroid/widget/Checkable;)V

    return-void
.end method

.method showSignOnDeviceHint()V
    .locals 2

    .line 174
    sget v0, Lcom/squareup/settingsapplet/R$string;->paper_signature_sign_on_device_hint:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSignOnDeviceHint(I)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnDeviceHint:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showSignOnDeviceHintAu()V
    .locals 2

    .line 179
    sget v0, Lcom/squareup/settingsapplet/R$string;->paper_signature_sign_on_device_hint_au:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSignOnDeviceHint(I)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signOnDeviceHint:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showSkipSignUnderAmount(Ljava/lang/String;)V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->underAmountSkipSignatureButton:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showSkipSignUnderAmount(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountTooltip:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setEnabled(Z)V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountTooltip:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method updateDividers()V
    .locals 5

    .line 258
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureEnabled()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->signatureAvailableOptions:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipSignatureUnderAmountSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 264
    :goto_1
    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureDivider:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureOptions:Landroid/view/View;

    .line 265
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    .line 264
    :goto_2
    invoke-static {v3, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 266
    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenDivider:Landroid/view/View;

    iget-object v4, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->skipReceiptScreenSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->paperSignatureOptions:Landroid/view/View;

    .line 267
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    .line 266
    :cond_4
    :goto_3
    invoke-static {v3, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
