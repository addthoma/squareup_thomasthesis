.class Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "SignatureAndReceiptSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/Confirmation;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->access$000(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;Z)V

    .line 104
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->access$100(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;Lcom/squareup/analytics/RegisterActionName;Z)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 101
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$1;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
