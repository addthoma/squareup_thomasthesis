.class final enum Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;
.super Ljava/lang/Enum;
.source "MerchantProfileEditLogoScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ImageStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

.field public static final enum FAILED:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

.field public static final enum LOADED:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

.field public static final enum LOADING:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 55
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    const/4 v1, 0x0

    const-string v2, "LOADING"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->LOADING:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    const/4 v2, 0x1

    const-string v3, "LOADED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->LOADED:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    const/4 v3, 0x2

    const-string v4, "FAILED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->FAILED:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    .line 54
    sget-object v4, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->LOADING:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->LOADED:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->FAILED:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->$VALUES:[Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;
    .locals 1

    .line 54
    const-class v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->$VALUES:[Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    return-object v0
.end method
