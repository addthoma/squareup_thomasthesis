.class Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "MerchantProfileBusinessAddressView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;

    iget-object p1, p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->access$000(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;

    invoke-static {v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->access$100(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)Lcom/squareup/address/AddressLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/address/AddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->onSave(ZLcom/squareup/address/Address;)V

    return-void
.end method
