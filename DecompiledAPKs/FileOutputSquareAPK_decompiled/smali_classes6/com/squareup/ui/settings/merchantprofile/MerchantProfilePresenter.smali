.class Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "MerchantProfilePresenter.java"

# interfaces
.implements Lcom/squareup/camerahelper/CameraHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;",
        ">;",
        "Lcom/squareup/camerahelper/CameraHelper$Listener;"
    }
.end annotation


# static fields
.field private static final FACEBOOK_PATTERN:Ljava/util/regex/Pattern;

.field static final MIN_FEATURED_IMAGE_HEIGHT:I = 0x122

.field static final MIN_FEATURED_IMAGE_WIDTH:I = 0x244

.field private static final REQUEST_EDIT_FEATURED_IMAGE:Ljava/lang/String; = "REQUEST_EDIT_FEATURED_IMAGE"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private final application:Landroid/app/Application;

.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

.field private final curatedImage:Lcom/squareup/merchantimages/CuratedImage;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field loaded:Z

.field private final merchantEditPhotoPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private final merchantProfileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

.field private final monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

.field private requestEditFeaturedImage:Z

.field private requiresBusinessAddressValidation:Z

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

.field private final state:Lcom/squareup/merchantprofile/MerchantProfileState;

.field private final subscriptions:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "(https?://)?(www\\.)?facebook\\.com/.+"

    const/4 v1, 0x2

    .line 75
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->FACEBOOK_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/merchantprofile/MerchantProfileState;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/app/NotificationManager;Landroid/app/Application;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/util/AppNameFormatter;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 141
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 81
    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V

    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->merchantEditPhotoPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 118
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    .line 127
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    move-object v1, p2

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    move-object v1, p3

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    move-object v1, p4

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p5

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p6

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->notificationManager:Landroid/app/NotificationManager;

    move-object v1, p7

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->application:Landroid/app/Application;

    move-object v1, p8

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    move-object v1, p9

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->merchantProfileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    move-object v1, p10

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    move-object v1, p12

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    move-object v1, p11

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    .line 153
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->flow:Lflow/Flow;

    move-object v1, p13

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    move-object/from16 v1, p14

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p15

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p16

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Lcom/squareup/camerahelper/CameraHelper;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->requestEditFeaturedImage:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->clearMerchantLogoState()V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Ljava/lang/Object;
    .locals 0

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->clearFeaturedImageState()V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Ljava/lang/Object;
    .locals 0

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private canPublishOrNotInMarket()Z
    .locals 2

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-boolean v0, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->appearInMarket:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileState;->hasLogo()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->hasAddress()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v0, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private clearFeaturedImageState()V
    .locals 2

    .line 443
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImage:Landroid/net/Uri;

    const/4 v1, 0x0

    .line 444
    iput v1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageWidth:I

    .line 445
    iput v1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageHeight:I

    return-void
.end method

.method private clearMerchantLogoState()V
    .locals 2

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->setLogo(Ljava/io/File;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private goBack()V
    .locals 5

    .line 411
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    sget-object v2, Lcom/squareup/protos/checklist/common/ActionItemName;->UPDATE_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/common/ActionItemName;

    sget-object v3, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    new-instance v4, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$c2sROFSSoZ-xuskxIK-iTXsxydE;

    invoke-direct {v4, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$c2sROFSSoZ-xuskxIK-iTXsxydE;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V

    .line 412
    invoke-interface {v1, v2, v3, v4}, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;->handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object v1

    .line 416
    invoke-virtual {v1}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 411
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private hasAddress()Z
    .locals 2

    .line 429
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object v0

    .line 430
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->isMobileBusiness()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    .line 432
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    .line 433
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    .line 434
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    .line 435
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isAddressValid(Lcom/squareup/address/Address;)Z
    .locals 2

    if-eqz p1, :cond_0

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Lcom/squareup/settings/server/Features$Feature;

    .line 294
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {p1}, Lcom/squareup/address/Address;->isCompleteAndWithoutPoBox()Z

    move-result p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/address/Address;->isComplete()Z

    move-result p1

    :goto_0
    return p1
.end method

.method static isSupportedImageType(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "image/png"

    .line 532
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "image/jpeg"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static synthetic lambda$j8XqOqV90zDd74svsjln1fVCdO0(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->refreshBusinessLogo(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private loadState()V
    .locals 4

    .line 481
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->merchantProfileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    .line 483
    invoke-interface {v1}, Lcom/squareup/merchantprofile/MerchantProfileUpdater;->getMerchantProfile()Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    .line 484
    invoke-interface {v2}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->addressRequiresValidation()Lio/reactivex/Observable;

    move-result-object v2

    .line 485
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v3

    .line 482
    invoke-static {v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 486
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$kdbXtCzlZmorQCSBNnw1AiNPeHI;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$kdbXtCzlZmorQCSBNnw1AiNPeHI;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V

    .line 487
    invoke-static {v2}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 481
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private onLoadMerchantProfileState(Lcom/squareup/server/account/MerchantProfileResponse;)V
    .locals 17

    move-object/from16 v0, p0

    .line 502
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->hasView()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 505
    iput-boolean v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loaded:Z

    .line 506
    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v2, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantProfileSettings()Lcom/squareup/settings/server/MerchantProfileSettings;

    move-result-object v2

    move-object/from16 v3, p1

    invoke-virtual {v1, v3, v2}, Lcom/squareup/merchantprofile/MerchantProfileState;->loadFrom(Lcom/squareup/server/account/MerchantProfileResponse;Lcom/squareup/settings/server/MerchantProfileSettings;)V

    .line 508
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->isMobileBusiness()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-boolean v4, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->photoOnReceipt:Z

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    .line 509
    invoke-virtual {v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getLocalLogo()Ljava/io/File;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v7, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->profileImageUrl:Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v8, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageUrl:Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v9, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageWidth:I

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v10, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageHeight:I

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v11, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImage:Landroid/net/Uri;

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v12, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageWidth:I

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v13, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageHeight:I

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v14, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->businessName:Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v1, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    if-nez v1, :cond_1

    sget-object v1, Lcom/squareup/merchantprofile/ContactInfo;->EMPTY:Lcom/squareup/merchantprofile/ContactInfo;

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v1, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    :goto_0
    move-object v15, v1

    iget-object v1, v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v1, v1, Lcom/squareup/merchantprofile/MerchantProfileState;->description:Ljava/lang/String;

    move-object/from16 v16, v1

    .line 508
    invoke-virtual/range {v2 .. v16}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setContent(ZZLcom/squareup/address/Address;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;IILjava/lang/String;Lcom/squareup/merchantprofile/ContactInfo;Ljava/lang/String;)V

    return-void
.end method

.method private onMerchantProfileStateLoadFailure()V
    .locals 1

    .line 524
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 527
    iput-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loaded:Z

    .line 528
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setErrorContent()V

    return-void
.end method

.method private refreshBusinessAddress(Lcom/squareup/address/Address;)V
    .locals 1

    .line 520
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setBusinessAddress(Lcom/squareup/address/Address;)V

    return-void
.end method

.method private refreshBusinessLogo(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 516
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getLocalLogo()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v2, v2, Lcom/squareup/merchantprofile/MerchantProfileState;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setMerchantLogoBitmap(Ljava/io/File;Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void
.end method

.method private showValidationError()Z
    .locals 1

    .line 449
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->isFacebookFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->resolveFacebookAutofill()V

    .line 453
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->hasValidFacebookURL()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->hasValidEmail()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->canPublishOrNotInMarket()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return v0

    .line 454
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->sendErrorNotification()V

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method businessAddressChanged(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/address/Address;",
            ">;)V"
        }
    .end annotation

    .line 201
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setMobileBusinessContent(Z)V

    .line 202
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/address/Address;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->refreshBusinessAddress(Lcom/squareup/address/Address;)V

    return-void
.end method

.method businessNameChanged(Ljava/lang/String;)V
    .locals 1

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iput-object p1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessName:Ljava/lang/String;

    return-void
.end method

.method contactInfoChanged(Lcom/squareup/merchantprofile/ContactInfo;)V
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iput-object p1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    return-void
.end method

.method descriptionChanged(Ljava/lang/String;)V
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iput-object p1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->description:Ljava/lang/String;

    return-void
.end method

.method public bridge synthetic dropView(Landroid/view/ViewGroup;)V
    .locals 0

    .line 67
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->dropView(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    return-void
.end method

.method public dropView(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V
    .locals 2

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 212
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0, p0}, Lcom/squareup/camerahelper/CameraHelper;->dropListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->merchantEditPhotoPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getMerchantEditPhotoPopup()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 216
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->dropView(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 67
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->dropView(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    return-void
.end method

.method editBusinessAddressClicked()V
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logEditAddress(Lcom/squareup/analytics/Analytics;)V

    .line 338
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;->INSTANCE:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method featuredImageChosen(Landroid/net/Uri;IILjava/lang/String;)V
    .locals 7

    .line 352
    invoke-static {p4}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->isSupportedImageType(Ljava/lang/String;)Z

    move-result p4

    if-nez p4, :cond_0

    .line 353
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->showFeaturedImageWrongMimeTypeError()V

    return-void

    :cond_0
    const/16 p4, 0x244

    if-lt p2, p4, :cond_2

    const/16 p4, 0x122

    if-ge p3, p4, :cond_1

    goto :goto_0

    .line 362
    :cond_1
    iget-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iput-object p1, p4, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImage:Landroid/net/Uri;

    .line 363
    iput p2, p4, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageWidth:I

    .line 364
    iput p3, p4, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageHeight:I

    .line 365
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object p4

    move-object v0, p4

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    iget-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v1, p4, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageUrl:Ljava/lang/String;

    iget-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v2, p4, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageWidth:I

    iget-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v3, p4, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageHeight:I

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setFeaturedImage(Ljava/lang/String;IILandroid/net/Uri;II)V

    return-void

    .line 358
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->showFeaturedImageTooSmallError()V

    return-void
.end method

.method featuredImageClicked()V
    .locals 3

    const/4 v0, 0x1

    .line 347
    iput-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->requestEditFeaturedImage:Z

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->merchantEditPhotoPresenter:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;

    sget-object v2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->ADDING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;)V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method hasValidEmail()Z
    .locals 2

    .line 378
    iget-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loaded:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v0, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v0, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v0, v0, Lcom/squareup/merchantprofile/ContactInfo;->email:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v0, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v0, v0, Lcom/squareup/merchantprofile/ContactInfo;->email:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method hasValidFacebookURL()Z
    .locals 3

    .line 370
    iget-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loaded:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v0, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v0, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v0, v0, Lcom/squareup/merchantprofile/ContactInfo;->facebook:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->FACEBOOK_PATTERN:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v2, v2, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v2, v2, Lcom/squareup/merchantprofile/ContactInfo;->facebook:Ljava/lang/String;

    .line 374
    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method public synthetic lambda$goBack$2$MerchantProfilePresenter()Lkotlin/Unit;
    .locals 1

    .line 414
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 415
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$loadState$3$MerchantProfilePresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 488
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 489
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 490
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/MerchantProfileResponse;

    .line 489
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->onLoadMerchantProfileState(Lcom/squareup/server/account/MerchantProfileResponse;)V

    .line 491
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 492
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->onAddressValidationRequired()V

    goto :goto_0

    .line 495
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->onMerchantProfileStateLoadFailure()V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$onImagePicked$0$MerchantProfilePresenter(Landroid/net/Uri;IILjava/lang/String;)V
    .locals 1

    .line 252
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->featuredImageChosen(Landroid/net/Uri;IILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$saveProfile$1$MerchantProfilePresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 394
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->requiresBusinessAddressValidation:Z

    if-eqz p1, :cond_0

    .line 398
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    invoke-interface {p1}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->setBusinessAddressVerified()V

    :cond_0
    return-void
.end method

.method merchantPhotoClicked()V
    .locals 3

    const/4 v0, 0x0

    .line 315
    iput-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->requestEditFeaturedImage:Z

    .line 316
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->merchantEditPhotoPresenter:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;

    sget-object v2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->ADDING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;)V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method mobileBusinessChanged(Z)V
    .locals 2

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->setMobileBusiness(Ljava/lang/Boolean;)V

    if-eqz p1, :cond_0

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logMobileBusiness(Lcom/squareup/analytics/Analytics;)V

    goto :goto_0

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logNotMobileBusiness(Lcom/squareup/analytics/Analytics;)V

    .line 284
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_BADGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    .line 286
    invoke-virtual {p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->isAddressValid(Lcom/squareup/address/Address;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 287
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    invoke-interface {p1}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->setBusinessAddressUnverified()V

    .line 288
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->onAddressValidationRequired()V

    :cond_1
    return-void
.end method

.method onAddressValidationRequired()V
    .locals 2

    const/4 v0, 0x1

    .line 342
    iput-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->requiresBusinessAddressValidation:Z

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;->INSTANCE:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    .line 264
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 265
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->goBack()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 161
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 162
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileState;->businessAddress()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$llxqmnuifnx6HwcQPm9aJ78EguM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$llxqmnuifnx6HwcQPm9aJ78EguM;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    .line 221
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->saveProfile()V

    .line 222
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->clearMerchantLogoState()V

    .line 223
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->clearFeaturedImageState()V

    return-void
.end method

.method public onImageCanceled()V
    .locals 0

    return-void
.end method

.method public onImagePicked(Landroid/net/Uri;)V
    .locals 2

    .line 243
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 247
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->requestEditFeaturedImage:Z

    if-nez v0, :cond_1

    const-string v0, "image"

    .line 248
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 251
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$X2Q1LS9vM1ueajofiyHqsPpOZzU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$X2Q1LS9vM1ueajofiyHqsPpOZzU;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;Landroid/net/Uri;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->decodeImageDimensions(Landroid/net/Uri;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 11

    .line 167
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    const-string v1, "REQUEST_EDIT_FEATURED_IMAGE"

    .line 169
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->requestEditFeaturedImage:Z

    .line 172
    iget-boolean p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loaded:Z

    if-eqz p1, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getLocalLogo()Ljava/io/File;

    move-result-object v1

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v2, p1, Lcom/squareup/merchantprofile/MerchantProfileState;->profileImageUrl:Ljava/lang/String;

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v3, p1, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageUrl:Ljava/lang/String;

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v4, p1, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageWidth:I

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v5, p1, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageHeight:I

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v6, p1, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImage:Landroid/net/Uri;

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v7, p1, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageWidth:I

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iget v8, p1, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageHeight:I

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    .line 176
    invoke-virtual {p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->isMobileBusiness()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object v10

    .line 173
    invoke-virtual/range {v0 .. v10}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->restoreContent(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;IIZLcom/squareup/address/Address;)V

    .line 180
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->merchantEditPhotoPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getMerchantEditPhotoPopup()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 181
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {p1, p0}, Lcom/squareup/camerahelper/CameraHelper;->setListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileState;->tempLogoBitmap()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$j8XqOqV90zDd74svsjln1fVCdO0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$j8XqOqV90zDd74svsjln1fVCdO0;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 187
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0}, Lcom/squareup/camerahelper/CameraHelper;->isCameraOrGalleryAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setFeaturedClickListener()V

    .line 190
    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->showChangeLogoContainer()V

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->hideChangeLogoContainer()V

    .line 195
    :goto_0
    iget-boolean p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loaded:Z

    if-nez p1, :cond_2

    .line 196
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loadState()V

    :cond_2
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 206
    iget-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->requestEditFeaturedImage:Z

    const-string v1, "REQUEST_EDIT_FEATURED_IMAGE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 207
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method public onUpPressedAction()V
    .locals 0

    .line 231
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->goBack()V

    return-void
.end method

.method photoOnReceiptChanged(Z)V
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iput-boolean p1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->photoOnReceipt:Z

    return-void
.end method

.method retryClicked()V
    .locals 0

    .line 270
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loadState()V

    return-void
.end method

.method saveProfile()V
    .locals 3

    .line 386
    iget-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->loaded:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->showValidationError()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileState;->toSnapshot()Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    move-result-object v0

    .line 392
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->merchantProfileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    invoke-interface {v1, v0}, Lcom/squareup/merchantprofile/MerchantProfileUpdater;->updateProfile(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$Glm0e2Kdzmwl8pWqzqzu68fvfE0;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfilePresenter$Glm0e2Kdzmwl8pWqzqzu68fvfE0;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V

    .line 393
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 402
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantProfileSettings()Lcom/squareup/settings/server/MerchantProfileSettings;

    move-result-object v1

    iget-boolean v2, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->photoOnReceipt:Z

    invoke-virtual {v1, v2}, Lcom/squareup/settings/server/MerchantProfileSettings;->set(Z)V

    .line 404
    iget-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImage:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 406
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iget-object v0, v0, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->pendingFeaturedImage:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/merchantimages/CuratedImage;->setLocalUriOverride(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 227
    const-class v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen;

    return-object v0
.end method

.method protected sendErrorNotification()V
    .locals 4

    .line 463
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v1, Lcom/squareup/merchantprofile/R$string;->merchant_profile_saving_profile_failed_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 466
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->application:Landroid/app/Application;

    sget-object v3, Lcom/squareup/notification/Channels;->MERCHANT_PROFILE:Lcom/squareup/notification/Channels;

    .line 467
    invoke-virtual {v1, v2, v3}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 468
    invoke-virtual {v1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/merchantprofile/R$string;->merchant_profile_saving_profile_failed_text:I

    .line 469
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->application:Landroid/app/Application;

    const-string v2, "SETTINGS"

    .line 472
    invoke-static {v1, v2}, Lcom/squareup/ui/main/PosIntentParser;->createPendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 471
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 474
    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 476
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->notificationManager:Landroid/app/NotificationManager;

    sget v2, Lcom/squareup/merchantprofile/R$id;->notification_profile_error:I

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method showFeaturedImageTooSmallError()V
    .locals 4

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_featured_image_error_small_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 321
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_featured_image_error_small:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "min_width"

    const/16 v3, 0x244

    .line 322
    invoke-virtual {v1, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "min_height"

    const/16 v3, 0x122

    .line 323
    invoke-virtual {v1, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 324
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 325
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 326
    new-instance v2, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-direct {v2, v0, v1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method showFeaturedImageWrongMimeTypeError()V
    .locals 3

    .line 331
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_featured_image_error_wrong_type_title:I

    sget v2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_featured_image_error_wrong_type:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 333
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
