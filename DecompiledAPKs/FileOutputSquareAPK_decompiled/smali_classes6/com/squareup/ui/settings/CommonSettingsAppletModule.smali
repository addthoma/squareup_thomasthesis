.class public abstract Lcom/squareup/ui/settings/CommonSettingsAppletModule;
.super Ljava/lang/Object;
.source "CommonSettingsAppletModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideFeesEditor(Lcom/squareup/settings/server/RealFeesEditor;)Lcom/squareup/settings/server/FeesEditor;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 20
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealFeesEditor;->setInForeground()V

    return-object p0
.end method


# virtual methods
.method abstract settingsAppletClientActionTranslator(Lcom/squareup/ui/settings/SettingsAppletClientActionTranslator;)Lcom/squareup/clientactiontranslation/ClientActionTranslator;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
