.class Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "TipSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/tipping/TipSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final customTipEntryPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field customTips:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private lastEditedTipIndex:I

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

.field private final x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 63
    new-instance p1, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;-><init>(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTipEntryPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    const/4 p1, -0x1

    .line 86
    iput p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->lastEditedTipIndex:I

    .line 88
    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/util/Percentage;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    .line 95
    iput-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 96
    iput-object p3, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 97
    iput-object p4, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 98
    iput-object p5, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 99
    iput-object p6, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 100
    iput-object p7, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 101
    iput-object p8, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)I
    .locals 0

    .line 61
    iget p0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->lastEditedTipIndex:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private onChanged(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)Lkotlin/Unit;
    .locals 3

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getTipsEnabled()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getTipsEnabled()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/fastcheckout/CollectTipsToggleEvent;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getTipsEnabled()Z

    move-result v2

    invoke-direct {v1, v2}, Lcom/squareup/log/fastcheckout/CollectTipsToggleEvent;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 191
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    .line 192
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->saveSettings()V

    .line 193
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/tipping/TipSettingsView;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/tipping/TipSettingsView;->update(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)V

    .line 194
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method


# virtual methods
.method public bridge synthetic dropView(Landroid/view/ViewGroup;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/settings/tipping/TipSettingsView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->dropView(Lcom/squareup/ui/settings/tipping/TipSettingsView;)V

    return-void
.end method

.method public dropView(Lcom/squareup/ui/settings/tipping/TipSettingsView;)V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTipEntryPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView;->getEnterCustomTipAmountPopup()Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 183
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->dropView(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/settings/tipping/TipSettingsView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->dropView(Lcom/squareup/ui/settings/tipping/TipSettingsView;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/tipping/TippingSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$0$TipSettingsScreen$Presenter(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)Lkotlin/Unit;
    .locals 0

    .line 136
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->onChanged(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$1$TipSettingsScreen$Presenter(Ljava/lang/Integer;)Lkotlin/Unit;
    .locals 0

    .line 137
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->onCustomTipClicked(I)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method onCustomTipClicked(I)Lkotlin/Unit;
    .locals 5

    .line 199
    iput p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->lastEditedTipIndex:I

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Percentage;

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTipEntryPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;

    invoke-static {p1}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p1

    sget v2, Lcom/squareup/registerlib/R$string;->tip_amount:I

    const-string v3, "0"

    const/4 v4, 0x2

    invoke-direct {v1, p1, v3, v2, v4}, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 203
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 13

    .line 105
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 110
    iget-object v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    const-string v2, "custom-tips"

    .line 111
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 112
    iget-object v3, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    invoke-static {v2}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->getCustomPercentages()Ljava/util/List;

    move-result-object p1

    const/4 v2, 0x0

    .line 116
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 117
    iget-object v3, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 122
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    const/4 v2, 0x1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 123
    invoke-virtual {p1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 124
    invoke-interface {p1}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v9, 0x1

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v9, 0x0

    .line 128
    :goto_3
    new-instance p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    .line 129
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v4

    iget-object v5, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    .line 131
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomPercentages()Z

    move-result v1

    xor-int/lit8 v6, v1, 0x1

    .line 132
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts()Z

    move-result v7

    .line 133
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingSeparateTippingScreen()Z

    move-result v8

    .line 135
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingTipPreTax()Z

    move-result v10

    new-instance v11, Lcom/squareup/ui/settings/tipping/-$$Lambda$TipSettingsScreen$Presenter$7KZXC65KjHXarSkuadEdTxdG3lw;

    invoke-direct {v11, p0}, Lcom/squareup/ui/settings/tipping/-$$Lambda$TipSettingsScreen$Presenter$7KZXC65KjHXarSkuadEdTxdG3lw;-><init>(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)V

    new-instance v12, Lcom/squareup/ui/settings/tipping/-$$Lambda$TipSettingsScreen$Presenter$rcdf7TcOW5fRCDQRRhpv447WHFo;

    invoke-direct {v12, p0}, Lcom/squareup/ui/settings/tipping/-$$Lambda$TipSettingsScreen$Presenter$rcdf7TcOW5fRCDQRRhpv447WHFo;-><init>(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)V

    move-object v3, p1

    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;-><init>(ZLjava/util/List;ZZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/tipping/TipSettingsView;

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/tipping/TipSettingsView;->update(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTipEntryPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView;->getEnterCustomTipAmountPopup()Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Percentage;

    .line 153
    invoke-virtual {v2}, Lcom/squareup/util/Percentage;->stringSnapshot()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v1, "custom-tips"

    .line 155
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 156
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method protected saveSettings()V
    .locals 7

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    .line 171
    invoke-virtual {v1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getTipsEnabled()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    .line 172
    invoke-virtual {v2}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getSmartTippingEnabled()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    .line 173
    invoke-virtual {v3}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getCustomAmounts()Z

    move-result v3

    iget-object v4, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    .line 174
    invoke-virtual {v4}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getSeparateTippingScreen()Z

    move-result v4

    iget-object v5, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    .line 175
    invoke-virtual {v5}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getTipPreTax()Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;->PRE_TAX_TIP_CALCULATION:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    goto :goto_0

    :cond_0
    sget-object v5, Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;->POST_TAX_TIP_CALCULATION:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    :goto_0
    iget-object v6, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    .line 170
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/settings/server/AccountStatusSettings;->setTipSettings(ZZZZLcom/squareup/settings/server/TipSettings$TippingCalculationPhase;Ljava/util/List;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 160
    const-class v0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen;

    return-object v0
.end method
