.class public final Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "TipSettingsScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final coreParametersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final sidebarRefresherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;)",
            "Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;"
        }
    .end annotation

    .line 69
    new-instance v9, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;
    .locals 10

    .line 77
    new-instance v9, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;
    .locals 9

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/settings/SidebarRefresher;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
