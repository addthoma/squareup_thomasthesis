.class public final Lcom/squareup/ui/settings/tipping/TipSettingsView;
.super Landroid/widget/LinearLayout;
.source "TipSettingsView.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/HasActionBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;,
        Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0002$%B\u0017\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0014\u001a\n \u0016*\u0004\u0018\u00010\u00150\u0015H\u0016J\u0008\u0010\u0017\u001a\u00020\u0018H\u0014J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u0018H\u0014J\u0008\u0010\u001c\u001a\u00020\u0018H\u0014J\u0010\u0010\u001d\u001a\u00020\u00182\u0006\u0010\u001e\u001a\u00020\u001fH\u0014J\n\u0010 \u001a\u0004\u0018\u00010\u001fH\u0014J\u000e\u0010!\u001a\u00020\u00182\u0006\u0010\"\u001a\u00020#R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/ui/settings/tipping/TipSettingsView;",
        "Landroid/widget/LinearLayout;",
        "Lcom/squareup/workflow/ui/HandlesBack;",
        "Lcom/squareup/ui/HasActionBar;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "enterCustomTipAmountPopup",
        "Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;",
        "getEnterCustomTipAmountPopup",
        "()Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "presenter",
        "Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;",
        "root",
        "Lcom/squareup/mosaic/core/Root;",
        "getActionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "kotlin.jvm.PlatformType",
        "onAttachedToWindow",
        "",
        "onBackPressed",
        "",
        "onDetachedFromWindow",
        "onFinishInflate",
        "onRestoreInstanceState",
        "state",
        "Landroid/os/Parcelable;",
        "onSaveInstanceState",
        "update",
        "rendering",
        "Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;",
        "EnterCustomTipAmountPopup",
        "Rendering",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enterCustomTipAmountPopup:Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

.field public localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public presenter:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private root:Lcom/squareup/mosaic/core/Root;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    const-class p2, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/tipping/TipSettingsView;)V

    .line 101
    new-instance p2, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->localeProvider:Ljavax/inject/Provider;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p2, p1, v0}, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;-><init>(Landroid/content/Context;Ljavax/inject/Provider;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->enterCustomTipAmountPopup:Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 149
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public final getEnterCustomTipAmountPopup()Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->enterCustomTipAmountPopup:Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 113
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->presenter:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 127
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 v0, 0x0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->presenter:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->dropView(Lcom/squareup/ui/settings/tipping/TipSettingsView;)V

    .line 123
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 108
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 109
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->root:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/core/Root;

    iput-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->root:Lcom/squareup/mosaic/core/Root;

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    instance-of v0, p1, Lcom/squareup/mortar/BundleSavedState;

    if-eqz v0, :cond_0

    .line 142
    check-cast p1, Lcom/squareup/mortar/BundleSavedState;

    invoke-virtual {p1}, Lcom/squareup/mortar/BundleSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->enterCustomTipAmountPopup:Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

    iget-object p1, p1, Lcom/squareup/mortar/BundleSavedState;->bundle:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->onRestoreInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    .line 145
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 132
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->enterCustomTipAmountPopup:Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->enterCustomTipAmountPopup:Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "enterCustomTipAmountPopup.onSaveInstanceState()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    new-instance v2, Lcom/squareup/mortar/BundleSavedState;

    invoke-direct {v2, v0, v1}, Lcom/squareup/mortar/BundleSavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    check-cast v2, Landroid/os/Parcelable;

    return-object v2

    :cond_0
    return-object v0
.end method

.method public final update(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)V
    .locals 2

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView;->root:Lcom/squareup/mosaic/core/Root;

    if-nez v0, :cond_0

    const-string v1, "root"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;-><init>(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/mosaic/core/Root;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
