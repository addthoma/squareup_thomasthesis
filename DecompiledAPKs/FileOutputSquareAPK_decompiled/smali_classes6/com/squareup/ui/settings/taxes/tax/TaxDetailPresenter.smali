.class public Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;
.super Lcom/squareup/ui/items/BaseEditObjectViewPresenter;
.source "TaxDetailPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/BaseEditObjectViewPresenter<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private availableTypes:Lcom/squareup/server/account/FeeTypes;

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final feesEditor:Lcom/squareup/settings/server/FeesEditor;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private showFeeTypes:Z

.field private taxCogsId:Ljava/lang/String;

.field private final taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

.field private final taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/FeesEditor;Lflow/Flow;Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 99
    invoke-direct {p0, p4, p2, p5}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 101
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 102
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    .line 103
    iput-object p5, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 104
    iput-object p6, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    .line 105
    iput-object p7, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    .line 106
    iput-object p8, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    .line 107
    iput-object p9, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method static buildApplicableItemCountText(Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 45
    invoke-static {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->numberOfAppliedItems(Lcom/squareup/ui/settings/taxes/tax/TaxState;)I

    move-result v0

    .line 48
    invoke-static {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->numberOfTotalItemsAvailable(Lcom/squareup/ui/settings/taxes/tax/TaxState;)I

    move-result p0

    if-lt v0, p0, :cond_0

    .line 49
    sget p0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_items_count_all:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 51
    sget p0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_items_count_none:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    if-ne v0, p0, :cond_2

    .line 53
    sget p0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_items_count_one:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 55
    :cond_2
    sget p0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_items_count_some:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "count"

    .line 56
    invoke-virtual {p0, p1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 57
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method static buildApplicableServiceCountText(Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 64
    invoke-static {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->numberOfAppliedServices(Lcom/squareup/ui/settings/taxes/tax/TaxState;)I

    move-result v0

    .line 67
    invoke-static {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->numberOfTotalServicesAvailable(Lcom/squareup/ui/settings/taxes/tax/TaxState;)I

    move-result p0

    if-lt v0, p0, :cond_0

    .line 68
    sget p0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_services_count_all:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 70
    sget p0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_services_count_none:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    if-ne v0, p0, :cond_2

    .line 72
    sget p0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_services_count_one:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 74
    :cond_2
    sget p0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_services_count_some:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "count"

    .line 75
    invoke-virtual {p0, p1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 76
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private getPricingText(Lcom/squareup/api/items/Fee$InclusionType;)Ljava/lang/CharSequence;
    .locals 2

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$array;->fee_inclusion_types_short:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 315
    invoke-virtual {p1}, Lcom/squareup/api/items/Fee$InclusionType;->ordinal()I

    move-result p1

    aget-object p1, v0, p1

    return-object p1
.end method

.method private isValidTax()Z
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getPercentage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 310
    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->isPositive()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->isZero()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$iInQ7BAaqjHi5zkAgnt6b8RwVBQ(Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;Lcom/squareup/ui/settings/taxes/tax/TaxState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->onTaxStateUpdated(Lcom/squareup/ui/settings/taxes/tax/TaxState;)V

    return-void
.end method

.method private static numberOfAppliedItems(Lcom/squareup/ui/settings/taxes/tax/TaxState;)I
    .locals 0

    .line 319
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getItemCounts()Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->getApplicableItemCount()I

    move-result p0

    return p0
.end method

.method private static numberOfAppliedServices(Lcom/squareup/ui/settings/taxes/tax/TaxState;)I
    .locals 0

    .line 327
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getItemCounts()Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->getApplicableServiceCount()I

    move-result p0

    return p0
.end method

.method private static numberOfTotalItemsAvailable(Lcom/squareup/ui/settings/taxes/tax/TaxState;)I
    .locals 0

    .line 323
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getItemCounts()Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    move-result-object p0

    iget p0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->itemCount:I

    return p0
.end method

.method private static numberOfTotalServicesAvailable(Lcom/squareup/ui/settings/taxes/tax/TaxState;)I
    .locals 0

    .line 331
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getItemCounts()Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    move-result-object p0

    iget p0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->serviceCount:I

    return p0
.end method

.method private onTaxStateUpdated(Lcom/squareup/ui/settings/taxes/tax/TaxState;)V
    .locals 0

    .line 282
    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->hasTaxDataLoaded()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 288
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->hideProgressBar()V

    .line 289
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->bindView()V

    return-void
.end method

.method private onTaxValidationError(II)V
    .locals 3

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    invoke-direct {v2, p1, p2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private showValidationErrorIfNeeded()Z
    .locals 3

    .line 293
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->getCurrentName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 294
    sget v0, Lcom/squareup/settingsapplet/R$string;->tax_error_missing_name_title:I

    sget v2, Lcom/squareup/settingsapplet/R$string;->tax_error_missing_name_message:I

    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->onTaxValidationError(II)V

    return v1

    .line 297
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->isValidTax()Z

    move-result v0

    if-nez v0, :cond_1

    .line 298
    sget v0, Lcom/squareup/settingsapplet/R$string;->tax_error_missing_rate_title:I

    sget v2, Lcom/squareup/settingsapplet/R$string;->tax_error_missing_rate_message:I

    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->onTaxValidationError(II)V

    return v1

    :cond_1
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method bindView()V
    .locals 4

    .line 170
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;

    .line 171
    iget-boolean v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->showFeeTypes:Z

    if-eqz v1, :cond_0

    .line 172
    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->showFeeTypes()V

    .line 175
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->isNewObject()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 176
    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->hideRemoveTax()V

    .line 180
    :cond_1
    iget-boolean v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->showFeeTypes:Z

    if-eqz v1, :cond_3

    .line 181
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getFeeTypeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/server/account/FeeTypes;->withId(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 183
    iget-object v3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v3}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getFeeTypeId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "Ignoring invalid tax ID: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 185
    :cond_2
    iget-object v1, v1, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->setTypeRowValue(Ljava/lang/CharSequence;)V

    .line 189
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getPercentage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 191
    invoke-static {v1}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->setPercentageRowValue(Ljava/lang/CharSequence;)V

    .line 193
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->getCurrentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->setNameRowValue(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->setTaxEnabledRowChecked(Z)V

    .line 195
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->appliesToCustomAmounts()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->setCustomTaxEnabled(Z)V

    .line 196
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->buildApplicableItemCountText(Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->setApplicableItemsRowValue(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->buildApplicableServiceCountText(Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->setApplicableServicesRowValue(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->getPricingText(Lcom/squareup/api/items/Fee$InclusionType;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->setItemPricingRowValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method deleteTax()V
    .locals 4

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v0

    const-string v1, "deletedTax"

    .line 249
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 250
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v3, v2}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    invoke-interface {v1, v0, v3}, Lcom/squareup/settings/server/FeesEditor;->deleteTax(Lcom/squareup/shared/catalog/models/CatalogTax;Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCurrentName()Ljava/lang/String;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNewObject()Z
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->isNewTax()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onLoad$0$TaxDetailPresenter()Lrx/Subscription;
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailPresenter$iInQ7BAaqjHi5zkAgnt6b8RwVBQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailPresenter$iInQ7BAaqjHi5zkAgnt6b8RwVBQ;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method onApplicableItemsClicked()V
    .locals 3

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->setApplicableDisplayForService(Z)V

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxCogsId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onApplicableServiceClicked()V
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SERVICES_TAX:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->setApplicableDisplayForService(Z)V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxCogsId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    .line 202
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->showConfirmDiscardDialogOrFinish()V

    const/4 v0, 0x1

    return v0
.end method

.method onCustomAmountRowClicked(Z)V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setAppliesToCustomAmounts(Z)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    return-void
.end method

.method onEditFeeTypeClicked()V
    .locals 3

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxCogsId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 111
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;

    .line 112
    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;->cogsTaxId:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxCogsId:Ljava/lang/String;

    return-void
.end method

.method onItemPricingClicked()V
    .locals 3

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxCogsId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 116
    invoke-super {p0, p1}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 118
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->isNewObject()Z

    move-result p1

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/settingsapplet/R$string;->tax_create_tax:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/settingsapplet/R$string;->tax_edit:I

    .line 119
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 120
    invoke-interface {v3, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$Gia2LNgPX7zcndN2FzxiEEMC2w0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$Gia2LNgPX7zcndN2FzxiEEMC2w0;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;)V

    .line 121
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    .line 122
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$0GDnKVUoq8XPtLZo6JoCaaXHUKQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$0GDnKVUoq8XPtLZo6JoCaaXHUKQ;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;)V

    .line 123
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 119
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 125
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->updatePrimaryButtonState()V

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getFeeTypes()Lcom/squareup/server/account/FeeTypes;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    invoke-virtual {p1}, Lcom/squareup/server/account/FeeTypes;->getTypes()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->showFeeTypes:Z

    .line 130
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailPresenter$tPwKoL-y2KE3IdVnIhKHNZGXREQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailPresenter$tPwKoL-y2KE3IdVnIhKHNZGXREQ;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onNameClicked()V
    .locals 8

    .line 254
    new-instance v7, Lcom/squareup/register/widgets/EditTextDialogFactory;

    sget v1, Lcom/squareup/flowlegacy/R$layout;->editor_dialog:I

    sget v2, Lcom/squareup/settingsapplet/R$id;->editor:I

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/print/R$string;->tax_name:I

    .line 257
    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->getCurrentName()Ljava/lang/String;

    move-result-object v5

    const/16 v3, 0x2000

    const-string v6, ""

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/widgets/EditTextDialogFactory;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxCogsId:Ljava/lang/String;

    invoke-direct {v1, v2, v7}, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;-><init>(Ljava/lang/String;Lcom/squareup/register/widgets/EditTextDialogFactory;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onPercentageRowClicked()V
    .locals 9

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getFeeTypeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/FeeTypes;->withId(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, v0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/util/Percentage;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v7, v0

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getPercentage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {v0, v1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v0

    .line 265
    new-instance v8, Lcom/squareup/register/widgets/EditTextDialogFactory;

    sget v2, Lcom/squareup/settingsapplet/R$layout;->editor_dialog_percentage:I

    sget v3, Lcom/squareup/settingsapplet/R$id;->editor:I

    const/4 v4, -0x1

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/settingsapplet/R$string;->tax_percentage_dialog_title:I

    .line 268
    invoke-interface {v1, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v6

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/register/widgets/EditTextDialogFactory;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxCogsId:Ljava/lang/String;

    invoke-direct {v1, v2, v8}, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;-><init>(Ljava/lang/String;Lcom/squareup/register/widgets/EditTextDialogFactory;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onProgressHidden()V
    .locals 1

    .line 165
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->showContent()V

    .line 166
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->bindView()V

    return-void
.end method

.method onSaveClicked()V
    .locals 3

    .line 157
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->showValidationErrorIfNeeded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->FEE:Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v0}, Lsquareup/items/merchant/CatalogObjectType;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->logEditCatalogObjectEvent(Ljava/lang/String;Z)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getTaxItemUpdater()Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/settings/server/FeesEditor;->writeTax(Lcom/squareup/shared/catalog/models/CatalogTax;Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :cond_0
    return-void
.end method

.method onTaxEnabledClicked(Z)V
    .locals 2

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->hasTaxDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz p1, :cond_0

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TAX_ENABLED:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TAX_DISABLED:Lcom/squareup/analytics/RegisterActionName;

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setEnabled(Z)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    :cond_1
    return-void
.end method

.method showConfirmDiscardDialogOrFinish()V
    .locals 3

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->hasTaxChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxCogsId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->clear()V

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method

.method supportsServices()Z
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v0

    return v0
.end method

.method public updatePrimaryButtonState()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->locationCountUnavailable()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    :cond_0
    return-void
.end method
