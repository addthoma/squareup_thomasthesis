.class final Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;
.super Ljava/lang/Object;
.source "TaxApplicableScreen.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ItemRelationshipCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Lcom/squareup/shared/catalog/TypedCursor<",
        "Lcom/squareup/shared/catalog/Related<",
        "Lcom/squareup/shared/catalog/models/CatalogItem;",
        "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private canceled:Z

.field final synthetic this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;)V
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$1;)V
    .locals 0

    .line 208
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;>;)V"
        }
    .end annotation

    .line 218
    iget-boolean v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->canceled:Z

    if-eqz v0, :cond_0

    .line 219
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/TypedCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->close()V

    goto :goto_0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->access$102(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;)Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->access$200(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->hideProgressBar()V

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    .line 224
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/TypedCursor;

    invoke-static {v0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->access$300(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;Lcom/squareup/shared/catalog/TypedCursor;)Ljava/util/List;

    move-result-object p1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    .line 226
    invoke-static {v0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->access$400(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->access$500(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->setItems(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 213
    iput-boolean v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->canceled:Z

    return-void
.end method
