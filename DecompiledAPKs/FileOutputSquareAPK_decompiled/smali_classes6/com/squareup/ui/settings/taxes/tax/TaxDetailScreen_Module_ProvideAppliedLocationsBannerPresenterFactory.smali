.class public final Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;
.super Ljava/lang/Object;
.source "TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final appliedLocationCountFetcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->module:Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAppliedLocationsBannerPresenter(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 0

    .line 50
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;->provideAppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->module:Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->provideAppliedLocationsBannerPresenter(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->get()Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object v0

    return-object v0
.end method
