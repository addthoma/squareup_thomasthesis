.class final enum Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;
.super Ljava/lang/Enum;
.source "TaxState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "GlobalAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

.field public static final enum APPLY:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 57
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    const/4 v1, 0x0

    const-string v2, "APPLY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->APPLY:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    .line 55
    sget-object v2, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->APPLY:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->$VALUES:[Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;
    .locals 1

    .line 55
    const-class v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->$VALUES:[Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    return-object v0
.end method
