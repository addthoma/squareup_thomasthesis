.class public Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;
.super Ljava/lang/Object;
.source "TaxDetailScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideAppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 44
    new-instance v7, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    sget-object v3, Lcom/squareup/ui/items/ItemEditingStringIds;->TAX:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;

    .line 45
    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;->isNewTax()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;ZZZ)V

    return-object v7
.end method
