.class public Lcom/squareup/ui/settings/taxes/TaxesListView;
.super Landroid/widget/LinearLayout;
.source "TaxesListView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;
    }
.end annotation


# instance fields
.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private listView:Landroid/widget/ListView;

.field presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

.field private taxAdapter:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const-class p2, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Component;->inject(Lcom/squareup/ui/settings/taxes/TaxesListView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 71
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method hideProgressBarToShowList()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$TaxesListView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->onTaxClicked(I)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$TaxesListView()V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->onProgressHidden()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 43
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 44
    sget v0, Lcom/squareup/settingsapplet/R$id;->taxes_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 45
    sget v0, Lcom/squareup/settingsapplet/R$id;->taxes_list_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->listView:Landroid/widget/ListView;

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/ui/settings/taxes/-$$Lambda$TaxesListView$rKRfsLGnitYvPzrf_7M80fXIJJE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/-$$Lambda$TaxesListView$rKRfsLGnitYvPzrf_7M80fXIJJE;-><init>(Lcom/squareup/ui/settings/taxes/TaxesListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v1, Lcom/squareup/ui/settings/taxes/-$$Lambda$TaxesListView$A4lwtVr2f2LiF5jX15Mv24svwZc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/-$$Lambda$TaxesListView$A4lwtVr2f2LiF5jX15Mv24svwZc;-><init>(Lcom/squareup/ui/settings/taxes/TaxesListView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method refreshList()V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->taxAdapter:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;-><init>(Lcom/squareup/ui/settings/taxes/TaxesListView;Lcom/squareup/ui/settings/taxes/TaxesListView$1;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->taxAdapter:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->taxAdapter:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->taxAdapter:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->notifyDataSetChanged()V

    return-void
.end method
