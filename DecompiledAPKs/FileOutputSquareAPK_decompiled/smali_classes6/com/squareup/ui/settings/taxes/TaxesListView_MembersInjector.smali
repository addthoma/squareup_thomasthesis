.class public final Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;
.super Ljava/lang/Object;
.source "TaxesListView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/taxes/TaxesListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/taxes/TaxesListView;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAnalytics(Lcom/squareup/ui/settings/taxes/TaxesListView;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/taxes/TaxesListView;Ljava/lang/Object;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/taxes/TaxesListView;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/taxes/TaxesListView;Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;->injectAnalytics(Lcom/squareup/ui/settings/taxes/TaxesListView;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/settings/taxes/TaxesListView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/taxes/TaxesListView;)V

    return-void
.end method
