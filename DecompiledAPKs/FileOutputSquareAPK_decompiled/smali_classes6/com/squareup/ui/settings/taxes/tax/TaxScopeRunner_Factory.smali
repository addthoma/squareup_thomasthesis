.class public final Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;
.super Ljava/lang/Object;
.source "TaxScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final appliedLocationCountFetcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final taxStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->taxStateProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p7, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;"
        }
    .end annotation

    .line 58
    new-instance v8, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/cogs/Cogs;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;"
        }
    .end annotation

    .line 64
    new-instance v8, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/util/Res;Ljavax/inject/Provider;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;
    .locals 8

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->taxStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Res;

    iget-object v7, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->newInstance(Lcom/squareup/cogs/Cogs;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner_Factory;->get()Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    move-result-object v0

    return-object v0
.end method
