.class public Lcom/squareup/widgets/PersistentViewAnimator;
.super Lcom/squareup/widgets/SquareViewAnimator;
.source "PersistentViewAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/PersistentViewAnimator$SavedState;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/widgets/SquareViewAnimator;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public isSaveEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .line 30
    check-cast p1, Lcom/squareup/widgets/PersistentViewAnimator$SavedState;

    .line 31
    invoke-virtual {p1}, Lcom/squareup/widgets/PersistentViewAnimator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/squareup/widgets/SquareViewAnimator;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 34
    invoke-virtual {p0}, Lcom/squareup/widgets/PersistentViewAnimator;->getInAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 35
    invoke-virtual {p0}, Lcom/squareup/widgets/PersistentViewAnimator;->getOutAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    const/4 v2, 0x0

    .line 36
    invoke-virtual {p0, v2}, Lcom/squareup/widgets/PersistentViewAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 37
    invoke-virtual {p0, v2}, Lcom/squareup/widgets/PersistentViewAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 40
    invoke-static {p1}, Lcom/squareup/widgets/PersistentViewAnimator$SavedState;->access$100(Lcom/squareup/widgets/PersistentViewAnimator$SavedState;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/PersistentViewAnimator;->setDisplayedChild(I)V

    .line 43
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/PersistentViewAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 44
    invoke-virtual {p0, v1}, Lcom/squareup/widgets/PersistentViewAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 26
    new-instance v0, Lcom/squareup/widgets/PersistentViewAnimator$SavedState;

    invoke-super {p0}, Lcom/squareup/widgets/SquareViewAnimator;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/widgets/PersistentViewAnimator;->getDisplayedChild()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/widgets/PersistentViewAnimator$SavedState;-><init>(Landroid/os/Parcelable;ILcom/squareup/widgets/PersistentViewAnimator$1;)V

    return-object v0
.end method
