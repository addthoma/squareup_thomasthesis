.class public final Lcom/squareup/widgets/pos/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/pos/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final about_setting_body:I = 0x7f0a00f9

.field public static final about_setting_glyph:I = 0x7f0a00fa

.field public static final about_setting_text_container:I = 0x7f0a00fb

.field public static final about_setting_title:I = 0x7f0a00fc

.field public static final action_bar:I = 0x7f0a0135

.field public static final allow_year_check:I = 0x7f0a01b3

.field public static final amount_field:I = 0x7f0a01bd

.field public static final animator:I = 0x7f0a01c2

.field public static final attribute_pair_key:I = 0x7f0a01d6

.field public static final attribute_pair_value:I = 0x7f0a01d7

.field public static final badge:I = 0x7f0a01ef

.field public static final badged_icon_block:I = 0x7f0a01f2

.field public static final button:I = 0x7f0a0272

.field public static final card_glyph:I = 0x7f0a02b9

.field public static final chevron:I = 0x7f0a0329

.field public static final chevron_block:I = 0x7f0a032a

.field public static final complete:I = 0x7f0a0372

.field public static final coupon_value:I = 0x7f0a03be

.field public static final cvv_editor:I = 0x7f0a0541

.field public static final cvv_input:I = 0x7f0a0542

.field public static final date_picker:I = 0x7f0a0548

.field public static final date_picker_month_pager:I = 0x7f0a054a

.field public static final date_picker_month_title:I = 0x7f0a054b

.field public static final date_picker_next_month:I = 0x7f0a054c

.field public static final date_picker_prev_month:I = 0x7f0a054d

.field public static final description:I = 0x7f0a0589

.field public static final details:I = 0x7f0a05ac

.field public static final drag_handle:I = 0x7f0a05ef

.field public static final edit_catalog_object_label_abbreviation:I = 0x7f0a060e

.field public static final edit_catalog_object_label_animator:I = 0x7f0a060f

.field public static final edit_catalog_object_label_image:I = 0x7f0a0610

.field public static final edit_catalog_object_label_name:I = 0x7f0a0611

.field public static final edit_catalog_object_label_progress:I = 0x7f0a0612

.field public static final empty_message:I = 0x7f0a06f3

.field public static final empty_title:I = 0x7f0a06f5

.field public static final expiration_editor:I = 0x7f0a0738

.field public static final expiration_input:I = 0x7f0a0739

.field public static final glyph:I = 0x7f0a07ae

.field public static final glyph_edit_text_field:I = 0x7f0a07b0

.field public static final height:I = 0x7f0a07cf

.field public static final hex_color:I = 0x7f0a07d5

.field public static final hour_picker:I = 0x7f0a07e6

.field public static final hud_glyph:I = 0x7f0a0813

.field public static final hud_message_text:I = 0x7f0a0814

.field public static final hud_text:I = 0x7f0a0816

.field public static final hud_vector:I = 0x7f0a0817

.field public static final info_bar_button:I = 0x7f0a082e

.field public static final info_bar_message:I = 0x7f0a082f

.field public static final inline_button:I = 0x7f0a0833

.field public static final item_image:I = 0x7f0a08d8

.field public static final item_name:I = 0x7f0a08dd

.field public static final keypad:I = 0x7f0a08f9

.field public static final light:I = 0x7f0a0938

.field public static final line_row_glyph_view:I = 0x7f0a0940

.field public static final load_more_message:I = 0x7f0a094d

.field public static final load_more_spinner:I = 0x7f0a094e

.field public static final medium:I = 0x7f0a09c7

.field public static final message:I = 0x7f0a09d3

.field public static final minute_picker:I = 0x7f0a09de

.field public static final name:I = 0x7f0a0a0c

.field public static final note:I = 0x7f0a0a47

.field public static final number:I = 0x7f0a0a7c

.field public static final number_last_digits:I = 0x7f0a0a7f

.field public static final options:I = 0x7f0a0ac0

.field public static final postal:I = 0x7f0a0c3f

.field public static final postal_keyboard_switch:I = 0x7f0a0c40

.field public static final postal_pii_scrubber:I = 0x7f0a0c41

.field public static final preserved_label:I = 0x7f0a0c49

.field public static final progress:I = 0x7f0a0c7b

.field public static final quantity_decrease:I = 0x7f0a0c9c

.field public static final quantity_increase:I = 0x7f0a0c9e

.field public static final quantity_text:I = 0x7f0a0ca0

.field public static final reader:I = 0x7f0a0ce4

.field public static final regular:I = 0x7f0a0d4f

.field public static final ribbon_image:I = 0x7f0a0d8c

.field public static final rl:I = 0x7f0a0d93

.field public static final single_picker_view_picker:I = 0x7f0a0e97

.field public static final subtitle:I = 0x7f0a0f49

.field public static final text_container:I = 0x7f0a0f9e

.field public static final text_tile_color:I = 0x7f0a0fa3

.field public static final text_tile_icon:I = 0x7f0a0fa4

.field public static final text_tile_name:I = 0x7f0a0fa5

.field public static final time_picker:I = 0x7f0a0fe0

.field public static final time_zone_tag:I = 0x7f0a0fe5

.field public static final title:I = 0x7f0a103f

.field public static final toggle_button_row:I = 0x7f0a104a

.field public static final touch_region:I = 0x7f0a105a

.field public static final value:I = 0x7f0a10de

.field public static final value_container:I = 0x7f0a10df

.field public static final value_subtitle:I = 0x7f0a10e0

.field public static final values:I = 0x7f0a10e1

.field public static final vector_image:I = 0x7f0a10e6

.field public static final width:I = 0x7f0a111d

.field public static final x_button:I = 0x7f0a1126

.field public static final xable_text_field:I = 0x7f0a1127


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
