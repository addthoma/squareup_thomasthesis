.class public Lcom/squareup/widgets/OnDeleteEditText;
.super Lcom/squareup/widgets/OnScreenRectangleEditText;
.source "OnDeleteEditText.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;,
        Lcom/squareup/widgets/OnDeleteEditText$CustomInputConnectionWrapper;
    }
.end annotation


# instance fields
.field private listener:Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/OnScreenRectangleEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-virtual {p0, p0}, Lcom/squareup/widgets/OnDeleteEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/OnDeleteEditText;)Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/widgets/OnDeleteEditText;->listener:Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;

    return-object p0
.end method


# virtual methods
.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .line 33
    invoke-super {p0, p1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 34
    :cond_0
    new-instance v0, Lcom/squareup/widgets/OnDeleteEditText$CustomInputConnectionWrapper;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/widgets/OnDeleteEditText$CustomInputConnectionWrapper;-><init>(Lcom/squareup/widgets/OnDeleteEditText;Landroid/view/inputmethod/InputConnection;Z)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 45
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p3

    if-nez p3, :cond_0

    const/16 p3, 0x43

    if-ne p2, p3, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/squareup/widgets/OnDeleteEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-interface {p2}, Landroid/text/Editable;->length()I

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/squareup/widgets/OnDeleteEditText;->listener:Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;

    if-eqz p2, :cond_0

    .line 49
    invoke-interface {p2, p1}, Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;->onDeleteKey(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/widgets/OnDeleteEditText;->listener:Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;

    return-void
.end method
