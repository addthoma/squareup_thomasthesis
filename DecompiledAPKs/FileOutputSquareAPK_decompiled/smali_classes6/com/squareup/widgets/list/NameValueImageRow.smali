.class public Lcom/squareup/widgets/list/NameValueImageRow;
.super Landroid/widget/LinearLayout;
.source "NameValueImageRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/list/NameValueImageRow$Builder;
    }
.end annotation


# instance fields
.field protected final nameView:Lcom/squareup/widgets/ShorteningTextView;

.field protected final valueView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/list/NameValueImageRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 61
    sget v0, Lcom/squareup/widgets/R$attr;->nameValueRowStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/list/NameValueImageRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    sget v0, Lcom/squareup/widgets/R$layout;->name_value_image_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/widgets/list/NameValueImageRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 69
    sget v0, Lcom/squareup/widgets/R$id;->name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ShorteningTextView;

    iput-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    .line 70
    sget v0, Lcom/squareup/widgets/R$id;->value:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->valueView:Landroid/widget/ImageView;

    .line 72
    sget-object v0, Lcom/squareup/widgets/R$styleable;->NameValueImageRow:[I

    const/4 v1, 0x0

    .line 73
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 74
    sget p3, Lcom/squareup/widgets/R$styleable;->NameValueImageRow_android_src:I

    const/4 v0, -0x1

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    .line 75
    sget v1, Lcom/squareup/widgets/R$styleable;->NameValueImageRow_android_text:I

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 76
    sget v2, Lcom/squareup/widgets/R$styleable;->NameValueImageRow_shortText:I

    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 78
    sget v3, Lcom/squareup/widgets/R$styleable;->NameValueImageRow_nameValueRowNameStyle:I

    invoke-virtual {p2, v3, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    if-eq v3, v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1, v3}, Lcom/squareup/widgets/ShorteningTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 83
    :cond_0
    invoke-virtual {p0, v1, v2, p3}, Lcom/squareup/widgets/list/NameValueImageRow;->update(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 84
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public enableName(Z)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setEnabled(Z)V

    return-void
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/ShorteningTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->valueView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 116
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 117
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setEnabled(Z)V

    .line 118
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->valueView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 102
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/NameValueImageRow;->setName(Ljava/lang/CharSequence;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/ShorteningTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 107
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/list/NameValueImageRow;->setName(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 108
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueImageRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/ShorteningTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setValue(I)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow;->valueView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public update(Ljava/lang/CharSequence;I)V
    .locals 0

    .line 131
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/NameValueImageRow;->setName(Ljava/lang/CharSequence;)V

    .line 132
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/list/NameValueImageRow;->setValue(I)V

    return-void
.end method

.method public update(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 0

    .line 136
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/list/NameValueImageRow;->setName(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 137
    invoke-virtual {p0, p3}, Lcom/squareup/widgets/list/NameValueImageRow;->setValue(I)V

    return-void
.end method
