.class public Lcom/squareup/widgets/list/ToggleButtonRow;
.super Landroid/widget/LinearLayout;
.source "ToggleButtonRow.java"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;,
        Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;
    }
.end annotation


# static fields
.field private static final NO_SHORT_TEXT:Ljava/lang/CharSequence; = null

.field private static final TYPE_CHECK:I = 0x2

.field private static final TYPE_RADIO:I = 0x0

.field private static final TYPE_SWITCH:I = 0x1


# instance fields
.field private button:Landroid/widget/CompoundButton;

.field private checkListener:Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;

.field private name:Lcom/squareup/widgets/ShorteningTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 4

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x101004d

    aput v3, v1, v2

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v1, 0x30

    .line 45
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setMinimumHeight(I)V

    .line 46
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    invoke-direct/range {p0 .. p5}, Lcom/squareup/widgets/list/ToggleButtonRow;->init(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 53
    sget v0, Lcom/squareup/widgets/R$attr;->toggleButtonRowStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    sget-object v0, Lcom/squareup/widgets/R$styleable;->ToggleButtonRow:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 60
    sget p3, Lcom/squareup/widgets/R$styleable;->ToggleButtonRow_android_text:I

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 61
    sget p3, Lcom/squareup/widgets/R$styleable;->ToggleButtonRow_shortText:I

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 62
    sget p3, Lcom/squareup/widgets/R$styleable;->ToggleButtonRow_type:I

    const/4 v0, 0x1

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 64
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    .line 66
    invoke-direct/range {v0 .. v5}, Lcom/squareup/widgets/list/ToggleButtonRow;->init(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/list/ToggleButtonRow;)Landroid/widget/CompoundButton;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    return-object p0
.end method

.method private init(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 3

    const-string v0, "Invalid button type: "

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eqz p5, :cond_3

    if-eqz p2, :cond_2

    if-eq p2, v1, :cond_1

    if-eq p2, v2, :cond_0

    .line 183
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 181
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "No support for Check rows with PreservedLabelView!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 178
    :cond_1
    sget p2, Lcom/squareup/widgets/R$layout;->togglebutton_row_switch_preserve:I

    goto :goto_0

    .line 175
    :cond_2
    sget p2, Lcom/squareup/widgets/R$layout;->togglebutton_row_radio_preserve:I

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_6

    if-eq p2, v1, :cond_5

    if-ne p2, v2, :cond_4

    .line 194
    sget p2, Lcom/squareup/widgets/R$layout;->togglebutton_row_check:I

    goto :goto_0

    .line 197
    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 191
    :cond_5
    sget p2, Lcom/squareup/widgets/R$layout;->togglebutton_row_switch_shorten:I

    goto :goto_0

    .line 188
    :cond_6
    sget p2, Lcom/squareup/widgets/R$layout;->togglebutton_row_radio_shorten:I

    .line 200
    :goto_0
    invoke-static {p1, p2, p0}, Lcom/squareup/widgets/list/ToggleButtonRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    if-nez p5, :cond_7

    .line 203
    sget p1, Lcom/squareup/widgets/R$id;->name:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/ShorteningTextView;

    iput-object p1, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    .line 204
    iget-object p1, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    iget-object p1, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, p4}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 207
    :cond_7
    sget p1, Lcom/squareup/widgets/R$id;->preserved_label:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/PreservedLabelView;

    .line 208
    invoke-virtual {p1, p3}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 209
    invoke-virtual {p1, p4}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    .line 212
    :goto_1
    new-instance p1, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;-><init>(Lcom/squareup/widgets/list/ToggleButtonRow$1;)V

    iput-object p1, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->checkListener:Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;

    .line 214
    sget p1, Lcom/squareup/widgets/R$id;->button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CompoundButton;

    iput-object p1, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    .line 215
    iget-object p1, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    iget-object p2, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->checkListener:Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;

    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 216
    iget-object p1, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v2}, Landroid/widget/CompoundButton;->setImportantForAccessibility(I)V

    .line 218
    invoke-virtual {p0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isClickable()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 219
    new-instance p1, Lcom/squareup/widgets/list/ToggleButtonRow$3;

    invoke-direct {p1, p0}, Lcom/squareup/widgets/list/ToggleButtonRow$3;-><init>(Lcom/squareup/widgets/list/ToggleButtonRow;)V

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    return-void
.end method

.method public static radioRow(Landroid/content/Context;II)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 0

    .line 131
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->radioRow(Landroid/content/Context;Ljava/lang/CharSequence;I)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object p0

    return-object p0
.end method

.method public static radioRow(Landroid/content/Context;Ljava/lang/CharSequence;I)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 7

    .line 136
    new-instance v6, Lcom/squareup/widgets/list/ToggleButtonRow;

    sget-object v4, Lcom/squareup/widgets/list/ToggleButtonRow;->NO_SHORT_TEXT:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/widgets/list/ToggleButtonRow;-><init>(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    if-eqz p2, :cond_0

    .line 139
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    const/4 p1, 0x0

    .line 140
    invoke-virtual {v6, p0, p1, p0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setPadding(IIII)V

    :cond_0
    return-object v6
.end method

.method public static radioRowPreservedLabel(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 7

    const/4 v0, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    .line 104
    invoke-static/range {v0 .. v6}, Lcom/squareup/widgets/list/ToggleButtonRow;->rowPreservedLabel(ILandroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object p0

    return-object p0
.end method

.method private static rowPreservedLabel(ILandroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 7

    .line 111
    new-instance v6, Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v5, 0x1

    move-object v0, v6

    move-object v1, p1

    move v2, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/widgets/list/ToggleButtonRow;-><init>(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, p5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    const/4 p2, 0x0

    .line 116
    invoke-virtual {v6, p0, p2, p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setPadding(IIII)V

    :cond_0
    if-eqz p6, :cond_1

    .line 119
    invoke-virtual {v6, p6}, Lcom/squareup/widgets/list/ToggleButtonRow;->setBackgroundResource(I)V

    .line 120
    new-instance p0, Lcom/squareup/widgets/list/ToggleButtonRow$2;

    invoke-direct {p0, v6}, Lcom/squareup/widgets/list/ToggleButtonRow$2;-><init>(Lcom/squareup/widgets/list/ToggleButtonRow;)V

    invoke-virtual {v6, p0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-object v6
.end method

.method public static switchRow(Landroid/content/Context;Ljava/lang/CharSequence;II)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 1

    const/4 v0, -0x1

    .line 71
    invoke-static {p0, p1, p2, p3, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->switchRow(Landroid/content/Context;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object p0

    return-object p0
.end method

.method public static switchRow(Landroid/content/Context;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 7

    .line 76
    new-instance v6, Lcom/squareup/widgets/list/ToggleButtonRow;

    sget-object v4, Lcom/squareup/widgets/list/ToggleButtonRow;->NO_SHORT_TEXT:Ljava/lang/CharSequence;

    const/4 v2, 0x1

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/widgets/list/ToggleButtonRow;-><init>(Landroid/content/Context;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 79
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 80
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    const/4 p2, 0x0

    .line 81
    invoke-virtual {v6, p1, p2, p0, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setPadding(IIII)V

    :cond_0
    if-eqz p4, :cond_1

    .line 84
    invoke-virtual {v6, p4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setBackgroundResource(I)V

    .line 85
    new-instance p0, Lcom/squareup/widgets/list/ToggleButtonRow$1;

    invoke-direct {p0, v6}, Lcom/squareup/widgets/list/ToggleButtonRow$1;-><init>(Lcom/squareup/widgets/list/ToggleButtonRow;)V

    invoke-virtual {v6, p0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-object v6
.end method

.method public static switchRowPreservedLabel(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 7

    const/4 v0, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    .line 97
    invoke-static/range {v0 .. v6}, Lcom/squareup/widgets/list/ToggleButtonRow;->rowPreservedLabel(ILandroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public isShortened()Z
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/ShorteningTextView;->isShortened()Z

    move-result v0

    return v0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .line 288
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 289
    invoke-virtual {p1, p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;)V

    const/4 v0, 0x1

    .line 290
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 291
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 292
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 164
    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;

    .line 165
    invoke-virtual {p1}, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-static {p1}, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;->access$200(Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;)Landroid/os/Parcelable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 160
    new-instance v0, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Parcelable;Lcom/squareup/widgets/list/ToggleButtonRow$1;)V

    return-object v0
.end method

.method public setButtonEnabled(Z)V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    return-void
.end method

.method public setButtonTag(Ljava/lang/Object;)V
    .locals 1

    .line 228
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 261
    invoke-virtual {p0, p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    return-void
.end method

.method public setChecked(ZZ)V
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->checkListener:Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;->deliverEvents:Z

    .line 267
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    instance-of v1, v0, Lcom/squareup/widgets/list/AnimatableSwitch;

    if-eqz v1, :cond_1

    .line 268
    check-cast v0, Lcom/squareup/widgets/list/AnimatableSwitch;

    if-eqz p2, :cond_0

    .line 270
    invoke-interface {v0, p1}, Lcom/squareup/widgets/list/AnimatableSwitch;->setChecked(Z)V

    goto :goto_0

    .line 272
    :cond_0
    invoke-interface {v0, p1}, Lcom/squareup/widgets/list/AnimatableSwitch;->setCheckedNoAnimation(Z)V

    goto :goto_0

    .line 275
    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 277
    :goto_0
    iget-object p2, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->checkListener:Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;

    const/4 v0, 0x1

    iput-boolean v0, p2, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;->deliverEvents:Z

    .line 278
    iget-object p2, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    if-eqz p2, :cond_2

    .line 279
    invoke-virtual {p2, p1}, Lcom/squareup/widgets/ShorteningTextView;->setSelected(Z)V

    :cond_2
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 146
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 147
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setEnabled(Z)V

    .line 148
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    return-void
.end method

.method public setImportantForAccessibility(I)V
    .locals 1

    .line 296
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setImportantForAccessibility(I)V

    .line 297
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setImportantForAccessibility(I)V

    .line 298
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setImportantForAccessibility(I)V

    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->checkListener:Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;

    iput-object p1, v0, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;->delegate:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public showButton(Z)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->button:Landroid/widget/CompoundButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 257
    invoke-virtual {p0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public update(Ljava/lang/CharSequence;Z)V
    .locals 1

    const/4 v0, 0x1

    .line 240
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->update(Ljava/lang/CharSequence;ZZ)V

    return-void
.end method

.method public update(Ljava/lang/CharSequence;ZZ)V
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    invoke-virtual {p0, p2, p3}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    return-void
.end method
