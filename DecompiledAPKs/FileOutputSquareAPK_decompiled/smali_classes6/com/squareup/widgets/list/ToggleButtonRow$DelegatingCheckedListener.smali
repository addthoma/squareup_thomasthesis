.class Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;
.super Ljava/lang/Object;
.source "ToggleButtonRow.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/list/ToggleButtonRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DelegatingCheckedListener"
.end annotation


# instance fields
.field delegate:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field deliverEvents:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 303
    iput-boolean v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;->deliverEvents:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/widgets/list/ToggleButtonRow$1;)V
    .locals 0

    .line 301
    invoke-direct {p0}, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 306
    iget-boolean v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;->deliverEvents:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/widgets/list/ToggleButtonRow$DelegatingCheckedListener;->delegate:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    :cond_0
    return-void
.end method
