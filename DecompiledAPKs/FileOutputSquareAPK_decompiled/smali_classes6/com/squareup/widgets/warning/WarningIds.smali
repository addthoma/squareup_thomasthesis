.class public Lcom/squareup/widgets/warning/WarningIds;
.super Ljava/lang/Object;
.source "WarningIds.java"

# interfaces
.implements Lcom/squareup/widgets/warning/Warning;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/widgets/warning/WarningIds;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final bodyResId:I

.field public final titleResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds$1;

    invoke-direct {v0}, Lcom/squareup/widgets/warning/WarningIds$1;-><init>()V

    sput-object v0, Lcom/squareup/widgets/warning/WarningIds;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/squareup/widgets/warning/WarningIds;->titleResId:I

    .line 13
    iput p2, p0, Lcom/squareup/widgets/warning/WarningIds;->bodyResId:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 28
    :cond_1
    check-cast p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 30
    iget v2, p0, Lcom/squareup/widgets/warning/WarningIds;->bodyResId:I

    iget v3, p1, Lcom/squareup/widgets/warning/WarningIds;->bodyResId:I

    if-eq v2, v3, :cond_2

    return v1

    .line 31
    :cond_2
    iget v2, p0, Lcom/squareup/widgets/warning/WarningIds;->titleResId:I

    iget p1, p1, Lcom/squareup/widgets/warning/WarningIds;->titleResId:I

    if-ne v2, p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_4
    :goto_1
    return v1
.end method

.method public getStrings(Landroid/content/res/Resources;)Lcom/squareup/widgets/warning/Warning$Strings;
    .locals 3

    .line 17
    new-instance v0, Lcom/squareup/widgets/warning/Warning$Strings;

    iget v1, p0, Lcom/squareup/widgets/warning/WarningIds;->titleResId:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/squareup/widgets/warning/WarningIds;->bodyResId:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/Warning$Strings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 35
    iget v0, p0, Lcom/squareup/widgets/warning/WarningIds;->titleResId:I

    mul-int/lit8 v0, v0, 0x1f

    .line 36
    iget v1, p0, Lcom/squareup/widgets/warning/WarningIds;->bodyResId:I

    add-int/2addr v0, v1

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 41
    iget p2, p0, Lcom/squareup/widgets/warning/WarningIds;->titleResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    iget p2, p0, Lcom/squareup/widgets/warning/WarningIds;->bodyResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
