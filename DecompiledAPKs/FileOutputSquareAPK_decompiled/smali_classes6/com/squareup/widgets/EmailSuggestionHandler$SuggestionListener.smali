.class public interface abstract Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;
.super Ljava/lang/Object;
.source "EmailSuggestionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/EmailSuggestionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SuggestionListener"
.end annotation


# virtual methods
.method public abstract addTextChangedListener(Landroid/text/TextWatcher;)V
.end method

.method public abstract getText()Ljava/lang/CharSequence;
.end method

.method public abstract setText(Ljava/lang/CharSequence;)V
.end method
