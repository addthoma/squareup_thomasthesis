.class public Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
.super Landroid/app/AlertDialog$Builder;
.source "ThemedAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/dialog/ThemedAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;
    }
.end annotation


# instance fields
.field private nullWindowBackground:Z

.field private final params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

.field private final theme:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 106
    invoke-static {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->resolveDialogTheme(Landroid/content/Context;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .line 110
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 112
    iput p2, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->theme:I

    .line 113
    iget p2, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->theme:I

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 118
    new-instance v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-direct {v1, p1, p2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    .line 119
    iget-object p1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    invoke-static {}, Lcom/squareup/util/Dialogs;->ignoresSearchKeyListener()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    return-void

    .line 114
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Themed alert dialog requires \'themedAlertDialogTheme\' attribute on the context theme."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static resolveDialogTheme(Landroid/content/Context;)I
    .locals 2

    .line 99
    sget-object v0, Lcom/squareup/widgets/R$styleable;->ThemedAlertDialog:[I

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p0

    .line 100
    sget v0, Lcom/squareup/widgets/R$styleable;->ThemedAlertDialog_themedAlertDialogTheme:I

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 101
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    return v0
.end method


# virtual methods
.method public clearWindowBackground()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 294
    iput-boolean v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->nullWindowBackground:Z

    return-object p0
.end method

.method public create()Landroid/app/AlertDialog;
    .locals 4

    .line 299
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;

    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    iget v2, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->theme:I

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;-><init>(Landroid/content/Context;ILcom/squareup/widgets/dialog/ThemedAlertDialog$1;)V

    .line 300
    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    invoke-static {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->access$200(Lcom/squareup/widgets/dialog/ThemedAlertDialog;)Lcom/squareup/widgets/dialog/internal/AlertController;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->apply(Lcom/squareup/widgets/dialog/internal/AlertController;)V

    .line 301
    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-boolean v1, v1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->cancelable:Z

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->setCancelable(Z)V

    .line 302
    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-boolean v1, v1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->cancelable:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 303
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 305
    :cond_0
    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 306
    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    if-eqz v1, :cond_1

    .line 307
    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 309
    :cond_1
    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v1, :cond_2

    .line 310
    iget-object v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 312
    :cond_2
    iget-boolean v1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->nullWindowBackground:Z

    if-eqz v1, :cond_3

    .line 313
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 315
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, -0x1

    .line 316
    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setLayout(II)V

    return-object v0
.end method

.method public bridge synthetic setCancelable(Z)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-boolean p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->cancelable:Z

    return-object p0
.end method

.method public setDialogContentLayout(Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    return-object p0
.end method

.method public bridge synthetic setIcon(I)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setIcon(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setIcon(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->iconId:I

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->icon:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public bridge synthetic setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setItems(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->items:[Ljava/lang/CharSequence;

    .line 252
    iget-object p1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p2, p1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->items:[Ljava/lang/CharSequence;

    .line 258
    iput-object p2, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public bridge synthetic setMessage(I)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->message:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->message:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setMessageContentDescription(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->messageContentDescription:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public bridge synthetic setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 225
    invoke-virtual {p0, p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->negativeButtonText:Ljava/lang/CharSequence;

    .line 210
    iget-object p1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p2, p1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->negativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public setNegativeButton(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 221
    invoke-virtual {p0, p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->negativeButtonText:Ljava/lang/CharSequence;

    .line 216
    iput-object p2, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->negativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public bridge synthetic setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->neutralButtonText:Ljava/lang/CharSequence;

    .line 230
    iget-object p1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p2, p1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->neutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->neutralButtonText:Ljava/lang/CharSequence;

    .line 236
    iput-object p2, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->neutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public bridge synthetic setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    return-object p0
.end method

.method public bridge synthetic setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 3

    .line 273
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;-><init>(Landroid/content/DialogInterface$OnDismissListener;Lcom/squareup/widgets/dialog/ThemedAlertDialog$1;)V

    iput-object v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-object p0
.end method

.method public bridge synthetic setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setPositiveButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 201
    invoke-virtual {p0, p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonText:Ljava/lang/CharSequence;

    .line 190
    iget-object p1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p2, p1, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public setPositiveButton(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 205
    invoke-virtual {p0, p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonText:Ljava/lang/CharSequence;

    .line 196
    iput-object p2, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    return-object p0
.end method

.method public setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonBackground:I

    return-object p0
.end method

.method public setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonTextColor:I

    return-object p0
.end method

.method public setPrimaryButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->primaryButton:I

    return-object p0
.end method

.method public bridge synthetic setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->adapter:Landroid/widget/ListAdapter;

    .line 265
    iput-object p3, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onClickListener:Landroid/content/DialogInterface$OnClickListener;

    const/4 p1, 0x1

    .line 266
    iput-boolean p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->isSingleChoice:Z

    .line 267
    iput p2, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->checkedItem:I

    return-object p0
.end method

.method public bridge synthetic setTitle(I)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iget-object v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->title:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->title:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitleContentDescription(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->titleContentDescription:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setType(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->type:I

    return-object p0
.end method

.method public setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->verticalButtonOrientation:Z

    return-object p0
.end method

.method public bridge synthetic setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->params:Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;

    iput-object p1, v0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->view:Landroid/view/View;

    return-object p0
.end method

.method public show()Landroid/app/AlertDialog;
    .locals 1

    .line 322
    invoke-virtual {p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-object v0
.end method
