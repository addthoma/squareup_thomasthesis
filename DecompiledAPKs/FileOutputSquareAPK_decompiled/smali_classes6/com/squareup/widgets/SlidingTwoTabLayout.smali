.class public Lcom/squareup/widgets/SlidingTwoTabLayout;
.super Lcom/squareup/widgets/PairLayout;
.source "SlidingTwoTabLayout.java"


# instance fields
.field private indicatorColor:I

.field private indicatorHeight:I

.field private indicatorPaint:Landroid/graphics/Paint;

.field protected leftTab:Landroid/view/View;

.field private offset:F

.field protected rightTab:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->indicatorPaint:Landroid/graphics/Paint;

    .line 30
    sget-object v0, Lcom/squareup/widgets/R$styleable;->SlidingTwoTabLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 31
    sget p2, Lcom/squareup/widgets/R$styleable;->SlidingTwoTabLayout_indicatorColor:I

    const/high16 v0, -0x1000000

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->indicatorColor:I

    .line 32
    sget p2, Lcom/squareup/widgets/R$styleable;->SlidingTwoTabLayout_indicatorHeight:I

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->indicatorHeight:I

    .line 33
    iget p2, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->indicatorHeight:I

    if-eq p2, v0, :cond_0

    .line 36
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 38
    iget-object p1, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->indicatorPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->indicatorColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 40
    sget-object p1, Lcom/squareup/widgets/PairLayout$Child;->FIRST:Lcom/squareup/widgets/PairLayout$Child;

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/SlidingTwoTabLayout;->setPrimaryChild(Lcom/squareup/widgets/PairLayout$Child;)V

    const/high16 p1, 0x3f000000    # 0.5f

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/SlidingTwoTabLayout;->setChildPercentage(F)V

    return-void

    .line 34
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Must supply indicatorHeight for SlidingTwoTabLayout!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .line 56
    invoke-super {p0, p1}, Lcom/squareup/widgets/PairLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->getHeight()I

    move-result v0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v2, v1

    .line 60
    iget v3, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->offset:F

    mul-float v2, v2, v3

    float-to-int v2, v2

    int-to-float v4, v2

    .line 61
    iget v3, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->indicatorHeight:I

    sub-int v3, v0, v3

    int-to-float v5, v3

    add-int/2addr v2, v1

    int-to-float v6, v2

    int-to-float v7, v0

    iget-object v8, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->indicatorPaint:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 45
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    const/4 v0, 0x0

    .line 46
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->leftTab:Landroid/view/View;

    const/4 v0, 0x1

    .line 47
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->rightTab:Landroid/view/View;

    return-void
.end method

.method public setOffset(F)V
    .locals 0

    .line 51
    iput p1, p0, Lcom/squareup/widgets/SlidingTwoTabLayout;->offset:F

    .line 52
    invoke-virtual {p0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->invalidate()V

    return-void
.end method
