.class public Lcom/squareup/widgets/ResponsiveFrameLayout;
.super Landroid/widget/FrameLayout;
.source "ResponsiveFrameLayout.java"


# instance fields
.field private final controller:Lcom/squareup/widgets/ResponsiveViewController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 13
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    new-instance v0, Lcom/squareup/widgets/ResponsiveViewController;

    invoke-direct {v0, p1, p2, p0}, Lcom/squareup/widgets/ResponsiveViewController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    iput-object v0, p0, Lcom/squareup/widgets/ResponsiveFrameLayout;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/widgets/ResponsiveFrameLayout;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/ResponsiveViewController;->measure(II)V

    .line 19
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method
