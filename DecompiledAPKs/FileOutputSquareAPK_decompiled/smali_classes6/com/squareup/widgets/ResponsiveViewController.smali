.class public Lcom/squareup/widgets/ResponsiveViewController;
.super Ljava/lang/Object;
.source "ResponsiveViewController.java"


# instance fields
.field private final isResponsive:Z

.field private originalLeftPadding:I

.field private originalRightPadding:I

.field private screenHeight:I

.field private screenWidth:I

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p3, p0, Lcom/squareup/widgets/ResponsiveViewController;->view:Landroid/view/View;

    .line 37
    sget-object p3, Lcom/squareup/widgets/R$styleable;->ResponsiveView:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 38
    sget p3, Lcom/squareup/widgets/R$styleable;->ResponsiveView_isResponsive:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/squareup/widgets/ResponsiveViewController;->isResponsive:Z

    .line 39
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/widgets/ResponsiveViewController;->setup(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLandroid/view/View;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p3, p0, Lcom/squareup/widgets/ResponsiveViewController;->view:Landroid/view/View;

    .line 26
    iput-boolean p2, p0, Lcom/squareup/widgets/ResponsiveViewController;->isResponsive:Z

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/widgets/ResponsiveViewController;->setup(Landroid/content/Context;)V

    return-void
.end method

.method private setup(Landroid/content/Context;)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->originalLeftPadding:I

    .line 46
    iget-object v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->originalRightPadding:I

    .line 48
    iget-boolean v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->isResponsive:Z

    if-nez v0, :cond_0

    return-void

    .line 50
    :cond_0
    invoke-static {p1}, Lcom/squareup/util/ScreenParameters;->getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p1

    .line 51
    iget v0, p1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->screenWidth:I

    .line 52
    iget p1, p1, Landroid/graphics/Point;->y:I

    iput p1, p0, Lcom/squareup/widgets/ResponsiveViewController;->screenHeight:I

    return-void
.end method


# virtual methods
.method public getOriginalLeftPadding()I
    .locals 1

    .line 69
    iget v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->originalLeftPadding:I

    return v0
.end method

.method public getOriginalRightPadding()I
    .locals 1

    .line 73
    iget v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->originalRightPadding:I

    return v0
.end method

.method public measure(II)V
    .locals 3

    .line 57
    iget-boolean p2, p0, Lcom/squareup/widgets/ResponsiveViewController;->isResponsive:Z

    if-nez p2, :cond_0

    return-void

    .line 60
    :cond_0
    iget p2, p0, Lcom/squareup/widgets/ResponsiveViewController;->screenHeight:I

    iget v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->screenWidth:I

    if-le p2, v0, :cond_1

    return-void

    .line 62
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    const/4 p2, 0x0

    .line 63
    iget v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->screenHeight:I

    sub-int/2addr p1, v0

    div-int/lit8 p1, p1, 0x2

    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 64
    iget-object p2, p0, Lcom/squareup/widgets/ResponsiveViewController;->view:Landroid/view/View;

    iget v0, p0, Lcom/squareup/widgets/ResponsiveViewController;->originalLeftPadding:I

    add-int/2addr v0, p1

    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/squareup/widgets/ResponsiveViewController;->originalRightPadding:I

    add-int/2addr p1, v2

    iget-object v2, p0, Lcom/squareup/widgets/ResponsiveViewController;->view:Landroid/view/View;

    .line 65
    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    .line 64
    invoke-virtual {p2, v0, v1, p1, v2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method
