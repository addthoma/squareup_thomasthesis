.class Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CheckableGroup.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/CheckableGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckedStateTracker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/widgets/CheckableGroup;


# direct methods
.method private constructor <init>(Lcom/squareup/widgets/CheckableGroup;)V
    .locals 0

    .line 531
    iput-object p1, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/widgets/CheckableGroup;Lcom/squareup/widgets/CheckableGroup$1;)V
    .locals 0

    .line 531
    invoke-direct {p0, p1}, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;-><init>(Lcom/squareup/widgets/CheckableGroup;)V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    .line 552
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$400(Lcom/squareup/widgets/CheckableGroup;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    .line 553
    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$800(Lcom/squareup/widgets/CheckableGroup;)Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    .line 554
    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$800(Lcom/squareup/widgets/CheckableGroup;)Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;->onCheckedClicked(Lcom/squareup/widgets/CheckableGroup;I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$500(Lcom/squareup/widgets/CheckableGroup;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    goto :goto_0

    .line 561
    :cond_1
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->toggle(I)V

    :goto_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 536
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {p2}, Lcom/squareup/widgets/CheckableGroup;->access$300(Lcom/squareup/widgets/CheckableGroup;)Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 540
    :cond_0
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/squareup/widgets/CheckableGroup;->access$302(Lcom/squareup/widgets/CheckableGroup;Z)Z

    .line 541
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {p2}, Lcom/squareup/widgets/CheckableGroup;->access$400(Lcom/squareup/widgets/CheckableGroup;)Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result p2

    const/4 v0, 0x0

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {p2}, Lcom/squareup/widgets/CheckableGroup;->access$500(Lcom/squareup/widgets/CheckableGroup;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 542
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {p2}, Lcom/squareup/widgets/CheckableGroup;->access$400(Lcom/squareup/widgets/CheckableGroup;)Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 543
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v1, p2, v0}, Lcom/squareup/widgets/CheckableGroup;->access$600(Lcom/squareup/widgets/CheckableGroup;IZ)Z

    .line 545
    :cond_1
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {p2, v0}, Lcom/squareup/widgets/CheckableGroup;->access$302(Lcom/squareup/widgets/CheckableGroup;Z)Z

    .line 547
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result p1

    .line 548
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {p2, p1}, Lcom/squareup/widgets/CheckableGroup;->access$700(Lcom/squareup/widgets/CheckableGroup;I)V

    return-void
.end method
