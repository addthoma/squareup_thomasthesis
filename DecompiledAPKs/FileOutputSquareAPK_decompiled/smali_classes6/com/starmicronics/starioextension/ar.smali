.class Lcom/starmicronics/starioextension/ar;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/ar$1;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/ar$1;-><init>()V

    const/16 v1, 0x18

    if-gt p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x20

    :goto_0
    const/4 p1, 0x3

    new-array p1, p1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1b

    aput-byte v3, p1, v2

    const/4 v2, 0x1

    const/16 v3, 0x7a

    aput-byte v3, p1, v2

    const/4 v2, 0x2

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, p1, v2

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v1, 0x7f

    if-le p1, v1, :cond_1

    const/16 p1, 0x7f

    :cond_1
    const/4 v1, 0x3

    new-array v1, v1, [B

    const/16 v2, 0x1b

    aput-byte v2, v1, v0

    const/4 v0, 0x1

    const/16 v2, 0x33

    aput-byte v2, v1, v0

    const/4 v0, 0x2

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v1, 0x7f

    if-le p1, v1, :cond_1

    const/16 p1, 0x7f

    :cond_1
    const/4 v1, 0x3

    new-array v1, v1, [B

    const/16 v2, 0x1b

    aput-byte v2, v1, v0

    const/4 v0, 0x1

    const/16 v2, 0x33

    aput-byte v2, v1, v0

    const/4 v0, 0x2

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static e(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v1, 0xff

    if-le p1, v1, :cond_1

    const/16 p1, 0xff

    :cond_1
    const/4 v1, 0x3

    new-array v1, v1, [B

    const/16 v2, 0x1b

    aput-byte v2, v1, v0

    const/4 v0, 0x1

    const/16 v2, 0x79

    aput-byte v2, v1, v0

    const/4 v0, 0x2

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
