.class public interface abstract Lcom/starmicronics/starioextension/ICommandBuilder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;,
        Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;,
        Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;,
        Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;,
        Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;,
        Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;,
        Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;,
        Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;,
        Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;,
        Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;,
        Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;,
        Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;,
        Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;,
        Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;,
        Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;,
        Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;,
        Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;
    }
.end annotation


# virtual methods
.method public abstract append(B)V
.end method

.method public abstract append([B)V
.end method

.method public abstract appendAbsolutePosition(I)V
.end method

.method public abstract appendAbsolutePosition([BI)V
.end method

.method public abstract appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendAlignment([BLcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendBarcode([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
.end method

.method public abstract appendBarcodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZI)V
.end method

.method public abstract appendBarcodeWithAlignment([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZLcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendBitmap(Landroid/graphics/Bitmap;Z)V
.end method

.method public abstract appendBitmap(Landroid/graphics/Bitmap;ZIZ)V
.end method

.method public abstract appendBitmap(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
.end method

.method public abstract appendBitmap(Landroid/graphics/Bitmap;ZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
.end method

.method public abstract appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZI)V
.end method

.method public abstract appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZIZI)V
.end method

.method public abstract appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;I)V
.end method

.method public abstract appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;I)V
.end method

.method public abstract appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZLcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendBlackMark(Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
.end method

.method public abstract appendCharacterSpace(I)V
.end method

.method public abstract appendCodePage(Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V
.end method

.method public abstract appendCutPaper(Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
.end method

.method public abstract appendEmphasis(Z)V
.end method

.method public abstract appendEmphasis([B)V
.end method

.method public abstract appendFontStyle(Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;)V
.end method

.method public abstract appendHorizontalTabPosition([I)V
.end method

.method public abstract appendInitialization(Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;)V
.end method

.method public abstract appendInternational(Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V
.end method

.method public abstract appendInvert(Z)V
.end method

.method public abstract appendInvert([B)V
.end method

.method public abstract appendLineFeed()V
.end method

.method public abstract appendLineFeed(I)V
.end method

.method public abstract appendLineFeed([B)V
.end method

.method public abstract appendLineFeed([BI)V
.end method

.method public abstract appendLineSpace(I)V
.end method

.method public abstract appendLogo(Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
.end method

.method public abstract appendMultiple(II)V
.end method

.method public abstract appendMultiple([BII)V
.end method

.method public abstract appendMultipleHeight(I)V
.end method

.method public abstract appendMultipleHeight([BI)V
.end method

.method public abstract appendMultipleWidth(I)V
.end method

.method public abstract appendMultipleWidth([BI)V
.end method

.method public abstract appendPageModeRotation(Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
.end method

.method public abstract appendPageModeVerticalAbsolutePosition(I)V
.end method

.method public abstract appendPdf417([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
.end method

.method public abstract appendPdf417WithAbsolutePosition([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;III)V
.end method

.method public abstract appendPdf417WithAlignment([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;IILcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendPeripheral(Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;)V
.end method

.method public abstract appendPeripheral(Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
.end method

.method public abstract appendPrintableArea(Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
.end method

.method public abstract appendQrCode([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
.end method

.method public abstract appendQrCodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;II)V
.end method

.method public abstract appendQrCodeWithAlignment([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;ILcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public abstract appendRaw(B)V
.end method

.method public abstract appendRaw([B)V
.end method

.method public abstract appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;)V
.end method

.method public abstract appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;I)V
.end method

.method public abstract appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
.end method

.method public abstract appendTopMargin(I)V
.end method

.method public abstract appendUnderLine(Z)V
.end method

.method public abstract appendUnderLine([B)V
.end method

.method public abstract appendUnitFeed(I)V
.end method

.method public abstract appendUnitFeed([BI)V
.end method

.method public abstract beginDocument()V
.end method

.method public abstract beginPageMode(Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
.end method

.method public abstract endDocument()V
.end method

.method public abstract endPageMode()V
.end method

.method public abstract getCommands()[B
.end method
