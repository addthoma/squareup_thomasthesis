.class final Lcom/starmicronics/starioextension/al$1;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/al;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->USA:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->France:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Germany:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->UK:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Denmark:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Sweden:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Italy:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Spain:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Japan:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Norway:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Denmark2:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Spain2:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->LatinAmerica:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Korea:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Ireland:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Legal:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v1, 0x40

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/al$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
