.class Lcom/starmicronics/starioextension/be;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;",
            ")V"
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/starmicronics/starioextension/be;->d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V

    return-void
.end method

.method public static b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;",
            ")V"
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/starmicronics/starioextension/be;->d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V

    return-void
.end method

.method public static c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method private static d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/be$1;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/be$1;-><init>()V

    const/4 v1, 0x4

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0x1e

    aput-byte v3, v1, v2

    const/4 v2, 0x2

    const/16 v3, 0x41

    aput-byte v3, v1, v2

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/4 v0, 0x3

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
