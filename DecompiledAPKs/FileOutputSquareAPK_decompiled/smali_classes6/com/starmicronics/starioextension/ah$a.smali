.class Lcom/starmicronics/starioextension/ah$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/ah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;I)Lcom/starmicronics/starioextension/ah;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/ah$2;->a:[I

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    new-instance p0, Lcom/starmicronics/starioextension/br;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/br;-><init>(I)V

    return-object p0

    :pswitch_0
    new-instance p0, Lcom/starmicronics/starioextension/az;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/az;-><init>(I)V

    return-object p0

    :pswitch_1
    new-instance p0, Lcom/starmicronics/starioextension/q;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/q;-><init>(I)V

    return-object p0

    :pswitch_2
    new-instance p0, Lcom/starmicronics/starioextension/o;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/o;-><init>(I)V

    return-object p0

    :pswitch_3
    new-instance p0, Lcom/starmicronics/starioextension/an;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/an;-><init>(I)V

    return-object p0

    :pswitch_4
    new-instance p0, Lcom/starmicronics/starioextension/p;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/p;-><init>(I)V

    return-object p0

    :pswitch_5
    new-instance p0, Lcom/starmicronics/starioextension/ao;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/ao;-><init>(I)V

    return-object p0

    :pswitch_6
    new-instance p0, Lcom/starmicronics/starioextension/ap;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/ap;-><init>(I)V

    return-object p0

    :pswitch_7
    new-instance p0, Lcom/starmicronics/starioextension/bq;

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/bq;-><init>(I)V

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
