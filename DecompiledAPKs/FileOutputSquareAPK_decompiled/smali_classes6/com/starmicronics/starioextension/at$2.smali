.class final Lcom/starmicronics/starioextension/at$2;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/at;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/at$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;->DoubleWidth:Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/at$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;->DoubleHeight:Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/at$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;->DoubleWidthDoubleHeight:Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/at$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
