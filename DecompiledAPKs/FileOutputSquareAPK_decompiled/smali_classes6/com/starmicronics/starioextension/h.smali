.class Lcom/starmicronics/starioextension/h;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Landroid/graphics/Rect;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    new-instance v2, Lcom/starmicronics/starioextension/h$1;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/h$1;-><init>()V

    const/4 v3, 0x4

    new-array v4, v3, [B

    fill-array-data v4, :array_0

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v4, p2

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    const/4 v4, 0x5

    new-array v5, v4, [B

    const/16 v6, 0x1b

    const/4 v7, 0x0

    aput-byte v6, v5, v7

    const/16 v8, 0x1d

    const/4 v9, 0x1

    aput-byte v8, v5, v9

    const/16 v10, 0x50

    const/4 v11, 0x2

    aput-byte v10, v5, v11

    const/4 v12, 0x3

    const/16 v13, 0x32

    aput-byte v13, v5, v12

    aput-byte v2, v5, v3

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v2, v1, Landroid/graphics/Rect;->left:I

    rem-int/lit16 v2, v2, 0x100

    int-to-byte v2, v2

    iget v5, v1, Landroid/graphics/Rect;->left:I

    div-int/lit16 v5, v5, 0x100

    int-to-byte v5, v5

    iget v13, v1, Landroid/graphics/Rect;->top:I

    rem-int/lit16 v13, v13, 0x100

    int-to-byte v13, v13

    iget v14, v1, Landroid/graphics/Rect;->top:I

    div-int/lit16 v14, v14, 0x100

    int-to-byte v14, v14

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v15

    rem-int/lit16 v15, v15, 0x100

    int-to-byte v15, v15

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit16 v4, v4, 0x100

    int-to-byte v4, v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    rem-int/lit16 v3, v3, 0x100

    int-to-byte v3, v3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit16 v1, v1, 0x100

    int-to-byte v1, v1

    const/16 v12, 0xc

    new-array v12, v12, [B

    aput-byte v6, v12, v7

    aput-byte v8, v12, v9

    aput-byte v10, v12, v11

    const/16 v6, 0x33

    const/4 v7, 0x3

    aput-byte v6, v12, v7

    const/4 v6, 0x4

    aput-byte v2, v12, v6

    const/4 v2, 0x5

    aput-byte v5, v12, v2

    const/4 v2, 0x6

    aput-byte v13, v12, v2

    const/4 v2, 0x7

    aput-byte v14, v12, v2

    const/16 v2, 0x8

    aput-byte v15, v12, v2

    const/16 v2, 0x9

    aput-byte v4, v12, v2

    const/16 v2, 0xa

    aput-byte v3, v12, v2

    const/16 v2, 0xb

    aput-byte v1, v12, v2

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1dt
        0x50t
        0x30t
    .end array-data
.end method

.method static b(Ljava/util/List;Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Landroid/graphics/Rect;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Landroid/graphics/Rect;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/h$2;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/h$2;-><init>()V

    const/4 v1, 0x2

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Byte;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result p2

    const/4 v0, 0x3

    new-array v2, v0, [B

    const/16 v3, 0x1b

    const/4 v4, 0x0

    aput-byte v3, v2, v4

    const/4 v5, 0x1

    const/16 v6, 0x54

    aput-byte v6, v2, v5

    aput-byte p2, v2, v1

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget p2, p1, Landroid/graphics/Rect;->left:I

    rem-int/lit16 p2, p2, 0x100

    int-to-byte p2, p2

    iget v2, p1, Landroid/graphics/Rect;->left:I

    div-int/lit16 v2, v2, 0x100

    int-to-byte v2, v2

    iget v6, p1, Landroid/graphics/Rect;->top:I

    rem-int/lit16 v6, v6, 0x100

    int-to-byte v6, v6

    iget v7, p1, Landroid/graphics/Rect;->top:I

    div-int/lit16 v7, v7, 0x100

    int-to-byte v7, v7

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    rem-int/lit16 v8, v8, 0x100

    int-to-byte v8, v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v9

    div-int/lit16 v9, v9, 0x100

    int-to-byte v9, v9

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v10

    rem-int/lit16 v10, v10, 0x100

    int-to-byte v10, v10

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    div-int/lit16 p1, p1, 0x100

    int-to-byte p1, p1

    const/16 v11, 0xa

    new-array v11, v11, [B

    aput-byte v3, v11, v4

    const/16 v3, 0x57

    aput-byte v3, v11, v5

    aput-byte p2, v11, v1

    aput-byte v2, v11, v0

    const/4 p2, 0x4

    aput-byte v6, v11, p2

    const/4 p2, 0x5

    aput-byte v7, v11, p2

    const/4 p2, 0x6

    aput-byte v8, v11, p2

    const/4 p2, 0x7

    aput-byte v9, v11, p2

    const/16 p2, 0x8

    aput-byte v10, v11, p2

    const/16 p2, 0x9

    aput-byte p1, v11, p2

    invoke-interface {p0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x4ct
    .end array-data
.end method

.method static d(Ljava/util/List;Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Landroid/graphics/Rect;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
            ")V"
        }
    .end annotation

    return-void
.end method
