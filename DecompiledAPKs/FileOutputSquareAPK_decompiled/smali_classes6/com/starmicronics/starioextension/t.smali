.class Lcom/starmicronics/starioextension/t;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/t$a;
    }
.end annotation


# instance fields
.field private a:Lcom/starmicronics/starioextension/t$a;

.field private b:Lcom/starmicronics/starioextension/u;


# direct methods
.method constructor <init>(Lcom/starmicronics/starioextension/t$a;Lcom/starmicronics/starioextension/u;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/starmicronics/starioextension/t;->a:Lcom/starmicronics/starioextension/t$a;

    iput-object p2, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    sget-object v0, Lcom/starmicronics/starioextension/t$1;->a:[I

    iget-object v1, p0, Lcom/starmicronics/starioextension/t;->a:Lcom/starmicronics/starioextension/t$a;

    invoke-virtual {v1}, Lcom/starmicronics/starioextension/t$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_7

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/starmicronics/starioextension/ConnectionCallback;->onConnected(ZI)V

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_9

    :goto_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/IConnectionCallback$ConnectResult;->Failure:Lcom/starmicronics/starioextension/IConnectionCallback$ConnectResult;

    :goto_1
    invoke-interface {v0, v1}, Lcom/starmicronics/starioextension/IConnectionCallback;->onConnected(Lcom/starmicronics/starioextension/IConnectionCallback$ConnectResult;)V

    goto/16 :goto_2

    :cond_1
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/ConnectionCallback;->onDisconnected()V

    :cond_2
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/IConnectionCallback;->onDisconnected()V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    const/16 v1, -0x64

    invoke-virtual {v0, v2, v1}, Lcom/starmicronics/starioextension/ConnectionCallback;->onConnected(ZI)V

    :cond_4
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_9

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    const/4 v1, -0x2

    invoke-virtual {v0, v2, v1}, Lcom/starmicronics/starioextension/ConnectionCallback;->onConnected(ZI)V

    :cond_6
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/IConnectionCallback$ConnectResult;->AlreadyConnected:Lcom/starmicronics/starioextension/IConnectionCallback$ConnectResult;

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->a()Lcom/starmicronics/starioextension/ConnectionCallback;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/starmicronics/starioextension/ConnectionCallback;->onConnected(ZI)V

    :cond_8
    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/starmicronics/starioextension/t;->b:Lcom/starmicronics/starioextension/u;

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/u;->b()Lcom/starmicronics/starioextension/IConnectionCallback;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/IConnectionCallback$ConnectResult;->Success:Lcom/starmicronics/starioextension/IConnectionCallback$ConnectResult;

    goto/16 :goto_1

    :cond_9
    :goto_2
    return-void
.end method
