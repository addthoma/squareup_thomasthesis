.class Lcom/starmicronics/starioextension/bb;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    rem-int/lit16 v0, p1, 0x100

    int-to-byte v0, v0

    div-int/lit16 p1, p1, 0x100

    int-to-byte p1, p1

    const/4 v1, 0x6

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0x1d

    aput-byte v3, v1, v2

    const/4 v2, 0x2

    const/16 v3, 0x50

    aput-byte v3, v1, v2

    const/4 v2, 0x3

    const/16 v3, 0x34

    aput-byte v3, v1, v2

    const/4 v2, 0x4

    aput-byte v0, v1, v2

    const/4 v0, 0x5

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    rem-int/lit16 v0, p1, 0x100

    int-to-byte v0, v0

    div-int/lit16 p1, p1, 0x100

    int-to-byte p1, p1

    const/4 v1, 0x4

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1d

    aput-byte v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0x24

    aput-byte v3, v1, v2

    const/4 v2, 0x2

    aput-byte v0, v1, v2

    const/4 v0, 0x3

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    return-void
.end method
