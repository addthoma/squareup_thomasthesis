.class Lcom/starmicronics/starioextension/bc;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0x3

.field private static final b:I = 0x5a

.field private static final c:I = 0x1

.field private static final d:I = 0x1e


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[BII",
            "Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;",
            "II)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/starmicronics/starioextension/bc$1;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/bc$1;-><init>()V

    const/4 v3, 0x3

    const/4 v4, 0x0

    move/from16 v5, p2

    if-ge v5, v3, :cond_1

    const/4 v5, 0x0

    :cond_1
    const/16 v6, 0x5a

    if-le v5, v6, :cond_2

    const/16 v5, 0x5a

    :cond_2
    const/4 v6, 0x1

    move/from16 v7, p3

    if-ge v7, v6, :cond_3

    const/4 v7, 0x0

    :cond_3
    const/16 v8, 0x1e

    if-le v7, v8, :cond_4

    const/16 v7, 0x1e

    :cond_4
    if-nez v5, :cond_5

    if-nez v7, :cond_5

    const/4 v7, 0x1

    :cond_5
    const/4 v8, 0x2

    move/from16 v9, p5

    if-ge v9, v8, :cond_6

    const/4 v9, 0x2

    :cond_6
    const/16 v10, 0x8

    if-le v9, v10, :cond_7

    move/from16 v9, p6

    const/16 v11, 0x8

    goto :goto_0

    :cond_7
    move v11, v9

    move/from16 v9, p6

    :goto_0
    if-ge v9, v8, :cond_8

    const/4 v9, 0x2

    :cond_8
    if-le v9, v10, :cond_9

    const/16 v9, 0x8

    :cond_9
    const/16 v12, 0x1a

    new-array v12, v12, [B

    const/16 v13, 0x1b

    aput-byte v13, v12, v4

    const/16 v14, 0x1d

    aput-byte v14, v12, v6

    const/16 v15, 0x78

    aput-byte v15, v12, v8

    const/16 v16, 0x53

    aput-byte v16, v12, v3

    const/16 v17, 0x30

    const/4 v3, 0x4

    aput-byte v17, v12, v3

    const/16 v17, 0x5

    aput-byte v6, v12, v17

    int-to-byte v5, v5

    const/4 v3, 0x6

    aput-byte v5, v12, v3

    const/4 v5, 0x7

    int-to-byte v7, v7

    aput-byte v7, v12, v5

    aput-byte v13, v12, v10

    const/16 v5, 0x9

    aput-byte v14, v12, v5

    const/16 v5, 0xa

    aput-byte v15, v12, v5

    const/16 v5, 0xb

    aput-byte v16, v12, v5

    const/16 v5, 0xc

    const/16 v7, 0x31

    aput-byte v7, v12, v5

    const/16 v5, 0xd

    move-object/from16 v7, p4

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v12, v5

    const/16 v2, 0xe

    aput-byte v13, v12, v2

    const/16 v2, 0xf

    aput-byte v14, v12, v2

    const/16 v2, 0x10

    aput-byte v15, v12, v2

    const/16 v2, 0x11

    aput-byte v16, v12, v2

    const/16 v2, 0x12

    const/16 v5, 0x32

    aput-byte v5, v12, v2

    const/16 v2, 0x13

    int-to-byte v5, v11

    aput-byte v5, v12, v2

    const/16 v2, 0x14

    aput-byte v13, v12, v2

    const/16 v2, 0x15

    aput-byte v14, v12, v2

    const/16 v2, 0x16

    aput-byte v15, v12, v2

    const/16 v2, 0x17

    aput-byte v16, v12, v2

    const/16 v2, 0x18

    const/16 v5, 0x33

    aput-byte v5, v12, v2

    const/16 v2, 0x19

    int-to-byte v5, v9

    aput-byte v5, v12, v2

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v2, v1

    rem-int/lit16 v2, v2, 0x100

    array-length v5, v1

    div-int/lit16 v5, v5, 0x100

    new-array v3, v3, [B

    aput-byte v13, v3, v4

    aput-byte v14, v3, v6

    aput-byte v15, v3, v8

    const/16 v4, 0x44

    const/4 v6, 0x3

    aput-byte v4, v3, v6

    int-to-byte v2, v2

    const/4 v4, 0x4

    aput-byte v2, v3, v4

    int-to-byte v2, v5

    aput-byte v2, v3, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v1, v4, [B

    fill-array-data v1, :array_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x1dt
        0x78t
        0x50t
    .end array-data
.end method

.method static a(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;III)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[BII",
            "Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;",
            "III)V"
        }
    .end annotation

    move-object v0, p1

    move-object/from16 v1, p4

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/starmicronics/starioextension/bc$2;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/bc$2;-><init>()V

    const/4 v3, 0x2

    move/from16 v4, p5

    if-ge v4, v3, :cond_1

    const/4 v4, 0x2

    :cond_1
    const/16 v5, 0x8

    if-le v4, v5, :cond_2

    move/from16 v4, p6

    const/16 v6, 0x8

    goto :goto_0

    :cond_2
    move v6, v4

    move/from16 v4, p6

    :goto_0
    if-ge v4, v3, :cond_3

    goto :goto_1

    :cond_3
    move v3, v4

    :goto_1
    if-le v3, v5, :cond_4

    const/16 v3, 0x8

    :cond_4
    new-instance v4, Lcom/google/zxing/pdf417/encoder/PDF417;

    invoke-direct {v4}, Lcom/google/zxing/pdf417/encoder/PDF417;-><init>()V

    const/4 v5, 0x3

    const/4 v7, 0x0

    move/from16 v8, p2

    if-ge v8, v5, :cond_5

    const/4 v8, 0x0

    :cond_5
    const/16 v9, 0x5a

    if-le v8, v9, :cond_6

    const/16 v8, 0x5a

    :cond_6
    const/4 v10, 0x1

    move/from16 v11, p3

    if-ge v11, v10, :cond_7

    const/4 v11, 0x0

    :cond_7
    const/16 v12, 0x1e

    if-le v11, v12, :cond_8

    const/16 v11, 0x1e

    :cond_8
    const-string v13, "UTF-8"

    if-nez v8, :cond_9

    if-nez v11, :cond_9

    :try_start_0
    invoke-virtual {v4, v10, v10, v9, v5}, Lcom/google/zxing/pdf417/encoder/PDF417;->setDimensions(IIII)V

    goto :goto_2

    :cond_9
    if-nez v8, :cond_a

    invoke-virtual {v4, v11, v11, v9, v5}, Lcom/google/zxing/pdf417/encoder/PDF417;->setDimensions(IIII)V

    goto :goto_2

    :cond_a
    if-nez v11, :cond_b

    invoke-virtual {v4, v12, v10, v8, v8}, Lcom/google/zxing/pdf417/encoder/PDF417;->setDimensions(IIII)V

    goto :goto_2

    :cond_b
    invoke-virtual {v4, v11, v11, v8, v8}, Lcom/google/zxing/pdf417/encoder/PDF417;->setDimensions(IIII)V

    :goto_2
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, v13}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v4, v8, v11}, Lcom/google/zxing/pdf417/encoder/PDF417;->generateBarcodeLogic(Ljava/lang/String;I)V
    :try_end_0
    .catch Lcom/google/zxing/WriterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    :try_start_1
    invoke-virtual {v4, v12, v10, v9, v5}, Lcom/google/zxing/pdf417/encoder/PDF417;->setDimensions(IIII)V

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, p1, v13}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v5, v0}, Lcom/google/zxing/pdf417/encoder/PDF417;->generateBarcodeLogic(Ljava/lang/String;I)V
    :try_end_1
    .catch Lcom/google/zxing/WriterException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    invoke-virtual {v4}, Lcom/google/zxing/pdf417/encoder/PDF417;->getBarcodeMatrix()Lcom/google/zxing/pdf417/encoder/BarcodeMatrix;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/pdf417/encoder/BarcodeMatrix;->getMatrix()[[B

    move-result-object v0

    if-nez v0, :cond_c

    return-void

    :cond_c
    aget-object v1, v0, v7

    if-nez v1, :cond_d

    return-void

    :cond_d
    aget-object v1, v0, v7

    array-length v1, v1

    array-length v2, v0

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-ge v2, v4, :cond_10

    const/4 v4, 0x0

    :goto_5
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ge v4, v5, :cond_f

    aget-object v5, v0, v2

    aget-byte v5, v5, v4

    if-ne v5, v10, :cond_e

    const/high16 v5, -0x1000000

    goto :goto_6

    :cond_e
    const/4 v5, -0x1

    :goto_6
    invoke-virtual {v1, v4, v2, v5}, Landroid/graphics/Bitmap;->setPixel(III)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_10
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    mul-int v0, v0, v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int v2, v2, v6

    mul-int v2, v2, v3

    invoke-static {v1, v0, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Lcom/starmicronics/starioextension/j;

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x1

    sget-object v5, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    move-object p1, v1

    move-object/from16 p2, v0

    move/from16 p3, v2

    move/from16 p4, v3

    move/from16 p5, v4

    move-object/from16 p6, v5

    invoke-direct/range {p1 .. p6}, Lcom/starmicronics/starioextension/j;-><init>(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    move-object v0, p0

    move/from16 v2, p7

    invoke-static {p0, v1, v2, v10}, Lcom/starmicronics/starioextension/i;->e(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V

    :catch_1
    return-void
.end method

.method static b(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[BII",
            "Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;",
            "II)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/starmicronics/starioextension/bc$3;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/bc$3;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x3

    move/from16 v5, p2

    if-ge v5, v4, :cond_1

    const/4 v5, 0x0

    :cond_1
    const/16 v6, 0x5a

    if-le v5, v6, :cond_2

    const/16 v5, 0x5a

    :cond_2
    const/4 v6, 0x1

    move/from16 v7, p3

    if-ge v7, v6, :cond_3

    const/4 v7, 0x0

    :cond_3
    const/16 v8, 0x1e

    if-le v7, v8, :cond_4

    const/16 v7, 0x1e

    :cond_4
    if-nez v5, :cond_5

    if-nez v7, :cond_5

    const/4 v7, 0x1

    :cond_5
    const/4 v9, 0x2

    move/from16 v10, p5

    if-ge v10, v9, :cond_6

    const/4 v10, 0x2

    :cond_6
    const/16 v11, 0x8

    if-le v10, v11, :cond_7

    move/from16 v10, p6

    const/16 v12, 0x8

    goto :goto_0

    :cond_7
    move v12, v10

    move/from16 v10, p6

    :goto_0
    if-ge v10, v9, :cond_8

    const/4 v10, 0x2

    :cond_8
    if-le v10, v11, :cond_9

    const/16 v10, 0x8

    :cond_9
    const/16 v13, 0x29

    new-array v13, v13, [B

    const/16 v14, 0x1d

    aput-byte v14, v13, v3

    const/16 v15, 0x28

    aput-byte v15, v13, v6

    const/16 v16, 0x6b

    aput-byte v16, v13, v9

    aput-byte v4, v13, v4

    const/16 v17, 0x4

    aput-byte v3, v13, v17

    const/16 v18, 0x5

    const/16 v19, 0x30

    aput-byte v19, v13, v18

    const/16 v20, 0x41

    const/16 v21, 0x6

    aput-byte v20, v13, v21

    int-to-byte v7, v7

    const/16 v20, 0x7

    aput-byte v7, v13, v20

    aput-byte v14, v13, v11

    const/16 v7, 0x9

    aput-byte v15, v13, v7

    const/16 v7, 0xa

    aput-byte v16, v13, v7

    const/16 v7, 0xb

    aput-byte v4, v13, v7

    const/16 v7, 0xc

    aput-byte v3, v13, v7

    const/16 v7, 0xd

    aput-byte v19, v13, v7

    const/16 v7, 0xe

    const/16 v22, 0x42

    aput-byte v22, v13, v7

    const/16 v7, 0xf

    int-to-byte v5, v5

    aput-byte v5, v13, v7

    const/16 v5, 0x10

    aput-byte v14, v13, v5

    const/16 v5, 0x11

    aput-byte v15, v13, v5

    const/16 v5, 0x12

    aput-byte v16, v13, v5

    const/16 v5, 0x13

    aput-byte v4, v13, v5

    const/16 v5, 0x14

    aput-byte v3, v13, v5

    const/16 v5, 0x15

    aput-byte v19, v13, v5

    const/16 v5, 0x16

    const/16 v7, 0x43

    aput-byte v7, v13, v5

    const/16 v5, 0x17

    int-to-byte v7, v12

    aput-byte v7, v13, v5

    const/16 v5, 0x18

    aput-byte v14, v13, v5

    const/16 v5, 0x19

    aput-byte v15, v13, v5

    const/16 v5, 0x1a

    aput-byte v16, v13, v5

    const/16 v5, 0x1b

    aput-byte v4, v13, v5

    const/16 v5, 0x1c

    aput-byte v3, v13, v5

    aput-byte v19, v13, v14

    const/16 v5, 0x44

    aput-byte v5, v13, v8

    const/16 v5, 0x1f

    int-to-byte v7, v10

    aput-byte v7, v13, v5

    const/16 v5, 0x20

    aput-byte v14, v13, v5

    const/16 v5, 0x21

    aput-byte v15, v13, v5

    const/16 v5, 0x22

    aput-byte v16, v13, v5

    const/16 v5, 0x23

    aput-byte v17, v13, v5

    const/16 v5, 0x24

    aput-byte v3, v13, v5

    const/16 v5, 0x25

    aput-byte v19, v13, v5

    const/16 v5, 0x26

    const/16 v7, 0x45

    aput-byte v7, v13, v5

    const/16 v5, 0x27

    aput-byte v19, v13, v5

    move-object/from16 v5, p4

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v13, v15

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v2, v1

    add-int/2addr v2, v4

    rem-int/lit16 v2, v2, 0x100

    array-length v5, v1

    add-int/2addr v5, v4

    div-int/lit16 v5, v5, 0x100

    new-array v7, v11, [B

    aput-byte v14, v7, v3

    aput-byte v15, v7, v6

    aput-byte v16, v7, v9

    int-to-byte v2, v2

    aput-byte v2, v7, v4

    int-to-byte v2, v5

    aput-byte v2, v7, v17

    aput-byte v19, v7, v18

    const/16 v2, 0x50

    aput-byte v2, v7, v21

    aput-byte v19, v7, v20

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v1, v11, [B

    fill-array-data v1, :array_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    nop

    :array_0
    .array-data 1
        0x1dt
        0x28t
        0x6bt
        0x3t
        0x0t
        0x30t
        0x51t
        0x30t
    .end array-data
.end method

.method static c(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[BII",
            "Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;",
            "II)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/starmicronics/starioextension/bc$4;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/bc$4;-><init>()V

    const/4 v3, 0x1

    move/from16 v4, p3

    if-ge v4, v3, :cond_1

    const/4 v4, 0x1

    :cond_1
    const/16 v5, 0x1e

    if-le v4, v5, :cond_2

    const/16 v4, 0x1e

    :cond_2
    const/4 v5, 0x2

    move/from16 v6, p5

    if-ge v6, v5, :cond_3

    const/4 v6, 0x2

    :cond_3
    const/16 v7, 0x8

    if-le v6, v7, :cond_4

    goto :goto_0

    :cond_4
    move v7, v6

    :goto_0
    move/from16 v6, p6

    if-ge v6, v5, :cond_5

    const/4 v6, 0x2

    :cond_5
    const/4 v8, 0x5

    if-le v6, v8, :cond_6

    const/4 v6, 0x5

    :cond_6
    const/4 v9, 0x6

    new-array v10, v9, [B

    const/16 v11, 0x1d

    const/4 v12, 0x0

    aput-byte v11, v10, v12

    const/16 v13, 0x5a

    aput-byte v13, v10, v3

    aput-byte v12, v10, v5

    const/4 v14, 0x3

    aput-byte v11, v10, v14

    const/16 v11, 0x77

    const/4 v15, 0x4

    aput-byte v11, v10, v15

    int-to-byte v7, v7

    aput-byte v7, v10, v8

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v7, v1

    rem-int/lit16 v7, v7, 0x100

    array-length v10, v1

    div-int/lit16 v10, v10, 0x100

    const/4 v11, 0x7

    new-array v11, v11, [B

    const/16 v16, 0x1b

    aput-byte v16, v11, v12

    aput-byte v13, v11, v3

    int-to-byte v4, v4

    aput-byte v4, v11, v5

    move-object/from16 v4, p4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v11, v14

    int-to-byte v2, v6

    aput-byte v2, v11, v15

    int-to-byte v2, v7

    aput-byte v2, v11, v8

    int-to-byte v2, v10

    aput-byte v2, v11, v9

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v1, v3, [B

    const/16 v2, 0xa

    aput-byte v2, v1, v12

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[BII",
            "Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;",
            "II)V"
        }
    .end annotation

    return-void
.end method
