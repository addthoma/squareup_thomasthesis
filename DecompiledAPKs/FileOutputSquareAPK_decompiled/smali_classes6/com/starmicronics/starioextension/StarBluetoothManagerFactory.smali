.class public Lcom/starmicronics/starioextension/StarBluetoothManagerFactory;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getManager(Ljava/lang/String;Ljava/lang/String;ILcom/starmicronics/starioextension/StarIoExt$Emulation;)Lcom/starmicronics/stario/StarBluetoothManager;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/starioextension/StarBluetoothManagerFactory$1;->a:[I

    invoke-virtual {p3}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    if-eq p3, v0, :cond_0

    const/4 v0, 0x2

    if-eq p3, v0, :cond_0

    const/4 v0, 0x3

    if-eq p3, v0, :cond_0

    new-instance p3, Lcom/starmicronics/stario/StarBluetoothManager;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypePortablePrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    invoke-direct {p3, p0, p1, p2, v0}, Lcom/starmicronics/stario/StarBluetoothManager;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;)V

    return-object p3

    :cond_0
    new-instance p3, Lcom/starmicronics/stario/StarBluetoothManager;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypeDesktopPrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    invoke-direct {p3, p0, p1, p2, v0}, Lcom/starmicronics/stario/StarBluetoothManager;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;)V

    return-object p3
.end method
