.class public Lcom/starmicronics/starioextension/StarIoExt;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;,
        Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;,
        Lcom/starmicronics/starioextension/StarIoExt$BcrModel;,
        Lcom/starmicronics/starioextension/StarIoExt$CharacterCode;,
        Lcom/starmicronics/starioextension/StarIoExt$Emulation;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createBcrConnectParser(Lcom/starmicronics/starioextension/StarIoExt$BcrModel;)Lcom/starmicronics/starioextension/IPeripheralConnectParser;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->c:[I

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->ordinal()I

    move-result p0

    aget p0, v0, p0

    new-instance p0, Lcom/starmicronics/starioextension/f;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/f;-><init>()V

    return-object p0
.end method

.method public static createCommandBuilder(Lcom/starmicronics/starioextension/StarIoExt$Emulation;)Lcom/starmicronics/starioextension/ICommandBuilder;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    new-instance p0, Lcom/starmicronics/starioextension/bl;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/bl;-><init>()V

    return-object p0

    :pswitch_0
    new-instance p0, Lcom/starmicronics/starioextension/bi;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/bi;-><init>()V

    return-object p0

    :pswitch_1
    new-instance p0, Lcom/starmicronics/starioextension/ac;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/ac;-><init>()V

    return-object p0

    :pswitch_2
    new-instance p0, Lcom/starmicronics/starioextension/ab;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/ab;-><init>()V

    return-object p0

    :pswitch_3
    new-instance p0, Lcom/starmicronics/starioextension/bj;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/bj;-><init>()V

    return-object p0

    :pswitch_4
    new-instance p0, Lcom/starmicronics/starioextension/bk;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/bk;-><init>()V

    return-object p0

    :pswitch_5
    new-instance p0, Lcom/starmicronics/starioextension/bm;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/bm;-><init>()V

    return-object p0

    :pswitch_6
    new-instance p0, Lcom/starmicronics/starioextension/bl;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/bl;-><init>()V

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static createDisplayCommandBuilder(Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;)Lcom/starmicronics/starioextension/IDisplayCommandBuilder;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->b:[I

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;->ordinal()I

    move-result p0

    aget p0, v0, p0

    new-instance p0, Lcom/starmicronics/starioextension/bg;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/bg;-><init>()V

    return-object p0
.end method

.method public static createDisplayConnectParser(Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;)Lcom/starmicronics/starioextension/IPeripheralConnectParser;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->b:[I

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;->ordinal()I

    move-result p0

    aget p0, v0, p0

    new-instance p0, Lcom/starmicronics/starioextension/x;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/x;-><init>()V

    return-object p0
.end method

.method public static createMelodySpeakerCommandBuilder(Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;)Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->d:[I

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    new-instance p0, Lcom/starmicronics/starioextension/au;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/au;-><init>()V

    return-object p0

    :cond_0
    new-instance p0, Lcom/starmicronics/starioextension/ae;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/ae;-><init>()V

    return-object p0
.end method

.method public static createMelodySpeakerConnectParser(Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;)Lcom/starmicronics/starioextension/IPeripheralConnectParser;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->d:[I

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    new-instance p0, Lcom/starmicronics/starioextension/av;

    invoke-direct {p0}, Lcom/starmicronics/starioextension/av;-><init>()V

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Unsupported function in this model."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static getDescription()Ljava/lang/String;
    .locals 1

    const-string v0, "StarIO_Extension version 1.10.0"

    return-object v0
.end method
