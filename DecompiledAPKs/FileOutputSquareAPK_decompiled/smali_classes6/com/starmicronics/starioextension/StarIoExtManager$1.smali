.class Lcom/starmicronics/starioextension/StarIoExtManager$1;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/u;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starioextension/u;

.field final synthetic b:Lcom/starmicronics/starioextension/StarIoExtManager;


# direct methods
.method constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/u;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->b:Lcom/starmicronics/starioextension/StarIoExtManager;

    iput-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->a:Lcom/starmicronics/starioextension/u;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->b:Lcom/starmicronics/starioextension/StarIoExtManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Z)I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/starmicronics/starioextension/t;

    sget-object v1, Lcom/starmicronics/starioextension/t$a;->a:Lcom/starmicronics/starioextension/t$a;

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->a:Lcom/starmicronics/starioextension/u;

    invoke-direct {v0, v1, v2}, Lcom/starmicronics/starioextension/t;-><init>(Lcom/starmicronics/starioextension/t$a;Lcom/starmicronics/starioextension/u;)V

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->b:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_0
    const/16 v2, -0x64

    if-ne v0, v2, :cond_1

    new-instance v0, Lcom/starmicronics/starioextension/t;

    sget-object v2, Lcom/starmicronics/starioextension/t$a;->d:Lcom/starmicronics/starioextension/t$a;

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->a:Lcom/starmicronics/starioextension/u;

    invoke-direct {v0, v2, v3}, Lcom/starmicronics/starioextension/t;-><init>(Lcom/starmicronics/starioextension/t$a;Lcom/starmicronics/starioextension/u;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/starmicronics/starioextension/t;

    sget-object v2, Lcom/starmicronics/starioextension/t$a;->b:Lcom/starmicronics/starioextension/t$a;

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->a:Lcom/starmicronics/starioextension/u;

    invoke-direct {v0, v2, v3}, Lcom/starmicronics/starioextension/t;-><init>(Lcom/starmicronics/starioextension/t$a;Lcom/starmicronics/starioextension/u;)V

    :goto_0
    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->b:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v2}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->b:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->c(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->b:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v2}, Lcom/starmicronics/starioextension/StarIoExtManager;->b(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$1;->b:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->b(Lcom/starmicronics/starioextension/StarIoExtManager;Z)Z

    :goto_1
    return-void
.end method
