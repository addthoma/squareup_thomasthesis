.class public final enum Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/IPeripheralCommandParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ParseResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

.field public static final enum Failure:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

.field public static final enum Invalid:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

.field public static final enum Success:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    const/4 v1, 0x0

    const-string v2, "Invalid"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Invalid:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    new-instance v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    const/4 v2, 0x1

    const-string v3, "Success"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Success:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    new-instance v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    const/4 v3, 0x2

    const-string v4, "Failure"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Failure:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    sget-object v4, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Invalid:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    aput-object v4, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Success:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Failure:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    aput-object v1, v0, v3

    sput-object v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->$VALUES:[Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->$VALUES:[Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object v0
.end method
