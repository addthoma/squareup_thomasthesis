.class Lcom/starmicronics/stario/TCPPort;
.super Lcom/starmicronics/stario/StarIOPort;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/TCPPort$a;
    }
.end annotation


# instance fields
.field private A:[B

.field private B:Z

.field private C:I

.field private final D:Ljava/lang/Object;

.field private E:Z

.field private F:J

.field a:I

.field b:Ljava/lang/String;

.field c:Ljava/net/Socket;

.field d:Ljava/io/DataOutputStream;

.field e:Ljava/io/DataInputStream;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:I

.field i:Lcom/starmicronics/stario/StarPrinterStatus;

.field j:Z

.field k:Z

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:I

.field private r:Ljava/net/Socket;

.field private s:Ljava/io/DataOutputStream;

.field private t:Ljava/io/DataInputStream;

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/StarIOPort;-><init>()V

    const-string v0, "StarLine"

    iput-object v0, p0, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/starmicronics/stario/TCPPort;->m:Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/TCPPort;->n:Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/TCPPort;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/TCPPort;->o:Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->p:Z

    iput v1, p0, Lcom/starmicronics/stario/TCPPort;->q:I

    iput v1, p0, Lcom/starmicronics/stario/TCPPort;->u:I

    iput-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->j:Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/starmicronics/stario/TCPPort;->k:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->v:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->w:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->x:Z

    iput v1, p0, Lcom/starmicronics/stario/TCPPort;->y:I

    iput v1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    iput-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->B:Z

    iput v1, p0, Lcom/starmicronics/stario/TCPPort;->C:I

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/starmicronics/stario/TCPPort;->D:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->E:Z

    iput-object p1, p0, Lcom/starmicronics/stario/TCPPort;->f:Ljava/lang/String;

    iput-object p2, p0, Lcom/starmicronics/stario/TCPPort;->g:Ljava/lang/String;

    iput p3, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    iput p3, p0, Lcom/starmicronics/stario/TCPPort;->a:I

    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string p3, "PORTABLE"

    invoke-virtual {p1, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    const-string p3, "z"

    const-string v3, "n"

    const-string v4, ";"

    const-string v5, "a"

    if-eqz p1, :cond_3

    invoke-virtual {p2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length p2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_8

    aget-object v4, p1, v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v2, :cond_2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v2, :cond_0

    iput-boolean v2, p0, Lcom/starmicronics/stario/TCPPort;->j:Z

    goto :goto_1

    :cond_0
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v2, :cond_1

    iput-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->k:Z

    goto :goto_1

    :cond_1
    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v2, :cond_2

    iput-boolean v2, p0, Lcom/starmicronics/stario/TCPPort;->v:Z

    :cond_2
    :goto_1
    invoke-direct {p0, v4}, Lcom/starmicronics/stario/TCPPort;->b(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-ne p1, v2, :cond_4

    const/4 p1, 0x1

    goto :goto_2

    :cond_4
    const/4 p1, 0x0

    :goto_2
    iput-boolean p1, p0, Lcom/starmicronics/stario/TCPPort;->j:Z

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-ne p1, v2, :cond_5

    const/4 p1, 0x0

    goto :goto_3

    :cond_5
    const/4 p1, 0x1

    :goto_3
    iput-boolean p1, p0, Lcom/starmicronics/stario/TCPPort;->k:Z

    invoke-virtual {p2, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-ne p1, v2, :cond_6

    const/4 p1, 0x1

    goto :goto_4

    :cond_6
    const/4 p1, 0x0

    :goto_4
    iput-boolean p1, p0, Lcom/starmicronics/stario/TCPPort;->v:Z

    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string p3, "ESCPOS"

    invoke-virtual {p1, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_7

    iput-object p3, p0, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    goto :goto_5

    :cond_7
    iput-object v0, p0, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    :goto_5
    invoke-virtual {p2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length p2, p1

    :goto_6
    if-ge v1, p2, :cond_8

    aget-object p3, p1, v1

    invoke-direct {p0, p3}, Lcom/starmicronics/stario/TCPPort;->b(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_8
    new-instance p1, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {p1}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iput-object p1, p0, Lcom/starmicronics/stario/TCPPort;->i:Lcom/starmicronics/stario/StarPrinterStatus;

    iget-object p1, p0, Lcom/starmicronics/stario/TCPPort;->i:Lcom/starmicronics/stario/StarPrinterStatus;

    iput-boolean v2, p1, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    invoke-virtual {p0}, Lcom/starmicronics/stario/TCPPort;->b()V

    return-void
.end method

.method private a(IJ)I
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    long-to-int p2, v0

    sub-int/2addr p1, p2

    return p1
.end method

.method private a([B)I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/starmicronics/stario/NoReturnException;
        }
    .end annotation

    const/4 v0, 0x0

    aget-byte v1, p1, v0

    invoke-static {v1}, Lcom/starmicronics/stario/f;->a(B)I

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_c

    iget-object v3, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    const/4 v4, 0x7

    sub-int/2addr v1, v4

    invoke-virtual {v3, p1, v4, v1}, Ljava/io/DataInputStream;->read([BII)I

    move-result v3

    const-string v4, "ABS not found"

    if-ne v3, v1, :cond_b

    aget-byte v1, p1, v2

    const/16 v5, 0x80

    and-int/2addr v1, v5

    if-eq v1, v5, :cond_0

    return v3

    :cond_0
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    const/4 v3, 0x2

    invoke-virtual {v1, p1, v0, v3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v1

    if-ne v1, v3, :cond_a

    aget-byte v5, p1, v0

    and-int/lit16 v5, v5, 0xff

    aget-byte v6, p1, v2

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v5, v5, 0x8

    add-int/2addr v5, v6

    if-nez v5, :cond_1

    return v1

    :cond_1
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    const/4 v5, 0x6

    invoke-virtual {v1, p1, v0, v5}, Ljava/io/DataInputStream;->read([BII)I

    move-result v1

    if-ne v1, v5, :cond_9

    aget-byte v5, p1, v3

    const/4 v6, 0x5

    const/16 v7, 0x42

    const/4 v8, 0x4

    const/16 v9, 0x3a

    if-ne v5, v9, :cond_2

    const/4 v5, 0x3

    aget-byte v5, p1, v5

    if-ne v5, v7, :cond_2

    aget-byte v1, p1, v8

    and-int/lit16 v1, v1, 0xff

    aget-byte p1, p1, v6

    :goto_0
    and-int/lit16 p1, p1, 0xff

    goto :goto_1

    :cond_2
    aget-byte v5, p1, v8

    if-ne v5, v9, :cond_8

    aget-byte v5, p1, v6

    if-ne v5, v7, :cond_8

    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v1, p1, v0, v3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v1

    if-ne v1, v3, :cond_7

    aget-byte v1, p1, v0

    and-int/lit16 v1, v1, 0xff

    aget-byte p1, p1, v2

    goto :goto_0

    :goto_1
    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v1, p1

    add-int/lit8 p1, v1, 0x1

    new-array v3, p1, [B

    iget-object v5, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v5, v3, v0, p1}, Ljava/io/DataInputStream;->read([BII)I

    move-result v5

    if-ne v5, p1, :cond_6

    iget-boolean p1, p0, Lcom/starmicronics/stario/TCPPort;->B:Z

    if-nez p1, :cond_3

    invoke-direct {p0, v3, v1}, Lcom/starmicronics/stario/TCPPort;->c([BI)I

    move-result p1

    const/4 v4, -0x1

    if-eq p1, v4, :cond_3

    add-int/lit8 v4, p1, 0x1

    new-array v4, v4, [B

    iput-object v4, p0, Lcom/starmicronics/stario/TCPPort;->A:[B

    iput p1, p0, Lcom/starmicronics/stario/TCPPort;->C:I

    iput-boolean v2, p0, Lcom/starmicronics/stario/TCPPort;->B:Z

    :cond_3
    iget p1, p0, Lcom/starmicronics/stario/TCPPort;->C:I

    if-lez p1, :cond_5

    iget-object p1, p0, Lcom/starmicronics/stario/TCPPort;->A:[B

    iget v2, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    invoke-static {v3, v0, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    iget p1, p0, Lcom/starmicronics/stario/TCPPort;->C:I

    if-gt p1, v1, :cond_4

    iput v0, p0, Lcom/starmicronics/stario/TCPPort;->C:I

    iput-boolean v0, p0, Lcom/starmicronics/stario/TCPPort;->B:Z

    goto :goto_2

    :cond_4
    sub-int/2addr p1, v1

    iput p1, p0, Lcom/starmicronics/stario/TCPPort;->C:I

    goto :goto_2

    :cond_5
    iput-object v3, p0, Lcom/starmicronics/stario/TCPPort;->A:[B

    iput v1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    iput v0, p0, Lcom/starmicronics/stario/TCPPort;->C:I

    :goto_2
    iput v0, p0, Lcom/starmicronics/stario/TCPPort;->y:I

    return v5

    :cond_6
    new-instance p1, Lcom/starmicronics/stario/NoReturnException;

    invoke-direct {p1, v4}, Lcom/starmicronics/stario/NoReturnException;-><init>(Ljava/lang/String;)V

    iput v5, p1, Lcom/starmicronics/stario/NoReturnException;->a:I

    throw p1

    :cond_7
    new-instance p1, Lcom/starmicronics/stario/NoReturnException;

    invoke-direct {p1, v4}, Lcom/starmicronics/stario/NoReturnException;-><init>(Ljava/lang/String;)V

    iput v1, p1, Lcom/starmicronics/stario/NoReturnException;->a:I

    throw p1

    :cond_8
    return v1

    :cond_9
    new-instance p1, Lcom/starmicronics/stario/NoReturnException;

    invoke-direct {p1, v4}, Lcom/starmicronics/stario/NoReturnException;-><init>(Ljava/lang/String;)V

    iput v1, p1, Lcom/starmicronics/stario/NoReturnException;->a:I

    throw p1

    :cond_a
    new-instance p1, Lcom/starmicronics/stario/NoReturnException;

    invoke-direct {p1, v4}, Lcom/starmicronics/stario/NoReturnException;-><init>(Ljava/lang/String;)V

    iput v1, p1, Lcom/starmicronics/stario/NoReturnException;->a:I

    throw p1

    :cond_b
    new-instance p1, Lcom/starmicronics/stario/NoReturnException;

    invoke-direct {p1, v4}, Lcom/starmicronics/stario/NoReturnException;-><init>(Ljava/lang/String;)V

    iput v3, p1, Lcom/starmicronics/stario/NoReturnException;->a:I

    throw p1

    :cond_c
    new-instance v1, Lcom/starmicronics/stario/NoReturnException;

    new-array v2, v2, [Ljava/lang/Object;

    aget-byte p1, p1, v0

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    aput-object p1, v2, v0

    const-string p1, "ABS not found. The first byte of the status(0x%02x) was an unexpected value."

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/starmicronics/stario/NoReturnException;-><init>(Ljava/lang/String;)V

    iput v0, v1, Lcom/starmicronics/stario/NoReturnException;->a:I

    throw v1
.end method

.method static synthetic a(Lcom/starmicronics/stario/TCPPort;J)J
    .locals 0

    iput-wide p1, p0, Lcom/starmicronics/stario/TCPPort;->F:J

    return-wide p1
.end method

.method private declared-synchronized a(Z)Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v0}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/TCPPort;->b()V

    :cond_0
    const/4 v2, 0x1

    new-array v3, v2, [B

    const/4 v4, 0x0

    aput-byte v4, v3, v4

    array-length v5, v3

    invoke-virtual {v1, v3, v4, v5}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    const-string v3, "StarLine"

    iget-object v5, v1, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/16 v5, 0x10

    const/16 v6, 0x33

    const/4 v7, 0x3

    const/16 v8, 0x238d

    const/4 v9, 0x4

    const/4 v10, 0x2

    if-eqz v3, :cond_18

    :goto_0
    iget-boolean v3, v1, Lcom/starmicronics/stario/TCPPort;->w:Z

    const/16 v11, 0x8

    const/16 v12, 0x80

    const/4 v13, 0x7

    if-ne v3, v2, :cond_3

    iget v3, v1, Lcom/starmicronics/stario/TCPPort;->q:I

    if-lt v3, v10, :cond_1

    :goto_1
    iput v4, v1, Lcom/starmicronics/stario/TCPPort;->q:I

    goto :goto_2

    :cond_1
    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->available()I

    move-result v3

    if-nez v3, :cond_2

    iget v3, v1, Lcom/starmicronics/stario/TCPPort;->q:I

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    iget v3, v1, Lcom/starmicronics/stario/TCPPort;->q:I

    add-int/2addr v3, v2

    iput v3, v1, Lcom/starmicronics/stario/TCPPort;->q:I

    goto/16 :goto_7

    :cond_3
    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->available()I

    move-result v3

    if-nez v3, :cond_f

    :goto_2
    iget v3, v1, Lcom/starmicronics/stario/TCPPort;->u:I

    if-ne v3, v8, :cond_21

    iget-boolean v3, v1, Lcom/starmicronics/stario/TCPPort;->w:Z

    if-nez v3, :cond_21

    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->f:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_f
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    new-instance v14, Ljava/net/InetSocketAddress;

    invoke-direct {v14, v3, v8}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    new-instance v3, Ljava/net/Socket;

    invoke-direct {v3}, Ljava/net/Socket;-><init>()V

    iput-object v3, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    iget v8, v1, Lcom/starmicronics/stario/TCPPort;->h:I

    invoke-virtual {v3, v8}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    iget v8, v1, Lcom/starmicronics/stario/TCPPort;->h:I

    invoke-virtual {v3, v14, v8}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    new-instance v3, Ljava/io/DataOutputStream;

    iget-object v8, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    invoke-virtual {v8}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v3, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/DataInputStream;

    iget-object v8, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    invoke-virtual {v8}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v3, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;

    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;

    new-array v8, v6, [B

    const/16 v14, 0x32

    aput-byte v14, v8, v4

    aput-byte v4, v8, v2

    aput-byte v4, v8, v10

    aput-byte v4, v8, v7

    aput-byte v4, v8, v9

    const/4 v15, 0x5

    aput-byte v4, v8, v15

    const/4 v15, 0x6

    aput-byte v4, v8, v15

    aput-byte v4, v8, v13

    aput-byte v4, v8, v11

    const/16 v11, 0x9

    aput-byte v4, v8, v11

    const/16 v11, 0xa

    aput-byte v4, v8, v11

    const/16 v11, 0xb

    aput-byte v4, v8, v11

    const/16 v11, 0xc

    aput-byte v4, v8, v11

    const/16 v11, 0xd

    aput-byte v4, v8, v11

    const/16 v11, 0xe

    aput-byte v4, v8, v11

    const/16 v11, 0xf

    aput-byte v4, v8, v11

    aput-byte v4, v8, v5

    const/16 v11, 0x11

    aput-byte v4, v8, v11

    const/16 v11, 0x12

    aput-byte v4, v8, v11

    const/16 v11, 0x13

    aput-byte v4, v8, v11

    const/16 v11, 0x14

    aput-byte v4, v8, v11

    const/16 v11, 0x15

    aput-byte v4, v8, v11

    const/16 v11, 0x16

    aput-byte v4, v8, v11

    const/16 v11, 0x17

    aput-byte v4, v8, v11

    const/16 v11, 0x18

    aput-byte v4, v8, v11

    const/16 v11, 0x19

    aput-byte v4, v8, v11

    const/16 v11, 0x1a

    aput-byte v4, v8, v11

    const/16 v11, 0x1b

    aput-byte v4, v8, v11

    const/16 v11, 0x1c

    aput-byte v4, v8, v11

    const/16 v11, 0x1d

    aput-byte v4, v8, v11

    const/16 v11, 0x1e

    aput-byte v4, v8, v11

    const/16 v11, 0x1f

    aput-byte v4, v8, v11

    const/16 v11, 0x20

    aput-byte v4, v8, v11

    const/16 v11, 0x21

    aput-byte v4, v8, v11

    const/16 v11, 0x22

    aput-byte v4, v8, v11

    const/16 v11, 0x23

    aput-byte v4, v8, v11

    const/16 v11, 0x24

    aput-byte v4, v8, v11

    const/16 v11, 0x25

    aput-byte v4, v8, v11

    const/16 v11, 0x26

    aput-byte v4, v8, v11

    const/16 v11, 0x27

    aput-byte v4, v8, v11

    const/16 v11, 0x28

    aput-byte v4, v8, v11

    const/16 v11, 0x29

    aput-byte v4, v8, v11

    const/16 v11, 0x2a

    aput-byte v4, v8, v11

    const/16 v11, 0x2b

    aput-byte v4, v8, v11

    const/16 v11, 0x2c

    aput-byte v4, v8, v11

    const/16 v11, 0x2d

    aput-byte v4, v8, v11

    const/16 v11, 0x2e

    aput-byte v4, v8, v11

    const/16 v11, 0x2f

    aput-byte v4, v8, v11

    const/16 v11, 0x30

    aput-byte v4, v8, v11

    const/16 v11, 0x31

    aput-byte v4, v8, v11

    aput-byte v4, v8, v14

    invoke-virtual {v3, v8, v4, v6}, Ljava/io/DataOutputStream;->write([BII)V

    const/16 v3, 0x108

    new-array v3, v3, [B

    iget-object v6, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;

    array-length v8, v3

    invoke-virtual {v6, v3, v4, v8}, Ljava/io/DataInputStream;->read([BII)I

    move-result v6

    if-lt v6, v13, :cond_5

    aget-byte v5, v3, v4

    and-int/lit8 v5, v5, 0x11

    if-ne v5, v2, :cond_7

    aget-byte v2, v3, v2

    and-int/lit16 v2, v2, 0x91

    if-ne v2, v12, :cond_7

    aget-byte v2, v3, v10

    and-int/lit16 v2, v2, 0x91

    if-nez v2, :cond_7

    aget-byte v2, v3, v7

    and-int/lit16 v2, v2, 0x91

    if-nez v2, :cond_7

    aget-byte v2, v3, v9

    and-int/lit16 v2, v2, 0x91

    if-nez v2, :cond_7

    const/4 v2, 0x5

    aget-byte v2, v3, v2

    and-int/lit16 v2, v2, 0x91

    if-nez v2, :cond_7

    const/4 v2, 0x6

    aget-byte v2, v3, v2

    and-int/lit16 v2, v2, 0x91

    if-nez v2, :cond_7

    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v2, v2

    if-le v6, v2, :cond_4

    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v2, v2

    iput v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    goto :goto_3

    :cond_4
    iput v6, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    :goto_3
    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget v5, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    invoke-static {v3, v4, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0}, Lcom/starmicronics/stario/f;->c(Lcom/starmicronics/stario/StarPrinterStatus;)V

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarPrinterStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/starmicronics/stario/StarPrinterStatus;

    iput-object v0, v1, Lcom/starmicronics/stario/TCPPort;->i:Lcom/starmicronics/stario/StarPrinterStatus;

    goto :goto_4

    :cond_5
    if-ne v6, v9, :cond_b

    aget-byte v0, v3, v4

    and-int/lit16 v0, v0, 0x90

    if-ne v0, v5, :cond_6

    aget-byte v0, v3, v2

    and-int/lit16 v0, v0, 0x90

    if-nez v0, :cond_6

    aget-byte v0, v3, v10

    and-int/lit16 v0, v0, 0x90

    if-nez v0, :cond_6

    aget-byte v0, v3, v7

    and-int/lit16 v0, v0, 0x90

    if-nez v0, :cond_6

    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->i:Lcom/starmicronics/stario/StarPrinterStatus;

    iput-boolean v4, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_6
    if-eqz p1, :cond_a

    :cond_7
    :goto_4
    :try_start_3
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_f
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v0, :cond_8

    :try_start_4
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catch_0
    :cond_8
    :try_start_5
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_f
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v0, :cond_9

    :try_start_6
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catch_1
    :cond_9
    :try_start_7
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_f
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v0, :cond_21

    :try_start_8
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    :goto_5
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_e
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_c

    :cond_a
    :try_start_9
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed to read status"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed to read status"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_9
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    :try_start_a
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_3
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Cannot connect to printer"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :goto_6
    :try_start_b
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_f
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    if-eqz v2, :cond_c

    :try_start_c
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :catch_4
    :cond_c
    :try_start_d
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_f
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    if-eqz v2, :cond_d

    :try_start_e
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :catch_5
    :cond_d
    :try_start_f
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    if-eqz v2, :cond_e

    :try_start_10
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :catch_6
    :cond_e
    :try_start_11
    throw v0

    :cond_f
    :goto_7
    iget v3, v1, Lcom/starmicronics/stario/TCPPort;->u:I

    if-ne v3, v8, :cond_11

    iget-boolean v3, v1, Lcom/starmicronics/stario/TCPPort;->w:Z

    if-eqz v3, :cond_10

    goto :goto_8

    :cond_10
    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    iget-object v11, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget-object v12, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v12, v12

    invoke-virtual {v3, v11, v4, v12}, Ljava/io/DataInputStream;->read([BII)I

    goto/16 :goto_0

    :cond_11
    :goto_8
    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    iget-object v14, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    invoke-virtual {v3, v14, v4, v13}, Ljava/io/DataInputStream;->read([BII)I

    move-result v3

    if-ne v3, v13, :cond_17

    iput v13, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget-object v3, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/starmicronics/stario/f;->a(B)I

    move-result v3

    if-eqz v3, :cond_16

    iget-object v13, v1, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    iget-object v14, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget v15, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    add-int/lit8 v3, v3, -0x7

    invoke-virtual {v13, v14, v15, v3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v13

    if-ne v13, v3, :cond_15

    iget v3, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    add-int/2addr v3, v13

    iput v3, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget-object v3, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v3, v3, v2

    and-int/2addr v3, v12

    if-ne v3, v12, :cond_14

    const/16 v3, 0xc8

    new-array v3, v3, [B

    iget-object v12, v1, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v12, v3, v4, v10}, Ljava/io/DataInputStream;->read([BII)I

    move-result v12

    if-ne v12, v10, :cond_13

    aget-byte v12, v3, v2

    and-int/lit16 v12, v12, 0xff

    aget-byte v13, v3, v4

    and-int/lit16 v13, v13, 0xff

    shl-int/lit8 v11, v13, 0x8

    add-int/2addr v12, v11

    if-lez v12, :cond_14

    iget-object v11, v1, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v11, v3, v4, v12}, Ljava/io/DataInputStream;->read([BII)I

    move-result v3

    if-ne v3, v12, :cond_12

    goto :goto_9

    :cond_12
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed to get status"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed to get status"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    :goto_9
    invoke-static {v0}, Lcom/starmicronics/stario/f;->c(Lcom/starmicronics/stario/StarPrinterStatus;)V

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarPrinterStatus;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/starmicronics/stario/StarPrinterStatus;

    iput-object v3, v1, Lcom/starmicronics/stario/TCPPort;->i:Lcom/starmicronics/stario/StarPrinterStatus;

    goto/16 :goto_0

    :cond_15
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed to get status"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_16
    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    const-string v5, "Failed to get status. The first byte of the status(0x%02x) was an unexpected value."

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v4

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v5, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_17
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed to get status"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_18
    const-string v3, "ESCPOS"

    iget-object v11, v1, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->f:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_f
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    :try_start_12
    new-instance v11, Ljava/net/InetSocketAddress;

    invoke-direct {v11, v3, v8}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    new-instance v3, Ljava/net/Socket;

    invoke-direct {v3}, Ljava/net/Socket;-><init>()V

    iput-object v3, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    iget v8, v1, Lcom/starmicronics/stario/TCPPort;->h:I

    invoke-virtual {v3, v8}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    iget v8, v1, Lcom/starmicronics/stario/TCPPort;->h:I

    invoke-virtual {v3, v11, v8}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    new-instance v3, Ljava/io/DataOutputStream;

    iget-object v8, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    invoke-virtual {v8}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v3, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/DataInputStream;

    iget-object v8, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    invoke-virtual {v8}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v3, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;

    iget-object v3, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;

    new-array v8, v2, [B

    aput-byte v6, v8, v4

    invoke-virtual {v3, v8, v4, v2}, Ljava/io/DataOutputStream;->write([BII)V

    new-array v3, v9, [B

    iget-object v6, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;

    array-length v8, v3

    invoke-virtual {v6, v3, v4, v8}, Ljava/io/DataInputStream;->read([BII)I

    move-result v6

    if-ne v6, v9, :cond_1b

    aget-byte v8, v3, v4

    and-int/lit16 v8, v8, 0x90

    if-ne v8, v5, :cond_1a

    aget-byte v2, v3, v2

    and-int/lit16 v2, v2, 0x90

    if-nez v2, :cond_1a

    aget-byte v2, v3, v10

    and-int/lit16 v2, v2, 0x90

    if-nez v2, :cond_1a

    aget-byte v2, v3, v7

    and-int/lit16 v2, v2, 0x90

    if-nez v2, :cond_1a

    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v2, v2

    if-le v6, v2, :cond_19

    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v2, v2

    iput v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    goto :goto_a

    :cond_19
    iput v6, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    :cond_1a
    :goto_a
    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget v5, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    invoke-static {v3, v4, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0}, Lcom/starmicronics/stario/f;->a(Lcom/starmicronics/stario/StarPrinterStatus;)V

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarPrinterStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/starmicronics/stario/StarPrinterStatus;

    iput-object v0, v1, Lcom/starmicronics/stario/TCPPort;->i:Lcom/starmicronics/stario/StarPrinterStatus;
    :try_end_12
    .catch Ljava/net/UnknownHostException; {:try_start_12 .. :try_end_12} :catch_a
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_9
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :cond_1b
    :try_start_13
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_f
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    if-eqz v0, :cond_1c

    :try_start_14
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_7
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    :catch_7
    :cond_1c
    :try_start_15
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_f
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    if-eqz v0, :cond_1d

    :try_start_16
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_8
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    :catch_8
    :cond_1d
    :try_start_17
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_f
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    if-eqz v0, :cond_21

    :try_start_18
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_e
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    goto/16 :goto_5

    :catchall_1
    move-exception v0

    goto :goto_b

    :catch_9
    move-exception v0

    :try_start_19
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_a
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Cannot connect to printer"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    :goto_b
    :try_start_1a
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_f
    .catchall {:try_start_1a .. :try_end_1a} :catchall_2

    if-eqz v2, :cond_1e

    :try_start_1b
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->s:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_2

    :catch_b
    :cond_1e
    :try_start_1c
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_f
    .catchall {:try_start_1c .. :try_end_1c} :catchall_2

    if-eqz v2, :cond_1f

    :try_start_1d
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->t:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_c
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    :catch_c
    :cond_1f
    :try_start_1e
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_f
    .catchall {:try_start_1e .. :try_end_1e} :catchall_2

    if-eqz v2, :cond_20

    :try_start_1f
    iget-object v2, v1, Lcom/starmicronics/stario/TCPPort;->r:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_d
    .catchall {:try_start_1f .. :try_end_1f} :catchall_2

    :catch_d
    :cond_20
    :try_start_20
    throw v0
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_f
    .catchall {:try_start_20 .. :try_end_20} :catchall_2

    :catch_e
    :cond_21
    :goto_c
    :try_start_21
    iget-object v0, v1, Lcom/starmicronics/stario/TCPPort;->i:Lcom/starmicronics/stario/StarPrinterStatus;
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_2

    monitor-exit p0

    return-object v0

    :catch_f
    :try_start_22
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed to get status"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_2

    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(J)V
    .locals 0

    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(ZI)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, " is busy."

    const-string v1, "TCP Port "

    if-eqz p1, :cond_0

    new-instance p1, Lcom/starmicronics/stario/StarConnectionRejectedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarConnectionRejectedException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic a(Lcom/starmicronics/stario/TCPPort;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/starmicronics/stario/TCPPort;->E:Z

    return p0
.end method

.method static a(Ljava/lang/String;)Z
    .locals 3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    const-string v0, "TCP:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    return v2
.end method

.method private declared-synchronized a(Ljava/lang/String;I)Z
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    move/from16 v0, p2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    int-to-long v6, v0

    add-long/2addr v4, v6

    :try_start_1
    invoke-static/range {p1 .. p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/16 v7, 0x56ce

    const/16 v8, 0x200

    :try_start_2
    new-array v9, v8, [B

    const/16 v10, 0x1c

    new-array v11, v10, [B

    const/16 v12, 0x53

    const/4 v13, 0x0

    aput-byte v12, v11, v13

    const/16 v14, 0x54

    const/4 v15, 0x1

    aput-byte v14, v11, v15

    const/16 v16, 0x2

    const/16 v17, 0x52

    aput-byte v17, v11, v16

    const/16 v16, 0x3

    const/16 v18, 0x5f

    aput-byte v18, v11, v16

    const/16 v16, 0x4

    const/16 v18, 0x42

    aput-byte v18, v11, v16

    const/16 v16, 0x5

    const/16 v18, 0x43

    aput-byte v18, v11, v16

    const/16 v16, 0x6

    const/16 v18, 0x41

    aput-byte v18, v11, v16

    const/16 v16, 0x7

    aput-byte v12, v11, v16

    const/16 v12, 0x8

    aput-byte v14, v11, v12

    const/16 v12, 0x9

    aput-byte v13, v11, v12

    const/16 v12, 0xa

    aput-byte v13, v11, v12

    const/16 v12, 0xb

    aput-byte v13, v11, v12

    const/16 v12, 0xc

    aput-byte v13, v11, v12

    const/16 v12, 0xd

    aput-byte v13, v11, v12

    const/16 v12, 0xe

    aput-byte v13, v11, v12

    const/16 v12, 0xf

    aput-byte v13, v11, v12

    const/16 v12, 0x10

    aput-byte v17, v11, v12

    const/16 v12, 0x11

    const/16 v14, 0x51

    aput-byte v14, v11, v12

    const/16 v12, 0x12

    const/16 v14, 0x31

    aput-byte v14, v11, v12

    const/16 v12, 0x13

    const/16 v16, 0x2e

    aput-byte v16, v11, v12

    const/16 v12, 0x14

    const/16 v17, 0x30

    aput-byte v17, v11, v12

    const/16 v12, 0x15

    aput-byte v16, v11, v12

    const/16 v12, 0x16

    aput-byte v17, v11, v12

    const/16 v12, 0x17

    aput-byte v13, v11, v12

    const/16 v12, 0x18

    aput-byte v13, v11, v12

    const/16 v12, 0x19

    aput-byte v10, v11, v12

    const/16 v10, 0x1a

    const/16 v12, 0x64

    aput-byte v12, v11, v10

    const/16 v10, 0x1b

    aput-byte v14, v11, v10

    :goto_0
    invoke-direct {v1, v0, v2, v3}, Lcom/starmicronics/stario/TCPPort;->a(IJ)I

    move-result v10

    const/16 v14, 0x7d0

    invoke-static {v14, v10}, Ljava/lang/Math;->min(II)I

    move-result v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-gtz v10, :cond_0

    monitor-exit p0

    return v13

    :cond_0
    :try_start_3
    new-instance v14, Ljava/net/DatagramPacket;

    array-length v15, v11

    invoke-direct {v14, v11, v15, v6, v7}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    new-instance v15, Ljava/net/DatagramSocket;

    invoke-direct {v15, v7}, Ljava/net/DatagramSocket;-><init>(I)V
    :try_end_4
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-virtual {v15, v10}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    invoke-virtual {v15, v14}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_5
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    new-instance v10, Ljava/net/DatagramPacket;

    invoke-direct {v10, v9, v8}, Ljava/net/DatagramPacket;-><init>([BI)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    invoke-virtual {v15, v10}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    invoke-virtual {v10}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v10

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v12, :cond_2

    add-int/lit8 v16, v14, 0x24

    aget-byte v16, v10, v16

    if-nez v16, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    new-array v7, v14, [B

    const/16 v8, 0x24

    invoke-static {v10, v8, v7, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v7}, Ljava/lang/String;-><init>([B)V

    iput-object v8, v1, Lcom/starmicronics/stario/TCPPort;->b:Ljava/lang/String;

    const-string v7, "IFBD-HE05/06"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/16 v7, 0x238c

    iput v7, v1, Lcom/starmicronics/stario/TCPPort;->u:I

    goto :goto_5

    :cond_3
    const-string v7, "DK-AirCash"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    const/16 v14, 0x238d

    if-eqz v7, :cond_6

    iput v14, v1, Lcom/starmicronics/stario/TCPPort;->u:I

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v12, :cond_5

    add-int/lit8 v8, v7, 0x34

    aget-byte v8, v10, v8

    if-nez v8, :cond_4

    goto :goto_4

    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_5
    :goto_4
    new-array v8, v7, [B

    const/16 v14, 0x35

    add-int/lit8 v7, v7, -0x1

    invoke-static {v10, v14, v8, v13, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>([B)V

    iput-object v7, v1, Lcom/starmicronics/stario/TCPPort;->o:Ljava/lang/String;

    goto :goto_5

    :cond_6
    const-string v7, "TSP100LAN"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    iput v14, v1, Lcom/starmicronics/stario/TCPPort;->u:I

    invoke-direct {v1, v10}, Lcom/starmicronics/stario/TCPPort;->b([B)Z

    move-result v7

    iput-boolean v7, v1, Lcom/starmicronics/stario/TCPPort;->p:Z

    goto :goto_5

    :cond_7
    iput v14, v1, Lcom/starmicronics/stario/TCPPort;->u:I
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_5
    :try_start_8
    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    monitor-exit p0

    const/4 v7, 0x1

    return v7

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_0
    const/16 v0, 0x238c

    :try_start_9
    iput v0, v1, Lcom/starmicronics/stario/TCPPort;->u:I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    monitor-exit p0

    return v13

    :goto_6
    :try_start_b
    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V

    throw v0

    :catch_1
    const/4 v7, 0x1

    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    cmp-long v8, v14, v4

    if-ltz v8, :cond_8

    monitor-exit p0

    return v13

    :cond_8
    const/16 v7, 0x56ce

    const/16 v8, 0x200

    const/4 v15, 0x1

    goto/16 :goto_0

    :catch_2
    nop

    goto :goto_7

    :catch_3
    nop

    goto :goto_8

    :catch_4
    const/4 v15, 0x0

    :goto_7
    if-eqz v15, :cond_9

    :try_start_c
    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V

    :cond_9
    const/16 v0, 0x238c

    iput v0, v1, Lcom/starmicronics/stario/TCPPort;->u:I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    monitor-exit p0

    return v13

    :catch_5
    const/4 v15, 0x0

    :goto_8
    if-eqz v15, :cond_a

    :try_start_d
    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V

    :cond_a
    const/16 v0, 0x238c

    iput v0, v1, Lcom/starmicronics/stario/TCPPort;->u:I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    monitor-exit p0

    return v13

    :catch_6
    :try_start_e
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed to find printer"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a([BI)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x7

    if-eq p2, v1, :cond_0

    return v0

    :cond_0
    aget-byte p2, p1, v0

    and-int/lit8 p2, p2, 0x11

    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    aget-byte p2, p1, v1

    and-int/lit16 p2, p2, 0x91

    const/16 v2, 0x80

    if-ne p2, v2, :cond_1

    const/4 p2, 0x2

    aget-byte p2, p1, p2

    and-int/lit16 p2, p2, 0x91

    if-nez p2, :cond_1

    const/4 p2, 0x3

    aget-byte p2, p1, p2

    and-int/lit16 p2, p2, 0x91

    if-nez p2, :cond_1

    const/4 p2, 0x4

    aget-byte p2, p1, p2

    and-int/lit16 p2, p2, 0x91

    if-nez p2, :cond_1

    const/4 p2, 0x5

    aget-byte p2, p1, p2

    and-int/lit16 p2, p2, 0x91

    if-nez p2, :cond_1

    const/4 p2, 0x6

    aget-byte p1, p1, p2

    and-int/lit16 p1, p1, 0x91

    if-nez p1, :cond_1

    return v1

    :cond_1
    return v0
.end method

.method private a([B[B)[B
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    array-length v0, p1

    array-length v1, p2

    add-int/2addr v0, v1

    new-array v0, v0, [B

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, p1

    array-length v3, p2

    invoke-static {p2, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :try_start_0
    array-length v1, v0

    invoke-virtual {p0, v0, v2, v1}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_2

    iget v0, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/16 v1, 0x64

    new-array v1, v1, [B

    const/4 v5, 0x0

    :goto_1
    :try_start_1
    iget-object v6, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->available()I

    move-result v6

    if-lez v6, :cond_3

    iget-object v6, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    array-length v7, v1

    sub-int/2addr v7, v5

    invoke-virtual {v6, v1, v5, v7}, Ljava/io/DataInputStream;->read([BII)I

    move-result v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v7, 0x7

    if-lt v6, v7, :cond_2

    new-array v0, v6, [B

    invoke-static {v1, v2, v0, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0, p1, p2}, Lcom/starmicronics/stario/f;->a([B[B[B)[B

    move-result-object p1

    if-eqz p1, :cond_1

    return-object p1

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to parse dip-switch condition."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    add-int/2addr v5, v6

    :cond_3
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v3

    int-to-long v8, v0

    cmp-long v10, v6, v8

    if-gtz v10, :cond_4

    goto :goto_1

    :cond_4
    new-instance p1, Ljava/util/concurrent/TimeoutException;

    const-string p2, "There was no response of the device within the timeout period."

    invoke-direct {p1, p2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p1

    new-instance p2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_2
    move-exception p1

    new-instance p2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method static synthetic b(Lcom/starmicronics/stario/TCPPort;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/TCPPort;->D:Ljava/lang/Object;

    return-object p0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    const-string v0, "l"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "910"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v2, v0, 0x4

    if-lt v1, v2, :cond_1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/starmicronics/stario/TCPPort;->u:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/starmicronics/stario/TCPPort;->w:Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    iput p1, p0, Lcom/starmicronics/stario/TCPPort;->u:I

    :cond_1
    :goto_0
    return-void
.end method

.method private b([BI)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0xc8

    new-array v1, v0, [B

    const/4 v2, 0x0

    const/4 v3, 0x7

    if-ne p2, v3, :cond_0

    iget-object v3, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    sub-int/2addr v0, p2

    invoke-virtual {v3, v1, v2, v0}, Ljava/io/DataInputStream;->read([BII)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int v3, p2, v0

    new-array v4, v3, [B

    iput-object v4, p0, Lcom/starmicronics/stario/TCPPort;->A:[B

    const/4 v4, 0x0

    :goto_1
    if-ge v4, p2, :cond_1

    iget-object v5, p0, Lcom/starmicronics/stario/TCPPort;->A:[B

    aget-byte v6, p1, v4

    aput-byte v6, v5, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_2
    if-ge p1, v0, :cond_2

    iget-object v4, p0, Lcom/starmicronics/stario/TCPPort;->A:[B

    add-int v5, p1, p2

    aget-byte v6, v1, p1

    aput-byte v6, v4, v5

    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_2
    iput v3, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    iput v2, p0, Lcom/starmicronics/stario/TCPPort;->y:I

    return-void
.end method

.method private b([B)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0x40

    if-ge v1, v2, :cond_1

    add-int/lit16 v2, v1, 0xcc

    aget-byte v2, p1, v2

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/String;

    const/16 v3, 0xcc

    invoke-direct {v2, p1, v3, v1}, Ljava/lang/String;-><init>([BII)V

    const-string p1, "TSP143 (STR_T-001)"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "TSP113 (STR_T-001)"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method private c([BI)I
    .locals 1

    invoke-static {p1, p2}, Lcom/starmicronics/stario/f;->b([BI)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Lcom/starmicronics/stario/f;->e([BI)I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-static {p1, p2}, Lcom/starmicronics/stario/f;->c([BI)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1, p2}, Lcom/starmicronics/stario/f;->f([BI)I

    move-result p1

    goto :goto_0

    :cond_1
    invoke-static {p1, p2}, Lcom/starmicronics/stario/f;->d([BI)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1, p2}, Lcom/starmicronics/stario/f;->g([BI)I

    move-result p1

    goto :goto_0

    :cond_2
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method static synthetic c(Lcom/starmicronics/stario/TCPPort;)J
    .locals 2

    iget-wide v0, p0, Lcom/starmicronics/stario/TCPPort;->F:J

    return-wide v0
.end method

.method static c()Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/starmicronics/stario/PortInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v1, "."

    const-string v2, ":"

    const-string v3, "%1$02x"

    const/16 v4, 0x1c

    new-array v4, v4, [B

    fill-array-data v4, :array_0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object v10, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_0
    const/4 v11, 0x2

    if-ge v8, v11, :cond_9

    const/16 v11, 0x56ce

    :try_start_0
    new-instance v12, Ljava/net/DatagramSocket;

    invoke-direct {v12, v11}, Ljava/net/DatagramSocket;-><init>(I)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v10, "255.255.255.255"

    invoke-static {v10}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v10

    new-instance v13, Ljava/net/DatagramPacket;

    array-length v14, v4

    invoke-direct {v13, v4, v14, v10, v11}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    invoke-virtual {v12, v13}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    :goto_1
    const/16 v13, 0x400

    new-array v14, v13, [B

    new-instance v15, Ljava/net/DatagramPacket;

    invoke-direct {v15, v14, v13}, Ljava/net/DatagramPacket;-><init>([BI)V

    const/16 v13, 0xbb8

    invoke-virtual {v12, v13}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    invoke-virtual {v12, v15}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v16, 0x58

    aget-byte v13, v14, v16

    and-int/lit16 v13, v13, 0xff

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v13, 0x59

    aget-byte v13, v14, v13

    and-int/lit16 v13, v13, 0xff

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v13, 0x5a

    aget-byte v13, v14, v13

    and-int/lit16 v13, v13, 0xff

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v13, 0x5b

    aget-byte v13, v14, v13

    and-int/lit16 v13, v13, 0xff

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TCP:"

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v15, "0.0.0.0"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_5

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    new-array v15, v6, [Ljava/lang/Object;

    const/16 v17, 0x4e

    aget-byte v6, v14, v17

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v16, 0x0

    :try_start_2
    aput-object v6, v15, v16
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {v3, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x1

    new-array v15, v6, [Ljava/lang/Object;

    const/16 v6, 0x4f

    aget-byte v6, v14, v6

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/16 v16, 0x0

    :try_start_4
    aput-object v6, v15, v16
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-static {v3, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x1

    new-array v15, v6, [Ljava/lang/Object;

    const/16 v6, 0x50

    aget-byte v6, v14, v6

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/16 v16, 0x0

    :try_start_6
    aput-object v6, v15, v16
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-static {v3, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x1

    new-array v15, v6, [Ljava/lang/Object;

    const/16 v6, 0x51

    aget-byte v6, v14, v6

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/16 v16, 0x0

    :try_start_8
    aput-object v6, v15, v16
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/net/SocketException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-static {v3, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x1

    new-array v15, v6, [Ljava/lang/Object;

    const/16 v6, 0x52

    aget-byte v6, v14, v6

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6
    :try_end_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/16 v16, 0x0

    :try_start_a
    aput-object v6, v15, v16
    :try_end_a
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/net/SocketException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    invoke-static {v3, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x1

    new-array v15, v6, [Ljava/lang/Object;

    const/16 v17, 0x53

    aget-byte v6, v14, v17

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6
    :try_end_b
    .catch Ljava/net/SocketTimeoutException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/net/SocketException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    const/16 v16, 0x0

    :try_start_c
    aput-object v6, v15, v16

    invoke-static {v3, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v13, 0x0

    :goto_2
    const/16 v15, 0x40

    if-ge v13, v15, :cond_1

    add-int/lit16 v15, v13, 0xcc

    aget-byte v15, v14, v15

    if-nez v15, :cond_0

    goto :goto_3

    :cond_0
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_1
    :goto_3
    new-instance v15, Ljava/lang/String;
    :try_end_c
    .catch Ljava/net/SocketTimeoutException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/net/SocketException; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-object/from16 v18, v1

    const/16 v1, 0xcc

    :try_start_d
    invoke-direct {v15, v14, v1, v13}, Ljava/lang/String;-><init>([BII)V
    :try_end_d
    .catch Ljava/net/SocketTimeoutException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/net/SocketException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    const-string v1, ""

    if-eqz v9, :cond_4

    :try_start_e
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    const/4 v14, 0x0

    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/starmicronics/stario/PortInfo;
    :try_end_e
    .catch Ljava/net/SocketTimeoutException; {:try_start_e .. :try_end_e} :catch_1
    .catch Ljava/net/SocketException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-object/from16 v19, v2

    :try_start_f
    invoke-virtual/range {v17 .. v17}, Lcom/starmicronics/stario/PortInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v2, v19

    const/4 v14, 0x1

    goto :goto_4

    :cond_2
    move-object/from16 v2, v19

    goto :goto_4

    :cond_3
    move-object/from16 v19, v2

    if-nez v14, :cond_6

    new-instance v2, Lcom/starmicronics/stario/PortInfo;

    invoke-direct {v2, v7, v6, v15, v1}, Lcom/starmicronics/stario/PortInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_5
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_4
    move-object/from16 v19, v2

    new-instance v2, Lcom/starmicronics/stario/PortInfo;

    invoke-direct {v2, v7, v6, v15, v1}, Lcom/starmicronics/stario/PortInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :catch_0
    move-object/from16 v18, v1

    :catch_1
    move-object/from16 v19, v2

    goto/16 :goto_b

    :cond_5
    move-object/from16 v18, v1

    move-object/from16 v19, v2

    const/16 v16, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1
    :try_end_f
    .catch Ljava/net/SocketTimeoutException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/net/SocketException; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    sub-long/2addr v1, v10

    const/16 v6, 0xbb8

    int-to-long v6, v6

    cmp-long v13, v1, v6

    if-lez v13, :cond_6

    goto/16 :goto_c

    :cond_6
    :goto_6
    move-object/from16 v1, v18

    move-object/from16 v2, v19

    const/4 v6, 0x1

    goto/16 :goto_1

    :catch_2
    nop

    goto :goto_b

    :catchall_0
    move-exception v0

    move-object v1, v0

    goto :goto_a

    :catch_3
    move-exception v0

    move-object v1, v0

    move-object v10, v12

    goto :goto_7

    :catch_4
    move-exception v0

    move-object v1, v0

    move-object v10, v12

    goto :goto_8

    :catch_5
    move-exception v0

    move-object v1, v0

    move-object v10, v12

    goto :goto_9

    :catch_6
    move-object/from16 v18, v1

    move-object/from16 v19, v2

    const/16 v16, 0x0

    goto :goto_b

    :catchall_1
    move-exception v0

    move-object v1, v0

    move-object v12, v10

    goto :goto_a

    :catch_7
    move-exception v0

    move-object v1, v0

    :goto_7
    :try_start_10
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_8
    move-exception v0

    move-object v1, v0

    :goto_8
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v1}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_9
    move-exception v0

    move-object v1, v0

    :goto_9
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v1}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :goto_a
    if-eqz v12, :cond_7

    invoke-virtual {v12}, Ljava/net/DatagramSocket;->close()V

    :cond_7
    throw v1

    :catch_a
    move-object/from16 v18, v1

    move-object/from16 v19, v2

    const/16 v16, 0x0

    move-object v12, v10

    :goto_b
    if-eqz v12, :cond_8

    :goto_c
    invoke-virtual {v12}, Ljava/net/DatagramSocket;->close()V

    :cond_8
    move-object v10, v12

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    const/4 v6, 0x1

    const/4 v9, 0x1

    goto/16 :goto_0

    :cond_9
    return-object v5

    nop

    :array_0
    .array-data 1
        0x53t
        0x54t
        0x52t
        0x5ft
        0x42t
        0x43t
        0x41t
        0x53t
        0x54t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x52t
        0x51t
        0x31t
        0x2et
        0x30t
        0x2et
        0x30t
        0x0t
        0x0t
        0x1ct
        0x64t
        0x31t
    .end array-data
.end method

.method private d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->b:Ljava/lang/String;

    const-string v1, "DK-AirCash"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->o:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/TCPPort;->f()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private e()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    :try_start_0
    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_2

    iget v0, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/16 v1, 0x64

    new-array v1, v1, [B

    const/4 v5, 0x4

    new-array v5, v5, [B

    fill-array-data v5, :array_1

    const/4 v6, 0x2

    new-array v6, v6, [B

    fill-array-data v6, :array_2

    :goto_1
    :try_start_1
    array-length v7, v1

    invoke-virtual {p0, v1, v2, v7}, Lcom/starmicronics/stario/TCPPort;->readPort([BII)I

    move-result v7

    if-eqz v7, :cond_2

    new-array v8, v7, [B

    invoke-static {v1, v2, v8, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v8, v5, v6}, Lcom/starmicronics/stario/f;->a([B[B[B)[B

    move-result-object v7
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v7, :cond_2

    if-eqz v7, :cond_1

    invoke-static {v7}, Lcom/starmicronics/stario/f;->b([B)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/stario/f$a;->a:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v1}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/TCPPort;->m:Ljava/lang/String;

    sget-object v1, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v1}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/TCPPort;->n:Ljava/lang/String;

    return-void

    :cond_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Failed to parse model and version"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v3

    int-to-long v9, v0

    cmp-long v11, v7, v9

    if-gtz v11, :cond_3

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "There was no response of the device within the timeout period."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :array_0
    .array-data 1
        0x1bt
        0x23t
        0x2at
        0xat
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1bt
        0x23t
        0x2at
        0x2ct
    .end array-data

    :array_2
    .array-data 1
        0xat
        0x0t
    .end array-data
.end method

.method private f()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    :try_start_0
    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_3

    iget v0, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/16 v1, 0x200

    new-array v1, v1, [B

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :catch_0
    :goto_1
    :try_start_1
    iget-object v9, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->available()I

    move-result v9

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    array-length v10, v1

    sub-int/2addr v10, v5

    invoke-virtual {v9, v1, v5, v10}, Ljava/io/DataInputStream;->read([BII)I

    move-result v9

    add-int/2addr v5, v9

    add-int/2addr v6, v9

    :cond_1
    :goto_2
    const/4 v9, 0x7

    const/4 v10, 0x1

    if-lt v6, v9, :cond_3

    aget-byte v9, v1, v7

    const/16 v11, 0x1b

    if-ne v9, v11, :cond_2

    add-int/lit8 v9, v7, 0x1

    aget-byte v9, v1, v9

    const/16 v11, 0x23

    if-ne v9, v11, :cond_2

    add-int/lit8 v9, v7, 0x2

    aget-byte v9, v1, v9

    const/16 v11, 0x2c

    if-ne v9, v11, :cond_2

    add-int/lit8 v9, v7, 0x3

    aget-byte v9, v1, v9

    const/16 v11, 0x31

    if-ne v9, v11, :cond_2

    add-int/lit8 v9, v7, 0x5

    aget-byte v9, v1, v9

    const/16 v11, 0xa

    if-ne v9, v11, :cond_2

    add-int/lit8 v9, v7, 0x6

    aget-byte v9, v1, v9
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v9, :cond_2

    const/4 v8, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    :cond_3
    :goto_3
    if-ne v8, v10, :cond_5

    new-array v0, v10, [B

    add-int/lit8 v7, v7, 0x4

    aget-byte v1, v1, v7

    aput-byte v1, v0, v2

    iget v1, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    invoke-static {v0, v1}, Lcom/starmicronics/stario/f;->a([BI)I

    move-result v0

    if-eqz v0, :cond_4

    iput v0, p0, Lcom/starmicronics/stario/TCPPort;->a:I

    :cond_4
    return-void

    :cond_5
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_1

    sub-long/2addr v9, v3

    int-to-long v11, v0

    cmp-long v13, v9, v11

    if-gtz v13, :cond_6

    const-wide/16 v9, 0x64

    :try_start_3
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :cond_6
    :try_start_4
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "There was no response of the device within the timeout period."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :array_0
    .array-data 1
        0x1bt
        0x23t
        0x2ct
        0x31t
        0xat
        0x0t
    .end array-data
.end method

.method private g()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/TCPPort;->A:[B

    const/4 v0, 0x0

    iput v0, p0, Lcom/starmicronics/stario/TCPPort;->C:I

    iput-boolean v0, p0, Lcom/starmicronics/stario/TCPPort;->B:Z

    return-void
.end method

.method private declared-synchronized internalWritePort([BIII)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/starmicronics/stario/TCPPort;->b()V

    :cond_0
    new-instance v1, Lcom/starmicronics/stario/TCPPort$a;

    invoke-direct {v1, p0, p4}, Lcom/starmicronics/stario/TCPPort$a;-><init>(Lcom/starmicronics/stario/TCPPort;I)V

    const/4 p4, 0x1

    iput-boolean p4, p0, Lcom/starmicronics/stario/TCPPort;->E:Z

    invoke-virtual {v1}, Lcom/starmicronics/stario/TCPPort$a;->start()V

    move p4, p2

    const/4 p2, 0x0

    :goto_0
    if-ge p2, p3, :cond_2

    sub-int v1, p3, p2

    const/16 v2, 0x400

    if-ge v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x400

    :goto_1
    iget-object v2, p0, Lcom/starmicronics/stario/TCPPort;->D:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/starmicronics/stario/TCPPort;->F:J

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p1, p4, v1}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    add-int p4, p2, v1

    move p2, p4

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_2
    :try_start_5
    iput-boolean v0, p0, Lcom/starmicronics/stario/TCPPort;->E:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    goto :goto_2

    :catch_0
    :try_start_6
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to write"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :goto_2
    :try_start_7
    iput-boolean v0, p0, Lcom/starmicronics/stario/TCPPort;->E:Z

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method protected declared-synchronized a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/TCPPort;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    const/16 v0, 0xc8

    :try_start_3
    new-array v1, v0, [B

    :cond_0
    iget-object v2, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Ljava/io/DataInputStream;->read([BII)I

    move-result v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-gez v2, :cond_0

    :try_start_4
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    :try_start_5
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v0, :cond_1

    :try_start_6
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_2
    :cond_1
    :try_start_7
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v0, :cond_2

    :try_start_8
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catch_3
    :cond_2
    :try_start_9
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v0, :cond_8

    :try_start_a
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    :goto_0
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_b
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v1, :cond_3

    :try_start_c
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :catch_4
    :cond_3
    :try_start_d
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    if-eqz v1, :cond_4

    :try_start_e
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :catch_5
    :cond_4
    :try_start_f
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    if-eqz v1, :cond_5

    :try_start_10
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :catch_6
    :cond_5
    :try_start_11
    throw v0

    :catch_7
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    if-eqz v0, :cond_6

    :try_start_12
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_8
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :catch_8
    :cond_6
    :try_start_13
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    if-eqz v0, :cond_7

    :try_start_14
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_9
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    :catch_9
    :cond_7
    :try_start_15
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    if-eqz v0, :cond_8

    :try_start_16
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_a
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    goto :goto_0

    :catch_a
    :cond_8
    :goto_1
    monitor-exit p0

    return-void

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized b()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget v2, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    iget-object v3, p0, Lcom/starmicronics/stario/TCPPort;->g:Ljava/lang/String;

    iget v4, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    invoke-static {v3, v4}, Lcom/starmicronics/stario/f;->a(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x1

    if-eq v3, v5, :cond_1

    iget-boolean v7, p0, Lcom/starmicronics/stario/TCPPort;->w:Z

    if-eqz v7, :cond_0

    goto :goto_0

    :cond_0
    iput-boolean v6, p0, Lcom/starmicronics/stario/TCPPort;->x:Z

    move v2, v3

    goto :goto_1

    :cond_1
    :goto_0
    iput-boolean v4, p0, Lcom/starmicronics/stario/TCPPort;->x:Z

    :goto_1
    iget-object v3, p0, Lcom/starmicronics/stario/TCPPort;->f:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget v7, p0, Lcom/starmicronics/stario/TCPPort;->u:I

    const/16 v8, 0x238c

    if-nez v7, :cond_2

    invoke-direct {p0, v3, v2}, Lcom/starmicronics/stario/TCPPort;->a(Ljava/lang/String;I)Z

    move-result v7

    goto :goto_2

    :cond_2
    iget v7, p0, Lcom/starmicronics/stario/TCPPort;->u:I

    if-lt v7, v8, :cond_b

    iget v7, p0, Lcom/starmicronics/stario/TCPPort;->u:I

    const/16 v9, 0x2395

    if-gt v7, v9, :cond_b

    const/4 v7, 0x1

    :goto_2
    iget-boolean v9, p0, Lcom/starmicronics/stario/TCPPort;->w:Z

    if-eqz v9, :cond_3

    iget v8, p0, Lcom/starmicronics/stario/TCPPort;->u:I

    :cond_3
    if-eqz v7, :cond_a

    const/4 v7, 0x5

    const/4 v7, 0x0

    const/4 v9, 0x5

    :goto_3
    invoke-direct {p0, v2, v0, v1}, Lcom/starmicronics/stario/TCPPort;->a(IJ)I

    move-result v10

    if-gtz v10, :cond_4

    invoke-direct {p0, v7, v8}, Lcom/starmicronics/stario/TCPPort;->a(ZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :try_start_1
    new-instance v7, Ljava/net/InetSocketAddress;

    invoke-direct {v7, v3, v8}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    new-instance v11, Ljava/net/Socket;

    invoke-direct {v11}, Ljava/net/Socket;-><init>()V

    iput-object v11, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    iget-object v11, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    iget v12, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    invoke-virtual {v11, v12}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v11, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    iget-boolean v12, p0, Lcom/starmicronics/stario/TCPPort;->j:Z

    invoke-virtual {v11, v12}, Ljava/net/Socket;->setKeepAlive(Z)V

    iget-object v11, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    iget-boolean v12, p0, Lcom/starmicronics/stario/TCPPort;->k:Z

    invoke-virtual {v11, v12}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    iget-object v11, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v11, v6}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object v11, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v11, v7, v10}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    new-instance v7, Ljava/io/DataOutputStream;

    iget-object v10, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v7, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    new-instance v7, Ljava/io/DataInputStream;

    iget-object v10, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v7, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v0, "ESCPOS"

    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    new-array v0, v0, [B

    const/16 v1, 0x1d

    aput-byte v1, v0, v4

    const/16 v1, 0x61

    aput-byte v1, v0, v6

    const/4 v1, 0x2

    aput-byte v5, v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catch_0
    :try_start_4
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TCP Port "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " is busy."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_5
    :goto_4
    monitor-exit p0

    return-void

    :catch_1
    move-exception v7

    :try_start_5
    instance-of v10, v7, Ljava/net/ConnectException;

    iget-boolean v11, p0, Lcom/starmicronics/stario/TCPPort;->x:Z

    if-nez v11, :cond_7

    if-nez v9, :cond_7

    iget-boolean v11, p0, Lcom/starmicronics/stario/TCPPort;->w:Z

    if-nez v11, :cond_6

    invoke-direct {p0, v10, v8}, Lcom/starmicronics/stario/TCPPort;->a(ZI)V

    goto :goto_5

    :cond_6
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    :goto_5
    iget-boolean v7, p0, Lcom/starmicronics/stario/TCPPort;->x:Z

    if-eqz v7, :cond_9

    if-gtz v9, :cond_9

    invoke-direct {p0, v2, v0, v1}, Lcom/starmicronics/stario/TCPPort;->a(IJ)I

    move-result v7

    const/16 v11, 0x3e8

    invoke-static {v11, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    if-gtz v7, :cond_8

    invoke-direct {p0, v10, v8}, Lcom/starmicronics/stario/TCPPort;->a(ZI)V

    :cond_8
    int-to-long v11, v7

    invoke-direct {p0, v11, v12}, Lcom/starmicronics/stario/TCPPort;->a(J)V

    :cond_9
    add-int/lit8 v9, v9, -0x1

    move v7, v10

    goto/16 :goto_3

    :catch_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Cannot connect to printer"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Printer is power off."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Not supported port number"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized beginCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/TCPPort;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    const/4 v3, 0x1

    if-eq v2, v3, :cond_f

    const-string v2, "StarLine"

    iget-object v4, v1, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->etbAvailable:Z

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Checked block is not available for this printer"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/TCPPort;->d()V
    :try_end_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v2, "StarLine"

    iget-object v4, v1, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/16 v4, 0x8

    const/16 v5, 0xa

    const/4 v6, 0x5

    const/16 v7, 0x1d

    const/4 v8, 0x6

    const/4 v9, 0x2

    const/16 v10, 0x1b

    const/4 v11, 0x4

    const/4 v12, 0x3

    const/4 v13, 0x0

    if-eqz v2, :cond_2

    new-array v2, v5, [B

    aput-byte v10, v2, v13

    const/16 v14, 0x2a

    aput-byte v14, v2, v3

    const/16 v14, 0x72

    aput-byte v14, v2, v9

    const/16 v14, 0x42

    aput-byte v14, v2, v12

    aput-byte v10, v2, v11

    aput-byte v7, v2, v6

    aput-byte v12, v2, v8

    const/4 v14, 0x7

    aput-byte v11, v2, v14

    aput-byte v13, v2, v4

    const/16 v14, 0x9

    aput-byte v13, v2, v14

    goto :goto_1

    :cond_2
    new-array v2, v8, [B

    aput-byte v10, v2, v13

    aput-byte v7, v2, v3

    aput-byte v12, v2, v9

    aput-byte v11, v2, v12

    aput-byte v13, v2, v11

    aput-byte v13, v2, v6

    :goto_1
    array-length v14, v2

    invoke-virtual {v1, v2, v13, v14}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    const/4 v2, 0x0

    const-string v14, "StarLine"

    iget-object v15, v1, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    iget-boolean v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->etbAvailable:Z

    if-eqz v2, :cond_3

    new-array v2, v11, [B

    aput-byte v10, v2, v13

    const/16 v14, 0x1e

    aput-byte v14, v2, v3

    const/16 v14, 0x45

    aput-byte v14, v2, v9

    aput-byte v13, v2, v12

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Checked block is not available for this printer"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const-string v14, "ESCPOS"

    iget-object v15, v1, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    new-array v2, v8, [B

    aput-byte v10, v2, v13

    aput-byte v7, v2, v3

    aput-byte v12, v2, v9

    aput-byte v9, v2, v12

    aput-byte v13, v2, v11

    aput-byte v13, v2, v6

    :cond_5
    :goto_2
    array-length v14, v2

    invoke-virtual {v1, v2, v13, v14}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    iget v2, v1, Lcom/starmicronics/stario/TCPPort;->h:I

    const/16 v14, 0x2710

    if-le v2, v14, :cond_6

    iget v14, v1, Lcom/starmicronics/stario/TCPPort;->h:I

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    const-string v2, "StarLine"

    iget-object v4, v1, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    new-array v0, v3, [B

    const/16 v2, 0x17

    aput-byte v2, v0, v13

    array-length v2, v0

    invoke-virtual {v1, v0, v13, v2}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    :goto_3
    invoke-direct {v1, v13}, Lcom/starmicronics/stario/TCPPort;->a(Z)Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-eq v2, v3, :cond_9

    iget v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v2, v3, :cond_7

    goto :goto_6

    :cond_7
    const-wide/16 v4, 0xc8

    :try_start_3
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catch_0
    move-exception v0

    move-object v2, v0

    :try_start_4
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v15

    int-to-long v11, v14

    cmp-long v0, v4, v11

    if-gtz v0, :cond_8

    const/4 v11, 0x4

    const/4 v12, 0x3

    goto :goto_3

    :cond_8
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v2, "There was no response of the printer within the timeout period."

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Printer is offline"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    new-array v4, v5, [B

    new-array v5, v8, [B

    aput-byte v10, v5, v13

    aput-byte v7, v5, v3

    const/4 v11, 0x3

    aput-byte v11, v5, v9

    aput-byte v13, v5, v11

    const/4 v2, 0x4

    aput-byte v13, v5, v2

    aput-byte v13, v5, v6

    array-length v11, v5

    invoke-virtual {v1, v5, v13, v11}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    :goto_5
    array-length v11, v4

    invoke-virtual {v1, v4, v13, v11}, Lcom/starmicronics/stario/TCPPort;->readPort([BII)I

    move-result v11

    const/16 v12, 0x8

    if-lt v11, v12, :cond_c

    invoke-static {v4}, Lcom/starmicronics/stario/f;->a([B)I

    move-result v11

    if-nez v11, :cond_b

    :goto_6
    new-array v4, v8, [B

    aput-byte v10, v4, v13

    aput-byte v7, v4, v3

    const/4 v11, 0x3

    aput-byte v11, v4, v9

    aput-byte v11, v4, v11

    const/4 v2, 0x4

    aput-byte v13, v4, v2

    aput-byte v13, v4, v6

    array-length v2, v4

    invoke-virtual {v1, v4, v13, v2}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V
    :try_end_4
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_b
    const/4 v2, 0x4

    const/4 v11, 0x3

    :try_start_5
    array-length v0, v5

    invoke-virtual {v1, v5, v13, v0}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    goto :goto_7

    :cond_c
    const/4 v2, 0x4

    const/4 v11, 0x3

    :goto_7
    invoke-direct {v1, v13}, Lcom/starmicronics/stario/TCPPort;->a(Z)Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v2

    iget-boolean v0, v2, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z
    :try_end_5
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eq v0, v3, :cond_e

    const-wide/16 v17, 0x64

    :try_start_6
    invoke-static/range {v17 .. v18}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_8

    :catch_1
    move-exception v0

    move-object/from16 v17, v0

    :try_start_7
    invoke-virtual/range {v17 .. v17}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    sub-long v17, v17, v15

    move-object/from16 v19, v4

    int-to-long v3, v14

    cmp-long v0, v17, v3

    if-gtz v0, :cond_d

    move-object v0, v2

    move-object/from16 v4, v19

    const/4 v3, 0x1

    goto :goto_5

    :cond_d
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v2, "There was no response of the printer within the timeout period."

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Printer is offline"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception v0

    move-object v2, v0

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Printer is offline"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_9

    :catch_3
    move-exception v0

    :try_start_8
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :goto_9
    monitor-exit p0

    throw v0
.end method

.method public endCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "StarLine"

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x5

    const/16 v4, 0x1d

    const/16 v5, 0x1b

    const/4 v6, 0x6

    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x1

    const/4 v11, 0x0

    if-eqz v2, :cond_0

    new-array v1, v10, [B

    const/16 v2, 0x17

    aput-byte v2, v1, v11

    goto :goto_0

    :cond_0
    const-string v2, "ESCPOS"

    iget-object v12, p0, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-array v1, v6, [B

    aput-byte v5, v1, v11

    aput-byte v4, v1, v10

    aput-byte v9, v1, v8

    aput-byte v10, v1, v9

    aput-byte v11, v1, v7

    aput-byte v11, v1, v3

    :cond_1
    :goto_0
    array-length v2, v1

    invoke-virtual {p0, v1, v11, v2}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    new-array v1, v6, [B

    aput-byte v5, v1, v11

    aput-byte v4, v1, v10

    aput-byte v9, v1, v8

    aput-byte v7, v1, v9

    aput-byte v11, v1, v7

    aput-byte v11, v1, v3

    array-length v2, v1

    invoke-virtual {p0, v1, v11, v2}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/TCPPort;->a:I

    const/16 v2, 0x2710

    if-le v1, v2, :cond_2

    iget v2, p0, Lcom/starmicronics/stario/TCPPort;->a:I

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-direct {p0, v11}, Lcom/starmicronics/stario/TCPPort;->a(Z)Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v1

    iget-object v14, p0, Lcom/starmicronics/stario/TCPPort;->l:Ljava/lang/String;

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    const-string v14, "There was no response of the printer within the timeout period."

    if-eqz v0, :cond_6

    :goto_1
    :try_start_1
    invoke-direct {p0, v11}, Lcom/starmicronics/stario/TCPPort;->a(Z)Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-ne v1, v10, :cond_3

    return-object v0

    :cond_3
    iget v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    if-ne v1, v8, :cond_4

    return-object v0

    :cond_4
    const-wide/16 v0, 0xc8

    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v12

    int-to-long v3, v2

    cmp-long v5, v0, v3

    if-gtz v5, :cond_5

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v14}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/16 v0, 0xa

    new-array v0, v0, [B

    new-array v6, v6, [B

    aput-byte v5, v6, v11

    aput-byte v4, v6, v10

    aput-byte v9, v6, v8

    aput-byte v11, v6, v9

    aput-byte v11, v6, v7

    aput-byte v11, v6, v3

    array-length v3, v6

    invoke-virtual {p0, v6, v11, v3}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    :goto_3
    array-length v3, v0

    invoke-virtual {p0, v0, v11, v3}, Lcom/starmicronics/stario/TCPPort;->readPort([BII)I

    move-result v3

    const/16 v4, 0x8

    if-lt v3, v4, :cond_8

    invoke-static {v0}, Lcom/starmicronics/stario/f;->a([B)I

    move-result v3

    if-ne v3, v10, :cond_7

    return-object v1

    :cond_7
    array-length v1, v6

    invoke-virtual {p0, v6, v11, v1}, Lcom/starmicronics/stario/TCPPort;->writePort([BII)V

    :cond_8
    invoke-direct {p0, v11}, Lcom/starmicronics/stario/TCPPort;->a(Z)Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v1

    iget-boolean v3, v1, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2

    if-ne v3, v10, :cond_9

    return-object v1

    :cond_9
    const-wide/16 v3, 0x64

    :try_start_4
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    :catch_1
    move-exception v3

    :try_start_5
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v12

    int-to-long v7, v2

    cmp-long v5, v3, v7

    if-gtz v5, :cond_a

    goto :goto_3

    :cond_a
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v14}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getDipSwitchInformation()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/TCPPort;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/starmicronics/stario/TCPPort;->w:Z

    const-string v1, "This model is not supported this method."

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->b:Ljava/lang/String;

    const-string v2, "DK-AirCash"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->o:Ljava/lang/String;

    const-string v1, "1.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x4

    if-eqz v0, :cond_0

    new-array v0, v2, [B

    fill-array-data v0, :array_0

    new-array v2, v2, [B

    fill-array-data v2, :array_1

    goto :goto_0

    :cond_0
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    new-array v2, v2, [B

    fill-array-data v2, :array_3

    :goto_0
    new-array v1, v1, [B

    fill-array-data v1, :array_4

    invoke-direct {p0, v0, v1}, Lcom/starmicronics/stario/TCPPort;->a([B[B)[B

    move-result-object v0

    invoke-direct {p0, v2, v1}, Lcom/starmicronics/stario/TCPPort;->a([B[B)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/starmicronics/stario/f;->a([B[B)Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "printer is offline."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :array_0
    .array-data 1
        0x1bt
        0x3ft
        0x21t
        0x31t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x3ft
        0x21t
        0x32t
    .end array-data

    :array_2
    .array-data 1
        0x1bt
        0x23t
        0x2ct
        0x31t
    .end array-data

    :array_3
    .array-data 1
        0x1bt
        0x23t
        0x2ct
        0x32t
    .end array-data

    :array_4
    .array-data 1
        0xat
        0x0t
    .end array-data
.end method

.method public getFirmwareInformation()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/TCPPort;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->p:Z

    const-string v2, ""

    if-eqz v1, :cond_0

    const-string v1, "TSP100LAN"

    iput-object v1, p0, Lcom/starmicronics/stario/TCPPort;->m:Ljava/lang/String;

    :goto_0
    iput-object v2, p0, Lcom/starmicronics/stario/TCPPort;->n:Ljava/lang/String;

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->b:Ljava/lang/String;

    const-string v3, "DK-AirCash"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/TCPPort;->m:Ljava/lang/String;

    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->o:Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/TCPPort;->n:Ljava/lang/String;

    goto :goto_1

    :cond_1
    iget-boolean v1, p0, Lcom/starmicronics/stario/TCPPort;->w:Z

    if-eqz v1, :cond_2

    iput-object v2, p0, Lcom/starmicronics/stario/TCPPort;->m:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/starmicronics/stario/TCPPort;->e()V

    :goto_1
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->m:Ljava/lang/String;

    const-string v2, "ModelName"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort;->n:Ljava/lang/String;

    const-string v2, "FirmwareVersion"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :cond_3
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "printer is offline."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized getPortName()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPortSettings()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized readPort([BII)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/starmicronics/stario/TCPPort;->b()V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    if-lez p3, :cond_a

    iget v1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    if-ge v1, p3, :cond_1

    iget p3, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-ge v1, p3, :cond_2

    add-int v2, v1, p2

    iget-object v3, p0, Lcom/starmicronics/stario/TCPPort;->A:[B

    iget v4, p0, Lcom/starmicronics/stario/TCPPort;->y:I

    add-int/2addr v4, v1

    aget-byte v3, v3, v4

    aput-byte v3, p1, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    add-int/2addr v0, p3

    iget p1, p0, Lcom/starmicronics/stario/TCPPort;->y:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/starmicronics/stario/TCPPort;->y:I

    iget p1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    sub-int/2addr p1, p3

    iput p1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    iget p1, p0, Lcom/starmicronics/stario/TCPPort;->z:I

    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/starmicronics/stario/TCPPort;->g()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return v0

    :cond_4
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    :cond_5
    const/16 v3, 0xc8

    new-array v3, v3, [B

    iget-object v4, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v4}, Ljava/io/DataInputStream;->available()I

    move-result v4

    if-nez v4, :cond_8

    iget-boolean v3, p0, Lcom/starmicronics/stario/TCPPort;->B:Z

    if-eqz v3, :cond_7

    iget v3, p0, Lcom/starmicronics/stario/TCPPort;->h:I

    int-to-long v3, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    cmp-long v7, v3, v5

    if-ltz v7, :cond_6

    goto :goto_2

    :cond_6
    new-instance p1, Ljava/io/IOException;

    const-string p2, "There was no response of the device within the timeout period."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_7
    monitor-exit p0

    return v0

    :cond_8
    :try_start_2
    iget-object v4, p0, Lcom/starmicronics/stario/TCPPort;->e:Ljava/io/DataInputStream;

    const/4 v5, 0x7

    invoke-virtual {v4, v3, v0, v5}, Ljava/io/DataInputStream;->read([BII)I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/starmicronics/stario/TCPPort;->a([BI)Z

    move-result v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v6, 0x1

    if-ne v5, v6, :cond_9

    :try_start_3
    invoke-direct {p0, v3}, Lcom/starmicronics/stario/TCPPort;->a([B)I
    :try_end_3
    .catch Lcom/starmicronics/stario/NoReturnException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catch_0
    :try_start_4
    invoke-direct {p0}, Lcom/starmicronics/stario/TCPPort;->g()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return v0

    :cond_9
    :try_start_5
    invoke-direct {p0, v3, v4}, Lcom/starmicronics/stario/TCPPort;->b([BI)V

    :goto_2
    iget v3, p0, Lcom/starmicronics/stario/TCPPort;->C:I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-gtz v3, :cond_5

    goto :goto_0

    :cond_a
    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_1
    :try_start_6
    invoke-direct {p0}, Lcom/starmicronics/stario/TCPPort;->g()V

    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to read"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_3
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/starmicronics/stario/TCPPort;->a(Z)Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setEndCheckedBlockTimeoutMillis(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/stario/TCPPort;->a:I

    return-void
.end method

.method public declared-synchronized writePort([BII)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/starmicronics/stario/TCPPort;->b()V

    :cond_0
    const/4 v0, 0x0

    const/16 v1, 0x400

    if-ge v1, p3, :cond_2

    const/16 v2, 0x400

    :goto_0
    if-ge v0, p3, :cond_3

    iget-object v3, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v3, p1, p2, v2}, Ljava/io/DataOutputStream;->write([BII)V

    add-int p2, v0, v2

    sub-int v0, p3, p2

    if-ge v0, v1, :cond_1

    move v2, v0

    :cond_1
    move v0, p2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    :try_start_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to write"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    monitor-exit p0

    throw p1
.end method
