.class Lcom/starmicronics/stario/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/f$a;
    }
.end annotation


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/starmicronics/stario/f;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x1bt
        0x42t
        0x1bt
        0x2at
        0x72t
        0x42t
        0x0t
    .end array-data
.end method

.method static a(B)I
    .locals 2

    and-int/lit16 p0, p0, 0xbf

    int-to-byte p0, p0

    const/16 v0, 0xf

    if-eq p0, v0, :cond_8

    const/16 v1, 0x21

    if-eq p0, v1, :cond_7

    const/16 v1, 0x23

    if-eq p0, v1, :cond_6

    const/16 v1, 0x25

    if-eq p0, v1, :cond_5

    const/16 v1, 0x27

    if-eq p0, v1, :cond_4

    const/16 v1, 0x29

    if-eq p0, v1, :cond_3

    const/16 v1, 0x2b

    if-eq p0, v1, :cond_2

    const/16 v1, 0x2d

    if-eq p0, v1, :cond_1

    const/16 v1, 0x2f

    if-eq p0, v1, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    return v0

    :cond_1
    const/16 p0, 0xe

    return p0

    :cond_2
    const/16 p0, 0xd

    return p0

    :cond_3
    const/16 p0, 0xc

    return p0

    :cond_4
    const/16 p0, 0xb

    return p0

    :cond_5
    const/16 p0, 0xa

    return p0

    :cond_6
    const/16 p0, 0x9

    return p0

    :cond_7
    const/16 p0, 0x8

    return p0

    :cond_8
    const/4 p0, 0x7

    return p0
.end method

.method static a(Ljava/lang/String;I)I
    .locals 6

    const-string v0, ";"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    array-length v0, p0

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    :goto_0
    if-ge v2, v0, :cond_4

    aget-object v4, p0, v2

    const-string v5, "l"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    move v3, p1

    goto :goto_1

    :cond_2
    const-string v4, "^[0-9]+$"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    :catch_0
    const/4 v3, -0x1

    goto :goto_1

    :cond_3
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-ltz v3, :cond_1

    const v4, 0x493e0

    if-gt v3, v4, :cond_1

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return v3
.end method

.method static a([B)I
    .locals 11

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_9

    const/4 v3, 0x6

    const/4 v4, 0x5

    const/4 v5, 0x4

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x1

    if-nez v2, :cond_0

    aget-byte v9, p0, v1

    and-int/2addr v9, v8

    if-nez v9, :cond_0

    goto :goto_1

    :cond_0
    aget-byte v9, p0, v1

    const/16 v10, 0x1b

    if-ne v9, v10, :cond_1

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    aget-byte v9, p0, v1

    const/16 v10, 0x1d

    if-ne v9, v10, :cond_2

    if-ne v8, v2, :cond_2

    const/4 v2, 0x2

    goto :goto_1

    :cond_2
    aget-byte v9, p0, v1

    if-ne v9, v7, :cond_3

    if-ne v6, v2, :cond_3

    const/4 v2, 0x3

    goto :goto_1

    :cond_3
    aget-byte v6, p0, v1

    if-eqz v6, :cond_4

    aget-byte v6, p0, v1

    if-ne v6, v8, :cond_5

    :cond_4
    if-ne v7, v2, :cond_5

    const/4 v2, 0x4

    goto :goto_1

    :cond_5
    aget-byte v6, p0, v1

    if-nez v6, :cond_6

    if-ne v5, v2, :cond_6

    const/4 v2, 0x5

    goto :goto_1

    :cond_6
    aget-byte v5, p0, v1

    if-nez v5, :cond_7

    if-ne v4, v2, :cond_7

    const/4 v2, 0x6

    goto :goto_1

    :cond_7
    aget-byte v4, p0, v1

    if-ne v4, v8, :cond_8

    if-ne v3, v2, :cond_8

    return v8

    :cond_8
    if-eqz v2, :cond_9

    const/4 v2, 0x0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_9
    return v0
.end method

.method static a([BI)I
    .locals 2

    const/4 v0, 0x0

    aget-byte p0, p0, v0

    and-int/lit16 p0, p0, 0xff

    shr-int/lit8 p0, p0, 0x6

    if-eqz p0, :cond_2

    const/4 v1, 0x1

    if-eq p0, v1, :cond_1

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    add-int/2addr p1, v0

    goto :goto_0

    :cond_0
    add-int/lit16 p1, p1, 0x1388

    goto :goto_0

    :cond_1
    add-int/lit16 p1, p1, 0x2710

    goto :goto_0

    :cond_2
    add-int/lit16 p1, p1, 0x4e20

    :goto_0
    return p1
.end method

.method static a(Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Ver"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const-string v3, "FirmwareVersion"

    const-string v4, "ModelName"

    const/4 v5, -0x1

    if-eq v2, v5, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-interface {v0, v4, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p0, ""

    :goto_0
    invoke-interface {v0, v3, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method static a([B[B)Ljava/util/Map;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B[B)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    const/4 v1, 0x0

    aget-byte v2, p0, v1

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    aget-byte v4, p0, v1

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    aget-byte v5, p0, v1

    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    :goto_2
    aget-byte v6, p0, v1

    and-int/lit8 v6, v6, 0x8

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    :goto_3
    aget-byte v7, p0, v1

    and-int/lit8 v7, v7, 0x10

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    goto :goto_4

    :cond_4
    const/4 v7, 0x0

    :goto_4
    aget-byte v8, p0, v1

    and-int/lit8 v8, v8, 0x20

    if-eqz v8, :cond_5

    const/4 v8, 0x1

    goto :goto_5

    :cond_5
    const/4 v8, 0x0

    :goto_5
    aget-byte v9, p0, v1

    and-int/lit8 v9, v9, 0x40

    if-eqz v9, :cond_6

    const/4 v9, 0x1

    goto :goto_6

    :cond_6
    const/4 v9, 0x0

    :goto_6
    aget-byte v10, p0, v1

    and-int/lit16 v10, v10, 0x80

    if-eqz v10, :cond_7

    const/4 v10, 0x1

    goto :goto_7

    :cond_7
    const/4 v10, 0x0

    :goto_7
    aget-byte v11, p1, v1

    and-int/2addr v11, v3

    if-eqz v11, :cond_8

    const/4 v11, 0x1

    goto :goto_8

    :cond_8
    const/4 v11, 0x0

    :goto_8
    aget-byte v12, p1, v1

    and-int/lit8 v12, v12, 0x2

    if-eqz v12, :cond_9

    const/4 v12, 0x1

    goto :goto_9

    :cond_9
    const/4 v12, 0x0

    :goto_9
    aget-byte v13, p1, v1

    and-int/lit8 v13, v13, 0x4

    if-eqz v13, :cond_a

    const/4 v13, 0x1

    goto :goto_a

    :cond_a
    const/4 v13, 0x0

    :goto_a
    aget-byte v14, p1, v1

    and-int/lit8 v14, v14, 0x8

    if-eqz v14, :cond_b

    const/4 v14, 0x1

    goto :goto_b

    :cond_b
    const/4 v14, 0x0

    :goto_b
    aget-byte v15, p1, v1

    and-int/lit8 v15, v15, 0x10

    if-eqz v15, :cond_c

    const/4 v15, 0x1

    goto :goto_c

    :cond_c
    const/4 v15, 0x0

    :goto_c
    aget-byte v16, p1, v1

    and-int/lit8 v16, v16, 0x20

    if-eqz v16, :cond_d

    const/16 v16, 0x1

    goto :goto_d

    :cond_d
    const/16 v16, 0x0

    :goto_d
    aget-byte v17, p1, v1

    and-int/lit8 v17, v17, 0x40

    if-eqz v17, :cond_e

    const/16 v17, 0x1

    goto :goto_e

    :cond_e
    const/16 v17, 0x0

    :goto_e
    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_f

    const/4 v1, 0x1

    :cond_f
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW11"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW12"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW13"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW14"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW15"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW16"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW17"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW18"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW21"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW22"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW23"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW24"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW25"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW26"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "DIPSW27"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "DIPSW28"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method static a(Lcom/starmicronics/stario/StarPrinterStatus;)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->blackMarkError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->cutterError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->cutterError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->etbAvailable:Z

    iput v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->headThermistorError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->headUpError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->mechError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->overTemp:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->pageModeCmdError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->peelerPaperPresent:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperJamError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperPresent:Z

    iput v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterState:I

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptBlackMarkDetection:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyInner:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyOuter:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiveBufferOverflow:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipBOF:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipCOF:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipTOF:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipPaperPresent:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->stackerFull:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->unrecoverableError:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->validationPaperPresent:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->voltageError:Z

    iput v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->connectedInterface:I

    iget-object v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v1, v1, v0

    const/16 v2, 0x8

    and-int/2addr v1, v2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_0

    iput-boolean v3, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    :cond_0
    iget-object v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v1, v1, v0

    const/4 v2, 0x4

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_1

    iput-boolean v3, p0, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    :cond_1
    iget-object v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v1, v0

    const/16 v1, 0x20

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_2

    iput-boolean v3, p0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    :cond_2
    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    const/4 v1, 0x2

    aget-byte v0, v0, v1

    const/16 v1, 0xc

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_3

    iput-boolean v3, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    :cond_3
    return-void
.end method

.method static a(Lcom/starmicronics/stario/StarPrinterStatus;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x1

    iput v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->blackMarkError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->cutterError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->cutterError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->etbAvailable:Z

    iput v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->headThermistorError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->headUpError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->mechError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->overTemp:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->pageModeCmdError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->peelerPaperPresent:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperJamError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperPresent:Z

    iput v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterState:I

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptBlackMarkDetection:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyInner:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyOuter:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiveBufferOverflow:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipBOF:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipCOF:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipTOF:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipPaperPresent:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->stackerFull:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->unrecoverableError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->validationPaperPresent:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->voltageError:Z

    iput v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->connectedInterface:I

    const-string v2, "PaperEmpty"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v2, v2, v1

    const/16 v3, 0x60

    and-int/2addr v2, v3

    if-ne v2, v3, :cond_0

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    goto :goto_0

    :cond_0
    const-string v2, "CoverOpen"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x4

    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v2, v2, v1

    and-int/2addr v2, v3

    if-ne v2, v3, :cond_1

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    goto :goto_0

    :cond_1
    const-string v2, "Online/CashDrawer"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-ne v0, p1, :cond_3

    iget-object p1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte p1, p1, v1

    const/16 v2, 0x8

    and-int/2addr p1, v2

    if-ne p1, v2, :cond_2

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    :cond_2
    iget-object p1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte p1, p1, v1

    and-int/2addr p1, v3

    if-ne p1, v3, :cond_3

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    :cond_3
    :goto_0
    return-void
.end method

.method static a()[B
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1dt
    .end array-data
.end method

.method static a([B[B[B)[B
    .locals 7

    array-length v0, p1

    new-array v0, v0, [B

    array-length v1, p2

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    array-length v4, p0

    array-length v5, p1

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    const/4 v5, -0x1

    if-ge v3, v4, :cond_1

    array-length v4, p1

    invoke-static {p0, v3, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, -0x1

    :goto_1
    array-length v0, p1

    add-int/2addr v0, v3

    :goto_2
    array-length v4, p0

    array-length v6, p2

    sub-int/2addr v4, v6

    add-int/lit8 v4, v4, 0x1

    if-ge v0, v4, :cond_3

    array-length v4, p2

    invoke-static {p0, v0, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v1, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, -0x1

    :goto_3
    if-ltz v3, :cond_5

    if-gez v0, :cond_4

    goto :goto_4

    :cond_4
    sub-int/2addr v0, v3

    array-length p2, p1

    sub-int p2, v0, p2

    new-array p2, p2, [B

    array-length v1, p1

    add-int/2addr v3, v1

    array-length p1, p1

    sub-int/2addr v0, p1

    invoke-static {p0, v3, p2, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    :cond_5
    :goto_4
    const/4 p0, 0x0

    return-object p0
.end method

.method static b()Ljava/lang/String;
    .locals 1

    const-string v0, "SM-"

    return-object v0
.end method

.method static b([B)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lcom/starmicronics/stario/f$a;->a:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v1}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v1}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v1, p0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object p0, Lcom/starmicronics/stario/f$a;->a:Lcom/starmicronics/stario/f$a;

    invoke-virtual {p0}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p0, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {p0}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v2

    :goto_0
    const-string p0, "Ver"

    invoke-virtual {v1, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    invoke-virtual {v1, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p0

    sget-object v2, Lcom/starmicronics/stario/f$a;->a:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v2}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3, p0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v2}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 p0, p0, 0x3

    :goto_1
    invoke-virtual {v1, p0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_0
    const-string p0, "S7"

    invoke-virtual {v1, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    sget-object p0, Lcom/starmicronics/stario/f$a;->a:Lcom/starmicronics/stario/f$a;

    invoke-virtual {p0}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v2, "SP700"

    invoke-interface {v0, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p0, 0x2

    const-string v2, "J"

    invoke-virtual {v1, v2, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "T"

    invoke-virtual {v1, v2, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "K"

    invoke-virtual {v1, v2, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_2

    :cond_1
    sget-object v2, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v2}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    :goto_2
    sget-object p0, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {p0}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_3
    sget-object p0, Lcom/starmicronics/stario/f$a;->a:Lcom/starmicronics/stario/f$a;

    invoke-virtual {p0}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p0, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {p0}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    return-object v0
.end method

.method static b(Lcom/starmicronics/stario/StarPrinterStatus;)V
    .locals 4

    const/4 v0, 0x1

    iput v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->blackMarkError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->cutterError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->cutterError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->etbAvailable:Z

    iput v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->headThermistorError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->headUpError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->mechError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->overTemp:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->pageModeCmdError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->peelerPaperPresent:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperJamError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperPresent:Z

    iput v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterState:I

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptBlackMarkDetection:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyInner:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyOuter:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiveBufferOverflow:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipBOF:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipCOF:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipTOF:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipPaperPresent:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->stackerFull:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->unrecoverableError:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->validationPaperPresent:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->voltageError:Z

    iput v1, p0, Lcom/starmicronics/stario/StarPrinterStatus;->connectedInterface:I

    iget-object v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v2, v2, v1

    and-int/2addr v2, v0

    if-ne v2, v0, :cond_0

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    :cond_0
    iget-object v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v2, v2, v1

    const/4 v3, 0x4

    and-int/2addr v2, v3

    if-ne v2, v3, :cond_1

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptBlackMarkDetection:Z

    :cond_1
    iget-object v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v1, v2, v1

    const/4 v2, 0x2

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_2

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    :cond_2
    return-void
.end method

.method static b([BI)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x7

    if-ge p1, v1, :cond_0

    return v0

    :cond_0
    aget-byte p1, p0, v0

    const/16 v1, 0x1b

    const/4 v2, 0x1

    if-ne p1, v1, :cond_1

    aget-byte p1, p0, v2

    const/16 v1, 0x1d

    if-ne p1, v1, :cond_1

    const/4 p1, 0x2

    aget-byte p1, p0, p1

    const/16 v1, 0x29

    if-ne p1, v1, :cond_1

    const/4 p1, 0x3

    aget-byte v1, p0, p1

    const/16 v3, 0x4c

    if-ne v1, v3, :cond_1

    const/4 v1, 0x4

    aget-byte v1, p0, v1

    if-ne v1, p1, :cond_1

    const/4 p1, 0x5

    aget-byte p1, p0, p1

    if-nez p1, :cond_1

    const/4 p1, 0x6

    aget-byte p0, p0, p1

    const/16 p1, 0x32

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method static c(Lcom/starmicronics/stario/StarPrinterStatus;)V
    .locals 9

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    const/4 v1, 0x2

    aget-byte v0, v0, v1

    and-int/lit8 v0, v0, 0x20

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v1

    const/16 v4, 0x8

    and-int/2addr v0, v4

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v1

    const/4 v5, 0x4

    and-int/2addr v0, v5

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    const/4 v6, 0x3

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->overTemp:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_4

    :cond_4
    const/4 v0, 0x0

    :goto_4
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->unrecoverableError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v4

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    :goto_5
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->cutterError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v5

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_6

    :cond_6
    const/4 v0, 0x0

    :goto_6
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->mechError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v5

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    goto :goto_7

    :cond_7
    const/4 v0, 0x0

    :goto_7
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->headThermistorError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v5

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_8

    :cond_8
    const/4 v0, 0x0

    :goto_8
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiveBufferOverflow:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v5

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    goto :goto_9

    :cond_9
    const/4 v0, 0x0

    :goto_9
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->pageModeCmdError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v5

    and-int/2addr v0, v4

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_a

    :cond_a
    const/4 v0, 0x0

    :goto_a
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->blackMarkError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v5

    and-int/2addr v0, v5

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    goto :goto_b

    :cond_b
    const/4 v0, 0x0

    :goto_b
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperJamError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v5

    and-int/2addr v0, v1

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    goto :goto_c

    :cond_c
    const/4 v0, 0x0

    :goto_c
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->headUpError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v5

    and-int/2addr v0, v1

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    goto :goto_d

    :cond_d
    const/4 v0, 0x0

    :goto_d
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->voltageError:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    const/4 v6, 0x5

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_e

    :cond_e
    const/4 v0, 0x0

    :goto_e
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptBlackMarkDetection:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v4

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    goto :goto_f

    :cond_f
    const/4 v0, 0x0

    :goto_f
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v5

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    goto :goto_10

    :cond_10
    const/4 v0, 0x0

    :goto_10
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyInner:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v1

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    goto :goto_11

    :cond_11
    const/4 v0, 0x0

    :goto_11
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyOuter:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    const/4 v6, 0x6

    aget-byte v0, v0, v6

    and-int/2addr v0, v1

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    goto :goto_12

    :cond_12
    const/4 v0, 0x0

    :goto_12
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperPresent:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v1

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    goto :goto_13

    :cond_13
    const/4 v0, 0x0

    :goto_13
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->peelerPaperPresent:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v1

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    goto :goto_14

    :cond_14
    const/4 v0, 0x0

    :goto_14
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->stackerFull:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v1

    if-nez v0, :cond_15

    const/4 v0, 0x1

    goto :goto_15

    :cond_15
    const/4 v0, 0x0

    :goto_15
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipTOF:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v5

    if-nez v0, :cond_16

    const/4 v0, 0x1

    goto :goto_16

    :cond_16
    const/4 v0, 0x0

    :goto_16
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipCOF:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/2addr v0, v4

    if-nez v0, :cond_17

    const/4 v0, 0x1

    goto :goto_17

    :cond_17
    const/4 v0, 0x0

    :goto_17
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipBOF:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_18

    :goto_18
    iput-boolean v3, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipPaperPresent:Z

    :goto_19
    iput-boolean v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->validationPaperPresent:Z

    goto :goto_1a

    :cond_18
    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_19

    goto :goto_18

    :cond_19
    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_1a

    iput-boolean v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipPaperPresent:Z

    iput-boolean v3, p0, Lcom/starmicronics/stario/StarPrinterStatus;->validationPaperPresent:Z

    goto :goto_1a

    :cond_1a
    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v6

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_1b

    iput-boolean v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->slipPaperPresent:Z

    goto :goto_19

    :cond_1b
    :goto_1a
    iget v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    const/16 v7, 0x9

    if-lt v0, v7, :cond_1c

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1c
    const/4 v0, 0x0

    :goto_1b
    iput-boolean v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->etbAvailable:Z

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    const/4 v7, 0x7

    aget-byte v0, v0, v7

    and-int/lit8 v0, v0, 0x40

    shr-int/2addr v0, v1

    iget-object v8, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v8, v8, v7

    and-int/lit8 v8, v8, 0x20

    shr-int/2addr v8, v1

    or-int/2addr v0, v8

    iget-object v8, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v8, v8, v7

    and-int/2addr v8, v4

    shr-int/2addr v8, v3

    or-int/2addr v0, v8

    iget-object v8, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v8, v8, v7

    and-int/2addr v8, v5

    shr-int/2addr v8, v3

    or-int/2addr v0, v8

    iget-object v8, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v7, v8, v7

    and-int/2addr v7, v1

    shr-int/2addr v7, v3

    or-int/2addr v0, v7

    iput v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v0, v0, v4

    and-int/2addr v0, v4

    shr-int/2addr v0, v3

    iget-object v7, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v7, v7, v4

    and-int/2addr v5, v7

    shr-int/2addr v5, v3

    or-int/2addr v0, v5

    iget-object v5, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v4, v5, v4

    and-int/2addr v1, v4

    shr-int/2addr v1, v3

    or-int/2addr v0, v1

    iput v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterState:I

    iget v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_1d

    iget-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    and-int/2addr v0, v6

    shr-int/2addr v0, v3

    iput v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->connectedInterface:I

    goto :goto_1c

    :cond_1d
    iput v2, p0, Lcom/starmicronics/stario/StarPrinterStatus;->connectedInterface:I

    :goto_1c
    return-void
.end method

.method static c([BI)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x7

    if-ge p1, v1, :cond_0

    return v0

    :cond_0
    aget-byte p1, p0, v0

    const/16 v1, 0x1b

    const/4 v2, 0x1

    if-ne p1, v1, :cond_1

    aget-byte p1, p0, v2

    const/16 v1, 0x1d

    if-ne p1, v1, :cond_1

    const/4 p1, 0x2

    aget-byte p1, p0, p1

    const/16 v1, 0x29

    if-ne p1, v1, :cond_1

    const/4 p1, 0x3

    aget-byte v1, p0, p1

    const/16 v3, 0x4c

    if-ne v1, v3, :cond_1

    const/4 v1, 0x4

    aget-byte v1, p0, v1

    if-ne v1, p1, :cond_1

    const/4 p1, 0x5

    aget-byte p1, p0, p1

    if-nez p1, :cond_1

    const/4 p1, 0x6

    aget-byte p0, p0, p1

    const/16 p1, 0x33

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method static c()[B
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 1
        0x10t
        0x4t
        0x4t
    .end array-data
.end method

.method static d([BI)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x4

    if-ge p1, v1, :cond_0

    return v0

    :cond_0
    aget-byte p1, p0, v0

    const/16 v1, 0x1b

    const/4 v2, 0x1

    if-ne p1, v1, :cond_1

    aget-byte p1, p0, v2

    const/16 v1, 0x1d

    if-ne p1, v1, :cond_1

    const/4 p1, 0x2

    aget-byte p1, p0, p1

    const/16 v1, 0x42

    if-ne p1, v1, :cond_1

    const/4 p1, 0x3

    aget-byte p0, p0, p1

    const/16 p1, 0x32

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method static e([BI)I
    .locals 2

    const/16 v0, 0x9

    if-ge p1, v0, :cond_0

    const/4 p0, -0x1

    return p0

    :cond_0
    const/4 p1, 0x7

    aget-byte p1, p0, p1

    and-int/lit16 p1, p1, 0xff

    const/16 v1, 0x8

    aget-byte p0, p0, v1

    and-int/lit16 p0, p0, 0xff

    shl-int/2addr p0, v1

    add-int/2addr p0, p1

    add-int/2addr p0, v0

    return p0
.end method

.method static f([BI)I
    .locals 4

    const/4 v0, -0x1

    const/16 v1, 0xb

    if-ge p1, v1, :cond_0

    return v0

    :cond_0
    if-ne p1, v1, :cond_1

    return v1

    :cond_1
    const/16 v2, 0xe

    if-ge p1, v2, :cond_2

    return v0

    :cond_2
    const/16 p1, 0x9

    aget-byte p1, p0, p1

    const/16 v0, 0xa

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x7

    div-int/lit8 v1, v1, 0x8

    const/16 v0, 0xc

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    const/16 v3, 0xd

    aget-byte p0, p0, v3

    and-int/lit16 p0, p0, 0xff

    shl-int/lit8 p0, p0, 0x8

    add-int/2addr p0, v0

    mul-int v1, v1, p0

    add-int/lit8 v1, v1, 0x1

    mul-int p1, p1, v1

    add-int/2addr p1, v2

    add-int/lit8 p1, p1, 0x2

    return p1
.end method

.method static g([BI)I
    .locals 2

    const/4 v0, 0x6

    if-ge p1, v0, :cond_0

    const/4 p0, -0x1

    return p0

    :cond_0
    const/4 p1, 0x4

    aget-byte p1, p0, p1

    and-int/lit16 p1, p1, 0xff

    const/4 v1, 0x5

    aget-byte p0, p0, v1

    and-int/lit16 p0, p0, 0xff

    shl-int/lit8 p0, p0, 0x8

    add-int/2addr p0, p1

    add-int/2addr p0, v0

    return p0
.end method
