.class final enum Lcom/starmicronics/stario/StarIOPort$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/stario/StarIOPort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/stario/StarIOPort$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/starmicronics/stario/StarIOPort$a;

.field public static final enum b:Lcom/starmicronics/stario/StarIOPort$a;

.field public static final enum c:Lcom/starmicronics/stario/StarIOPort$a;

.field public static final enum d:Lcom/starmicronics/stario/StarIOPort$a;

.field private static final synthetic e:[Lcom/starmicronics/stario/StarIOPort$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/starmicronics/stario/StarIOPort$a;

    const/4 v1, 0x0

    const-string v2, "DESKTOP"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/stario/StarIOPort$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarIOPort$a;->a:Lcom/starmicronics/stario/StarIOPort$a;

    new-instance v0, Lcom/starmicronics/stario/StarIOPort$a;

    const/4 v2, 0x1

    const-string v3, "DKAIRCASH"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarIOPort$a;->b:Lcom/starmicronics/stario/StarIOPort$a;

    new-instance v0, Lcom/starmicronics/stario/StarIOPort$a;

    const/4 v3, 0x2

    const-string v4, "PORTABLE"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/stario/StarIOPort$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarIOPort$a;->c:Lcom/starmicronics/stario/StarIOPort$a;

    new-instance v0, Lcom/starmicronics/stario/StarIOPort$a;

    const/4 v4, 0x3

    const-string v5, "MPOP"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/stario/StarIOPort$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarIOPort$a;->d:Lcom/starmicronics/stario/StarIOPort$a;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/starmicronics/stario/StarIOPort$a;

    sget-object v5, Lcom/starmicronics/stario/StarIOPort$a;->a:Lcom/starmicronics/stario/StarIOPort$a;

    aput-object v5, v0, v1

    sget-object v1, Lcom/starmicronics/stario/StarIOPort$a;->b:Lcom/starmicronics/stario/StarIOPort$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/stario/StarIOPort$a;->c:Lcom/starmicronics/stario/StarIOPort$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/stario/StarIOPort$a;->d:Lcom/starmicronics/stario/StarIOPort$a;

    aput-object v1, v0, v4

    sput-object v0, Lcom/starmicronics/stario/StarIOPort$a;->e:[Lcom/starmicronics/stario/StarIOPort$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/stario/StarIOPort$a;
    .locals 1

    const-class v0, Lcom/starmicronics/stario/StarIOPort$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/stario/StarIOPort$a;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/stario/StarIOPort$a;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/StarIOPort$a;->e:[Lcom/starmicronics/stario/StarIOPort$a;

    invoke-virtual {v0}, [Lcom/starmicronics/stario/StarIOPort$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/stario/StarIOPort$a;

    return-object v0
.end method
