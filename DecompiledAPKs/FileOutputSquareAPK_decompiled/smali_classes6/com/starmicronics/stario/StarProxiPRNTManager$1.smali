.class Lcom/starmicronics/stario/StarProxiPRNTManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/stario/StarProxiPRNTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final a:[B

.field final synthetic b:Lcom/starmicronics/stario/StarProxiPRNTManager;


# direct methods
.method constructor <init>(Lcom/starmicronics/stario/StarProxiPRNTManager;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 p1, 0x10

    new-array p1, p1, [B

    fill-array-data p1, :array_0

    iput-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        -0x21t
        -0x36t
        0x71t
        -0x67t
        0x70t
        -0x62t
        -0x20t
        -0x68t
        0xct
        0x44t
        0x43t
        0x3ft
        0x6t
        -0x27t
        0x37t
        0x4ft
    .end array-data
.end method


# virtual methods
.method public onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 17

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v0}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c(Lcom/starmicronics/stario/StarProxiPRNTManager;)Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/starmicronics/stario/StarProxiPRNTManagerCallback$StarDeviceType;->StarDeviceTypeDesktopPrinter:Lcom/starmicronics/stario/StarProxiPRNTManagerCallback$StarDeviceType;

    const/4 v3, 0x3

    aget-byte v4, p3, v3

    const/16 v5, 0x11

    if-ne v4, v5, :cond_1d

    const/4 v4, 0x4

    aget-byte v4, p3, v4

    const/4 v5, 0x7

    if-ne v4, v5, :cond_1d

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    const/16 v6, 0x10

    if-ge v5, v6, :cond_2

    add-int/lit8 v6, v5, 0x5

    aget-byte v6, p3, v6

    iget-object v7, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->a:[B

    aget-byte v7, v7, v5

    if-eq v6, v7, :cond_1

    return-void

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    iget-object v5, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v5, v0}, Lcom/starmicronics/stario/StarProxiPRNTManager;->a(Lcom/starmicronics/stario/StarProxiPRNTManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->b()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    :try_start_0
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iget-object v9, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v9}, Lcom/starmicronics/stario/StarProxiPRNTManager;->d(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    const/4 v10, 0x1

    if-eqz v9, :cond_1c

    iget-object v9, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v9}, Lcom/starmicronics/stario/StarProxiPRNTManager;->d(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/HashMap;

    if-eqz v9, :cond_1b

    const-string v11, "Stock"

    invoke-virtual {v9, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [I

    check-cast v11, [I

    aget v12, v11, v4

    add-int/2addr v12, v10

    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v13

    const/4 v14, 0x2

    if-ge v12, v13, :cond_5

    aput p2, v11, v12

    aget v2, v11, v10

    :goto_1
    if-gt v14, v12, :cond_4

    aget v3, v11, v14

    if-ge v2, v3, :cond_3

    aget v2, v11, v14

    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_4
    aput v12, v11, v4

    const-string v3, "RSSI"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v3, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "Date"

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7, v3, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "Stock"

    invoke-virtual {v7, v3, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "Last Max RSSI"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "found Port Name"

    invoke-virtual {v7, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->d(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6

    return-void

    :cond_5
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v13

    if-ne v12, v13, :cond_6

    invoke-static {v10}, Lcom/starmicronics/stario/StarProxiPRNTManager;->a(Z)Z

    aput p2, v11, v12

    goto :goto_2

    :cond_6
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v13

    if-ge v13, v12, :cond_7

    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v13

    sub-int v13, v12, v13

    aput p2, v11, v13

    :cond_7
    :goto_2
    aget v13, v11, v10

    move v15, v13

    const/4 v13, 0x2

    :goto_3
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v3

    if-gt v13, v3, :cond_9

    aget v3, v11, v13

    if-ge v15, v3, :cond_8

    aget v15, v11, v13

    :cond_8
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_9
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v3

    if-eq v12, v3, :cond_e

    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->d()Z

    move-result v3

    if-ne v3, v10, :cond_a

    goto :goto_6

    :cond_a
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v3

    if-ge v3, v12, :cond_d

    const-string v3, "RSSI"

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v13, v15, v3

    if-gt v13, v10, :cond_b

    const/4 v4, -0x1

    if-lt v13, v4, :cond_b

    move v3, v15

    goto :goto_4

    :cond_b
    const-string v4, "Count"

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/2addr v4, v10

    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v9

    if-ge v4, v9, :cond_c

    goto :goto_5

    :cond_c
    move v3, v15

    goto :goto_5

    :cond_d
    move/from16 v3, p2

    :goto_4
    const/4 v4, 0x0

    :goto_5
    const/16 v16, 0x0

    goto :goto_8

    :cond_e
    :goto_6
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v3

    add-int/2addr v3, v14

    if-ne v12, v3, :cond_f

    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Lcom/starmicronics/stario/StarProxiPRNTManager;->a(Z)Z

    goto :goto_7

    :cond_f
    const/16 v16, 0x0

    :goto_7
    move v3, v15

    const/4 v4, 0x0

    :goto_8
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v9

    mul-int/lit8 v9, v9, 0x2

    if-ge v12, v9, :cond_10

    aput v12, v11, v16

    goto :goto_9

    :cond_10
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v9

    aput v9, v11, v16

    :goto_9
    const-string v9, "RSSI"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v9, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "Date"

    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7, v9, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "Stock"

    invoke-virtual {v7, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "Last Max RSSI"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "Count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v9, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "found Port Name"

    invoke-virtual {v7, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->d(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_11
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_12

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    const/4 v4, 0x1

    goto :goto_a

    :cond_12
    const/4 v4, 0x0

    :goto_a
    if-nez v4, :cond_13

    const-string v4, "Count"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "found Port Name"

    invoke-virtual {v8, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_13
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v7, "RSSI"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "Date"

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "found Port Name"

    invoke-virtual {v4, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v7}, Lcom/starmicronics/stario/StarProxiPRNTManager;->b(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v5, :cond_19

    const-string v4, ""

    const-string v7, ""

    iget-object v9, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v9}, Lcom/starmicronics/stario/StarProxiPRNTManager;->f(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/HashMap;

    const-string v11, "Device Type"

    invoke-virtual {v9, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    iget-object v11, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v11}, Lcom/starmicronics/stario/StarProxiPRNTManager;->f(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashMap;

    const-string v12, "MAC Address"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    iget-object v11, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v11}, Lcom/starmicronics/stario/StarProxiPRNTManager;->f(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashMap;

    const-string v12, "Threshold RSSI"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ge v11, v3, :cond_18

    iget-object v11, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v11}, Lcom/starmicronics/stario/StarProxiPRNTManager;->e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashMap;

    const-string v12, "Count"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    add-int/2addr v11, v10

    const/4 v12, 0x3

    if-ge v11, v12, :cond_14

    const-string v2, "Count"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "found Port Name"

    invoke-virtual {v8, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6

    return-void

    :cond_14
    const-string v11, "Count"

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v11, "found Port Name"

    invoke-virtual {v8, v11, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v11, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v11}, Lcom/starmicronics/stario/StarProxiPRNTManager;->e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v11

    invoke-virtual {v11, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "Printer"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_15

    move-object v4, v5

    goto :goto_b

    :cond_15
    const-string v8, "DK-AirCash"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    move-object v7, v5

    :cond_16
    :goto_b
    const-string v8, "Printer"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_17

    iget-object v7, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v7, v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->b(Lcom/starmicronics/stario/StarProxiPRNTManager;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c(Lcom/starmicronics/stario/StarProxiPRNTManager;)Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    move-result-object v4

    iget-object v7, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v7}, Lcom/starmicronics/stario/StarProxiPRNTManager;->g(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;->onClosestPrinterFound(Ljava/lang/String;)V

    goto :goto_c

    :cond_17
    const-string v4, "DK-AirCash"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4, v7}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c(Lcom/starmicronics/stario/StarProxiPRNTManager;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c(Lcom/starmicronics/stario/StarProxiPRNTManager;)Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    move-result-object v4

    iget-object v7, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v7}, Lcom/starmicronics/stario/StarProxiPRNTManager;->h(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;->onClosestCashDrawerFound(Ljava/lang/String;)V

    goto :goto_c

    :cond_18
    const-string v4, "Count"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v8, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "found Port Name"

    invoke-virtual {v8, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_19
    :goto_c
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->i(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-ne v4, v10, :cond_1a

    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4, v3}, Lcom/starmicronics/stario/StarProxiPRNTManager;->a(Lcom/starmicronics/stario/StarProxiPRNTManager;I)I

    :cond_1a
    iget-object v4, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c(Lcom/starmicronics/stario/StarProxiPRNTManager;)Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    move-result-object v4

    invoke-interface {v4, v5, v2, v0, v3}, Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;->onPortDiscovered(Ljava/lang/String;Lcom/starmicronics/stario/StarProxiPRNTManagerCallback$StarDeviceType;Ljava/lang/String;I)V

    goto/16 :goto_d

    :cond_1b
    :try_start_1
    const-string v2, "RSSI"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Date"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v2

    add-int/2addr v2, v10

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v10, v2, v3

    aput p2, v2, v10

    const-string v3, "Stock"

    invoke-virtual {v7, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Last Max RSSI"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Count"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "found Port Name"

    invoke-virtual {v7, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->d(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Count"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "found Port Name"

    invoke-virtual {v8, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6

    return-void

    :cond_1c
    const-string v2, "RSSI"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Date"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c()I

    move-result v2

    add-int/2addr v2, v10

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v10, v2, v3

    aput p2, v2, v10

    const-string v3, "Stock"

    invoke-virtual {v7, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Last Max RSSI"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Count"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "found Port Name"

    invoke-virtual {v7, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->d(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Count"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "found Port Name"

    invoke-virtual {v8, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/starmicronics/stario/StarProxiPRNTManager$1;->b:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1d
    :goto_d
    return-void
.end method
