.class Lcom/starmicronics/starmgsio/T$b;
.super Ljava/lang/Thread;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/T;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field a:Lcom/starmicronics/starmgsio/p$a;

.field final synthetic b:Lcom/starmicronics/starmgsio/T;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/T;Lcom/starmicronics/starmgsio/p$a;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/starmicronics/starmgsio/T$b;->a:Lcom/starmicronics/starmgsio/p$a;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    iget-object v0, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->a:Lcom/starmicronics/starmgsio/p$a;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    sget-object v3, Lcom/starmicronics/starmgsio/p$b;->c:Lcom/starmicronics/starmgsio/p$b;

    invoke-interface {v1, v2, v3}, Lcom/starmicronics/starmgsio/p$a;->a(Lcom/starmicronics/starmgsio/p;Lcom/starmicronics/starmgsio/p$b;)V

    monitor-exit v0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    iget-object v3, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/T;->b(Lcom/starmicronics/starmgsio/T;)Lcom/starmicronics/starmgsio/ConnectionInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Ljava/lang/String;)Landroid/hardware/usb/UsbDevice;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDevice;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->c(Lcom/starmicronics/starmgsio/T;)Landroid/content/Context;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "usb"

    :try_start_1
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbManager;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDevice;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->a:Lcom/starmicronics/starmgsio/p$a;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    sget-object v3, Lcom/starmicronics/starmgsio/p$b;->d:Lcom/starmicronics/starmgsio/p$b;

    invoke-interface {v1, v2, v3}, Lcom/starmicronics/starmgsio/p$a;->a(Lcom/starmicronics/starmgsio/p;Lcom/starmicronics/starmgsio/p$b;)V

    monitor-exit v0

    return-void

    :cond_1
    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    new-instance v3, Lcom/starmicronics/starmgsio/S;

    iget-object v4, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v4}, Lcom/starmicronics/starmgsio/T;->c(Lcom/starmicronics/starmgsio/T;)Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0x2710

    invoke-direct {v3, v4, v5}, Lcom/starmicronics/starmgsio/S;-><init>(Landroid/content/Context;I)V

    invoke-static {v2, v3}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Lcom/starmicronics/starmgsio/S;)Lcom/starmicronics/starmgsio/S;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/T;->d(Lcom/starmicronics/starmgsio/T;)Lcom/starmicronics/starmgsio/S;

    move-result-object v2

    iget-object v3, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDevice;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/starmicronics/starmgsio/S;->a(Landroid/hardware/usb/UsbDevice;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_2

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1, v3}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Lcom/starmicronics/starmgsio/S;)Lcom/starmicronics/starmgsio/S;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1, v3}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDevice;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->a:Lcom/starmicronics/starmgsio/p$a;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    sget-object v3, Lcom/starmicronics/starmgsio/p$b;->f:Lcom/starmicronics/starmgsio/p$b;

    invoke-interface {v1, v2, v3}, Lcom/starmicronics/starmgsio/p$a;->a(Lcom/starmicronics/starmgsio/p;Lcom/starmicronics/starmgsio/p$b;)V

    monitor-exit v0

    return-void

    :cond_2
    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v2, v3}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Lcom/starmicronics/starmgsio/S;)Lcom/starmicronics/starmgsio/S;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    iget-object v4, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v4}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDevice;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbInterface;)Landroid/hardware/usb/UsbInterface;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/T;->e(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbInterface;

    move-result-object v2

    if-eqz v2, :cond_7

    if-nez v1, :cond_3

    goto/16 :goto_2

    :cond_3
    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/T;->e(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbInterface;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_6

    iget-object v4, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v4}, Lcom/starmicronics/starmgsio/T;->e(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbInterface;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v6

    if-eqz v6, :cond_5

    const/16 v7, 0x80

    if-eq v6, v7, :cond_4

    goto :goto_1

    :cond_4
    iget-object v6, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v6, v4}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbEndpoint;)Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    :cond_5
    iget-object v6, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v6, v4}, Lcom/starmicronics/starmgsio/T;->b(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbEndpoint;)Landroid/hardware/usb/UsbEndpoint;

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    iget-object v3, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDevice;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbDeviceConnection;)Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->f(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v6

    const/16 v7, 0x41

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x3e8

    invoke-virtual/range {v6 .. v13}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->b(Lcom/starmicronics/starmgsio/T;)Lcom/starmicronics/starmgsio/ConnectionInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getBaudRate()I

    move-result v1

    const/4 v2, 0x4

    new-array v11, v2, [B

    shr-int/lit8 v2, v1, 0x0

    int-to-byte v2, v2

    aput-byte v2, v11, v5

    shr-int/lit8 v2, v1, 0x8

    int-to-byte v2, v2

    const/4 v3, 0x1

    aput-byte v2, v11, v3

    const/4 v2, 0x2

    shr-int/lit8 v3, v1, 0x10

    int-to-byte v3, v3

    aput-byte v3, v11, v2

    const/4 v2, 0x3

    shr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, v11, v2

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->f(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v6

    const/16 v7, 0x41

    const/16 v8, 0x1e

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x4

    const/16 v13, 0x3e8

    invoke-virtual/range {v6 .. v13}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->a:Lcom/starmicronics/starmgsio/p$a;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    sget-object v3, Lcom/starmicronics/starmgsio/p$b;->a:Lcom/starmicronics/starmgsio/p$b;

    invoke-interface {v1, v2, v3}, Lcom/starmicronics/starmgsio/p$a;->a(Lcom/starmicronics/starmgsio/p;Lcom/starmicronics/starmgsio/p$b;)V

    monitor-exit v0

    return-void

    :cond_7
    :goto_2
    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1, v3}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDevice;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$b;->a:Lcom/starmicronics/starmgsio/p$a;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$b;->b:Lcom/starmicronics/starmgsio/T;

    sget-object v3, Lcom/starmicronics/starmgsio/p$b;->g:Lcom/starmicronics/starmgsio/p$b;

    invoke-interface {v1, v2, v3}, Lcom/starmicronics/starmgsio/p$a;->a(Lcom/starmicronics/starmgsio/p;Lcom/starmicronics/starmgsio/p$b;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
