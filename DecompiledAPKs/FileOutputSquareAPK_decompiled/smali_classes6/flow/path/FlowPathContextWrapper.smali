.class public final Lflow/path/FlowPathContextWrapper;
.super Landroid/content/ContextWrapper;
.source "FlowPathContextWrapper.java"


# static fields
.field static final LOCAL_WRAPPER_SERVICE:Ljava/lang/String; = "flow_local_screen_context_wrapper"


# instance fields
.field private inflater:Landroid/view/LayoutInflater;

.field final localScreen:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p2, p0, Lflow/path/FlowPathContextWrapper;->localScreen:Ljava/lang/Object;

    return-void
.end method

.method static get(Landroid/content/Context;)Lflow/path/FlowPathContextWrapper;
    .locals 1

    const-string v0, "flow_local_screen_context_wrapper"

    .line 14
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lflow/path/FlowPathContextWrapper;

    return-object p0
.end method


# virtual methods
.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const-string v0, "flow_local_screen_context_wrapper"

    .line 25
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const-string v0, "layout_inflater"

    .line 28
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29
    iget-object p1, p0, Lflow/path/FlowPathContextWrapper;->inflater:Landroid/view/LayoutInflater;

    if-nez p1, :cond_1

    .line 30
    invoke-virtual {p0}, Lflow/path/FlowPathContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lflow/path/FlowPathContextWrapper;->inflater:Landroid/view/LayoutInflater;

    .line 32
    :cond_1
    iget-object p1, p0, Lflow/path/FlowPathContextWrapper;->inflater:Landroid/view/LayoutInflater;

    return-object p1

    .line 34
    :cond_2
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
