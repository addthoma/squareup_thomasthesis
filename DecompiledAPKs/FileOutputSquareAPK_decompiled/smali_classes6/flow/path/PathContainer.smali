.class public abstract Lflow/path/PathContainer;
.super Ljava/lang/Object;
.source "PathContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflow/path/PathContainer$NullViewState;,
        Lflow/path/PathContainer$TraversalState;
    }
.end annotation


# static fields
.field private static final NULL_VIEW_STATE:Lflow/ViewState;


# instance fields
.field private final tagKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 31
    new-instance v0, Lflow/path/PathContainer$NullViewState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lflow/path/PathContainer$NullViewState;-><init>(Lflow/path/PathContainer$1;)V

    sput-object v0, Lflow/path/PathContainer;->NULL_VIEW_STATE:Lflow/ViewState;

    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput p1, p0, Lflow/path/PathContainer;->tagKey:I

    return-void
.end method

.method private varargs doShowPath(Lflow/path/Path;Landroid/view/ViewGroup;Landroid/view/View;Lflow/Direction;Lflow/ViewState;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
    .locals 6

    .line 101
    invoke-direct {p0, p2}, Lflow/path/PathContainer;->ensureTag(Landroid/view/ViewGroup;)Lflow/path/PathContainer$TraversalState;

    move-result-object v2

    if-eqz p3, :cond_0

    .line 105
    invoke-static {v2}, Lflow/path/PathContainer$TraversalState;->access$100(Lflow/path/PathContainer$TraversalState;)Lflow/path/Path;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 106
    invoke-virtual {p3}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object p3

    aput-object p3, v1, v3

    const-string p3, "Container view has child %s with no path"

    .line 105
    invoke-static {v0, p3, v1}, Lflow/path/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lflow/path/Path;

    .line 107
    invoke-virtual {p3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 108
    invoke-interface {p6}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    .line 113
    :cond_0
    invoke-virtual {v2, p1, p5}, Lflow/path/PathContainer$TraversalState;->setNextEntry(Lflow/path/Path;Lflow/ViewState;)V

    move-object v0, p0

    move-object v1, p2

    move-object v3, p4

    move-object v4, p6

    move-object v5, p7

    .line 114
    invoke-virtual/range {v0 .. v5}, Lflow/path/PathContainer;->performTraversal(Landroid/view/ViewGroup;Lflow/path/PathContainer$TraversalState;Lflow/Direction;Lflow/TraversalCallback;[Lflow/path/PathContext;)V

    return-void
.end method

.method private ensureTag(Landroid/view/ViewGroup;)Lflow/path/PathContainer$TraversalState;
    .locals 2

    .line 121
    iget v0, p0, Lflow/path/PathContainer;->tagKey:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/path/PathContainer$TraversalState;

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Lflow/path/PathContainer$TraversalState;

    invoke-direct {v0}, Lflow/path/PathContainer$TraversalState;-><init>()V

    .line 124
    iget v1, p0, Lflow/path/PathContainer;->tagKey:I

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final varargs executeFlowTraversal(Landroid/view/ViewGroup;Lflow/Traversal;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
    .locals 9

    const/4 v0, 0x0

    .line 82
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 83
    iget-object v0, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->currentViewState()Lflow/ViewState;

    move-result-object v6

    .line 84
    iget-object v0, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lflow/path/Path;

    iget-object v5, p2, Lflow/Traversal;->direction:Lflow/Direction;

    move-object v1, p0

    move-object v3, p1

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, Lflow/path/PathContainer;->doShowPath(Lflow/path/Path;Landroid/view/ViewGroup;Landroid/view/View;Lflow/Direction;Lflow/ViewState;Lflow/TraversalCallback;[Lflow/path/PathContext;)V

    return-void
.end method

.method protected varargs abstract performTraversal(Landroid/view/ViewGroup;Lflow/path/PathContainer$TraversalState;Lflow/Direction;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
.end method

.method public varargs setPath(Landroid/view/ViewGroup;Lflow/path/Path;Lflow/Direction;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
    .locals 9

    const/4 v0, 0x0

    .line 94
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    sget-object v6, Lflow/path/PathContainer;->NULL_VIEW_STATE:Lflow/ViewState;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v5, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lflow/path/PathContainer;->doShowPath(Lflow/path/Path;Landroid/view/ViewGroup;Landroid/view/View;Lflow/Direction;Lflow/ViewState;Lflow/TraversalCallback;[Lflow/path/PathContext;)V

    return-void
.end method
