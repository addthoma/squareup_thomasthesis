.class abstract Lflow/Flow$PendingTraversal;
.super Ljava/lang/Object;
.source "Flow.java"

# interfaces
.implements Lflow/TraversalCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Flow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "PendingTraversal"
.end annotation


# instance fields
.field next:Lflow/Flow$PendingTraversal;

.field nextHistory:Lflow/History;

.field state:Lflow/Flow$TraversalState;

.field final synthetic this$0:Lflow/Flow;


# direct methods
.method private constructor <init>(Lflow/Flow;)V
    .locals 0

    .line 251
    iput-object p1, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    sget-object p1, Lflow/Flow$TraversalState;->ENQUEUED:Lflow/Flow$TraversalState;

    iput-object p1, p0, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    return-void
.end method

.method synthetic constructor <init>(Lflow/Flow;Lflow/Flow$1;)V
    .locals 0

    .line 251
    invoke-direct {p0, p1}, Lflow/Flow$PendingTraversal;-><init>(Lflow/Flow;)V

    return-void
.end method


# virtual methods
.method dispatch(Lflow/History;Lflow/Direction;)V
    .locals 3

    const-string v0, "nextHistory"

    .line 283
    invoke-static {p1, v0}, Lflow/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 284
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$400(Lflow/Flow;)Lflow/Dispatcher;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 288
    new-instance v0, Lflow/Traversal;

    iget-object v1, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-virtual {v1}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lflow/Traversal;-><init>(Lflow/History;Lflow/History;Lflow/Direction;)V

    .line 289
    iget-object p1, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {p1}, Lflow/Flow;->access$500(Lflow/Flow;)Lflow/Processor;

    move-result-object p1

    invoke-interface {p1, v0}, Lflow/Processor;->process(Lflow/Traversal;)Lflow/Traversal;

    move-result-object p1

    .line 291
    invoke-virtual {v0, p1}, Lflow/Traversal;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 292
    new-instance v0, Lflow/Traversal;

    iget-object p2, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-virtual {p2}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object p2

    iget-object v1, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    .line 293
    invoke-virtual {v1}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v1

    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-static {v1, v2}, Lflow/Flow;->access$100(Lflow/History;Lflow/History;)Lflow/History;

    move-result-object v1

    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-direct {v0, p2, v1, p1}, Lflow/Traversal;-><init>(Lflow/History;Lflow/History;Lflow/Direction;)V

    .line 296
    :cond_0
    iget-object p1, v0, Lflow/Traversal;->destination:Lflow/History;

    iput-object p1, p0, Lflow/Flow$PendingTraversal;->nextHistory:Lflow/History;

    .line 297
    iget-object p1, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {p1}, Lflow/Flow;->access$400(Lflow/Flow;)Lflow/Dispatcher;

    move-result-object p1

    if-nez p1, :cond_1

    .line 300
    invoke-virtual {p0}, Lflow/Flow$PendingTraversal;->onTraversalCompleted()V

    goto :goto_0

    .line 302
    :cond_1
    iget-object p1, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {p1}, Lflow/Flow;->access$400(Lflow/Flow;)Lflow/Dispatcher;

    move-result-object p1

    invoke-interface {p1, v0, p0}, Lflow/Dispatcher;->dispatch(Lflow/Traversal;Lflow/TraversalCallback;)V

    :goto_0
    return-void

    .line 285
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "Bad doExecute method allowed dispatcher to be cleared"

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method abstract doExecute()V
.end method

.method enqueue(Lflow/Flow$PendingTraversal;)V
    .locals 1

    .line 258
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->next:Lflow/Flow$PendingTraversal;

    if-nez v0, :cond_0

    .line 259
    iput-object p1, p0, Lflow/Flow$PendingTraversal;->next:Lflow/Flow$PendingTraversal;

    goto :goto_0

    .line 261
    :cond_0
    invoke-virtual {v0, p1}, Lflow/Flow$PendingTraversal;->enqueue(Lflow/Flow$PendingTraversal;)V

    :goto_0
    return-void
.end method

.method final execute()V
    .locals 3

    .line 307
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    sget-object v1, Lflow/Flow$TraversalState;->ENQUEUED:Lflow/Flow$TraversalState;

    if-ne v0, v1, :cond_1

    .line 308
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$400(Lflow/Flow;)Lflow/Dispatcher;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310
    sget-object v0, Lflow/Flow$TraversalState;->DISPATCHED:Lflow/Flow$TraversalState;

    iput-object v0, p0, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    .line 311
    invoke-virtual {p0}, Lflow/Flow$PendingTraversal;->doExecute()V

    return-void

    .line 308
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Caller must ensure that dispatcher is set"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 307
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public onTraversalCompleted()V
    .locals 3

    .line 266
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    sget-object v1, Lflow/Flow$TraversalState;->DISPATCHED:Lflow/Flow$TraversalState;

    if-eq v0, v1, :cond_1

    .line 267
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    sget-object v2, Lflow/Flow$TraversalState;->FINISHED:Lflow/Flow$TraversalState;

    if-ne v1, v2, :cond_0

    const-string v1, "onComplete already called for this transition"

    goto :goto_0

    :cond_0
    const-string v1, "transition not yet dispatched!"

    :goto_0
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :cond_1
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->nextHistory:Lflow/History;

    if-eqz v0, :cond_2

    .line 273
    iget-object v1, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {v1, v0}, Lflow/Flow;->access$202(Lflow/Flow;Lflow/History;)Lflow/History;

    .line 275
    :cond_2
    sget-object v0, Lflow/Flow$TraversalState;->FINISHED:Lflow/Flow$TraversalState;

    iput-object v0, p0, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    .line 276
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    iget-object v1, p0, Lflow/Flow$PendingTraversal;->next:Lflow/Flow$PendingTraversal;

    invoke-static {v0, v1}, Lflow/Flow;->access$302(Lflow/Flow;Lflow/Flow$PendingTraversal;)Lflow/Flow$PendingTraversal;

    .line 277
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$400(Lflow/Flow;)Lflow/Dispatcher;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$300(Lflow/Flow;)Lflow/Flow$PendingTraversal;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 278
    iget-object v0, p0, Lflow/Flow$PendingTraversal;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$300(Lflow/Flow;)Lflow/Flow$PendingTraversal;

    move-result-object v0

    invoke-virtual {v0}, Lflow/Flow$PendingTraversal;->execute()V

    :cond_3
    return-void
.end method
