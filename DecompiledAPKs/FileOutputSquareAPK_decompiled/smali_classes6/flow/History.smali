.class public final Lflow/History;
.super Ljava/lang/Object;
.source "History.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflow/History$ReadStateIterator;,
        Lflow/History$ReverseIterator;,
        Lflow/History$HistoryIterable;,
        Lflow/History$Builder;,
        Lflow/History$Entry;,
        Lflow/History$Filter;
    }
.end annotation


# instance fields
.field private final history:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lflow/History$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lflow/History$Entry;",
            ">;)V"
        }
    .end annotation

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 108
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "History may not be empty"

    invoke-static {v0, v1}, Lflow/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 109
    iput-object p1, p0, Lflow/History;->history:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lflow/History$1;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lflow/History;-><init>(Ljava/util/List;)V

    return-void
.end method

.method public static emptyBuilder()Lflow/History$Builder;
    .locals 3

    .line 99
    new-instance v0, Lflow/History$Builder;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lflow/History$Builder;-><init>(Ljava/util/Collection;Lflow/History$1;)V

    return-object v0
.end method

.method public static from(Landroid/os/Parcelable;Lflow/KeyParceler;)Lflow/History;
    .locals 4

    .line 44
    check-cast p0, Landroid/os/Bundle;

    .line 47
    const-class v0, Lflow/Flow;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v0, "ENTRIES"

    .line 48
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 50
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    const-string v2, "OBJECT"

    .line 51
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-interface {p1, v2}, Lflow/KeyParceler;->unwrap(Landroid/os/Parcelable;)Ljava/lang/Object;

    move-result-object v2

    .line 52
    new-instance v3, Lflow/History$Entry;

    invoke-direct {v3, v2}, Lflow/History$Entry;-><init>(Ljava/lang/Object;)V

    const-string v2, "VIEW_STATE"

    .line 53
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v1

    iput-object v1, v3, Lflow/History$Entry;->viewState:Landroid/util/SparseArray;

    .line 54
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :cond_0
    new-instance p0, Lflow/History;

    invoke-direct {p0, v0}, Lflow/History;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method public static single(Ljava/lang/Object;)Lflow/History;
    .locals 1

    .line 104
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public buildUpon()Lflow/History$Builder;
    .locals 3

    .line 158
    new-instance v0, Lflow/History$Builder;

    iget-object v1, p0, Lflow/History;->history:Ljava/util/List;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lflow/History$Builder;-><init>(Ljava/util/Collection;Lflow/History$1;)V

    return-object v0
.end method

.method public currentViewState()Lflow/ViewState;
    .locals 2

    .line 147
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/ViewState;

    :goto_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    .line 166
    instance-of v0, p1, Lflow/History;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    check-cast p1, Lflow/History;

    iget-object p1, p1, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public framesFromBottom()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/lang/Iterable<",
            "TT;>;"
        }
    .end annotation

    .line 113
    new-instance v0, Lflow/History$HistoryIterable;

    iget-object v1, p0, Lflow/History;->history:Ljava/util/List;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lflow/History$HistoryIterable;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public framesFromTop()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/lang/Iterable<",
            "TT;>;"
        }
    .end annotation

    .line 117
    new-instance v0, Lflow/History$HistoryIterable;

    iget-object v1, p0, Lflow/History;->history:Ljava/util/List;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lflow/History$HistoryIterable;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public getParcelable(Lflow/KeyParceler;)Landroid/os/Parcelable;
    .locals 4

    .line 63
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 65
    iget-object v2, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflow/History$Entry;

    .line 66
    invoke-virtual {v3, p1}, Lflow/History$Entry;->getBundle(Lflow/KeyParceler;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string p1, "ENTRIES"

    .line 68
    invoke-virtual {v0, p1, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public getParcelable(Lflow/KeyParceler;Lflow/History$Filter;)Landroid/os/Parcelable;
    .locals 5

    .line 81
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 83
    iget-object v2, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 84
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 85
    invoke-interface {v2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflow/History$Entry;

    .line 86
    iget-object v4, v3, Lflow/History$Entry;->state:Ljava/lang/Object;

    invoke-interface {p2, v4}, Lflow/History$Filter;->apply(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 87
    invoke-virtual {v3, p1}, Lflow/History$Entry;->getBundle(Lflow/KeyParceler;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 90
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 93
    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    const-string p1, "ENTRIES"

    .line 94
    invoke-virtual {v0, p1, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 170
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public peek(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)TT;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/History$Entry;

    iget-object p1, p1, Lflow/History$Entry;->state:Ljava/lang/Object;

    :goto_0
    return-object p1
.end method

.method public peekViewState(I)Lflow/ViewState;
    .locals 2

    .line 143
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/ViewState;

    :goto_0
    return-object p1
.end method

.method public size()I
    .locals 1

    .line 121
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 162
    iget-object v0, p0, Lflow/History;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public top()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 126
    invoke-virtual {p0, v0}, Lflow/History;->peek(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
