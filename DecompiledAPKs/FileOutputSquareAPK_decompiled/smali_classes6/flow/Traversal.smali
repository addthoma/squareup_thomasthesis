.class public final Lflow/Traversal;
.super Ljava/lang/Object;
.source "Traversal.java"


# instance fields
.field public final destination:Lflow/History;

.field public final direction:Lflow/Direction;

.field public final origin:Lflow/History;


# direct methods
.method public constructor <init>(Lflow/History;)V
    .locals 1

    .line 12
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-direct {p0, p1, p1, v0}, Lflow/Traversal;-><init>(Lflow/History;Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method public constructor <init>(Lflow/History;Lflow/History;Lflow/Direction;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lflow/Traversal;->origin:Lflow/History;

    .line 17
    iput-object p2, p0, Lflow/Traversal;->destination:Lflow/History;

    .line 18
    iput-object p3, p0, Lflow/Traversal;->direction:Lflow/Direction;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_5

    .line 23
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 25
    :cond_1
    check-cast p1, Lflow/Traversal;

    .line 27
    iget-object v2, p0, Lflow/Traversal;->origin:Lflow/History;

    iget-object v3, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v2, v3}, Lflow/History;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 28
    :cond_2
    iget-object v2, p0, Lflow/Traversal;->destination:Lflow/History;

    iget-object v3, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2, v3}, Lflow/History;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 29
    :cond_3
    iget-object v2, p0, Lflow/Traversal;->direction:Lflow/Direction;

    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    if-eq v2, p1, :cond_4

    return v1

    :cond_4
    return v0

    :cond_5
    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 35
    iget-object v0, p0, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 36
    iget-object v1, p0, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 37
    iget-object v1, p0, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-virtual {v1}, Lflow/Direction;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Traversal{origin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", destination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", direction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
