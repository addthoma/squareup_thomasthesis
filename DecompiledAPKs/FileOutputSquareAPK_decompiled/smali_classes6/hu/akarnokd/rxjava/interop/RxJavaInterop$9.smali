.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$9;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lrx/Completable$Transformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Transformer(Lio/reactivex/CompletableTransformer;)Lrx/Completable$Transformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$transformer:Lio/reactivex/CompletableTransformer;


# direct methods
.method constructor <init>(Lio/reactivex/CompletableTransformer;)V
    .locals 0

    .line 591
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$9;->val$transformer:Lio/reactivex/CompletableTransformer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 591
    check-cast p1, Lrx/Completable;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$9;->call(Lrx/Completable;)Lrx/Completable;

    move-result-object p1

    return-object p1
.end method

.method public call(Lrx/Completable;)Lrx/Completable;
    .locals 1

    .line 594
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$9;->val$transformer:Lio/reactivex/CompletableTransformer;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;

    move-result-object p1

    invoke-interface {v0, p1}, Lio/reactivex/CompletableTransformer;->apply(Lio/reactivex/Completable;)Lio/reactivex/CompletableSource;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Completable(Lio/reactivex/CompletableSource;)Lrx/Completable;

    move-result-object p1

    return-object p1
.end method
