.class final Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;
.super Ljava/lang/Object;
.source "ObservableV1ToFlowableV2.java"

# interfaces
.implements Lorg/reactivestreams/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ObservableSubscriberSubscription"
.end annotation


# instance fields
.field final parent:Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber<",
            "*>;)V"
        }
    .end annotation

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;->parent:Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 107
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;->parent:Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;

    invoke-virtual {v0}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;->unsubscribe()V

    return-void
.end method

.method public request(J)V
    .locals 1

    .line 102
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;->parent:Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;

    invoke-virtual {v0, p1, p2}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;->requestMore(J)V

    return-void
.end method
