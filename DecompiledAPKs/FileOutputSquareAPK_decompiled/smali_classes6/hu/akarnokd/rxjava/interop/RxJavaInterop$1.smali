.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$1;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lio/reactivex/FlowableTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Observable$Transformer;)Lio/reactivex/FlowableTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/FlowableTransformer<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic val$transformer:Lrx/Observable$Transformer;


# direct methods
.method constructor <init>(Lrx/Observable$Transformer;)V
    .locals 0

    .line 203
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$1;->val$transformer:Lrx/Observable$Transformer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lio/reactivex/Flowable;)Lorg/reactivestreams/Publisher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Flowable<",
            "TT;>;)",
            "Lorg/reactivestreams/Publisher<",
            "TR;>;"
        }
    .end annotation

    .line 206
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$1;->val$transformer:Lrx/Observable$Transformer;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lorg/reactivestreams/Publisher;)Lrx/Observable;

    move-result-object p1

    invoke-interface {v0, p1}, Lrx/Observable$Transformer;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/Observable;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Flowable(Lrx/Observable;)Lio/reactivex/Flowable;

    move-result-object p1

    return-object p1
.end method
