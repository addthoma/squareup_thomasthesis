.class final Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2;
.super Lio/reactivex/Maybe;
.source "SingleV1ToMaybeV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/Maybe<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final source:Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lrx/Single;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Single<",
            "TT;>;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Lio/reactivex/Maybe;-><init>()V

    .line 31
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2;->source:Lrx/Single;

    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/MaybeObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/MaybeObserver<",
            "-TT;>;)V"
        }
    .end annotation

    .line 36
    new-instance v0, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;-><init>(Lio/reactivex/MaybeObserver;)V

    .line 37
    invoke-interface {p1, v0}, Lio/reactivex/MaybeObserver;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 38
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2;->source:Lrx/Single;

    invoke-virtual {p1, v0}, Lrx/Single;->subscribe(Lrx/SingleSubscriber;)Lrx/Subscription;

    return-void
.end method
