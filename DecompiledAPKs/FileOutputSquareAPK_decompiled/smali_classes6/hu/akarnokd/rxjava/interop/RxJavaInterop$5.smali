.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$5;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lio/reactivex/FlowableOperator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Operator(Lrx/Observable$Operator;)Lio/reactivex/FlowableOperator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/FlowableOperator<",
        "TR;TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$operator:Lrx/Observable$Operator;


# direct methods
.method constructor <init>(Lrx/Observable$Operator;)V
    .locals 0

    .line 298
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$5;->val$operator:Lrx/Observable$Operator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lorg/reactivestreams/Subscriber;)Lorg/reactivestreams/Subscriber;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/reactivestreams/Subscriber<",
            "-TR;>;)",
            "Lorg/reactivestreams/Subscriber<",
            "-TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 301
    new-instance v0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;-><init>(Lorg/reactivestreams/Subscriber;)V

    .line 302
    new-instance v1, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;

    invoke-direct {v1, v0}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;-><init>(Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;)V

    .line 303
    invoke-interface {p1, v1}, Lorg/reactivestreams/Subscriber;->onSubscribe(Lorg/reactivestreams/Subscription;)V

    .line 308
    :try_start_0
    iget-object v1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$5;->val$operator:Lrx/Observable$Operator;

    invoke-interface {v1, v0}, Lrx/Observable$Operator;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "The operator returned a null rx.Subscriber"

    invoke-static {v0, v1}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Subscriber;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 310
    invoke-static {v0}, Lio/reactivex/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    .line 311
    invoke-static {v0}, Lrx/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    .line 312
    invoke-interface {p1, v0}, Lorg/reactivestreams/Subscriber;->onError(Ljava/lang/Throwable;)V

    .line 313
    invoke-static {}, Lrx/observers/Subscribers;->empty()Lrx/Subscriber;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, Lrx/Subscriber;->unsubscribe()V

    .line 317
    :goto_0
    new-instance p1, Lhu/akarnokd/rxjava/interop/FlowableV2ToObservableV1$SourceSubscriber;

    invoke-direct {p1, v0}, Lhu/akarnokd/rxjava/interop/FlowableV2ToObservableV1$SourceSubscriber;-><init>(Lrx/Subscriber;)V

    .line 319
    invoke-virtual {v0, p1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    .line 320
    invoke-virtual {v0, p1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-object p1
.end method
