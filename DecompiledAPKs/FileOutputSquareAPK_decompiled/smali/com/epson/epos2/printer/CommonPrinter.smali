.class abstract Lcom/epson/epos2/printer/CommonPrinter;
.super Ljava/lang/Object;
.source "CommonPrinter.java"


# static fields
.field public static final ALIGN_CENTER:I = 0x1

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x2

.field public static final AUTOCUTTER_ERR:I = 0x2

.field public static final AUTORECOVER_ERR:I = 0x4

.field public static final BARCODE_CODABAR:I = 0x8

.field public static final BARCODE_CODE128:I = 0xa

.field public static final BARCODE_CODE39:I = 0x6

.field public static final BARCODE_CODE93:I = 0x9

.field public static final BARCODE_EAN13:I = 0x2

.field public static final BARCODE_EAN8:I = 0x4

.field public static final BARCODE_GS1_128:I = 0xb

.field public static final BARCODE_GS1_DATABAR_EXPANDED:I = 0xf

.field public static final BARCODE_GS1_DATABAR_LIMITED:I = 0xe

.field public static final BARCODE_GS1_DATABAR_OMNIDIRECTIONAL:I = 0xc

.field public static final BARCODE_GS1_DATABAR_TRUNCATED:I = 0xd

.field public static final BARCODE_ITF:I = 0x7

.field public static final BARCODE_JAN13:I = 0x3

.field public static final BARCODE_JAN8:I = 0x5

.field public static final BARCODE_UPC_A:I = 0x0

.field public static final BARCODE_UPC_E:I = 0x1

.field public static final BATTERY_OVERHEAT:I = 0x2

.field public static final COLOR_1:I = 0x1

.field public static final COLOR_2:I = 0x2

.field public static final COLOR_3:I = 0x3

.field public static final COLOR_4:I = 0x4

.field public static final COLOR_NONE:I = 0x0

.field public static final COMPRESS_AUTO:I = 0x2

.field public static final COMPRESS_DEFLATE:I = 0x1

.field public static final COMPRESS_NONE:I = 0x0

.field public static final COVER_OPEN:I = 0x4

.field public static final CUT_FEED:I = 0x1

.field public static final CUT_NO_FEED:I = 0x0

.field public static final CUT_RESERVE:I = 0x2

.field public static final DIRECTION_BOTTOM_TO_TOP:I = 0x1

.field public static final DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field public static final DIRECTION_RIGHT_TO_LEFT:I = 0x2

.field public static final DIRECTION_TOP_TO_BOTTOM:I = 0x3

.field public static final DRAWER_2PIN:I = 0x0

.field public static final DRAWER_5PIN:I = 0x1

.field public static final DRAWER_HIGH:I = 0x0

.field public static final DRAWER_LOW:I = 0x1

.field public static final EVENT_COVER_CLOSE:I = 0x3

.field public static final EVENT_COVER_OPEN:I = 0x4

.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_DRAWER_HIGH:I = 0x8

.field public static final EVENT_DRAWER_LOW:I = 0x9

.field public static final EVENT_OFFLINE:I = 0x1

.field public static final EVENT_ONLINE:I = 0x0

.field public static final EVENT_PAPER_EMPTY:I = 0x7

.field public static final EVENT_PAPER_NEAR_END:I = 0x6

.field public static final EVENT_PAPER_OK:I = 0x5

.field public static final EVENT_POWER_OFF:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field public static final FONT_A:I = 0x0

.field public static final FONT_B:I = 0x1

.field public static final FONT_C:I = 0x2

.field public static final FONT_D:I = 0x3

.field public static final FONT_E:I = 0x4

.field public static final HALFTONE_DITHER:I = 0x0

.field public static final HALFTONE_ERROR_DIFFUSION:I = 0x1

.field public static final HALFTONE_THRESHOLD:I = 0x2

.field public static final HEAD_OVERHEAT:I = 0x0

.field public static final HRI_ABOVE:I = 0x1

.field public static final HRI_BELOW:I = 0x2

.field public static final HRI_BOTH:I = 0x3

.field public static final HRI_NONE:I = 0x0

.field private static final INTERVAL_DEFAULT:I = 0xbb8

.field public static final LANG_EN:I = 0x0

.field public static final LANG_JA:I = 0x1

.field public static final LANG_KO:I = 0x4

.field public static final LANG_TH:I = 0x5

.field public static final LANG_VI:I = 0x6

.field public static final LANG_ZH_CN:I = 0x2

.field public static final LANG_ZH_TW:I = 0x3

.field public static final LEVEL_0:I = 0x0

.field public static final LEVEL_1:I = 0x1

.field public static final LEVEL_2:I = 0x2

.field public static final LEVEL_3:I = 0x3

.field public static final LEVEL_4:I = 0x4

.field public static final LEVEL_5:I = 0x5

.field public static final LEVEL_6:I = 0x6

.field public static final LEVEL_7:I = 0x7

.field public static final LEVEL_8:I = 0x8

.field public static final LEVEL_H:I = 0xc

.field public static final LEVEL_L:I = 0x9

.field public static final LEVEL_M:I = 0xa

.field public static final LEVEL_Q:I = 0xb

.field public static final LINE_MEDIUM:I = 0x1

.field public static final LINE_MEDIUM_DOUBLE:I = 0x4

.field public static final LINE_THICK:I = 0x2

.field public static final LINE_THICK_DOUBLE:I = 0x5

.field public static final LINE_THIN:I = 0x0

.field public static final LINE_THIN_DOUBLE:I = 0x3

.field public static final MECHANICAL_ERR:I = 0x1

.field private static final MIN_IMAGE_HEIGHT:I = 0x1

.field private static final MIN_IMAGE_WIDTH:I = 0x1

.field public static final MODEL_ANK:I = 0x0

.field public static final MODEL_CHINESE:I = 0x2

.field public static final MODEL_JAPANESE:I = 0x1

.field public static final MODEL_KOREAN:I = 0x4

.field public static final MODEL_SOUTHASIA:I = 0x6

.field public static final MODEL_TAIWAN:I = 0x3

.field public static final MODEL_THAI:I = 0x5

.field public static final MODE_GRAY16:I = 0x1

.field public static final MODE_MONO:I = 0x0

.field public static final MODE_MONO_HIGH_DENSITY:I = 0x2

.field public static final MOTOR_OVERHEAT:I = 0x1

.field public static final NO_ERR:I = 0x0

.field static final NO_EXCEPTION:I = 0x0

.field public static final PAPER_EMPTY:I = 0x2

.field public static final PAPER_NEAR_END:I = 0x1

.field public static final PAPER_OK:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field public static final PARAM_UNSPECIFIED:I = -0x1

.field public static final PULSE_100:I = 0x0

.field public static final PULSE_200:I = 0x1

.field public static final PULSE_300:I = 0x2

.field public static final PULSE_400:I = 0x3

.field public static final PULSE_500:I = 0x4

.field static final RETURN_NULL:I = 0x101

.field static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final SWITCH_OFF:I = 0x0

.field public static final SWITCH_ON:I = 0x1

.field public static final SYMBOL_AZTECCODE_COMPACT:I = 0xe

.field public static final SYMBOL_AZTECCODE_FULLRANGE:I = 0xd

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_12:I = 0x11

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_16:I = 0x12

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_8:I = 0x10

.field public static final SYMBOL_DATAMATRIX_SQUARE:I = 0xf

.field public static final SYMBOL_GS1_DATABAR_EXPANDED_STACKED:I = 0xc

.field public static final SYMBOL_GS1_DATABAR_STACKED:I = 0xa

.field public static final SYMBOL_GS1_DATABAR_STACKED_OMNIDIRECTIONAL:I = 0xb

.field public static final SYMBOL_MAXICODE_MODE_2:I = 0x5

.field public static final SYMBOL_MAXICODE_MODE_3:I = 0x6

.field public static final SYMBOL_MAXICODE_MODE_4:I = 0x7

.field public static final SYMBOL_MAXICODE_MODE_5:I = 0x8

.field public static final SYMBOL_MAXICODE_MODE_6:I = 0x9

.field public static final SYMBOL_PDF417_STANDARD:I = 0x0

.field public static final SYMBOL_PDF417_TRUNCATED:I = 0x1

.field public static final SYMBOL_QRCODE_MICRO:I = 0x4

.field public static final SYMBOL_QRCODE_MODEL_1:I = 0x2

.field public static final SYMBOL_QRCODE_MODEL_2:I = 0x3

.field public static final TRUE:I = 0x1

.field public static final UNKNOWN:I = -0x3

.field public static final UNRECOVER_ERR:I = 0x3

.field public static final WRONG_PAPER:I = 0x3


# instance fields
.field APMClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field APMInstance:Ljava/lang/Object;

.field APMPresent:Z

.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field mContext:Landroid/content/Context;

.field mInterval:I

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field mPrinterHandle:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 220
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method constructor <init>()V
    .locals 2

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 18
    iput-wide v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mContext:Landroid/content/Context;

    const/16 v1, 0xbb8

    .line 205
    iput v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->mInterval:I

    .line 211
    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    .line 212
    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 213
    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 214
    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 215
    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method private getEdcHandle()J
    .locals 2

    .line 284
    iget-wide v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    return-wide v0
.end method


# virtual methods
.method public addBarcode(Ljava/lang/String;IIIII)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v12, p0

    const/4 v13, 0x5

    new-array v0, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p1, v0, v14

    .line 1366
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x1

    aput-object v1, v0, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v16, 0x2

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x3

    aput-object v1, v0, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v18, 0x4

    aput-object v1, v0, v18

    const-string v10, "addBarcode"

    invoke-virtual {v12, v10, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1370
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    if-eqz p1, :cond_1

    .line 1376
    invoke-direct/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v2
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v11, p5

    int-to-long v8, v11

    move/from16 v0, p6

    int-to-long v6, v0

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v19, v6

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v21, v10

    move-wide/from16 v10, v19

    :try_start_1
    invoke-virtual/range {v1 .. v11}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddBarcode(JLjava/lang/String;IIIJJ)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v13, [Ljava/lang/Object;

    aput-object p1, v0, v14

    .line 1387
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v12, v1, v14, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v1, v21

    .line 1378
    :try_start_2
    new-instance v2, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v2, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v2

    :catch_0
    move-exception v0

    move-object/from16 v1, v21

    goto :goto_0

    :cond_1
    move-object v1, v10

    .line 1373
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v1, v10

    .line 1382
    :goto_0
    invoke-virtual {v12, v1, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1383
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v2

    new-array v3, v13, [Ljava/lang/Object;

    aput-object p1, v3, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v18

    invoke-virtual {v12, v1, v2, v3}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1384
    throw v0
.end method

.method public addCommand([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addCommand"

    .line 2044
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2048
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    if-eqz p1, :cond_1

    .line 2054
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddCommand(J[B)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 2065
    invoke-virtual {p0, v3, v2, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 2056
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4

    .line 2051
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 2060
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2061
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-virtual {p0, v3, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 2062
    throw v1
.end method

.method public addCut(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 1943
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addCut"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1947
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1949
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddCut(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1960
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1951
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1955
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1956
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1957
    throw v1
.end method

.method public addFeedLine(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 974
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addFeedLine"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 978
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 980
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddFeedLine(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 991
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 982
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 986
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 987
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 988
    throw v1
.end method

.method public addFeedUnit(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 937
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addFeedUnit"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 941
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 943
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddFeedUnit(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 954
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 945
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 949
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 950
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 951
    throw v1
.end method

.method public addHPosition(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 900
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextPosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 904
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 906
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddHPosition(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 917
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 908
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 912
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 913
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 914
    throw v1
.end method

.method public addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V
    .locals 32
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v4, p0

    move-object/from16 v2, p1

    const/4 v3, 0x7

    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    .line 1076
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v15, 0x1

    aput-object v5, v0, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/16 v23, 0x2

    aput-object v5, v0, v23

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/16 v24, 0x3

    aput-object v5, v0, v24

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v13, 0x4

    aput-object v5, v0, v13

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/16 v25, 0x5

    aput-object v5, v0, v25

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/16 v26, 0x6

    aput-object v5, v0, v26

    const-string v14, "addImage"

    invoke-virtual {v4, v14, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1080
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    if-eqz v2, :cond_9

    .line 1088
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 1089
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 1102
    new-array v11, v0, [I

    const/16 v5, 0x100

    new-array v10, v5, [D

    if-gt v15, v0, :cond_8

    if-gt v15, v12, :cond_8

    .line 1113
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v6, v7, :cond_0

    move-object/from16 v16, v2

    goto :goto_0

    .line 1117
    :cond_0
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v6, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_7

    move-object/from16 v16, v6

    :goto_0
    const/4 v6, 0x0

    :goto_1
    const-wide v17, 0x406fe00000000000L    # 255.0

    if-ge v6, v5, :cond_1

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    int-to-double v1, v6

    div-double v1, v1, v17

    sub-double/2addr v7, v1

    .line 1124
    aput-wide v7, v10, v6

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v2, p1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    mul-int/lit8 v1, v0, 0x3

    mul-int v1, v1, v12

    .line 1127
    new-array v2, v1, [B

    const/4 v1, 0x0

    const/16 v20, 0x0

    :goto_2
    if-ge v1, v12, :cond_5

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v21, 0x1

    move-object/from16 v5, v16

    move-object v6, v11

    move v8, v0

    move-object/from16 v22, v10

    move v10, v1

    move-object/from16 v27, v11

    move v11, v0

    move v3, v12

    move/from16 v12, v21

    .line 1130
    invoke-virtual/range {v5 .. v12}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_3
    if-ge v5, v0, :cond_4

    .line 1134
    aget v7, v27, v6

    shr-int/lit8 v7, v7, 0x18

    const/16 v8, 0xff

    and-int/2addr v7, v8

    .line 1135
    aget v9, v27, v6

    shr-int/lit8 v9, v9, 0x10

    and-int/2addr v9, v8

    .line 1136
    aget v10, v27, v6

    shr-int/lit8 v10, v10, 0x8

    and-int/2addr v10, v8

    .line 1137
    aget v11, v27, v6

    and-int/2addr v11, v8

    if-ne v8, v7, :cond_2

    int-to-byte v7, v11

    .line 1140
    aput-byte v7, v2, v20

    add-int/lit8 v7, v20, 0x1

    int-to-byte v8, v10

    .line 1141
    aput-byte v8, v2, v7

    add-int/lit8 v7, v20, 0x2

    int-to-byte v8, v9

    .line 1142
    aput-byte v8, v2, v7

    :goto_4
    move-object/from16 v30, v14

    goto :goto_5

    :cond_2
    if-nez v7, :cond_3

    const/4 v7, -0x1

    .line 1146
    aput-byte v7, v2, v20

    add-int/lit8 v8, v20, 0x1

    .line 1147
    aput-byte v7, v2, v8

    add-int/lit8 v8, v20, 0x2

    .line 1148
    aput-byte v7, v2, v8

    goto :goto_4

    :cond_3
    int-to-double v7, v7

    .line 1152
    aget-wide v11, v22, v11

    mul-double v11, v11, v7

    sub-double v11, v17, v11

    double-to-int v11, v11

    int-to-byte v11, v11

    aput-byte v11, v2, v20

    add-int/lit8 v11, v20, 0x1

    .line 1153
    aget-wide v28, v22, v10
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_5

    mul-double v28, v28, v7

    move-object/from16 v30, v14

    sub-double v13, v17, v28

    double-to-int v10, v13

    int-to-byte v10, v10

    :try_start_1
    aput-byte v10, v2, v11

    add-int/lit8 v10, v20, 0x2

    .line 1154
    aget-wide v11, v22, v9

    mul-double v7, v7, v11

    sub-double v7, v17, v7

    double-to-int v7, v7

    int-to-byte v7, v7

    aput-byte v7, v2, v10

    :goto_5
    add-int/lit8 v20, v20, 0x3

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v14, v30

    const/4 v13, 0x4

    goto :goto_3

    :cond_4
    move-object/from16 v30, v14

    add-int/lit8 v1, v1, 0x1

    move v12, v3

    move-object/from16 v10, v22

    move-object/from16 v11, v27

    const/4 v3, 0x7

    const/4 v13, 0x4

    goto/16 :goto_2

    :cond_5
    move v3, v12

    move-object/from16 v30, v14

    .line 1162
    invoke-direct/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v17
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    int-to-long v5, v0

    int-to-long v7, v3

    move/from16 v3, p2

    int-to-long v9, v3

    move/from16 v1, p3

    int-to-long v11, v1

    move/from16 v13, p4

    int-to-long v0, v13

    move-object/from16 v31, v30

    move-wide v13, v0

    move/from16 v1, p5

    move-object v0, v2

    int-to-long v2, v1

    move-wide v15, v2

    const/4 v2, 0x0

    move-object/from16 v1, p0

    move-wide/from16 v2, v17

    move-object v4, v0

    move/from16 v17, p6

    move/from16 v18, p7

    move/from16 v19, p8

    move-wide/from16 v20, p9

    move/from16 v22, p11

    :try_start_2
    invoke-virtual/range {v1 .. v22}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddImage(J[BJJJJJJIIIDI)I

    move-result v0
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v0, :cond_6

    const/4 v1, 0x7

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 1178
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v23

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v24

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x4

    aput-object v1, v0, v4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v25

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v26

    move-object/from16 v5, p0

    move-object/from16 v6, v31

    invoke-virtual {v5, v6, v2, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_6
    move-object/from16 v6, v31

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x4

    move-object/from16 v5, p0

    .line 1164
    :try_start_3
    new-instance v7, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v7, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v7

    :catch_0
    const/4 v4, 0x4

    move-object/from16 v5, p0

    move-object/from16 v6, v31

    goto :goto_7

    :catch_1
    move-exception v0

    move-object/from16 v6, v31

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x4

    move-object/from16 v5, p0

    goto :goto_9

    :catch_2
    move-object v5, v4

    move-object/from16 v6, v30

    goto :goto_6

    :catch_3
    move-exception v0

    move-object v5, v4

    move-object/from16 v6, v30

    goto :goto_8

    :cond_7
    move-object v5, v4

    move-object v6, v14

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x4

    .line 1119
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_8
    move-object v5, v4

    move-object v6, v14

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x4

    .line 1108
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v3}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_9
    move-object v5, v4

    move-object v6, v14

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x4

    .line 1083
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v3}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_3
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_6

    :catch_4
    move-exception v0

    goto :goto_9

    :catch_5
    move-object v5, v4

    move-object v6, v14

    :goto_6
    const/4 v4, 0x4

    .line 1173
    :catch_6
    :goto_7
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    .line 1174
    invoke-virtual {v5, v6, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1175
    throw v0

    :catch_7
    move-exception v0

    move-object v5, v4

    move-object v6, v14

    :goto_8
    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x4

    .line 1168
    :goto_9
    invoke-virtual {v5, v6, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1169
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v7

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v23

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v24

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v25

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v26

    invoke-virtual {v5, v6, v7, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1170
    throw v0
.end method

.method public addLineSpace(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 502
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addLineSpace"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 506
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 508
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddLineSpace(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 519
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 510
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 514
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 515
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 516
    throw v1
.end method

.method public addLogo(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1206
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addLogo"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1210
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1212
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v6

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddLogo(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1223
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1214
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1218
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1219
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v5, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1220
    throw v1
.end method

.method public addPageArea(IIII)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v12, p0

    const/4 v13, 0x4

    new-array v0, v13, [Ljava/lang/Object;

    .line 1676
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v14, 0x0

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x1

    aput-object v1, v0, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v16, 0x2

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x3

    aput-object v1, v0, v17

    const-string v10, "addPageArea"

    invoke-virtual {v12, v10, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1680
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1682
    invoke-direct/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v2
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v11, p1

    int-to-long v4, v11

    move/from16 v8, p2

    int-to-long v6, v8

    move/from16 v9, p3

    int-to-long v0, v9

    move/from16 v15, p4

    int-to-long v13, v15

    move-wide/from16 v18, v0

    move-object/from16 v1, p0

    move-wide/from16 v8, v18

    move-object v15, v10

    move-wide v10, v13

    :try_start_1
    invoke-virtual/range {v1 .. v11}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddPageArea(JJJJJ)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    .line 1693
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-virtual {v12, v15, v2, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1684
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v15, v10

    .line 1688
    :goto_0
    invoke-virtual {v12, v15, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1689
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v17

    invoke-virtual {v12, v15, v1, v2}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1690
    throw v0
.end method

.method public addPageBegin()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addPageBegin"

    .line 1582
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1586
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1588
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddPageBegin(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 1599
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1590
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1594
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1595
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1596
    throw v1
.end method

.method public addPageDirection(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 1722
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addPageDirection"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1726
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1728
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddPageDirection(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1739
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1730
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1734
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1735
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1736
    throw v1
.end method

.method public addPageEnd()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addPageEnd"

    .line 1619
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1623
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1625
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddPageEnd(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 1636
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1627
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1631
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1632
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1633
    throw v1
.end method

.method public addPageLine(IIIII)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v13, p0

    const/4 v14, 0x5

    new-array v0, v14, [Ljava/lang/Object;

    .line 1842
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x0

    aput-object v1, v0, v15

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v16, 0x1

    aput-object v1, v0, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x2

    aput-object v1, v0, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v18, 0x3

    aput-object v1, v0, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v19, 0x4

    aput-object v1, v0, v19

    const-string v12, "addPageLine"

    invoke-virtual {v13, v12, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1846
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1848
    invoke-direct/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v2
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v10, p1

    int-to-long v4, v10

    move/from16 v11, p2

    int-to-long v6, v11

    move/from16 v8, p3

    int-to-long v0, v8

    move/from16 v9, p4

    int-to-long v14, v9

    move-wide/from16 v20, v0

    move-object/from16 v1, p0

    move-wide/from16 v8, v20

    move-wide v10, v14

    move-object v14, v12

    move/from16 v12, p5

    :try_start_1
    invoke-virtual/range {v1 .. v12}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddPageLine(JJJJJI)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/Object;

    .line 1859
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    invoke-virtual {v13, v14, v2, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1850
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v14, v12

    .line 1854
    :goto_0
    invoke-virtual {v13, v14, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1855
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v19

    invoke-virtual {v13, v14, v1, v2}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1856
    throw v0
.end method

.method public addPagePosition(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1781
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addPagePosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1785
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1787
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v6

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddPagePosition(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1798
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1789
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1793
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1794
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v5, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1795
    throw v1
.end method

.method public addPageRectangle(IIIII)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v13, p0

    const/4 v14, 0x5

    new-array v0, v14, [Ljava/lang/Object;

    .line 1902
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x0

    aput-object v1, v0, v15

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v16, 0x1

    aput-object v1, v0, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x2

    aput-object v1, v0, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v18, 0x3

    aput-object v1, v0, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v19, 0x4

    aput-object v1, v0, v19

    const-string v12, "addPageRectangle"

    invoke-virtual {v13, v12, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1906
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 1908
    invoke-direct/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v2
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v10, p1

    int-to-long v4, v10

    move/from16 v11, p2

    int-to-long v6, v11

    move/from16 v8, p3

    int-to-long v0, v8

    move/from16 v9, p4

    int-to-long v14, v9

    move-wide/from16 v20, v0

    move-object/from16 v1, p0

    move-wide/from16 v8, v20

    move-wide v10, v14

    move-object v14, v12

    move/from16 v12, p5

    :try_start_1
    invoke-virtual/range {v1 .. v12}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddPageRectangle(JJJJJI)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/Object;

    .line 1919
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    invoke-virtual {v13, v14, v2, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1910
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v14, v12

    .line 1914
    :goto_0
    invoke-virtual {v13, v14, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1915
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v19

    invoke-virtual {v13, v14, v1, v2}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1916
    throw v0
.end method

.method public addPulse(II)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 2002
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addPulse"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2006
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 2008
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v5

    invoke-virtual {p0, v5, v6, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddPulse(JII)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 2019
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 2010
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 2014
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2015
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v5, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 2016
    throw v1
.end method

.method public addSymbol(Ljava/lang/String;IIIII)V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v13, p0

    const/4 v14, 0x6

    new-array v0, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object p1, v0, v15

    .line 1541
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x1

    aput-object v1, v0, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v16, 0x2

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x3

    aput-object v1, v0, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v18, 0x4

    aput-object v1, v0, v18

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v19, 0x5

    aput-object v1, v0, v19

    const-string v12, "addSymbol"

    invoke-virtual {v13, v12, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1545
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    if-eqz p1, :cond_1

    .line 1551
    invoke-direct/range {p0 .. p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v2
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v9, p4

    int-to-long v7, v9

    move/from16 v10, p5

    int-to-long v5, v10

    move/from16 v4, p6

    int-to-long v0, v4

    move-wide/from16 v20, v0

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-wide/from16 v22, v5

    move/from16 v5, p2

    move/from16 v6, p3

    move-wide/from16 v9, v22

    move-object/from16 v24, v12

    move-wide/from16 v11, v20

    :try_start_1
    invoke-virtual/range {v1 .. v12}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddSymbol(JLjava/lang/String;IIJJJ)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v14, [Ljava/lang/Object;

    aput-object p1, v0, v15

    .line 1562
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v13, v1, v15, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v1, v24

    const/4 v2, 0x1

    .line 1553
    :try_start_2
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3

    :catch_0
    move-exception v0

    move-object/from16 v1, v24

    goto :goto_0

    :cond_1
    move-object v1, v12

    const/4 v2, 0x1

    .line 1548
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v2}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v1, v12

    :goto_0
    const/4 v2, 0x1

    .line 1557
    :goto_1
    invoke-virtual {v13, v1, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1558
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v4, v14, [Ljava/lang/Object;

    aput-object p1, v4, v15

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v18

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v19

    invoke-virtual {v13, v1, v3, v4}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1559
    throw v0
.end method

.method public addText(Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addText"

    .line 588
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 592
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_4

    if-eqz p1, :cond_3

    :try_start_1
    const-string v1, "eu.epson.rdc.apm.APMInterface"

    .line 601
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMClass:Ljava/lang/Class;

    .line 603
    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMInstance:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 604
    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMInstance:Ljava/lang/Object;

    .line 606
    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMClass:Ljava/lang/Class;

    const-string v4, "APMInitialize"

    new-array v5, v0, [Ljava/lang/Class;

    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 607
    iget-object v4, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMInstance:Ljava/lang/Object;

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/epson/epos2/printer/CommonPrinter;->mContext:Landroid/content/Context;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 608
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMPresent:Z

    .line 612
    :cond_0
    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMClass:Ljava/lang/Class;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMInstance:Ljava/lang/Object;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMPresent:Z

    if-eqz v1, :cond_1

    .line 614
    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMClass:Ljava/lang/Class;

    const-string v4, "APMProcessChars"

    new-array v5, v0, [Ljava/lang/Class;

    const-class v6, Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 615
    iget-object v4, p0, Lcom/epson/epos2/printer/CommonPrinter;->APMInstance:Ljava/lang/Object;

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 616
    check-cast v1, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-object p1, v1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v4, "APM Exception: "

    new-array v5, v0, [Ljava/lang/Object;

    .line 632
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v4, "APM IllegalAccessException: "

    new-array v5, v0, [Ljava/lang/Object;

    .line 629
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v4, "APM IllegalArgumentException: "

    new-array v5, v0, [Ljava/lang/Object;

    .line 626
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 636
    :catch_3
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddText(JLjava/lang/String;)I

    move-result v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_4

    if-nez v1, :cond_2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 647
    invoke-virtual {p0, v3, v2, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 638
    :cond_2
    :try_start_3
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4

    .line 595
    :cond_3
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_3
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_3 .. :try_end_3} :catch_4

    :catch_4
    move-exception v1

    .line 642
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 643
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-virtual {p0, v3, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 644
    throw v1
.end method

.method public addTextAlign(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 466
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextAlign"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 471
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddTextAlign(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 482
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 473
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 477
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 478
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 479
    throw v1
.end method

.method public addTextFont(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 717
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextFont"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 721
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 723
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddTextFont(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 734
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 725
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 729
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 730
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 731
    throw v1
.end method

.method public addTextLang(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 678
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextLang"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 682
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 684
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddTextLang(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 695
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 686
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 690
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 691
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 692
    throw v1
.end method

.method public addTextRotate(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 545
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextRotate"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 549
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 551
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddTextRotate(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 562
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 553
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 557
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 558
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 559
    throw v1
.end method

.method public addTextSize(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 797
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addTextSize"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 801
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 803
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v6

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddTextSize(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 814
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 805
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 809
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 810
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v5, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 811
    throw v1
.end method

.method public addTextSmooth(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 753
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextSmooth"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 757
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 759
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddTextSmooth(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 770
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 761
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 765
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 766
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 767
    throw v1
.end method

.method public addTextStyle(IIII)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object v8, p0

    const/4 v9, 0x4

    new-array v0, v9, [Ljava/lang/Object;

    .line 858
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v10, 0x0

    aput-object v1, v0, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x1

    aput-object v1, v0, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x2

    aput-object v1, v0, v12

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x3

    aput-object v1, v0, v13

    const-string v14, "addTextStyle"

    invoke-virtual {p0, v14, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 862
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 864
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v2

    move-object v1, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v1 .. v7}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2AddTextStyle(JIIII)I

    move-result v0
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    new-array v0, v9, [Ljava/lang/Object;

    .line 875
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-virtual {p0, v14, v10, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 866
    :cond_0
    :try_start_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 870
    invoke-virtual {p0, v14, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 871
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-virtual {p0, v14, v1, v2}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 872
    throw v0
.end method

.method public beginTransaction()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "beginTransaction"

    .line 387
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 393
    iget-wide v3, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2BeginTransaction(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 404
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 395
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 399
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 400
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 401
    throw v1
.end method

.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 278
    iget-wide v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 279
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public clearCommandBuffer()V
    .locals 7

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "clearCommandBuffer"

    .line 293
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 296
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getEdcHandle()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2ClearCommandBuffer(J)I

    :cond_0
    new-array v1, v0, [Ljava/lang/Object;

    .line 299
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method public endTransaction()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "endTransaction"

    .line 422
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 428
    iget-wide v3, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2EndTransaction(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 439
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 430
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 434
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 435
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 436
    throw v1
.end method

.method public forcePulse(III)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    .line 2150
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const-string v2, "forcePulse"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2154
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 2156
    iget-wide v7, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    move-object v6, p0

    move v9, p1

    move v10, p2

    move v11, p3

    invoke-virtual/range {v6 .. v11}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2ForcePulse(JIII)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 2167
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v5

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 2158
    :cond_0
    :try_start_1
    new-instance v6, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v6, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v6
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 2162
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2163
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v6

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v5

    invoke-virtual {p0, v2, v6, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 2164
    throw v1
.end method

.method public forceRecover(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 2092
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "forceRecover"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2096
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 2098
    iget-wide v4, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2ForceRecover(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 2109
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 2100
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 2104
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2105
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 2106
    throw v1
.end method

.method public forceReset(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 2193
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "forceReset"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2197
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 2199
    iget-wide v4, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2ForceReset(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 2210
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 2201
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 2205
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2206
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 2207
    throw v1
.end method

.method public getInterval()I
    .locals 1

    .line 2255
    iget v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mInterval:I

    return v0
.end method

.method initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 2261
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    .line 2263
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 2264
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2265
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 2266
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2267
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 2268
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2269
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 2270
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2271
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2272
    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2274
    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2277
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected native nativeEpos2AddBarcode(JLjava/lang/String;IIIJJ)I
.end method

.method protected native nativeEpos2AddCommand(J[B)I
.end method

.method protected native nativeEpos2AddCut(JI)I
.end method

.method protected native nativeEpos2AddFeedLine(JJ)I
.end method

.method protected native nativeEpos2AddFeedUnit(JJ)I
.end method

.method protected native nativeEpos2AddHPosition(JJ)I
.end method

.method protected native nativeEpos2AddImage(J[BJJJJJJIIIDI)I
.end method

.method protected native nativeEpos2AddLineSpace(JJ)I
.end method

.method protected native nativeEpos2AddLogo(JJJ)I
.end method

.method protected native nativeEpos2AddPageArea(JJJJJ)I
.end method

.method protected native nativeEpos2AddPageBegin(J)I
.end method

.method protected native nativeEpos2AddPageDirection(JI)I
.end method

.method protected native nativeEpos2AddPageEnd(J)I
.end method

.method protected native nativeEpos2AddPageLine(JJJJJI)I
.end method

.method protected native nativeEpos2AddPagePosition(JJJ)I
.end method

.method protected native nativeEpos2AddPageRectangle(JJJJJI)I
.end method

.method protected native nativeEpos2AddPulse(JII)I
.end method

.method protected native nativeEpos2AddSymbol(JLjava/lang/String;IIJJJ)I
.end method

.method protected native nativeEpos2AddText(JLjava/lang/String;)I
.end method

.method protected native nativeEpos2AddTextAlign(JI)I
.end method

.method protected native nativeEpos2AddTextFont(JI)I
.end method

.method protected native nativeEpos2AddTextLang(JI)I
.end method

.method protected native nativeEpos2AddTextRotate(JI)I
.end method

.method protected native nativeEpos2AddTextSize(JJJ)I
.end method

.method protected native nativeEpos2AddTextSmooth(JI)I
.end method

.method protected native nativeEpos2AddTextStyle(JIIII)I
.end method

.method protected native nativeEpos2BeginTransaction(J)I
.end method

.method protected native nativeEpos2ClearCommandBuffer(J)I
.end method

.method protected native nativeEpos2EndTransaction(J)I
.end method

.method protected native nativeEpos2ForcePulse(JIII)I
.end method

.method protected native nativeEpos2ForceRecover(JI)I
.end method

.method protected native nativeEpos2ForceReset(JI)I
.end method

.method protected native nativeEpos2SetInterval(JJ)I
.end method

.method protected native nativeEpos2StartMonitor(J)I
.end method

.method protected native nativeEpos2StopMonitor(J)I
.end method

.method outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 2301
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 2283
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 2310
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 2292
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/printer/CommonPrinter;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/printer/CommonPrinter;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public setInterval(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 2225
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setInterval"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2229
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 2233
    iget-wide v4, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2SetInterval(JJ)I

    move-result v1

    if-nez v1, :cond_0

    .line 2238
    iput p1, p0, Lcom/epson/epos2/printer/CommonPrinter;->mInterval:I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 2246
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 2235
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 2241
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2242
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 2243
    throw v1
.end method

.method public startMonitor()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "startMonitor"

    .line 323
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 329
    iget-wide v3, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2StartMonitor(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 340
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 331
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 335
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 336
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 337
    throw v1
.end method

.method public stopMonitor()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "stopMonitor"

    .line 352
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/CommonPrinter;->checkHandle()V

    .line 358
    iget-wide v3, p0, Lcom/epson/epos2/printer/CommonPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/CommonPrinter;->nativeEpos2StopMonitor(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 369
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 360
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 364
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/CommonPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 365
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/CommonPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 366
    throw v1
.end method
