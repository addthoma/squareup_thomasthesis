.class public Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;
.super Ljava/lang/Object;
.source "CommBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/epos2/commbox/CommBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CommBoxSendMessageCallbackAdapter"
.end annotation


# instance fields
.field private mAdapterCommBoxHandle:J

.field private mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

.field private mListener:Lcom/epson/epos2/commbox/SendMessageCallback;

.field final synthetic this$0:Lcom/epson/epos2/commbox/CommBox;


# direct methods
.method public constructor <init>(Lcom/epson/epos2/commbox/CommBox;JLcom/epson/epos2/commbox/SendMessageCallback;Lcom/epson/epos2/commbox/CommBox;)V
    .locals 2

    .line 461
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->this$0:Lcom/epson/epos2/commbox/CommBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 457
    iput-wide v0, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mAdapterCommBoxHandle:J

    const/4 p1, 0x0

    .line 458
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mListener:Lcom/epson/epos2/commbox/SendMessageCallback;

    .line 459
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    .line 462
    iput-wide p2, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mAdapterCommBoxHandle:J

    .line 463
    iput-object p4, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mListener:Lcom/epson/epos2/commbox/SendMessageCallback;

    .line 464
    iput-object p5, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    return-void
.end method


# virtual methods
.method public getAdapterCommBoxHandle()J
    .locals 2

    .line 468
    iget-wide v0, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mAdapterCommBoxHandle:J

    return-wide v0
.end method

.method public onCommBoxSendMessage(JIJ)V
    .locals 8

    .line 474
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->this$0:Lcom/epson/epos2/commbox/CommBox;

    const/4 p2, 0x4

    new-array v0, p2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const-string v1, "-"

    const/4 v4, 0x2

    aput-object v1, v0, v4

    iget-object v5, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    const/4 v6, 0x3

    aput-object v5, v0, v6

    const-string v5, "onCommBoxSendMessage"

    invoke-static {p1, v5, v0}, Lcom/epson/epos2/commbox/CommBox;->access$000(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 476
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mListener:Lcom/epson/epos2/commbox/SendMessageCallback;

    if-eqz p1, :cond_0

    .line 479
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->this$0:Lcom/epson/epos2/commbox/CommBox;

    new-array v0, p2, [Ljava/lang/Object;

    const-string v7, "code->"

    aput-object v7, v0, v2

    .line 480
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v0, v3

    const-string v7, "count->"

    aput-object v7, v0, v4

    .line 481
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v0, v6

    .line 479
    invoke-static {p1, v5, v0}, Lcom/epson/epos2/commbox/CommBox;->access$100(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 483
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mListener:Lcom/epson/epos2/commbox/SendMessageCallback;

    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    long-to-int v7, p4

    invoke-interface {p1, v0, p3, v7}, Lcom/epson/epos2/commbox/SendMessageCallback;->onCommBoxSendMessage(Lcom/epson/epos2/commbox/CommBox;II)V

    .line 487
    :cond_0
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->this$0:Lcom/epson/epos2/commbox/CommBox;

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, p2, v2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    aput-object p3, p2, v3

    aput-object v1, p2, v4

    iget-object p3, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    aput-object p3, p2, v6

    invoke-static {p1, v5, v2, p2}, Lcom/epson/epos2/commbox/CommBox;->access$200(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method
