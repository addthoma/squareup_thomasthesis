.class public Lcom/epson/epos2/cat/DailyLog;
.super Ljava/lang/Object;
.source "DailyLog.java"


# instance fields
.field private kid:Ljava/lang/String;

.field salesAmount:J

.field salesCount:J

.field voidAmount:J

.field voidCount:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setKid(Ljava/lang/String;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/epson/epos2/cat/DailyLog;->kid:Ljava/lang/String;

    return-void
.end method

.method private setSalesAmount(J)V
    .locals 0

    .line 55
    iput-wide p1, p0, Lcom/epson/epos2/cat/DailyLog;->salesAmount:J

    return-void
.end method

.method private setSalesCount(J)V
    .locals 0

    .line 48
    iput-wide p1, p0, Lcom/epson/epos2/cat/DailyLog;->salesCount:J

    return-void
.end method

.method private setVoidAmount(J)V
    .locals 0

    .line 69
    iput-wide p1, p0, Lcom/epson/epos2/cat/DailyLog;->voidAmount:J

    return-void
.end method

.method private setVoidCount(J)V
    .locals 0

    .line 62
    iput-wide p1, p0, Lcom/epson/epos2/cat/DailyLog;->voidCount:J

    return-void
.end method


# virtual methods
.method public getKid()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/epson/epos2/cat/DailyLog;->kid:Ljava/lang/String;

    return-object v0
.end method

.method public getSalesAmount()J
    .locals 2

    .line 58
    iget-wide v0, p0, Lcom/epson/epos2/cat/DailyLog;->salesAmount:J

    return-wide v0
.end method

.method public getSalesCount()J
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/epson/epos2/cat/DailyLog;->salesCount:J

    return-wide v0
.end method

.method public getVoidAmount()J
    .locals 2

    .line 72
    iget-wide v0, p0, Lcom/epson/epos2/cat/DailyLog;->voidAmount:J

    return-wide v0
.end method

.method public getVoidCount()J
    .locals 2

    .line 65
    iget-wide v0, p0, Lcom/epson/epos2/cat/DailyLog;->voidCount:J

    return-wide v0
.end method
