.class public abstract Lcom/epson/eposdevice/scanner/Scanner;
.super Lcom/epson/eposdevice/scanner/NativeScanner;
.source "Scanner.java"


# instance fields
.field private mDataListener:Lcom/epson/eposdevice/scanner/DataListener;

.field private mScanHandle:J


# direct methods
.method protected constructor <init>(J)V
    .locals 2

    .line 15
    invoke-direct {p0}, Lcom/epson/eposdevice/scanner/NativeScanner;-><init>()V

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/epson/eposdevice/scanner/Scanner;->mDataListener:Lcom/epson/eposdevice/scanner/DataListener;

    const-wide/16 v0, 0x0

    .line 41
    iput-wide v0, p0, Lcom/epson/eposdevice/scanner/Scanner;->mScanHandle:J

    .line 16
    iput-wide p1, p0, Lcom/epson/eposdevice/scanner/Scanner;->mScanHandle:J

    return-void
.end method

.method private OnScanData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 9
    iget-object v0, p0, Lcom/epson/eposdevice/scanner/Scanner;->mDataListener:Lcom/epson/eposdevice/scanner/DataListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const-string v1, "onScanData"

    .line 10
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/scanner/Scanner;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iget-object v0, p0, Lcom/epson/eposdevice/scanner/Scanner;->mDataListener:Lcom/epson/eposdevice/scanner/DataListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/epson/eposdevice/scanner/DataListener;->onScanData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected getInnerHandle()J
    .locals 2

    .line 19
    iget-wide v0, p0, Lcom/epson/eposdevice/scanner/Scanner;->mScanHandle:J

    return-wide v0
.end method

.method protected innerDeleteInstance()V
    .locals 3

    .line 22
    iget-wide v0, p0, Lcom/epson/eposdevice/scanner/Scanner;->mScanHandle:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/scanner/Scanner;->nativeSetScanDataCallback(JLcom/epson/eposdevice/scanner/NativeScanner;)I

    const-wide/16 v0, 0x0

    .line 23
    iput-wide v0, p0, Lcom/epson/eposdevice/scanner/Scanner;->mScanHandle:J

    return-void
.end method

.method protected nativeOnScanData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/eposdevice/scanner/Scanner;->OnScanData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected abstract outputException(Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method protected varargs abstract outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public setDataEventCallback(Lcom/epson/eposdevice/scanner/DataListener;)V
    .locals 5

    .line 26
    iget-wide v0, p0, Lcom/epson/eposdevice/scanner/Scanner;->mScanHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 28
    iput-object p1, p0, Lcom/epson/eposdevice/scanner/Scanner;->mDataListener:Lcom/epson/eposdevice/scanner/DataListener;

    .line 29
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/scanner/Scanner;->nativeSetScanDataCallback(JLcom/epson/eposdevice/scanner/NativeScanner;)I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 32
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/scanner/Scanner;->nativeSetScanDataCallback(JLcom/epson/eposdevice/scanner/NativeScanner;)I

    .line 33
    iput-object p1, p0, Lcom/epson/eposdevice/scanner/Scanner;->mDataListener:Lcom/epson/eposdevice/scanner/DataListener;

    :cond_1
    :goto_0
    return-void
.end method
