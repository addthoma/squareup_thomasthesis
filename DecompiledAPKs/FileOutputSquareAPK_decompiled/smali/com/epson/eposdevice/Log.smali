.class public Lcom/epson/eposdevice/Log;
.super Ljava/lang/Object;
.source "Log.java"


# static fields
.field public static final LOG_DISABLE:I = 0x0

.field public static final LOG_LOW:I = 0x0

.field public static final LOG_PERMANENT:I = 0x1

.field public static final LOG_STORAGE:I = 0x1

.field public static final LOG_TCP:I = 0x2

.field public static final LOG_TEMPORARY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 9
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native eposSetLogSettings(Ljava/lang/String;IILjava/lang/String;III)I
.end method

.method public static setLogSettings(Landroid/content/Context;IILjava/lang/String;III)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 36
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p0, ""

    :goto_0
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    .line 42
    invoke-static/range {v0 .. v6}, Lcom/epson/eposdevice/Log;->eposSetLogSettings(Ljava/lang/String;IILjava/lang/String;III)I

    move-result p0

    if-nez p0, :cond_0

    return-void

    .line 44
    :cond_0
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, p0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
.end method
