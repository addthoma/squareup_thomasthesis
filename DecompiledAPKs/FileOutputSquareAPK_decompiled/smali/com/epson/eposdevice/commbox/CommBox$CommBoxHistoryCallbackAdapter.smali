.class public Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;
.super Ljava/lang/Object;
.source "CommBox.java"

# interfaces
.implements Lcom/epson/eposdevice/commbox/NativeCommBox$NativeCommBoxHistoryCallbackAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/commbox/CommBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CommBoxHistoryCallbackAdapter"
.end annotation


# instance fields
.field private mHistoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mListener:Lcom/epson/eposdevice/commbox/GetCommHistoryListener;

.field final synthetic this$0:Lcom/epson/eposdevice/commbox/CommBox;


# direct methods
.method public constructor <init>(Lcom/epson/eposdevice/commbox/CommBox;Lcom/epson/eposdevice/commbox/GetCommHistoryListener;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 104
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/GetCommHistoryListener;

    .line 105
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    .line 107
    iput-object p2, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/GetCommHistoryListener;

    return-void
.end method


# virtual methods
.method public addHistory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 124
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "senderId"

    .line 125
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "receiverId"

    .line 126
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "message"

    .line 127
    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onCommBoxHistory(JLjava/lang/String;IJ)V
    .locals 5

    .line 111
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/GetCommHistoryListener;

    if-eqz p1, :cond_1

    const/4 p1, 0x3

    const/4 p2, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x4

    const-string v3, "onGetCommHistory"

    if-nez p4, :cond_0

    .line 113
    iget-object v4, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBox;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v0

    iget-object v0, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    aput-object v0, v2, p2

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, v2, p1

    invoke-virtual {v4, v3, v2}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/GetCommHistoryListener;

    iget-object p2, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    long-to-int p6, p5

    invoke-interface {p1, p3, p4, p2, p6}, Lcom/epson/eposdevice/commbox/GetCommHistoryListener;->onGetCommHistory(Ljava/lang/String;ILjava/util/ArrayList;I)V

    goto :goto_0

    .line 117
    :cond_0
    iget-object v4, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBox;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x0

    aput-object v0, v2, p2

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, v2, p1

    invoke-virtual {v4, v3, v2}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/GetCommHistoryListener;

    long-to-int p2, p5

    invoke-interface {p1, p3, p4, v0, p2}, Lcom/epson/eposdevice/commbox/GetCommHistoryListener;->onGetCommHistory(Ljava/lang/String;ILjava/util/ArrayList;I)V

    :cond_1
    :goto_0
    return-void
.end method
