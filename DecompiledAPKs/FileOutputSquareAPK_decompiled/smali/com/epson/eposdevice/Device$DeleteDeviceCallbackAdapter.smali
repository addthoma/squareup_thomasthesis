.class Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;
.super Ljava/lang/Object;
.source "Device.java"

# interfaces
.implements Lcom/epson/eposdevice/NativeDevice$NativeDeleteDeviceCallbackAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteDeviceCallbackAdapter"
.end annotation


# instance fields
.field private mDeleteObj:Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

.field private mListener:Lcom/epson/eposdevice/DeleteDeviceListener;

.field final synthetic this$0:Lcom/epson/eposdevice/Device;


# direct methods
.method public constructor <init>(Lcom/epson/eposdevice/Device;Lcom/epson/eposdevice/DeleteDeviceListener;Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;)V
    .locals 0

    .line 438
    iput-object p1, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 435
    iput-object p1, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->mListener:Lcom/epson/eposdevice/DeleteDeviceListener;

    .line 436
    iput-object p1, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->mDeleteObj:Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

    .line 439
    iput-object p2, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->mListener:Lcom/epson/eposdevice/DeleteDeviceListener;

    .line 440
    iput-object p3, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->mDeleteObj:Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

    return-void
.end method


# virtual methods
.method public nativeOnDeleteDevice(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .line 444
    iget-object v0, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->mListener:Lcom/epson/eposdevice/DeleteDeviceListener;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "OnDeleteDevice"

    invoke-virtual {v0, v2, v1}, Lcom/epson/eposdevice/Device;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446
    iget-object v0, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->mListener:Lcom/epson/eposdevice/DeleteDeviceListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/epson/eposdevice/DeleteDeviceListener;->onDeleteDevice(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    if-nez p3, :cond_1

    .line 449
    iget-object p1, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-static {p1}, Lcom/epson/eposdevice/Device;->access$000(Lcom/epson/eposdevice/Device;)Ljava/util/Vector;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->mDeleteObj:Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

    invoke-virtual {p1, p2}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 450
    iget-object p1, p0, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;->mDeleteObj:Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

    invoke-interface {p1}, Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;->deleteInstance()V

    :cond_1
    return-void
.end method
