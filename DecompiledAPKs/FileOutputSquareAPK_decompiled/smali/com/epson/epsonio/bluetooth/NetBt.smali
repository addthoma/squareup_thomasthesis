.class public Lcom/epson/epsonio/bluetooth/NetBt;
.super Ljava/lang/Object;
.source "NetBt.java"


# static fields
.field private static final M_INVALID_HANDLE_INDEX:I = -0x1

.field private static final M_IO_INTERVAL_MSEC:I = 0xa

.field private static final M_MAX_SENT_SIZE_AT_ONCE:I = 0x1000

.field private static final M_MAX_SOCKET_NUM:I = 0x10

.field private static final M_SDK_INT:I

.field private static final M_SERIAL_PORT_SERVICE_CLASS_UUID:Ljava/util/UUID;

.field private static mBtPairedNg:Ljava/lang/String;

.field private static mBtPairedNgField:Ljava/lang/reflect/Field;

.field private static mBtPairedOk:Ljava/lang/String;

.field private static mBtPairedOkField:Ljava/lang/reflect/Field;

.field private static mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private static mOutputLogInfoMethod:Ljava/lang/reflect/Method;

.field private static mSocketMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Landroid/bluetooth/BluetoothSocket;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mSocketMap:Ljava/util/Map;

    .line 32
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/epson/epsonio/bluetooth/NetBt;->M_SDK_INT:I

    const-string v0, "00001101-0000-1000-8000-00805F9B34FB"

    .line 33
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->M_SERIAL_PORT_SERVICE_CLASS_UUID:Ljava/util/UUID;

    const/4 v0, 0x0

    .line 36
    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mClassOutputLog:Ljava/lang/Class;

    .line 37
    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mOutputLogInfoMethod:Ljava/lang/reflect/Method;

    .line 39
    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedOkField:Ljava/lang/reflect/Field;

    .line 40
    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedNgField:Ljava/lang/reflect/Field;

    .line 42
    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedOk:Ljava/lang/String;

    .line 43
    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedNg:Ljava/lang/String;

    .line 47
    :try_start_0
    invoke-static {}, Lcom/epson/epsonio/bluetooth/NetBt;->initializeOuputLogFunctions()V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static close(I)I
    .locals 2

    .line 191
    :try_start_0
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mSocketMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothSocket;

    if-nez v0, :cond_0

    const/4 p0, 0x6

    return p0

    .line 195
    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    .line 198
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mSocketMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x0

    return p0

    :catch_0
    const/16 p0, 0xff

    return p0
.end method

.method private static initializeOuputLogFunctions()V
    .locals 6

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 470
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mClassOutputLog:Ljava/lang/Class;

    .line 472
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogInfo"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const/4 v3, 0x2

    const-class v5, [Ljava/lang/Object;

    aput-object v5, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mOutputLogInfoMethod:Ljava/lang/reflect/Method;

    .line 473
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mOutputLogInfoMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 475
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "LOGIF_OP_STR_BT_PAIRED_OK"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedOkField:Ljava/lang/reflect/Field;

    .line 476
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedOkField:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 477
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedOkField:Ljava/lang/reflect/Field;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedOk:Ljava/lang/String;

    .line 478
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mClassOutputLog:Ljava/lang/Class;

    const-string v2, "LOGIF_OP_STR_BT_PAIRED_NG"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedNgField:Ljava/lang/reflect/Field;

    .line 479
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedNgField:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 480
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedNgField:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedNg:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 483
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public static kill(I)I
    .locals 1

    .line 211
    :try_start_0
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mSocketMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/bluetooth/BluetoothSocket;

    if-nez p0, :cond_0

    const/4 p0, 0x6

    return p0

    .line 215
    :cond_0
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x0

    return p0

    :catch_0
    const/16 p0, 0xff

    return p0
.end method

.method public static open(Ljava/lang/String;Ljava/lang/String;[I)I
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "open"

    .line 60
    new-instance v1, Lcom/epson/epsonio/bluetooth/AdapterHandler;

    invoke-direct {v1}, Lcom/epson/epsonio/bluetooth/AdapterHandler;-><init>()V

    const/4 v2, 0x1

    if-nez p0, :cond_0

    return v2

    :cond_0
    if-eqz p1, :cond_1

    return v2

    :cond_1
    if-nez p2, :cond_2

    return v2

    .line 76
    :cond_2
    array-length p1, p2

    if-nez p1, :cond_3

    return v2

    :cond_3
    const/4 p1, -0x1

    const/4 v3, 0x0

    .line 79
    aput p1, p2, v3

    .line 81
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    return v2

    :cond_4
    const/4 v4, 0x2

    const/4 v5, 0x0

    .line 86
    :try_start_0
    invoke-virtual {v1}, Lcom/epson/epsonio/bluetooth/AdapterHandler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    cmp-long v10, v6, v8

    if-eqz v10, :cond_5

    .line 88
    :try_start_1
    invoke-virtual {v1}, Lcom/epson/epsonio/bluetooth/AdapterHandler;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1
    :try_end_1
    .catch Lcom/epson/epsonio/EpsonIoException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    :catch_0
    move-exception p0

    .line 91
    :try_start_2
    invoke-virtual {p0}, Lcom/epson/epsonio/EpsonIoException;->getStatus()I

    move-result p0

    return p0

    .line 95
    :cond_5
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_6

    return v4

    .line 102
    :cond_6
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    if-nez v6, :cond_7

    return v4

    .line 107
    :cond_7
    :try_start_3
    invoke-virtual {v1, p0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    const/16 v7, 0xa

    .line 113
    :try_start_4
    sget v8, Lcom/epson/epsonio/bluetooth/NetBt;->M_SDK_INT:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    if-gt v7, v8, :cond_8

    .line 115
    :try_start_5
    sget-object v7, Lcom/epson/epsonio/bluetooth/NetBt;->M_SERIAL_PORT_SERVICE_CLASS_UUID:Ljava/util/UUID;

    invoke-virtual {v6, v7}, Landroid/bluetooth/BluetoothDevice;->createInsecureRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v5
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 118
    :catch_1
    :try_start_6
    sget-object v7, Lcom/epson/epsonio/bluetooth/NetBt;->M_SERIAL_PORT_SERVICE_CLASS_UUID:Ljava/util/UUID;

    invoke-virtual {v6, v7}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v5

    goto :goto_1

    .line 122
    :cond_8
    sget-object v7, Lcom/epson/epsonio/bluetooth/NetBt;->M_SERIAL_PORT_SERVICE_CLASS_UUID:Ljava/util/UUID;

    invoke-virtual {v6, v7}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v5

    :goto_1
    if-nez v5, :cond_9

    goto :goto_5

    :cond_9
    const/4 v6, 0x0

    .line 130
    :goto_2
    aget v7, p2, v3

    if-ne p1, v7, :cond_b

    const/16 v7, 0x10

    if-ge v6, v7, :cond_b

    .line 131
    sget-object v7, Lcom/epson/epsonio/bluetooth/NetBt;->mSocketMap:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_a

    .line 132
    aput v6, p2, v3

    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_b
    const/16 v6, 0xff

    const/4 v6, 0x0

    const/16 v7, 0xff

    :goto_3
    const/4 v8, 0x3

    if-ge v6, v8, :cond_d

    .line 138
    aget v7, p2, v3
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    if-ne p1, v7, :cond_c

    const/4 v7, 0x6

    goto :goto_4

    .line 143
    :cond_c
    :try_start_7
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 144
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothSocket;->connect()V

    new-array v7, v4, [Ljava/lang/Object;

    .line 145
    sget-object v8, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedOk:Ljava/lang/String;

    aput-object v8, v7, v3

    aput-object p0, v7, v2

    invoke-static {v0, v7}, Lcom/epson/epsonio/bluetooth/NetBt;->outputLogInfo(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    const/4 v4, 0x0

    goto :goto_5

    :catch_2
    :try_start_8
    new-array v7, v4, [Ljava/lang/Object;

    .line 150
    sget-object v8, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedNg:Ljava/lang/String;

    aput-object v8, v7, v3

    aput-object p0, v7, v2

    invoke-static {v0, v7}, Lcom/epson/epsonio/bluetooth/NetBt;->outputLogInfo(Ljava/lang/String;[Ljava/lang/Object;)V

    const-wide/16 v7, 0x64

    .line 152
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    const/4 v7, 0x2

    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_d
    move v4, v7

    goto :goto_5

    :catch_3
    return v2

    :catch_4
    nop

    :goto_5
    if-nez v4, :cond_e

    .line 164
    sget-object p0, Lcom/epson/epsonio/bluetooth/NetBt;->mSocketMap:Ljava/util/Map;

    aget p1, p2, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p0, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_e
    if-eqz v5, :cond_f

    .line 169
    :try_start_9
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_6

    :catch_5
    nop

    .line 177
    :cond_f
    :goto_6
    aget p0, p2, v3

    if-eq p1, p0, :cond_10

    .line 178
    aput p1, p2, v3

    :cond_10
    :goto_7
    return v4
.end method

.method private static varargs outputLogInfo(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .line 489
    :try_start_0
    sget-object v0, Lcom/epson/epsonio/bluetooth/NetBt;->mOutputLogInfoMethod:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/epson/epsonio/bluetooth/NetBt;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 p0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, p0

    const/4 p0, 0x2

    aput-object p1, v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static read(I[BIII[I)I
    .locals 16

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p4

    move-object/from16 v3, p5

    const/4 v4, 0x1

    if-eqz v3, :cond_12

    .line 329
    array-length v5, v3

    if-le v4, v5, :cond_0

    goto/16 :goto_4

    :cond_0
    const/4 v5, 0x0

    .line 333
    aput v5, v3, v5

    if-gez p2, :cond_1

    return v4

    :cond_1
    if-gez v1, :cond_2

    return v4

    :cond_2
    if-gez v2, :cond_3

    return v4

    :cond_3
    const/16 v6, 0xff

    .line 349
    :try_start_0
    sget-object v7, Lcom/epson/epsonio/bluetooth/NetBt;->mSocketMap:Ljava/util/Map;

    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    if-nez v7, :cond_4

    const/4 v0, 0x6

    return v0

    :cond_4
    if-nez v1, :cond_5

    return v5

    :cond_5
    add-int v8, p2, v1

    if-gez v8, :cond_6

    return v6

    :cond_6
    if-eqz v0, :cond_11

    .line 370
    array-length v9, v0

    if-ge v9, v8, :cond_7

    goto/16 :goto_3

    :cond_7
    const/4 v8, 0x3

    .line 376
    :try_start_1
    invoke-virtual {v7}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 382
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 389
    :goto_0
    :try_start_2
    invoke-virtual {v7}, Ljava/io/InputStream;->available()I

    move-result v11
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-lez v11, :cond_f

    .line 396
    aget v12, v3, v5

    sub-int v12, v1, v12

    if-ge v12, v11, :cond_8

    aget v11, v3, v5

    sub-int v11, v1, v11

    .line 398
    :cond_8
    :try_start_3
    aget v12, v3, v5

    add-int v12, p2, v12

    invoke-virtual {v7, v0, v12, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v11
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    if-lez v11, :cond_f

    .line 405
    aget v2, v3, v5

    add-int/2addr v2, v11

    aput v2, v3, v5

    .line 422
    aget v2, v3, v5

    if-lt v2, v1, :cond_9

    return v5

    :cond_9
    const/4 v2, 0x1

    :cond_a
    :goto_1
    if-ne v4, v2, :cond_e

    .line 432
    :try_start_4
    invoke-virtual {v7}, Ljava/io/InputStream;->available()I

    move-result v6
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    if-gtz v6, :cond_b

    goto :goto_2

    .line 442
    :cond_b
    aget v9, v3, v5

    sub-int v9, v1, v9

    if-ge v9, v6, :cond_c

    aget v6, v3, v5

    sub-int v6, v1, v6

    .line 445
    :cond_c
    :try_start_5
    aget v9, v3, v5

    add-int v9, p2, v9

    invoke-virtual {v7, v0, v9, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    if-gtz v6, :cond_d

    goto :goto_2

    .line 455
    :cond_d
    aget v9, v3, v5

    add-int/2addr v9, v6

    aput v9, v3, v5

    .line 457
    aget v6, v3, v5

    if-lt v6, v1, :cond_a

    const/4 v2, 0x0

    goto :goto_1

    :catch_0
    return v8

    :cond_e
    :goto_2
    return v5

    :catch_1
    return v8

    .line 410
    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v9

    int-to-long v13, v2

    cmp-long v15, v11, v13

    if-lez v15, :cond_10

    const/4 v0, 0x4

    return v0

    :cond_10
    const-wide/16 v11, 0xa

    .line 415
    :try_start_6
    invoke-static {v11, v12}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    :catch_2
    return v6

    :catch_3
    return v8

    :cond_11
    :goto_3
    return v4

    :catch_4
    return v6

    :cond_12
    :goto_4
    return v4
.end method

.method public static write(I[BIII[I)I
    .locals 10

    const/4 v0, 0x1

    if-eqz p5, :cond_d

    .line 228
    array-length v1, p5

    if-le v0, v1, :cond_0

    goto/16 :goto_2

    :cond_0
    const/4 v1, 0x0

    .line 232
    aput v1, p5, v1

    if-gez p2, :cond_1

    return v0

    :cond_1
    if-gez p3, :cond_2

    return v0

    :cond_2
    if-gez p4, :cond_3

    return v0

    :cond_3
    const/16 v2, 0xff

    .line 248
    :try_start_0
    sget-object v3, Lcom/epson/epsonio/bluetooth/NetBt;->mSocketMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    if-nez p0, :cond_4

    const/4 p0, 0x6

    return p0

    :cond_4
    if-nez p3, :cond_5

    return v1

    :cond_5
    add-int v3, p2, p3

    if-gez v3, :cond_6

    return v2

    :cond_6
    if-eqz p1, :cond_c

    .line 269
    array-length v4, p1

    if-ge v4, v3, :cond_7

    goto :goto_1

    .line 276
    :cond_7
    :try_start_1
    new-array v3, p3, [B

    .line 277
    invoke-static {p1, p2, v3, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 285
    :try_start_2
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 291
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 294
    aput v1, p5, v1

    :goto_0
    aget p2, p5, v1

    if-ge p2, p3, :cond_b

    .line 296
    aget p2, p5, v1

    sub-int p2, p3, p2

    const/16 v6, 0x1000

    if-ge p2, v6, :cond_8

    aget p2, p5, v1

    sub-int v6, p3, p2

    .line 299
    :cond_8
    :try_start_3
    aget p2, p5, v1

    invoke-virtual {p1, v3, p2, v6}, Ljava/io/OutputStream;->write([BII)V

    .line 301
    aget p2, p5, v1

    add-int/2addr p2, v6

    aput p2, p5, v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 306
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    const-class v6, Ljava/io/IOException;

    invoke-virtual {p2, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_9

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    .line 307
    sget-object v6, Lcom/epson/epsonio/bluetooth/NetBt;->mBtPairedNg:Ljava/lang/String;

    aput-object v6, p2, v1

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    aput-object v6, p2, v0

    const-string/jumbo v6, "write"

    invoke-static {v6, p2}, Lcom/epson/epsonio/bluetooth/NetBt;->outputLogInfo(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    int-to-long v8, p4

    cmp-long p2, v6, v8

    if-lez p2, :cond_a

    const/4 p0, 0x4

    return p0

    :cond_a
    const-wide/16 v6, 0xa

    .line 315
    :try_start_4
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    return v2

    :cond_b
    return v1

    :catch_2
    const/4 p0, 0x3

    return p0

    :catch_3
    const/4 p0, 0x5

    return p0

    :cond_c
    :goto_1
    return v0

    :catch_4
    return v2

    :cond_d
    :goto_2
    return v0
.end method
