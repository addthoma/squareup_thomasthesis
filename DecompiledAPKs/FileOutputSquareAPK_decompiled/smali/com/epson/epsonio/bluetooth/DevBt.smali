.class public Lcom/epson/epsonio/bluetooth/DevBt;
.super Ljava/lang/Object;
.source "DevBt.java"


# static fields
.field public static final FALSE:I = 0x0

.field private static final MBT_DEV_SUB_CLASS_PRINTER:I = 0x80

.field private static final MBT_DEV_SUB_CLASS_PRINTER_MASK:I = 0x80

.field public static final TRUE:I = 0x1

.field private static mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private static mContext:Landroid/content/Context;

.field private static mDeviceInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/epsonio/DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static mIsFinding:Z

.field private static mReceiver:Landroid/content/BroadcastReceiver;

.field private static final unregisterReceiverCallbackHandler:Landroid/os/Handler;

.field private static unregisterReceiverSynchronizer:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 30
    sput-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 31
    sput-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 32
    sput-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->mContext:Landroid/content/Context;

    const/4 v0, 0x0

    .line 33
    sput-boolean v0, Lcom/epson/epsonio/bluetooth/DevBt;->mIsFinding:Z

    .line 36
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->unregisterReceiverSynchronizer:Ljava/lang/Integer;

    .line 294
    new-instance v0, Lcom/epson/epsonio/bluetooth/DevBt$2;

    .line 295
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/epson/epsonio/bluetooth/DevBt$2;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->unregisterReceiverCallbackHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/ArrayList;
    .locals 1

    .line 25
    sget-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100()Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .line 25
    sget-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/Integer;
    .locals 1

    .line 25
    sget-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->unregisterReceiverSynchronizer:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$300()Landroid/content/BroadcastReceiver;
    .locals 1

    .line 25
    sget-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$400()Landroid/content/Context;
    .locals 1

    .line 25
    sget-object v0, Lcom/epson/epsonio/bluetooth/DevBt;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private static deviceFilter(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "TM-T"

    .line 313
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    const-string v1, "TM-P"

    .line 316
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "TM-m"

    .line 320
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    const-string v1, "UB-B"

    .line 324
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    const-string v1, "BT304"

    .line 327
    invoke-virtual {p0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_0

    :cond_4
    const-string v1, "TM-H"

    .line 331
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p0, :cond_5

    goto :goto_0

    :catch_0
    :cond_5
    :goto_1
    return v0
.end method

.method public static final getResult([II)[Lcom/epson/epsonio/DeviceInfo;
    .locals 14

    const/4 v0, 0x0

    if-eqz p0, :cond_7

    .line 188
    array-length v1, p0

    if-nez v1, :cond_0

    goto/16 :goto_3

    :cond_0
    const/16 v1, 0xff

    const/4 v2, 0x0

    .line 192
    aput v1, p0, v2

    .line 194
    sget-boolean v1, Lcom/epson/epsonio/bluetooth/DevBt;->mIsFinding:Z

    if-nez v1, :cond_1

    const/4 p1, 0x6

    .line 195
    aput p1, p0, v2

    return-object v0

    .line 202
    :cond_1
    :try_start_0
    sget-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :try_start_1
    sget-object v3, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 208
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    .line 209
    :goto_0
    sget-object v5, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 211
    sget-object v5, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/epson/epsonio/DeviceInfo;

    const/4 v6, -0x2

    const/4 v7, 0x1

    if-eq p1, v6, :cond_2

    if-eq p1, v7, :cond_2

    :goto_1
    const/4 v6, 0x1

    goto :goto_2

    .line 216
    :cond_2
    invoke-virtual {v5}, Lcom/epson/epsonio/DeviceInfo;->getPrinterName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/epson/epsonio/bluetooth/DevBt;->deviceFilter(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    :goto_2
    if-ne v7, v6, :cond_4

    .line 228
    new-instance v6, Lcom/epson/epsonio/DeviceInfo;

    invoke-virtual {v5}, Lcom/epson/epsonio/DeviceInfo;->getDeviceType()I

    move-result v9

    invoke-virtual {v5}, Lcom/epson/epsonio/DeviceInfo;->getPrinterName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Lcom/epson/epsonio/DeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    const-string v13, ""

    move-object v8, v6

    invoke-direct/range {v8 .. v13}, Lcom/epson/epsonio/DeviceInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 232
    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result p1

    new-array p1, p1, [Lcom/epson/epsonio/DeviceInfo;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/epson/epsonio/DeviceInfo;

    move-object v0, p1

    .line 234
    :cond_6
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    :try_start_2
    aput v2, p0, v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :catchall_0
    move-exception p1

    .line 234
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    const/4 p1, 0x5

    .line 239
    aput p1, p0, v2

    :cond_7
    :goto_3
    return-object v0
.end method

.method public static final start(Landroid/content/Context;ILjava/lang/String;I)I
    .locals 8

    .line 43
    sget-boolean v0, Lcom/epson/epsonio/bluetooth/DevBt;->mIsFinding:Z

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    const/4 p0, 0x6

    return p0

    :cond_0
    const/16 v0, 0x102

    if-eq v0, p1, :cond_1

    return v1

    :cond_1
    if-eqz p2, :cond_2

    return v1

    :cond_2
    if-eq p3, v1, :cond_3

    if-eqz p3, :cond_3

    return v1

    :cond_3
    if-nez p0, :cond_4

    return v1

    .line 67
    :cond_4
    sget-object p1, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    monitor-enter p1

    .line 69
    :try_start_0
    sget-object p2, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 70
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    sput-object p0, Lcom/epson/epsonio/bluetooth/DevBt;->mContext:Landroid/content/Context;

    .line 75
    sget-object p0, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez p0, :cond_6

    .line 76
    new-instance p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;

    invoke-direct {p0}, Lcom/epson/epsonio/bluetooth/AdapterHandler;-><init>()V

    .line 77
    invoke-virtual {p0}, Lcom/epson/epsonio/bluetooth/AdapterHandler;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->getId()J

    move-result-wide p1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-eqz v0, :cond_5

    .line 79
    :try_start_1
    invoke-virtual {p0}, Lcom/epson/epsonio/bluetooth/AdapterHandler;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object p0

    sput-object p0, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    :try_end_1
    .catch Lcom/epson/epsonio/EpsonIoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 82
    invoke-virtual {p0}, Lcom/epson/epsonio/EpsonIoException;->getStatus()I

    move-result p0

    return p0

    .line 86
    :cond_5
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object p0

    sput-object p0, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 90
    :cond_6
    :goto_0
    sget-object p0, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/16 p1, 0xff

    if-nez p0, :cond_7

    return p1

    .line 94
    :cond_7
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result p0

    if-nez p0, :cond_8

    return p1

    :cond_8
    if-ne p3, v1, :cond_9

    .line 100
    sget-object p0, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object p0

    if-eqz p0, :cond_9

    .line 102
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result p2

    if-lez p2, :cond_9

    .line 103
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_9

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/bluetooth/BluetoothDevice;

    .line 104
    new-instance p3, Lcom/epson/epsonio/DeviceInfo;

    const/16 v3, 0x102

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const-string v7, ""

    move-object v2, p3

    invoke-direct/range {v2 .. v7}, Lcom/epson/epsonio/DeviceInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    sget-object p2, Lcom/epson/epsonio/bluetooth/DevBt;->mDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 113
    :cond_9
    new-instance p0, Lcom/epson/epsonio/bluetooth/DevBt$1;

    invoke-direct {p0}, Lcom/epson/epsonio/bluetooth/DevBt$1;-><init>()V

    sput-object p0, Lcom/epson/epsonio/bluetooth/DevBt;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 164
    new-instance p0, Landroid/content/IntentFilter;

    const-string p2, "android.bluetooth.device.action.FOUND"

    invoke-direct {p0, p2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string p2, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    .line 166
    invoke-virtual {p0, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 168
    sget-object p2, Lcom/epson/epsonio/bluetooth/DevBt;->mContext:Landroid/content/Context;

    sget-object p3, Lcom/epson/epsonio/bluetooth/DevBt;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p2, p3, p0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 172
    sget-object p0, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    move-result p0

    if-nez p0, :cond_a

    .line 173
    invoke-static {}, Lcom/epson/epsonio/bluetooth/DevBt;->stop()I

    return p1

    .line 177
    :cond_a
    sput-boolean v1, Lcom/epson/epsonio/bluetooth/DevBt;->mIsFinding:Z

    const/4 p0, 0x0

    return p0

    :catchall_0
    move-exception p0

    .line 70
    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method public static final stop()I
    .locals 6

    .line 250
    sget-boolean v0, Lcom/epson/epsonio/bluetooth/DevBt;->mIsFinding:Z

    if-nez v0, :cond_0

    const/4 v0, 0x6

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 254
    sput-boolean v0, Lcom/epson/epsonio/bluetooth/DevBt;->mIsFinding:Z

    .line 257
    sget-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_2

    .line 258
    sget-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->unregisterReceiverCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    .line 259
    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    .line 261
    sget-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->unregisterReceiverSynchronizer:Ljava/lang/Integer;

    monitor-enter v1

    .line 263
    :try_start_0
    sget-object v2, Lcom/epson/epsonio/bluetooth/DevBt;->unregisterReceiverCallbackHandler:Landroid/os/Handler;

    .line 264
    invoke-virtual {v2, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 265
    sget-object v3, Lcom/epson/epsonio/bluetooth/DevBt;->unregisterReceiverCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :try_start_1
    sget-object v2, Lcom/epson/epsonio/bluetooth/DevBt;->unregisterReceiverSynchronizer:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    :catch_0
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 278
    :cond_1
    sget-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/epson/epsonio/bluetooth/DevBt;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 283
    :cond_2
    :goto_0
    sget-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 284
    sget-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_3
    const/4 v1, 0x0

    .line 287
    sput-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 288
    sput-object v1, Lcom/epson/epsonio/bluetooth/DevBt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return v0
.end method
