.class public Lcom/squareup/comms/common/IoThread;
.super Ljava/lang/Object;
.source "IoThread.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/common/IoThread$IoLooper;
    }
.end annotation


# static fields
.field private static final POST_TIMEOUT_MS:I = 0x1388

.field private static final SHUTDOWN_DURATION_MS:I = 0x2710


# instance fields
.field private volatile closed:Z

.field private final closingLock:Ljava/lang/Object;

.field private final runnableQueue:Lcom/squareup/comms/common/RunnableQueue;

.field private final scheduler:Ljava/util/concurrent/ScheduledExecutorService;

.field private final selector:Ljava/nio/channels/Selector;

.field private final thread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/squareup/comms/common/IoThread;->closingLock:Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/squareup/comms/common/RunnableQueue;

    invoke-direct {v0}, Lcom/squareup/comms/common/RunnableQueue;-><init>()V

    iput-object v0, p0, Lcom/squareup/comms/common/IoThread;->runnableQueue:Lcom/squareup/comms/common/RunnableQueue;

    .line 57
    :try_start_0
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/common/IoThread;->selector:Ljava/nio/channels/Selector;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    new-instance v0, Lcom/squareup/comms/common/NamedThreadFactory;

    const-string v1, "scheduler-%d"

    invoke-direct {v0, v1}, Lcom/squareup/comms/common/NamedThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/common/IoThread;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    const/4 v0, 0x0

    .line 63
    iput-boolean v0, p0, Lcom/squareup/comms/common/IoThread;->closed:Z

    .line 64
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/squareup/comms/common/IoThread$IoLooper;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/squareup/comms/common/IoThread$IoLooper;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/common/IoThread$1;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/squareup/comms/common/IoThread;->thread:Ljava/lang/Thread;

    .line 65
    iget-object v1, p0, Lcom/squareup/comms/common/IoThread;->thread:Ljava/lang/Thread;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Sq-"

    aput-object v4, v3, v0

    const/4 v0, 0x1

    aput-object p1, v3, v0

    const/4 p1, 0x2

    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, p1

    const-string p1, "%sio-%s-%d"

    invoke-static {v2, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/comms/common/IoThread;->thread:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    return-void

    :catch_0
    move-exception p1

    .line 59
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static synthetic access$100(Lcom/squareup/comms/common/IoThread;)Ljava/nio/channels/Selector;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/comms/common/IoThread;->selector:Ljava/nio/channels/Selector;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/comms/common/IoThread;)Z
    .locals 0

    .line 32
    iget-boolean p0, p0, Lcom/squareup/comms/common/IoThread;->closed:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/comms/common/IoThread;)Lcom/squareup/comms/common/RunnableQueue;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/comms/common/IoThread;->runnableQueue:Lcom/squareup/comms/common/RunnableQueue;

    return-object p0
.end method

.method private isIoThread()Z
    .locals 5

    .line 185
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/comms/common/IoThread;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public assertIoThread()V
    .locals 2

    .line 189
    invoke-direct {p0}, Lcom/squareup/comms/common/IoThread;->isIoThread()Z

    move-result v0

    const-string v1, "Must be run on IO thread."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method

.method public close()V
    .locals 5

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->closingLock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    .line 76
    :try_start_1
    iput-boolean v1, p0, Lcom/squareup/comms/common/IoThread;->closed:Z

    .line 77
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    :try_start_2
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 80
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 81
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x2710

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->close()V

    .line 86
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 93
    iget-object v2, p0, Lcom/squareup/comms/common/IoThread;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "IoThread.close finished %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 82
    :cond_0
    :try_start_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Scheduler did not shut down."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catchall_0
    move-exception v1

    .line 77
    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception v0

    .line 91
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    .line 88
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 89
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public synthetic lambda$schedule$0$IoThread(Ljava/lang/Runnable;)V
    .locals 0

    .line 142
    invoke-virtual {p0, p1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public post(Ljava/lang/Runnable;)V
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->runnableQueue:Lcom/squareup/comms/common/RunnableQueue;

    invoke-virtual {v0, p1}, Lcom/squareup/comms/common/RunnableQueue;->enqueue(Ljava/lang/Runnable;)V

    .line 181
    iget-object p1, p0, Lcom/squareup/comms/common/IoThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {p1}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    return-void
.end method

.method public postAndWait(Ljava/lang/Runnable;)V
    .locals 3

    .line 153
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 154
    new-instance v1, Lcom/squareup/comms/common/IoThread$2;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/comms/common/IoThread$2;-><init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/Runnable;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {p0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    .line 162
    invoke-direct {p0}, Lcom/squareup/comms/common/IoThread;->isIoThread()Z

    move-result p1

    if-nez p1, :cond_0

    const-wide/16 v1, 0x1388

    .line 164
    :try_start_0
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, p1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 165
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "IoThread::postAndWait timed out!"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 168
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 169
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :goto_0
    return-void
.end method

.method public register(Ljava/nio/channels/SelectableChannel;ILcom/squareup/comms/common/IoCompletion;)V
    .locals 1

    .line 111
    new-instance v0, Lcom/squareup/comms/common/IoThread$1;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/squareup/comms/common/IoThread$1;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/common/IoCompletion;Ljava/nio/channels/SelectableChannel;I)V

    invoke-virtual {p0, v0}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public schedule(Ljava/lang/Runnable;ILjava/util/concurrent/TimeUnit;)V
    .locals 3

    .line 137
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread;->closingLock:Ljava/lang/Object;

    monitor-enter v0

    .line 138
    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/comms/common/IoThread;->closed:Z

    if-eqz v1, :cond_0

    const-string p1, "IoThread.schedule: ignoring runnable, queue is closed"

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    .line 139
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    monitor-exit v0

    return-void

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/squareup/comms/common/IoThread;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/squareup/comms/common/-$$Lambda$IoThread$PlBl6oDqGi9vXH2ChVKQAevAYvY;

    invoke-direct {v2, p0, p1}, Lcom/squareup/comms/common/-$$Lambda$IoThread$PlBl6oDqGi9vXH2ChVKQAevAYvY;-><init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/Runnable;)V

    int-to-long p1, p2

    invoke-interface {v1, v2, p1, p2, p3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 143
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
