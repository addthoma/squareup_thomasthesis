.class Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;
.super Ljava/lang/Object;
.source "Callbacks.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;->onReceive([BII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;

.field final synthetic val$buffer:[B

.field final synthetic val$count:I

.field final synthetic val$offset:I


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;[BII)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;->this$0:Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;

    iput-object p2, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;->val$buffer:[B

    iput p3, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;->val$offset:I

    iput p4, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;->val$count:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 103
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;->this$0:Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;

    invoke-static {v0}, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;->access$200(Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;)Lcom/squareup/comms/net/ConnectionCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;->val$buffer:[B

    iget v2, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;->val$offset:I

    iget v3, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;->val$count:I

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/comms/net/ConnectionCallback;->onReceive([BII)V

    return-void
.end method
