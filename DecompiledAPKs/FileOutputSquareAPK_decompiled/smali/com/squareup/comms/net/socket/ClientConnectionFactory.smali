.class public final Lcom/squareup/comms/net/socket/ClientConnectionFactory;
.super Ljava/lang/Object;
.source "ClientConnectionFactory.java"

# interfaces
.implements Lcom/squareup/comms/net/ConnectionFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;
    }
.end annotation


# static fields
.field private static final CONNECT_TIMEOUT_MS:I = 0x3e8

.field private static final MIN_RECONNECT_TIMEOUT_MS:I = 0x64


# instance fields
.field private callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

.field private closed:Z

.field private final ioThread:Lcom/squareup/comms/common/IoThread;

.field private final localHost:Ljava/lang/String;

.field private final maxReconnectTimeoutMs:I

.field private final pendingChannels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/nio/channels/SocketChannel;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteHost:Ljava/lang/String;

.field private final remotePort:I

.field private final socketConnectionFailureListener:Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;


# direct methods
.method public constructor <init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .line 53
    new-instance v6, Lcom/squareup/comms/net/socket/SocketConnectionFailureListener$NoSocketConnectionErrorListener;

    invoke-direct {v6}, Lcom/squareup/comms/net/socket/SocketConnectionFailureListener$NoSocketConnectionErrorListener;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;-><init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/String;Ljava/lang/String;IILcom/squareup/comms/net/socket/SocketConnectionFailureListener;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/String;Ljava/lang/String;IILcom/squareup/comms/net/socket/SocketConnectionFailureListener;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->ioThread:Lcom/squareup/comms/common/IoThread;

    .line 76
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->pendingChannels:Ljava/util/List;

    .line 77
    iput-object p2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->localHost:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->remoteHost:Ljava/lang/String;

    .line 79
    iput p4, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->remotePort:I

    .line 80
    iput p5, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->maxReconnectTimeoutMs:I

    .line 81
    iput-object p6, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->socketConnectionFailureListener:Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/util/List;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->pendingChannels:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->localHost:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->remoteHost:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->remotePort:I

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Lcom/squareup/comms/common/IoThread;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->ioThread:Lcom/squareup/comms/common/IoThread;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->socketConnectionFailureListener:Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/comms/net/socket/ClientConnectionFactory;I)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->createConnector(I)V

    return-void
.end method

.method private checkNotClosed()V
    .locals 2

    .line 132
    iget-boolean v0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->closed:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The factory is closed!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private createConnector(I)V
    .locals 3

    .line 120
    iget-boolean v0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->closed:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Ignoring connnection request, factory is closed"

    .line 121
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 125
    :cond_0
    iget v0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->maxReconnectTimeoutMs:I

    mul-int/lit8 v1, p1, 0x64

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 127
    new-instance v1, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;-><init>(Lcom/squareup/comms/net/socket/ClientConnectionFactory;ILcom/squareup/comms/net/ConnectionFactory$Callback;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->ioThread:Lcom/squareup/comms/common/IoThread;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/comms/common/IoThread;->schedule(Ljava/lang/Runnable;ILjava/util/concurrent/TimeUnit;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .line 99
    iget-boolean v0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->closed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 100
    iput-boolean v0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->closed:Z

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->pendingChannels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/channels/SocketChannel;

    .line 105
    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->close()V

    goto :goto_0

    :cond_1
    const-string v0, "Client connection factory shut down"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 108
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 110
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public requestConnection()V
    .locals 3

    .line 92
    invoke-direct {p0}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->checkNotClosed()V

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Client, new connection requested"

    .line 93
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    invoke-direct {p0, v0}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->createConnector(I)V

    return-void
.end method

.method public setCallback(Lcom/squareup/comms/net/ConnectionFactory$Callback;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->checkNotClosed()V

    .line 87
    iput-object p1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    return-void
.end method
