.class Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;
.super Ljava/lang/Object;
.source "ServerConnectionFactory.java"

# interfaces
.implements Lcom/squareup/comms/common/IoCompletion;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->postAccept()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;->this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/io/IOException;)V
    .locals 1

    .line 179
    :try_start_0
    iget-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;->this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    iget-object p1, p1, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {p1}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$300(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/nio/channels/ServerSocketChannel;

    move-result-object p1

    invoke-virtual {p1}, Ljava/nio/channels/ServerSocketChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :catch_0
    iget-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;->this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    iget-object p1, p1, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    iget-object v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;->this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->access$800(Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$400(Lcom/squareup/comms/net/socket/ServerConnectionFactory;I)V

    return-void
.end method

.method public onReady(Ljava/nio/channels/SelectionKey;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 168
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-array v1, v1, [Ljava/lang/Object;

    .line 169
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "Unexpected interest ops: %d"

    .line 168
    invoke-static {v0, p1, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 170
    iget-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;->this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    iget-object p1, p1, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {p1}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$300(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/nio/channels/ServerSocketChannel;

    move-result-object p1

    invoke-virtual {p1}, Ljava/nio/channels/ServerSocketChannel;->accept()Ljava/nio/channels/SocketChannel;

    move-result-object p1

    .line 171
    invoke-virtual {p1, v2}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 172
    iget-object v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;->this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->access$600(Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;)Lcom/squareup/comms/net/ConnectionFactory$Callback;

    move-result-object v0

    new-instance v1, Lcom/squareup/comms/net/socket/SocketConnection;

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;->this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    iget-object v2, v2, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v2}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$500(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Lcom/squareup/comms/common/IoThread;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/comms/net/socket/SocketConnection;-><init>(Lcom/squareup/comms/common/IoThread;Ljava/nio/channels/SocketChannel;)V

    sget-object p1, Lcom/squareup/comms/net/Device;->VOID_DEVICE:Lcom/squareup/comms/net/Device;

    invoke-interface {v0, v1, p1}, Lcom/squareup/comms/net/ConnectionFactory$Callback;->onNewConnection(Lcom/squareup/comms/net/Connection;Lcom/squareup/comms/net/Device;)V

    .line 174
    iget-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;->this$1:Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    invoke-static {p1}, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->access$700(Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;)V

    return-void
.end method
