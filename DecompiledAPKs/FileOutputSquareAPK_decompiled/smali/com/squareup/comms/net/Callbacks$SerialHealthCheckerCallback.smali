.class Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;
.super Ljava/lang/Object;
.source "Callbacks.java"

# interfaces
.implements Lcom/squareup/comms/HealthChecker$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/net/Callbacks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SerialHealthCheckerCallback"
.end annotation


# instance fields
.field private final delegate:Lcom/squareup/comms/HealthChecker$Callback;

.field private final ioThread:Lcom/squareup/comms/common/IoThread;


# direct methods
.method constructor <init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/HealthChecker$Callback;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    .line 53
    iput-object p2, p0, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;->delegate:Lcom/squareup/comms/HealthChecker$Callback;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;)Lcom/squareup/comms/HealthChecker$Callback;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;->delegate:Lcom/squareup/comms/HealthChecker$Callback;

    return-object p0
.end method


# virtual methods
.method public onHealthy()V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback$1;

    invoke-direct {v1, p0}, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback$1;-><init>(Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onUnhealthy()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback$2;

    invoke-direct {v1, p0}, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback$2;-><init>(Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method
