.class public final Lcom/squareup/comms/protos/buyer/OnTender;
.super Lcom/squareup/wire/AndroidMessage;
.source "OnTender.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/OnTender$Builder;,
        Lcom/squareup/comms/protos/buyer/OnTender$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/OnTender;",
        "Lcom/squareup/comms/protos/buyer/OnTender$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnTender.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnTender.kt\ncom/squareup/comms/protos/buyer/OnTender\n*L\n1#1,268:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00192\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0018\u0019BC\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\u0006\u0010\u0008\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJN\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00042\u0008\u0008\u0002\u0010\t\u001a\u00020\u00042\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rJ\u0013\u0010\u0010\u001a\u00020\u00042\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0096\u0002J\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0002H\u0016J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnTender;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/OnTender$Builder;",
        "is_card_present",
        "",
        "prompt_amount",
        "",
        "prompt_offline_mode",
        "prompt_above_credit_card_maximum",
        "prompt_below_credit_card_minimum",
        "magswipe",
        "Lcom/squareup/comms/protos/buyer/MagSwipe;",
        "unknownFields",
        "Lokio/ByteString;",
        "(ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;)V",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/OnTender;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/OnTender;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/OnTender$Companion;


# instance fields
.field public final is_card_present:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field

.field public final magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.MagSwipe#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final prompt_above_credit_card_maximum:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation
.end field

.field public final prompt_amount:J
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field

.field public final prompt_below_credit_card_minimum:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation
.end field

.field public final prompt_offline_mode:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/OnTender$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/OnTender$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnTender;->Companion:Lcom/squareup/comms/protos/buyer/OnTender$Companion;

    .line 199
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnTender$Companion$ADAPTER$1;

    .line 200
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 201
    const-class v2, Lcom/squareup/comms/protos/buyer/OnTender;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/OnTender$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 265
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/OnTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnTender;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;)V
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget-object v0, Lcom/squareup/comms/protos/buyer/OnTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-boolean p1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->is_card_present:Z

    iput-wide p2, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_amount:J

    iput-boolean p4, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_offline_mode:Z

    iput-boolean p5, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_above_credit_card_maximum:Z

    iput-boolean p6, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_below_credit_card_minimum:Z

    iput-object p7, p0, Lcom/squareup/comms/protos/buyer/OnTender;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    return-void
.end method

.method public synthetic constructor <init>(ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p9, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 67
    check-cast v0, Lcom/squareup/comms/protos/buyer/MagSwipe;

    move-object v8, v0

    goto :goto_0

    :cond_0
    move-object/from16 v8, p7

    :goto_0
    and-int/lit8 v0, p9, 0x40

    if-eqz v0, :cond_1

    .line 68
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v9, v0

    goto :goto_1

    :cond_1
    move-object/from16 v9, p8

    :goto_1
    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v9}, Lcom/squareup/comms/protos/buyer/OnTender;-><init>(ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/OnTender;ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/OnTender;
    .locals 9

    move-object v0, p0

    and-int/lit8 v1, p9, 0x1

    if-eqz v1, :cond_0

    .line 121
    iget-boolean v1, v0, Lcom/squareup/comms/protos/buyer/OnTender;->is_card_present:Z

    goto :goto_0

    :cond_0
    move v1, p1

    :goto_0
    and-int/lit8 v2, p9, 0x2

    if-eqz v2, :cond_1

    .line 122
    iget-wide v2, v0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_amount:J

    goto :goto_1

    :cond_1
    move-wide v2, p2

    :goto_1
    and-int/lit8 v4, p9, 0x4

    if-eqz v4, :cond_2

    .line 123
    iget-boolean v4, v0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_offline_mode:Z

    goto :goto_2

    :cond_2
    move v4, p4

    :goto_2
    and-int/lit8 v5, p9, 0x8

    if-eqz v5, :cond_3

    .line 124
    iget-boolean v5, v0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_above_credit_card_maximum:Z

    goto :goto_3

    :cond_3
    move v5, p5

    :goto_3
    and-int/lit8 v6, p9, 0x10

    if-eqz v6, :cond_4

    .line 125
    iget-boolean v6, v0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_below_credit_card_minimum:Z

    goto :goto_4

    :cond_4
    move v6, p6

    :goto_4
    and-int/lit8 v7, p9, 0x20

    if-eqz v7, :cond_5

    .line 126
    iget-object v7, v0, Lcom/squareup/comms/protos/buyer/OnTender;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p7

    :goto_5
    and-int/lit8 v8, p9, 0x40

    if-eqz v8, :cond_6

    .line 127
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTender;->unknownFields()Lokio/ByteString;

    move-result-object v8

    goto :goto_6

    :cond_6
    move-object/from16 v8, p8

    :goto_6
    move p1, v1

    move-wide p2, v2

    move p4, v4

    move p5, v5

    move p6, v6

    move-object/from16 p7, v7

    move-object/from16 p8, v8

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/comms/protos/buyer/OnTender;->copy(ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnTender;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final copy(ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnTender;
    .locals 10

    const-string v0, "unknownFields"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnTender;

    move-object v1, v0

    move v2, p1

    move-wide v3, p2

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/squareup/comms/protos/buyer/OnTender;-><init>(ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 83
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/OnTender;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 84
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/OnTender;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/OnTender;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/OnTender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/OnTender;->is_card_present:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/OnTender;->is_card_present:Z

    if-ne v0, v3, :cond_2

    iget-wide v3, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_amount:J

    iget-wide v5, p1, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_amount:J

    cmp-long v0, v3, v5

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_offline_mode:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_offline_mode:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_above_credit_card_maximum:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_above_credit_card_maximum:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_below_credit_card_minimum:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_below_credit_card_minimum:Z

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/OnTender;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/OnTender;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 95
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->is_card_present:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-wide v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_amount:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_offline_mode:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_above_credit_card_maximum:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_below_credit_card_minimum:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 104
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/OnTender$Builder;
    .locals 3

    .line 71
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/OnTender$Builder;-><init>()V

    .line 72
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->is_card_present:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->is_card_present:Ljava/lang/Boolean;

    .line 73
    iget-wide v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_amount:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_amount:Ljava/lang/Long;

    .line 74
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_offline_mode:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_offline_mode:Ljava/lang/Boolean;

    .line 75
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_above_credit_card_maximum:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_above_credit_card_maximum:Ljava/lang/Boolean;

    .line 76
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_below_credit_card_minimum:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_below_credit_card_minimum:Ljava/lang/Boolean;

    .line 77
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/OnTender;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTender;->newBuilder()Lcom/squareup/comms/protos/buyer/OnTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 111
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is_card_present="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/OnTender;->is_card_present:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prompt_amount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_amount:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 113
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prompt_offline_mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_offline_mode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prompt_above_credit_card_maximum="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_above_credit_card_maximum:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 115
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prompt_below_credit_card_minimum="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/OnTender;->prompt_below_credit_card_minimum:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/OnTender;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "magswipe="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/OnTender;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_0
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "OnTender{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
