.class public final Lcom/squareup/comms/protos/buyer/LogEvents$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LogEvents.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/LogEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/LogEvents;",
        "Lcom/squareup/comms/protos/buyer/LogEvents$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\t\u001a\u00020\u0002H\u0016J\u0014\u0010\u0004\u001a\u00020\u00002\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005J\u0014\u0010\u0007\u001a\u00020\u00002\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005R\u0018\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/LogEvents$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/LogEvents;",
        "()V",
        "oh_snap_log_events",
        "",
        "Lcom/squareup/comms/protos/buyer/OhSnapLogEvent;",
        "timber_log_events",
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public oh_snap_log_events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/OhSnapLogEvent;",
            ">;"
        }
    .end annotation
.end field

.field public timber_log_events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 82
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/buyer/LogEvents$Builder;->timber_log_events:Ljava/util/List;

    .line 85
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/buyer/LogEvents$Builder;->oh_snap_log_events:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/LogEvents;
    .locals 4

    .line 99
    new-instance v0, Lcom/squareup/comms/protos/buyer/LogEvents;

    .line 100
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogEvents$Builder;->timber_log_events:Ljava/util/List;

    .line 101
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/LogEvents$Builder;->oh_snap_log_events:Ljava/util/List;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogEvents$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 99
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/buyer/LogEvents;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogEvents$Builder;->build()Lcom/squareup/comms/protos/buyer/LogEvents;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final oh_snap_log_events(Ljava/util/List;)Lcom/squareup/comms/protos/buyer/LogEvents$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/OhSnapLogEvent;",
            ">;)",
            "Lcom/squareup/comms/protos/buyer/LogEvents$Builder;"
        }
    .end annotation

    const-string v0, "oh_snap_log_events"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 95
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogEvents$Builder;->oh_snap_log_events:Ljava/util/List;

    return-object p0
.end method

.method public final timber_log_events(Ljava/util/List;)Lcom/squareup/comms/protos/buyer/LogEvents$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
            ">;)",
            "Lcom/squareup/comms/protos/buyer/LogEvents$Builder;"
        }
    .end annotation

    const-string v0, "timber_log_events"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 89
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogEvents$Builder;->timber_log_events:Ljava/util/List;

    return-object p0
.end method
