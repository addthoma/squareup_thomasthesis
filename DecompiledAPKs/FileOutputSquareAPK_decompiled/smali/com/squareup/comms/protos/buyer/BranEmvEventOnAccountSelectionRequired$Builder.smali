.class public final Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BranEmvEventOnAccountSelectionRequired.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0014\u0010\u0004\u001a\u00020\u00002\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008J\u0008\u0010\n\u001a\u00020\u0002H\u0016J\u0014\u0010\t\u001a\u00020\u00002\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005R\u0018\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0018\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired;",
        "()V",
        "account_types_swig_values",
        "",
        "",
        "application_id",
        "",
        "language_prefs",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public account_types_swig_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public application_id:Ljava/lang/String;

.field public language_prefs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 98
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->account_types_swig_values:Ljava/util/List;

    .line 101
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->language_prefs:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final account_types_swig_values(Ljava/util/List;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;"
        }
    .end annotation

    const-string v0, "account_types_swig_values"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 108
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->account_types_swig_values:Ljava/util/List;

    return-object p0
.end method

.method public final application_id(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->application_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired;
    .locals 5

    .line 124
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired;

    .line 125
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->account_types_swig_values:Ljava/util/List;

    .line 126
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->language_prefs:Ljava/util/List;

    .line 127
    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->application_id:Ljava/lang/String;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    .line 124
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->build()Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final language_prefs(Ljava/util/List;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;"
        }
    .end annotation

    const-string v0, "language_prefs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 114
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired$Builder;->language_prefs:Ljava/util/List;

    return-object p0
.end method
