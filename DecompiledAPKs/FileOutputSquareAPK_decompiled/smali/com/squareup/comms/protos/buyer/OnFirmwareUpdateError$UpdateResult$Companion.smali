.class public final Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;
.super Ljava/lang/Object;
.source "OnFirmwareUpdateError.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008H\u0007R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;",
        "",
        "()V",
        "ADAPTER",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;",
        "fromValue",
        "value",
        "",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 162
    invoke-direct {p0}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromValue(I)Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/16 v0, 0xff

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    .line 191
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 189
    :pswitch_0
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE_VERSION:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 188
    :pswitch_1
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_ERROR_UNKNOWN:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 187
    :pswitch_2
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 186
    :pswitch_3
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 185
    :pswitch_4
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 184
    :pswitch_5
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 183
    :pswitch_6
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_BAD_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 182
    :pswitch_7
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_BAD_ARGUMENT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 181
    :pswitch_8
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_FLASH_FAILURE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 180
    :pswitch_9
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_DECRYPTION_FAILURE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 179
    :pswitch_a
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 178
    :pswitch_b
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 177
    :pswitch_c
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_NOT_INITIALIZED:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 176
    :pswitch_d
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 175
    :pswitch_e
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 174
    :pswitch_f
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 173
    :pswitch_10
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SEND_DATA:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 172
    :pswitch_11
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SUCCESS:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    goto :goto_0

    .line 190
    :cond_0
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_NONE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
