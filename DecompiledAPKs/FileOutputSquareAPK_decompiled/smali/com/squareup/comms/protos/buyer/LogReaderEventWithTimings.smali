.class public final Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;
.super Lcom/squareup/wire/AndroidMessage;
.source "LogReaderEventWithTimings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;,
        Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;,
        Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLogReaderEventWithTimings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LogReaderEventWithTimings.kt\ncom/squareup/comms/protos/buyer/LogReaderEventWithTimings\n*L\n1#1,309:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u0000 \u00192\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0017\u0018\u0019B/\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ4\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bJ\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0096\u0002J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u0002H\u0016J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;",
        "event_name",
        "Lcom/squareup/comms/protos/buyer/BuyerEventName;",
        "card_reader_info",
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
        "payment_timings",
        "",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;",
        "unknownFields",
        "Lokio/ByteString;",
        "(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "BuyerPaymentTiming",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion;


# instance fields
.field public final card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.BuyerCardReaderInfo#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field

.field public final event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.BuyerEventName#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field

.field public final payment_timings:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.LogReaderEventWithTimings$BuyerPaymentTiming#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->Companion:Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion;

    .line 134
    new-instance v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1;

    .line 136
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 137
    const-class v2, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 181
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/comms/protos/buyer/BuyerEventName;",
            "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    const-string v0, "event_name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "card_reader_info"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payment_timings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    iput-object p2, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    iput-object p3, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    .line 46
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p3

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    .line 47
    sget-object p4, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;-><init>(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 90
    iget-object p1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 91
    iget-object p2, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 92
    iget-object p3, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 93
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->unknownFields()Lokio/ByteString;

    move-result-object p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->copy(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/comms/protos/buyer/BuyerEventName;",
            "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;",
            ">;",
            "Lokio/ByteString;",
            ")",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;"
        }
    .end annotation

    const-string v0, "event_name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "card_reader_info"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payment_timings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    new-instance v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;-><init>(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 60
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 61
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 69
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 72
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    invoke-virtual {v1}, Lcom/squareup/comms/protos/buyer/BuyerEventName;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 73
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    invoke-virtual {v1}, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 74
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;
    .locals 2

    .line 51
    new-instance v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;-><init>()V

    .line 52
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    .line 53
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 54
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->payment_timings:Ljava/util/List;

    .line 55
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->newBuilder()Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 82
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event_name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "card_reader_info="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "payment_timings="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_0
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "LogReaderEventWithTimings{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    .line 86
    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    .line 85
    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
