.class public final Lcom/squareup/comms/protos/buyer/OnReceipt;
.super Lcom/squareup/wire/AndroidMessage;
.source "OnReceipt.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;,
        Lcom/squareup/comms/protos/buyer/OnReceipt$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/OnReceipt;",
        "Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnReceipt.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnReceipt.kt\ncom/squareup/comms/protos/buyer/OnReceipt\n*L\n1#1,199:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00172\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0016\u0017B/\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ7\u0010\u000c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\rJ\u0013\u0010\u000e\u001a\u00020\u00042\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0002J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0002H\u0016J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnReceipt;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;",
        "skip_receipt_screen",
        "",
        "capture_args",
        "Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;",
        "automatically_print_receipt_to_sign_or_tip",
        "unknownFields",
        "Lokio/ByteString;",
        "(ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;)V",
        "Ljava/lang/Boolean;",
        "copy",
        "(ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnReceipt;",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/OnReceipt;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/OnReceipt;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/OnReceipt$Companion;


# instance fields
.field public final automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.EmvCaptureArgs#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final skip_receipt_screen:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/OnReceipt$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/OnReceipt$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnReceipt;->Companion:Lcom/squareup/comms/protos/buyer/OnReceipt$Companion;

    .line 151
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnReceipt$Companion$ADAPTER$1;

    .line 152
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 153
    const-class v2, Lcom/squareup/comms/protos/buyer/OnReceipt;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/OnReceipt$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnReceipt;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 196
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/OnReceipt;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnReceipt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    sget-object v0, Lcom/squareup/comms/protos/buyer/OnReceipt;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-boolean p1, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->skip_receipt_screen:Z

    iput-object p2, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    iput-object p3, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 44
    move-object p2, v0

    check-cast p2, Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 53
    move-object p3, v0

    check-cast p3, Ljava/lang/Boolean;

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    .line 54
    sget-object p4, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/OnReceipt;-><init>(ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/OnReceipt;ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/OnReceipt;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 97
    iget-boolean p1, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->skip_receipt_screen:Z

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 98
    iget-object p2, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 100
    iget-object p3, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 101
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnReceipt;->unknownFields()Lokio/ByteString;

    move-result-object p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/OnReceipt;->copy(ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnReceipt;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnReceipt;
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnReceipt;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/OnReceipt;-><init>(ZLcom/squareup/comms/protos/buyer/EmvCaptureArgs;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 66
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/OnReceipt;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 67
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/OnReceipt;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 72
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnReceipt;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/OnReceipt;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/OnReceipt;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->skip_receipt_screen:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/OnReceipt;->skip_receipt_screen:Z

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/OnReceipt;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/OnReceipt;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 76
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnReceipt;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->skip_receipt_screen:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 81
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 82
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;
    .locals 2

    .line 57
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;-><init>()V

    .line 58
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->skip_receipt_screen:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    .line 59
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    .line 60
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnReceipt;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnReceipt;->newBuilder()Lcom/squareup/comms/protos/buyer/OnReceipt$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 89
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "skip_receipt_screen="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->skip_receipt_screen:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "capture_args="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_0
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 92
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "automatically_print_receipt_to_sign_or_tip="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/OnReceipt;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_1
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "OnReceipt{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
