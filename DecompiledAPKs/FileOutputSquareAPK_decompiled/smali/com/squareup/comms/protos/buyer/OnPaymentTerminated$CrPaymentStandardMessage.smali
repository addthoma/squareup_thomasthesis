.class public final enum Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;
.super Ljava/lang/Enum;
.source "OnPaymentTerminated.kt"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/OnPaymentTerminated;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CrPaymentStandardMessage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\'\u0008\u0086\u0001\u0018\u0000 *2\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001*B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001ej\u0002\u0008\u001fj\u0002\u0008 j\u0002\u0008!j\u0002\u0008\"j\u0002\u0008#j\u0002\u0008$j\u0002\u0008%j\u0002\u0008&j\u0002\u0008\'j\u0002\u0008(j\u0002\u0008)\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;",
        "",
        "Lcom/squareup/wire/WireEnum;",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "CR_PAYMENT_STD_MSG_NONE",
        "CR_PAYMENT_STD_MSG_AMOUNT",
        "CR_PAYMENT_STD_MSG_AMOUNT_OK",
        "CR_PAYMENT_STD_MSG_APPROVED",
        "CR_PAYMENT_STD_MSG_CALL_YOUR_BANK",
        "CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER",
        "CR_PAYMENT_STD_MSG_CARD_ERROR",
        "CR_PAYMENT_STD_MSG_DECLINED",
        "CR_PAYMENT_STD_MSG_ENTER_AMOUNT",
        "CR_PAYMENT_STD_MSG_ENTER_PIN",
        "CR_PAYMENT_STD_MSG_INCORRECT_PIN",
        "CR_PAYMENT_STD_MSG_INSERT_CARD",
        "CR_PAYMENT_STD_MSG_NOT_ACCEPTED",
        "CR_PAYMENT_STD_MSG_PIN_OK",
        "CR_PAYMENT_STD_MSG_PLEASE_WAIT",
        "CR_PAYMENT_STD_MSG_PROCESSING_ERROR",
        "CR_PAYMENT_STD_MSG_REMOVE_CARD",
        "CR_PAYMENT_STD_MSG_USE_CHIP_READER",
        "CR_PAYMENT_STD_MSG_USE_MAG_STRIP",
        "CR_PAYMENT_STD_MSG_TRY_AGAIN",
        "CR_PAYMENT_STD_MSG_WELCOME",
        "CR_PAYMENT_STD_MSG_PRESENT_CARD",
        "CR_PAYMENT_STD_MSG_PROCESSING",
        "CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD",
        "CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD",
        "CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD",
        "CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN",
        "CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT",
        "CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD",
        "CR_PAYMENT_STD_MSG_PLS_INSERT_CARD",
        "CR_PAYMENT_STD_MSG_NO_MSG",
        "CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS",
        "CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN",
        "CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CR_PAYMENT_STD_MSG_AMOUNT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_AMOUNT_OK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_APPROVED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_CALL_YOUR_BANK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_CARD_ERROR:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_ENTER_AMOUNT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_ENTER_PIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_INCORRECT_PIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_INSERT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_NOT_ACCEPTED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_NO_MSG:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PIN_OK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PLEASE_WAIT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PLS_INSERT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PRESENT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PROCESSING:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PROCESSING_ERROR:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_REMOVE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_TRY_AGAIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_USE_CHIP_READER:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_WELCOME:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

.field public static final Companion:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x22

    new-array v0, v0, [Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/4 v2, 0x0

    const-string v3, "CR_PAYMENT_STD_MSG_NONE"

    .line 130
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/4 v2, 0x1

    const-string v3, "CR_PAYMENT_STD_MSG_AMOUNT"

    .line 132
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/4 v2, 0x2

    const-string v3, "CR_PAYMENT_STD_MSG_AMOUNT_OK"

    .line 134
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT_OK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/4 v2, 0x3

    const-string v3, "CR_PAYMENT_STD_MSG_APPROVED"

    .line 136
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/4 v2, 0x4

    const-string v3, "CR_PAYMENT_STD_MSG_CALL_YOUR_BANK"

    .line 138
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CALL_YOUR_BANK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/4 v2, 0x5

    const-string v3, "CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER"

    .line 140
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/4 v2, 0x6

    const-string v3, "CR_PAYMENT_STD_MSG_CARD_ERROR"

    .line 142
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_ERROR:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/4 v2, 0x7

    const-string v3, "CR_PAYMENT_STD_MSG_DECLINED"

    .line 144
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x8

    const-string v3, "CR_PAYMENT_STD_MSG_ENTER_AMOUNT"

    .line 146
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_AMOUNT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x9

    const-string v3, "CR_PAYMENT_STD_MSG_ENTER_PIN"

    .line 148
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_PIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0xa

    const-string v3, "CR_PAYMENT_STD_MSG_INCORRECT_PIN"

    .line 150
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INCORRECT_PIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0xb

    const-string v3, "CR_PAYMENT_STD_MSG_INSERT_CARD"

    .line 152
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0xc

    const-string v3, "CR_PAYMENT_STD_MSG_NOT_ACCEPTED"

    .line 154
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NOT_ACCEPTED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0xd

    const-string v3, "CR_PAYMENT_STD_MSG_PIN_OK"

    .line 156
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PIN_OK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0xe

    const-string v3, "CR_PAYMENT_STD_MSG_PLEASE_WAIT"

    .line 158
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLEASE_WAIT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_PROCESSING_ERROR"

    const/16 v3, 0xf

    const/16 v4, 0xf

    .line 160
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING_ERROR:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_REMOVE_CARD"

    const/16 v3, 0x10

    const/16 v4, 0x10

    .line 162
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_REMOVE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_USE_CHIP_READER"

    const/16 v3, 0x11

    const/16 v4, 0x11

    .line 164
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_CHIP_READER:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_USE_MAG_STRIP"

    const/16 v3, 0x12

    const/16 v4, 0x12

    .line 166
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_TRY_AGAIN"

    const/16 v3, 0x13

    const/16 v4, 0x13

    .line 168
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TRY_AGAIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_WELCOME"

    const/16 v3, 0x14

    const/16 v4, 0x14

    .line 170
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_WELCOME:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_PRESENT_CARD"

    const/16 v3, 0x15

    const/16 v4, 0x15

    .line 172
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_PROCESSING"

    const/16 v3, 0x16

    const/16 v4, 0x16

    .line 174
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD"

    const/16 v3, 0x17

    const/16 v4, 0x17

    .line 176
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD"

    const/16 v3, 0x18

    const/16 v4, 0x18

    .line 178
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD"

    const/16 v3, 0x19

    const/16 v4, 0x19

    .line 180
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN"

    const/16 v3, 0x1a

    const/16 v4, 0x1a

    .line 182
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT"

    const/16 v3, 0x1b

    const/16 v4, 0x1b

    .line 184
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD"

    const/16 v3, 0x1c

    const/16 v4, 0x1c

    .line 186
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_PLS_INSERT_CARD"

    const/16 v3, 0x1d

    const/16 v4, 0x1d

    .line 188
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_NO_MSG"

    const/16 v3, 0x1e

    const/16 v4, 0x1e

    .line 190
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NO_MSG:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS"

    const/16 v3, 0x1f

    const/16 v4, 0x20

    .line 195
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN"

    const/16 v3, 0x20

    const/16 v4, 0x21

    .line 197
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const-string v2, "CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY"

    const/16 v3, 0x21

    const/16 v4, 0x22

    .line 199
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->$VALUES:[Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    new-instance v0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->Companion:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;

    .line 203
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion$ADAPTER$1;

    .line 205
    const-class v1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion$ADAPTER$1;-><init>(Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 127
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->value:I

    return-void
.end method

.method public static final fromValue(I)Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->Companion:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;->fromValue(I)Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;
    .locals 1

    const-class v0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    return-object p0
.end method

.method public static values()[Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;
    .locals 1

    sget-object v0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->$VALUES:[Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    invoke-virtual {v0}, [Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 128
    iget v0, p0, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->value:I

    return v0
.end method
