.class public final Lcom/squareup/comms/protos/common/Card$Brand$Companion;
.super Ljava/lang/Object;
.source "Card.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/common/Card$Brand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008H\u0007R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/common/Card$Brand$Companion;",
        "",
        "()V",
        "ADAPTER",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/common/Card$Brand;",
        "fromValue",
        "value",
        "",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 506
    invoke-direct {p0}, Lcom/squareup/comms/protos/common/Card$Brand$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromValue(I)Lcom/squareup/comms/protos/common/Card$Brand;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    packed-switch p1, :pswitch_data_0

    .line 531
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 530
    :pswitch_0
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->CASH_APP:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 529
    :pswitch_1
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->ALIPAY:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 528
    :pswitch_2
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->FELICA:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 527
    :pswitch_3
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->EFTPOS:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 526
    :pswitch_4
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 525
    :pswitch_5
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->INTERAC:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 524
    :pswitch_6
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 523
    :pswitch_7
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->UNION_PAY:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 522
    :pswitch_8
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->JCB:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 521
    :pswitch_9
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 520
    :pswitch_a
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->DISCOVER:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 519
    :pswitch_b
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 518
    :pswitch_c
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->MASTER_CARD:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 517
    :pswitch_d
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->VISA:Lcom/squareup/comms/protos/common/Card$Brand;

    goto :goto_0

    .line 516
    :pswitch_e
    sget-object p1, Lcom/squareup/comms/protos/common/Card$Brand;->UNKNOWN:Lcom/squareup/comms/protos/common/Card$Brand;

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
