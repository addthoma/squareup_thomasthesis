.class final Lcom/squareup/orders/SearchOrdersDateTimeFilter$ProtoAdapter_SearchOrdersDateTimeFilter;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SearchOrdersDateTimeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/SearchOrdersDateTimeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SearchOrdersDateTimeFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/SearchOrdersDateTimeFilter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 196
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/SearchOrdersDateTimeFilter;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 217
    new-instance v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;-><init>()V

    .line 218
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 219
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 225
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 223
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->closed_at(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    goto :goto_0

    .line 222
    :cond_1
    sget-object v3, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->updated_at(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    goto :goto_0

    .line 221
    :cond_2
    sget-object v3, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->created_at(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    goto :goto_0

    .line 229
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 230
    invoke-virtual {v0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 194
    invoke-virtual {p0, p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$ProtoAdapter_SearchOrdersDateTimeFilter;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/SearchOrdersDateTimeFilter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 209
    sget-object v0, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 210
    sget-object v0, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 211
    sget-object v0, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 212
    invoke-virtual {p2}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 194
    check-cast p2, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$ProtoAdapter_SearchOrdersDateTimeFilter;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/SearchOrdersDateTimeFilter;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/SearchOrdersDateTimeFilter;)I
    .locals 4

    .line 201
    sget-object v0, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    const/4 v3, 0x2

    .line 202
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    const/4 v3, 0x3

    .line 203
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$ProtoAdapter_SearchOrdersDateTimeFilter;->encodedSize(Lcom/squareup/orders/SearchOrdersDateTimeFilter;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/SearchOrdersDateTimeFilter;)Lcom/squareup/orders/SearchOrdersDateTimeFilter;
    .locals 2

    .line 235
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->newBuilder()Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    move-result-object p1

    .line 236
    iget-object v0, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/TimeRange;

    iput-object v0, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 237
    :cond_0
    iget-object v0, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/TimeRange;

    iput-object v0, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 238
    :cond_1
    iget-object v0, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/connect/v2/common/TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/TimeRange;

    iput-object v0, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 239
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 240
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$ProtoAdapter_SearchOrdersDateTimeFilter;->redact(Lcom/squareup/orders/SearchOrdersDateTimeFilter;)Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    move-result-object p1

    return-object p1
.end method
