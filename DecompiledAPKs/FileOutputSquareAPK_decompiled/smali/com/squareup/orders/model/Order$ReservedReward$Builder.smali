.class public final Lcom/squareup/orders/model/Order$ReservedReward$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReservedReward;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$ReservedReward;",
        "Lcom/squareup/orders/model/Order$ReservedReward$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public pricing_rule_id:Ljava/lang/String;

.field public reward_tier_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15549
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$ReservedReward;
    .locals 5

    .line 15578
    new-instance v0, Lcom/squareup/orders/model/Order$ReservedReward;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->reward_tier_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->pricing_rule_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/orders/model/Order$ReservedReward;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 15542
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->build()Lcom/squareup/orders/model/Order$ReservedReward;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReservedReward$Builder;
    .locals 0

    .line 15556
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public pricing_rule_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReservedReward$Builder;
    .locals 0

    .line 15572
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->pricing_rule_id:Ljava/lang/String;

    return-object p0
.end method

.method public reward_tier_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReservedReward$Builder;
    .locals 0

    .line 15564
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->reward_tier_id:Ljava/lang/String;

    return-object p0
.end method
