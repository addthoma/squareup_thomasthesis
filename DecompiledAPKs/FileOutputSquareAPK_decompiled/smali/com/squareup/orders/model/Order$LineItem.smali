.class public final Lcom/squareup/orders/model/Order$LineItem;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LineItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$LineItem$ProtoAdapter_LineItem;,
        Lcom/squareup/orders/model/Order$LineItem$AppliedTax;,
        Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;,
        Lcom/squareup/orders/model/Order$LineItem$Tax;,
        Lcom/squareup/orders/model/Order$LineItem$Modifier;,
        Lcom/squareup/orders/model/Order$LineItem$Discount;,
        Lcom/squareup/orders/model/Order$LineItem$ItemType;,
        Lcom/squareup/orders/model/Order$LineItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/orders/model/Order$LineItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_CATEGORY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_ITEM_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_ITEM_VARIATION_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_CATEGORY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ITEM_TYPE:Lcom/squareup/orders/model/Order$LineItem$ItemType;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = ""

.field public static final DEFAULT_SKU:Ljava/lang/String; = ""

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_VARIATION_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final applied_discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$AppliedDiscount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x16
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public final applied_taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$AppliedTax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x15
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;"
        }
    .end annotation
.end field

.field public final base_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final catalog_category_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final catalog_item_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x17
    .end annotation
.end field

.field public final catalog_item_variation_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2710
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final category_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x19
    .end annotation
.end field

.field public final discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Discount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public final gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$ItemType#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final metadata:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final modifiers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Modifier#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$QuantityUnit#ADAPTER"
        tag = 0x1d
    .end annotation
.end field

.field public final sku:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1b
    .end annotation
.end field

.field public final taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public final total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final variation_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x14
    .end annotation
.end field

.field public final was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x2328
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 2168
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$ProtoAdapter_LineItem;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$ProtoAdapter_LineItem;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 2182
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    .line 2190
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$ItemType;->ITEM:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem;->DEFAULT_ITEM_TYPE:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    const/4 v0, 0x0

    .line 2196
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem;->DEFAULT_CATALOG_ITEM_VARIATION_COUNT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$LineItem$Builder;Lokio/ByteString;)V
    .locals 1

    .line 2582
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2583
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->uid:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    .line 2584
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    .line 2585
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    .line 2586
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 2587
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    .line 2588
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_object_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    .line 2589
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_object_version:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_version:Ljava/lang/Long;

    .line 2590
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->variation_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    .line 2591
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_item_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_id:Ljava/lang/String;

    .line 2592
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_category_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    .line 2593
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    .line 2594
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->sku:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->sku:Ljava/lang/String;

    .line 2595
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->category_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->category_name:Ljava/lang/String;

    .line 2596
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->metadata:Ljava/util/Map;

    const-string v0, "metadata"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->metadata:Ljava/util/Map;

    .line 2597
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->modifiers:Ljava/util/List;

    const-string v0, "modifiers"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    .line 2598
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->taxes:Ljava/util/List;

    const-string v0, "taxes"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->taxes:Ljava/util/List;

    .line 2599
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->discounts:Ljava/util/List;

    const-string v0, "discounts"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->discounts:Ljava/util/List;

    .line 2600
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->applied_taxes:Ljava/util/List;

    const-string v0, "applied_taxes"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_taxes:Ljava/util/List;

    .line 2601
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->applied_discounts:Ljava/util/List;

    const-string v0, "applied_discounts"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_discounts:Ljava/util/List;

    .line 2602
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2603
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2604
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2605
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2606
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2607
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2608
    iget-object p2, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2609
    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_item_variation_count:Ljava/lang/Integer;

    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2649
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$LineItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2650
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$LineItem;

    .line 2651
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    .line 2652
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    .line 2653
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    .line 2654
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 2655
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    .line 2656
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    .line 2657
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_version:Ljava/lang/Long;

    .line 2658
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    .line 2659
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_id:Ljava/lang/String;

    .line 2660
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    .line 2661
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    .line 2662
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->sku:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->sku:Ljava/lang/String;

    .line 2663
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->category_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->category_name:Ljava/lang/String;

    .line 2664
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->metadata:Ljava/util/Map;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->metadata:Ljava/util/Map;

    .line 2665
    invoke-interface {v1, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    .line 2666
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->taxes:Ljava/util/List;

    .line 2667
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->discounts:Ljava/util/List;

    .line 2668
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->applied_taxes:Ljava/util/List;

    .line 2669
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->applied_discounts:Ljava/util/List;

    .line 2670
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2671
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2672
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2673
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2674
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2675
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2676
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2677
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    .line 2678
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2683
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_15

    .line 2685
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2686
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2687
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2688
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2689
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$QuantityUnit;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2690
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2691
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2692
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2693
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2694
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_id:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2695
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2696
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$ItemType;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2697
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->sku:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2698
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->category_name:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2699
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2700
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2701
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2702
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2703
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2704
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2705
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2706
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2707
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2708
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2709
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2710
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2711
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2712
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_14
    add-int/2addr v0, v2

    .line 2713
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_15
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 2

    .line 2614
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Builder;-><init>()V

    .line 2615
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->uid:Ljava/lang/String;

    .line 2616
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->name:Ljava/lang/String;

    .line 2617
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity:Ljava/lang/String;

    .line 2618
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 2619
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->note:Ljava/lang/String;

    .line 2620
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_object_id:Ljava/lang/String;

    .line 2621
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 2622
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->variation_name:Ljava/lang/String;

    .line 2623
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_item_id:Ljava/lang/String;

    .line 2624
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_category_id:Ljava/lang/String;

    .line 2625
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    .line 2626
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->sku:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->sku:Ljava/lang/String;

    .line 2627
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->category_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->category_name:Ljava/lang/String;

    .line 2628
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->metadata:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->metadata:Ljava/util/Map;

    .line 2629
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->modifiers:Ljava/util/List;

    .line 2630
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->taxes:Ljava/util/List;

    .line 2631
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->discounts:Ljava/util/List;

    .line 2632
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->applied_taxes:Ljava/util/List;

    .line 2633
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->applied_discounts:Ljava/util/List;

    .line 2634
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2635
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2636
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2637
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2638
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2639
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2640
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 2641
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_item_variation_count:Ljava/lang/Integer;

    .line 2642
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$LineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2167
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem;->newBuilder()Lcom/squareup/orders/model/Order$LineItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2720
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2721
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2722
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2723
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2724
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v1, :cond_3

    const-string v1, ", quantity_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2725
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2726
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2727
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2728
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", variation_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2729
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_id:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", catalog_item_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2730
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", catalog_category_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_category_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2731
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    if-eqz v1, :cond_a

    const-string v1, ", item_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2732
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->sku:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", sku="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->sku:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2733
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->category_name:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", category_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->category_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2734
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", metadata=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2735
    :cond_d
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, ", modifiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2736
    :cond_e
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, ", taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2737
    :cond_f
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_10

    const-string v1, ", discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2738
    :cond_10
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, ", applied_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2739
    :cond_11
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, ", applied_discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->applied_discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2740
    :cond_12
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_13

    const-string v1, ", base_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2741
    :cond_13
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_14

    const-string v1, ", variation_total_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2742
    :cond_14
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_15

    const-string v1, ", gross_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2743
    :cond_15
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_16

    const-string v1, ", total_tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2744
    :cond_16
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_17

    const-string v1, ", total_discount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2745
    :cond_17
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_18

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2746
    :cond_18
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_19

    const-string v1, ", was_multiple_quantity_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2747
    :cond_19
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    if-eqz v1, :cond_1a

    const-string v1, ", catalog_item_variation_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LineItem{"

    .line 2748
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
