.class final Lcom/squareup/orders/model/Order$ProtoAdapter_Order;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Order"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order;",
        ">;"
    }
.end annotation


# instance fields
.field private final metadata:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final old_metadata_map:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 15636
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    .line 15631
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->newMapAdapter(Lcom/squareup/wire/ProtoAdapter;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->metadata:Lcom/squareup/wire/ProtoAdapter;

    .line 15633
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->newMapAdapter(Lcom/squareup/wire/ProtoAdapter;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->old_metadata_map:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15741
    new-instance v0, Lcom/squareup/orders/model/Order$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Builder;-><init>()V

    .line 15742
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 15743
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/16 v4, 0x2328

    if-eq v3, v4, :cond_1

    const/16 v4, 0x2710

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    packed-switch v3, :pswitch_data_2

    packed-switch v3, :pswitch_data_3

    packed-switch v3, :pswitch_data_4

    .line 15805
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 15794
    :pswitch_0
    sget-object v3, Lcom/squareup/orders/model/Order$PricingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$PricingOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->pricing_options(Lcom/squareup/orders/model/Order$PricingOptions;)Lcom/squareup/orders/model/Order$Builder;

    goto :goto_0

    .line 15793
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->reserved_rewards:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReservedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15792
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->reserved_discount_codes:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15791
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->vaulted_data(Lcom/squareup/protos/common/privacyvault/VaultedData;)Lcom/squareup/orders/model/Order$Builder;

    goto :goto_0

    .line 15790
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->refund_groups:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$RefundGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15789
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->payment_groups:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$PaymentGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15788
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->customer_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto :goto_0

    .line 15787
    :pswitch_7
    sget-object v3, Lcom/squareup/orders/model/Order$Source;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$Source;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->source(Lcom/squareup/orders/model/Order$Source;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15786
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->creator_app_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15785
    :pswitch_9
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->old_metadata_map:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->old_metadata_map:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto/16 :goto_0

    .line 15784
    :pswitch_a
    sget-object v3, Lcom/squareup/orders/model/Order$RoundingAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->rounding_adjustment(Lcom/squareup/orders/model/Order$RoundingAdjustment;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15783
    :pswitch_b
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->refunds:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15782
    :pswitch_c
    sget-object v3, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->net_amounts(Lcom/squareup/orders/model/Order$MoneyAmounts;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15781
    :pswitch_d
    sget-object v3, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->return_amounts(Lcom/squareup/orders/model/Order$MoneyAmounts;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15780
    :pswitch_e
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->returns:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$Return;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15779
    :pswitch_f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->short_reference_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15778
    :pswitch_10
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->tenders_finalized(Ljava/lang/Boolean;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15777
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->total_service_charge_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15776
    :pswitch_12
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->service_charges:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15775
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15774
    :pswitch_14
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->tenders:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15773
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->closed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15772
    :pswitch_16
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->substatus(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15771
    :pswitch_17
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15770
    :pswitch_18
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->total_tip_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15769
    :pswitch_19
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->total_discount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15768
    :pswitch_1a
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->total_tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15767
    :pswitch_1b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->reference_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15766
    :pswitch_1c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->workflow_version(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15765
    :pswitch_1d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->workflow(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15764
    :pswitch_1e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15758
    :pswitch_1f
    :try_start_0
    sget-object v4, Lcom/squareup/orders/model/Order$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$State;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/model/Order$Builder;->state(Lcom/squareup/orders/model/Order$State;)Lcom/squareup/orders/model/Order$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 15760
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/model/Order$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 15755
    :pswitch_20
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->updated_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15754
    :pswitch_21
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->created_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15753
    :pswitch_22
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->metadata:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->metadata:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto/16 :goto_0

    .line 15752
    :pswitch_23
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->fulfillments:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$Fulfillment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15751
    :pswitch_24
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->tips:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$Tip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15750
    :pswitch_25
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->discounts:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$LineItem$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15749
    :pswitch_26
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->taxes:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$LineItem$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15748
    :pswitch_27
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Builder;->line_items:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$LineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15803
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/orders/OrderClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/OrderClientDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->ext_order_client_details(Lcom/squareup/protos/client/orders/OrderClientDetails;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15797
    :cond_1
    :try_start_1
    sget-object v4, Lcom/squareup/orders/model/Order$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$State;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/model/Order$Builder;->was_status(Lcom/squareup/orders/model/Order$State;)Lcom/squareup/orders/model/Order$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 15799
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/model/Order$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 15747
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->location_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15746
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15745
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Builder;->id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    goto/16 :goto_0

    .line 15809
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 15810
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$Builder;->build()Lcom/squareup/orders/model/Order;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xf
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1f
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x32
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15630
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15691
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15692
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15693
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15694
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    const/16 v2, 0x2c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15695
    sget-object v0, Lcom/squareup/orders/model/Order$Source;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    const/16 v2, 0x2d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15696
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    const/16 v2, 0x2e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15697
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    const/16 v2, 0x21

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15698
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15699
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15700
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15701
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15702
    sget-object v0, Lcom/squareup/orders/model/Order$ServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    const/16 v2, 0x22

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15703
    sget-object v0, Lcom/squareup/orders/model/Order$Tip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15704
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15705
    sget-object v0, Lcom/squareup/orders/model/Order$Return;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    const/16 v2, 0x26

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15706
    sget-object v0, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    const/16 v2, 0x27

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15707
    sget-object v0, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    const/16 v2, 0x28

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15708
    sget-object v0, Lcom/squareup/orders/model/Order$RoundingAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    const/16 v2, 0x2a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15709
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    const/16 v2, 0x24

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15710
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    const/16 v2, 0x20

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15711
    sget-object v0, Lcom/squareup/orders/model/Order$PaymentGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    const/16 v2, 0x2f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15712
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    const/16 v2, 0x29

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15713
    sget-object v0, Lcom/squareup/orders/model/Order$RefundGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    const/16 v2, 0x30

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15714
    iget-object v0, p0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->metadata:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->metadata:Ljava/util/Map;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15715
    iget-object v0, p0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->old_metadata_map:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->old_metadata_map:Ljava/util/Map;

    const/16 v2, 0x2b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15716
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15717
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15718
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    const/16 v2, 0x1f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15719
    sget-object v0, Lcom/squareup/orders/model/Order$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15720
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    const/16 v2, 0x1d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15721
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15722
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15723
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15724
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15725
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15726
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15727
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15728
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x23

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15729
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    const/16 v2, 0x25

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15730
    sget-object v0, Lcom/squareup/orders/model/Order$PricingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    const/16 v2, 0x35

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15731
    sget-object v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    const/16 v2, 0x33

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15732
    sget-object v0, Lcom/squareup/orders/model/Order$ReservedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    const/16 v2, 0x34

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15733
    sget-object v0, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    const/16 v2, 0x32

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15734
    sget-object v0, Lcom/squareup/orders/model/Order$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    const/16 v2, 0x2328

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15735
    sget-object v0, Lcom/squareup/protos/client/orders/OrderClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    const/16 v2, 0x2710

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15736
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15630
    check-cast p2, Lcom/squareup/orders/model/Order;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order;)I
    .locals 4

    .line 15641
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 15642
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    const/16 v3, 0x16

    .line 15643
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    const/16 v3, 0x2c

    .line 15644
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Source;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    const/16 v3, 0x2d

    .line 15645
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    const/16 v3, 0x2e

    .line 15646
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    const/16 v3, 0x21

    .line 15647
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 15648
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15649
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15650
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15651
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15652
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    const/16 v3, 0x22

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Tip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15653
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    const/16 v3, 0x9

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15654
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    const/16 v3, 0xa

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Return;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15655
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    const/16 v3, 0x26

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    const/16 v3, 0x27

    .line 15656
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    const/16 v3, 0x28

    .line 15657
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$RoundingAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    const/16 v3, 0x2a

    .line 15658
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    const/16 v3, 0x24

    .line 15659
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15660
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    const/16 v3, 0x20

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$PaymentGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15661
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    const/16 v3, 0x2f

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15662
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    const/16 v3, 0x29

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$RefundGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15663
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    const/16 v3, 0x30

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->metadata:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->metadata:Ljava/util/Map;

    const/16 v3, 0xf

    .line 15664
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->old_metadata_map:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->old_metadata_map:Ljava/util/Map;

    const/16 v3, 0x2b

    .line 15665
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    const/16 v3, 0x10

    .line 15666
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    const/16 v3, 0x11

    .line 15667
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    const/16 v3, 0x1f

    .line 15668
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    const/16 v3, 0x12

    .line 15669
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    const/16 v3, 0x1d

    .line 15670
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    const/16 v3, 0x13

    .line 15671
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    const/16 v3, 0x14

    .line 15672
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    const/16 v3, 0x15

    .line 15673
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x1c

    .line 15674
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x17

    .line 15675
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x1a

    .line 15676
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x1b

    .line 15677
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x23

    .line 15678
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    const/16 v3, 0x25

    .line 15679
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$PricingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    const/16 v3, 0x35

    .line 15680
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15681
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    const/16 v3, 0x33

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReservedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15682
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    const/16 v3, 0x34

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    const/16 v3, 0x32

    .line 15683
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    const/16 v3, 0x2328

    .line 15684
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/OrderClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    const/16 v3, 0x2710

    .line 15685
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15686
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 15630
    check-cast p1, Lcom/squareup/orders/model/Order;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->encodedSize(Lcom/squareup/orders/model/Order;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order;
    .locals 2

    .line 15815
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order;->newBuilder()Lcom/squareup/orders/model/Order$Builder;

    move-result-object p1

    .line 15816
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->source:Lcom/squareup/orders/model/Order$Source;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orders/model/Order$Source;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->source:Lcom/squareup/orders/model/Order$Source;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$Source;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->source:Lcom/squareup/orders/model/Order$Source;

    .line 15817
    :cond_0
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->line_items:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15818
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->taxes:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15819
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->discounts:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15820
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->service_charges:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15821
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->tips:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$Tip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15822
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->fulfillments:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15823
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->returns:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$Return;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15824
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$MoneyAmounts;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 15825
    :cond_1
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$MoneyAmounts;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 15826
    :cond_2
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/orders/model/Order$RoundingAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    .line 15827
    :cond_3
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->tenders:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15828
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->payment_groups:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$PaymentGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15829
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->refunds:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15830
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->refund_groups:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$RefundGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15831
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->metadata:Ljava/util/Map;

    .line 15832
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->old_metadata_map:Ljava/util/Map;

    .line 15833
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 15834
    :cond_4
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 15835
    :cond_5
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 15836
    :cond_6
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 15837
    :cond_7
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 15838
    :cond_8
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/orders/model/Order$PricingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$PricingOptions;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    .line 15839
    :cond_9
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->reserved_discount_codes:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15840
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->reserved_rewards:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReservedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 15841
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/privacyvault/VaultedData;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 15842
    :cond_a
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/client/orders/OrderClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Builder;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/OrderClientDetails;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Builder;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    .line 15843
    :cond_b
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 15844
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Builder;->build()Lcom/squareup/orders/model/Order;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15630
    check-cast p1, Lcom/squareup/orders/model/Order;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;->redact(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order;

    move-result-object p1

    return-object p1
.end method
