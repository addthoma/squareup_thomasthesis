.class final Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$ProtoAdapter_FulfillmentDeliveryDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentDeliveryDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 10531
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10574
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;-><init>()V

    .line 10575
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 10576
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 10600
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 10598
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->cancel_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10597
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->canceled_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10596
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->delivered_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10595
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->ready_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10594
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->rejected_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10593
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->in_progress_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10592
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->completed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10591
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10590
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->delivery_window_duration(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto :goto_0

    .line 10589
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->prep_time_duration(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto/16 :goto_0

    .line 10588
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->deliver_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto/16 :goto_0

    .line 10587
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto/16 :goto_0

    .line 10581
    :pswitch_c
    :try_start_0
    sget-object v4, Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->schedule_type(Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 10583
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 10578
    :pswitch_d
    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->recipient(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    goto/16 :goto_0

    .line 10604
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 10605
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10529
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$ProtoAdapter_FulfillmentDeliveryDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10555
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10556
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10557
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10558
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10559
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10560
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10561
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10562
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10563
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10564
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10565
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10566
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10567
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10568
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10569
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10529
    check-cast p2, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$ProtoAdapter_FulfillmentDeliveryDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;)I
    .locals 4

    .line 10536
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    const/4 v3, 0x2

    .line 10537
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    const/4 v3, 0x3

    .line 10538
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    const/4 v3, 0x4

    .line 10539
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    const/4 v3, 0x5

    .line 10540
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    const/4 v3, 0x6

    .line 10541
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    const/4 v3, 0x7

    .line 10542
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    const/16 v3, 0x8

    .line 10543
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    const/16 v3, 0x9

    .line 10544
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    const/16 v3, 0xa

    .line 10545
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    const/16 v3, 0xb

    .line 10546
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    const/16 v3, 0xc

    .line 10547
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    const/16 v3, 0xd

    .line 10548
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    const/16 v3, 0xe

    .line 10549
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10550
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 10529
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$ProtoAdapter_FulfillmentDeliveryDetails;->encodedSize(Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;
    .locals 2

    .line 10610
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    move-result-object p1

    .line 10611
    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 10612
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 10613
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10529
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$ProtoAdapter_FulfillmentDeliveryDetails;->redact(Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    move-result-object p1

    return-object p1
.end method
