.class public final Lcom/squareup/orders/model/Order$ReturnTax;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReturnTax"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ReturnTax$ProtoAdapter_ReturnTax;,
        Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ReturnTax;",
        "Lcom/squareup/orders/model/Order$ReturnTax$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

.field public static final DEFAULT_SOURCE_TAX_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final applied_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax$Scope#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final source_tax_uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax$Type#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 14036
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnTax$ProtoAdapter_ReturnTax;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnTax$ProtoAdapter_ReturnTax;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 14046
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnTax;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    .line 14050
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->ADDITIVE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnTax;->DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 14054
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;->TAX_SUBTOTAL_PHASE:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnTax;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    .line 14056
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;->OTHER_TAX_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnTax;->DEFAULT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;)V
    .locals 12

    .line 14166
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/orders/model/Order$ReturnTax;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;Lokio/ByteString;)V
    .locals 1

    .line 14173
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 14174
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->uid:Ljava/lang/String;

    .line 14175
    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnTax;->source_tax_uid:Ljava/lang/String;

    .line 14176
    iput-object p3, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_id:Ljava/lang/String;

    .line 14177
    iput-object p4, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_version:Ljava/lang/Long;

    .line 14178
    iput-object p5, p0, Lcom/squareup/orders/model/Order$ReturnTax;->name:Ljava/lang/String;

    .line 14179
    iput-object p6, p0, Lcom/squareup/orders/model/Order$ReturnTax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 14180
    iput-object p7, p0, Lcom/squareup/orders/model/Order$ReturnTax;->percentage:Ljava/lang/String;

    .line 14181
    iput-object p8, p0, Lcom/squareup/orders/model/Order$ReturnTax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    .line 14182
    iput-object p9, p0, Lcom/squareup/orders/model/Order$ReturnTax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14183
    iput-object p10, p0, Lcom/squareup/orders/model/Order$ReturnTax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 14206
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ReturnTax;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 14207
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnTax;

    .line 14208
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTax;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnTax;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->uid:Ljava/lang/String;

    .line 14209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->source_tax_uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->source_tax_uid:Ljava/lang/String;

    .line 14210
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_id:Ljava/lang/String;

    .line 14211
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_version:Ljava/lang/Long;

    .line 14212
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->name:Ljava/lang/String;

    .line 14213
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 14214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->percentage:Ljava/lang/String;

    .line 14215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    .line 14216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReturnTax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    .line 14218
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 14223
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 14225
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTax;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 14226
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14227
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->source_tax_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14228
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14229
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14230
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14231
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14232
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14233
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14234
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14235
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 14236
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 2

    .line 14188
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnTax$Builder;-><init>()V

    .line 14189
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->uid:Ljava/lang/String;

    .line 14190
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->source_tax_uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->source_tax_uid:Ljava/lang/String;

    .line 14191
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->catalog_object_id:Ljava/lang/String;

    .line 14192
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 14193
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->name:Ljava/lang/String;

    .line 14194
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 14195
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->percentage:Ljava/lang/String;

    .line 14196
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    .line 14197
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14198
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    .line 14199
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTax;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 14035
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTax;->newBuilder()Lcom/squareup/orders/model/Order$ReturnTax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 14243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14244
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14245
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->source_tax_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", source_tax_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->source_tax_uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14246
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14247
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14248
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14249
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    if-eqz v1, :cond_5

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14250
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14251
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    if-eqz v1, :cond_7

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14252
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14253
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    if-eqz v1, :cond_9

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReturnTax{"

    .line 14254
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
