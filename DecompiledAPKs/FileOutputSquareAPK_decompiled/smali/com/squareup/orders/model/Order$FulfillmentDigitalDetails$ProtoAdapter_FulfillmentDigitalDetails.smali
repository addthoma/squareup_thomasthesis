.class final Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$ProtoAdapter_FulfillmentDigitalDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentDigitalDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 9968
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9993
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;-><init>()V

    .line 9994
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 9995
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 10003
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 10001
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->cancel_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    goto :goto_0

    .line 10000
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->canceled_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    goto :goto_0

    .line 9999
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->completed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    goto :goto_0

    .line 9998
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    goto :goto_0

    .line 9997
    :cond_4
    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->recipient(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    goto :goto_0

    .line 10007
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 10008
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9966
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$ProtoAdapter_FulfillmentDigitalDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9983
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9984
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9985
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9986
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9987
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9988
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9966
    check-cast p2, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$ProtoAdapter_FulfillmentDigitalDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;)I
    .locals 4

    .line 9973
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    const/4 v3, 0x2

    .line 9974
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    const/4 v3, 0x3

    .line 9975
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    const/4 v3, 0x4

    .line 9976
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    const/4 v3, 0x5

    .line 9977
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9978
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 9966
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$ProtoAdapter_FulfillmentDigitalDetails;->encodedSize(Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;
    .locals 2

    .line 10013
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    move-result-object p1

    .line 10014
    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 10015
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 10016
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 9966
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$ProtoAdapter_FulfillmentDigitalDetails;->redact(Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    move-result-object p1

    return-object p1
.end method
