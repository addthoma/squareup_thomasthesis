.class public final Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$RoundingAdjustment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$RoundingAdjustment;",
        "Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public name:Ljava/lang/String;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10728
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;
    .locals 0

    .line 10757
    iput-object p1, p0, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$RoundingAdjustment;
    .locals 5

    .line 10763
    new-instance v0, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/orders/model/Order$RoundingAdjustment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 10721
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->build()Lcom/squareup/orders/model/Order$RoundingAdjustment;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;
    .locals 0

    .line 10747
    iput-object p1, p0, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;
    .locals 0

    .line 10737
    iput-object p1, p0, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
