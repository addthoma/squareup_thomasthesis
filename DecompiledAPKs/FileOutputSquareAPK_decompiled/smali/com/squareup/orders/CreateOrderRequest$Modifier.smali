.class public final Lcom/squareup/orders/CreateOrderRequest$Modifier;
.super Lcom/squareup/wire/Message;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Modifier"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/CreateOrderRequest$Modifier$ProtoAdapter_Modifier;,
        Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
        "Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final base_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1352
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Modifier$ProtoAdapter_Modifier;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Modifier$ProtoAdapter_Modifier;-><init>()V

    sput-object v0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 1

    .line 1397
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orders/CreateOrderRequest$Modifier;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 1402
    sget-object v0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1403
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    .line 1404
    iput-object p2, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    .line 1405
    iput-object p3, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1421
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1422
    :cond_1
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;

    .line 1423
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    .line 1424
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    .line 1425
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1426
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1431
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 1433
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1434
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1435
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1436
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 1437
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;
    .locals 2

    .line 1410
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;-><init>()V

    .line 1411
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->catalog_object_id:Ljava/lang/String;

    .line 1412
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->name:Ljava/lang/String;

    .line 1413
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1414
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1351
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Modifier;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1444
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1445
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1446
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1447
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", base_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Modifier{"

    .line 1448
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
