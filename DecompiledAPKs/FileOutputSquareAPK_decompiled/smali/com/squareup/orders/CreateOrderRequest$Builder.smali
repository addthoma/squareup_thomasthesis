.class public final Lcom/squareup/orders/CreateOrderRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/CreateOrderRequest;",
        "Lcom/squareup/orders/CreateOrderRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public fulfillments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Fulfillment;",
            ">;"
        }
    .end annotation
.end field

.field public idempotency_key:Ljava/lang/String;

.field public line_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
            ">;"
        }
    .end annotation
.end field

.field public location_id:Ljava/lang/String;

.field public order:Lcom/squareup/orders/model/Order;

.field public reference_id:Ljava/lang/String;

.field public taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 273
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 274
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->line_items:Ljava/util/List;

    .line 275
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->taxes:Ljava/util/List;

    .line 276
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->discounts:Ljava/util/List;

    .line 277
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->fulfillments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/CreateOrderRequest;
    .locals 11

    .line 401
    new-instance v10, Lcom/squareup/orders/CreateOrderRequest;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->order:Lcom/squareup/orders/model/Order;

    iget-object v2, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->location_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->idempotency_key:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->reference_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->line_items:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->taxes:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->discounts:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->fulfillments:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/orders/CreateOrderRequest;-><init>(Lcom/squareup/orders/model/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 256
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Builder;->build()Lcom/squareup/orders/CreateOrderRequest;

    move-result-object v0

    return-object v0
.end method

.method public discounts(Ljava/util/List;)Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;)",
            "Lcom/squareup/orders/CreateOrderRequest$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 383
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 384
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->discounts:Ljava/util/List;

    return-object p0
.end method

.method public fulfillments(Ljava/util/List;)Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Fulfillment;",
            ">;)",
            "Lcom/squareup/orders/CreateOrderRequest$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 394
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 395
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->fulfillments:Ljava/util/List;

    return-object p0
.end method

.method public idempotency_key(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 0

    .line 312
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->idempotency_key:Ljava/lang/String;

    return-object p0
.end method

.method public line_items(Ljava/util/List;)Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
            ">;)",
            "Lcom/squareup/orders/CreateOrderRequest$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 353
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 354
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->line_items:Ljava/util/List;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public order(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 0

    .line 287
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->order:Lcom/squareup/orders/model/Order;

    return-object p0
.end method

.method public reference_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 336
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->reference_id:Ljava/lang/String;

    return-object p0
.end method

.method public taxes(Ljava/util/List;)Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;)",
            "Lcom/squareup/orders/CreateOrderRequest$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 368
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 369
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Builder;->taxes:Ljava/util/List;

    return-object p0
.end method
