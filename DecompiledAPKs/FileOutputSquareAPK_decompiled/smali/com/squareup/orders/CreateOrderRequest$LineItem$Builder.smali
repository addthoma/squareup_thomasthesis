.class public final Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$LineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
        "Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_object_id:Ljava/lang/String;

.field public discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public modifiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public quantity:Ljava/lang/String;

.field public taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public variation_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 652
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 653
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->modifiers:Ljava/util/List;

    .line 654
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->taxes:Ljava/util/List;

    .line 655
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->discounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0

    .line 691
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/CreateOrderRequest$LineItem;
    .locals 12

    .line 763
    new-instance v11, Lcom/squareup/orders/CreateOrderRequest$LineItem;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->quantity:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v4, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->variation_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->note:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->modifiers:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->taxes:Ljava/util/List;

    iget-object v9, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->discounts:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/orders/CreateOrderRequest$LineItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 633
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$LineItem;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0

    .line 728
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public discounts(Ljava/util/List;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;)",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;"
        }
    .end annotation

    .line 756
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 757
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->discounts:Ljava/util/List;

    return-object p0
.end method

.method public modifiers(Ljava/util/List;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
            ">;)",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;"
        }
    .end annotation

    .line 738
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 739
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->modifiers:Ljava/util/List;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0

    .line 666
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0

    .line 715
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0

    .line 678
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method

.method public taxes(Ljava/util/List;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;)",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;"
        }
    .end annotation

    .line 747
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 748
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->taxes:Ljava/util/List;

    return-object p0
.end method

.method public variation_name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 0

    .line 705
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->variation_name:Ljava/lang/String;

    return-object p0
.end method
