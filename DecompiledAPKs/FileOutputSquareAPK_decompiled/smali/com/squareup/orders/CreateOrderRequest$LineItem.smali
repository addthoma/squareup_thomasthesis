.class public final Lcom/squareup/orders/CreateOrderRequest$LineItem;
.super Lcom/squareup/wire/Message;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LineItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/CreateOrderRequest$LineItem$ProtoAdapter_LineItem;,
        Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
        "Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = ""

.field public static final DEFAULT_VARIATION_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final base_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.CreateOrderRequest$Discount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x9
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public final modifiers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.CreateOrderRequest$Modifier#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.CreateOrderRequest$Tax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public final variation_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 419
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$ProtoAdapter_LineItem;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$LineItem$ProtoAdapter_LineItem;-><init>()V

    sput-object v0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;)V"
        }
    .end annotation

    .line 548
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/orders/CreateOrderRequest$LineItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 554
    sget-object v0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 555
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    .line 556
    iput-object p2, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    .line 557
    iput-object p3, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 558
    iput-object p4, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    .line 559
    iput-object p5, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    .line 560
    iput-object p6, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    const-string p1, "modifiers"

    .line 561
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    const-string p1, "taxes"

    .line 562
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    const-string p1, "discounts"

    .line 563
    invoke-static {p1, p9}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 585
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 586
    :cond_1
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;

    .line 587
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    .line 588
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    .line 589
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 590
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    .line 591
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    .line 592
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    .line 593
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    .line 594
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    .line 595
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    .line 596
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 601
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 603
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 604
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 605
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 606
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 607
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 608
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 609
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 610
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 611
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 612
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 613
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;
    .locals 2

    .line 568
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;-><init>()V

    .line 569
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->name:Ljava/lang/String;

    .line 570
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->quantity:Ljava/lang/String;

    .line 571
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 572
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->variation_name:Ljava/lang/String;

    .line 573
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->note:Ljava/lang/String;

    .line 574
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->catalog_object_id:Ljava/lang/String;

    .line 575
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->modifiers:Ljava/util/List;

    .line 576
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->taxes:Ljava/util/List;

    .line 577
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->discounts:Ljava/util/List;

    .line 578
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 418
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$LineItem;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 620
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 621
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", base_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 624
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", variation_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", modifiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 628
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 629
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LineItem{"

    .line 630
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
