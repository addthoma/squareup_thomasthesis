.class public Lcom/felhr/utils/ProtocolBuffer;
.super Ljava/lang/Object;
.source "ProtocolBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;
    }
.end annotation


# static fields
.field public static final BINARY:Ljava/lang/String; = "binary"

.field private static final DEFAULT_BUFFER_SIZE:I = 0x4000

.field public static final TEXT:Ljava/lang/String; = "text"


# instance fields
.field private bufferPointer:I

.field private commands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private delimiter:Ljava/lang/String;

.field private mode:Ljava/lang/String;

.field private rawBuffer:[B

.field private rawCommands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field

.field private separator:[B

.field private stringBuffer:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput v0, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->commands:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->rawCommands:Ljava/util/List;

    .line 33
    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->mode:Ljava/lang/String;

    const-string v0, "binary"

    .line 34
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/16 v0, 0x4000

    if-eqz p1, :cond_0

    new-array p1, v0, [B

    .line 35
    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->rawBuffer:[B

    goto :goto_0

    .line 37
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->stringBuffer:Ljava/lang/StringBuilder;

    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput v0, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->commands:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->rawCommands:Ljava/util/List;

    .line 42
    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->mode:Ljava/lang/String;

    const-string v0, "binary"

    .line 43
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 44
    new-array p1, p2, [B

    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->rawBuffer:[B

    goto :goto_0

    .line 46
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->stringBuffer:Ljava/lang/StringBuilder;

    :goto_0
    return-void
.end method

.method static synthetic access$100(Lcom/felhr/utils/ProtocolBuffer;)[B
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/felhr/utils/ProtocolBuffer;->rawBuffer:[B

    return-object p0
.end method

.method static synthetic access$200(Lcom/felhr/utils/ProtocolBuffer;)[B
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/felhr/utils/ProtocolBuffer;->separator:[B

    return-object p0
.end method

.method private appendRawData([B)V
    .locals 9

    .line 117
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->rawBuffer:[B

    iget v1, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    array-length v2, p1

    const/4 v3, 0x0

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    iget v0, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    .line 120
    new-instance v0, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;-><init>(Lcom/felhr/utils/ProtocolBuffer;Lcom/felhr/utils/ProtocolBuffer$1;)V

    .line 121
    iget v1, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    .line 122
    invoke-static {v3, v1}, Lcom/annimon/stream/IntStream;->range(II)Lcom/annimon/stream/IntStream;

    move-result-object v1

    .line 123
    invoke-virtual {v1, v0}, Lcom/annimon/stream/IntStream;->filter(Lcom/annimon/stream/function/IntPredicate;)Lcom/annimon/stream/IntStream;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lcom/annimon/stream/IntStream;->toArray()[I

    move-result-object v0

    .line 127
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget v5, v0, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 128
    iget-object v6, p0, Lcom/felhr/utils/ProtocolBuffer;->rawBuffer:[B

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p0, Lcom/felhr/utils/ProtocolBuffer;->separator:[B

    array-length v8, v8

    add-int/2addr v7, v8

    invoke-static {v6, v4, v7}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v4

    .line 129
    iget-object v6, p0, Lcom/felhr/utils/ProtocolBuffer;->rawCommands:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p0, Lcom/felhr/utils/ProtocolBuffer;->separator:[B

    array-length v5, v5

    add-int/2addr v4, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->rawBuffer:[B

    array-length v1, v0

    if-ge v4, v1, :cond_1

    if-lez v4, :cond_1

    .line 135
    array-length v1, v0

    invoke-static {v0, v4, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 136
    iput v3, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    .line 137
    iget-object v1, p0, Lcom/felhr/utils/ProtocolBuffer;->rawBuffer:[B

    iget v2, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    array-length v4, p1

    invoke-static {v0, v3, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    iget v0, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    array-length p1, p1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/felhr/utils/ProtocolBuffer;->bufferPointer:I

    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized appendData([B)V
    .locals 4

    monitor-enter p0

    .line 60
    :try_start_0
    array-length v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 62
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->mode:Ljava/lang/String;

    const-string v1, "text"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    .line 64
    :try_start_2
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 65
    iget-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->stringBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    iget-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->stringBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 69
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->delimiter:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ltz v0, :cond_1

    .line 71
    iget-object v3, p0, Lcom/felhr/utils/ProtocolBuffer;->delimiter:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 72
    iget-object v3, p0, Lcom/felhr/utils/ProtocolBuffer;->commands:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v2, p0, Lcom/felhr/utils/ProtocolBuffer;->delimiter:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    .line 74
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->stringBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/felhr/utils/ProtocolBuffer;->delimiter:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :cond_1
    if-lez v2, :cond_3

    .line 78
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 79
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->stringBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 80
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->stringBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 84
    :try_start_3
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->mode:Ljava/lang/String;

    const-string v1, "binary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 87
    invoke-direct {p0, p1}, Lcom/felhr/utils/ProtocolBuffer;->appendRawData([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 89
    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public hasMoreCommands()Z
    .locals 3

    .line 92
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->mode:Ljava/lang/String;

    const-string v1, "text"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->commands:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->rawCommands:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public nextBinaryCommand()[B
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->rawCommands:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->rawCommands:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public nextTextCommand()Ljava/lang/String;
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->commands:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer;->commands:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public setDelimiter(Ljava/lang/String;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->delimiter:Ljava/lang/String;

    return-void
.end method

.method public setDelimiter([B)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer;->separator:[B

    return-void
.end method
