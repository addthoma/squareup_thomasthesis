.class public Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;
.super Lcom/felhr/usbserial/AbstractWorkerThread;
.source "UsbSerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/UsbSerialDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "WorkerThread"
.end annotation


# instance fields
.field private callback:Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;

.field private requestIN:Landroid/hardware/usb/UsbRequest;

.field final synthetic this$0:Lcom/felhr/usbserial/UsbSerialDevice;

.field private final usbSerialDevice:Lcom/felhr/usbserial/UsbSerialDevice;


# direct methods
.method public constructor <init>(Lcom/felhr/usbserial/UsbSerialDevice;Lcom/felhr/usbserial/UsbSerialDevice;)V
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    invoke-direct {p0}, Lcom/felhr/usbserial/AbstractWorkerThread;-><init>()V

    .line 338
    iput-object p2, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->usbSerialDevice:Lcom/felhr/usbserial/UsbSerialDevice;

    return-void
.end method

.method private onReceivedData([B)V
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->callback:Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;

    if-eqz v0, :cond_0

    .line 391
    invoke-interface {v0, p1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;->onReceivedData([B)V

    :cond_0
    return-void
.end method


# virtual methods
.method public doRun()V
    .locals 3

    .line 344
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v0, v0, Lcom/felhr/usbserial/UsbSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->requestWait()Landroid/hardware/usb/UsbRequest;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 345
    invoke-virtual {v0}, Landroid/hardware/usb/UsbRequest;->getEndpoint()Landroid/hardware/usb/UsbEndpoint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 346
    invoke-virtual {v0}, Landroid/hardware/usb/UsbRequest;->getEndpoint()Landroid/hardware/usb/UsbEndpoint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v0

    const/16 v1, 0x80

    if-ne v0, v1, :cond_2

    .line 348
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v0, v0, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v0}, Lcom/felhr/usbserial/SerialBuffer;->getDataReceived()[B

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/UsbSerialDevice;->access$000(Lcom/felhr/usbserial/UsbSerialDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354
    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->usbSerialDevice:Lcom/felhr/usbserial/UsbSerialDevice;

    check-cast v1, Lcom/felhr/usbserial/FTDISerialDevice;

    iget-object v1, v1, Lcom/felhr/usbserial/FTDISerialDevice;->ftdiUtilities:Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;

    invoke-virtual {v1, v0}, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->checkModemStatus([B)V

    .line 355
    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v1, v1, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v1}, Lcom/felhr/usbserial/SerialBuffer;->clearReadBuffer()V

    .line 357
    array-length v1, v0

    if-le v1, v2, :cond_1

    .line 359
    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->adaptArray([B)[B

    move-result-object v0

    .line 360
    invoke-direct {p0, v0}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->onReceivedData([B)V

    goto :goto_0

    .line 365
    :cond_0
    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v1, v1, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v1}, Lcom/felhr/usbserial/SerialBuffer;->clearReadBuffer()V

    .line 366
    invoke-direct {p0, v0}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->onReceivedData([B)V

    .line 369
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->requestIN:Landroid/hardware/usb/UsbRequest;

    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v1, v1, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v1}, Lcom/felhr/usbserial/SerialBuffer;->getReadBuffer()Ljava/nio/ByteBuffer;

    move-result-object v1

    const/16 v2, 0x4000

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbRequest;->queue(Ljava/nio/ByteBuffer;I)Z

    :cond_2
    return-void
.end method

.method public getUsbRequest()Landroid/hardware/usb/UsbRequest;
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->requestIN:Landroid/hardware/usb/UsbRequest;

    return-object v0
.end method

.method public setCallback(Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;)V
    .locals 0

    .line 375
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->callback:Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;

    return-void
.end method

.method public setUsbRequest(Landroid/hardware/usb/UsbRequest;)V
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->requestIN:Landroid/hardware/usb/UsbRequest;

    return-void
.end method
