.class public Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;
.super Lcom/felhr/usbserial/AbstractWorkerThread;
.source "UsbSpiDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/UsbSpiDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "WriteThread"
.end annotation


# instance fields
.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field final synthetic this$0:Lcom/felhr/usbserial/UsbSpiDevice;


# direct methods
.method protected constructor <init>(Lcom/felhr/usbserial/UsbSpiDevice;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->this$0:Lcom/felhr/usbserial/UsbSpiDevice;

    invoke-direct {p0}, Lcom/felhr/usbserial/AbstractWorkerThread;-><init>()V

    return-void
.end method


# virtual methods
.method public doRun()V
    .locals 5

    .line 90
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->this$0:Lcom/felhr/usbserial/UsbSpiDevice;

    iget-object v0, v0, Lcom/felhr/usbserial/UsbSpiDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v0}, Lcom/felhr/usbserial/SerialBuffer;->getWriteBuffer()[B

    move-result-object v0

    .line 91
    array-length v1, v0

    if-lez v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->this$0:Lcom/felhr/usbserial/UsbSpiDevice;

    iget-object v1, v1, Lcom/felhr/usbserial/UsbSpiDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    array-length v3, v0

    const/16 v4, 0x1388

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    :cond_0
    return-void
.end method

.method public setUsbEndpoint(Landroid/hardware/usb/UsbEndpoint;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    return-void
.end method
