.class public Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;
.super Ljava/lang/Object;
.source "FTDISerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/FTDISerialDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FTDIUtilities"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/felhr/usbserial/FTDISerialDevice;


# direct methods
.method public constructor <init>(Lcom/felhr/usbserial/FTDISerialDevice;)V
    .locals 0

    .line 538
    iput-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public adaptArray([B)[B
    .locals 4

    .line 543
    array-length v0, p1

    const/4 v1, 0x2

    const/16 v2, 0x40

    if-le v0, v2, :cond_1

    const/4 v3, 0x1

    :goto_0
    if-ge v2, v0, :cond_0

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v2, v3, 0x40

    goto :goto_0

    :cond_0
    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 555
    new-array v0, v0, [B

    .line 556
    invoke-static {p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$000([B[B)V

    return-object v0

    .line 560
    :cond_1
    invoke-static {p1, v1, v0}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    return-object p1
.end method

.method public checkModemStatus([B)V
    .locals 6

    .line 566
    array-length v0, p1

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 569
    aget-byte v1, p1, v0

    const/16 v2, 0x10

    and-int/2addr v1, v2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 570
    :goto_0
    aget-byte v4, p1, v0

    const/16 v5, 0x20

    and-int/2addr v4, v5

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    .line 572
    :goto_1
    iget-object v5, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v5}, Lcom/felhr/usbserial/FTDISerialDevice;->access$100(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 574
    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$202(Lcom/felhr/usbserial/FTDISerialDevice;Z)Z

    .line 575
    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1, v4}, Lcom/felhr/usbserial/FTDISerialDevice;->access$302(Lcom/felhr/usbserial/FTDISerialDevice;Z)Z

    .line 577
    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$400(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$500(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 578
    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$500(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object p1

    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$200(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v1

    invoke-interface {p1, v1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;->onCTSChanged(Z)V

    .line 580
    :cond_3
    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$600(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$700(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 581
    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$700(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object p1

    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$300(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v1

    invoke-interface {p1, v1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;->onDSRChanged(Z)V

    .line 583
    :cond_4
    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$102(Lcom/felhr/usbserial/FTDISerialDevice;Z)Z

    return-void

    .line 587
    :cond_5
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$400(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    .line 588
    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$200(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v0

    if-eq v1, v0, :cond_6

    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$500(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 590
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$200(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v1

    xor-int/2addr v1, v3

    invoke-static {v0, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$202(Lcom/felhr/usbserial/FTDISerialDevice;Z)Z

    .line 591
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$500(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$200(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;->onCTSChanged(Z)V

    .line 594
    :cond_6
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$600(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    .line 595
    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$300(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v0

    if-eq v4, v0, :cond_7

    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$700(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 597
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$300(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v1

    xor-int/2addr v1, v3

    invoke-static {v0, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$302(Lcom/felhr/usbserial/FTDISerialDevice;Z)Z

    .line 598
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$700(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$300(Lcom/felhr/usbserial/FTDISerialDevice;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;->onDSRChanged(Z)V

    .line 601
    :cond_7
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$800(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 603
    aget-byte v0, p1, v3

    const/4 v1, 0x4

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_8

    .line 605
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$800(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;->onParityError()V

    .line 609
    :cond_8
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$900(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 611
    aget-byte v0, p1, v3

    const/16 v1, 0x8

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    .line 613
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$900(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;->onFramingError()V

    .line 617
    :cond_9
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$1000(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 619
    aget-byte v0, p1, v3

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_a

    .line 621
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$1000(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;->onOverrunError()V

    .line 625
    :cond_a
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->access$1100(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 627
    aget-byte p1, p1, v3

    and-int/2addr p1, v2

    if-ne p1, v2, :cond_b

    .line 629
    iget-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->this$0:Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-static {p1}, Lcom/felhr/usbserial/FTDISerialDevice;->access$1100(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

    move-result-object p1

    invoke-interface {p1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;->onBreakInterrupt()V

    :cond_b
    return-void
.end method
