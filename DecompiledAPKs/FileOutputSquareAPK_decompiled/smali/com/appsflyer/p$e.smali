.class final enum Lcom/appsflyer/p$e;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/appsflyer/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/appsflyer/p$e;",
        ">;"
    }
.end annotation


# static fields
.field private static enum ˊ:Lcom/appsflyer/p$e;

.field private static enum ˋ:Lcom/appsflyer/p$e;

.field public static final enum ˎ:Lcom/appsflyer/p$e;

.field private static enum ˏ:Lcom/appsflyer/p$e;

.field private static enum ॱ:Lcom/appsflyer/p$e;

.field private static enum ॱॱ:Lcom/appsflyer/p$e;

.field private static final synthetic ᐝ:[Lcom/appsflyer/p$e;


# instance fields
.field private ʻ:Ljava/lang/String;

.field private ʼ:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 14
    new-instance v0, Lcom/appsflyer/p$e;

    const/4 v1, 0x0

    const-string v2, "UNITY"

    const-string v3, "android_unity"

    const-string v4, "com.unity3d.player.UnityPlayer"

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/appsflyer/p$e;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/appsflyer/p$e;->ॱ:Lcom/appsflyer/p$e;

    .line 15
    new-instance v0, Lcom/appsflyer/p$e;

    const/4 v2, 0x1

    const-string v3, "REACT_NATIVE"

    const-string v4, "android_reactNative"

    const-string v5, "com.facebook.react.ReactApplication"

    invoke-direct {v0, v3, v2, v4, v5}, Lcom/appsflyer/p$e;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/appsflyer/p$e;->ˊ:Lcom/appsflyer/p$e;

    .line 16
    new-instance v0, Lcom/appsflyer/p$e;

    const/4 v3, 0x2

    const-string v4, "CORDOVA"

    const-string v5, "android_cordova"

    const-string v6, "org.apache.cordova.CordovaActivity"

    invoke-direct {v0, v4, v3, v5, v6}, Lcom/appsflyer/p$e;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/appsflyer/p$e;->ˏ:Lcom/appsflyer/p$e;

    .line 17
    new-instance v0, Lcom/appsflyer/p$e;

    const/4 v4, 0x3

    const-string v5, "SEGMENT"

    const-string v6, "android_segment"

    const-string v7, "com.segment.analytics.integrations.Integration"

    invoke-direct {v0, v5, v4, v6, v7}, Lcom/appsflyer/p$e;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/appsflyer/p$e;->ˋ:Lcom/appsflyer/p$e;

    .line 18
    new-instance v0, Lcom/appsflyer/p$e;

    const/4 v5, 0x4

    const-string v6, "COCOS2DX"

    const-string v7, "android_cocos2dx"

    const-string v8, "org.cocos2dx.lib.Cocos2dxActivity"

    invoke-direct {v0, v6, v5, v7, v8}, Lcom/appsflyer/p$e;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/appsflyer/p$e;->ॱॱ:Lcom/appsflyer/p$e;

    .line 19
    new-instance v0, Lcom/appsflyer/p$e;

    const-string v6, "android_native"

    const/4 v7, 0x5

    const-string v8, "DEFAULT"

    invoke-direct {v0, v8, v7, v6, v6}, Lcom/appsflyer/p$e;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/appsflyer/p$e;->ˎ:Lcom/appsflyer/p$e;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/appsflyer/p$e;

    .line 13
    sget-object v6, Lcom/appsflyer/p$e;->ॱ:Lcom/appsflyer/p$e;

    aput-object v6, v0, v1

    sget-object v1, Lcom/appsflyer/p$e;->ˊ:Lcom/appsflyer/p$e;

    aput-object v1, v0, v2

    sget-object v1, Lcom/appsflyer/p$e;->ˏ:Lcom/appsflyer/p$e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/appsflyer/p$e;->ˋ:Lcom/appsflyer/p$e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/appsflyer/p$e;->ॱॱ:Lcom/appsflyer/p$e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/appsflyer/p$e;->ˎ:Lcom/appsflyer/p$e;

    aput-object v1, v0, v7

    sput-object v0, Lcom/appsflyer/p$e;->ᐝ:[Lcom/appsflyer/p$e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput-object p3, p0, Lcom/appsflyer/p$e;->ʼ:Ljava/lang/String;

    .line 26
    iput-object p4, p0, Lcom/appsflyer/p$e;->ʻ:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/appsflyer/p$e;
    .locals 1

    .line 13
    const-class v0, Lcom/appsflyer/p$e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/appsflyer/p$e;

    return-object p0
.end method

.method public static values()[Lcom/appsflyer/p$e;
    .locals 1

    .line 13
    sget-object v0, Lcom/appsflyer/p$e;->ᐝ:[Lcom/appsflyer/p$e;

    invoke-virtual {v0}, [Lcom/appsflyer/p$e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/appsflyer/p$e;

    return-object v0
.end method

.method static synthetic ˋ(Lcom/appsflyer/p$e;)Ljava/lang/String;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/appsflyer/p$e;->ʼ:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic ॱ(Lcom/appsflyer/p$e;)Ljava/lang/String;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/appsflyer/p$e;->ʻ:Ljava/lang/String;

    return-object p0
.end method
