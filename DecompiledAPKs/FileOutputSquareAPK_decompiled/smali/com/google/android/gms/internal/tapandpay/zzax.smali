.class public abstract Lcom/google/android/gms/internal/tapandpay/zzax;
.super Lcom/google/android/gms/internal/tapandpay/zzat;

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/gms/internal/tapandpay/zzat<",
        "TE;>;",
        "Ljava/util/Set<",
        "TE;>;"
    }
.end annotation


# instance fields
.field private transient zzey:Lcom/google/android/gms/internal/tapandpay/zzau;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/tapandpay/zzau<",
            "TE;>;"
        }
    .end annotation

    .annotation runtime Lorg/checkerframework/checker/nullness/compatqual/NullableDecl;
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/tapandpay/zzat;-><init>()V

    return-void
.end method

.method public static zza(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/gms/internal/tapandpay/zzax;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;)",
            "Lcom/google/android/gms/internal/tapandpay/zzax<",
            "TE;>;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 p0, 0x1

    aput-object p1, v1, p0

    const/4 p1, 0x2

    :goto_0
    if-eqz p1, :cond_7

    if-eq p1, p0, :cond_6

    invoke-static {p1}, Lcom/google/android/gms/internal/tapandpay/zzax;->zzb(I)I

    move-result v3

    new-array v7, v3, [Ljava/lang/Object;

    add-int/lit8 v8, v3, -0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    :goto_1
    if-ge v4, p1, :cond_2

    aget-object v5, v1, v4

    invoke-static {v5, v4}, Lcom/google/android/gms/internal/tapandpay/zzaz;->zza(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v10

    invoke-static {v10}, Lcom/google/android/gms/internal/tapandpay/zzas;->zza(I)I

    move-result v11

    :goto_2
    and-int v12, v11, v8

    aget-object v13, v7, v12

    if-nez v13, :cond_0

    add-int/lit8 v11, v9, 0x1

    aput-object v5, v1, v9

    aput-object v5, v7, v12

    add-int/2addr v6, v10

    move v9, v11

    goto :goto_3

    :cond_0
    invoke-virtual {v13, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_1
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {v1, v9, p1, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    if-ne v9, p0, :cond_3

    aget-object p0, v1, v2

    new-instance p1, Lcom/google/android/gms/internal/tapandpay/zzbd;

    invoke-direct {p1, p0, v6}, Lcom/google/android/gms/internal/tapandpay/zzbd;-><init>(Ljava/lang/Object;I)V

    return-object p1

    :cond_3
    invoke-static {v9}, Lcom/google/android/gms/internal/tapandpay/zzax;->zzb(I)I

    move-result p1

    div-int/lit8 v3, v3, 0x2

    if-ge p1, v3, :cond_4

    move p1, v9

    goto :goto_0

    :cond_4
    array-length p0, v1

    shr-int/lit8 p1, p0, 0x1

    shr-int/2addr p0, v0

    add-int/2addr p1, p0

    if-ge v9, p1, :cond_5

    invoke-static {v1, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    :cond_5
    move-object v5, v1

    new-instance p0, Lcom/google/android/gms/internal/tapandpay/zzbb;

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/internal/tapandpay/zzbb;-><init>([Ljava/lang/Object;I[Ljava/lang/Object;II)V

    return-object p0

    :cond_6
    aget-object p0, v1, v2

    new-instance p1, Lcom/google/android/gms/internal/tapandpay/zzbd;

    invoke-direct {p1, p0}, Lcom/google/android/gms/internal/tapandpay/zzbd;-><init>(Ljava/lang/Object;)V

    return-object p1

    :cond_7
    sget-object p0, Lcom/google/android/gms/internal/tapandpay/zzbb;->zzfd:Lcom/google/android/gms/internal/tapandpay/zzbb;

    return-object p0
.end method

.method private static zzb(I)I
    .locals 6

    const/4 v0, 0x2

    invoke-static {p0, v0}, Ljava/lang/Math;->max(II)I

    move-result p0

    const/4 v0, 0x1

    const v1, 0x2ccccccc

    if-ge p0, v1, :cond_1

    add-int/lit8 v1, p0, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v1

    shl-int/lit8 v0, v1, 0x1

    :goto_0
    int-to-double v1, v0

    const-wide v3, 0x3fe6666666666666L    # 0.7

    mul-double v1, v1, v3

    int-to-double v3, p0

    cmpg-double v5, v1, v3

    if-gez v5, :cond_0

    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v0

    :cond_1
    const/high16 v1, 0x40000000    # 2.0f

    if-ge p0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    return v1

    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "collection too large"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lorg/checkerframework/checker/nullness/compatqual/NullableDecl;
        .end annotation
    .end param

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/internal/tapandpay/zzax;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/tapandpay/zzax;->zzd()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/tapandpay/zzax;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/tapandpay/zzax;->zzd()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/tapandpay/zzax;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/gms/internal/tapandpay/zzbc;->zza(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 4

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    add-int/2addr v2, v3

    not-int v2, v2

    not-int v2, v2

    goto :goto_0

    :cond_1
    return v2
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/tapandpay/zzat;->zza()Lcom/google/android/gms/internal/tapandpay/zzbe;

    move-result-object v0

    return-object v0
.end method

.method public final zzb()Lcom/google/android/gms/internal/tapandpay/zzau;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/tapandpay/zzau<",
            "TE;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/tapandpay/zzax;->zzey:Lcom/google/android/gms/internal/tapandpay/zzau;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/tapandpay/zzax;->zze()Lcom/google/android/gms/internal/tapandpay/zzau;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/tapandpay/zzax;->zzey:Lcom/google/android/gms/internal/tapandpay/zzau;

    :cond_0
    return-object v0
.end method

.method zzd()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method zze()Lcom/google/android/gms/internal/tapandpay/zzau;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/tapandpay/zzau<",
            "TE;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/tapandpay/zzat;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/tapandpay/zzau;->zza([Ljava/lang/Object;)Lcom/google/android/gms/internal/tapandpay/zzau;

    move-result-object v0

    return-object v0
.end method
