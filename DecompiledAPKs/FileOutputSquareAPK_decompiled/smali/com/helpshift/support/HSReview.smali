.class public final Lcom/helpshift/support/HSReview;
.super Landroidx/fragment/app/FragmentActivity;
.source "HSReview.java"


# instance fields
.field private flowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/helpshift/support/flows/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Landroidx/fragment/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .line 42
    invoke-static {p1}, Lcom/helpshift/util/ApplicationUtil;->getContextWithUpdatedLocaleLegacy(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->attachBaseContext(Landroid/content/Context;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 20
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    new-instance p1, Landroid/view/View;

    invoke-direct {p1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Lcom/helpshift/support/HSReview;->setContentView(Landroid/view/View;)V

    .line 25
    invoke-static {}, Lcom/helpshift/support/flows/CustomContactUsFlowListHolder;->getFlowList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/HSReview;->flowList:Ljava/util/List;

    const/4 p1, 0x0

    .line 26
    invoke-static {p1}, Lcom/helpshift/support/flows/CustomContactUsFlowListHolder;->setFlowList(Ljava/util/List;)V

    .line 28
    invoke-virtual {p0}, Lcom/helpshift/support/HSReview;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    .line 29
    new-instance v0, Lcom/helpshift/support/HSReviewFragment;

    invoke-direct {v0}, Lcom/helpshift/support/HSReviewFragment;-><init>()V

    const-string v1, "hs__review_dialog"

    .line 30
    invoke-virtual {v0, p1, v1}, Lcom/helpshift/support/HSReviewFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 35
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    .line 36
    iget-object v0, p0, Lcom/helpshift/support/HSReview;->flowList:Ljava/util/List;

    invoke-static {v0}, Lcom/helpshift/support/flows/CustomContactUsFlowListHolder;->setFlowList(Ljava/util/List;)V

    .line 37
    invoke-static {}, Lcom/helpshift/util/ApplicationUtil;->restoreApplicationLocale()V

    return-void
.end method
