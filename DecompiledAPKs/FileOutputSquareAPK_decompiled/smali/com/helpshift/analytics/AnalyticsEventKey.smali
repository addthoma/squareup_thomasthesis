.class public interface abstract Lcom/helpshift/analytics/AnalyticsEventKey;
.super Ljava/lang/Object;
.source "AnalyticsEventKey.java"


# static fields
.field public static final BODY:Ljava/lang/String; = "body"

.field public static final FAQ_SEARCH_RESULT_COUNT:Ljava/lang/String; = "n"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final ISSUE_ID:Ljava/lang/String; = "issue_id"

.field public static final NETWORK_CONNECTIVITY:Ljava/lang/String; = "nt"

.field public static final PREISSUE_ID:Ljava/lang/String; = "preissue_id"

.field public static final PROTOCOL:Ljava/lang/String; = "p"

.field public static final RESPONSE:Ljava/lang/String; = "response"

.field public static final SEARCH_QUERY:Ljava/lang/String; = "s"

.field public static final STR:Ljava/lang/String; = "str"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final URL:Ljava/lang/String; = "u"
