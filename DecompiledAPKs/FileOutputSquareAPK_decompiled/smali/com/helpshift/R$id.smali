.class public final Lcom/helpshift/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/helpshift/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f0a0135

.field public static final action_bar_activity_content:I = 0x7f0a0136

.field public static final action_bar_container:I = 0x7f0a0137

.field public static final action_bar_root:I = 0x7f0a0139

.field public static final action_bar_spinner:I = 0x7f0a013a

.field public static final action_bar_subtitle:I = 0x7f0a013b

.field public static final action_bar_title:I = 0x7f0a013c

.field public static final action_container:I = 0x7f0a0140

.field public static final action_context_bar:I = 0x7f0a0141

.field public static final action_divider:I = 0x7f0a0143

.field public static final action_image:I = 0x7f0a0146

.field public static final action_menu_divider:I = 0x7f0a0147

.field public static final action_menu_presenter:I = 0x7f0a0148

.field public static final action_mode_bar:I = 0x7f0a0149

.field public static final action_mode_bar_stub:I = 0x7f0a014a

.field public static final action_mode_close_button:I = 0x7f0a014b

.field public static final action_text:I = 0x7f0a014c

.field public static final actions:I = 0x7f0a014e

.field public static final activity_chooser_view_content:I = 0x7f0a0160

.field public static final add:I = 0x7f0a0166

.field public static final additional_feedback:I = 0x7f0a0176

.field public static final admin_attach_screenshot_button:I = 0x7f0a0199

.field public static final admin_attachment_imageview:I = 0x7f0a019a

.field public static final admin_attachment_message_layout:I = 0x7f0a019b

.field public static final admin_attachment_request_text:I = 0x7f0a019c

.field public static final admin_date_text:I = 0x7f0a019d

.field public static final admin_image_attachment_message_container:I = 0x7f0a019e

.field public static final admin_image_message_layout:I = 0x7f0a019f

.field public static final admin_message:I = 0x7f0a01a0

.field public static final admin_message_container:I = 0x7f0a01a1

.field public static final admin_message_text:I = 0x7f0a01a2

.field public static final admin_review_message_layout:I = 0x7f0a01a3

.field public static final admin_suggestion_message:I = 0x7f0a01a4

.field public static final admin_suggestion_message_layout:I = 0x7f0a01a5

.field public static final admin_text_message_layout:I = 0x7f0a01a6

.field public static final agent_screenshot_request_message_layout:I = 0x7f0a01a8

.field public static final agent_typing_container:I = 0x7f0a01a9

.field public static final agent_typing_indicator:I = 0x7f0a01aa

.field public static final alertTitle:I = 0x7f0a01ad

.field public static final async:I = 0x7f0a01d1

.field public static final attachment_date:I = 0x7f0a01d2

.field public static final attachment_file_name:I = 0x7f0a01d3

.field public static final attachment_file_size:I = 0x7f0a01d4

.field public static final attachment_icon:I = 0x7f0a01d5

.field public static final authentication_failed_body:I = 0x7f0a01d8

.field public static final authentication_failed_title:I = 0x7f0a01d9

.field public static final auto:I = 0x7f0a01db

.field public static final blocking:I = 0x7f0a0239

.field public static final bottom:I = 0x7f0a023f

.field public static final buttonPanel:I = 0x7f0a0276

.field public static final button_containers:I = 0x7f0a0278

.field public static final button_retry:I = 0x7f0a0279

.field public static final buttons_separator:I = 0x7f0a027b

.field public static final center:I = 0x7f0a02fa

.field public static final change:I = 0x7f0a0300

.field public static final checkbox:I = 0x7f0a0318

.field public static final chronometer:I = 0x7f0a033a

.field public static final contact_us_button:I = 0x7f0a0399

.field public static final contact_us_hint_text:I = 0x7f0a039a

.field public static final contact_us_view:I = 0x7f0a039b

.field public static final container:I = 0x7f0a03a1

.field public static final content:I = 0x7f0a03a3

.field public static final contentPanel:I = 0x7f0a03a4

.field public static final conversation_closed_view:I = 0x7f0a03aa

.field public static final conversation_redacted_view:I = 0x7f0a03ab

.field public static final conversations_divider:I = 0x7f0a03ac

.field public static final coordinator:I = 0x7f0a03ad

.field public static final csat_dislike_msg:I = 0x7f0a0516

.field public static final csat_like_msg:I = 0x7f0a0517

.field public static final csat_message:I = 0x7f0a0518

.field public static final csat_msg_container:I = 0x7f0a0519

.field public static final csat_view_layout:I = 0x7f0a051a

.field public static final custom:I = 0x7f0a0524

.field public static final customPanel:I = 0x7f0a0525

.field public static final date:I = 0x7f0a0546

.field public static final decor_content_parent:I = 0x7f0a055d

.field public static final default_activity_button:I = 0x7f0a055e

.field public static final design_bottom_sheet:I = 0x7f0a058b

.field public static final design_menu_item_action_area:I = 0x7f0a058c

.field public static final design_menu_item_action_area_stub:I = 0x7f0a058d

.field public static final design_menu_item_text:I = 0x7f0a058e

.field public static final design_navigation_view:I = 0x7f0a058f

.field public static final details_fragment_container:I = 0x7f0a05ad

.field public static final divider:I = 0x7f0a05e3

.field public static final download_attachment_progressbar:I = 0x7f0a05e7

.field public static final download_button:I = 0x7f0a05e8

.field public static final download_progressbar_container:I = 0x7f0a05ea

.field public static final edit_query:I = 0x7f0a0660

.field public static final end:I = 0x7f0a0707

.field public static final errorReplyTextView:I = 0x7f0a071c

.field public static final expand_activities_button:I = 0x7f0a0732

.field public static final expanded_menu:I = 0x7f0a0733

.field public static final faq_content_view:I = 0x7f0a0743

.field public static final faq_fragment_container:I = 0x7f0a0744

.field public static final fill:I = 0x7f0a0750

.field public static final filled:I = 0x7f0a0753

.field public static final fixed:I = 0x7f0a075c

.field public static final flow_fragment_container:I = 0x7f0a0764

.field public static final flow_list:I = 0x7f0a0765

.field public static final footer_message:I = 0x7f0a076c

.field public static final forever:I = 0x7f0a076d

.field public static final ghost_view:I = 0x7f0a078a

.field public static final group_divider:I = 0x7f0a07be

.field public static final helpful_button:I = 0x7f0a07d4

.field public static final history_loading_layout_view:I = 0x7f0a07dc

.field public static final home:I = 0x7f0a07df

.field public static final hs__action_done:I = 0x7f0a07e7

.field public static final hs__attach_screenshot:I = 0x7f0a07e8

.field public static final hs__bottom_sheet:I = 0x7f0a07e9

.field public static final hs__bottomsheet_coordinator:I = 0x7f0a07ea

.field public static final hs__collapsed_picker_header_text:I = 0x7f0a07eb

.field public static final hs__confirmation:I = 0x7f0a07ec

.field public static final hs__contact_us:I = 0x7f0a07ed

.field public static final hs__conversationDetail:I = 0x7f0a07ee

.field public static final hs__conversationDetailWrapper:I = 0x7f0a07ef

.field public static final hs__conversation_cardview_container:I = 0x7f0a07f0

.field public static final hs__conversation_icon:I = 0x7f0a07f1

.field public static final hs__csat_option:I = 0x7f0a07f2

.field public static final hs__email:I = 0x7f0a07f3

.field public static final hs__emailWrapper:I = 0x7f0a07f4

.field public static final hs__empty_picker_view:I = 0x7f0a07f5

.field public static final hs__expanded_picker_header_text:I = 0x7f0a07f6

.field public static final hs__messageText:I = 0x7f0a07f7

.field public static final hs__messagesList:I = 0x7f0a07f8

.field public static final hs__new_conversation:I = 0x7f0a07f9

.field public static final hs__new_conversation_btn:I = 0x7f0a07fa

.field public static final hs__new_conversation_footer_reason:I = 0x7f0a07fb

.field public static final hs__notification_badge:I = 0x7f0a07fc

.field public static final hs__notification_badge_padding:I = 0x7f0a07fd

.field public static final hs__option:I = 0x7f0a07fe

.field public static final hs__optionsList:I = 0x7f0a07ff

.field public static final hs__picker_action_back:I = 0x7f0a0800

.field public static final hs__picker_action_clear:I = 0x7f0a0801

.field public static final hs__picker_action_collapse:I = 0x7f0a0802

.field public static final hs__picker_action_expand:I = 0x7f0a0803

.field public static final hs__picker_action_search:I = 0x7f0a0804

.field public static final hs__picker_collapsed_header:I = 0x7f0a0805

.field public static final hs__picker_collapsed_shadow:I = 0x7f0a0806

.field public static final hs__picker_expanded_header:I = 0x7f0a0807

.field public static final hs__picker_expanded_shadow:I = 0x7f0a0808

.field public static final hs__picker_header_container:I = 0x7f0a0809

.field public static final hs__picker_header_search:I = 0x7f0a080a

.field public static final hs__screenshot:I = 0x7f0a080b

.field public static final hs__search:I = 0x7f0a080c

.field public static final hs__sendMessageBtn:I = 0x7f0a080d

.field public static final hs__start_new_conversation:I = 0x7f0a080e

.field public static final hs__username:I = 0x7f0a080f

.field public static final hs__usernameWrapper:I = 0x7f0a0810

.field public static final hs_download_foreground_view:I = 0x7f0a0811

.field public static final hs_logo:I = 0x7f0a0812

.field public static final icon:I = 0x7f0a0818

.field public static final icon_group:I = 0x7f0a081a

.field public static final image:I = 0x7f0a0821

.field public static final imageview_container:I = 0x7f0a0828

.field public static final info:I = 0x7f0a082d

.field public static final info_icon:I = 0x7f0a0830

.field public static final issue_publish_id_label:I = 0x7f0a08c3

.field public static final italic:I = 0x7f0a08c8

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a08e3

.field public static final labeled:I = 0x7f0a090f

.field public static final largeLabel:I = 0x7f0a0911

.field public static final left:I = 0x7f0a0920

.field public static final like_status:I = 0x7f0a0939

.field public static final line1:I = 0x7f0a093a

.field public static final line3:I = 0x7f0a093b

.field public static final listMode:I = 0x7f0a094a

.field public static final list_fragment_container:I = 0x7f0a094b

.field public static final list_item:I = 0x7f0a094c

.field public static final loading_error_state_view:I = 0x7f0a0950

.field public static final loading_error_tap_to_retry:I = 0x7f0a0951

.field public static final loading_progressbar:I = 0x7f0a0954

.field public static final loading_state_view:I = 0x7f0a0957

.field public static final masked:I = 0x7f0a09ac

.field public static final message:I = 0x7f0a09d3

.field public static final mini:I = 0x7f0a09dd

.field public static final mtrl_child_content_container:I = 0x7f0a0a00

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a0a01

.field public static final multiply:I = 0x7f0a0a0b

.field public static final navigation_header_container:I = 0x7f0a0a10

.field public static final networkErrorFooter:I = 0x7f0a0a13

.field public static final networkErrorFooterText:I = 0x7f0a0a14

.field public static final networkErrorFooterTryAgainText:I = 0x7f0a0a15

.field public static final networkErrorIcon:I = 0x7f0a0a16

.field public static final networkErrorProgressBar:I = 0x7f0a0a17

.field public static final no_faqs_view:I = 0x7f0a0a2b

.field public static final none:I = 0x7f0a0a45

.field public static final normal:I = 0x7f0a0a46

.field public static final notification_background:I = 0x7f0a0a4c

.field public static final notification_main_column:I = 0x7f0a0a60

.field public static final notification_main_column_container:I = 0x7f0a0a61

.field public static final offline_error_view:I = 0x7f0a0a81

.field public static final option_list_item_layout:I = 0x7f0a0abc

.field public static final option_text:I = 0x7f0a0abe

.field public static final options_header:I = 0x7f0a0ac2

.field public static final options_message_view:I = 0x7f0a0ac3

.field public static final outline:I = 0x7f0a0b73

.field public static final pager_tabs:I = 0x7f0a0b86

.field public static final parallax:I = 0x7f0a0bac

.field public static final parentPanel:I = 0x7f0a0bae

.field public static final parent_matrix:I = 0x7f0a0baf

.field public static final pin:I = 0x7f0a0c2c

.field public static final progress:I = 0x7f0a0c7b

.field public static final progress_bar:I = 0x7f0a0c7c

.field public static final progress_circular:I = 0x7f0a0c7e

.field public static final progress_description_text_view:I = 0x7f0a0c80

.field public static final progress_horizontal:I = 0x7f0a0c81

.field public static final progressbar:I = 0x7f0a0c83

.field public static final question_footer:I = 0x7f0a0ca2

.field public static final question_footer_message:I = 0x7f0a0ca3

.field public static final question_list:I = 0x7f0a0ca4

.field public static final radio:I = 0x7f0a0cd9

.field public static final ratingBar:I = 0x7f0a0ce2

.field public static final relativeLayout1:I = 0x7f0a0d53

.field public static final replyBoxLabelLayout:I = 0x7f0a0d5f

.field public static final replyBoxViewStub:I = 0x7f0a0d60

.field public static final replyFieldLabel:I = 0x7f0a0d61

.field public static final report_issue:I = 0x7f0a0d69

.field public static final resolution_accepted_button:I = 0x7f0a0d70

.field public static final resolution_question_text:I = 0x7f0a0d71

.field public static final resolution_rejected_button:I = 0x7f0a0d72

.field public static final review_request_button:I = 0x7f0a0d77

.field public static final review_request_date:I = 0x7f0a0d78

.field public static final review_request_message:I = 0x7f0a0d79

.field public static final review_request_message_container:I = 0x7f0a0d7a

.field public static final right:I = 0x7f0a0d8d

.field public static final right_icon:I = 0x7f0a0d8f

.field public static final right_side:I = 0x7f0a0d92

.field public static final save_non_transition_alpha:I = 0x7f0a0e04

.field public static final screen:I = 0x7f0a0e0a

.field public static final screenshot_loading_indicator:I = 0x7f0a0e0c

.field public static final screenshot_preview:I = 0x7f0a0e0d

.field public static final screenshot_view_container:I = 0x7f0a0e0e

.field public static final scrollIndicatorDown:I = 0x7f0a0e10

.field public static final scrollIndicatorUp:I = 0x7f0a0e11

.field public static final scrollView:I = 0x7f0a0e12

.field public static final scroll_indicator:I = 0x7f0a0e14

.field public static final scroll_jump_button:I = 0x7f0a0e15

.field public static final scrollable:I = 0x7f0a0e17

.field public static final search_badge:I = 0x7f0a0e19

.field public static final search_bar:I = 0x7f0a0e1a

.field public static final search_button:I = 0x7f0a0e1b

.field public static final search_close_btn:I = 0x7f0a0e1c

.field public static final search_edit_frame:I = 0x7f0a0e1d

.field public static final search_go_btn:I = 0x7f0a0e1f

.field public static final search_list:I = 0x7f0a0e20

.field public static final search_list_footer_divider:I = 0x7f0a0e21

.field public static final search_mag_icon:I = 0x7f0a0e22

.field public static final search_plate:I = 0x7f0a0e23

.field public static final search_result:I = 0x7f0a0e24

.field public static final search_result_message:I = 0x7f0a0e25

.field public static final search_src_text:I = 0x7f0a0e26

.field public static final search_voice_btn:I = 0x7f0a0e27

.field public static final secondary_button:I = 0x7f0a0e2e

.field public static final section_list:I = 0x7f0a0e3a

.field public static final section_pager:I = 0x7f0a0e3b

.field public static final select_dialog_listview:I = 0x7f0a0e41

.field public static final select_question_view:I = 0x7f0a0e4d

.field public static final selectable_option_skip:I = 0x7f0a0e56

.field public static final selectable_option_text:I = 0x7f0a0e57

.field public static final selectable_options_container:I = 0x7f0a0e58

.field public static final selected:I = 0x7f0a0e5b

.field public static final send_anyway_button:I = 0x7f0a0e62

.field public static final shortcut:I = 0x7f0a0e7d

.field public static final single_question_container:I = 0x7f0a0e98

.field public static final skipBubbleTextView:I = 0x7f0a0e99

.field public static final skipOuterBubble:I = 0x7f0a0e9b

.field public static final smallLabel:I = 0x7f0a0ea9

.field public static final snackbar_action:I = 0x7f0a0eb3

.field public static final snackbar_text:I = 0x7f0a0eb4

.field public static final spacer:I = 0x7f0a0eb8

.field public static final split_action_bar:I = 0x7f0a0ecd

.field public static final src_atop:I = 0x7f0a0f19

.field public static final src_in:I = 0x7f0a0f1a

.field public static final src_over:I = 0x7f0a0f1b

.field public static final start:I = 0x7f0a0f2a

.field public static final stretch:I = 0x7f0a0f42

.field public static final submenuarrow:I = 0x7f0a0f44

.field public static final submit:I = 0x7f0a0f45

.field public static final submit_area:I = 0x7f0a0f46

.field public static final suggestionsListStub:I = 0x7f0a0f4e

.field public static final support_fragment_container:I = 0x7f0a0f50

.field public static final system_message:I = 0x7f0a0f5b

.field public static final tabMode:I = 0x7f0a0f5e

.field public static final tag_transition_group:I = 0x7f0a0f6b

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a0f6c

.field public static final tag_unhandled_key_listeners:I = 0x7f0a0f6d

.field public static final text:I = 0x7f0a0f94

.field public static final text2:I = 0x7f0a0f95

.field public static final textSpacerNoButtons:I = 0x7f0a0f98

.field public static final textSpacerNoTitle:I = 0x7f0a0f99

.field public static final textViewFailureMessage:I = 0x7f0a0f9b

.field public static final textViewLoadingText:I = 0x7f0a0f9c

.field public static final textinput_counter:I = 0x7f0a0fa7

.field public static final textinput_error:I = 0x7f0a0fa8

.field public static final textinput_helper_text:I = 0x7f0a0fa9

.field public static final time:I = 0x7f0a0fde

.field public static final title:I = 0x7f0a103f

.field public static final titleDividerNoCustom:I = 0x7f0a1041

.field public static final title_template:I = 0x7f0a1046

.field public static final toolbar:I = 0x7f0a104d

.field public static final top:I = 0x7f0a104e

.field public static final topPanel:I = 0x7f0a104f

.field public static final touch_outside:I = 0x7f0a1059

.field public static final transition_current_scene:I = 0x7f0a1085

.field public static final transition_layout_save:I = 0x7f0a1086

.field public static final transition_position:I = 0x7f0a1087

.field public static final transition_scene_layoutid_cache:I = 0x7f0a1088

.field public static final transition_transform:I = 0x7f0a1089

.field public static final unhelpful_button:I = 0x7f0a10b1

.field public static final uniform:I = 0x7f0a10b2

.field public static final unlabeled:I = 0x7f0a10b8

.field public static final unread_indicator_red_dot:I = 0x7f0a10b9

.field public static final unread_indicator_red_dot_image_view:I = 0x7f0a10ba

.field public static final up:I = 0x7f0a10bf

.field public static final upload_attachment_progressbar:I = 0x7f0a10d0

.field public static final user_attachment_imageview:I = 0x7f0a10d7

.field public static final user_date_text:I = 0x7f0a10d8

.field public static final user_image_message_layout:I = 0x7f0a10d9

.field public static final user_message_container:I = 0x7f0a10da

.field public static final user_message_retry_button:I = 0x7f0a10db

.field public static final user_message_text:I = 0x7f0a10dc

.field public static final user_text_message_layout:I = 0x7f0a10dd

.field public static final vertical_divider:I = 0x7f0a10ec

.field public static final view_faqs_load_error:I = 0x7f0a10f5

.field public static final view_faqs_loading:I = 0x7f0a10f6

.field public static final view_no_faqs:I = 0x7f0a10f7

.field public static final view_offset_helper:I = 0x7f0a10fc

.field public static final view_pager_container:I = 0x7f0a10fe

.field public static final visible:I = 0x7f0a1107

.field public static final web_view:I = 0x7f0a1117

.field public static final wrap_content:I = 0x7f0a1125


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
