.class final Lcom/helpshift/CoreInternal$6;
.super Ljava/lang/Object;
.source "CoreInternal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/CoreInternal;->handlePush(Landroid/content/Context;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$data:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/os/Bundle;Landroid/content/Context;)V
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/helpshift/CoreInternal$6;->val$data:Landroid/os/Bundle;

    iput-object p2, p0, Lcom/helpshift/CoreInternal$6;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 151
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/helpshift/CoreInternal$6;->val$data:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v1, "Helpshift_CoreInternal"

    const-string v2, "Handling push on main thread"

    .line 153
    invoke-static {v1, v2}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    sget-object v1, Lcom/helpshift/CoreInternal;->apiProvider:Lcom/helpshift/Core$ApiProvider;

    iget-object v2, p0, Lcom/helpshift/CoreInternal$6;->val$context:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/helpshift/Core$ApiProvider;->_handlePush(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method
