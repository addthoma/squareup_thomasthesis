.class Lcom/helpshift/conversation/viewmodel/ConversationVM$13;
.super Lcom/helpshift/common/domain/F;
.source "ConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationVM;->markMessagesAsSeenOnExit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V
    .locals 0

    .line 590
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$13;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 598
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$13;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getAllConversations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 599
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->markMessagesAsSeen()V

    goto :goto_0

    :cond_0
    return-void
.end method
