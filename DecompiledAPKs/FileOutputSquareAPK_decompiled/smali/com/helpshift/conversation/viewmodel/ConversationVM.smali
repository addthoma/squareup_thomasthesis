.class public Lcom/helpshift/conversation/viewmodel/ConversationVM;
.super Ljava/lang/Object;
.source "ConversationVM.java"

# interfaces
.implements Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;
.implements Lcom/helpshift/conversation/viewmodel/MessageListVMCallback;
.implements Lcom/helpshift/account/AuthenticationFailureDM$AuthenticationFailureObserver;
.implements Ljava/util/Observer;


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_ConvVM"


# instance fields
.field protected attachImageButtonWidget:Lcom/helpshift/widget/ButtonWidget;

.field private confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

.field protected conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

.field final conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

.field conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

.field domain:Lcom/helpshift/common/domain/Domain;

.field historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

.field private isScreenCurrentlyVisible:Z

.field messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

.field platform:Lcom/helpshift/common/platform/Platform;

.field renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

.field replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

.field replyFieldWidget:Lcom/helpshift/widget/ReplyFieldWidget;

.field private retainMessageBoxOnUI:Z

.field private scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

.field final sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field public final viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

.field widgetGateway:Lcom/helpshift/widget/WidgetGateway;


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/activeconversation/ViewableConversation;Lcom/helpshift/conversation/activeconversation/ConversationRenderer;Z)V
    .locals 1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    .line 85
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 86
    iput-object p3, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    .line 87
    iput-object p4, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 88
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 89
    iput-boolean p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->retainMessageBoxOnUI:Z

    .line 90
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, p0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->addObserver(Ljava/util/Observer;)V

    .line 91
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/helpshift/account/AuthenticationFailureDM;->registerListener(Lcom/helpshift/account/AuthenticationFailureDM$AuthenticationFailureObserver;)V

    .line 93
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->createWidgetGateway()V

    .line 96
    invoke-virtual {p0, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->createMediator(Lcom/helpshift/common/domain/Domain;)Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    .line 98
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p1}, Lcom/helpshift/widget/WidgetGateway;->makeAttachImageButtonWidget()Lcom/helpshift/widget/ButtonWidget;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->attachImageButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    .line 99
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->attachImageButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setAttachImageButton(Lcom/helpshift/widget/ButtonWidget;)V

    .line 101
    new-instance p1, Lcom/helpshift/widget/ButtonWidget;

    invoke-direct {p1}, Lcom/helpshift/widget/ButtonWidget;-><init>()V

    .line 102
    iget-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p2}, Lcom/helpshift/widget/WidgetGateway;->makeReplyFieldWidget()Lcom/helpshift/widget/ReplyFieldWidget;

    move-result-object p2

    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->replyFieldWidget:Lcom/helpshift/widget/ReplyFieldWidget;

    .line 104
    iget-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p2}, Lcom/helpshift/widget/WidgetGateway;->makeScrollJumperWidget()Lcom/helpshift/widget/ScrollJumperWidget;

    move-result-object p2

    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    .line 105
    iget-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {p2, p6}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setScrollJumperWidget(Lcom/helpshift/widget/ScrollJumperWidget;)V

    .line 107
    iget-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {p2, p1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setReplyButtonWidget(Lcom/helpshift/widget/ButtonWidget;)V

    .line 108
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->replyFieldWidget:Lcom/helpshift/widget/ReplyFieldWidget;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setReplyFieldWidget(Lcom/helpshift/widget/ReplyFieldWidget;)V

    .line 110
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->shouldShowReplyBoxOnConversationRejected()Z

    move-result p1

    .line 111
    invoke-virtual {p4}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p2

    .line 112
    invoke-virtual {p2, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setEnableMessageClickOnResolutionRejected(Z)V

    .line 113
    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    .line 114
    invoke-virtual {p6, p2, p1}, Lcom/helpshift/widget/WidgetGateway;->makeReplyBoxWidget(Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)Lcom/helpshift/widget/ButtonWidget;

    move-result-object p6

    iput-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    .line 115
    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {p6, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setReplyBoxWidget(Lcom/helpshift/widget/ButtonWidget;)V

    .line 117
    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p6, p2}, Lcom/helpshift/widget/WidgetGateway;->makeConfirmationBoxWidget(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Lcom/helpshift/widget/ButtonWidget;

    move-result-object p6

    iput-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    .line 118
    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {p6, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setConfirmationBoxWidget(Lcom/helpshift/widget/ButtonWidget;)V

    .line 120
    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    .line 121
    invoke-virtual {p6, p2, p1}, Lcom/helpshift/widget/WidgetGateway;->makeConversationFooterWidget(Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)Lcom/helpshift/widget/ConversationFooterWidget;

    move-result-object p6

    iput-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    .line 123
    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    invoke-virtual {p6, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setConversationFooterWidget(Lcom/helpshift/widget/ConversationFooterWidget;)V

    .line 126
    new-instance p6, Lcom/helpshift/widget/HistoryLoadingWidget;

    invoke-direct {p6}, Lcom/helpshift/widget/HistoryLoadingWidget;-><init>()V

    iput-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    .line 127
    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    invoke-virtual {p6, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setHistoryLoadingWidget(Lcom/helpshift/widget/HistoryLoadingWidget;)V

    .line 130
    iget-object p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {p6}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result p6

    if-eqz p6, :cond_0

    const/4 p6, 0x2

    .line 131
    invoke-virtual {p3, p6}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setConversationViewState(I)V

    goto :goto_0

    :cond_0
    const/4 p6, -0x1

    .line 134
    invoke-virtual {p3, p6}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setConversationViewState(I)V

    :goto_0
    if-nez p1, :cond_1

    .line 136
    iget-object p1, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object p3, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, p3, :cond_1

    .line 138
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->handleConversationEnded()V

    .line 142
    :cond_1
    invoke-virtual {p4, p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->setConversationVMCallback(Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;)V

    .line 145
    iput-object p5, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    .line 146
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setRenderer(Lcom/helpshift/conversation/activeconversation/ConversationRenderer;)V

    .line 147
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {p1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderAll()V

    .line 148
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->initMessagesList()V

    return-void
.end method

.method private clearNotifications()V
    .locals 2

    .line 734
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$16;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$16;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private generateSystemRedactedConversationMessageDM(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Lcom/helpshift/conversation/activeconversation/message/SystemRedactedConversationMessageDM;
    .locals 5

    .line 232
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/SystemRedactedConversationMessageDM;

    .line 233
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getCreatedAt()Ljava/lang/String;

    move-result-object v1

    .line 234
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getEpochCreatedAtTime()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/helpshift/conversation/activeconversation/message/SystemRedactedConversationMessageDM;-><init>(Ljava/lang/String;JI)V

    .line 236
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/message/SystemRedactedConversationMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 237
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object p1, v0, Lcom/helpshift/conversation/activeconversation/message/SystemRedactedConversationMessageDM;->conversationLocalId:Ljava/lang/Long;

    return-object v0
.end method

.method private getUIMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ")",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    .line 207
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 212
    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    if-eqz v1, :cond_0

    .line 213
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->generateSystemRedactedConversationMessageDM(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Lcom/helpshift/conversation/activeconversation/message/SystemRedactedConversationMessageDM;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 216
    :cond_0
    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->buildUIMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_0
    return-object v0
.end method

.method private getUIMessagesForHistory(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ")",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    .line 467
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 472
    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    if-eqz v1, :cond_0

    .line 473
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->generateSystemRedactedConversationMessageDM(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Lcom/helpshift/conversation/activeconversation/message/SystemRedactedConversationMessageDM;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 476
    :cond_0
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_0
    return-object v0
.end method

.method private loadHistoryMessagesInternal()V
    .locals 2

    .line 961
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/HistoryLoadingWidget;->getState()Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    move-result-object v0

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;->LOADING:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    if-ne v0, v1, :cond_0

    return-void

    .line 966
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$20;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$20;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private markMessagesAsSeenOnEntry()V
    .locals 2

    .line 575
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$12;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$12;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private markMessagesAsSeenOnExit()V
    .locals 2

    .line 590
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$13;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$13;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private notifyRendererForScrollToBottom()V
    .locals 2

    .line 999
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$21;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$21;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private resetIncrementMessageCountFlag()V
    .locals 2

    .line 746
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$17;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$17;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private setScreenVisibility(Z)V
    .locals 0

    .line 428
    iput-boolean p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->isScreenCurrentlyVisible:Z

    return-void
.end method

.method private setUserCanReadMessages(Z)V
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setUserCanReadMessages(Z)V

    .line 316
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isAgentTyping()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onAgentTypingUpdate(Z)V

    return-void
.end method

.method private shouldShowReplyBoxOnConversationRejected()Z
    .locals 1

    .line 702
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getUserReplyText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    .line 703
    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->shouldPersistMessageBox()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->retainMessageBoxOnUI:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private showUnreadMessagesIndicator()V
    .locals 2

    .line 878
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ScrollJumperWidget;->setShouldShowUnreadMessagesIndicator(Z)V

    return-void
.end method


# virtual methods
.method public add(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 0

    .line 829
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->addAll(Ljava/util/Collection;)V

    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .line 54
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->add(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    return-void
.end method

.method public addAll(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    .line 834
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "addAll called : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_ConvVM"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    if-eqz v0, :cond_0

    .line 836
    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->addMessages(Ljava/util/Collection;)V

    :cond_0
    return-void
.end method

.method public appendMessages(II)V
    .locals 1

    .line 854
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_0

    .line 855
    invoke-interface {v0, p1, p2}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->appendMessages(II)V

    :cond_0
    return-void
.end method

.method protected buildUIMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ")",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    .line 227
    new-instance v0, Ljava/util/ArrayList;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method clearReply()V
    .locals 2

    .line 341
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$1;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$1;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public clearUserReplyDraft()V
    .locals 2

    .line 324
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveUserReplyText(Ljava/lang/String;)V

    .line 325
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->clearReply()V

    return-void
.end method

.method protected createMediator(Lcom/helpshift/common/domain/Domain;)Lcom/helpshift/conversation/viewmodel/ConversationMediator;
    .locals 1

    .line 156
    new-instance v0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-direct {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;-><init>(Lcom/helpshift/common/domain/Domain;)V

    return-object v0
.end method

.method protected createWidgetGateway()V
    .locals 3

    .line 152
    new-instance v0, Lcom/helpshift/widget/WidgetGateway;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-direct {v0, v1, v2}, Lcom/helpshift/widget/WidgetGateway;-><init>(Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V

    iput-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    return-void
.end method

.method public handleAdminAttachmentMessageClick(Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V
    .locals 2

    .line 535
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$9;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM$9;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public handleAppReviewRequestClick(Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V
    .locals 2

    .line 385
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method handleConversationRejectedState()V
    .locals 3

    .line 674
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 675
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveUserReplyText(Ljava/lang/String;)V

    .line 678
    iget-boolean v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    if-eqz v0, :cond_0

    .line 679
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->REDACTED_STATE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 682
    :cond_0
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->REJECTED_MESSAGE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    .line 684
    :goto_0
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showStartNewConversation(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    .line 685
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setConversationRejected(Z)V

    return-void
.end method

.method public handlePreIssueCreationSuccess()V
    .locals 0

    return-void
.end method

.method public handleScreenshotMessageClick(Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;)V
    .locals 2

    .line 376
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$4;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM$4;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method protected initMessagesList()V
    .locals 6

    .line 167
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->unregisterMessageListVMCallback()V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->initializeConversationsForUI()V

    .line 178
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->initializeIssueStatusForUI()V

    .line 179
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->hasMoreMessages()Z

    move-result v1

    .line 180
    new-instance v2, Lcom/helpshift/conversation/viewmodel/MessageListVM;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v4, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-direct {v2, v3, v4}, Lcom/helpshift/conversation/viewmodel/MessageListVM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;)V

    iput-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    .line 181
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getUIConversations()Ljava/util/List;

    move-result-object v2

    .line 184
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 185
    iget-object v4, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v4}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getAllConversations()Ljava/util/List;

    move-result-object v4

    .line 186
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 187
    invoke-direct {p0, v5}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->getUIMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 189
    :cond_1
    iget-object v4, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    invoke-virtual {v4, v2, v3, v1, p0}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->initializeMessageList(Ljava/util/List;Ljava/util/List;ZLcom/helpshift/conversation/viewmodel/MessageListVMCallback;)V

    .line 191
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    invoke-virtual {v2}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->getUiMessageDMs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->initializeMessages(Ljava/util/List;)V

    .line 193
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v1, p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->registerMessagesObserver(Lcom/helpshift/common/util/HSListObserver;)V

    .line 194
    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 195
    :goto_1
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setConversationRejected(Z)V

    .line 196
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->prefillReplyBox()V

    return-void
.end method

.method public isMessageBoxVisible()Z
    .locals 1

    .line 419
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    return v0
.end method

.method public isVisibleOnUI()Z
    .locals 1

    .line 424
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->isScreenCurrentlyVisible:Z

    return v0
.end method

.method public launchAttachment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 545
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$10;

    invoke-direct {v1, p0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM$10;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public launchScreenshotAttachment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 407
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM$6;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public markConversationResolutionStatus(Z)V
    .locals 2

    .line 556
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$11;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM$11;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Z)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public newAdminMessagesAdded()V
    .locals 0

    .line 862
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->updateUIOnNewMessageReceived()V

    return-void
.end method

.method public newUserMessagesAdded()V
    .locals 0

    .line 868
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->notifyRendererForScrollToBottom()V

    return-void
.end method

.method public onAdminMessageLinkClicked(Ljava/lang/String;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 5

    const/4 v0, 0x0

    .line 762
    :try_start_0
    invoke-static {p1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 765
    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    move-object v1, v0

    .line 770
    :goto_0
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 771
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getAllConversations()Ljava/util/List;

    move-result-object v2

    .line 773
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 774
    iget-object v4, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v4, p2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v0, v3

    .line 779
    :cond_2
    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_5

    .line 780
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    if-eqz v0, :cond_4

    .line 782
    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 783
    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    const-string v3, "preissue_id"

    invoke-interface {p2, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    :cond_3
    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 786
    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    const-string v2, "issue_id"

    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    const-string v0, "p"

    .line 789
    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "u"

    .line 790
    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 791
    sget-object p1, Lcom/helpshift/analytics/AnalyticsEventType;->ADMIN_MESSAGE_DEEPLINK_CLICKED:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {p0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->pushAnalyticsEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/util/Map;)V

    :cond_5
    return-void
.end method

.method public onAgentTypingUpdate(Z)V
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$7;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM$7;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Z)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public onAuthenticationFailure()V
    .locals 2

    .line 921
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$19;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$19;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public onCSATSurveySubmitted(ILjava/lang/String;)V
    .locals 3

    .line 708
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_0

    .line 709
    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->showCSATSubmittedView()V

    .line 711
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 712
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v1

    if-nez v1, :cond_1

    .line 713
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v2, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->START_NEW_CONVERSATION:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {v1, v2}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showStartNewConversation(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    .line 715
    :cond_1
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v2, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;ILjava/lang/String;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    invoke-virtual {v1, v2}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public onConversationInboxPollFailure()V
    .locals 0

    return-void
.end method

.method public onConversationInboxPollSuccess()V
    .locals 0

    return-void
.end method

.method public onHistoryLoadingError()V
    .locals 2

    .line 517
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;->ERROR:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setHistoryLoadingWidgetState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V

    return-void
.end method

.method public onHistoryLoadingStarted()V
    .locals 2

    .line 522
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;->LOADING:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setHistoryLoadingWidgetState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V

    return-void
.end method

.method public onHistoryLoadingSuccess()V
    .locals 2

    .line 512
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;->NONE:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setHistoryLoadingWidgetState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V

    return-void
.end method

.method public onImageAttachmentButtonClick()V
    .locals 2

    .line 693
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$14;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$14;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V
    .locals 6

    .line 615
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Changing conversation status to: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_ConvVM"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 617
    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInProgressState(Lcom/helpshift/conversation/dto/IssueState;)Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, -0x1

    if-eqz v1, :cond_0

    .line 619
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->resetDefaultMenuItemsVisibility()V

    .line 620
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {p1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showMessageBox()V

    const/4 p1, 0x0

    :goto_0
    const/4 v0, 0x0

    const/4 v5, 0x2

    goto/16 :goto_2

    .line 624
    :cond_0
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v1, :cond_3

    .line 625
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldShowConversationResolutionQuestion()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 626
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {p1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->confirmationBox()V

    .line 630
    :cond_1
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {p1}, Lcom/helpshift/widget/ScrollJumperWidget;->isVisible()Z

    move-result p1

    if-nez p1, :cond_2

    .line 631
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->notifyRendererForScrollToBottom()V

    :cond_2
    const/4 p1, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    goto :goto_2

    .line 634
    :cond_3
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v1, :cond_4

    .line 636
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->handleConversationRejectedState()V

    const/4 p1, 0x1

    const/4 v0, 0x1

    goto :goto_2

    .line 638
    :cond_4
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v1, :cond_6

    .line 639
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const-string v1, ""

    invoke-virtual {p1, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveUserReplyText(Ljava/lang/String;)V

    .line 640
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldShowCSATInFooter()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 641
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->CSAT_RATING:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showStartNewConversation(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    goto :goto_1

    .line 644
    :cond_5
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->START_NEW_CONVERSATION:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showStartNewConversation(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    goto :goto_1

    .line 647
    :cond_6
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v1, :cond_7

    .line 648
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {p1, v4}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setPersistMessageBox(Z)V

    .line 649
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {p1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showMessageBox()V

    .line 650
    invoke-virtual {v0, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setEnableMessageClickOnResolutionRejected(Z)V

    const/4 p1, 0x1

    goto :goto_0

    .line 653
    :cond_7
    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v0, :cond_8

    .line 654
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->ARCHIVAL_MESSAGE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showStartNewConversation(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    goto :goto_1

    .line 656
    :cond_8
    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->AUTHOR_MISMATCH:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v0, :cond_9

    .line 657
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->AUTHOR_MISMATCH:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showStartNewConversation(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    :cond_9
    :goto_1
    const/4 p1, 0x1

    const/4 v0, 0x0

    :goto_2
    if-eqz v3, :cond_a

    .line 662
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->updateUIOnNewMessageReceived()V

    :cond_a
    if-eqz p1, :cond_b

    .line 665
    invoke-virtual {p0, v4}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onAgentTypingUpdate(Z)V

    .line 667
    :cond_b
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {p1, v5}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setConversationViewState(I)V

    if-nez v0, :cond_c

    .line 669
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {p1, v4}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setConversationRejected(Z)V

    :cond_c
    return-void
.end method

.method public onNewConversationButtonClicked()V
    .locals 2

    .line 729
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->stopLiveUpdates()V

    .line 730
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setStartNewConversationButtonClicked(ZZ)V

    return-void
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x0

    .line 251
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->setScreenVisibility(Z)V

    .line 252
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->setUserCanReadMessages(Z)V

    .line 253
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->markMessagesAsSeenOnExit()V

    .line 254
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->clearNotifications()V

    .line 255
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->resetIncrementMessageCountFlag()V

    .line 256
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->getReply()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->saveReplyText(Ljava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 242
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->refreshVM()V

    .line 243
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderMenuItems()V

    const/4 v0, 0x1

    .line 244
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->setScreenVisibility(Z)V

    .line 245
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->setUserCanReadMessages(Z)V

    .line 246
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->markMessagesAsSeenOnEntry()V

    .line 247
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->clearNotifications()V

    return-void
.end method

.method public onScrollJumperViewClicked()V
    .locals 0

    .line 932
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->notifyRendererForScrollToBottom()V

    return-void
.end method

.method public onScrolledToBottom()V
    .locals 2

    .line 936
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ScrollJumperWidget;->setVisible(Z)V

    .line 938
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ScrollJumperWidget;->setShouldShowUnreadMessagesIndicator(Z)V

    return-void
.end method

.method public onScrolledToTop()V
    .locals 2

    .line 947
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/HistoryLoadingWidget;->getState()Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    move-result-object v0

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;->NONE:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    if-ne v0, v1, :cond_0

    .line 948
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->loadHistoryMessagesInternal()V

    :cond_0
    return-void
.end method

.method public onScrolling()V
    .locals 2

    .line 942
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ScrollJumperWidget;->setVisible(Z)V

    return-void
.end method

.method public onSkipClick()V
    .locals 0

    return-void
.end method

.method public onUIMessageListUpdated()V
    .locals 0

    return-void
.end method

.method protected prefillReplyBox()V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getUserReplyText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->setReply(Ljava/lang/String;)V

    return-void
.end method

.method public prependConversations(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;Z)V"
        }
    .end annotation

    .line 484
    invoke-static {p1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    .line 491
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->prependMessages(Ljava/util/List;Z)V

    :cond_0
    return-void

    .line 496
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getUIConversations()Ljava/util/List;

    move-result-object v0

    .line 499
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 500
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 501
    invoke-direct {p0, v2}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->getUIMessagesForHistory(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 504
    :cond_2
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    if-eqz p1, :cond_3

    .line 505
    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->updateUIConversationOrder(Ljava/util/List;)V

    .line 506
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    invoke-virtual {p1, v1, p2}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->prependMessages(Ljava/util/List;Z)V

    :cond_3
    return-void
.end method

.method public pushAnalyticsEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/analytics/AnalyticsEventType;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 755
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/util/Map;)V

    return-void
.end method

.method public refreshAll()V
    .locals 1

    .line 909
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_0

    .line 910
    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->notifyRefreshList()V

    :cond_0
    return-void
.end method

.method public refreshVM()V
    .locals 4

    .line 268
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->shouldShowReplyBoxOnConversationRejected()Z

    move-result v0

    .line 269
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    .line 270
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v2, v3, v1, v0}, Lcom/helpshift/widget/WidgetGateway;->updateReplyBoxWidget(Lcom/helpshift/widget/ButtonWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V

    .line 272
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v2, v3, v1}, Lcom/helpshift/widget/WidgetGateway;->updateConfirmationBoxWidget(Lcom/helpshift/widget/ButtonWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 273
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    invoke-virtual {v2, v3, v1, v0}, Lcom/helpshift/widget/WidgetGateway;->updateConversationFooterWidget(Lcom/helpshift/widget/ConversationFooterWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V

    .line 277
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setConversationViewState(I)V

    goto :goto_0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setConversationViewState(I)V

    .line 285
    :goto_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0, p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->registerMessagesObserver(Lcom/helpshift/common/util/HSListObserver;)V

    .line 288
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0, p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->setConversationVMCallback(Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;)V

    .line 292
    iget-object v0, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/ConversationInboxPoller;->startChatPoller()V

    :cond_2
    return-void
.end method

.method public renderMenuItems()V
    .locals 1

    .line 606
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderAll()V

    return-void
.end method

.method protected resetDefaultMenuItemsVisibility()V
    .locals 2

    .line 689
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->attachImageButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {v1}, Lcom/helpshift/widget/WidgetGateway;->getDefaultVisibilityForAttachImageButton()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    return-void
.end method

.method public retryHistoryLoadingMessages()V
    .locals 2

    .line 954
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/HistoryLoadingWidget;->getState()Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    move-result-object v0

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;->ERROR:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    if-ne v0, v1, :cond_0

    .line 955
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->loadHistoryMessagesInternal()V

    :cond_0
    return-void
.end method

.method public retryMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 2

    .line 362
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$3;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM$3;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public saveReplyText(Ljava/lang/String;)V
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveUserReplyText(Ljava/lang/String;)V

    return-void
.end method

.method public sendScreenShot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V
    .locals 2

    .line 526
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM$8;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public sendTextMessage()V
    .locals 3

    .line 330
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->getReply()Ljava/lang/String;

    move-result-object v0

    .line 332
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setPersistMessageBox(Z)V

    .line 337
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sendTextMessage(Ljava/lang/String;)V

    return-void
.end method

.method protected sendTextMessage(Ljava/lang/String;)V
    .locals 2

    .line 352
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->clearReply()V

    .line 353
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationVM$2;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM$2;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setConversationViewState(I)V
    .locals 1

    .line 725
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setConversationViewState(I)V

    return-void
.end method

.method public shouldShowUnreadMessagesIndicator()Z
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ScrollJumperWidget;->shouldShowUnreadMessagesIndicator()Z

    move-result v0

    return v0
.end method

.method public startLiveUpdates()V
    .locals 1

    .line 796
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->startLiveUpdates()V

    return-void
.end method

.method public stopLiveUpdates()V
    .locals 1

    .line 800
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->stopLiveUpdates()V

    return-void
.end method

.method public unregisterRenderer()V
    .locals 2

    .line 299
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->unregisterConversationVMCallback()V

    .line 301
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->unregisterMessageListVMCallback()V

    .line 303
    iput-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    .line 306
    :cond_0
    iput-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    .line 308
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->setRenderer(Lcom/helpshift/conversation/activeconversation/ConversationRenderer;)V

    .line 309
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {v0, p0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->deleteObserver(Ljava/util/Observer;)V

    .line 310
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->deletePreIssueIfNotCreated(Lcom/helpshift/conversation/activeconversation/ViewableConversation;)V

    .line 311
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/helpshift/account/AuthenticationFailureDM;->unregisterListener(Lcom/helpshift/account/AuthenticationFailureDM$AuthenticationFailureObserver;)V

    return-void
.end method

.method public update(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 2

    .line 842
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "update called : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_ConvVM"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    if-eqz v0, :cond_0

    .line 844
    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic update(Ljava/lang/Object;)V
    .locals 0

    .line 54
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->update(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 1

    .line 805
    iget-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v0, Lcom/helpshift/conversation/viewmodel/ConversationVM$18;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM$18;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Ljava/util/Observable;)V

    invoke-virtual {p2, v0}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public updateLastUserActivityTime()V
    .locals 3

    .line 849
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateLastUserActivityTime(J)V

    return-void
.end method

.method public updateMessages(II)V
    .locals 1

    .line 902
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_0

    .line 903
    invoke-interface {v0, p1, p2}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->updateMessages(II)V

    :cond_0
    return-void
.end method

.method protected updateTypingIndicatorStatus(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 984
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {p1}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->showAgentTypingIndicator()V

    .line 986
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {p1}, Lcom/helpshift/widget/ScrollJumperWidget;->isVisible()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 989
    :cond_0
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {p1}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->hideAgentTypingIndicator()V

    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 993
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->notifyRendererForScrollToBottom()V

    :cond_1
    return-void
.end method

.method protected updateUIOnNewMessageReceived()V
    .locals 1

    .line 891
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ScrollJumperWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 893
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->showUnreadMessagesIndicator()V

    goto :goto_0

    .line 896
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->notifyRendererForScrollToBottom()V

    :goto_0
    return-void
.end method

.method public updateUnreadMessageCountIndicator(Z)V
    .locals 1

    .line 1010
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {v0, p1}, Lcom/helpshift/widget/ScrollJumperWidget;->setShouldShowUnreadMessagesIndicator(Z)V

    return-void
.end method
