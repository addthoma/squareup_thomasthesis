.class Lcom/helpshift/conversation/viewmodel/ConversationVM$12;
.super Lcom/helpshift/common/domain/F;
.source "ConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationVM;->markMessagesAsSeenOnEntry()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V
    .locals 0

    .line 575
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$12;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 1

    .line 578
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$12;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 580
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->markMessagesAsSeen()V

    :cond_0
    return-void
.end method
