.class Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;
.super Ljava/lang/Object;
.source "ConversationInboxDM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateConversationStateHolder"
.end annotation


# instance fields
.field final description:Ljava/lang/String;

.field final imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

.field private final startNewConversationInternalF:Lcom/helpshift/common/domain/F;

.field final synthetic this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

.field final userProvidedEmail:Ljava/lang/String;

.field final userProvidedName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 1

    .line 1974
    iput-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1966
    new-instance p1, Lcom/helpshift/common/domain/One;

    new-instance v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder$1;

    invoke-direct {v0, p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder$1;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;)V

    invoke-direct {p1, v0}, Lcom/helpshift/common/domain/One;-><init>(Lcom/helpshift/common/domain/F;)V

    iput-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;->startNewConversationInternalF:Lcom/helpshift/common/domain/F;

    .line 1975
    iput-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;->description:Ljava/lang/String;

    .line 1976
    iput-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;->userProvidedName:Ljava/lang/String;

    .line 1977
    iput-object p4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;->userProvidedEmail:Ljava/lang/String;

    .line 1978
    iput-object p5, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    return-void
.end method


# virtual methods
.method getStartNewConversationInternalF()Lcom/helpshift/common/domain/F;
    .locals 1

    .line 1982
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;->startNewConversationInternalF:Lcom/helpshift/common/domain/F;

    return-object v0
.end method
