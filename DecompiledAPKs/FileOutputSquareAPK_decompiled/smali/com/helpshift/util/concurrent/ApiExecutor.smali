.class public interface abstract Lcom/helpshift/util/concurrent/ApiExecutor;
.super Ljava/lang/Object;
.source "ApiExecutor.java"


# virtual methods
.method public abstract awaitForSyncExecution()V
.end method

.method public abstract runAsync(Ljava/lang/Runnable;)V
.end method

.method public abstract runOnUiThread(Ljava/lang/Runnable;)V
.end method

.method public abstract runSync(Ljava/lang/Runnable;)V
.end method
