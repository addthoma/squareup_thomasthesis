.class public Lcom/helpshift/widget/ImageAttachmentWidget;
.super Lcom/helpshift/widget/Widget;
.source "ImageAttachmentWidget.java"


# instance fields
.field private clickable:Z

.field private imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Lcom/helpshift/widget/Widget;-><init>()V

    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/helpshift/widget/ImageAttachmentWidget;->clickable:Z

    return-void
.end method


# virtual methods
.method public getImagePath()Ljava/lang/String;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/helpshift/widget/ImageAttachmentWidget;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    .line 27
    :cond_0
    iget-object v0, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/helpshift/widget/ImageAttachmentWidget;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    iget-object v1, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    :goto_0
    return-object v1
.end method

.method public getImagePickerFile()Lcom/helpshift/conversation/dto/ImagePickerFile;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/helpshift/widget/ImageAttachmentWidget;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    return-object v0
.end method

.method public isClickable()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/helpshift/widget/ImageAttachmentWidget;->clickable:Z

    return v0
.end method

.method public setClickable(Z)V
    .locals 0

    .line 35
    iput-boolean p1, p0, Lcom/helpshift/widget/ImageAttachmentWidget;->clickable:Z

    .line 36
    invoke-virtual {p0}, Lcom/helpshift/widget/ImageAttachmentWidget;->notifyChanged()V

    return-void
.end method

.method public setImagePickerFile(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/helpshift/widget/ImageAttachmentWidget;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    .line 20
    invoke-virtual {p0}, Lcom/helpshift/widget/ImageAttachmentWidget;->notifyChanged()V

    return-void
.end method
