.class public Lcom/helpshift/widget/HistoryLoadingWidget;
.super Lcom/helpshift/widget/Widget;
.source "HistoryLoadingWidget.java"


# instance fields
.field private state:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Lcom/helpshift/widget/Widget;-><init>()V

    .line 11
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;->NONE:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    iput-object v0, p0, Lcom/helpshift/widget/HistoryLoadingWidget;->state:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    return-void
.end method


# virtual methods
.method public getState()Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/helpshift/widget/HistoryLoadingWidget;->state:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    return-object v0
.end method

.method public setState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/helpshift/widget/HistoryLoadingWidget;->state:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    if-eq v0, p1, :cond_0

    .line 19
    iput-object p1, p0, Lcom/helpshift/widget/HistoryLoadingWidget;->state:Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    .line 20
    invoke-virtual {p0}, Lcom/helpshift/widget/HistoryLoadingWidget;->notifyChanged()V

    :cond_0
    return-void
.end method
