.class final Lcom/evernote/android/job/JobExecutor$JobCallable;
.super Ljava/lang/Object;
.source "JobExecutor.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/android/job/JobExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "JobCallable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/android/job/Job$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final mJob:Lcom/evernote/android/job/Job;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field final synthetic this$0:Lcom/evernote/android/job/JobExecutor;


# direct methods
.method private constructor <init>(Lcom/evernote/android/job/JobExecutor;Lcom/evernote/android/job/Job;)V
    .locals 2

    .line 156
    iput-object p1, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->this$0:Lcom/evernote/android/job/JobExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p2, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    .line 159
    iget-object p1, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-virtual {p1}, Lcom/evernote/android/job/Job;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 160
    invoke-static {}, Lcom/evernote/android/job/JobExecutor;->access$100()J

    move-result-wide v0

    const-string p2, "JobExecutor"

    invoke-static {p1, p2, v0, v1}, Lcom/evernote/android/job/WakeLockUtil;->acquireWakeLock(Landroid/content/Context;Ljava/lang/String;J)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    iput-object p1, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method synthetic constructor <init>(Lcom/evernote/android/job/JobExecutor;Lcom/evernote/android/job/Job;Lcom/evernote/android/job/JobExecutor$1;)V
    .locals 0

    .line 151
    invoke-direct {p0, p1, p2}, Lcom/evernote/android/job/JobExecutor$JobCallable;-><init>(Lcom/evernote/android/job/JobExecutor;Lcom/evernote/android/job/Job;)V

    return-void
.end method

.method private handleResult(Lcom/evernote/android/job/Job;Lcom/evernote/android/job/Job$Result;)V
    .locals 4

    .line 197
    iget-object v0, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-virtual {v0}, Lcom/evernote/android/job/Job;->getParams()Lcom/evernote/android/job/Job$Params;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/android/job/Job$Params;->getRequest()Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->isPeriodic()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_0

    sget-object v1, Lcom/evernote/android/job/Job$Result;->RESCHEDULE:Lcom/evernote/android/job/Job$Result;

    invoke-virtual {v1, p2}, Lcom/evernote/android/job/Job$Result;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/evernote/android/job/Job;->isDeleted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 202
    invoke-virtual {v0, v3, v3}, Lcom/evernote/android/job/JobRequest;->reschedule(ZZ)Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    .line 203
    iget-object p2, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/evernote/android/job/Job;->onReschedule(I)V

    goto :goto_0

    .line 206
    :cond_0
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->isPeriodic()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    sget-object v1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    invoke-virtual {v1, p2}, Lcom/evernote/android/job/Job$Result;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 214
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/android/job/Job;->isDeleted()Z

    move-result p1

    if-nez p1, :cond_4

    if-nez v2, :cond_3

    if-eqz v3, :cond_4

    .line 218
    :cond_3
    invoke-virtual {v0, v2, v3}, Lcom/evernote/android/job/JobRequest;->updateStats(ZZ)V

    :cond_4
    return-void
.end method

.method private runJob()Lcom/evernote/android/job/Job$Result;
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 183
    :try_start_0
    iget-object v2, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-virtual {v2}, Lcom/evernote/android/job/Job;->runJob()Lcom/evernote/android/job/Job$Result;

    move-result-object v2

    .line 184
    invoke-static {}, Lcom/evernote/android/job/JobExecutor;->access$200()Lcom/evernote/android/job/util/JobCat;

    move-result-object v3

    const-string v4, "Finished %s"

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/evernote/android/job/util/JobCat;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    iget-object v3, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-direct {p0, v3, v2}, Lcom/evernote/android/job/JobExecutor$JobCallable;->handleResult(Lcom/evernote/android/job/Job;Lcom/evernote/android/job/Job$Result;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    .line 189
    invoke-static {}, Lcom/evernote/android/job/JobExecutor;->access$200()Lcom/evernote/android/job/util/JobCat;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    aput-object v4, v1, v0

    const-string v0, "Crashed %s"

    invoke-virtual {v3, v2, v0, v1}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    iget-object v0, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-virtual {v0}, Lcom/evernote/android/job/Job;->getResult()Lcom/evernote/android/job/Job$Result;

    move-result-object v2

    :goto_0
    return-object v2
.end method


# virtual methods
.method public call()Lcom/evernote/android/job/Job$Result;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "Wake lock was not held after job %s was done. The job took too long to complete. This could have unintended side effects on your app."

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 167
    :try_start_0
    iget-object v3, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-virtual {v3}, Lcom/evernote/android/job/Job;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-static {}, Lcom/evernote/android/job/JobExecutor;->access$100()J

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Lcom/evernote/android/job/WakeLockUtil;->acquireWakeLock(Landroid/content/Context;Landroid/os/PowerManager$WakeLock;J)Z

    .line 168
    invoke-direct {p0}, Lcom/evernote/android/job/JobExecutor$JobCallable;->runJob()Lcom/evernote/android/job/Job$Result;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    iget-object v4, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->this$0:Lcom/evernote/android/job/JobExecutor;

    iget-object v5, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-virtual {v4, v5}, Lcom/evernote/android/job/JobExecutor;->markJobAsFinished(Lcom/evernote/android/job/Job;)V

    .line 173
    iget-object v4, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v4

    if-nez v4, :cond_1

    .line 174
    :cond_0
    invoke-static {}, Lcom/evernote/android/job/JobExecutor;->access$200()Lcom/evernote/android/job/util/JobCat;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    aput-object v5, v2, v1

    invoke-virtual {v4, v0, v2}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-static {v0}, Lcom/evernote/android/job/WakeLockUtil;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-object v3

    :catchall_0
    move-exception v3

    .line 171
    iget-object v4, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->this$0:Lcom/evernote/android/job/JobExecutor;

    iget-object v5, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    invoke-virtual {v4, v5}, Lcom/evernote/android/job/JobExecutor;->markJobAsFinished(Lcom/evernote/android/job/Job;)V

    .line 173
    iget-object v4, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v4

    if-nez v4, :cond_3

    .line 174
    :cond_2
    invoke-static {}, Lcom/evernote/android/job/JobExecutor;->access$200()Lcom/evernote/android/job/util/JobCat;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mJob:Lcom/evernote/android/job/Job;

    aput-object v5, v2, v1

    invoke-virtual {v4, v0, v2}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/evernote/android/job/JobExecutor$JobCallable;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-static {v0}, Lcom/evernote/android/job/WakeLockUtil;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 177
    throw v3
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 151
    invoke-virtual {p0}, Lcom/evernote/android/job/JobExecutor$JobCallable;->call()Lcom/evernote/android/job/Job$Result;

    move-result-object v0

    return-object v0
.end method
