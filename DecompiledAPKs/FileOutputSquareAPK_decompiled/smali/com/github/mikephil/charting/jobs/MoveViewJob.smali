.class public Lcom/github/mikephil/charting/jobs/MoveViewJob;
.super Ljava/lang/Object;
.source "MoveViewJob.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected mTrans:Lcom/github/mikephil/charting/utils/Transformer;

.field protected mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

.field protected view:Landroid/view/View;

.field protected xIndex:F

.field protected yValue:F


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/utils/ViewPortHandler;FFLcom/github/mikephil/charting/utils/Transformer;Landroid/view/View;)V
    .locals 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput v0, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->xIndex:F

    .line 20
    iput v0, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->yValue:F

    .line 27
    iput-object p1, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    .line 28
    iput p2, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->xIndex:F

    .line 29
    iput p3, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->yValue:F

    .line 30
    iput-object p4, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->mTrans:Lcom/github/mikephil/charting/utils/Transformer;

    .line 31
    iput-object p5, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->view:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 38
    iget v1, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->xIndex:F

    const/4 v2, 0x0

    aput v1, v0, v2

    iget v1, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->yValue:F

    const/4 v2, 0x1

    aput v1, v0, v2

    .line 41
    iget-object v1, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->mTrans:Lcom/github/mikephil/charting/utils/Transformer;

    invoke-virtual {v1, v0}, Lcom/github/mikephil/charting/utils/Transformer;->pointValuesToPixel([F)V

    .line 42
    iget-object v1, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, p0, Lcom/github/mikephil/charting/jobs/MoveViewJob;->view:Landroid/view/View;

    invoke-virtual {v1, v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->centerViewPort([FLandroid/view/View;)V

    return-void
.end method
