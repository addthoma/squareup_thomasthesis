.class public Lcom/github/mikephil/charting/data/BarEntry;
.super Lcom/github/mikephil/charting/data/Entry;
.source "BarEntry.java"


# instance fields
.field private mVals:[F


# direct methods
.method public constructor <init>(FI)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    return-void
.end method

.method public constructor <init>(FILjava/lang/Object;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/github/mikephil/charting/data/Entry;-><init>(FILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>([FI)V
    .locals 1

    .line 21
    invoke-static {p1}, Lcom/github/mikephil/charting/data/BarEntry;->calcSum([F)F

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/github/mikephil/charting/data/Entry;-><init>(FI)V

    .line 23
    iput-object p1, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    return-void
.end method

.method public constructor <init>([FILjava/lang/String;)V
    .locals 1

    .line 44
    invoke-static {p1}, Lcom/github/mikephil/charting/data/BarEntry;->calcSum([F)F

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/github/mikephil/charting/data/Entry;-><init>(FILjava/lang/Object;)V

    .line 46
    iput-object p1, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    return-void
.end method

.method private static calcSum([F)F
    .locals 4

    .line 139
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v0, :cond_0

    return v1

    :cond_0
    aget v3, p0, v2

    add-float/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public copy()Lcom/github/mikephil/charting/data/BarEntry;
    .locals 4

    .line 65
    new-instance v0, Lcom/github/mikephil/charting/data/BarEntry;

    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/BarEntry;->getVal()F

    move-result v1

    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/BarEntry;->getXIndex()I

    move-result v2

    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/BarEntry;->getData()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/github/mikephil/charting/data/BarEntry;-><init>(FILjava/lang/Object;)V

    .line 66
    iget-object v1, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    iput-object v1, v0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    return-object v0
.end method

.method public bridge synthetic copy()Lcom/github/mikephil/charting/data/Entry;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/BarEntry;->copy()Lcom/github/mikephil/charting/data/BarEntry;

    move-result-object v0

    return-object v0
.end method

.method public getBelowSum(I)F
    .locals 3

    .line 115
    iget-object v0, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 119
    :cond_0
    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-le v0, p1, :cond_2

    if-gez v0, :cond_1

    goto :goto_1

    .line 122
    :cond_1
    iget-object v2, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method

.method public getClosestIndexAbove(F)I
    .locals 4

    .line 99
    iget-object v0, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 102
    :cond_0
    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    :goto_0
    if-lez v0, :cond_2

    .line 105
    iget-object v2, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    aget v3, v2, v0

    add-float/2addr v3, v1

    cmpl-float v3, p1, v3

    if-gtz v3, :cond_1

    goto :goto_1

    .line 106
    :cond_1
    aget v2, v2, v0

    add-float/2addr v1, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v0
.end method

.method public getVals()[F
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    return-object v0
.end method

.method public setVals([F)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/github/mikephil/charting/data/BarEntry;->mVals:[F

    return-void
.end method
