.class public Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
.super Ljava/lang/Object;
.source "DividerDecoration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mColour:I

.field private mHeight:I

.field private mLPadding:I

.field private mRPadding:I

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mHeight:I

    .line 92
    iput v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mLPadding:I

    .line 93
    iput v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mRPadding:I

    const/high16 p1, -0x1000000

    .line 94
    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mColour:I

    return-void
.end method


# virtual methods
.method public build()Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;
    .locals 7

    .line 212
    new-instance v6, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;

    iget v1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mHeight:I

    iget v2, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mLPadding:I

    iget v3, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mRPadding:I

    iget v4, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mColour:I

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;-><init>(IIIILca/barrenechea/widget/recyclerview/decoration/DividerDecoration$1;)V

    return-object v6
.end method

.method public setColor(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 0

    .line 202
    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mColour:I

    return-object p0
.end method

.method public setColorResource(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 1

    .line 191
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->setColor(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;

    return-object p0
.end method

.method public setHeight(F)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 2

    .line 103
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mHeight:I

    return-object p0
.end method

.method public setHeight(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 1

    .line 114
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mHeight:I

    return-object p0
.end method

.method public setLeftPadding(F)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 2

    .line 147
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mLPadding:I

    return-object p0
.end method

.method public setLeftPadding(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 1

    .line 169
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mLPadding:I

    return-object p0
.end method

.method public setPadding(F)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 0

    .line 124
    invoke-virtual {p0, p1}, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->setLeftPadding(F)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;

    .line 125
    invoke-virtual {p0, p1}, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->setRightPadding(F)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;

    return-object p0
.end method

.method public setPadding(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 0

    .line 136
    invoke-virtual {p0, p1}, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->setLeftPadding(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;

    .line 137
    invoke-virtual {p0, p1}, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->setRightPadding(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;

    return-object p0
.end method

.method public setRightPadding(F)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 2

    .line 158
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mRPadding:I

    return-object p0
.end method

.method public setRightPadding(I)Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    .locals 1

    .line 180
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;->mRPadding:I

    return-object p0
.end method
