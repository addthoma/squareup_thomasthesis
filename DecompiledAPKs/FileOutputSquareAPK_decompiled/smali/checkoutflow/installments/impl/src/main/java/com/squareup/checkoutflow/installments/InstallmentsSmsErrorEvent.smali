.class public final Lcheckoutflow/installments/impl/src/main/java/com/squareup/checkoutflow/installments/InstallmentsSmsErrorEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "InstallmentsErrorEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcheckoutflow/installments/impl/src/main/java/com/squareup/checkoutflow/installments/InstallmentsSmsErrorEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "response_code",
        "",
        "(I)V",
        "getResponse_code",
        "()I",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final response_code:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .line 14
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTALLMENTS_SMS_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterErrorName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput p1, p0, Lcheckoutflow/installments/impl/src/main/java/com/squareup/checkoutflow/installments/InstallmentsSmsErrorEvent;->response_code:I

    return-void
.end method


# virtual methods
.method public final getResponse_code()I
    .locals 1

    .line 13
    iget v0, p0, Lcheckoutflow/installments/impl/src/main/java/com/squareup/checkoutflow/installments/InstallmentsSmsErrorEvent;->response_code:I

    return v0
.end method
