.class public final Lcom/squareup/setupguide/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CollapsingHeaderView:[I

.field public static final CollapsingHeaderView_headerBottomMargin:I = 0x0

.field public static final CollapsingHeaderView_headerTopMargin:I = 0x1

.field public static final CollapsingHeaderView_overlay:I = 0x2

.field public static final CollapsingHeaderView_toolBarHeight:I = 0x3

.field public static final LabeledProgressBar:[I

.field public static final LabeledProgressBar_title:I = 0x0

.field public static final SetupGuideIntroCard:[I

.field public static final SetupGuideIntroCard_subtitle:I = 0x0

.field public static final SetupGuideIntroCard_title:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [I

    .line 124
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/setupguide/R$styleable;->CollapsingHeaderView:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f04045d

    aput v2, v0, v1

    .line 129
    sput-object v0, Lcom/squareup/setupguide/R$styleable;->LabeledProgressBar:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 131
    fill-array-data v0, :array_1

    sput-object v0, Lcom/squareup/setupguide/R$styleable;->SetupGuideIntroCard:[I

    return-void

    :array_0
    .array-data 4
        0x7f0401ae
        0x7f0401b1
        0x7f040304
        0x7f04046d
    .end array-data

    :array_1
    .array-data 4
        0x7f040402
        0x7f04045d
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
