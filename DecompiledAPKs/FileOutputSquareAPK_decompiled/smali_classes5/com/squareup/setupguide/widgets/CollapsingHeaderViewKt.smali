.class public final Lcom/squareup/setupguide/widgets/CollapsingHeaderViewKt;
.super Ljava/lang/Object;
.source "CollapsingHeaderView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0004\u00a8\u0006\u0007"
    }
    d2 = {
        "bottomCoordinate",
        "",
        "Landroid/view/View;",
        "getBottomCoordinate",
        "(Landroid/view/View;)I",
        "topCoordinate",
        "getTopCoordinate",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getBottomCoordinate$p(Landroid/view/View;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderViewKt;->getBottomCoordinate(Landroid/view/View;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getTopCoordinate$p(Landroid/view/View;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/setupguide/widgets/CollapsingHeaderViewKt;->getTopCoordinate(Landroid/view/View;)I

    move-result p0

    return p0
.end method

.method private static final getBottomCoordinate(Landroid/view/View;)I
    .locals 0

    .line 215
    invoke-static {p0}, Lcom/squareup/util/Views;->getLocationOfViewOnScreen(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object p0

    iget p0, p0, Landroid/graphics/Rect;->bottom:I

    return p0
.end method

.method private static final getTopCoordinate(Landroid/view/View;)I
    .locals 0

    .line 218
    invoke-static {p0}, Lcom/squareup/util/Views;->getLocationOfViewOnScreen(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object p0

    iget p0, p0, Landroid/graphics/Rect;->top:I

    return p0
.end method
