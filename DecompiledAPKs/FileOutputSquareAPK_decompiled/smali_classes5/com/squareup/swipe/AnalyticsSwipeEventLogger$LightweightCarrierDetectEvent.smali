.class Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "AnalyticsSwipeEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/swipe/AnalyticsSwipeEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LightweightCarrierDetectEvent"
.end annotation


# instance fields
.field final carrierdetectinfo_isearlypacket:Ljava/lang/Boolean;

.field final carrierdetectinfo_numsamples:Ljava/lang/Integer;

.field final signal_event:Ljava/lang/String;

.field final signalfound_classifiedlinktype:Ljava/lang/String;

.field final signalfound_decision:Ljava/lang/String;

.field final signalfound_expectedreadertype:Ljava/lang/String;

.field final signalfound_linktype:Ljava/lang/String;

.field final signalfound_packettype:Ljava/lang/String;

.field final signalfound_r4packet_r4carddata_counter:Ljava/lang/Long;

.field final signalfound_r4packet_r4carddata_entropy:Ljava/lang/Long;

.field final signalfound_readerhardwareid:Ljava/lang/String;

.field final signalfound_readertype:Ljava/lang/String;

.field final signalfound_track1readsuccess:Ljava/lang/Boolean;

.field final signalfound_track2readsuccess:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V
    .locals 3

    .line 48
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_CARRIER_DETECT:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v1}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 49
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signal_event:Ljava/lang/String;

    .line 50
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 52
    iget-object v2, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    iput-object v2, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->carrierdetectinfo_isearlypacket:Ljava/lang/Boolean;

    .line 53
    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->carrierdetectinfo_numsamples:Ljava/lang/Integer;

    goto :goto_0

    .line 55
    :cond_0
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->carrierdetectinfo_isearlypacket:Ljava/lang/Boolean;

    .line 56
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->carrierdetectinfo_numsamples:Ljava/lang/Integer;

    .line 59
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    if-eqz p1, :cond_2

    .line 61
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_classifiedlinktype:Ljava/lang/String;

    .line 62
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_decision:Ljava/lang/String;

    .line 63
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-static {v0}, Lcom/squareup/util/Objects;->nameOrNull(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_expectedreadertype:Ljava/lang/String;

    .line 64
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-static {v0}, Lcom/squareup/util/Objects;->nameOrNull(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_linktype:Ljava/lang/String;

    .line 65
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    invoke-static {v0}, Lcom/squareup/util/Objects;->nameOrNull(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_packettype:Ljava/lang/String;

    .line 66
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    if-eqz v0, :cond_1

    .line 67
    iget-object v2, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    if-eqz v2, :cond_1

    .line 68
    iget-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    iget-object v1, v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_r4packet_r4carddata_counter:Ljava/lang/Long;

    .line 69
    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_r4packet_r4carddata_entropy:Ljava/lang/Long;

    goto :goto_1

    .line 71
    :cond_1
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_r4packet_r4carddata_counter:Ljava/lang/Long;

    .line 72
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_r4packet_r4carddata_entropy:Ljava/lang/Long;

    .line 74
    :goto_1
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_readerhardwareid:Ljava/lang/String;

    .line 75
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_track1readsuccess:Ljava/lang/Boolean;

    .line 76
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_track2readsuccess:Ljava/lang/Boolean;

    .line 77
    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-static {p1}, Lcom/squareup/util/Objects;->nameOrNull(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_readertype:Ljava/lang/String;

    goto :goto_2

    .line 79
    :cond_2
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_classifiedlinktype:Ljava/lang/String;

    .line 80
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_decision:Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_expectedreadertype:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_linktype:Ljava/lang/String;

    .line 83
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_packettype:Ljava/lang/String;

    .line 84
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_r4packet_r4carddata_counter:Ljava/lang/Long;

    .line 85
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_r4packet_r4carddata_entropy:Ljava/lang/Long;

    .line 86
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_readerhardwareid:Ljava/lang/String;

    .line 87
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_track1readsuccess:Ljava/lang/Boolean;

    .line 88
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_track2readsuccess:Ljava/lang/Boolean;

    .line 89
    iput-object v1, p0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger$LightweightCarrierDetectEvent;->signalfound_readertype:Ljava/lang/String;

    :goto_2
    return-void
.end method
