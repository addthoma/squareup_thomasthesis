.class public final Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;
.super Ljava/lang/Object;
.source "RecorderErrorReporterListener_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/swipe/RecorderErrorReporterListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;"
        }
    .end annotation
.end field

.field private final telephonyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->telephonyManagerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->headsetProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;)",
            "Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/telephony/TelephonyManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/swipe/RecorderErrorReporterListener;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/swipe/RecorderErrorReporterListener;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/swipe/RecorderErrorReporterListener;-><init>(Landroid/telephony/TelephonyManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/Headset;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/swipe/RecorderErrorReporterListener;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->telephonyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->headsetProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/wavpool/swipe/Headset;

    invoke-static {v0, v1, v2}, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->newInstance(Landroid/telephony/TelephonyManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/swipe/RecorderErrorReporterListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->get()Lcom/squareup/swipe/RecorderErrorReporterListener;

    move-result-object v0

    return-object v0
.end method
