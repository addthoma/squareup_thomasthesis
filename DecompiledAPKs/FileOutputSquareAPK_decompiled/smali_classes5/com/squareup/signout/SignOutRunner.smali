.class public Lcom/squareup/signout/SignOutRunner;
.super Ljava/lang/Object;
.source "SignOutRunner.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;,
        Lcom/squareup/signout/SignOutRunner$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSignOutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SignOutRunner.kt\ncom/squareup/signout/SignOutRunner\n*L\n1#1,157:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0008\u0017\u0018\u00002\u00020\u0001:\u0002/0B}\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u001d\u00a2\u0006\u0002\u0010\u001fJ\u0008\u0010%\u001a\u00020&H\u0002J\u0008\u0010\'\u001a\u00020&H\u0016J\u000e\u0010(\u001a\u00020&2\u0006\u0010)\u001a\u00020*J\u0008\u0010+\u001a\u00020&H\u0002J\u0018\u0010+\u001a\u00020&2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020-H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010 \u001a\u00020\u001e8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008#\u0010$\u001a\u0004\u0008!\u0010\"R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/signout/SignOutRunner;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "authenticator",
        "Lcom/squareup/account/LegacyAuthenticator;",
        "openTicketsSettings",
        "Lcom/squareup/tickets/OpenTicketsSettings;",
        "pendingTransactionsStore",
        "Lcom/squareup/payment/pending/PendingTransactionsStore;",
        "tickets",
        "Lcom/squareup/tickets/Tickets;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "employeeManagementModeDecider",
        "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
        "unsyncedOpenTicketsSpinner",
        "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
        "cashManagementSettings",
        "Lcom/squareup/cashmanagement/CashManagementSettings;",
        "cashDrawerShiftManager",
        "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
        "signOutTaskStackUpdater",
        "Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "lazyFlow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "(Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/tickets/Tickets;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;Lcom/squareup/settings/server/Features;Ldagger/Lazy;)V",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "flow$delegate",
        "Ldagger/Lazy;",
        "attemptShowSignoutDialog",
        "",
        "attemptSignout",
        "doSignOut",
        "context",
        "Landroid/content/Context;",
        "showSignOutDialog",
        "signOutTitle",
        "",
        "signOutMessage",
        "ParentComponent",
        "SignOutTaskStackUpdater",
        "sign-out_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow$delegate:Ldagger/Lazy;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field private final signOutTaskStackUpdater:Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final unsyncedOpenTicketsSpinner:Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/signout/SignOutRunner;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "flow"

    const-string v4, "getFlow()Lflow/Flow;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/signout/SignOutRunner;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/tickets/Tickets;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;Lcom/squareup/settings/server/Features;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/account/LegacyAuthenticator;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            "Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;",
            "Lcom/squareup/settings/server/Features;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticator"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicketsSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pendingTransactionsStore"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tickets"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagementModeDecider"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unsyncedOpenTicketsSpinner"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashManagementSettings"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashDrawerShiftManager"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signOutTaskStackUpdater"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyFlow"

    invoke-static {p14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/signout/SignOutRunner;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/signout/SignOutRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/signout/SignOutRunner;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    iput-object p4, p0, Lcom/squareup/signout/SignOutRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    iput-object p5, p0, Lcom/squareup/signout/SignOutRunner;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    iput-object p6, p0, Lcom/squareup/signout/SignOutRunner;->tickets:Lcom/squareup/tickets/Tickets;

    iput-object p7, p0, Lcom/squareup/signout/SignOutRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p8, p0, Lcom/squareup/signout/SignOutRunner;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iput-object p9, p0, Lcom/squareup/signout/SignOutRunner;->unsyncedOpenTicketsSpinner:Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

    iput-object p10, p0, Lcom/squareup/signout/SignOutRunner;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    iput-object p11, p0, Lcom/squareup/signout/SignOutRunner;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    iput-object p12, p0, Lcom/squareup/signout/SignOutRunner;->signOutTaskStackUpdater:Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;

    iput-object p13, p0, Lcom/squareup/signout/SignOutRunner;->features:Lcom/squareup/settings/server/Features;

    .line 62
    iput-object p14, p0, Lcom/squareup/signout/SignOutRunner;->flow$delegate:Ldagger/Lazy;

    return-void
.end method

.method public static final synthetic access$attemptShowSignoutDialog(Lcom/squareup/signout/SignOutRunner;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/signout/SignOutRunner;->attemptShowSignoutDialog()V

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/signout/SignOutRunner;)Lcom/squareup/util/Res;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/signout/SignOutRunner;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getUnsyncedOpenTicketsSpinner$p(Lcom/squareup/signout/SignOutRunner;)Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/signout/SignOutRunner;->unsyncedOpenTicketsSpinner:Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

    return-object p0
.end method

.method public static final synthetic access$showSignOutDialog(Lcom/squareup/signout/SignOutRunner;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/signout/SignOutRunner;->showSignOutDialog()V

    return-void
.end method

.method public static final synthetic access$showSignOutDialog(Lcom/squareup/signout/SignOutRunner;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/signout/SignOutRunner;->showSignOutDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final attemptShowSignoutDialog()V
    .locals 4

    .line 99
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    invoke-interface {v0}, Lcom/squareup/payment/pending/PendingTransactionsStore;->smoothedAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$totalPendingPayments$1;->INSTANCE:Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$totalPendingPayments$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrProvideDefault(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v0

    .line 98
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-lez v0, :cond_1

    if-le v0, v1, :cond_0

    .line 103
    sget v0, Lcom/squareup/signout/R$string;->cannot_sign_out_message_plural:I

    goto :goto_0

    .line 105
    :cond_0
    sget v0, Lcom/squareup/signout/R$string;->cannot_sign_out_message:I

    .line 107
    :goto_0
    invoke-direct {p0}, Lcom/squareup/signout/SignOutRunner;->getFlow()Lflow/Flow;

    move-result-object v1

    new-instance v2, Lcom/squareup/signout/PendingPaymentsDialogScreen;

    iget-object v3, p0, Lcom/squareup/signout/SignOutRunner;->res:Lcom/squareup/util/Res;

    invoke-interface {v3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/squareup/signout/PendingPaymentsDialogScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementAllowed()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->cashManagementEnabledAndIsOpenShift()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 116
    invoke-direct {p0}, Lcom/squareup/signout/SignOutRunner;->getFlow()Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;->INSTANCE:Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAllowed()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v2, Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$1;

    invoke-direct {v2, p0}, Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$1;-><init>(Lcom/squareup/signout/SignOutRunner;)V

    check-cast v2, Lcom/squareup/tickets/TicketsCallback;

    invoke-interface {v0, v2}, Lcom/squareup/tickets/Tickets;->getUnsyncedTicketCount(Lcom/squareup/tickets/TicketsCallback;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->unsyncedOpenTicketsSpinner:Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

    invoke-interface {v0, v1}, Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;->showSpinner(Z)V

    return-void

    .line 139
    :cond_3
    invoke-direct {p0}, Lcom/squareup/signout/SignOutRunner;->showSignOutDialog()V

    return-void
.end method

.method private final getFlow()Lflow/Flow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->flow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/signout/SignOutRunner;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method

.method private final showSignOutDialog()V
    .locals 3

    .line 143
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/signout/R$string;->sign_out:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/signout/SignOutRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/signout/R$string;->sign_out_confirm:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/signout/SignOutRunner;->showSignOutDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final showSignOutDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 150
    invoke-direct {p0}, Lcom/squareup/signout/SignOutRunner;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/signout/SignoutDialogScreen;

    invoke-direct {v1, p1, p2}, Lcom/squareup/signout/SignoutDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public attemptSignout()V
    .locals 3

    .line 70
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 72
    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_1

    .line 74
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/permissions/Permission;->ALLOW_APP_SIGN_OUT:Lcom/squareup/permissions/Permission;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    .line 75
    :goto_0
    iget-object v1, p0, Lcom/squareup/signout/SignOutRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v2, Lcom/squareup/signout/SignOutRunner$attemptSignout$1;

    invoke-direct {v2, p0}, Lcom/squareup/signout/SignOutRunner$attemptSignout$1;-><init>(Lcom/squareup/signout/SignOutRunner;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_1

    .line 81
    :cond_1
    invoke-direct {p0}, Lcom/squareup/signout/SignOutRunner;->attemptShowSignoutDialog()V

    :goto_1
    return-void
.end method

.method public final doSignOut(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner;->signOutTaskStackUpdater:Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;

    invoke-static {p1}, Lcom/squareup/util/Contexts;->getActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {v0, p1}, Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;->updateTaskStack(Landroid/app/Activity;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/signout/SignOutRunner;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {p1}, Lcom/squareup/account/LegacyAuthenticator;->logOut()V

    .line 93
    iget-object p1, p0, Lcom/squareup/signout/SignOutRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->LOG_OUT:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void

    .line 89
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Expecting the Context to be an ancestor of an Activity"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
