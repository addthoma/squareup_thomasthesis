.class public final Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;
.super Ljava/lang/Object;
.source "NoSingleSignOnModule_ProvideSingleSignOnFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/singlesignon/SingleSignOn;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory$InstanceHolder;->access$000()Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSingleSignOn()Lcom/squareup/singlesignon/SingleSignOn;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/singlesignon/NoSingleSignOnModule;->provideSingleSignOn()Lcom/squareup/singlesignon/SingleSignOn;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/singlesignon/SingleSignOn;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/singlesignon/SingleSignOn;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;->provideSingleSignOn()Lcom/squareup/singlesignon/SingleSignOn;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;->get()Lcom/squareup/singlesignon/SingleSignOn;

    move-result-object v0

    return-object v0
.end method
