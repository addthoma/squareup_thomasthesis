.class public final enum Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;
.super Ljava/lang/Enum;
.source "CardDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/CardDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DismissGlyph"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

.field public static final enum BACK:Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

.field public static final enum CLOSE:Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;


# instance fields
.field private glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 28
    new-instance v0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    const-string v3, "CLOSE"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->CLOSE:Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    .line 29
    new-instance v0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x1

    const-string v4, "BACK"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->BACK:Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    .line 27
    sget-object v1, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->CLOSE:Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->BACK:Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->$VALUES:[Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            ")V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-object p3, p0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;
    .locals 1

    .line 27
    const-class v0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->$VALUES:[Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    invoke-virtual {v0}, [Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;

    return-object v0
.end method
