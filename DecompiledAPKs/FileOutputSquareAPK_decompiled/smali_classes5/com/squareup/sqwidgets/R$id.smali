.class public final Lcom/squareup/sqwidgets/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CUSTOMERS:I = 0x7f0a0055

.field public static final HELP_CENTER:I = 0x7f0a0063

.field public static final INVOICES:I = 0x7f0a0082

.field public static final ITEMS:I = 0x7f0a0083

.field public static final REGISTER:I = 0x7f0a00b3

.field public static final REPORTS:I = 0x7f0a00b5

.field public static final SALES_HISTORY:I = 0x7f0a00bd

.field public static final SETTINGS:I = 0x7f0a00c0

.field public static final action_button:I = 0x7f0a013f

.field public static final am_pm_picker:I = 0x7f0a01b6

.field public static final confirm_button:I = 0x7f0a038b

.field public static final content_view:I = 0x7f0a03a7

.field public static final dialog_icon:I = 0x7f0a05b5

.field public static final dismiss_button:I = 0x7f0a05d6

.field public static final edit_text_password_contents:I = 0x7f0a067d

.field public static final hour_minute_divider:I = 0x7f0a07e5

.field public static final hour_picker:I = 0x7f0a07e6

.field public static final market_date_picker:I = 0x7f0a09ab

.field public static final message_text:I = 0x7f0a09d8

.field public static final minute_picker:I = 0x7f0a09de

.field public static final overlay:I = 0x7f0a0b77

.field public static final overlay_bottom:I = 0x7f0a0b78

.field public static final overlay_middle:I = 0x7f0a0b79

.field public static final overlay_top:I = 0x7f0a0b7a

.field public static final parent_panel:I = 0x7f0a0bb0

.field public static final password_field:I = 0x7f0a0bd3

.field public static final password_show:I = 0x7f0a0bd4

.field public static final status_bar_overlay:I = 0x7f0a0f35

.field public static final time_picker_layout:I = 0x7f0a0fe1

.field public static final title_text:I = 0x7f0a1047

.field public static final title_text_view:I = 0x7f0a1048


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
