.class final Lcom/squareup/sqwidgets/time/DataAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "DataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final CELL_SIZE:I = 0xa0


# instance fields
.field private final cellHeight:I

.field private final cellStyleResId:I

.field private final cellWidth:I

.field private final context:Landroid/content/Context;

.field private final data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final pageCount:I


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/Collection;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->context:Landroid/content/Context;

    .line 47
    iput p3, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->pageCount:I

    .line 48
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p3, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->data:Ljava/util/List;

    .line 49
    iput p4, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->cellStyleResId:I

    const/4 p2, 0x2

    new-array p2, p2, [I

    .line 51
    fill-array-data p2, :array_0

    invoke-virtual {p1, p4, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 57
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/4 p3, 0x0

    const/16 p4, 0xa0

    .line 59
    invoke-virtual {p2, p3, p4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result p3

    int-to-float p3, p3

    div-float/2addr p3, p1

    float-to-int p3, p3

    iput p3, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->cellWidth:I

    const/4 p3, 0x1

    .line 60
    invoke-virtual {p2, p3, p4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result p3

    int-to-float p3, p3

    div-float/2addr p3, p1

    float-to-int p1, p3

    iput p1, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->cellHeight:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 63
    throw p1

    nop

    :array_0
    .array-data 4
        0x10100f4
        0x10100f5
    .end array-data
.end method

.method private valueForPosition(I)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_1

    .line 120
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/DataAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 124
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rem-int/2addr p1, v0

    .line 125
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_1
    :goto_0
    const-string p1, ""

    return-object p1
.end method


# virtual methods
.method public getData(Landroid/view/View;)Ljava/lang/String;
    .locals 0

    .line 104
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getItemCount()I
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->pageCount:I

    mul-int v0, v0, v1

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method getViewHeight()I
    .locals 1

    .line 96
    iget v0, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->cellHeight:I

    return v0
.end method

.method getViewWidth()I
    .locals 1

    .line 89
    iget v0, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->cellWidth:I

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/sqwidgets/time/DataAdapter;->onBindViewHolder(Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;I)V
    .locals 0

    .line 77
    invoke-direct {p0, p2}, Lcom/squareup/sqwidgets/time/DataAdapter;->valueForPosition(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/squareup/sqwidgets/time/DataAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;
    .locals 3

    .line 67
    new-instance p2, Lcom/squareup/marketfont/MarketTextView;

    iget-object v0, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->context:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    .line 68
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->cellWidth:I

    iget v2, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->cellHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v0, 0x11

    .line 69
    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 70
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    iget v0, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->cellStyleResId:I

    invoke-virtual {p2, p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 71
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    const/4 p1, 0x2

    .line 72
    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setImportantForAccessibility(I)V

    .line 73
    new-instance p1, Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/sqwidgets/time/DataAdapter$ViewHolder;-><init>(Landroid/widget/TextView;)V

    return-object p1
.end method

.method positionOf(Ljava/lang/String;)I
    .locals 2

    .line 114
    iget v0, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->pageCount:I

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->data:Ljava/util/List;

    .line 115
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/squareup/sqwidgets/time/DataAdapter;->data:Ljava/util/List;

    .line 116
    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method
