.class final Lcom/squareup/sqwidgets/time/SnappingScrollListener;
.super Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;
.source "SnappingScrollListener.java"


# instance fields
.field private final layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;


# direct methods
.method constructor <init>(Landroidx/recyclerview/widget/LinearLayoutManager;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/sqwidgets/time/SnappingScrollListener;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    return-void
.end method

.method private static centerForView(Landroid/view/View;)I
    .locals 1

    .line 67
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result p0

    add-int/2addr v0, p0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private findClosestView(I)Landroid/view/View;
    .locals 5

    .line 48
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/SnappingScrollListener;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v0

    const v1, 0x7fffffff

    const/4 v2, 0x0

    .line 49
    :goto_0
    iget-object v3, p0, Lcom/squareup/sqwidgets/time/SnappingScrollListener;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v3

    if-gt v0, v3, :cond_1

    .line 50
    iget-object v3, p0, Lcom/squareup/sqwidgets/time/SnappingScrollListener;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v3, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v3

    .line 51
    invoke-static {v3}, Lcom/squareup/sqwidgets/time/SnappingScrollListener;->centerForView(Landroid/view/View;)I

    move-result v4

    sub-int/2addr v4, p1

    .line 52
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-gt v4, v1, :cond_0

    move-object v2, v3

    move v1, v4

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method


# virtual methods
.method public onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V
    .locals 1

    .line 23
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;->onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V

    if-nez p2, :cond_0

    .line 28
    invoke-static {p1}, Lcom/squareup/sqwidgets/time/SnappingScrollListener;->centerForView(Landroid/view/View;)I

    move-result p2

    .line 31
    invoke-direct {p0, p2}, Lcom/squareup/sqwidgets/time/SnappingScrollListener;->findClosestView(I)Landroid/view/View;

    move-result-object v0

    .line 32
    invoke-static {v0}, Lcom/squareup/sqwidgets/time/SnappingScrollListener;->centerForView(Landroid/view/View;)I

    move-result v0

    sub-int/2addr v0, p2

    const/4 p2, 0x0

    .line 37
    invoke-virtual {p1, p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollBy(II)V

    :cond_0
    return-void
.end method
