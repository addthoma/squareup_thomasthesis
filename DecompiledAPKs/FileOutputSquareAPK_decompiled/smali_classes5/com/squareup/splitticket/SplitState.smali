.class public interface abstract Lcom/squareup/splitticket/SplitState;
.super Ljava/lang/Object;
.source "SplitState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\n\u0008f\u0018\u00002\u00020\u0001J\u0008\u00104\u001a\u00020\u0014H&J\u0008\u00105\u001a\u000206H&J\u0008\u00107\u001a\u00020\u0014H&J\u0008\u00108\u001a\u00020\u0014H&J\u0008\u00109\u001a\u00020\u0014H&J\u0010\u0010:\u001a\u00020\u00142\u0006\u0010;\u001a\u00020!H&J\u0010\u0010<\u001a\u00020\u00142\u0006\u0010;\u001a\u00020!H&J\u000e\u0010=\u001a\u0008\u0012\u0004\u0012\u00020?0>H&J\u0008\u0010@\u001a\u00020\u0014H&J\u0008\u0010A\u001a\u00020\u0014H&J\u0010\u0010B\u001a\u00020\u00142\u0006\u0010C\u001a\u00020\u0014H&J\u0008\u0010D\u001a\u00020\u0014H&J\u0010\u0010E\u001a\u00020\u00142\u0006\u0010F\u001a\u00020!H&J\u0010\u0010G\u001a\u00020\u00142\u0006\u0010F\u001a\u00020!H&J&\u0010H\u001a\u00020?2\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u000e2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u000e2\u0008\u0010$\u001a\u0004\u0018\u00010%H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005R\u0018\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u0004\u0018\u00010\u000eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0005R\u0012\u0010\u0013\u001a\u00020\u0014X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R\u0018\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u000cR\u0014\u0010\u001a\u001a\u0004\u0018\u00010\u000eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u0010R\u0012\u0010\u001c\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u0005R\u0014\u0010\u001e\u001a\u0004\u0018\u00010\u000eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001f\u0010\u0010R\u0012\u0010 \u001a\u00020!X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010#R\u0014\u0010$\u001a\u0004\u0018\u00010%X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008&\u0010\'R\u0012\u0010(\u001a\u00020!X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010#R\u0012\u0010*\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008+\u0010\u0005R\u0012\u0010,\u001a\u00020!X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008-\u0010#R\u0012\u0010.\u001a\u00020\u000eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008/\u0010\u0010R\u0012\u00100\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u00081\u0010\u0005R\u0012\u00102\u001a\u00020!X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u00083\u0010#\u00a8\u0006I"
    }
    d2 = {
        "Lcom/squareup/splitticket/SplitState;",
        "",
        "additionalTaxAmount",
        "Lcom/squareup/protos/common/Money;",
        "getAdditionalTaxAmount",
        "()Lcom/squareup/protos/common/Money;",
        "automaticGratuityAmount",
        "getAutomaticGratuityAmount",
        "cartAmountDiscounts",
        "",
        "Lcom/squareup/checkout/Discount;",
        "getCartAmountDiscounts",
        "()Ljava/util/List;",
        "cartDiningOptionText",
        "",
        "getCartDiningOptionText",
        "()Ljava/lang/String;",
        "compAmount",
        "getCompAmount",
        "hasBeenSaved",
        "",
        "getHasBeenSaved",
        "()Z",
        "items",
        "Lcom/squareup/checkout/CartItem;",
        "getItems",
        "name",
        "getName",
        "negativePerItemDiscountsAmount",
        "getNegativePerItemDiscountsAmount",
        "note",
        "getNote",
        "ordinal",
        "",
        "getOrdinal",
        "()I",
        "predefinedTicket",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "getPredefinedTicket",
        "()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "selectedEntriesCount",
        "getSelectedEntriesCount",
        "subtotalAmount",
        "getSubtotalAmount",
        "taxTypeId",
        "getTaxTypeId",
        "ticketId",
        "getTicketId",
        "totalAmount",
        "getTotalAmount",
        "viewIndex",
        "getViewIndex",
        "autoGratuityIsTaxable",
        "getHoldsCustomer",
        "Lcom/squareup/payment/crm/HoldsCustomer;",
        "hasCartAmountDiscounts",
        "hasItems",
        "hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem",
        "isDiscountSelected",
        "positionToCheck",
        "isItemSelected",
        "onCustomerChanged",
        "Lrx/Observable;",
        "",
        "shouldShowAutoGratuityRow",
        "shouldShowComps",
        "shouldShowPerItemDiscounts",
        "includeComps",
        "shouldShowTaxRow",
        "toggleDiscountSelection",
        "position",
        "toggleItemSelection",
        "updateTicketDetails",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract autoGratuityIsTaxable()Z
.end method

.method public abstract getAdditionalTaxAmount()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getAutomaticGratuityAmount()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getCartAmountDiscounts()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCartDiningOptionText()Ljava/lang/String;
.end method

.method public abstract getCompAmount()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getHasBeenSaved()Z
.end method

.method public abstract getHoldsCustomer()Lcom/squareup/payment/crm/HoldsCustomer;
.end method

.method public abstract getItems()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNegativePerItemDiscountsAmount()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getNote()Ljava/lang/String;
.end method

.method public abstract getOrdinal()I
.end method

.method public abstract getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
.end method

.method public abstract getSelectedEntriesCount()I
.end method

.method public abstract getSubtotalAmount()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getTaxTypeId()I
.end method

.method public abstract getTicketId()Ljava/lang/String;
.end method

.method public abstract getTotalAmount()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getViewIndex()I
.end method

.method public abstract hasCartAmountDiscounts()Z
.end method

.method public abstract hasItems()Z
.end method

.method public abstract hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z
.end method

.method public abstract isDiscountSelected(I)Z
.end method

.method public abstract isItemSelected(I)Z
.end method

.method public abstract onCustomerChanged()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract shouldShowAutoGratuityRow()Z
.end method

.method public abstract shouldShowComps()Z
.end method

.method public abstract shouldShowPerItemDiscounts(Z)Z
.end method

.method public abstract shouldShowTaxRow()Z
.end method

.method public abstract toggleDiscountSelection(I)Z
.end method

.method public abstract toggleItemSelection(I)Z
.end method

.method public abstract updateTicketDetails(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
.end method
