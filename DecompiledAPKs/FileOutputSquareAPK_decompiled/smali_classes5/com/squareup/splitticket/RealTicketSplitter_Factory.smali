.class public final Lcom/squareup/splitticket/RealTicketSplitter_Factory;
.super Ljava/lang/Object;
.source "RealTicketSplitter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/splitticket/RealTicketSplitter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/SplitTransactionHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/SplitTransactionHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p2, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 51
    iput-object p3, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p5, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p6, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 55
    iput-object p7, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 56
    iput-object p8, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 57
    iput-object p9, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 58
    iput-object p10, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg9Provider:Ljavax/inject/Provider;

    .line 59
    iput-object p11, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg10Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/splitticket/RealTicketSplitter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/SplitTransactionHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;)",
            "Lcom/squareup/splitticket/RealTicketSplitter_Factory;"
        }
    .end annotation

    .line 74
    new-instance v12, Lcom/squareup/splitticket/RealTicketSplitter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/splitticket/RealTicketSplitter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/BundleKey;Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;Lcom/squareup/splitticket/PricingEngineServiceWrapper;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/tickets/Tickets;Lcom/squareup/splitticket/SplitTransactionHandler;Lcom/squareup/print/PrinterStations;)Lcom/squareup/splitticket/RealTicketSplitter;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;",
            "Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;",
            "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;>;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/splitticket/SplitTransactionHandler;",
            "Lcom/squareup/print/PrinterStations;",
            ")",
            "Lcom/squareup/splitticket/RealTicketSplitter;"
        }
    .end annotation

    .line 81
    new-instance v12, Lcom/squareup/splitticket/RealTicketSplitter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/splitticket/RealTicketSplitter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/BundleKey;Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;Lcom/squareup/splitticket/PricingEngineServiceWrapper;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/tickets/Tickets;Lcom/squareup/splitticket/SplitTransactionHandler;Lcom/squareup/print/PrinterStations;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/splitticket/RealTicketSplitter;
    .locals 12

    .line 64
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/splitticket/PricingEngineServiceWrapper;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/tickets/Tickets;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/splitticket/SplitTransactionHandler;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/print/PrinterStations;

    invoke-static/range {v1 .. v11}, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/BundleKey;Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;Lcom/squareup/splitticket/PricingEngineServiceWrapper;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/tickets/Tickets;Lcom/squareup/splitticket/SplitTransactionHandler;Lcom/squareup/print/PrinterStations;)Lcom/squareup/splitticket/RealTicketSplitter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter_Factory;->get()Lcom/squareup/splitticket/RealTicketSplitter;

    move-result-object v0

    return-object v0
.end method
