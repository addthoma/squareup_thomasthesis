.class public interface abstract Lcom/squareup/splitticket/PricingEngineServiceWrapper;
.super Ljava/lang/Object;
.source "PricingEngineServiceWrapper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
        "",
        "rulesForOrder",
        "Lrx/Single;",
        "Lcom/squareup/prices/PricingEngineServiceResult;",
        "order",
        "Lcom/squareup/payment/Order;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract rulesForOrder(Lcom/squareup/payment/Order;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/prices/PricingEngineServiceResult;",
            ">;"
        }
    .end annotation
.end method
