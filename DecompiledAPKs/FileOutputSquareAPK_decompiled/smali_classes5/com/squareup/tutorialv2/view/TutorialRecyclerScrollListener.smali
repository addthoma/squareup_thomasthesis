.class public final Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;
.super Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;
.source "TutorialRecyclerScrollListener.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u00a2\u0006\u0002\u0010\tJ \u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u0003H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;",
        "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;",
        "position",
        "",
        "onItemVisible",
        "Lkotlin/Function0;",
        "",
        "condition",
        "",
        "(ILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V",
        "delegate",
        "Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;",
        "lastFirstVisible",
        "lastItemCount",
        "lastVisibleCount",
        "onScrolled",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "dx",
        "dy",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final delegate:Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;

.field private lastFirstVisible:I

.field private lastItemCount:I

.field private lastVisibleCount:I


# direct methods
.method public constructor <init>(ILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onItemVisible"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "condition"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 17
    new-instance v0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;-><init>(ILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->delegate:Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;

    const/4 p1, -0x1

    .line 18
    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastFirstVisible:I

    .line 19
    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastVisibleCount:I

    .line 20
    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastItemCount:I

    return-void
.end method


# virtual methods
.method public onScrolled(Landroidx/recyclerview/widget/RecyclerView;II)V
    .locals 1

    const-string p2, "recyclerView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object p2

    if-eqz p2, :cond_3

    check-cast p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 29
    invoke-virtual {p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result p3

    .line 30
    invoke-virtual {p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result p2

    sub-int p2, p3, p2

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    .line 31
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 35
    :goto_0
    iget v0, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastFirstVisible:I

    if-ne p3, v0, :cond_1

    iget v0, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastVisibleCount:I

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastItemCount:I

    if-eq p1, v0, :cond_2

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->delegate:Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;

    invoke-virtual {v0, p3, p2, p1}, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->onScroll(III)V

    .line 37
    iput p3, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastFirstVisible:I

    .line 38
    iput p2, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastVisibleCount:I

    .line 39
    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialRecyclerScrollListener;->lastItemCount:I

    :cond_2
    return-void

    .line 27
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
