.class public final Lcom/squareup/tutorialv2/TutorialState$Companion;
.super Ljava/lang/Object;
.source "TutorialState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0012\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0001\u0010\n\u001a\u00020\u0007H\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialState$Companion;",
        "",
        "()V",
        "CLEAR",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "FINISHED",
        "HIDE_BUTTON",
        "",
        "display",
        "Lcom/squareup/tutorialv2/TutorialState$Builder;",
        "content",
        "",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 314
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 331
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->Companion:Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "content"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->Companion:Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    return-object p1
.end method
