.class final Lcom/squareup/thread/RxSchedulers$1;
.super Lio/reactivex/Scheduler;
.source "RxSchedulers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/thread/RxSchedulers;->mainThread()Lio/reactivex/Scheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;
    }
.end annotation


# instance fields
.field private final handler:Landroid/os/Handler;

.field private final looper:Landroid/os/Looper;


# direct methods
.method constructor <init>()V
    .locals 2

    .line 15
    invoke-direct {p0}, Lio/reactivex/Scheduler;-><init>()V

    .line 16
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/thread/RxSchedulers$1;->looper:Landroid/os/Looper;

    .line 17
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/thread/RxSchedulers$1;->looper:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/squareup/thread/RxSchedulers$1;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/thread/RxSchedulers$1;Ljava/lang/Runnable;J)Z
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/thread/RxSchedulers$1;->runIfApplicable(Ljava/lang/Runnable;J)Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/thread/RxSchedulers$1;)Landroid/os/Handler;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/thread/RxSchedulers$1;->handler:Landroid/os/Handler;

    return-object p0
.end method

.method private runIfApplicable(Ljava/lang/Runnable;J)Z
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-nez v2, :cond_0

    .line 20
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/thread/RxSchedulers$1;->looper:Landroid/os/Looper;

    if-ne p2, p3, :cond_0

    .line 22
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 24
    invoke-static {p1}, Lio/reactivex/plugins/RxJavaPlugins;->onError(Ljava/lang/Throwable;)V

    :goto_0
    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public createWorker()Lio/reactivex/Scheduler$Worker;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/thread/RxSchedulers$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/thread/RxSchedulers$1$1;-><init>(Lcom/squareup/thread/RxSchedulers$1;)V

    return-object v0
.end method

.method public scheduleDirect(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/disposables/Disposable;
    .locals 1

    if-eqz p1, :cond_2

    if-eqz p4, :cond_1

    .line 36
    invoke-static {p1}, Lio/reactivex/plugins/RxJavaPlugins;->onSchedule(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object p1

    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/thread/RxSchedulers$1;->runIfApplicable(Ljava/lang/Runnable;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1

    .line 43
    :cond_0
    new-instance v0, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;

    invoke-direct {v0, p0, p1}, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;-><init>(Lcom/squareup/thread/RxSchedulers$1;Ljava/lang/Runnable;)V

    .line 44
    iget-object p1, p0, Lcom/squareup/thread/RxSchedulers$1;->handler:Landroid/os/Handler;

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p2

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-object v0

    .line 34
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string/jumbo p2, "unit == null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 33
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "run == null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
