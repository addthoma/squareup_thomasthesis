.class public abstract Lcom/squareup/thread/FileThreadModule;
.super Ljava/lang/Object;
.source "FileThreadModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/FileThreadModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H!\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/thread/FileThreadModule;",
        "",
        "()V",
        "bindFileThread",
        "Ljava/lang/Thread;",
        "handlerThread",
        "Landroid/os/HandlerThread;",
        "Companion",
        "impl-file-thread-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/thread/FileThreadModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/thread/FileThreadModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/thread/FileThreadModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/thread/FileThreadModule;->Companion:Lcom/squareup/thread/FileThreadModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideFileHandlerThread(Lcom/squareup/thread/FileThreadHolder;)Landroid/os/HandlerThread;
    .locals 1
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/thread/FileThreadModule;->Companion:Lcom/squareup/thread/FileThreadModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/thread/FileThreadModule$Companion;->provideFileHandlerThread(Lcom/squareup/thread/FileThreadHolder;)Landroid/os/HandlerThread;

    move-result-object p0

    return-object p0
.end method

.method public static final provideFilethreadEnforcer(Lcom/squareup/thread/FileThreadHolder;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 1
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/thread/FileThreadModule;->Companion:Lcom/squareup/thread/FileThreadModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/thread/FileThreadModule$Companion;->provideFilethreadEnforcer(Lcom/squareup/thread/FileThreadHolder;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract bindFileThread(Landroid/os/HandlerThread;)Ljava/lang/Thread;
    .param p1    # Landroid/os/HandlerThread;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
