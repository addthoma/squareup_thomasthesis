.class public final Lcom/squareup/thread/Rx1SchedulerModule;
.super Ljava/lang/Object;
.source "Rx1SchedulerModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0001J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0001J\u0008\u0010\u0008\u001a\u00020\u0004H\u0001J\u0008\u0010\t\u001a\u00020\u0004H\u0001\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/thread/Rx1SchedulerModule;",
        "",
        "()V",
        "provideComputationScheduler",
        "Lrx/Scheduler;",
        "provideFileThreadScheduler",
        "scheduler",
        "Lcom/squareup/thread/Rx1FileScheduler;",
        "provideMainScheduler",
        "provideRpcScheduler",
        "impl-rx1-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/thread/Rx1SchedulerModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/thread/Rx1SchedulerModule;

    invoke-direct {v0}, Lcom/squareup/thread/Rx1SchedulerModule;-><init>()V

    sput-object v0, Lcom/squareup/thread/Rx1SchedulerModule;->INSTANCE:Lcom/squareup/thread/Rx1SchedulerModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideComputationScheduler()Lrx/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Computation;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 11
    sget-object v0, Lcom/squareup/thread/Rx1Schedulers;->INSTANCE:Lcom/squareup/thread/Rx1Schedulers;

    invoke-virtual {v0}, Lcom/squareup/thread/Rx1Schedulers;->getComputation()Lrx/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public static final provideFileThreadScheduler(Lcom/squareup/thread/Rx1FileScheduler;)Lrx/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "scheduler"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0}, Lcom/squareup/thread/Rx1FileScheduler;->getFile()Lrx/Scheduler;

    move-result-object p0

    return-object p0
.end method

.method public static final provideMainScheduler()Lrx/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/thread/Rx1Schedulers;->INSTANCE:Lcom/squareup/thread/Rx1Schedulers;

    invoke-virtual {v0}, Lcom/squareup/thread/Rx1Schedulers;->getMain()Lrx/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public static final provideRpcScheduler()Lrx/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Rpc;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 19
    sget-object v0, Lcom/squareup/thread/Rx1Schedulers;->INSTANCE:Lcom/squareup/thread/Rx1Schedulers;

    invoke-virtual {v0}, Lcom/squareup/thread/Rx1Schedulers;->getIo()Lrx/Scheduler;

    move-result-object v0

    return-object v0
.end method
