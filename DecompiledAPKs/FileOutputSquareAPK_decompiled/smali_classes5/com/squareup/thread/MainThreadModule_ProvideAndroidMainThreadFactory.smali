.class public final Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory;
.super Ljava/lang/Object;
.source "MainThreadModule_ProvideAndroidMainThreadFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/thread/executor/MainThread;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory$InstanceHolder;->access$000()Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideAndroidMainThread()Lcom/squareup/thread/executor/MainThread;
    .locals 2

    .line 27
    sget-object v0, Lcom/squareup/thread/MainThreadModule;->INSTANCE:Lcom/squareup/thread/MainThreadModule;

    invoke-virtual {v0}, Lcom/squareup/thread/MainThreadModule;->provideAndroidMainThread()Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/thread/executor/MainThread;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory;->provideAndroidMainThread()Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory;->get()Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    return-object v0
.end method
