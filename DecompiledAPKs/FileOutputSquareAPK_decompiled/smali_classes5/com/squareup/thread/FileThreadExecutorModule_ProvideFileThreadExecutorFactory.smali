.class public final Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;
.super Ljava/lang/Object;
.source "FileThreadExecutorModule_ProvideFileThreadExecutorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/concurrent/Executor;",
        ">;"
    }
.end annotation


# instance fields
.field private final holderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/FileThreadHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/FileThreadHolder;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;->holderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/FileThreadHolder;",
            ">;)",
            "Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;

    invoke-direct {v0, p0}, Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFileThreadExecutor(Lcom/squareup/thread/FileThreadHolder;)Ljava/util/concurrent/Executor;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/thread/FileThreadExecutorModule;->INSTANCE:Lcom/squareup/thread/FileThreadExecutorModule;

    invoke-virtual {v0, p0}, Lcom/squareup/thread/FileThreadExecutorModule;->provideFileThreadExecutor(Lcom/squareup/thread/FileThreadHolder;)Ljava/util/concurrent/Executor;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;->get()Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/concurrent/Executor;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;->holderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/FileThreadHolder;

    invoke-static {v0}, Lcom/squareup/thread/FileThreadExecutorModule_ProvideFileThreadExecutorFactory;->provideFileThreadExecutor(Lcom/squareup/thread/FileThreadHolder;)Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method
