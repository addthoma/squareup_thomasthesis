.class public interface abstract Lcom/squareup/server/DamagedReaderService;
.super Ljava/lang/Object;
.source "DamagedReaderService.java"


# virtual methods
.method public abstract send(Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/reader/report-damaged-reader"
    .end annotation
.end method
