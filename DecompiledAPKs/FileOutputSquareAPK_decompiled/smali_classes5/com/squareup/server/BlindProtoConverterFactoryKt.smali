.class public final Lcom/squareup/server/BlindProtoConverterFactoryKt;
.super Ljava/lang/Object;
.source "BlindProtoConverterFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"
    }
    d2 = {
        "MIME_TYPE",
        "Lokhttp3/MediaType;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final MIME_TYPE:Lokhttp3/MediaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 12
    sget-object v0, Lokhttp3/MediaType;->Companion:Lokhttp3/MediaType$Companion;

    const-string v1, "application/x-protobuf"

    invoke-virtual {v0, v1}, Lokhttp3/MediaType$Companion;->get(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/BlindProtoConverterFactoryKt;->MIME_TYPE:Lokhttp3/MediaType;

    return-void
.end method

.method public static final synthetic access$getMIME_TYPE$p()Lokhttp3/MediaType;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/server/BlindProtoConverterFactoryKt;->MIME_TYPE:Lokhttp3/MediaType;

    return-object v0
.end method
