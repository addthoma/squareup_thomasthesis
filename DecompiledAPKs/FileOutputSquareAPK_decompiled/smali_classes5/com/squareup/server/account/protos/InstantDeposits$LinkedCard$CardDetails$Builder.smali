.class public final Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstantDeposits.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public pan_suffix:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 422
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
    .locals 0

    .line 431
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
    .locals 5

    .line 442
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v2, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->pan_suffix:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 415
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object v0

    return-object v0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public pan_suffix(Ljava/lang/String;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
    .locals 0

    .line 436
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->pan_suffix:Ljava/lang/String;

    return-object p0
.end method
