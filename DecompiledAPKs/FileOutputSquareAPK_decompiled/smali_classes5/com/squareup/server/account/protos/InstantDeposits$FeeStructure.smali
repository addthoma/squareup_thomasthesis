.class public final Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;
.super Lcom/squareup/wire/AndroidMessage;
.source "InstantDeposits.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeeStructure"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$ProtoAdapter_FeeStructure;,
        Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;",
        "Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FEE_BASIS_POINTS:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final fee_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final fee_basis_points:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field

.field public final minimum_fee_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 618
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$ProtoAdapter_FeeStructure;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$ProtoAdapter_FeeStructure;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 620
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 624
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->DEFAULT_FEE_BASIS_POINTS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 6

    .line 668
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 674
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 675
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    .line 676
    iput-object p2, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 677
    iput-object p3, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    .line 678
    iput-object p4, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 746
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 695
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 696
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    .line 697
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    .line 698
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 699
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    .line 700
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    .line 701
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 706
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_4

    .line 708
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 709
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 710
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 711
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 712
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 713
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;
    .locals 2

    .line 683
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;-><init>()V

    .line 684
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_basis_points:Ljava/lang/Integer;

    .line 685
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 686
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    .line 687
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    .line 688
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 617
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;
    .locals 2

    .line 738
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_basis_points(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v1

    .line 739
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v1

    .line 740
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->minimum_fee_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v1

    .line 741
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->minimum_fee_deposit_limit(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object p1, p0

    goto :goto_0

    .line 742
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 617
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->overlay(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;
    .locals 2

    .line 731
    iget-object v0, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->DEFAULT_FEE_BASIS_POINTS:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_basis_points(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 732
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 617
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 720
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 721
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", fee_basis_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 722
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", fee_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 723
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", minimum_fee_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 724
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", minimum_fee_deposit_limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeeStructure{"

    .line 725
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
