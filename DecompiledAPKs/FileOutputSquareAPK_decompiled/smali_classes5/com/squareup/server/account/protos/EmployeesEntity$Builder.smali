.class public final Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmployeesEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/EmployeesEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/EmployeesEntity;",
        "Lcom/squareup/server/account/protos/EmployeesEntity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public active:Ljava/lang/Boolean;

.field public can_access_register_with_passcode:Ljava/lang/Boolean;

.field public can_track_time:Ljava/lang/Boolean;

.field public current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

.field public employee_number:Ljava/lang/String;

.field public employee_role_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public first_name:Ljava/lang/String;

.field public is_account_owner:Ljava/lang/Boolean;

.field public is_owner:Ljava/lang/Boolean;

.field public last_name:Ljava/lang/String;

.field public passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

.field public permissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 331
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 332
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->permissions:Ljava/util/List;

    .line 333
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_role_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/EmployeesEntity;
    .locals 17

    move-object/from16 v0, p0

    .line 405
    new-instance v16, Lcom/squareup/server/account/protos/EmployeesEntity;

    iget-object v2, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->last_name:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_number:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->token:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active:Ljava/lang/Boolean;

    iget-object v7, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    iget-object v8, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->permissions:Ljava/util/List;

    iget-object v9, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode:Ljava/lang/Boolean;

    iget-object v10, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time:Ljava/lang/Boolean;

    iget-object v11, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    iget-object v12, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner:Ljava/lang/Boolean;

    iget-object v13, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner:Ljava/lang/Boolean;

    iget-object v14, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_role_tokens:Ljava/util/List;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/server/account/protos/EmployeesEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 304
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    return-object v0
.end method

.method public can_access_register_with_passcode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 373
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_track_time(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time:Ljava/lang/Boolean;

    return-object p0
.end method

.method public current_timecard(Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 383
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    return-object p0
.end method

.method public employee_number(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 347
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_number:Ljava/lang/String;

    return-object p0
.end method

.method public employee_role_tokens(Ljava/util/List;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/EmployeesEntity$Builder;"
        }
    .end annotation

    .line 398
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 399
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_role_tokens:Ljava/util/List;

    return-object p0
.end method

.method public first_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name:Ljava/lang/String;

    return-object p0
.end method

.method public is_account_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner:Ljava/lang/Boolean;

    return-object p0
.end method

.method public last_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->last_name:Ljava/lang/String;

    return-object p0
.end method

.method public passcode_only_credential(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 362
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    return-object p0
.end method

.method public permissions(Ljava/util/List;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/EmployeesEntity$Builder;"
        }
    .end annotation

    .line 367
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 368
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->permissions:Ljava/util/List;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    .line 352
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
