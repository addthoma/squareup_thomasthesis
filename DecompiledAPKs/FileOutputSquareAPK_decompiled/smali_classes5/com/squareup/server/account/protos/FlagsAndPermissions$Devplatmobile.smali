.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Devplatmobile"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$ProtoAdapter_Devplatmobile;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_USE_PAYSDK:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final use_paysdk:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12991
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$ProtoAdapter_Devplatmobile;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$ProtoAdapter_Devplatmobile;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 12993
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 12997
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->DEFAULT_USE_PAYSDK:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .line 13010
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 13014
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 13015
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 13068
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 13029
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 13030
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    .line 13031
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    .line 13032
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 13037
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 13039
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 13040
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 13041
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;
    .locals 2

    .line 13020
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;-><init>()V

    .line 13021
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->use_paysdk:Ljava/lang/Boolean;

    .line 13022
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 12990
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;
    .locals 2

    .line 13063
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->use_paysdk(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object p1, p0

    goto :goto_0

    .line 13064
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 12990
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;
    .locals 2

    .line 13056
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->DEFAULT_USE_PAYSDK:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->use_paysdk(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 13057
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 12990
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 13048
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13049
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", use_paysdk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->use_paysdk:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Devplatmobile{"

    .line 13050
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
