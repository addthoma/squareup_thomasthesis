.class public final Lcom/squareup/server/account/protos/BusinessBanking$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BusinessBanking.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/BusinessBanking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/BusinessBanking;",
        "Lcom/squareup/server/account/protos/BusinessBanking$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public show_card_spend:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/BusinessBanking;
    .locals 3

    .line 115
    new-instance v0, Lcom/squareup/server/account/protos/BusinessBanking;

    iget-object v1, p0, Lcom/squareup/server/account/protos/BusinessBanking$Builder;->show_card_spend:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/BusinessBanking;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/BusinessBanking$Builder;->build()Lcom/squareup/server/account/protos/BusinessBanking;

    move-result-object v0

    return-object v0
.end method

.method public show_card_spend(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/BusinessBanking$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/server/account/protos/BusinessBanking$Builder;->show_card_spend:Ljava/lang/Boolean;

    return-object p0
.end method
