.class public final Lcom/squareup/server/account/protos/AppointmentSettings;
.super Lcom/squareup/wire/AndroidMessage;
.source "AppointmentSettings.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/AppointmentSettings$ProtoAdapter_AppointmentSettings;,
        Lcom/squareup/server/account/protos/AppointmentSettings$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/AppointmentSettings;",
        "Lcom/squareup/server/account/protos/AppointmentSettings$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/AppointmentSettings;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/AppointmentSettings;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/AppointmentSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/AppointmentSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RECENTLY_JOINED_APPOINTMENTS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final recently_joined_appointments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/server/account/protos/AppointmentSettings$ProtoAdapter_AppointmentSettings;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AppointmentSettings$ProtoAdapter_AppointmentSettings;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/AppointmentSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/server/account/protos/AppointmentSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AppointmentSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AppointmentSettings;->DEFAULT_RECENTLY_JOINED_APPOINTMENTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/server/account/protos/AppointmentSettings;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/server/account/protos/AppointmentSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/AppointmentSettings$Builder;)Lcom/squareup/server/account/protos/AppointmentSettings$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AppointmentSettings;->newBuilder()Lcom/squareup/server/account/protos/AppointmentSettings$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 70
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/AppointmentSettings;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 71
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/AppointmentSettings;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AppointmentSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AppointmentSettings;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    .line 73
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 78
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AppointmentSettings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 81
    iget-object v1, p0, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 82
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/AppointmentSettings$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;->recently_joined_appointments:Ljava/lang/Boolean;

    .line 63
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AppointmentSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AppointmentSettings;->newBuilder()Lcom/squareup/server/account/protos/AppointmentSettings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/AppointmentSettings;)Lcom/squareup/server/account/protos/AppointmentSettings;
    .locals 2

    .line 103
    iget-object v0, p1, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AppointmentSettings;->requireBuilder(Lcom/squareup/server/account/protos/AppointmentSettings$Builder;)Lcom/squareup/server/account/protos/AppointmentSettings$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;->recently_joined_appointments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AppointmentSettings$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object p1, p0

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AppointmentSettings$Builder;->build()Lcom/squareup/server/account/protos/AppointmentSettings;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/account/protos/AppointmentSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AppointmentSettings;->overlay(Lcom/squareup/server/account/protos/AppointmentSettings;)Lcom/squareup/server/account/protos/AppointmentSettings;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/AppointmentSettings;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AppointmentSettings;->populateDefaults()Lcom/squareup/server/account/protos/AppointmentSettings;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", recently_joined_appointments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AppointmentSettings;->recently_joined_appointments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AppointmentSettings{"

    .line 91
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
