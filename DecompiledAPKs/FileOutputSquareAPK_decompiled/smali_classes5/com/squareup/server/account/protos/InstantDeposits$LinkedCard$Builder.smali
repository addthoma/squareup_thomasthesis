.class public final Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstantDeposits.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

.field public card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 284
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
    .locals 4

    .line 299
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    iget-object v2, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;-><init>(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 279
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v0

    return-object v0
.end method

.method public card_details(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    return-object p0
.end method

.method public card_status(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;
    .locals 0

    .line 293
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object p0
.end method
