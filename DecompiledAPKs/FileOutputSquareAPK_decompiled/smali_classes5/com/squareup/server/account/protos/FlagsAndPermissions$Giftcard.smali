.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Giftcard"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$ProtoAdapter_Giftcard;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ANDROID_THIRD_PARTY_GC_IME:Ljava/lang/Boolean;

.field public static final DEFAULT_ANDROID_THIRD_PARTY_GC_SUPPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_CASHOUT_REASON:Ljava/lang/Boolean;

.field public static final DEFAULT_CONFIGURE_EGIFT_ON_POS:Ljava/lang/Boolean;

.field public static final DEFAULT_REFUND_TO_GIFT_CARD_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_SELL_EGIFT_ON_POS:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_PLASTIC_GIFT_CARDS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final android_third_party_gc_ime:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final android_third_party_gc_support:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final can_use_cashout_reason:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final configure_egift_on_pos:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final refund_to_gift_card_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final sell_egift_on_pos:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final show_plastic_gift_cards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9823
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$ProtoAdapter_Giftcard;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$ProtoAdapter_Giftcard;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 9825
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 9829
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_ANDROID_THIRD_PARTY_GC_SUPPORT:Ljava/lang/Boolean;

    .line 9831
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_SHOW_PLASTIC_GIFT_CARDS:Ljava/lang/Boolean;

    .line 9833
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_CAN_USE_CASHOUT_REASON:Ljava/lang/Boolean;

    .line 9835
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_REFUND_TO_GIFT_CARD_ANDROID:Ljava/lang/Boolean;

    .line 9837
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_SELL_EGIFT_ON_POS:Ljava/lang/Boolean;

    .line 9839
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_CONFIGURE_EGIFT_ON_POS:Ljava/lang/Boolean;

    .line 9841
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_ANDROID_THIRD_PARTY_GC_IME:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 9

    .line 9917
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 9925
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 9926
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    .line 9927
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    .line 9928
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    .line 9929
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    .line 9930
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    .line 9931
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    .line 9932
    iput-object p7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 10021
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 9952
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 9953
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    .line 9954
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    .line 9955
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    .line 9956
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    .line 9957
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    .line 9958
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    .line 9959
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    .line 9960
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    .line 9961
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 9966
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_7

    .line 9968
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 9969
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9970
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9971
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9972
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9973
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9974
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9975
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 9976
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 2

    .line 9937
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;-><init>()V

    .line 9938
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_support:Ljava/lang/Boolean;

    .line 9939
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->show_plastic_gift_cards:Ljava/lang/Boolean;

    .line 9940
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->can_use_cashout_reason:Ljava/lang/Boolean;

    .line 9941
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->refund_to_gift_card_android:Ljava/lang/Boolean;

    .line 9942
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->sell_egift_on_pos:Ljava/lang/Boolean;

    .line 9943
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->configure_egift_on_pos:Ljava/lang/Boolean;

    .line 9944
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_ime:Ljava/lang/Boolean;

    .line 9945
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 9822
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;
    .locals 2

    .line 10010
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_support(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10011
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->show_plastic_gift_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10012
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->can_use_cashout_reason(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10013
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->refund_to_gift_card_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10014
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->sell_egift_on_pos(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10015
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->configure_egift_on_pos(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10016
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_ime(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object p1, p0

    goto :goto_0

    .line 10017
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 9822
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;
    .locals 2

    .line 9997
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_ANDROID_THIRD_PARTY_GC_SUPPORT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_support(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 9998
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_SHOW_PLASTIC_GIFT_CARDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->show_plastic_gift_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 9999
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_CAN_USE_CASHOUT_REASON:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->can_use_cashout_reason(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10000
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_REFUND_TO_GIFT_CARD_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->refund_to_gift_card_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10001
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_SELL_EGIFT_ON_POS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->sell_egift_on_pos(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10002
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_CONFIGURE_EGIFT_ON_POS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->configure_egift_on_pos(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    .line 10003
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->DEFAULT_ANDROID_THIRD_PARTY_GC_IME:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_ime(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object v0, p0

    goto :goto_0

    .line 10004
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 9822
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 9983
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9984
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", android_third_party_gc_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9985
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", show_plastic_gift_cards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9986
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_use_cashout_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9987
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", refund_to_gift_card_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9988
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", sell_egift_on_pos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9989
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", configure_egift_on_pos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9990
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", android_third_party_gc_ime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Giftcard{"

    .line 9991
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
