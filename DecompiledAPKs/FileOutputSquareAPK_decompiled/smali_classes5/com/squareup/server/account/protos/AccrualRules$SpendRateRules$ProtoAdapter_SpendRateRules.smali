.class final Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$ProtoAdapter_SpendRateRules;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SpendRateRules"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 811
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 832
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;-><init>()V

    .line 833
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 834
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 840
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 838
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->include_tax(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;

    goto :goto_0

    .line 837
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->spend(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;

    goto :goto_0

    .line 836
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;

    goto :goto_0

    .line 844
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 845
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 809
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$ProtoAdapter_SpendRateRules;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 824
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->points:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 825
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->spend:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 826
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->include_tax:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 827
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 809
    check-cast p2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$ProtoAdapter_SpendRateRules;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)I
    .locals 4

    .line 816
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->points:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->spend:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 817
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->include_tax:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 818
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 819
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 809
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$ProtoAdapter_SpendRateRules;->encodedSize(Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;
    .locals 2

    .line 850
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;

    move-result-object p1

    .line 851
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->spend:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->spend:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->spend:Lcom/squareup/protos/common/Money;

    .line 852
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 853
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 809
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules$ProtoAdapter_SpendRateRules;->redact(Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    move-result-object p1

    return-object p1
.end method
