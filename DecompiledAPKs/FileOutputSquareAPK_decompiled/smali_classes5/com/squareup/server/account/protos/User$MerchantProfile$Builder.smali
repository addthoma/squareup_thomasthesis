.class public final Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User$MerchantProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/User$MerchantProfile;",
        "Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public curated_image_url:Ljava/lang/String;

.field public custom_receipt_text:Ljava/lang/String;

.field public printed_receipt_image_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1158
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/User$MerchantProfile;
    .locals 5

    .line 1178
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantProfile;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->curated_image_url:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->printed_receipt_image_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->custom_receipt_text:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/User$MerchantProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1151
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object v0

    return-object v0
.end method

.method public curated_image_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;
    .locals 0

    .line 1162
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->curated_image_url:Ljava/lang/String;

    return-object p0
.end method

.method public custom_receipt_text(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;
    .locals 0

    .line 1172
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->custom_receipt_text:Ljava/lang/String;

    return-object p0
.end method

.method public printed_receipt_image_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;
    .locals 0

    .line 1167
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->printed_receipt_image_url:Ljava/lang/String;

    return-object p0
.end method
