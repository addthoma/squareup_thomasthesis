.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Appointments"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$ProtoAdapter_Appointments;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ANDROID_APPOINTMENT_LOCATION_EDITING_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_ANDROID_BUSINESS_HOURS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_ANDROID_CONFIRMATIONS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_ANDROID_EDIT_RECURRING_APPOINTMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_ANDROID_STAFF_HOURS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_ANDROID_SUPPORTS_PAGINATION:Ljava/lang/Boolean;

.field public static final DEFAULT_AVAILABILITY_OVERRIDE:Ljava/lang/Boolean;

.field public static final DEFAULT_CONFIRMATIONS_AND_SQUARE_ASSISTANT_EDUCATIONAL_MODAL:Ljava/lang/Boolean;

.field public static final DEFAULT_CONFIRMATIONS_BACKEND:Ljava/lang/Boolean;

.field public static final DEFAULT_DISMISS_REFERRALS:Ljava/lang/Boolean;

.field public static final DEFAULT_DISMISS_TUTORIALS:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_RATE_BASED_SERVICES:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_SERVICES_CATALOG_INTEGRATION:Ljava/lang/Boolean;

.field public static final DEFAULT_FIXED_PRICE_SERVICE_ITEMIZATION_PRICE_OVERRIDE:Ljava/lang/Boolean;

.field public static final DEFAULT_LIST_OF_EVENTS_VIEW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_MULTI_STAFF_VIEW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_PROCESSING_TIME_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_RESOURCE_BOOKING:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_WEEKVIEW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_STAFF_SWITCHER_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPRESS_UPDATE_FLAG_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CONVERSATIONS_SMS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final android_appointment_location_editing_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final android_business_hours_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final android_confirmations_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final android_edit_recurring_appointments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1b
    .end annotation
.end field

.field public final android_staff_hours_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final android_supports_pagination:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final availability_override:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final confirmations_backend:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final dismiss_referrals:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final dismiss_tutorials:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final enable_rate_based_services:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final enable_services_catalog_integration:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final fixed_price_service_itemization_price_override:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final list_of_events_view_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x18
    .end annotation
.end field

.field public final multi_staff_view_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1a
    .end annotation
.end field

.field public final processing_time_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final resource_booking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x19
    .end annotation
.end field

.field public final show_weekview_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final staff_switcher_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final suppress_update_flag_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final use_conversations_sms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16596
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$ProtoAdapter_Appointments;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$ProtoAdapter_Appointments;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 16598
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 16602
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_DISMISS_TUTORIALS:Ljava/lang/Boolean;

    .line 16604
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_DISMISS_REFERRALS:Ljava/lang/Boolean;

    .line 16606
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_SUPPRESS_UPDATE_FLAG_ANDROID:Ljava/lang/Boolean;

    .line 16608
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_SHOW_WEEKVIEW_ANDROID:Ljava/lang/Boolean;

    .line 16610
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_CONFIRMATIONS_ENABLED:Ljava/lang/Boolean;

    .line 16612
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ENABLE_SERVICES_CATALOG_INTEGRATION:Ljava/lang/Boolean;

    .line 16614
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_STAFF_SWITCHER_ANDROID:Ljava/lang/Boolean;

    .line 16616
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_AVAILABILITY_OVERRIDE:Ljava/lang/Boolean;

    .line 16618
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_FIXED_PRICE_SERVICE_ITEMIZATION_PRICE_OVERRIDE:Ljava/lang/Boolean;

    .line 16620
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_SUPPORTS_PAGINATION:Ljava/lang/Boolean;

    .line 16622
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_CONFIRMATIONS_BACKEND:Ljava/lang/Boolean;

    .line 16624
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ENABLE_RATE_BASED_SERVICES:Ljava/lang/Boolean;

    .line 16626
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_BUSINESS_HOURS_ENABLED:Ljava/lang/Boolean;

    .line 16628
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_CONFIRMATIONS_AND_SQUARE_ASSISTANT_EDUCATIONAL_MODAL:Ljava/lang/Boolean;

    .line 16630
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_USE_CONVERSATIONS_SMS:Ljava/lang/Boolean;

    .line 16632
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_STAFF_HOURS_ENABLED:Ljava/lang/Boolean;

    .line 16634
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_PROCESSING_TIME_ANDROID:Ljava/lang/Boolean;

    .line 16636
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_APPOINTMENT_LOCATION_EDITING_ENABLED:Ljava/lang/Boolean;

    .line 16638
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_LIST_OF_EVENTS_VIEW_ANDROID:Ljava/lang/Boolean;

    .line 16640
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_RESOURCE_BOOKING:Ljava/lang/Boolean;

    .line 16642
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_MULTI_STAFF_VIEW_ANDROID:Ljava/lang/Boolean;

    .line 16644
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_EDIT_RECURRING_APPOINTMENTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;Lokio/ByteString;)V
    .locals 1

    .line 16870
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 16871
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_tutorials:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    .line 16872
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_referrals:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    .line 16873
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->suppress_update_flag_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    .line 16874
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->show_weekview_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    .line 16875
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_confirmations_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    .line 16876
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_services_catalog_integration:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    .line 16877
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->staff_switcher_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    .line 16878
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->availability_override:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    .line 16879
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    .line 16880
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_supports_pagination:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    .line 16881
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_backend:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    .line 16882
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_rate_based_services:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    .line 16883
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_business_hours_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    .line 16884
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    .line 16885
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->use_conversations_sms:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    .line 16886
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_staff_hours_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    .line 16887
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->processing_time_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    .line 16888
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    .line 16889
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->list_of_events_view_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    .line 16890
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->resource_booking:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    .line 16891
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->multi_staff_view_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    .line 16892
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 17071
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 16927
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 16928
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    .line 16929
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    .line 16930
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    .line 16931
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    .line 16932
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    .line 16933
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    .line 16934
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    .line 16935
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    .line 16936
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    .line 16937
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    .line 16938
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    .line 16939
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    .line 16940
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    .line 16941
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    .line 16942
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    .line 16943
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    .line 16944
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    .line 16945
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    .line 16946
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    .line 16947
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    .line 16948
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    .line 16949
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    .line 16950
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    .line 16951
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 16956
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_16

    .line 16958
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 16959
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16960
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16961
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16962
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16963
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16964
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16965
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16966
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16967
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16968
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16969
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16970
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16971
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16972
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16973
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16974
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16975
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16976
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16977
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16978
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16979
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 16980
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_15
    add-int/2addr v0, v2

    .line 16981
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_16
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 2

    .line 16897
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;-><init>()V

    .line 16898
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_tutorials:Ljava/lang/Boolean;

    .line 16899
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_referrals:Ljava/lang/Boolean;

    .line 16900
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->suppress_update_flag_android:Ljava/lang/Boolean;

    .line 16901
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->show_weekview_android:Ljava/lang/Boolean;

    .line 16902
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_confirmations_enabled:Ljava/lang/Boolean;

    .line 16903
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_services_catalog_integration:Ljava/lang/Boolean;

    .line 16904
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->staff_switcher_android:Ljava/lang/Boolean;

    .line 16905
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->availability_override:Ljava/lang/Boolean;

    .line 16906
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    .line 16907
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_supports_pagination:Ljava/lang/Boolean;

    .line 16908
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_backend:Ljava/lang/Boolean;

    .line 16909
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_rate_based_services:Ljava/lang/Boolean;

    .line 16910
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_business_hours_enabled:Ljava/lang/Boolean;

    .line 16911
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    .line 16912
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->use_conversations_sms:Ljava/lang/Boolean;

    .line 16913
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_staff_hours_enabled:Ljava/lang/Boolean;

    .line 16914
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->processing_time_android:Ljava/lang/Boolean;

    .line 16915
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    .line 16916
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->list_of_events_view_android:Ljava/lang/Boolean;

    .line 16917
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->resource_booking:Ljava/lang/Boolean;

    .line 16918
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->multi_staff_view_android:Ljava/lang/Boolean;

    .line 16919
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    .line 16920
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 16595
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;
    .locals 2

    .line 17045
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_tutorials(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17046
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_referrals(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17047
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->suppress_update_flag_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17048
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->show_weekview_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17049
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_confirmations_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17050
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_services_catalog_integration(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17051
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->staff_switcher_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17052
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->availability_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17053
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->fixed_price_service_itemization_price_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17054
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_supports_pagination(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17055
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_backend(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17056
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_rate_based_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17057
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_business_hours_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17058
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_and_square_assistant_educational_modal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17059
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->use_conversations_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17060
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_staff_hours_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17061
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->processing_time_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17062
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_appointment_location_editing_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17063
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->list_of_events_view_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17064
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->resource_booking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17065
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->multi_staff_view_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17066
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    if-eqz v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_edit_recurring_appointments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    :cond_15
    if-nez v1, :cond_16

    move-object p1, p0

    goto :goto_0

    .line 17067
    :cond_16
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 16595
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;
    .locals 2

    .line 17017
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_DISMISS_TUTORIALS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_tutorials(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17018
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_DISMISS_REFERRALS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_referrals(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17019
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_SUPPRESS_UPDATE_FLAG_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->suppress_update_flag_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17020
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_SHOW_WEEKVIEW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->show_weekview_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17021
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_CONFIRMATIONS_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_confirmations_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17022
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ENABLE_SERVICES_CATALOG_INTEGRATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_services_catalog_integration(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17023
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_STAFF_SWITCHER_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->staff_switcher_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17024
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_AVAILABILITY_OVERRIDE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->availability_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17025
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_FIXED_PRICE_SERVICE_ITEMIZATION_PRICE_OVERRIDE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->fixed_price_service_itemization_price_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17026
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_SUPPORTS_PAGINATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_supports_pagination(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17027
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_CONFIRMATIONS_BACKEND:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_backend(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17028
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ENABLE_RATE_BASED_SERVICES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_rate_based_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17029
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_BUSINESS_HOURS_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_business_hours_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17030
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_CONFIRMATIONS_AND_SQUARE_ASSISTANT_EDUCATIONAL_MODAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_and_square_assistant_educational_modal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17031
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_USE_CONVERSATIONS_SMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->use_conversations_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17032
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_STAFF_HOURS_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_staff_hours_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17033
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_PROCESSING_TIME_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->processing_time_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17034
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_APPOINTMENT_LOCATION_EDITING_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_appointment_location_editing_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17035
    :cond_11
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    if-nez v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_LIST_OF_EVENTS_VIEW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->list_of_events_view_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17036
    :cond_12
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_RESOURCE_BOOKING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->resource_booking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17037
    :cond_13
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    if-nez v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_MULTI_STAFF_VIEW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->multi_staff_view_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    .line 17038
    :cond_14
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    if-nez v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->DEFAULT_ANDROID_EDIT_RECURRING_APPOINTMENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_edit_recurring_appointments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;

    move-result-object v1

    :cond_15
    if-nez v1, :cond_16

    move-object v0, p0

    goto :goto_0

    .line 17039
    :cond_16
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 16595
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 16988
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16989
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", dismiss_tutorials="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16990
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", dismiss_referrals="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16991
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", suppress_update_flag_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->suppress_update_flag_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16992
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", show_weekview_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16993
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", android_confirmations_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16994
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", enable_services_catalog_integration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16995
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", staff_switcher_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->staff_switcher_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16996
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", availability_override="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16997
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", fixed_price_service_itemization_price_override="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16998
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", android_supports_pagination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16999
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", confirmations_backend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17000
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", enable_rate_based_services="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17001
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", android_business_hours_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17002
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", confirmations_and_square_assistant_educational_modal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17003
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", use_conversations_sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17004
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", android_staff_hours_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17005
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", processing_time_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17006
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", android_appointment_location_editing_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17007
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", list_of_events_view_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17008
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", resource_booking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17009
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", multi_staff_view_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17010
    :cond_14
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    const-string v1, ", android_edit_recurring_appointments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_15
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Appointments{"

    .line 17011
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
