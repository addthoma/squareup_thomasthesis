.class public final Lcom/squareup/server/account/protos/EmployeesEntity;
.super Lcom/squareup/wire/AndroidMessage;
.source "EmployeesEntity.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/EmployeesEntity$ProtoAdapter_EmployeesEntity;,
        Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;,
        Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;,
        Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/EmployeesEntity;",
        "Lcom/squareup/server/account/protos/EmployeesEntity$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/EmployeesEntity;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/EmployeesEntity;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/EmployeesEntity;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/EmployeesEntity;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIVE:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_ACCESS_REGISTER_WITH_PASSCODE:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_TRACK_TIME:Ljava/lang/Boolean;

.field public static final DEFAULT_EMPLOYEE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_FIRST_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_ACCOUNT_OWNER:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_OWNER:Ljava/lang/Boolean;

.field public static final DEFAULT_LAST_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final active:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final can_access_register_with_passcode:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final can_track_time:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.EmployeesEntity$CurrentTimecard#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final employee_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final employee_role_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final first_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final is_account_owner:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final is_owner:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final last_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.EmployeesEntity$PasscodeOnlyCredential#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final permissions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$ProtoAdapter_EmployeesEntity;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$ProtoAdapter_EmployeesEntity;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 45
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_ACTIVE:Ljava/lang/Boolean;

    .line 47
    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_CAN_ACCESS_REGISTER_WITH_PASSCODE:Ljava/lang/Boolean;

    .line 49
    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_CAN_TRACK_TIME:Ljava/lang/Boolean;

    .line 51
    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_IS_ACCOUNT_OWNER:Ljava/lang/Boolean;

    .line 53
    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_IS_OWNER:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 154
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/server/account/protos/EmployeesEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 163
    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 164
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    .line 165
    iput-object p2, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    .line 166
    iput-object p3, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    .line 167
    iput-object p4, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    .line 168
    iput-object p5, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    .line 169
    iput-object p6, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    const-string p1, "permissions"

    .line 170
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    .line 171
    iput-object p8, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    .line 172
    iput-object p9, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    .line 173
    iput-object p10, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    .line 174
    iput-object p11, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    .line 175
    iput-object p12, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    const-string p1, "employee_role_tokens"

    .line 176
    invoke-static {p1, p13}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 301
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 202
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 203
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/EmployeesEntity;

    .line 204
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    .line 206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    .line 207
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    .line 210
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    .line 211
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    .line 212
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    .line 213
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    .line 214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    .line 217
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 222
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_b

    .line 224
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 226
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$Builder;
    .locals 2

    .line 181
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;-><init>()V

    .line 182
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name:Ljava/lang/String;

    .line 183
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->last_name:Ljava/lang/String;

    .line 184
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_number:Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->token:Ljava/lang/String;

    .line 186
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active:Ljava/lang/Boolean;

    .line 187
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    .line 188
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->permissions:Ljava/util/List;

    .line 189
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode:Ljava/lang/Boolean;

    .line 190
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time:Ljava/lang/Boolean;

    .line 191
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    .line 192
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner:Ljava/lang/Boolean;

    .line 193
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner:Ljava/lang/Boolean;

    .line 194
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_role_tokens:Ljava/util/List;

    .line 195
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/server/account/protos/EmployeesEntity;
    .locals 2

    .line 284
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 285
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 286
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_number(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 287
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->token(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 288
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 289
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 290
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->permissions(Ljava/util/List;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 291
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 292
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 293
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard(Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 294
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 295
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 296
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_role_tokens(Ljava/util/List;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    :cond_c
    if-nez v1, :cond_d

    move-object p1, p0

    goto :goto_0

    .line 297
    :cond_d
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity;->overlay(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity;
    .locals 3

    .line 265
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_ACTIVE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-eqz v0, :cond_1

    .line 267
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object v0

    .line 268
    iget-object v2, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_CAN_ACCESS_REGISTER_WITH_PASSCODE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 271
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_CAN_TRACK_TIME:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 272
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    if-eqz v0, :cond_4

    .line 273
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;->populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    move-result-object v0

    .line 274
    iget-object v2, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    if-eq v0, v2, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard(Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 276
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_IS_ACCOUNT_OWNER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    .line 277
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity;->DEFAULT_IS_OWNER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object v0, p0

    goto :goto_0

    .line 278
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity;->populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", first_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", last_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", employee_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-eqz v1, :cond_5

    const-string v1, ", passcode_only_credential="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", permissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 253
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", can_access_register_with_passcode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 254
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", can_track_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 255
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    if-eqz v1, :cond_9

    const-string v1, ", current_timecard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 256
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", is_account_owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 257
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", is_owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 258
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", employee_role_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EmployeesEntity{"

    .line 259
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
