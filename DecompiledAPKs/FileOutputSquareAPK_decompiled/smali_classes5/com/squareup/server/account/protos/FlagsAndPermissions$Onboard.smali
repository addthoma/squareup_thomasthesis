.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Onboard"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$ProtoAdapter_Onboard;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ASK_INTENT_QUESTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_BANK_LINK:Ljava/lang/Boolean;

.field public static final DEFAULT_CHOOSE_DEFAULT_DEPOSIT_METHOD:Ljava/lang/Boolean;

.field public static final DEFAULT_OFFER_FREE_READER:Ljava/lang/Boolean;

.field public static final DEFAULT_REFERRAL:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPRESS_FIRST_TUTORIAL:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SERVER_DRIVEN_FLOW:Ljava/lang/Boolean;

.field public static final DEFAULT_VALIDATE_SHIPPING_ADDRESS:Ljava/lang/Boolean;

.field public static final DEFAULT_X2_VERTICAL_SELECTION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final ask_intent_questions:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final bank_link:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final choose_default_deposit_method:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final offer_free_reader:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final referral:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final suppress_first_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final use_server_driven_flow:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final validate_shipping_address:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final x2_vertical_selection:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4856
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$ProtoAdapter_Onboard;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$ProtoAdapter_Onboard;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4858
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 4862
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_VALIDATE_SHIPPING_ADDRESS:Ljava/lang/Boolean;

    .line 4864
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_ASK_INTENT_QUESTIONS:Ljava/lang/Boolean;

    .line 4866
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_CHOOSE_DEFAULT_DEPOSIT_METHOD:Ljava/lang/Boolean;

    .line 4868
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_OFFER_FREE_READER:Ljava/lang/Boolean;

    .line 4870
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_BANK_LINK:Ljava/lang/Boolean;

    .line 4872
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_REFERRAL:Ljava/lang/Boolean;

    .line 4874
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_SUPPRESS_FIRST_TUTORIAL:Ljava/lang/Boolean;

    .line 4876
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_USE_SERVER_DRIVEN_FLOW:Ljava/lang/Boolean;

    .line 4878
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_X2_VERTICAL_SELECTION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 11

    .line 4975
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 4984
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4985
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    .line 4986
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    .line 4987
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    .line 4988
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    .line 4989
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    .line 4990
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    .line 4991
    iput-object p7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    .line 4992
    iput-object p8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    .line 4993
    iput-object p9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 5094
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5015
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5016
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    .line 5017
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    .line 5018
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    .line 5019
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    .line 5020
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    .line 5021
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    .line 5022
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    .line 5023
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    .line 5024
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    .line 5025
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    .line 5026
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5031
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_9

    .line 5033
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5034
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5035
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5036
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5037
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5038
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5039
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5040
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5041
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5042
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 5043
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 2

    .line 4998
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;-><init>()V

    .line 4999
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->validate_shipping_address:Ljava/lang/Boolean;

    .line 5000
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->ask_intent_questions:Ljava/lang/Boolean;

    .line 5001
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->choose_default_deposit_method:Ljava/lang/Boolean;

    .line 5002
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->offer_free_reader:Ljava/lang/Boolean;

    .line 5003
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->bank_link:Ljava/lang/Boolean;

    .line 5004
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->referral:Ljava/lang/Boolean;

    .line 5005
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->suppress_first_tutorial:Ljava/lang/Boolean;

    .line 5006
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->use_server_driven_flow:Ljava/lang/Boolean;

    .line 5007
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->x2_vertical_selection:Ljava/lang/Boolean;

    .line 5008
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4855
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;
    .locals 2

    .line 5081
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->validate_shipping_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5082
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->ask_intent_questions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5083
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->choose_default_deposit_method(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5084
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->offer_free_reader(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5085
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->bank_link(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5086
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->referral(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5087
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->suppress_first_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5088
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->use_server_driven_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5089
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->x2_vertical_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    :cond_8
    if-nez v1, :cond_9

    move-object p1, p0

    goto :goto_0

    .line 5090
    :cond_9
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 4855
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;
    .locals 2

    .line 5066
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_VALIDATE_SHIPPING_ADDRESS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->validate_shipping_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5067
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_ASK_INTENT_QUESTIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->ask_intent_questions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5068
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_CHOOSE_DEFAULT_DEPOSIT_METHOD:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->choose_default_deposit_method(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5069
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_OFFER_FREE_READER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->offer_free_reader(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5070
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_BANK_LINK:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->bank_link(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5071
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_REFERRAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->referral(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5072
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_SUPPRESS_FIRST_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->suppress_first_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5073
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_USE_SERVER_DRIVEN_FLOW:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->use_server_driven_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    .line 5074
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->DEFAULT_X2_VERTICAL_SELECTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->x2_vertical_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;

    move-result-object v1

    :cond_8
    if-nez v1, :cond_9

    move-object v0, p0

    goto :goto_0

    .line 5075
    :cond_9
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 4855
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5050
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5051
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", validate_shipping_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5052
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", ask_intent_questions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5053
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", choose_default_deposit_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5054
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", offer_free_reader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5055
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", bank_link="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5056
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", referral="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5057
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", suppress_first_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5058
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", use_server_driven_flow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5059
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", x2_vertical_selection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Onboard{"

    .line 5060
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
