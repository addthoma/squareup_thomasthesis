.class public final Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmployeesEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;",
        "Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public clockin_time:Ljava/lang/String;

.field public id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 699
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;
    .locals 4

    .line 717
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->clockin_time:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 694
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    move-result-object v0

    return-object v0
.end method

.method public clockin_time(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;
    .locals 0

    .line 711
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->clockin_time:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;
    .locals 0

    .line 703
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->id:Ljava/lang/String;

    return-object p0
.end method
