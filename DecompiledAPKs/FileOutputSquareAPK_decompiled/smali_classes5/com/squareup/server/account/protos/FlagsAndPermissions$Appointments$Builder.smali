.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public android_appointment_location_editing_enabled:Ljava/lang/Boolean;

.field public android_business_hours_enabled:Ljava/lang/Boolean;

.field public android_confirmations_enabled:Ljava/lang/Boolean;

.field public android_edit_recurring_appointments:Ljava/lang/Boolean;

.field public android_staff_hours_enabled:Ljava/lang/Boolean;

.field public android_supports_pagination:Ljava/lang/Boolean;

.field public availability_override:Ljava/lang/Boolean;

.field public confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

.field public confirmations_backend:Ljava/lang/Boolean;

.field public dismiss_referrals:Ljava/lang/Boolean;

.field public dismiss_tutorials:Ljava/lang/Boolean;

.field public enable_rate_based_services:Ljava/lang/Boolean;

.field public enable_services_catalog_integration:Ljava/lang/Boolean;

.field public fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

.field public list_of_events_view_android:Ljava/lang/Boolean;

.field public multi_staff_view_android:Ljava/lang/Boolean;

.field public processing_time_android:Ljava/lang/Boolean;

.field public resource_booking:Ljava/lang/Boolean;

.field public show_weekview_android:Ljava/lang/Boolean;

.field public staff_switcher_android:Ljava/lang/Boolean;

.field public suppress_update_flag_android:Ljava/lang/Boolean;

.field public use_conversations_sms:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public android_appointment_location_editing_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17269
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public android_business_hours_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17226
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_business_hours_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public android_confirmations_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17159
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_confirmations_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public android_edit_recurring_appointments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17302
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public android_staff_hours_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17252
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_staff_hours_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public android_supports_pagination(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17202
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->android_supports_pagination:Ljava/lang/Boolean;

    return-object p0
.end method

.method public availability_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17185
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->availability_override:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;
    .locals 2

    .line 17308
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 17074
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object v0

    return-object v0
.end method

.method public confirmations_and_square_assistant_educational_modal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 17236
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    return-object p0
.end method

.method public confirmations_backend(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17210
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->confirmations_backend:Ljava/lang/Boolean;

    return-object p0
.end method

.method public dismiss_referrals(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17134
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_referrals:Ljava/lang/Boolean;

    return-object p0
.end method

.method public dismiss_tutorials(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17126
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->dismiss_tutorials:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_rate_based_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17218
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_rate_based_services:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_services_catalog_integration(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17168
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->enable_services_catalog_integration:Ljava/lang/Boolean;

    return-object p0
.end method

.method public fixed_price_service_itemization_price_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17194
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    return-object p0
.end method

.method public list_of_events_view_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17277
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->list_of_events_view_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public multi_staff_view_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17293
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->multi_staff_view_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public processing_time_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17260
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->processing_time_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public resource_booking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17285
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->resource_booking:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_weekview_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17151
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->show_weekview_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public staff_switcher_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 17177
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->staff_switcher_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public suppress_update_flag_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 17143
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->suppress_update_flag_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_conversations_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;
    .locals 0

    .line 17244
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments$Builder;->use_conversations_sms:Ljava/lang/Boolean;

    return-object p0
.end method
