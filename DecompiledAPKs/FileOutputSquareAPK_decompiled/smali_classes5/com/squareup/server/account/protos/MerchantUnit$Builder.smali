.class public final Lcom/squareup/server/account/protos/MerchantUnit$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MerchantUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/MerchantUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/MerchantUnit;",
        "Lcom/squareup/server/account/protos/MerchantUnit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public active:Ljava/lang/Boolean;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/MerchantUnit$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantUnit$Builder;->active:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/MerchantUnit;
    .locals 4

    .line 141
    new-instance v0, Lcom/squareup/server/account/protos/MerchantUnit;

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantUnit$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/MerchantUnit$Builder;->active:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/MerchantUnit;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/MerchantUnit$Builder;->build()Lcom/squareup/server/account/protos/MerchantUnit;

    move-result-object v0

    return-object v0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/server/account/protos/MerchantUnit$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantUnit$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
