.class public final Lcom/squareup/server/account/protos/Tipping;
.super Lcom/squareup/wire/AndroidMessage;
.source "Tipping.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/Tipping$ProtoAdapter_Tipping;,
        Lcom/squareup/server/account/protos/Tipping$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/Tipping;",
        "Lcom/squareup/server/account/protos/Tipping$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/Tipping;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/Tipping;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/Tipping;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/Tipping;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

.field public static final DEFAULT_MANUAL_TIP_ENTRY_MAX_PERCENTAGE:Ljava/lang/Double;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_manual_tip_entry:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final manual_tip_entry_max_percentage:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x7
    .end annotation
.end field

.field public final manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final smart_tipping_over_threshold_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.tipping.TipOption#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final smart_tipping_under_threshold_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.tipping.TipOption#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    new-instance v0, Lcom/squareup/server/account/protos/Tipping$ProtoAdapter_Tipping;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Tipping$ProtoAdapter_Tipping;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/Tipping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 36
    sget-object v0, Lcom/squareup/server/account/protos/Tipping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Tipping;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Tipping;->DEFAULT_ALLOW_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 42
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Tipping;->DEFAULT_MANUAL_TIP_ENTRY_MAX_PERCENTAGE:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Double;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Double;",
            ")V"
        }
    .end annotation

    .line 100
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/Tipping;-><init>(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Double;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 109
    sget-object v0, Lcom/squareup/server/account/protos/Tipping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 110
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    const-string p1, "smart_tipping_under_threshold_options"

    .line 111
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    const-string p1, "smart_tipping_over_threshold_options"

    .line 112
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    .line 113
    iput-object p4, p0, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    .line 114
    iput-object p5, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    .line 115
    iput-object p6, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    .line 116
    iput-object p7, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tipping;->newBuilder()Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/Tipping;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 137
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/Tipping;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tipping;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Tipping;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    .line 140
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    .line 141
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    .line 145
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 150
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_5

    .line 152
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tipping;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 160
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 2

    .line 121
    new-instance v0, Lcom/squareup/server/account/protos/Tipping$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Tipping$Builder;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    .line 123
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    .line 124
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    .line 125
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tipping$Builder;->allow_manual_tip_entry:Ljava/lang/Boolean;

    .line 126
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    .line 127
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    .line 128
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tipping;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tipping;->newBuilder()Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/Tipping;)Lcom/squareup/server/account/protos/Tipping;
    .locals 2

    .line 188
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tipping;->requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_threshold_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v1

    .line 189
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tipping;->requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_under_threshold_options(Ljava/util/List;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v1

    .line 190
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tipping;->requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_over_threshold_options(Ljava/util/List;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v1

    .line 191
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tipping;->requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->allow_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v1

    .line 192
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tipping;->requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_largest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v1

    .line 193
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tipping;->requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_smallest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v1

    .line 194
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tipping;->requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_max_percentage(Ljava/lang/Double;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object p1, p0

    goto :goto_0

    .line 195
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->build()Lcom/squareup/server/account/protos/Tipping;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/server/account/protos/Tipping;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Tipping;->overlay(Lcom/squareup/server/account/protos/Tipping;)Lcom/squareup/server/account/protos/Tipping;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/Tipping;
    .locals 2

    .line 181
    iget-object v0, p0, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tipping;->requireBuilder(Lcom/squareup/server/account/protos/Tipping$Builder;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Tipping;->DEFAULT_ALLOW_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->allow_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tipping$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 182
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Tipping$Builder;->build()Lcom/squareup/server/account/protos/Tipping;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tipping;->populateDefaults()Lcom/squareup/server/account/protos/Tipping;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", smart_tipping_threshold_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", smart_tipping_under_threshold_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", smart_tipping_over_threshold_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", allow_manual_tip_entry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->allow_manual_tip_entry:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", manual_tip_entry_largest_max_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", manual_tip_entry_smallest_max_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    if-eqz v1, :cond_6

    const-string v1, ", manual_tip_entry_max_percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Tipping{"

    .line 175
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
