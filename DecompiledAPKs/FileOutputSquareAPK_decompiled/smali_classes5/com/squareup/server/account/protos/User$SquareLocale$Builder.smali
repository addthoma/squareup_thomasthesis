.class public final Lcom/squareup/server/account/protos/User$SquareLocale$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User$SquareLocale;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/User$SquareLocale;",
        "Lcom/squareup/server/account/protos/User$SquareLocale$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public country_code:Ljava/lang/String;

.field public country_code_guess:Ljava/lang/String;

.field public currency_codes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 619
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 620
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->currency_codes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/User$SquareLocale;
    .locals 5

    .line 644
    new-instance v0, Lcom/squareup/server/account/protos/User$SquareLocale;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code_guess:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->currency_codes:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/User$SquareLocale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 612
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->build()Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object v0

    return-object v0
.end method

.method public country_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;
    .locals 0

    .line 632
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code:Ljava/lang/String;

    return-object p0
.end method

.method public country_code_guess(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;
    .locals 0

    .line 624
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code_guess:Ljava/lang/String;

    return-object p0
.end method

.method public currency_codes(Ljava/util/List;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/server/account/protos/User$SquareLocale$Builder;"
        }
    .end annotation

    .line 637
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 638
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->currency_codes:Ljava/util/List;

    return-object p0
.end method
