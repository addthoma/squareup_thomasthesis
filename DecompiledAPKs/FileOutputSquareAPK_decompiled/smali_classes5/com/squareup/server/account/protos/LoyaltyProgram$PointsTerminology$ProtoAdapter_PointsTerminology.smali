.class final Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$ProtoAdapter_PointsTerminology;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LoyaltyProgram.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PointsTerminology"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 560
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 579
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;-><init>()V

    .line 580
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 581
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 586
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 584
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->other(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    goto :goto_0

    .line 583
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->one(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    goto :goto_0

    .line 590
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 591
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 558
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$ProtoAdapter_PointsTerminology;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 572
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 573
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 574
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 558
    check-cast p2, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$ProtoAdapter_PointsTerminology;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)I
    .locals 4

    .line 565
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    const/4 v3, 0x2

    .line 566
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 558
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$ProtoAdapter_PointsTerminology;->encodedSize(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
    .locals 0

    .line 596
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->newBuilder()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    move-result-object p1

    .line 597
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 598
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 558
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$ProtoAdapter_PointsTerminology;->redact(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object p1

    return-object p1
.end method
