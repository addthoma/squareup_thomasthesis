.class public final Lcom/squareup/server/account/protos/User$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/User;",
        "Lcom/squareup/server/account/protos/User$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_abn:Ljava/lang/String;

.field public email:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public impersonating:Ljava/lang/Boolean;

.field public locale:Lcom/squareup/server/account/protos/User$SquareLocale;

.field public mcc:Ljava/lang/Integer;

.field public merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

.field public merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

.field public merchant_token:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public phone:Ljava/lang/String;

.field public profile_image_url:Ljava/lang/String;

.field public receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

.field public subunit_name:Ljava/lang/String;

.field public token:Ljava/lang/String;

.field public twitter:Ljava/lang/String;

.field public website:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 402
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/User;
    .locals 2

    .line 498
    new-instance v0, Lcom/squareup/server/account/protos/User;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/User;-><init>(Lcom/squareup/server/account/protos/User$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 367
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$Builder;->build()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    return-object v0
.end method

.method public business_abn(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 464
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->business_abn:Ljava/lang/String;

    return-object p0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 416
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 406
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public impersonating(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 487
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->impersonating:Ljava/lang/Boolean;

    return-object p0
.end method

.method public locale(Lcom/squareup/server/account/protos/User$SquareLocale;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 449
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    return-object p0
.end method

.method public mcc(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 444
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->mcc:Ljava/lang/Integer;

    return-object p0
.end method

.method public merchant_key(Lcom/squareup/server/account/protos/User$MerchantKey;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 469
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    return-object p0
.end method

.method public merchant_profile(Lcom/squareup/server/account/protos/User$MerchantProfile;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 459
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 492
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 411
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public phone(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->phone:Ljava/lang/String;

    return-object p0
.end method

.method public profile_image_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 474
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->profile_image_url:Ljava/lang/String;

    return-object p0
.end method

.method public receipt_address(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 454
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    return-object p0
.end method

.method public subunit_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 436
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->subunit_name:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 479
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public twitter(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 421
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->twitter:Ljava/lang/String;

    return-object p0
.end method

.method public website(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    .line 431
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$Builder;->website:Ljava/lang/String;

    return-object p0
.end method
