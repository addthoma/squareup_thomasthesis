.class public final Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;
.super Lcom/squareup/wire/AndroidMessage;
.source "LoyaltyPointsExpirationPolicy.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$ProtoAdapter_LoyaltyPointsExpirationPolicy;,
        Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;,
        Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;",
        "Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$ProtoAdapter_LoyaltyPointsExpirationPolicy;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$ProtoAdapter_LoyaltyPointsExpirationPolicy;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 38
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, v0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;)Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->newBuilder()Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 55
    :cond_0
    instance-of v0, p1, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 56
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;

    .line 57
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 62
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;
    .locals 2

    .line 47
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;-><init>()V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->newBuilder()Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;)Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->overlay(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;)Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;->populateDefaults()Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyPointsExpirationPolicy{"

    .line 68
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
