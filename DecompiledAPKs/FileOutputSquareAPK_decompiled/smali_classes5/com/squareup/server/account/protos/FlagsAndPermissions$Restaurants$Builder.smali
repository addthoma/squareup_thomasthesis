.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_change_employee_defaults:Ljava/lang/Boolean;

.field public can_change_ot_defaults:Ljava/lang/Boolean;

.field public can_see_auto_grat:Ljava/lang/Boolean;

.field public can_see_cover_counts:Ljava/lang/Boolean;

.field public can_see_seating:Ljava/lang/Boolean;

.field public manual_86:Ljava/lang/Boolean;

.field public use_auto_gratuities:Ljava/lang/Boolean;

.field public use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

.field public use_configured_business_hours:Ljava/lang/Boolean;

.field public use_rst_friendly_t2:Ljava/lang/Boolean;

.field public use_sales_limits:Ljava/lang/Boolean;

.field public use_split_ticket:Ljava/lang/Boolean;

.field public use_table_management:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16404
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;
    .locals 17

    move-object/from16 v0, p0

    .line 16513
    new-instance v16, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    iget-object v2, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_rst_friendly_t2:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_ot_defaults:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_employee_defaults:Ljava/lang/Boolean;

    iget-object v5, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_seating:Ljava/lang/Boolean;

    iget-object v6, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_auto_grat:Ljava/lang/Boolean;

    iget-object v7, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_cover_counts:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->manual_86:Ljava/lang/Boolean;

    iget-object v9, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_configured_business_hours:Ljava/lang/Boolean;

    iget-object v10, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_sales_limits:Ljava/lang/Boolean;

    iget-object v11, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_split_ticket:Ljava/lang/Boolean;

    iget-object v13, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_table_management:Ljava/lang/Boolean;

    iget-object v14, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_auto_gratuities:Ljava/lang/Boolean;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 16377
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    return-object v0
.end method

.method public can_change_employee_defaults(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16427
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_employee_defaults:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_change_ot_defaults(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16419
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_change_ot_defaults:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_see_auto_grat(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16443
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_auto_grat:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_see_cover_counts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16451
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_cover_counts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_see_seating(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16435
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->can_see_seating:Ljava/lang/Boolean;

    return-object p0
.end method

.method public manual_86(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16459
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->manual_86:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_auto_gratuities(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16507
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_auto_gratuities:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_comp_void_assign_move_ticket(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16483
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_configured_business_hours(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16467
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_configured_business_hours:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_rst_friendly_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16411
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_rst_friendly_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_sales_limits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16475
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_sales_limits:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_split_ticket(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16491
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_split_ticket:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_table_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;
    .locals 0

    .line 16499
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants$Builder;->use_table_management:Ljava/lang/Boolean;

    return-object p0
.end method
