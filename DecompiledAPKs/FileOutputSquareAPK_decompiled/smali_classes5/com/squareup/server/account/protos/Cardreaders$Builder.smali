.class public final Lcom/squareup/server/account/protos/Cardreaders$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cardreaders.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Cardreaders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/Cardreaders;",
        "Lcom/squareup/server/account/protos/Cardreaders$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public disable_magstripe_processing:Ljava/lang/Boolean;

.field public enable_emoney_speedtest:Ljava/lang/Boolean;

.field public r12_early_k400_powerup:Ljava/lang/Boolean;

.field public record_tmn_timings:Ljava/lang/Boolean;

.field public reorder_in_app:Ljava/lang/Boolean;

.field public reorder_in_app_email_confirm:Ljava/lang/Boolean;

.field public show_order_reader_status_in_app:Ljava/lang/Boolean;

.field public use_v2_ble_state_machine:Ljava/lang/Boolean;

.field public use_v2_cardreaders:Ljava/lang/Boolean;

.field public verbose_tmn_timings:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 281
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/Cardreaders;
    .locals 13

    .line 336
    new-instance v12, Lcom/squareup/server/account/protos/Cardreaders;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->r12_early_k400_powerup:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->disable_magstripe_processing:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->record_tmn_timings:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->verbose_tmn_timings:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->enable_emoney_speedtest:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_cardreaders:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/account/protos/Cardreaders;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 260
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->build()Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object v0

    return-object v0
.end method

.method public disable_magstripe_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->disable_magstripe_processing:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_emoney_speedtest(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 325
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->enable_emoney_speedtest:Ljava/lang/Boolean;

    return-object p0
.end method

.method public r12_early_k400_powerup(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->r12_early_k400_powerup:Ljava/lang/Boolean;

    return-object p0
.end method

.method public record_tmn_timings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->record_tmn_timings:Ljava/lang/Boolean;

    return-object p0
.end method

.method public reorder_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app:Ljava/lang/Boolean;

    return-object p0
.end method

.method public reorder_in_app_email_confirm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_order_reader_status_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_v2_ble_state_machine(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_v2_cardreaders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_cardreaders:Ljava/lang/Boolean;

    return-object p0
.end method

.method public verbose_tmn_timings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->verbose_tmn_timings:Ljava/lang/Boolean;

    return-object p0
.end method
