.class public Lcom/squareup/server/payment/CreatePaymentMessage;
.super Lcom/squareup/server/payment/LocalPaymentMessageBase;
.source "CreatePaymentMessage.java"


# instance fields
.field public final amount_cents:J

.field public final tax_cents:J


# direct methods
.method public constructor <init>(JJLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;)V"
        }
    .end annotation

    move-object v9, p0

    move-object v0, p0

    move-object v1, p5

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move/from16 v7, p11

    move-object/from16 v8, p12

    .line 13
    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/payment/LocalPaymentMessageBase;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V

    move-wide v0, p1

    .line 15
    iput-wide v0, v9, Lcom/squareup/server/payment/CreatePaymentMessage;->amount_cents:J

    move-wide v0, p3

    .line 16
    iput-wide v0, v9, Lcom/squareup/server/payment/CreatePaymentMessage;->tax_cents:J

    return-void
.end method
