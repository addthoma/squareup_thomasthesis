.class public final enum Lcom/squareup/server/payment/CustomerPresence;
.super Ljava/lang/Enum;
.source "CustomerPresence.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/payment/CustomerPresence;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/payment/CustomerPresence;

.field public static final enum phone:Lcom/squareup/server/payment/CustomerPresence;

.field public static final enum present:Lcom/squareup/server/payment/CustomerPresence;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 13
    new-instance v0, Lcom/squareup/server/payment/CustomerPresence;

    const/4 v1, 0x0

    const-string v2, "present"

    invoke-direct {v0, v2, v1}, Lcom/squareup/server/payment/CustomerPresence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/server/payment/CustomerPresence;->present:Lcom/squareup/server/payment/CustomerPresence;

    new-instance v0, Lcom/squareup/server/payment/CustomerPresence;

    const/4 v2, 0x1

    const-string v3, "phone"

    invoke-direct {v0, v3, v2}, Lcom/squareup/server/payment/CustomerPresence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/server/payment/CustomerPresence;->phone:Lcom/squareup/server/payment/CustomerPresence;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/server/payment/CustomerPresence;

    .line 12
    sget-object v3, Lcom/squareup/server/payment/CustomerPresence;->present:Lcom/squareup/server/payment/CustomerPresence;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/server/payment/CustomerPresence;->phone:Lcom/squareup/server/payment/CustomerPresence;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/server/payment/CustomerPresence;->$VALUES:[Lcom/squareup/server/payment/CustomerPresence;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/payment/CustomerPresence;
    .locals 1

    .line 12
    const-class v0, Lcom/squareup/server/payment/CustomerPresence;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/payment/CustomerPresence;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/payment/CustomerPresence;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/server/payment/CustomerPresence;->$VALUES:[Lcom/squareup/server/payment/CustomerPresence;

    invoke-virtual {v0}, [Lcom/squareup/server/payment/CustomerPresence;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/payment/CustomerPresence;

    return-object v0
.end method
