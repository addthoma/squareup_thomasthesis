.class public interface abstract Lcom/squareup/server/payment/BillRefundService;
.super Ljava/lang/Object;
.source "BillRefundService.java"


# virtual methods
.method public abstract issueRefunds(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bills/IssueRefundsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/IssueRefundsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bills/IssueRefundsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bills/issue-refunds"
    .end annotation
.end method
