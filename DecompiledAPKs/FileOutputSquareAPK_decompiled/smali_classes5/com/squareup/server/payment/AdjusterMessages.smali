.class public Lcom/squareup/server/payment/AdjusterMessages;
.super Ljava/lang/Object;
.source "AdjusterMessages.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asAdjustmentMessages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/Adjustment;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/value/Adjustment;

    .line 47
    invoke-static {v1}, Lcom/squareup/server/payment/AdjusterMessages;->asMessage(Lcom/squareup/server/payment/value/Adjustment;)Lcom/squareup/server/payment/AdjustmentMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static asItemModifierMessages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModifier;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModifierMessage;",
            ">;"
        }
    .end annotation

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 88
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/ItemModifier;

    invoke-static {v1}, Lcom/squareup/server/payment/AdjusterMessages;->asMessage(Lcom/squareup/server/payment/ItemModifier;)Lcom/squareup/server/payment/ItemModifierMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static asItemizationMessages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/Itemization;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/Itemization;

    .line 29
    invoke-static {v1}, Lcom/squareup/server/payment/AdjusterMessages;->asMessage(Lcom/squareup/server/payment/Itemization;)Lcom/squareup/server/payment/ItemizationMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static asItemizedAdjustmentMessages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizedAdjustmentMessage;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 67
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 68
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/value/ItemizedAdjustment;

    .line 69
    invoke-static {v1}, Lcom/squareup/server/payment/AdjusterMessages;->asMessage(Lcom/squareup/server/payment/value/ItemizedAdjustment;)Lcom/squareup/server/payment/ItemizedAdjustmentMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static asMessage(Lcom/squareup/server/payment/value/Adjustment;)Lcom/squareup/server/payment/AdjustmentMessage;
    .locals 13

    .line 115
    new-instance v12, Lcom/squareup/server/payment/AdjustmentMessage;

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getTypeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getApplied()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 116
    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getCalculationPhase()I

    move-result v5

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getRawInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v6

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v8

    .line 117
    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v9

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getChildAdjustments()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/server/payment/AdjusterMessages;->asItemizedAdjustmentMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getCouponToken()Ljava/lang/String;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/payment/AdjustmentMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ILcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/lang/String;)V

    return-object v12
.end method

.method private static asMessage(Lcom/squareup/server/payment/ItemModifier;)Lcom/squareup/server/payment/ItemModifierMessage;
    .locals 3

    .line 133
    new-instance v0, Lcom/squareup/server/payment/ItemModifierMessage;

    iget-object v1, p0, Lcom/squareup/server/payment/ItemModifier;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/payment/ItemModifier;->name:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/server/payment/ItemModifier;->price_money:Lcom/squareup/protos/common/Money;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/server/payment/ItemModifierMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method private static asMessage(Lcom/squareup/server/payment/Itemization;)Lcom/squareup/server/payment/ItemizationMessage;
    .locals 15

    .line 101
    iget-object v0, p0, Lcom/squareup/server/payment/Itemization;->item_id:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getItemName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->image_token:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getNotes()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/server/payment/Itemization;->item_variation_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/server/payment/Itemization;->item_variation_name:Ljava/lang/String;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getItemQuantity()Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getQuantityPrecision()I

    move-result v7

    .line 103
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getPriceMoney()Lcom/squareup/protos/common/Money;

    move-result-object v10

    iget-object v11, p0, Lcom/squareup/server/payment/Itemization;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v12, p0, Lcom/squareup/server/payment/Itemization;->adjustments:Ljava/util/List;

    .line 104
    invoke-static {v12}, Lcom/squareup/server/payment/AdjusterMessages;->asItemizedAdjustmentMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    iget-object v13, p0, Lcom/squareup/server/payment/Itemization;->applied_modifier_options:Ljava/util/List;

    .line 105
    invoke-static {v13}, Lcom/squareup/server/payment/AdjusterMessages;->asItemModifierMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    iget-object v14, p0, Lcom/squareup/server/payment/Itemization;->color:Ljava/lang/String;

    .line 101
    invoke-static/range {v0 .. v14}, Lcom/squareup/server/payment/ItemizationMessage;->forRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/server/payment/ItemizationMessage;

    move-result-object p0

    return-object p0
.end method

.method private static asMessage(Lcom/squareup/server/payment/value/ItemizedAdjustment;)Lcom/squareup/server/payment/ItemizedAdjustmentMessage;
    .locals 2

    .line 121
    new-instance v0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/ItemizedAdjustment;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/server/payment/value/ItemizedAdjustment;->getApplied()Lcom/squareup/protos/common/Money;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public static fromAdjustmentMessages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/Adjustment;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_0

    .line 37
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/AdjustmentMessage;

    .line 38
    invoke-static {v1}, Lcom/squareup/server/payment/AdjusterMessages;->fromMessage(Lcom/squareup/server/payment/AdjustmentMessage;)Lcom/squareup/server/payment/value/Adjustment;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static fromItemModifierMessages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModifierMessage;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModifier;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 79
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 80
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/ItemModifierMessage;

    .line 81
    invoke-static {v1}, Lcom/squareup/server/payment/AdjusterMessages;->fromMessage(Lcom/squareup/server/payment/ItemModifierMessage;)Lcom/squareup/server/payment/ItemModifier;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static fromItemizationMessages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/Itemization;",
            ">;"
        }
    .end annotation

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_0

    .line 19
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/ItemizationMessage;

    .line 20
    invoke-static {v1}, Lcom/squareup/server/payment/AdjusterMessages;->fromMessage(Lcom/squareup/server/payment/ItemizationMessage;)Lcom/squareup/server/payment/Itemization;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static fromItemizedAdjustmentMessages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizedAdjustmentMessage;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 55
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;

    .line 57
    invoke-static {v1}, Lcom/squareup/server/payment/AdjusterMessages;->fromMessage(Lcom/squareup/server/payment/ItemizedAdjustmentMessage;)Lcom/squareup/server/payment/value/ItemizedAdjustment;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static fromMessage(Lcom/squareup/server/payment/ItemModifierMessage;)Lcom/squareup/server/payment/ItemModifier;
    .locals 3

    .line 129
    new-instance v0, Lcom/squareup/server/payment/ItemModifier;

    iget-object v1, p0, Lcom/squareup/server/payment/ItemModifierMessage;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/payment/ItemModifierMessage;->name:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/server/payment/ItemModifierMessage;->price_money:Lcom/squareup/protos/common/Money;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/server/payment/ItemModifier;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method private static fromMessage(Lcom/squareup/server/payment/ItemizationMessage;)Lcom/squareup/server/payment/Itemization;
    .locals 15

    .line 93
    iget-object v0, p0, Lcom/squareup/server/payment/ItemizationMessage;->item_id:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/server/payment/ItemizationMessage;->getItemName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/server/payment/ItemizationMessage;->image_token:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/server/payment/ItemizationMessage;->getNotes()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/server/payment/ItemizationMessage;->item_variation_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/server/payment/ItemizationMessage;->item_variation_name:Ljava/lang/String;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/server/payment/ItemizationMessage;->getItemQuantity()Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {p0}, Lcom/squareup/server/payment/ItemizationMessage;->getQuantityPrecision()I

    move-result v7

    .line 95
    invoke-virtual {p0}, Lcom/squareup/server/payment/ItemizationMessage;->getUnitName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/squareup/server/payment/ItemizationMessage;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/squareup/server/payment/ItemizationMessage;->getPriceMoney()Lcom/squareup/protos/common/Money;

    move-result-object v10

    iget-object v11, p0, Lcom/squareup/server/payment/ItemizationMessage;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v12, p0, Lcom/squareup/server/payment/ItemizationMessage;->adjustments:Ljava/util/List;

    .line 96
    invoke-static {v12}, Lcom/squareup/server/payment/AdjusterMessages;->fromItemizedAdjustmentMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    iget-object v13, p0, Lcom/squareup/server/payment/ItemizationMessage;->applied_modifier_options:Ljava/util/List;

    .line 97
    invoke-static {v13}, Lcom/squareup/server/payment/AdjusterMessages;->fromItemModifierMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    iget-object v14, p0, Lcom/squareup/server/payment/ItemizationMessage;->color:Ljava/lang/String;

    .line 93
    invoke-static/range {v0 .. v14}, Lcom/squareup/server/payment/Itemization;->forRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/server/payment/Itemization;

    move-result-object p0

    return-object p0
.end method

.method private static fromMessage(Lcom/squareup/server/payment/AdjustmentMessage;)Lcom/squareup/server/payment/value/Adjustment;
    .locals 13

    .line 109
    new-instance v12, Lcom/squareup/server/payment/value/Adjustment;

    invoke-virtual {p0}, Lcom/squareup/server/payment/AdjustmentMessage;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/server/payment/AdjustmentMessage;->getType()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    iget v5, p0, Lcom/squareup/server/payment/AdjustmentMessage;->calculation_phase:I

    .line 110
    invoke-virtual {p0}, Lcom/squareup/server/payment/AdjustmentMessage;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/server/payment/AdjustmentMessage;->name:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/server/payment/AdjustmentMessage;->percentage:Lcom/squareup/util/Percentage;

    iget-object v9, p0, Lcom/squareup/server/payment/AdjustmentMessage;->amount_money:Lcom/squareup/protos/common/Money;

    iget-object v0, p0, Lcom/squareup/server/payment/AdjustmentMessage;->child_adjustments:Ljava/util/List;

    .line 111
    invoke-static {v0}, Lcom/squareup/server/payment/AdjusterMessages;->fromItemizedAdjustmentMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    iget-object v11, p0, Lcom/squareup/server/payment/AdjustmentMessage;->coupon_token:Ljava/lang/String;

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/payment/value/Adjustment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ILcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/lang/String;)V

    return-object v12
.end method

.method private static fromMessage(Lcom/squareup/server/payment/ItemizedAdjustmentMessage;)Lcom/squareup/server/payment/value/ItemizedAdjustment;
    .locals 2

    .line 125
    new-instance v0, Lcom/squareup/server/payment/value/ItemizedAdjustment;

    iget-object v1, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->id:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/server/payment/ItemizedAdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-direct {v0, v1, p0}, Lcom/squareup/server/payment/value/ItemizedAdjustment;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method
