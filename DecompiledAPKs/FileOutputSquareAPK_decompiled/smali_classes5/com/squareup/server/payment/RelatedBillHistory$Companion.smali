.class public final Lcom/squareup/server/payment/RelatedBillHistory$Companion;
.super Ljava/lang/Object;
.source "RelatedBillHistory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/payment/RelatedBillHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRelatedBillHistory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RelatedBillHistory.kt\ncom/squareup/server/payment/RelatedBillHistory$Companion\n*L\n1#1,284:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007J\u0018\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\u0008H\u0007J\u0018\u0010\u000c\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0011H\u0007J$\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0007J\u001c\u0010\u0012\u001a\u00020\u00062\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0007J\u0012\u0010\u0019\u001a\u00020\u00042\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0006H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/server/payment/RelatedBillHistory$Companion;",
        "",
        "()V",
        "FUTURE_TIME",
        "",
        "fromBill",
        "Lcom/squareup/server/payment/RelatedBillHistory;",
        "bill",
        "Lcom/squareup/protos/client/bills/Bill;",
        "fromRefund",
        "refund",
        "Lcom/squareup/protos/client/bills/Refund;",
        "fromRefundV1",
        "Lcom/squareup/protos/client/bills/RefundV1;",
        "billIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "getCreatedByDescendingComparator",
        "Ljava/util/Comparator;",
        "of",
        "tenderId",
        "",
        "refundedMoney",
        "Lcom/squareup/protos/common/Money;",
        "state",
        "Lcom/squareup/server/payment/RefundHistoryState;",
        "timestamp",
        "relatedBill",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$timestamp(Lcom/squareup/server/payment/RelatedBillHistory$Companion;Lcom/squareup/server/payment/RelatedBillHistory;)J
    .locals 0

    .line 115
    invoke-direct {p0, p1}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->timestamp(Lcom/squareup/server/payment/RelatedBillHistory;)J

    move-result-wide p0

    return-wide p0
.end method

.method private final timestamp(Lcom/squareup/server/payment/RelatedBillHistory;)J
    .locals 3

    const-wide v0, 0xe677d21fdc00L

    if-eqz p1, :cond_0

    .line 147
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getCreatedAt()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 146
    invoke-static {p1}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 150
    check-cast p1, Ljava/lang/Throwable;

    const-string v2, "Unable to parse refund date"

    invoke-static {p1, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-wide v0
.end method


# virtual methods
.method public final fromBill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 16
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "bill"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    new-instance v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 267
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const-string v2, "bill.bill_id_pair"

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Bill$Dates;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v2, :cond_0

    iget-object v2, v2, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    move-object v9, v2

    goto :goto_0

    :cond_0
    move-object v9, v4

    :goto_0
    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 276
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-object v12, v2

    goto :goto_1

    :cond_1
    move-object v12, v4

    .line 277
    :goto_1
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v2, :cond_2

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    move-object v13, v2

    goto :goto_2

    :cond_2
    move-object v13, v4

    .line 278
    :goto_2
    iget-object v14, v0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    const/4 v15, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, v1

    .line 266
    invoke-direct/range {v2 .. v15}, Lcom/squareup/server/payment/RelatedBillHistory;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Refund$Destination;)V

    return-object v1
.end method

.method public final fromRefund(Lcom/squareup/protos/client/bills/Refund;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 18
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "refund"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "bill"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 239
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 241
    iget-object v4, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v4, :cond_0

    .line 243
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v3, v3, Lcom/squareup/protos/client/Employee;->employee_token:Ljava/lang/String;

    :cond_0
    move-object v10, v3

    .line 246
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Refund;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/Refund;->DEFAULT_REASON_OPTION:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    :goto_0
    move-object v8, v3

    .line 247
    new-instance v3, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 248
    iget-object v5, v1, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const-string v4, "bill.bill_id_pair"

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    iget-object v4, v0, Lcom/squareup/protos/client/bills/Refund;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v4, :cond_2

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    move-object v6, v4

    goto :goto_1

    :cond_2
    move-object v6, v2

    .line 250
    :goto_1
    iget-object v7, v0, Lcom/squareup/protos/client/bills/Refund;->reason:Ljava/lang/String;

    .line 252
    iget-object v9, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    .line 254
    iget-object v4, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v4, :cond_3

    iget-object v4, v4, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    move-object v11, v4

    goto :goto_2

    :cond_3
    move-object v11, v2

    .line 255
    :goto_2
    iget-object v12, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    .line 256
    sget-object v4, Lcom/squareup/server/payment/RefundHistoryState;->Companion:Lcom/squareup/server/payment/RefundHistoryState$Companion;

    iget-object v13, v0, Lcom/squareup/protos/client/bills/Refund;->state:Lcom/squareup/protos/client/bills/Refund$State;

    invoke-virtual {v4, v13}, Lcom/squareup/server/payment/RefundHistoryState$Companion;->fromRefundState(Lcom/squareup/protos/client/bills/Refund$State;)Lcom/squareup/server/payment/RefundHistoryState;

    move-result-object v13

    .line 257
    iget-object v4, v1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v4, :cond_4

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-object v14, v4

    goto :goto_3

    :cond_4
    move-object v14, v2

    .line 258
    :goto_3
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_5

    iget-object v2, v1, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    :cond_5
    move-object v15, v2

    const/16 v16, 0x0

    .line 260
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Refund;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    move-object v4, v3

    move-object/from16 v17, v0

    .line 247
    invoke-direct/range {v4 .. v17}, Lcom/squareup/server/payment/RelatedBillHistory;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Refund$Destination;)V

    return-object v3
.end method

.method public final fromRefundV1(Lcom/squareup/protos/client/bills/RefundV1;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 17
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "refund"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "billIdPair"

    move-object/from16 v3, p2

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 213
    move-object v2, v1

    check-cast v2, Ljava/lang/String;

    .line 214
    iget-object v4, v0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v4, :cond_0

    .line 215
    iget-object v2, v0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v2, v2, Lcom/squareup/protos/client/Employee;->employee_token:Ljava/lang/String;

    :cond_0
    move-object v8, v2

    .line 217
    new-instance v16, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 219
    iget-object v2, v0, Lcom/squareup/protos/client/bills/RefundV1;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 220
    iget-object v5, v0, Lcom/squareup/protos/client/bills/RefundV1;->refund_reason:Ljava/lang/String;

    .line 221
    sget-object v6, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 222
    iget-object v2, v0, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    :goto_0
    move-object v7, v1

    .line 224
    iget-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v9, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 225
    iget-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    iget-object v10, v1, Lcom/squareup/protos/client/bills/RefundV1$Amounts;->refunded_money:Lcom/squareup/protos/common/Money;

    .line 226
    sget-object v1, Lcom/squareup/server/payment/RefundHistoryState;->Companion:Lcom/squareup/server/payment/RefundHistoryState$Companion;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/RefundV1;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v1, v0}, Lcom/squareup/server/payment/RefundHistoryState$Companion;->fromRefundV1State(Lcom/squareup/protos/client/bills/RefundV1$State;)Lcom/squareup/server/payment/RefundHistoryState;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, v16

    move-object/from16 v3, p2

    .line 217
    invoke-direct/range {v2 .. v15}, Lcom/squareup/server/payment/RelatedBillHistory;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Refund$Destination;)V

    return-object v16
.end method

.method public final getCreatedByDescendingComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 123
    sget-object v0, Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;->INSTANCE:Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;

    check-cast v0, Ljava/util/Comparator;

    return-object v0
.end method

.method public final of(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 15
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 164
    new-instance v14, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 165
    new-instance v1, Lcom/squareup/protos/client/IdPair;

    const-string v0, ""

    invoke-direct {v1, v0, v0}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    sget-object v4, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 173
    sget-object v9, Lcom/squareup/server/payment/RefundHistoryState;->COMPLETED:Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x0

    const-string v3, "Test Reason"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, v14

    move-object/from16 v5, p1

    move-object/from16 v8, p2

    .line 164
    invoke-direct/range {v0 .. v13}, Lcom/squareup/server/payment/RelatedBillHistory;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Refund$Destination;)V

    return-object v14
.end method

.method public final of(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 15
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "tenderId"

    move-object/from16 v6, p1

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    new-instance v0, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 192
    new-instance v2, Lcom/squareup/protos/client/IdPair;

    const-string v1, ""

    invoke-direct {v2, v1, v1}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    sget-object v5, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, v0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    .line 191
    invoke-direct/range {v1 .. v14}, Lcom/squareup/server/payment/RelatedBillHistory;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Refund$Destination;)V

    return-object v0
.end method
