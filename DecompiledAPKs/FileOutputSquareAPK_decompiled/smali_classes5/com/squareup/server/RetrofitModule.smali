.class public final Lcom/squareup/server/RetrofitModule;
.super Ljava/lang/Object;
.source "RetrofitModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0011\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J>\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0004H\u0002J>\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u0014\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u0012\u0010\u0015\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0004H\u0007J@\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u0014\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u0012\u0010\u0017\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0004H\u0007J@\u0010\u0018\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u0012\u0010\u0019\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0004H\u0007J\u0012\u0010\u001a\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0004H\u0007J@\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u0014\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007JB\u0010\u001c\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u0012\u0010\u001d\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0004H\u0007J\u0012\u0010\u001e\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0004H\u0007J@\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u0014\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007JB\u0010 \u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u0012\u0010!\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0004H\u0007\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/server/RetrofitModule;",
        "",
        "()V",
        "createRetrofit",
        "Lretrofit2/Retrofit;",
        "server",
        "Lcom/squareup/http/Server;",
        "client",
        "Lokhttp3/OkHttpClient;",
        "rpcScheduler",
        "Lio/reactivex/Scheduler;",
        "mainScheduler",
        "gson",
        "Lcom/google/gson/Gson;",
        "standardReceiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "createRetrofitServiceCreator",
        "Lcom/squareup/api/ServiceCreator;",
        "retrofit",
        "provideAuthenticatedRetrofit",
        "scheduler",
        "provideAuthenticatedServiceCreator",
        "provideCogsAuthenticatedRetrofit",
        "provideCogsAuthenticatedServiceCreator",
        "provideConnectRetrofit",
        "provideConnectServiceCreator",
        "provideFelicaAuthenticatedServiceCreator",
        "provideFelicatAuthenticatedRetrofit",
        "provideUnauthenticatedConnectRetrofit",
        "provideUnauthenticatedConnectServiceCreator",
        "provideUnauthenticatedProtoServiceCreator",
        "provideUnauthenticatedRetrofit",
        "provideUnauthenticatedWeeblyRetrofit",
        "provideWeeblyServiceCreator",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/server/RetrofitModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/server/RetrofitModule;

    invoke-direct {v0}, Lcom/squareup/server/RetrofitModule;-><init>()V

    sput-object v0, Lcom/squareup/server/RetrofitModule;->INSTANCE:Lcom/squareup/server/RetrofitModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param

    .line 167
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 168
    invoke-virtual {p1}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 169
    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 170
    new-instance p2, Lcom/squareup/receiving/retrofit/StandardResponseConverterFactory;

    invoke-direct {p2}, Lcom/squareup/receiving/retrofit/StandardResponseConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 171
    new-instance p2, Lcom/squareup/receiving/retrofit/OptionalConverterFactory;

    invoke-direct {p2}, Lcom/squareup/receiving/retrofit/OptionalConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 176
    new-instance p2, Lcom/squareup/receiving/retrofit/WireGsonConverterFactory;

    invoke-direct {p2, p5}, Lcom/squareup/receiving/retrofit/WireGsonConverterFactory;-><init>(Lcom/google/gson/Gson;)V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 179
    invoke-static {}, Lretrofit2/converter/wire/WireConverterFactory;->create()Lretrofit2/converter/wire/WireConverterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 182
    new-instance p2, Lcom/squareup/server/BlindProtoConverterFactory;

    invoke-direct {p2}, Lcom/squareup/server/BlindProtoConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 185
    invoke-static {p5}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/Gson;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 187
    new-instance p2, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;

    invoke-direct {p2, p3, p4, p6}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;-><init>(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/receiving/StandardReceiver;)V

    check-cast p2, Lretrofit2/CallAdapter$Factory;

    .line 186
    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 193
    invoke-static {p3}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->createWithScheduler(Lio/reactivex/Scheduler;)Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/CallAdapter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 194
    invoke-virtual {p1}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object p1

    const-string p2, "Retrofit.Builder()\n     \u2026eduler))\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final createRetrofitServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1

    .line 232
    new-instance v0, Lcom/squareup/server/RetrofitServiceCreator;

    invoke-direct {v0, p1}, Lcom/squareup/server/RetrofitServiceCreator;-><init>(Lretrofit2/Retrofit;)V

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    return-object v0
.end method


# virtual methods
.method public final provideAuthenticatedRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct/range {p0 .. p6}, Lcom/squareup/server/RetrofitModule;->createRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;

    move-result-object p1

    return-object p1
.end method

.method public final provideAuthenticatedServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .param p1    # Lretrofit2/Retrofit;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    invoke-direct {p0, p1}, Lcom/squareup/server/RetrofitModule;->createRetrofitServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object p1

    return-object p1
.end method

.method public final provideCogsAuthenticatedRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 1
    .param p2    # Lokhttp3/OkHttpClient;
        .annotation runtime Lcom/squareup/http/Cogs;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitCogs;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct/range {p0 .. p6}, Lcom/squareup/server/RetrofitModule;->createRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;

    move-result-object p1

    return-object p1
.end method

.method public final provideCogsAuthenticatedServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .param p1    # Lretrofit2/Retrofit;
        .annotation runtime Lcom/squareup/api/RetrofitCogs;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitCogs;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    invoke-direct {p0, p1}, Lcom/squareup/server/RetrofitModule;->createRetrofitServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object p1

    return-object p1
.end method

.method public final provideConnectRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 1
    .param p1    # Lcom/squareup/http/Server;
        .annotation runtime Lcom/squareup/api/Connect;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitConnect;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rpcScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 91
    invoke-virtual {p1}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 92
    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 93
    new-instance p2, Lcom/squareup/receiving/retrofit/StandardResponseConverterFactory;

    invoke-direct {p2}, Lcom/squareup/receiving/retrofit/StandardResponseConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 94
    new-instance p2, Lcom/squareup/receiving/retrofit/OptionalConverterFactory;

    invoke-direct {p2}, Lcom/squareup/receiving/retrofit/OptionalConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 95
    invoke-static {p5}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/Gson;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 97
    new-instance p2, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;

    invoke-direct {p2, p3, p4, p6}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;-><init>(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/receiving/StandardReceiver;)V

    check-cast p2, Lretrofit2/CallAdapter$Factory;

    .line 96
    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 103
    invoke-static {p3}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->createWithScheduler(Lio/reactivex/Scheduler;)Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/CallAdapter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object p1

    const-string p2, "Retrofit.Builder()\n     \u2026eduler))\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final provideConnectServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .param p1    # Lretrofit2/Retrofit;
        .annotation runtime Lcom/squareup/api/RetrofitConnect;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitConnect;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    invoke-direct {p0, p1}, Lcom/squareup/server/RetrofitModule;->createRetrofitServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object p1

    return-object p1
.end method

.method public final provideFelicaAuthenticatedServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .param p1    # Lretrofit2/Retrofit;
        .annotation runtime Lcom/squareup/http/FelicaAuthenticated;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Lcom/squareup/http/FelicaAuthenticated;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-direct {p0, p1}, Lcom/squareup/server/RetrofitModule;->createRetrofitServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object p1

    return-object p1
.end method

.method public final provideFelicatAuthenticatedRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 1
    .param p2    # Lokhttp3/OkHttpClient;
        .annotation runtime Lcom/squareup/http/FelicaAuthenticated;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Lcom/squareup/http/FelicaAuthenticated;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct/range {p0 .. p6}, Lcom/squareup/server/RetrofitModule;->createRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;

    move-result-object p1

    return-object p1
.end method

.method public final provideUnauthenticatedConnectRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 1
    .param p1    # Lcom/squareup/http/Server;
        .annotation runtime Lcom/squareup/api/Connect;
        .end annotation
    .end param
    .param p2    # Lokhttp3/OkHttpClient;
        .annotation runtime Lcom/squareup/http/UnauthenticatedClient;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitConnectUnauthenticated;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rpcScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 118
    invoke-virtual {p1}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 119
    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 120
    new-instance p2, Lcom/squareup/receiving/retrofit/StandardResponseConverterFactory;

    invoke-direct {p2}, Lcom/squareup/receiving/retrofit/StandardResponseConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 121
    new-instance p2, Lcom/squareup/receiving/retrofit/OptionalConverterFactory;

    invoke-direct {p2}, Lcom/squareup/receiving/retrofit/OptionalConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 122
    invoke-static {p5}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/Gson;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 124
    new-instance p2, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;

    invoke-direct {p2, p3, p4, p6}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;-><init>(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/receiving/StandardReceiver;)V

    check-cast p2, Lretrofit2/CallAdapter$Factory;

    .line 123
    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 130
    invoke-static {p3}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->createWithScheduler(Lio/reactivex/Scheduler;)Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/CallAdapter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object p1

    const-string p2, "Retrofit.Builder()\n     \u2026eduler))\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final provideUnauthenticatedConnectServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .param p1    # Lretrofit2/Retrofit;
        .annotation runtime Lcom/squareup/api/RetrofitConnectUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitConnectUnauthenticated;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    invoke-direct {p0, p1}, Lcom/squareup/server/RetrofitModule;->createRetrofitServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object p1

    return-object p1
.end method

.method public final provideUnauthenticatedProtoServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .param p1    # Lretrofit2/Retrofit;
        .annotation runtime Lcom/squareup/api/RetrofitUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitUnauthenticated;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    invoke-direct {p0, p1}, Lcom/squareup/server/RetrofitModule;->createRetrofitServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object p1

    return-object p1
.end method

.method public final provideUnauthenticatedRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 1
    .param p2    # Lokhttp3/OkHttpClient;
        .annotation runtime Lcom/squareup/http/UnauthenticatedClient;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitUnauthenticated;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-direct/range {p0 .. p6}, Lcom/squareup/server/RetrofitModule;->createRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;

    move-result-object p1

    return-object p1
.end method

.method public final provideUnauthenticatedWeeblyRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 1
    .param p1    # Lcom/squareup/http/Server;
        .annotation runtime Lcom/squareup/api/Weebly;
        .end annotation
    .end param
    .param p2    # Lokhttp3/OkHttpClient;
        .annotation runtime Lcom/squareup/http/UnauthenticatedClient;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitWeeblyUnauthenticated;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rpcScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 144
    invoke-virtual {p1}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 145
    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 146
    new-instance p2, Lcom/squareup/receiving/retrofit/StandardResponseConverterFactory;

    invoke-direct {p2}, Lcom/squareup/receiving/retrofit/StandardResponseConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 147
    new-instance p2, Lcom/squareup/receiving/retrofit/OptionalConverterFactory;

    invoke-direct {p2}, Lcom/squareup/receiving/retrofit/OptionalConverterFactory;-><init>()V

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 148
    invoke-static {p5}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/Gson;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/Converter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 150
    new-instance p2, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;

    invoke-direct {p2, p3, p4, p6}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;-><init>(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/receiving/StandardReceiver;)V

    check-cast p2, Lretrofit2/CallAdapter$Factory;

    .line 149
    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 156
    invoke-static {p3}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->createWithScheduler(Lio/reactivex/Scheduler;)Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object p2

    check-cast p2, Lretrofit2/CallAdapter$Factory;

    invoke-virtual {p1, p2}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object p1

    .line 157
    invoke-virtual {p1}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object p1

    const-string p2, "Retrofit.Builder()\n     \u2026eduler))\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final provideWeeblyServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .param p1    # Lretrofit2/Retrofit;
        .annotation runtime Lcom/squareup/api/RetrofitWeeblyUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RetrofitWeeblyUnauthenticated;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/server/RetrofitModule;->createRetrofitServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object p1

    return-object p1
.end method
