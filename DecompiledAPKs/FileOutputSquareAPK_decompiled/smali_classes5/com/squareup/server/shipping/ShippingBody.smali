.class public Lcom/squareup/server/shipping/ShippingBody;
.super Ljava/lang/Object;
.source "ShippingBody.java"


# instance fields
.field public final city:Ljava/lang/String;

.field public final country_code:Lcom/squareup/CountryCode;

.field public final name:Ljava/lang/String;

.field public final postal_code:Ljava/lang/String;

.field public final state:Ljava/lang/String;

.field public final street1:Ljava/lang/String;

.field public final street2:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/shipping/ShippingBody;->name:Ljava/lang/String;

    .line 21
    invoke-static {p2}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/shipping/ShippingBody;->street1:Ljava/lang/String;

    .line 22
    invoke-static {p3}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/shipping/ShippingBody;->street2:Ljava/lang/String;

    .line 23
    invoke-static {p4}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/shipping/ShippingBody;->city:Ljava/lang/String;

    .line 24
    invoke-static {p5}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/shipping/ShippingBody;->state:Ljava/lang/String;

    .line 25
    invoke-static {p6}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/shipping/ShippingBody;->postal_code:Ljava/lang/String;

    .line 26
    iput-object p7, p0, Lcom/squareup/server/shipping/ShippingBody;->country_code:Lcom/squareup/CountryCode;

    return-void
.end method

.method public static from(Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/server/shipping/ShippingBody;
    .locals 9

    .line 30
    new-instance v8, Lcom/squareup/server/shipping/ShippingBody;

    iget-object v2, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    iget-object v5, p1, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    iget-object v6, p1, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 32
    invoke-virtual {p1}, Lcom/squareup/protos/common/countries/Country;->name()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v7

    move-object v0, v8

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/shipping/ShippingBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object v8
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 37
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 38
    :cond_1
    check-cast p1, Lcom/squareup/server/shipping/ShippingBody;

    .line 39
    iget-object v2, p0, Lcom/squareup/server/shipping/ShippingBody;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/shipping/ShippingBody;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/shipping/ShippingBody;->street1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/shipping/ShippingBody;->street1:Ljava/lang/String;

    .line 40
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/shipping/ShippingBody;->street2:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/shipping/ShippingBody;->street2:Ljava/lang/String;

    .line 41
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/shipping/ShippingBody;->city:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/shipping/ShippingBody;->city:Ljava/lang/String;

    .line 42
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/shipping/ShippingBody;->state:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/shipping/ShippingBody;->state:Ljava/lang/String;

    .line 43
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/shipping/ShippingBody;->postal_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/shipping/ShippingBody;->postal_code:Ljava/lang/String;

    .line 44
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/shipping/ShippingBody;->country_code:Lcom/squareup/CountryCode;

    iget-object p1, p1, Lcom/squareup/server/shipping/ShippingBody;->country_code:Lcom/squareup/CountryCode;

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    iget-object v1, p0, Lcom/squareup/server/shipping/ShippingBody;->name:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/shipping/ShippingBody;->street1:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/shipping/ShippingBody;->street2:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/shipping/ShippingBody;->city:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/shipping/ShippingBody;->state:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/shipping/ShippingBody;->postal_code:Ljava/lang/String;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/shipping/ShippingBody;->country_code:Lcom/squareup/CountryCode;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
