.class public interface abstract Lcom/squareup/server/SafetyNetService;
.super Ljava/lang/Object;
.source "SafetyNetService.java"


# virtual methods
.method public abstract startAttestation(Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationRequest;)Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;
    .param p1    # Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/safetynet/startattestation"
    .end annotation
.end method

.method public abstract validateAttestation(Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest;)Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse;
    .param p1    # Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/safetynet/validateattestation"
    .end annotation
.end method
