.class public interface abstract Lcom/squareup/server/orderhub/OrderHubService;
.super Ljava/lang/Object;
.source "OrderHubService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\t\u001a\u00020\nH\'J\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00032\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\'J\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u00032\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0012H\'J\u0018\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00032\u0008\u0008\u0001\u0010\u0015\u001a\u00020\u0016H\'\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/server/orderhub/OrderHubService;",
        "",
        "getActiveOrderCount",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;",
        "getActiveOrderCountRequest",
        "Lcom/squareup/protos/client/orders/GetActiveOrderCountRequest;",
        "getOrder",
        "Lcom/squareup/protos/client/orders/GetOrderResponse;",
        "getOrderRequest",
        "Lcom/squareup/protos/client/orders/GetOrderRequest;",
        "getOrders",
        "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
        "searchRequest",
        "Lcom/squareup/protos/client/orders/SearchOrdersRequest;",
        "performFulfillmentAction",
        "Lcom/squareup/protos/client/orders/PerformFulfillmentActionResponse;",
        "fulfillmentRequest",
        "Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;",
        "updateOrder",
        "Lcom/squareup/protos/client/orders/UpdateOrderResponse;",
        "updateOrderRequest",
        "Lcom/squareup/protos/client/orders/UpdateOrderRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getActiveOrderCount(Lcom/squareup/protos/client/orders/GetActiveOrderCountRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/orders/GetActiveOrderCountRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/GetActiveOrderCountRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/orders/get-active-order-count"
    .end annotation
.end method

.method public abstract getOrder(Lcom/squareup/protos/client/orders/GetOrderRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/orders/GetOrderRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/GetOrderRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/orders/GetOrderResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/orders/get-order"
    .end annotation
.end method

.method public abstract getOrders(Lcom/squareup/protos/client/orders/SearchOrdersRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/orders/SearchOrdersRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/SearchOrdersRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/orders/search-orders"
    .end annotation
.end method

.method public abstract performFulfillmentAction(Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/orders/PerformFulfillmentActionResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/orders/perform-fulfillment-action"
    .end annotation
.end method

.method public abstract updateOrder(Lcom/squareup/protos/client/orders/UpdateOrderRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/orders/UpdateOrderRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/UpdateOrderRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/orders/UpdateOrderResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/orders/update-order"
    .end annotation
.end method
