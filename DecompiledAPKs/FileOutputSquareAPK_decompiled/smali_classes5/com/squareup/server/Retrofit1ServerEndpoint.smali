.class final Lcom/squareup/server/Retrofit1ServerEndpoint;
.super Ljava/lang/Object;
.source "Retrofit1ServerEndpoint.kt"

# interfaces
.implements Lretrofit/Endpoint;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/server/Retrofit1ServerEndpoint;",
        "Lretrofit/Endpoint;",
        "server",
        "Lcom/squareup/http/Server;",
        "(Lcom/squareup/http/Server;)V",
        "getName",
        "",
        "getUrl",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final server:Lcom/squareup/http/Server;


# direct methods
.method public constructor <init>(Lcom/squareup/http/Server;)V
    .locals 1

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/server/Retrofit1ServerEndpoint;->server:Lcom/squareup/http/Server;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 2

    .line 10
    iget-object v0, p0, Lcom/squareup/server/Retrofit1ServerEndpoint;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "server.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    .line 9
    iget-object v0, p0, Lcom/squareup/server/Retrofit1ServerEndpoint;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "server.url"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
