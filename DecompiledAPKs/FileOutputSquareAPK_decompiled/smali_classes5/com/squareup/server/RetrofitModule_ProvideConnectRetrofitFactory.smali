.class public final Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;
.super Ljava/lang/Object;
.source "RetrofitModule_ProvideConnectRetrofitFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lretrofit2/Retrofit;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->serverProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->clientProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->standardReceiverProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;)",
            "Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static provideConnectRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;
    .locals 7

    .line 62
    sget-object v0, Lcom/squareup/server/RetrofitModule;->INSTANCE:Lcom/squareup/server/RetrofitModule;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/server/RetrofitModule;->provideConnectRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lretrofit2/Retrofit;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->get()Lretrofit2/Retrofit;

    move-result-object v0

    return-object v0
.end method

.method public get()Lretrofit2/Retrofit;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/http/Server;

    iget-object v0, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->clientProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lokhttp3/OkHttpClient;

    iget-object v0, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->standardReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/receiving/StandardReceiver;

    invoke-static/range {v1 .. v6}, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->provideConnectRetrofit(Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/receiving/StandardReceiver;)Lretrofit2/Retrofit;

    move-result-object v0

    return-object v0
.end method
