.class public interface abstract Lcom/squareup/server/coupons/CouponsService;
.super Ljava/lang/Object;
.source "CouponsService.java"


# virtual methods
.method public abstract lookup(Lcom/squareup/protos/client/coupons/LookupCouponsRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/coupons/LookupCouponsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/coupons/LookupCouponsRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/coupons/v2/lookup"
    .end annotation
.end method
