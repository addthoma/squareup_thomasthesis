.class public interface abstract Lcom/squareup/server/employees/EmployeesService;
.super Ljava/lang/Object;
.source "EmployeesService.java"


# virtual methods
.method public abstract clockIn(Lcom/squareup/server/employees/ClockInOutBody;)Lrx/Observable;
    .param p1    # Lcom/squareup/server/employees/ClockInOutBody;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/employees/ClockInOutBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/server/employees/ClockInOutResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/api/v1/employee-timecards/clockin"
    .end annotation
.end method

.method public abstract clockOut(Lcom/squareup/server/employees/ClockInOutBody;)Lrx/Observable;
    .param p1    # Lcom/squareup/server/employees/ClockInOutBody;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/employees/ClockInOutBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/server/employees/ClockInOutResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/api/v1/employee-timecards/clockout"
    .end annotation
.end method

.method public abstract getEmployees(Ljava/lang/String;I)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Query;
            value = "cursor"
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lretrofit/http/Query;
            value = "max_results"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Lcom/squareup/server/employees/EmployeesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/api/v1/rest/external/employees"
    .end annotation
.end method

.method public abstract setPasscode(Lcom/squareup/server/employees/SetPasscodeRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/server/employees/SetPasscodeRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/employees/SetPasscodeRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/server/employees/SetPasscodeResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/api/v1/employee-passcode/set-passcode"
    .end annotation
.end method
