.class public interface abstract Lcom/squareup/server/sdk/ReaderSdkAuthorizationService;
.super Ljava/lang/Object;
.source "ReaderSdkAuthorizationService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008H\'\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/server/sdk/ReaderSdkAuthorizationService;",
        "",
        "login",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;",
        "appId",
        "",
        "request",
        "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeRequest;",
        "reader-sdk_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract login(Ljava/lang/String;Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "app_id"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/mobile/clients/{app_id}/authorization-code/session"
    .end annotation
.end method
