.class public interface abstract Lcom/squareup/server/accountstatus/AccountStatusService;
.super Ljava/lang/Object;
.source "AccountStatusService.java"


# static fields
.field public static final EMPTY_ACCOUNT_STATUS_RESPONSE:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 20
    new-instance v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;-><init>()V

    new-instance v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;-><init>()V

    .line 21
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/server/account/protos/GiftCards$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;-><init>()V

    .line 22
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->build()Lcom/squareup/server/account/protos/GiftCards;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards(Lcom/squareup/server/account/protos/GiftCards;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/server/account/protos/Limits$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/Limits$Builder;-><init>()V

    .line 23
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Limits$Builder;->build()Lcom/squareup/server/account/protos/Limits;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits(Lcom/squareup/server/account/protos/Limits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/server/account/protos/Preferences$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/Preferences$Builder;-><init>()V

    .line 24
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->build()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/server/account/protos/Tutorial$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/Tutorial$Builder;-><init>()V

    .line 25
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->build()Lcom/squareup/server/account/protos/Tutorial;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse;->populateDefaults()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/accountstatus/AccountStatusService;->EMPTY_ACCOUNT_STATUS_RESPONSE:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method


# virtual methods
.method public abstract getAccountStatus(Ljava/lang/String;)Lcom/squareup/server/accountstatus/AccountStatusStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/accountstatus/AccountStatusStandardResponse<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/1.0/account/status"
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json; charset=utf-8"
        }
    .end annotation
.end method

.method public abstract setPreferences(Ljava/lang/String;Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/accountstatus/AccountStatusStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/server/account/protos/PreferencesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lcom/squareup/server/accountstatus/AccountStatusStandardResponse<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json; charset=utf-8"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/account/status/preferences"
    .end annotation
.end method
