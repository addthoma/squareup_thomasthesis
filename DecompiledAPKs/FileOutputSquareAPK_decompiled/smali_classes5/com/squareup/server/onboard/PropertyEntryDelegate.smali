.class public final Lcom/squareup/server/onboard/PropertyEntryDelegate;
.super Ljava/lang/Object;
.source "Panels.kt"

# interfaces
.implements Lkotlin/properties/ReadWriteProperty;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/onboard/PropertyEntryDelegate$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/properties/ReadWriteProperty<",
        "Lcom/squareup/server/onboard/ComponentBuilder;",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Panels.kt\ncom/squareup/server/onboard/PropertyEntryDelegate\n*L\n1#1,379:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0000\u0018\u0000 \u0018*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u0001H\u00010\u0003:\u0001\u0018BK\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0019\u0010\u0007\u001a\u0015\u0012\u0004\u0012\u00020\t\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0008\u00a2\u0006\u0002\u0008\n\u0012\u001f\u0010\u000b\u001a\u001b\u0012\u0004\u0012\u00020\r\u0012\u0006\u0012\u0004\u0018\u00018\u0000\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\n\u00a2\u0006\u0002\u0010\u000fJ$\u0010\u0010\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0011\u001a\u00020\u00042\n\u0010\u0012\u001a\u0006\u0012\u0002\u0008\u00030\u0013H\u0096\u0002\u00a2\u0006\u0002\u0010\u0014J,\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00042\n\u0010\u0012\u001a\u0006\u0012\u0002\u0008\u00030\u00132\u0008\u0010\u0016\u001a\u0004\u0018\u00018\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010\u0017R!\u0010\u0007\u001a\u0015\u0012\u0004\u0012\u00020\t\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0008\u00a2\u0006\u0002\u0008\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\'\u0010\u000b\u001a\u001b\u0012\u0004\u0012\u00020\r\u0012\u0006\u0012\u0004\u0018\u00018\u0000\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/server/onboard/PropertyEntryDelegate;",
        "T",
        "",
        "Lkotlin/properties/ReadWriteProperty;",
        "Lcom/squareup/server/onboard/ComponentBuilder;",
        "key",
        "",
        "getter",
        "Lkotlin/Function1;",
        "Lcom/squareup/protos/client/onboard/PropertyMapEntry;",
        "Lkotlin/ExtensionFunctionType;",
        "setter",
        "Lkotlin/Function2;",
        "Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;",
        "",
        "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V",
        "getValue",
        "thisRef",
        "property",
        "Lkotlin/reflect/KProperty;",
        "(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;",
        "setValue",
        "value",
        "(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/server/onboard/PropertyEntryDelegate$Companion;

.field private static final EMPTY_PROPERTY:Lcom/squareup/protos/client/onboard/PropertyMapEntry;


# instance fields
.field private final getter:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/protos/client/onboard/PropertyMapEntry;",
            "TT;>;"
        }
    .end annotation
.end field

.field private final key:Ljava/lang/String;

.field private final setter:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;",
            "TT;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/server/onboard/PropertyEntryDelegate$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/PropertyEntryDelegate$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->Companion:Lcom/squareup/server/onboard/PropertyEntryDelegate$Companion;

    .line 309
    new-instance v0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;-><init>()V

    .line 310
    invoke-virtual {v0}, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->build()Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->EMPTY_PROPERTY:Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/onboard/PropertyMapEntry;",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "getter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->key:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getter:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setter:Lkotlin/jvm/functions/Function2;

    return-void
.end method


# virtual methods
.method public getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/onboard/ComponentBuilder;",
            "Lkotlin/reflect/KProperty<",
            "*>;)TT;"
        }
    .end annotation

    const-string/jumbo v0, "thisRef"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "property"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 293
    invoke-virtual {p1}, Lcom/squareup/server/onboard/ComponentBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Component$Builder;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Component$Builder;->properties:Ljava/util/Map;

    iget-object v0, p0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->key:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lkotlin/reflect/KProperty;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    sget-object p1, Lcom/squareup/server/onboard/PropertyEntryDelegate;->EMPTY_PROPERTY:Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    const-string p2, "EMPTY_PROPERTY"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    :goto_1
    iget-object p2, p0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getter:Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;
    .locals 0

    .line 284
    check-cast p1, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/onboard/ComponentBuilder;",
            "Lkotlin/reflect/KProperty<",
            "*>;TT;)V"
        }
    .end annotation

    const-string/jumbo v0, "thisRef"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "property"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    invoke-virtual {p1}, Lcom/squareup/server/onboard/ComponentBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Component$Builder;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Component$Builder;->properties:Ljava/util/Map;

    const-string/jumbo v0, "thisRef.builder.properties"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->key:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lkotlin/reflect/KProperty;->getName()Ljava/lang/String;

    move-result-object v0

    .line 303
    :goto_0
    new-instance p2, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;-><init>()V

    .line 304
    iget-object v1, p0, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setter:Lkotlin/jvm/functions/Function2;

    invoke-interface {v1, p2, p3}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    invoke-virtual {p2}, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->build()Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    move-result-object p2

    .line 302
    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V
    .locals 0

    .line 284
    check-cast p1, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
