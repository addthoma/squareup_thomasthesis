.class public final Lcom/squareup/server/analytics/Es1BatchUploader_Factory;
.super Ljava/lang/Object;
.source "Es1BatchUploader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/server/analytics/Es1BatchUploader;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EventStreamService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EventStreamService;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/server/analytics/Es1BatchUploader_Factory;->serviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/server/analytics/Es1BatchUploader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EventStreamService;",
            ">;)",
            "Lcom/squareup/server/analytics/Es1BatchUploader_Factory;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/server/analytics/Es1BatchUploader_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/server/analytics/Es1BatchUploader_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/analytics/EventStreamService;)Lcom/squareup/server/analytics/Es1BatchUploader;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/server/analytics/Es1BatchUploader;

    invoke-direct {v0, p0}, Lcom/squareup/server/analytics/Es1BatchUploader;-><init>(Lcom/squareup/server/analytics/EventStreamService;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/server/analytics/Es1BatchUploader;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/server/analytics/Es1BatchUploader_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/analytics/EventStreamService;

    invoke-static {v0}, Lcom/squareup/server/analytics/Es1BatchUploader_Factory;->newInstance(Lcom/squareup/server/analytics/EventStreamService;)Lcom/squareup/server/analytics/Es1BatchUploader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/server/analytics/Es1BatchUploader_Factory;->get()Lcom/squareup/server/analytics/Es1BatchUploader;

    move-result-object v0

    return-object v0
.end method
