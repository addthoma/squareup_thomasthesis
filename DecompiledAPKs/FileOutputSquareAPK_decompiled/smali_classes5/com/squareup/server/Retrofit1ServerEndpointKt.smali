.class public final Lcom/squareup/server/Retrofit1ServerEndpointKt;
.super Ljava/lang/Object;
.source "Retrofit1ServerEndpoint.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toEndpoint",
        "Lretrofit/Endpoint;",
        "Lcom/squareup/http/Server;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toEndpoint(Lcom/squareup/http/Server;)Lretrofit/Endpoint;
    .locals 1

    const-string v0, "$this$toEndpoint"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcom/squareup/server/Retrofit1ServerEndpoint;

    invoke-direct {v0, p0}, Lcom/squareup/server/Retrofit1ServerEndpoint;-><init>(Lcom/squareup/http/Server;)V

    check-cast v0, Lretrofit/Endpoint;

    return-object v0
.end method
