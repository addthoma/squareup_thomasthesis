.class public final Lcom/squareup/tenderpayment/SelectMethodScreenKeys;
.super Ljava/lang/Object;
.source "SelectMethodScreenKeys.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0008\u001a\u00020\t2\u0012\u0010\n\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u000bH\u0007J\u001c\u0010\u000c\u001a\u00020\t2\u0012\u0010\n\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u000bH\u0007J\u001c\u0010\r\u001a\u00020\t2\u0012\u0010\n\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u000bH\u0007J\u001c\u0010\u000e\u001a\u00020\t2\u0012\u0010\n\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u000bH\u0007J\u001c\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\u0010\u001a\u00020\u0011H\u0007J\u001c\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\u0010\u001a\u00020\u0011H\u0007R\u001c\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodScreenKeys;",
        "",
        "()V",
        "PRIMARY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "SECONDARY",
        "isInSelectMethodWorkflow",
        "",
        "key",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "isPrimaryScreenKey",
        "isSecondaryScreenKey",
        "matchesSelectMethodScreenKey",
        "primaryScreenKey",
        "tenderIndex",
        "",
        "secondaryScreenKey",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tenderpayment/SelectMethodScreenKeys;

.field public static final PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation
.end field

.field public static final SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 13
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;-><init>()V

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodScreenKeys;

    .line 14
    new-instance v1, Lcom/squareup/workflow/legacy/Screen$Key;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "-PRIMARY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 15
    new-instance v1, Lcom/squareup/workflow/legacy/Screen$Key;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-SECONDARY"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v4, v3, v4}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final isInSelectMethodWorkflow(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "key"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {p0}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->matchesSelectMethodScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 39
    const-class v0, Lcom/squareup/checkoutflow/payother/PayOtherScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2, v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/tenderpayment/SplitTenderWarningDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/tenderpayment/SplitTenderAmount;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 45
    const-class v0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v2, v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->EVEN_SPLIT_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->CONFIRMING_CANCEL_SPLIT_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public static final isPrimaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "key"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p0, v0}, Lcom/squareup/workflow/legacy/Screen$Key;->castOrNull(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isSecondaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "key"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p0, v0}, Lcom/squareup/workflow/legacy/Screen$Key;->castOrNull(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final matchesSelectMethodScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "key"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {p0}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->isPrimaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->isSecondaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final primaryScreenKey(I)Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 18
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, p0, v2, v1}, Lcom/squareup/workflow/legacy/Screen$Key;->copy$default(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p0

    return-object p0
.end method

.method public static final secondaryScreenKey(I)Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 21
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, p0, v2, v1}, Lcom/squareup/workflow/legacy/Screen$Key;->copy$default(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p0

    return-object p0
.end method
