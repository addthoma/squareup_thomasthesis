.class public final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;
.super Ljava/lang/Object;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onAddGiftCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectMethodStateWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectMethodStateWorkflow.kt\ncom/squareup/tenderpayment/SelectMethodStateWorkflow$actionInState$1\n+ 2 SelectMethodStateWorkflow.kt\ncom/squareup/tenderpayment/SelectMethodStateWorkflow\n*L\n1#1,1372:1\n815#2,4:1373\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001J\u0008\u0010\u0004\u001a\u00020\u0005H\u0016J\u0018\u0010\u0006\u001a\u00020\u0007*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0008H\u0016\u00a8\u0006\t\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/tenderpayment/SelectMethodStateWorkflow$actionInState$1",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

.field final synthetic this$0$inline_fun:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;->this$0$inline_fun:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    .line 1341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/TenderPaymentResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            ">;)",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1341
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 1341
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/TenderPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "-",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1343
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    .line 1344
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    if-eqz v1, :cond_1

    .line 1345
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;

    .line 1349
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v2

    .line 1345
    invoke-direct {v1, v0, v2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1373
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getTutorialCore$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object v0

    const-string v2, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v2}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 1375
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCard;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCard;

    invoke-virtual {v1, v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->setOutput(Ljava/lang/Object;)V

    .line 1352
    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    .line 1353
    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->getOutput$tender_payment_release()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    if-eqz v0, :cond_0

    .line 1354
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;->this$0$inline_fun:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v1, v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$performOutputSideEffect(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    .line 1356
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :cond_0
    return-void

    .line 1360
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected WorkflowAction.apply() in "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", expected "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1359
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WorkflowAction()@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
