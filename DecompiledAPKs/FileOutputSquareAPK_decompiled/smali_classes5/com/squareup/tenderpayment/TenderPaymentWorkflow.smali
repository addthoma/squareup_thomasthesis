.class public final Lcom/squareup/tenderpayment/TenderPaymentWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "TenderPaymentWorkflow.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTenderPaymentWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TenderPaymentWorkflow.kt\ncom/squareup/tenderpayment/TenderPaymentWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,491:1\n32#2,12:492\n*E\n*S KotlinDebug\n*F\n+ 1 TenderPaymentWorkflow.kt\ncom/squareup/tenderpayment/TenderPaymentWorkflow\n*L\n146#1,12:492\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008d\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005*\u00019\u0018\u00002@\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t0\u00012\u00020\nB\u0095\u0001\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\u0006\u0010\u001f\u001a\u00020 \u0012\u0006\u0010!\u001a\u00020\"\u0012\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020+\u0012\u0006\u0010,\u001a\u00020-\u00a2\u0006\u0002\u0010.J\u0010\u0010F\u001a\n G*\u0004\u0018\u00010000H\u0002J\u0010\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0002J\u001c\u0010L\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040M2\u0006\u0010N\u001a\u00020OH\u0002J$\u0010P\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040M2\u0006\u0010Q\u001a\u00020\u00022\u0006\u0010R\u001a\u00020\u0004H\u0002J4\u0010S\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040M2\u0006\u0010Q\u001a\u00020\u00022\u0006\u0010T\u001a\u00020U2\u0006\u0010R\u001a\u00020V2\u0006\u0010W\u001a\u00020XH\u0002J\u001c\u0010Y\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040M2\u0006\u0010R\u001a\u00020\u0004H\u0002J\u001c\u0010Z\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040M2\u0006\u0010Q\u001a\u00020\u0002H\u0002J$\u0010[\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040M2\u0006\u0010Q\u001a\u00020\u00022\u0006\u0010R\u001a\u00020\\H\u0002J$\u0010]\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040M2\u0006\u0010Q\u001a\u00020\u00022\u0006\u0010R\u001a\u00020^H\u0002J\u001a\u0010_\u001a\u00020\u00032\u0006\u0010`\u001a\u00020\u00022\u0008\u0010a\u001a\u0004\u0018\u00010bH\u0016J\u0008\u0010c\u001a\u00020dH\u0002J\u0008\u0010e\u001a\u00020dH\u0002J\u0008\u0010f\u001a\u00020dH\u0002JR\u0010g\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t2\u0006\u0010`\u001a\u00020\u00022\u0006\u0010N\u001a\u00020\u00032\u0012\u0010h\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040iH\u0016J\u0008\u0010j\u001a\u00020dH\u0002J\u0010\u0010k\u001a\u00020d2\u0006\u0010l\u001a\u00020\u0002H\u0002J\u0010\u0010m\u001a\u00020b2\u0006\u0010N\u001a\u00020\u0003H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010/\u001a\u0002008BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00081\u00102R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u00103\u001a\u0008\u0012\u0004\u0012\u000205048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00086\u00107R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u00108\u001a\u000209X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010:R\u0014\u0010;\u001a\u00020<8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008=\u0010>R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010?\u001a\u00020%8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008B\u0010C\u001a\u0004\u0008@\u0010AR\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010D\u001a\u0002008BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008E\u00102R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006n"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;",
        "apiTransactionState",
        "Lcom/squareup/api/ApiTransactionState;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "offlineModeMonitor",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "cardReaderHubUtils",
        "Lcom/squareup/cardreader/CardReaderHubUtils;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "separateTenderHandler",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;",
        "tenderOptionMap",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
        "payOtherTenderCompleter",
        "Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;",
        "buyerCheckoutWorkflow",
        "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
        "legacySelectMethodWorkflow",
        "Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;",
        "selectMethodWorkflowProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/tenderpayment/SelectMethodV2Workflow;",
        "payOtherWorkflow",
        "Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;",
        "sellerQuickCashWorkflow",
        "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;",
        "separateTenderWorkflow",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V",
        "billAmount",
        "Lcom/squareup/protos/common/Money;",
        "getBillAmount",
        "()Lcom/squareup/protos/common/Money;",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "getCompletedTenders",
        "()Ljava/util/List;",
        "lifecycleWorker",
        "com/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;",
        "maxSplits",
        "",
        "getMaxSplits",
        "()J",
        "selectMethodWorkflow",
        "getSelectMethodWorkflow",
        "()Lcom/squareup/tenderpayment/SelectMethodV2Workflow;",
        "selectMethodWorkflow$delegate",
        "Ljavax/inject/Provider;",
        "splitTenderTotalAmountRemaining",
        "getSplitTenderTotalAmountRemaining",
        "amountRemaining",
        "kotlin.jvm.PlatformType",
        "defaultSelectMethodStartArgs",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "tenderOptions",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "enterSelectMethod",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;",
        "handleBuyerCheckoutResult",
        "config",
        "result",
        "handlePayOtherResult",
        "payOtherInput",
        "Lcom/squareup/checkoutflow/payother/PayOtherInput;",
        "Lcom/squareup/checkoutflow/payother/PayOtherOutput;",
        "restartSelectMethodWorkflowData",
        "Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;",
        "handleSelectMethodResult",
        "handleSellerQuickCash",
        "handleSeparateTenderResult",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "handleTenderOptionResult",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "isSplitTender",
        "",
        "maybeCheckPaymentReadySmartReaderIsAvailable",
        "offlineButNotInOfflineMode",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "shouldCheckPaymentReadySmartReaderIsAvailable",
        "shouldEnterBuyerCheckout",
        "input",
        "snapshotState",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final buyerCheckoutWorkflow:Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final legacySelectMethodWorkflow:Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;

.field private final lifecycleWorker:Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final payOtherTenderCompleter:Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;

.field private final payOtherWorkflow:Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;

.field private final selectMethodWorkflow$delegate:Ljavax/inject/Provider;

.field private final sellerQuickCashWorkflow:Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;

.field private final separateTenderHandler:Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;

.field private final separateTenderWorkflow:Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderOptionMap:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "selectMethodWorkflow"

    const-string v4, "getSelectMethodWorkflow()Lcom/squareup/tenderpayment/SelectMethodV2Workflow;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ApiTransactionState;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            "Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            "Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodV2Workflow;",
            ">;",
            "Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "apiTransactionState"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineModeMonitor"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHubUtils"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "separateTenderHandler"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderOptionMap"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payOtherTenderCompleter"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerCheckoutWorkflow"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "legacySelectMethodWorkflow"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectMethodWorkflowProvider"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payOtherWorkflow"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sellerQuickCashWorkflow"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "separateTenderWorkflow"

    move-object/from16 v13, p16

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    move-object/from16 v13, p17

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct/range {p0 .. p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v13, p16

    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    iput-object v2, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v3, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v4, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    iput-object v5, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object v6, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    iput-object v7, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object v8, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->separateTenderHandler:Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;

    iput-object v9, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->tenderOptionMap:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;

    iput-object v10, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->payOtherTenderCompleter:Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;

    iput-object v11, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->buyerCheckoutWorkflow:Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

    iput-object v12, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->legacySelectMethodWorkflow:Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;

    iput-object v14, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->payOtherWorkflow:Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;

    iput-object v15, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->sellerQuickCashWorkflow:Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;

    iput-object v13, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->separateTenderWorkflow:Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;

    move-object/from16 v1, p13

    .line 108
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->selectMethodWorkflow$delegate:Ljavax/inject/Provider;

    .line 130
    new-instance v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;

    move-object/from16 v2, p17

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->lifecycleWorker:Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;

    return-void
.end method

.method public static final synthetic access$handleBuyerCheckoutResult(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->handleBuyerCheckoutResult(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handlePayOtherResult(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/checkoutflow/payother/PayOtherOutput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->handlePayOtherResult(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/checkoutflow/payother/PayOtherOutput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleSelectMethodResult(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->handleSelectMethodResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleSellerQuickCash(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->handleSellerQuickCash(Lcom/squareup/tenderpayment/TenderPaymentConfig;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleSeparateTenderResult(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->handleSeparateTenderResult(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleTenderOptionResult(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->handleTenderOptionResult(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final amountRemaining()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 465
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method private final defaultSelectMethodStartArgs(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;
    .locals 18

    move-object/from16 v0, p0

    .line 470
    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v1}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v1

    const/4 v2, 0x0

    const-string v3, "amountRemaining()"

    if-eqz v1, :cond_0

    .line 471
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    .line 473
    iget-object v4, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v4}, Lcom/squareup/api/ApiTransactionState;->cardSupported()Z

    move-result v6

    .line 474
    iget-object v4, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v4}, Lcom/squareup/api/ApiTransactionState;->splitTenderSupported()Z

    move-result v7

    .line 475
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->amountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v8

    invoke-static {v8, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 476
    new-instance v9, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-direct {v9, v2}, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;-><init>(Z)V

    const/4 v10, 0x0

    move-object v4, v1

    move-object/from16 v5, p1

    .line 471
    invoke-direct/range {v4 .. v10}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;Z)V

    return-object v1

    .line 481
    :cond_0
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    .line 483
    iget-object v4, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v4}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v13

    const/4 v14, 0x1

    .line 485
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->amountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v15

    invoke-static {v15, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 486
    new-instance v3, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-direct {v3, v2}, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;-><init>(Z)V

    const/16 v17, 0x0

    move-object v11, v1

    move-object/from16 v12, p1

    move-object/from16 v16, v3

    .line 481
    invoke-direct/range {v11 .. v17}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;Z)V

    return-object v1
.end method

.method private final enterSelectMethod(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 267
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->separateTenderHandler:Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string/jumbo v2, "transaction.tenderAmountDue"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;->createSplitTender(Lcom/squareup/protos/common/Money;)V

    .line 271
    :cond_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final getBillAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string/jumbo v1, "transaction.amountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getCompletedTenders()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 125
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getCapturedTenders()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillP\u2026         .capturedTenders"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "Collections.emptyList()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private final getMaxSplits()J
    .locals 4

    .line 112
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 113
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->getSplitTenderTotalAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    div-long/2addr v2, v0

    return-wide v2
.end method

.method private final getSelectMethodWorkflow()Lcom/squareup/tenderpayment/SelectMethodV2Workflow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->selectMethodWorkflow$delegate:Ljavax/inject/Provider;

    sget-object v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/ProvidersKt;->getValue(Ljavax/inject/Provider;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodV2Workflow;

    return-object v0
.end method

.method private final getSplitTenderTotalAmountRemaining()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillP\u2026      .remainingAmountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :cond_0
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->getBillAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final handleBuyerCheckoutResult(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 373
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->defaultSelectMethodStartArgs(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object p1

    .line 375
    new-instance p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    invoke-direct {p2, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->enterSelectMethod(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 378
    :cond_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final handlePayOtherResult(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/checkoutflow/payother/PayOtherOutput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
            "Lcom/squareup/checkoutflow/payother/PayOtherInput;",
            "Lcom/squareup/checkoutflow/payother/PayOtherOutput;",
            "Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 390
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->defaultSelectMethodStartArgs(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object v0

    .line 393
    instance-of p1, p3, Lcom/squareup/checkoutflow/payother/PayOtherOutput$Cancel;

    if-eqz p1, :cond_0

    .line 394
    new-instance p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x2f

    const/4 v8, 0x0

    move-object v5, p4

    .line 395
    invoke-static/range {v0 .. v8}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->copy$default(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;ZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object p2

    .line 394
    invoke-direct {p1, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    .line 393
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->enterSelectMethod(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 401
    :cond_0
    instance-of p1, p3, Lcom/squareup/checkoutflow/payother/PayOtherOutput$RecordPayment;

    if-eqz p1, :cond_2

    .line 402
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->payOtherTenderCompleter:Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;

    .line 403
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/payother/PayOtherInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p4

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/payother/PayOtherInput;->getType()Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object p2

    check-cast p3, Lcom/squareup/checkoutflow/payother/PayOtherOutput$RecordPayment;

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/payother/PayOtherOutput$RecordPayment;->getNote()Ljava/lang/String;

    move-result-object p3

    .line 402
    invoke-interface {p1, p4, p2, p3}, Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;->completeTender(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderPaymentResult;

    move-result-object p1

    .line 406
    sget-object p2, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 411
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->separateTenderHandler:Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->amountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object p2

    const-string p3, "amountRemaining()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;->createSplitTender(Lcom/squareup/protos/common/Money;)V

    .line 412
    new-instance p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    invoke-direct {p1, v0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->enterSelectMethod(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 414
    :cond_1
    sget-object p2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p2, p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 405
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleSelectMethodResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 298
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$TenderOptionSelected;

    const-string v1, "amountRemaining()"

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v4

    .line 300
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 301
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 302
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string/jumbo v7, "transaction.asBillPayment()"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getUnboundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 303
    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->getDifference(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, v3

    .line 309
    :goto_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v8

    const/4 v9, 0x1

    .line 299
    invoke-virtual/range {v4 .. v9}, Lcom/squareup/payment/Order;->getCartProto(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    .line 313
    sget-object v4, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 314
    new-instance v5, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;

    .line 315
    new-instance v6, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    .line 316
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->amountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v1

    const-string v8, "cartProto"

    .line 318
    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 315
    invoke-direct {v6, v7, v1, v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;-><init>(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/client/bills/Cart;)V

    .line 320
    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$TenderOptionSelected;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$TenderOptionSelected;->getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object p1

    .line 314
    invoke-direct {v5, v6, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V

    .line 313
    invoke-static {v4, v5, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_1

    .line 324
    :cond_1
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;

    if-eqz v0, :cond_2

    .line 325
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 326
    new-instance v4, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    .line 327
    new-instance v5, Lcom/squareup/checkoutflow/payother/PayOtherInput;

    .line 328
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->amountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 329
    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->getTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    const-string v7, "result.tender.legacy_other_tender_type_id"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->fromValue(I)Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object v1

    const-string v7, "OtherTenderType.fromValu\u2026acy_other_tender_type_id)"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->getTenderName()Ljava/lang/String;

    move-result-object v7

    .line 327
    invoke-direct {v5, v6, v1, v7}, Lcom/squareup/checkoutflow/payother/PayOtherInput;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;Ljava/lang/String;)V

    .line 332
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->getRestartSelectMethodData()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    move-result-object p1

    .line 326
    invoke-direct {v4, v5, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;-><init>(Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)V

    .line 325
    invoke-static {v0, v4, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 336
    :cond_2
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$QuickCashReceived;

    if-eqz v0, :cond_3

    .line 337
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 338
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    .line 339
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    .line 341
    new-instance v0, Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;

    const-string v1, "lastAddedTender"

    .line 342
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v4, "lastAddedTender.amount"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    check-cast p1, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    invoke-interface {p1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string v4, "(lastAddedTender as ReturnsChange).tendered"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 341
    invoke-direct {v0, v1, p1}, Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 346
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSellerQuickCash;

    invoke-direct {v1, v0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSellerQuickCash;-><init>(Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;)V

    invoke-static {p1, v1, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 348
    :cond_3
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSeparateTenders;

    if-eqz v0, :cond_4

    .line 349
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSeparateTender;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSeparateTender;

    invoke-static {p1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 353
    :cond_4
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private final handleSellerQuickCash(Lcom/squareup/tenderpayment/TenderPaymentConfig;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 277
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->defaultSelectMethodStartArgs(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->enterSelectMethod(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final handleSeparateTenderResult(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 362
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->separateTenderHandler:Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;

    invoke-interface {v0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;->pushToTransaction(Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;)V

    .line 363
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->defaultSelectMethodStartArgs(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object v0

    .line 364
    instance-of p1, p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$CancelSplitTender;

    xor-int/lit8 v6, p1, 0x1

    const/16 v7, 0x1f

    const/4 v8, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v8}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->copy$default(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;ZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object p1

    .line 365
    new-instance p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    invoke-direct {p2, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->enterSelectMethod(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final handleTenderOptionResult(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 285
    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    new-instance p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->defaultSelectMethodStartArgs(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->enterSelectMethod(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 288
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$DoNothing;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$DoNothing;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object p2, Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 289
    :cond_1
    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Unsuccessful;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Unsuccessful;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object p2, Lcom/squareup/tenderpayment/TenderPaymentResult$UnsuccessfullyComplete;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$UnsuccessfullyComplete;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 290
    :cond_2
    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object p2, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfullyComplete;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfullyComplete;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final isSplitTender()Z
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    return v0
.end method

.method private final maybeCheckPaymentReadySmartReaderIsAvailable()Z
    .locals 1

    .line 447
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->shouldCheckPaymentReadySmartReaderIsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isPaymentReadySmartReaderConnected()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method private final offlineButNotInOfflineMode()Z
    .locals 1

    .line 463
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final shouldCheckPaymentReadySmartReaderIsAvailable()Z
    .locals 2

    .line 455
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_WITHOUT_MATCHING_TMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final shouldEnterBuyerCheckout(Lcom/squareup/tenderpayment/TenderPaymentConfig;)Z
    .locals 1

    .line 430
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->getDefaultToBuyerCheckout()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 432
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->isSplitTender()Z

    move-result p1

    if-nez p1, :cond_0

    .line 433
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->offlineButNotInOfflineMode()Z

    move-result p1

    if-nez p1, :cond_0

    .line 434
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->paymentIsWithinRange()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 435
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->maybeCheckPaymentReadySmartReaderIsAvailable()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p2, :cond_4

    .line 492
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v1

    :goto_1
    if-eqz p2, :cond_3

    .line 497
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v3, "Parcel.obtain()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 498
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 499
    array-length v3, p2

    invoke-virtual {v1, p2, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 500
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 501
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 502
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v1

    .line 503
    :goto_2
    move-object v1, p2

    check-cast v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    :cond_4
    if-eqz v1, :cond_5

    .line 150
    instance-of p2, v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;

    if-nez p2, :cond_5

    goto :goto_4

    .line 151
    :cond_5
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->shouldEnterBuyerCheckout(Lcom/squareup/tenderpayment/TenderPaymentConfig;)Z

    move-result p2

    if-eqz p2, :cond_7

    .line 152
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->getStartAtStep()Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    move-result-object p1

    sget-object p2, Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;->PAYMENT_PROMPT:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    if-ne p1, p2, :cond_6

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    .line 153
    :goto_3
    new-instance p1, Lcom/squareup/buyercheckout/BuyerCheckoutSettings;

    invoke-direct {p1, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutSettings;-><init>(Z)V

    .line 155
    new-instance p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InBuyerCheckout;

    invoke-direct {p2, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InBuyerCheckout;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutSettings;)V

    move-object v1, p2

    check-cast v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    goto :goto_4

    .line 158
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->defaultSelectMethodStartArgs(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object p1

    .line 159
    new-instance p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    invoke-direct {p2, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;-><init>(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    move-object v1, p2

    check-cast v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    .line 164
    :goto_4
    instance-of p1, v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 165
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->separateTenderHandler:Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;

    iget-object p2, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p2

    const-string/jumbo v0, "transaction.tenderAmountDue"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;->createSplitTender(Lcom/squareup/protos/common/Money;)V

    :cond_8
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentConfig;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->initialState(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentConfig;

    check-cast p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->render(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "-",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->lifecycleWorker:Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;

    check-cast v0, Lcom/squareup/workflow/Worker;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p3, v0, v1, v2, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 179
    instance-of v0, p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InBuyerCheckout;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->buyerCheckoutWorkflow:Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 183
    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    check-cast p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InBuyerCheckout;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InBuyerCheckout;->getInput()Lcom/squareup/buyercheckout/BuyerCheckoutSettings;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->startingState(Lcom/squareup/buyercheckout/BuyerCheckoutSettings;)Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v4

    const/4 v5, 0x0

    .line 184
    new-instance p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$1;-><init>(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;)V

    move-object v6, p2

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 181
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->getRendering()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    if-eqz p1, :cond_6

    .line 188
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 190
    :cond_0
    instance-of v0, p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SELECT_METHOD_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 193
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->getSelectMethodWorkflow()Lcom/squareup/tenderpayment/SelectMethodV2Workflow;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 194
    check-cast p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;->getInput()Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object v2

    const/4 v3, 0x0

    .line 195
    new-instance p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$2;

    invoke-direct {p1, p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$2;-><init>(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 192
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/util/Map;

    goto/16 :goto_0

    .line 201
    :cond_1
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->legacySelectMethodWorkflow:Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 202
    check-cast p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;->getInput()Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object v2

    const/4 v3, 0x0

    .line 203
    new-instance p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$3;

    invoke-direct {p1, p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$3;-><init>(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 200
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->getRendering()Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/util/Map;

    goto/16 :goto_0

    .line 208
    :cond_2
    instance-of v0, p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    if-eqz v0, :cond_3

    .line 211
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->payOtherWorkflow:Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 212
    move-object v0, p2

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->getInput()Lcom/squareup/checkoutflow/payother/PayOtherInput;

    move-result-object v3

    const/4 v4, 0x0

    .line 213
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;-><init>(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 210
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/util/Map;

    goto/16 :goto_0

    .line 219
    :cond_3
    instance-of v0, p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSellerQuickCash;

    if-eqz v0, :cond_4

    .line 222
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->sellerQuickCashWorkflow:Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 223
    check-cast p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSellerQuickCash;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSellerQuickCash;->getInput()Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;

    move-result-object v3

    const/4 v4, 0x0

    .line 224
    new-instance p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$5;

    invoke-direct {p2, p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$5;-><init>(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 221
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 227
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 229
    :cond_4
    instance-of v0, p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;

    if-eqz v0, :cond_5

    .line 232
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->tenderOptionMap:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;

    check-cast p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;->getKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;->getTenderOption(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->getWorkflow()Lcom/squareup/workflow/Workflow;

    move-result-object v2

    .line 233
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;->getInput()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    move-result-object v3

    const/4 v4, 0x0

    .line 234
    new-instance p2, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$6;

    invoke-direct {p2, p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$6;-><init>(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 231
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/util/Map;

    goto :goto_0

    .line 238
    :cond_5
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSeparateTender;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSeparateTender;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_8

    .line 239
    new-instance p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;

    .line 240
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->getSplitTenderTotalAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->amountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v0, "MoneyMath.subtract(split\u2026ining, amountRemaining())"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->getMaxSplits()J

    move-result-wide v2

    .line 242
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->amountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v4

    const-string v0, "amountRemaining()"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v5

    const-string/jumbo v0, "transaction.amountDue"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->getSplitTenderTotalAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 245
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->getCompletedTenders()Ljava/util/List;

    move-result-object v7

    move-object v0, p2

    .line 239
    invoke-direct/range {v0 .. v7}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;-><init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->separateTenderWorkflow:Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    .line 252
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$7;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$7;-><init>(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    move-object v2, p2

    .line 249
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/util/Map;

    :cond_6
    :goto_0
    if-eqz v1, :cond_7

    goto :goto_1

    .line 258
    :cond_7
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v1

    :goto_1
    return-object v1

    .line 249
    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->snapshotState(Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
