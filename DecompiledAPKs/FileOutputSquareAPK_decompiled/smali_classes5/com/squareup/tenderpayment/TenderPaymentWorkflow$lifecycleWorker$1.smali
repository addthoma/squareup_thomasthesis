.class public final Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "TenderPaymentWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/TenderPaymentWorkflow;-><init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0006\u001a\u00020\u0004H\u0016J\u0008\u0010\u0007\u001a\u00020\u0004H\u0016R\u001c\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "workflowCompleted",
        "Lio/reactivex/subjects/SingleSubject;",
        "",
        "kotlin.jvm.PlatformType",
        "onStarted",
        "onStopped",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final workflowCompleted:Lio/reactivex/subjects/SingleSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/SingleSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 1

    .line 130
    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;->$passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    .line 131
    invoke-static {}, Lio/reactivex/subjects/SingleSubject;->create()Lio/reactivex/subjects/SingleSubject;

    move-result-object p1

    const-string v0, "SingleSubject.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;->workflowCompleted:Lio/reactivex/subjects/SingleSubject;

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;->$passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;->workflowCompleted:Lio/reactivex/subjects/SingleSubject;

    invoke-virtual {v1}, Lio/reactivex/subjects/SingleSubject;->ignoreElement()Lio/reactivex/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimer(Lio/reactivex/Completable;)V

    return-void
.end method

.method public onStopped()V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$lifecycleWorker$1;->workflowCompleted:Lio/reactivex/subjects/SingleSubject;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/SingleSubject;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method
