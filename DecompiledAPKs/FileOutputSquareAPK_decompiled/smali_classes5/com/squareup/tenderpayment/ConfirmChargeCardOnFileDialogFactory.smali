.class Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialogFactory;
.super Ljava/lang/Object;
.source "ConfirmChargeCardOnFileDialogFactory.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;",
            ">;>;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic lambda$create$3(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 38
    const-class v0, Lcom/squareup/ui/main/CheckoutWorkflowRunner$ParentComponent;

    invoke-static {p0, v0}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/CheckoutWorkflowRunner$ParentComponent;

    .line 39
    invoke-interface {v0}, Lcom/squareup/ui/main/CheckoutWorkflowRunner$ParentComponent;->moneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    .line 41
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/tenderworkflow/R$string;->charge:I

    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$ConfirmChargeCardOnFileDialogFactory$zeGFF_V25Uh2XQDp6OdKZ21buqE;

    invoke-direct {v3, p1}, Lcom/squareup/tenderpayment/-$$Lambda$ConfirmChargeCardOnFileDialogFactory$zeGFF_V25Uh2XQDp6OdKZ21buqE;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 42
    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$ConfirmChargeCardOnFileDialogFactory$7FLb9Jjrc5KkqoFKcE6bsnmtZAs;

    invoke-direct {v3, p1}, Lcom/squareup/tenderpayment/-$$Lambda$ConfirmChargeCardOnFileDialogFactory$7FLb9Jjrc5KkqoFKcE6bsnmtZAs;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 45
    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/payment/R$string;->card_on_file_charge_confirmation_title:I

    .line 47
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/tenderworkflow/R$string;->card_on_file_charge_confirmation_body:I

    .line 48
    invoke-static {p0, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    iget-object v2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;

    iget-object v2, v2, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    .line 49
    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v2, "amount"

    invoke-virtual {p0, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->customerName:Ljava/lang/String;

    const-string v2, "customer_name"

    .line 50
    invoke-virtual {p0, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->cardNameAndNumber:Ljava/lang/String;

    const-string v2, "card_name"

    .line 51
    invoke-virtual {p0, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 52
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 48
    invoke-virtual {v1, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$ConfirmChargeCardOnFileDialogFactory$gb0-ZNBxPKZ9MnXi7Tz9X0AlNNM;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$ConfirmChargeCardOnFileDialogFactory$gb0-ZNBxPKZ9MnXi7Tz9X0AlNNM;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 53
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget p1, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    .line 54
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget p1, Lcom/squareup/noho/R$color;->noho_text_button_primary_enabled:I

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    .line 57
    invoke-virtual {p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/workflow/legacy/Screen;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 43
    iget-object p1, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance p2, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$ChargeCardOnFile;

    iget-object v0, p0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;

    iget-object p0, p0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->instrumentToken:Ljava/lang/String;

    invoke-direct {p2, v0, p0}, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$ChargeCardOnFile;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/squareup/workflow/legacy/Screen;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$DoNotChargeCardOnFile;->INSTANCE:Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$DoNotChargeCardOnFile;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$2(Lcom/squareup/workflow/legacy/Screen;Landroid/content/DialogInterface;)V
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$DoNotChargeCardOnFile;->INSTANCE:Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$DoNotChargeCardOnFile;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialogFactory;->screens:Lio/reactivex/Observable;

    .line 36
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$ConfirmChargeCardOnFileDialogFactory$jp4yvssAxPSU0fUuTmGsStRCuo4;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/-$$Lambda$ConfirmChargeCardOnFileDialogFactory$jp4yvssAxPSU0fUuTmGsStRCuo4;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
