.class public final Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealPaymentViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealPaymentViewFactory;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;Ljava/util/Set;Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"
    }
    d2 = {
        "com/squareup/tenderpayment/RealPaymentViewFactory$1",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $buyerCartFactory:Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;

.field final synthetic $paymentPromptCoordinator:Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;

.field final synthetic $selectMethodFactory:Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;

.field final synthetic $separateCustomEvenFactory:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;

.field final synthetic $separateTenderFactory:Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;

.field final synthetic $soloSellerCashReceivedLayoutRunnerFactory:Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;[Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;->$selectMethodFactory:Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;

    iput-object p2, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;->$buyerCartFactory:Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;

    iput-object p3, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;->$paymentPromptCoordinator:Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;

    iput-object p4, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;->$soloSellerCashReceivedLayoutRunnerFactory:Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;

    iput-object p5, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;->$separateTenderFactory:Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;

    iput-object p6, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;->$separateCustomEvenFactory:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;

    invoke-direct {p0, p7}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
