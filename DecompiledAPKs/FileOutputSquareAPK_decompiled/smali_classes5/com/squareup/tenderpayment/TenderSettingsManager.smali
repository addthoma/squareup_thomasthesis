.class public Lcom/squareup/tenderpayment/TenderSettingsManager;
.super Ljava/lang/Object;
.source "TenderSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;,
        Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;,
        Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;,
        Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;,
        Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;
    }
.end annotation


# static fields
.field public static final CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final CHECK:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final LEGACY_OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final OTHER_GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field public static final THIRD_PARTY_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

.field private final removedDisabledTenderSettingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final removedPrimaryTenderSettingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final removedSecondaryTenderSettingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 40
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CHECK:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->CHECK:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 41
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CUSTOM:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 42
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 43
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->MERCHANT_GIFT_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 44
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->OTHER_GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 45
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 46
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 47
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 48
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 49
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 50
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 51
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 52
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 53
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 54
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 55
    sput-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager;->LEGACY_OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedPrimaryTenderSettingMap:Ljava/util/Map;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedSecondaryTenderSettingMap:Ljava/util/Map;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedDisabledTenderSettingMap:Ljava/util/Map;

    .line 69
    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 70
    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->features:Lcom/squareup/settings/server/Features;

    .line 71
    iput-object p3, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    return-void
.end method

.method private addFilteredTenderBackIn(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 3

    .line 417
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 418
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 419
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 420
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 426
    iget-object p1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedPrimaryTenderSettingMap:Ljava/util/Map;

    invoke-direct {p0, p1, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addRemovedTender(Ljava/util/List;Ljava/util/Map;)V

    .line 427
    iget-object p1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedSecondaryTenderSettingMap:Ljava/util/Map;

    invoke-direct {p0, p1, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addRemovedTender(Ljava/util/List;Ljava/util/Map;)V

    .line 428
    iget-object p1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedDisabledTenderSettingMap:Ljava/util/Map;

    invoke-direct {p0, p1, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addRemovedTender(Ljava/util/List;Ljava/util/Map;)V

    .line 430
    invoke-virtual {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method private addMissingTenders(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 3

    .line 377
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 378
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 379
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 380
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 382
    iget-object p1, p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->primaryTenders:Ljava/util/List;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addTendersToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;Ljava/util/List;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V

    .line 383
    iget-object p1, p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->secondaryTenders:Ljava/util/List;

    sget-object p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addTendersToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;Ljava/util/List;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V

    .line 385
    invoke-virtual {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method private addRemovedTender(Ljava/util/List;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 434
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 435
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 436
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v2, :cond_0

    .line 437
    invoke-interface {p1, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 439
    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private addTendersToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;Ljava/util/List;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;",
            "Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;",
            ")V"
        }
    .end annotation

    .line 391
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderSettingsCategory:[I

    invoke-virtual {p3}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    if-eq p3, v0, :cond_1

    const/4 v0, 0x3

    if-eq p3, v0, :cond_0

    .line 397
    iget-object p3, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    goto :goto_0

    .line 400
    :cond_0
    iget-object p3, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    goto :goto_0

    .line 393
    :cond_1
    iget-object p3, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    .line 403
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 404
    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    .line 405
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    .line 406
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 407
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-void
.end method

.method private filterUnavailableTenders(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 5

    .line 447
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedPrimaryTenderSettingMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 448
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedSecondaryTenderSettingMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 449
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedDisabledTenderSettingMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 451
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 452
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 453
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 454
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 456
    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 457
    invoke-virtual {p2, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->contains(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 458
    iget-object v3, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedPrimaryTenderSettingMap:Ljava/util/Map;

    iget-object v4, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    iget-object v3, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 462
    :cond_1
    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 463
    invoke-virtual {p2, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->contains(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 464
    iget-object v3, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedSecondaryTenderSettingMap:Ljava/util/Map;

    iget-object v4, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    iget-object v3, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 468
    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 469
    invoke-virtual {p2, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->contains(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 470
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->removedDisabledTenderSettingMap:Ljava/util/Map;

    iget-object v3, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    iget-object v2, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 475
    :cond_5
    invoke-virtual {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method private generateMasterList(ZZ)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;
    .locals 4

    .line 319
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 320
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 321
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->EMONEY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager;->E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    :cond_0
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 325
    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    :cond_1
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 328
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 329
    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    :cond_2
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz p1, :cond_3

    .line 332
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    if-eqz p2, :cond_4

    .line 335
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    :cond_4
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-interface {p1}, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;->isOnlineCheckoutTenderOptionEnabled()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 338
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager;->CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    :cond_5
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForInstallments()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 341
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager;->INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    :cond_6
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForThirdPartyCardProcessing()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 344
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    :cond_7
    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/GiftCardSettings;->canAcceptThirdPartyGiftCards()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 347
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager;->OTHER_GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    :cond_8
    invoke-virtual {p0, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addOtherTenders(Ljava/util/List;)Ljava/util/List;

    .line 351
    new-instance p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;

    invoke-direct {p1, v0, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object p1
.end method

.method public static getTenderCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;
    .locals 0

    .line 249
    invoke-static {p0, p1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderIndex(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;->cat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    return-object p0
.end method

.method public static getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
            "Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation

    .line 128
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderSettingsCategory:[I

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 134
    iget-object p0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    return-object p0

    .line 136
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Invalid category"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 132
    :cond_1
    iget-object p0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    return-object p0

    .line 130
    :cond_2
    iget-object p0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    return-object p0
.end method

.method public static getTenderIndex(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;
    .locals 2

    .line 236
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;I)V

    return-object v0

    .line 238
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    .line 240
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;I)V

    return-object v0

    .line 241
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    .line 243
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;I)V

    return-object v0

    .line 245
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Tender is not in given TenderSettings."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static moreThanOnePrimaryPaymentMethodEnabled(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Z
    .locals 1

    .line 75
    iget-object p0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    const/4 v0, 0x1

    if-le p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static moveTenderToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Z)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 3

    .line 150
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    .line 151
    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    .line 152
    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    .line 155
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p1

    .line 158
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->moreThanOnePrimaryPaymentMethodEnabled(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Z

    move-result v0

    if-nez v0, :cond_3

    if-nez p3, :cond_2

    goto :goto_0

    .line 160
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Can\'t remove the last primary tender from the primary tender category. There must always be at least one primary tender."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 165
    :cond_3
    :goto_0
    new-instance p3, Ljava/util/ArrayList;

    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-static {p1, v0}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    .line 167
    invoke-static {p1, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 168
    new-instance v1, Ljava/util/ArrayList;

    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    .line 169
    invoke-static {p1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 171
    invoke-interface {p3, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 172
    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 173
    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 174
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderSettingsCategory:[I

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x1

    if-eq p1, p2, :cond_6

    const/4 p2, 0x2

    if-eq p1, p2, :cond_5

    const/4 p2, 0x3

    if-eq p1, p2, :cond_4

    goto :goto_1

    .line 182
    :cond_4
    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 179
    :cond_5
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 176
    :cond_6
    invoke-interface {p3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    :goto_1
    new-instance p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 187
    invoke-virtual {p0, p3}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    move-result-object p0

    .line 188
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    move-result-object p0

    .line 189
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    move-result-object p0

    .line 190
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p0

    return-object p0
.end method

.method public static moveTenderToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 4

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-static {p1, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 204
    new-instance v1, Ljava/util/ArrayList;

    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    .line 205
    invoke-static {p1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 206
    new-instance v2, Ljava/util/ArrayList;

    sget-object v3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    .line 207
    invoke-static {p1, v3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 209
    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 210
    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 211
    invoke-interface {v2, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 214
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderSettingsCategory:[I

    iget-object v3, p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;->cat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-virtual {v3}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->ordinal()I

    move-result v3

    aget p1, p1, v3

    const/4 v3, 0x1

    if-eq p1, v3, :cond_2

    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    const/4 v3, 0x3

    if-eq p1, v3, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    move-object p1, v2

    goto :goto_0

    :cond_1
    move-object p1, v1

    goto :goto_0

    :cond_2
    move-object p1, v0

    .line 226
    :goto_0
    iget p2, p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;->pos:I

    invoke-interface {p1, p2, p0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 228
    new-instance p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 229
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    move-result-object p0

    .line 230
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    move-result-object p0

    .line 231
    invoke-virtual {p0, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    move-result-object p0

    .line 232
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p0

    return-object p0
.end method

.method private processTenderSettings(Lcom/squareup/protos/client/devicesettings/TenderSettings;ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 0

    if-nez p1, :cond_0

    .line 306
    new-instance p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    .line 309
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->generateMasterList(ZZ)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;

    move-result-object p2

    .line 311
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->filterUnavailableTenders(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addMissingTenders(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    .line 310
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->removeDuplicates(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method public static readTenderSettingsBytesWithLength(Lokio/Buffer;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 122
    invoke-virtual {p0}, Lokio/Buffer;->readInt()I

    move-result v0

    .line 123
    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    int-to-long v2, v0

    invoke-virtual {p0, v2, v3}, Lokio/Buffer;->readByteArray(J)[B

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    return-object p0
.end method

.method private removeDuplicates(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 5

    .line 355
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 356
    new-instance v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 357
    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 358
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 359
    iget-object v4, v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 362
    :cond_1
    iget-object v2, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 363
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 364
    iget-object v4, v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 367
    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 368
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 369
    iget-object v3, v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 372
    :cond_5
    invoke-virtual {v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method public static supportedConnectivityModes(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    if-nez v0, :cond_1

    .line 88
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$1;->$SwitchMap$com$squareup$protos$client$bills$OtherTender$OtherTenderType:[I

    iget-object p0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->fromValue(I)Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    .line 101
    sget-object p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ONLINE_ONLY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    return-object p0

    .line 93
    :cond_0
    sget-object p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ALWAYS:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    return-object p0

    .line 104
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$1;->$SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

    iget-object p0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 116
    sget-object p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ONLINE_ONLY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    return-object p0

    .line 114
    :pswitch_0
    sget-object p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ONLINE_ONLY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    return-object p0

    .line 106
    :pswitch_1
    sget-object p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ALWAYS:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static writeTenderSettingsBytesWithLength(Lokio/Buffer;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lokio/Buffer;
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    .line 81
    array-length v0, p1

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeInt(I)Lokio/Buffer;

    .line 82
    invoke-virtual {p0, p1}, Lokio/Buffer;->write([B)Lokio/Buffer;

    return-object p0
.end method


# virtual methods
.method public addOtherTenders(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation

    .line 253
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getOtherTenderOptions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 254
    new-instance v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    const/4 v3, 0x0

    iget-object v1, v1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    invoke-direct {v2, v3, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V

    .line 256
    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 258
    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {p1, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    goto :goto_1

    .line 259
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 260
    :goto_1
    invoke-interface {p1, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-object p1
.end method

.method public createFromPosApiTenders(Ljava/util/List;ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;ZZ)",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings;"
        }
    .end annotation

    .line 268
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 271
    invoke-direct {p0, p2, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->generateMasterList(ZZ)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;

    move-result-object p2

    .line 273
    iget-object p3, p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->primaryTenders:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 274
    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 275
    iget-object v2, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 279
    :cond_1
    iget-object p2, p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsMasterList;->secondaryTenders:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 280
    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 281
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 285
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method public getTenderSettings(ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTenderSettings()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->processTenderSettings(Lcom/squareup/protos/client/devicesettings/TenderSettings;ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    return-object p1
.end method

.method public setTenderSettings(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addFilteredTenderBackIn(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/settings/server/AccountStatusSettings;->setTenderSettings(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V

    return-void
.end method
