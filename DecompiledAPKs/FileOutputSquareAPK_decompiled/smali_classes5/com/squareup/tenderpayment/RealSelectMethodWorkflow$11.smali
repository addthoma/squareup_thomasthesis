.class final synthetic Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$11;
.super Lkotlin/jvm/internal/MutablePropertyReference0;
.source "RealSelectMethodWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/MutablePropertyReference0;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$11;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 420
    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getShowSecondaryMethods$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "showSecondaryMethods"

    return-object v0
.end method

.method public getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "getShowSecondaryMethods()Z"

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$11;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 420
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$setShowSecondaryMethods$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Z)V

    return-void
.end method
