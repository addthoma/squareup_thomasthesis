.class abstract Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction;
.super Ljava/lang/Object;
.source "RealSeparateTenderV2Workflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "CustomEvenSplitAction"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction$CancelSelected;,
        Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction$EvenSplitSelected;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004\u0082\u0001\u0002\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "()V",
        "CancelSelected",
        "EvenSplitSelected",
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction$CancelSelected;",
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction$EvenSplitSelected;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 233
    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 233
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            "-",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
