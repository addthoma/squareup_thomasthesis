.class public final Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "SeparateCustomEvenCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->setupEditText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$1$1",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;

    .line 115
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;

    invoke-static {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->access$getContinueButton$p(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->access$isValidAmountEntered(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method
