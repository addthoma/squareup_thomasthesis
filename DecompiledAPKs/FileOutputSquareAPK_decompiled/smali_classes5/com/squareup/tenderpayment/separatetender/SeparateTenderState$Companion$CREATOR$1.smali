.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "SeparateTenderState.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSeparateTenderState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SeparateTenderState.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion$CREATOR$1\n+ 2 Parcels.kt\ncom/squareup/util/Parcels\n*L\n1#1,82:1\n23#2:83\n23#2:84\n23#2:85\n23#2:86\n*E\n*S KotlinDebug\n*F\n+ 1 SeparateTenderState.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion$CREATOR$1\n*L\n49#1:83\n51#1:84\n52#1:85\n53#1:86\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/tenderpayment/separatetender/SeparateTenderState$Companion$CREATOR$1",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "createFromParcel",
        "source",
        "Landroid/os/Parcel;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;
    .locals 11

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    const-class v0, Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 49
    :cond_0
    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    .line 84
    const-class v0, Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 51
    :cond_1
    move-object v5, v0

    check-cast v5, Lcom/squareup/protos/common/Money;

    .line 85
    const-class v0, Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 52
    :cond_2
    move-object v6, v0

    check-cast v6, Lcom/squareup/protos/common/Money;

    .line 86
    const-class v0, Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 53
    :cond_3
    move-object v7, v0

    check-cast v7, Lcom/squareup/protos/common/Money;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    .line 57
    const-class v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    :goto_0
    move-object v10, p1

    goto :goto_1

    .line 58
    :cond_4
    const-class v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomEvenSplit;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomEvenSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomEvenSplit;

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    goto :goto_0

    .line 60
    :cond_5
    const-class v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$ConfirmCancelSeparate;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    goto :goto_0

    .line 64
    :goto_1
    new-instance p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    .line 70
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v8

    .line 71
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object v1, p1

    .line 64
    invoke-direct/range {v1 .. v10}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;-><init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;)V

    return-object p1

    .line 61
    :cond_6
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown separate tender step: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion$CREATOR$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;
    .locals 0

    .line 77
    new-array p1, p1, [Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion$CREATOR$1;->newArray(I)[Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    move-result-object p1

    return-object p1
.end method
