.class final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;
.super Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;
.source "SeparateTenderAdapter.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HelpBannerRowViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0082\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J&\u0010\u0010\u001a\u00020\u00112\u001c\u0010\n\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0002`\u000f0\u000bH\u0016J\u0008\u0010\u0012\u001a\u00020\u0011H\u0016J\u0008\u0010\u0013\u001a\u00020\u0011H\u0016J\u0018\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\n\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0002`\u000f0\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;",
        "view",
        "Landroid/view/View;",
        "(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V",
        "disposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "helpBannerMessageView",
        "Landroid/widget/TextView;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "bind",
        "",
        "onAttach",
        "onDetach",
        "updateAmountSubtitle",
        "total",
        "Lcom/squareup/protos/common/Money;",
        "remaining",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final disposable:Lio/reactivex/disposables/SerialDisposable;

.field private final helpBannerMessageView:Landroid/widget/TextView;

.field private screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 206
    sget p1, Lcom/squareup/tenderworkflow/R$id;->split_tender_amount_subtitle:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->helpBannerMessageView:Landroid/widget/TextView;

    .line 207
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    return-void
.end method

.method public static final synthetic access$updateAmountSubtitle(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 205
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->updateAmountSubtitle(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method private final updateAmountSubtitle(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getRes$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/tenderworkflow/R$string;->split_tender_amount_remaining_help_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyFormatter$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/text/Formatter;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "amount_remaining"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 230
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyFormatter$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/text/Formatter;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string/jumbo v0, "total"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 231
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 232
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 234
    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->helpBannerMessageView:Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public bind(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public onAttach()V
    .locals 3

    .line 215
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->screens:Lio/reactivex/Observable;

    if-nez v1, :cond_0

    const-string v2, "screens"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder$onAttach$1;

    invoke-direct {v2, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder$onAttach$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onDetach()V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
