.class final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;
.super Ljava/lang/Object;
.source "SeparateTenderAdapter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->onAttach()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSeparateTenderAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SeparateTenderAdapter.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1\n*L\n1#1,462:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000120\u0010\u0002\u001a,\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\u0004\u0018\u0001`\u00060\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;)V"
        }
    .end annotation

    .line 264
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$setWorkflow$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 266
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$getInitialized$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$setInitialized$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;Z)V

    .line 268
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$setupViews(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    iget-object v2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    invoke-virtual {v2}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->getBillAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$updateMaxAmount(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;Lcom/squareup/protos/common/Money;)V

    .line 274
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$getAmountEditText$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    .line 275
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 277
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$updateAmount(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;Lcom/squareup/protos/common/Money;)V

    .line 280
    :cond_3
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$getContinueButton$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Landroid/view/View;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 241
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;->accept(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method
