.class final Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$2;
.super Ljava/lang/Object;
.source "SeparateCustomEvenCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->setupEditText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSeparateCustomEvenCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SeparateCustomEvenCoordinator.kt\ncom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$1$2\n*L\n1#1,150:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onFocusChange",
        "com/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$2;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 0

    .line 121
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$2;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;

    invoke-static {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->access$getEditText$p(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)Lcom/squareup/noho/NohoEditText;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/text/Editable;->clear()V

    .line 123
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/text/Editable;->length()I

    move-result p2

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setSelection(I)V

    return-void
.end method
