.class public final Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "SeparateCustomEvenCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->updateWorkflow(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J$\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "doOnEditorAction",
        "",
        "v",
        "Landroid/widget/TextView;",
        "actionId",
        "",
        "keyEvent",
        "Landroid/view/KeyEvent;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput;",
            ")V"
        }
    .end annotation

    .line 94
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 100
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;

    invoke-static {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->access$isValidAmountEntered(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 101
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;

    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->access$completeSplitTender(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
