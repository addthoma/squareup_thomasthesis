.class final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->render(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;->invoke(Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;)V
    .locals 2

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 511
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 515
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$cancellingBillDismissAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 513
    :cond_1
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$cancellingBillConfirmAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 510
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
