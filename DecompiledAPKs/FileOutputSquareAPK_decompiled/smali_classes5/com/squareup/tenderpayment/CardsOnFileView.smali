.class public Lcom/squareup/tenderpayment/CardsOnFileView;
.super Landroid/widget/LinearLayout;
.source "CardsOnFileView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;
    }
.end annotation


# static fields
.field private static final MAX_VISIBLE_CARDS_ON_FILE_PHONE:I = 0x1

.field private static final MAX_VISIBLE_CARDS_ON_FILE_TABLET:I = 0x3

.field private static final MAX_VISIBLE_GIFT_CARDS_ON_FILE_TABLET:I = 0x2


# instance fields
.field private final isEnabled:Z

.field private final maxVisible:I

.field private onAddNewCardClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onCardOnFileSelected:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;",
            ">;"
        }
    .end annotation
.end field

.field private final onNumberOfCardsClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;ZZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;",
            "Ljava/lang/String;",
            "ZZZ)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onCardOnFileSelected:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 70
    iput-boolean p4, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->isEnabled:Z

    if-eqz p6, :cond_1

    if-eqz p5, :cond_0

    const/4 p1, 0x2

    goto :goto_0

    :cond_0
    const/4 p1, 0x3

    .line 72
    :goto_0
    iput p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->maxVisible:I

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    .line 76
    iput p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->maxVisible:I

    :goto_1
    if-eqz p5, :cond_2

    .line 80
    invoke-direct {p0, p2, p3}, Lcom/squareup/tenderpayment/CardsOnFileView;->setGiftCardsOnFile(Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onNumberOfCardsClicked:Lio/reactivex/Observable;

    goto :goto_2

    .line 82
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/squareup/tenderpayment/CardsOnFileView;->setCreditCardsOnFile(Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onNumberOfCardsClicked:Lio/reactivex/Observable;

    :goto_2
    const/4 p1, 0x0

    .line 84
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/CardsOnFileView;->setOrientation(I)V

    return-void
.end method

.method private addAddGiftCard()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 122
    sget v0, Lcom/squareup/tenderworkflow/R$layout;->customer_card_on_file:I

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 123
    sget v1, Lcom/squareup/tenderworkflow/R$string;->new_card:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 124
    iget-boolean v1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->isEnabled:Z

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    .line 125
    invoke-virtual {p0, v0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addView(Landroid/view/View;)V

    .line 126
    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private addCardOnFileOption(ILcom/squareup/tenderpayment/CardOnFileSummary;)V
    .locals 2

    .line 147
    sget v0, Lcom/squareup/tenderworkflow/R$layout;->customer_card_on_file:I

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 148
    iget-boolean v1, p2, Lcom/squareup/tenderpayment/CardOnFileSummary;->isExpired:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 149
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    goto :goto_0

    .line 151
    :cond_0
    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$CardsOnFileView$XR8a6RKMISjXjzC7QCyPGsYvy3k;

    invoke-direct {v1, p0, p2}, Lcom/squareup/tenderpayment/-$$Lambda$CardsOnFileView$XR8a6RKMISjXjzC7QCyPGsYvy3k;-><init>(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/tenderpayment/CardOnFileSummary;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    :goto_0
    iget-object p2, p2, Lcom/squareup/tenderpayment/CardOnFileSummary;->cardNameAndNumber:Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTag(Ljava/lang/Object;)V

    .line 156
    invoke-virtual {p0, v0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addView(Landroid/view/View;)V

    return-void
.end method

.method private addNoCardsOnFile()V
    .locals 2

    .line 172
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->removeAllViews()V

    .line 173
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/tenderworkflow/R$layout;->customer_no_cards_on_file:I

    invoke-static {v0, v1, p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method private addNumberOfCardsOnFile(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 160
    sget v0, Lcom/squareup/tenderworkflow/R$layout;->customer_card_on_file:I

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 161
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-boolean p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->isEnabled:Z

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    .line 163
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->removeAllViews()V

    .line 164
    invoke-virtual {p0, v0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addView(Landroid/view/View;)V

    .line 165
    iget-boolean p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->isEnabled:Z

    if-eqz p1, :cond_0

    .line 166
    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 168
    :cond_0
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method private setCreditCardsOnFile(Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 131
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-direct {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addNoCardsOnFile()V

    .line 133
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 135
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->isEnabled:Z

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->maxVisible:I

    if-le v0, v1, :cond_1

    goto :goto_1

    .line 139
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->removeAllViews()V

    const/4 p2, 0x0

    .line 140
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_2

    .line 141
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/CardOnFileSummary;

    invoke-direct {p0, p2, v0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addCardOnFileOption(ILcom/squareup/tenderpayment/CardOnFileSummary;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 143
    :cond_2
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 136
    :cond_3
    :goto_1
    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/CardsOnFileView;->addNumberOfCardsOnFile(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method private setGiftCardsOnFile(Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 102
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-direct {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addNoCardsOnFile()V

    .line 104
    invoke-direct {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addAddGiftCard()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onAddNewCardClicked:Lio/reactivex/Observable;

    .line 105
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 107
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->isEnabled:Z

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->maxVisible:I

    if-le v0, v1, :cond_1

    goto :goto_1

    .line 113
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->removeAllViews()V

    const/4 p2, 0x0

    .line 114
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_2

    .line 115
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/CardOnFileSummary;

    invoke-direct {p0, p2, v0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addCardOnFileOption(ILcom/squareup/tenderpayment/CardOnFileSummary;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 117
    :cond_2
    invoke-direct {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addAddGiftCard()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onAddNewCardClicked:Lio/reactivex/Observable;

    .line 118
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 108
    :cond_3
    :goto_1
    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/CardsOnFileView;->addNumberOfCardsOnFile(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    .line 109
    invoke-direct {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->addAddGiftCard()Lio/reactivex/Observable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onAddNewCardClicked:Lio/reactivex/Observable;

    return-object p1
.end method


# virtual methods
.method public synthetic lambda$addCardOnFileOption$0$CardsOnFileView(Lcom/squareup/tenderpayment/CardOnFileSummary;Landroid/view/View;)V
    .locals 2

    .line 151
    iget-object p2, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onCardOnFileSelected:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v0, Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;

    iget-object v1, p1, Lcom/squareup/tenderpayment/CardOnFileSummary;->instrumentToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/tenderpayment/CardOnFileSummary;->cardNameAndNumber:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onAddNewCardClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onAddNewCardClicked:Lio/reactivex/Observable;

    return-object v0
.end method

.method public onCardOnFileSelected()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;",
            ">;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onCardOnFileSelected:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public onNumberOfCardsClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/tenderpayment/CardsOnFileView;->onNumberOfCardsClicked:Lio/reactivex/Observable;

    return-object v0
.end method
