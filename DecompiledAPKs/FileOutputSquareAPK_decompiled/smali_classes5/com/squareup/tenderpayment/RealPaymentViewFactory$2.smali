.class final synthetic Lcom/squareup/tenderpayment/RealPaymentViewFactory$2;
.super Lkotlin/jvm/internal/FunctionReference;
.source "RealPaymentViewFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealPaymentViewFactory;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;Ljava/util/Set;Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        ">;>;",
        "Lcom/squareup/tenderpayment/SelectMethodCoordinator;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u00b9\u0001\u0010\u0003\u001a\u00b4\u0001\u0012D\u0012B\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00070\u0007 \u0002* \u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00050\u0005 \u0002*Y\u0012D\u0012B\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00070\u0007 \u0002* \u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00050\u0005\u0018\u00010\u0004\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n0\u0004\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/tenderpayment/SelectMethodCoordinator;",
        "kotlin.jvm.PlatformType",
        "p1",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "Lkotlin/ParameterName;",
        "name",
        "screens",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "build"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "build(Lio/reactivex/Observable;)Lcom/squareup/tenderpayment/SelectMethodCoordinator;"

    return-object v0
.end method

.method public final invoke(Lio/reactivex/Observable;)Lcom/squareup/tenderpayment/SelectMethodCoordinator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;>;)",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$2;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;

    .line 53
    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->build(Lio/reactivex/Observable;)Lcom/squareup/tenderpayment/SelectMethodCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$2;->invoke(Lio/reactivex/Observable;)Lcom/squareup/tenderpayment/SelectMethodCoordinator;

    move-result-object p1

    return-object p1
.end method
