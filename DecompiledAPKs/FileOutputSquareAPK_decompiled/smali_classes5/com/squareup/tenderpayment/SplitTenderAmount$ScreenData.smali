.class public final Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;
.super Ljava/lang/Object;
.source "SplitTenderAmountScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SplitTenderAmount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0002\u0010\u000eR\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\n8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;",
        "",
        "amountEntered",
        "Lcom/squareup/protos/common/Money;",
        "amountRemainingAfterAmountEntered",
        "billAmount",
        "billAmountRemaining",
        "maxSplits",
        "",
        "isDoneEnabled",
        "",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final amountEntered:Lcom/squareup/protos/common/Money;

.field public final amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

.field public final billAmount:Lcom/squareup/protos/common/Money;

.field public final billAmountRemaining:Lcom/squareup/protos/common/Money;

.field public final completedTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field public final isDoneEnabled:Z

.field public final maxSplits:J


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "JZ",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)V"
        }
    .end annotation

    const-string v0, "amountEntered"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountRemainingAfterAmountEntered"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmountRemaining"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;->amountEntered:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;->billAmount:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    iput-wide p5, p0, Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;->maxSplits:J

    iput-boolean p7, p0, Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;->isDoneEnabled:Z

    iput-object p8, p0, Lcom/squareup/tenderpayment/SplitTenderAmount$ScreenData;->completedTenders:Ljava/util/List;

    return-void
.end method
