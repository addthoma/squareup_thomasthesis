.class public final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$checkCancelBillPaymentPermissions$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "RealSelectMethodWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->checkCancelBillPaymentPermissions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/tenderpayment/RealSelectMethodWorkflow$checkCancelBillPaymentPermissions$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "failure",
        "",
        "success",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1138
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$checkCancelBillPaymentPermissions$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 2

    .line 1144
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$checkCancelBillPaymentPermissions$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillDismiss;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillDismiss;

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$onEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public success()V
    .locals 2

    .line 1140
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$checkCancelBillPaymentPermissions$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillConfirmed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillConfirmed;

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$onEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method
