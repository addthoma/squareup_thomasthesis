.class public final Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory;
.super Ljava/lang/Object;
.source "ConfirmCollectCashDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConfirmCollectCashDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConfirmCollectCashDialogFactory.kt\ncom/squareup/tenderpayment/ConfirmCollectCashDialogFactory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,50:1\n52#2:51\n*E\n*S KotlinDebug\n*F\n+ 1 ConfirmCollectCashDialogFactory.kt\ncom/squareup/tenderpayment/ConfirmCollectCashDialogFactory\n*L\n25#1:51\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u000fB\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0002B%\u0012\u001e\u0010\u0003\u001a\u001a\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\tJ\u0016\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0016R&\u0010\u0003\u001a\u001a\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialogScreen;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "ParentComponent",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory;-><init>(Lio/reactivex/Observable;)V

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
            ">;>;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory;->screens:Lio/reactivex/Observable;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 51
    :cond_0
    const-class v0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$ParentComponent;

    .line 25
    invoke-interface {v0}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$ParentComponent;->dialogWorkflowScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    .line 24
    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "just(\n        context.co\u2026logWorkflowScreen()\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    :goto_0
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$create$1;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$create$1;-><init>(Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "resolvedScreen\n        .\u2026uilder.create()\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
