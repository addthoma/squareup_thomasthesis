.class public final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;
.super Ljava/lang/Object;
.source "SelectMethodStateWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TypeSafeUpdater"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<StateT:",
        "Ljava/lang/Object;",
        "SafeStateT::TStateT;OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\r\n\u0002\u0010\u0002\n\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u0002H\u0001*\n\u0008\u0002\u0010\u0003 \u0000*\u00020\u00042\u00020\u0004B\u0017\u0012\u0006\u0010\u0005\u001a\u00028\u0001\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0007J\u0013\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000e\u001a\u00028\u0002\u00a2\u0006\u0002\u0010\rR\u0013\u0010\u0005\u001a\u00028\u0001\u00a2\u0006\n\n\u0002\u0010\n\u001a\u0004\u0008\u0008\u0010\tR\u001c\u0010\u0006\u001a\u00028\u0000X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\n\u001a\u0004\u0008\u000b\u0010\t\"\u0004\u0008\u000c\u0010\rR\u001e\u0010\u000e\u001a\u0004\u0018\u00018\u0002X\u0080\u000e\u00a2\u0006\u0010\n\u0002\u0010\n\u001a\u0004\u0008\u000f\u0010\t\"\u0004\u0008\u0010\u0010\r\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;",
        "StateT",
        "SafeStateT",
        "OutputT",
        "",
        "currentState",
        "nextState",
        "(Ljava/lang/Object;Ljava/lang/Object;)V",
        "getCurrentState",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getNextState",
        "setNextState",
        "(Ljava/lang/Object;)V",
        "output",
        "getOutput$tender_payment_release",
        "setOutput$tender_payment_release",
        "setOutput",
        "",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentState:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSafeStateT;"
        }
    .end annotation
.end field

.field private nextState:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TStateT;"
        }
    .end annotation
.end field

.field private output:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TOutputT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSafeStateT;TStateT;)V"
        }
    .end annotation

    .line 1309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->currentState:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->nextState:Ljava/lang/Object;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    move-object p2, p1

    .line 1311
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getCurrentState()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TSafeStateT;"
        }
    .end annotation

    .line 1310
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->currentState:Ljava/lang/Object;

    return-object v0
.end method

.method public final getNextState()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TStateT;"
        }
    .end annotation

    .line 1311
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->nextState:Ljava/lang/Object;

    return-object v0
.end method

.method public final getOutput$tender_payment_release()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TOutputT;"
        }
    .end annotation

    .line 1313
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->output:Ljava/lang/Object;

    return-object v0
.end method

.method public final setNextState(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TStateT;)V"
        }
    .end annotation

    .line 1311
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->nextState:Ljava/lang/Object;

    return-void
.end method

.method public final setOutput(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TOutputT;)V"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1320
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->output:Ljava/lang/Object;

    return-void
.end method

.method public final setOutput$tender_payment_release(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TOutputT;)V"
        }
    .end annotation

    .line 1313
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;->output:Ljava/lang/Object;

    return-void
.end method
