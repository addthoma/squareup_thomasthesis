.class public final Lcom/squareup/tenderpayment/TenderSettingsFactory;
.super Ljava/lang/Object;
.source "TenderSettingsFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u001c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0002J\u0008\u0010\u0013\u001a\u00020\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0014H\u0002J\u0008\u0010\u0017\u001a\u00020\u0014H\u0002J\u0006\u0010\u0018\u001a\u00020\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/TenderSettingsFactory;",
        "",
        "apiTransactionState",
        "Lcom/squareup/api/ApiTransactionState;",
        "tenderSettingsManager",
        "Lcom/squareup/tenderpayment/TenderSettingsManager;",
        "customerManagementSettings",
        "Lcom/squareup/crm/CustomerManagementSettings;",
        "invoiceTenderSetting",
        "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/payment/Transaction;)V",
        "convertApiTenderTypes",
        "",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "apiTenderTypes",
        "",
        "Lcom/squareup/api/ApiTenderType;",
        "createFromApiTenders",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
        "createFromInvoiceTenders",
        "tenderSettings",
        "createFromPosTenders",
        "createTenders",
        "tender-payment-legacy_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

.field private final tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "apiTransactionState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderSettingsManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerManagementSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTenderSetting"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    iput-object p3, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    iput-object p4, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    iput-object p5, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method

.method private final convertApiTenderTypes(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lcom/squareup/api/ApiTenderType;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 64
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/ApiTenderType;

    .line 65
    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/api/ApiTenderType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 77
    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v1}, Lcom/squareup/api/ApiTransactionState;->giftCardEntrySupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_0
    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "New enum value in ApiTenderType not handled"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 74
    :cond_2
    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 73
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    :cond_3
    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    move-object v2, v0

    check-cast v2, Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->addOtherTenders(Ljava/util/List;)Ljava/util/List;

    goto :goto_0

    .line 70
    :cond_4
    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 69
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    :cond_5
    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 66
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    :cond_6
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final createFromApiTenders()Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    .line 39
    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v1}, Lcom/squareup/api/ApiTransactionState;->tenderTypes()Ljava/util/Set;

    move-result-object v1

    const-string v2, "apiTransactionState.tenderTypes()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    invoke-direct {p0, v1}, Lcom/squareup/tenderpayment/TenderSettingsFactory;->convertApiTenderTypes(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 40
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v2}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v2

    .line 41
    iget-object v3, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    invoke-interface {v3}, Lcom/squareup/tenderpayment/InvoiceTenderSetting;->canUseInvoiceAsTender()Z

    move-result v3

    .line 38
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->createFromPosApiTenders(Ljava/util/List;ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    const-string/jumbo v1, "tenderSettingsManager.cr\u2026seInvoiceAsTender()\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final createFromInvoiceTenders(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 3

    .line 54
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 56
    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    const/4 v2, 0x0

    .line 53
    invoke-static {v0, p1, v1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->moveTenderToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Z)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p1

    const-string v0, "TenderSettingsManager.mo\u2026BLED,\n        false\n    )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final createFromPosTenders()Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    .line 47
    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v1}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v1

    .line 48
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    invoke-interface {v2}, Lcom/squareup/tenderpayment/InvoiceTenderSetting;->canUseInvoiceAsTender()Z

    move-result v2

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderSettings(ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    const-string/jumbo v1, "tenderSettingsManager.ge\u2026seInvoiceAsTender()\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final createTenders()Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderSettingsFactory;->createFromApiTenders()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    return-object v0

    .line 28
    :cond_0
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderSettingsFactory;->createFromPosTenders()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 31
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/TenderSettingsFactory;->createFromInvoiceTenders(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    :cond_1
    return-object v0
.end method
