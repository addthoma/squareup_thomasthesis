.class final Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;
.super Lkotlin/jvm/internal/Lambda;
.source "TenderPaymentWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->render(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/payother/PayOtherOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
        "+",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "result",
        "Lcom/squareup/checkoutflow/payother/PayOtherOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/tenderpayment/TenderPaymentConfig;

.field final synthetic $state:Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

.field final synthetic this$0:Lcom/squareup/tenderpayment/TenderPaymentWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;->this$0:Lcom/squareup/tenderpayment/TenderPaymentWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;->$props:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    iput-object p3, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;->$state:Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/payother/PayOtherOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/payother/PayOtherOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;->this$0:Lcom/squareup/tenderpayment/TenderPaymentWorkflow;

    .line 215
    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;->$props:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;->$state:Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    check-cast v2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    invoke-virtual {v2}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->getInput()Lcom/squareup/checkoutflow/payother/PayOtherInput;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;->$state:Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;

    check-cast v3, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;

    invoke-virtual {v3}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;->getRestartSelectMethodWorkflowData()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    move-result-object v3

    .line 214
    invoke-static {v0, v1, v2, p1, v3}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;->access$handlePayOtherResult(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/checkoutflow/payother/PayOtherOutput;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/checkoutflow/payother/PayOtherOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow$render$rendering$4;->invoke(Lcom/squareup/checkoutflow/payother/PayOtherOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
