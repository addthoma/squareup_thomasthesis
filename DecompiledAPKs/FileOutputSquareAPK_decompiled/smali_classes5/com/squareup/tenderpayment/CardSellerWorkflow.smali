.class public interface abstract Lcom/squareup/tenderpayment/CardSellerWorkflow;
.super Ljava/lang/Object;
.source "CardSellerWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0001\u000bJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\tH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/CardSellerWorkflow;",
        "",
        "authorize",
        "Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;",
        "card",
        "Lcom/squareup/Card;",
        "readyToAuthorize",
        "",
        "startCardNotPresent",
        "Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;",
        "startGiftCard",
        "Result",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract authorize(Lcom/squareup/Card;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.end method

.method public abstract readyToAuthorize(Lcom/squareup/Card;)Z
.end method

.method public abstract startCardNotPresent()Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;
.end method

.method public abstract startGiftCard()Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;
.end method
