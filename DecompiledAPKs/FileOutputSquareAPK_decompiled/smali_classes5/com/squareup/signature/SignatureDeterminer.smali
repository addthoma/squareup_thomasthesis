.class public final Lcom/squareup/signature/SignatureDeterminer;
.super Ljava/lang/Object;
.source "SignatureDeterminer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signature/SignatureDeterminer$Builder;
    }
.end annotation


# instance fields
.field private final cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field private final hasIssuerRequestForSignature:Z

.field private final isFallbackSwipe:Z

.field private final isGiftCard:Z

.field private final merchantAlwaysSkipSignature:Z

.field private final merchantIsAllowedToNSR:Z

.field private final merchantOptedInToNSR:Z

.field private final showSignatureForCardNotPresent:Z

.field private final showSignatureForCardOnFile:Z

.field private final signatureRequiredThreshold:Lcom/squareup/protos/common/Money;

.field private final smartReaderPresentAndRequestsSignature:Z


# direct methods
.method private constructor <init>(Lcom/squareup/signature/SignatureDeterminer$Builder;)V
    .locals 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$000(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->hasIssuerRequestForSignature:Z

    .line 33
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$100(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->merchantAlwaysSkipSignature:Z

    .line 34
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$200(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->merchantIsAllowedToNSR:Z

    .line 35
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$300(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->merchantOptedInToNSR:Z

    .line 36
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$400(Lcom/squareup/signature/SignatureDeterminer$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/signature/SignatureDeterminer;->signatureRequiredThreshold:Lcom/squareup/protos/common/Money;

    .line 37
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$500(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->isGiftCard:Z

    .line 38
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$600(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->isFallbackSwipe:Z

    .line 39
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$700(Lcom/squareup/signature/SignatureDeterminer$Builder;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/signature/SignatureDeterminer;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 40
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$800(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->smartReaderPresentAndRequestsSignature:Z

    .line 41
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$900(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->showSignatureForCardNotPresent:Z

    .line 42
    invoke-static {p1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->access$1000(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/signature/SignatureDeterminer;->showSignatureForCardOnFile:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/signature/SignatureDeterminer$Builder;Lcom/squareup/signature/SignatureDeterminer$1;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureDeterminer;-><init>(Lcom/squareup/signature/SignatureDeterminer$Builder;)V

    return-void
.end method


# virtual methods
.method public shouldAskForSignature(Lcom/squareup/protos/common/Money;)Z
    .locals 3

    .line 46
    iget-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->isGiftCard:Z

    const/4 v1, 0x0

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->merchantAlwaysSkipSignature:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 50
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->hasIssuerRequestForSignature:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    .line 54
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->merchantIsAllowedToNSR:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/signature/SignatureDeterminer;->merchantOptedInToNSR:Z

    if-eqz v0, :cond_2

    .line 59
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer;->signatureRequiredThreshold:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_2

    return v1

    .line 68
    :cond_2
    iget-object p1, p0, Lcom/squareup/signature/SignatureDeterminer;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eq p1, v0, :cond_5

    iget-boolean p1, p0, Lcom/squareup/signature/SignatureDeterminer;->isFallbackSwipe:Z

    if-nez p1, :cond_5

    iget-object p1, p0, Lcom/squareup/signature/SignatureDeterminer;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne p1, v0, :cond_3

    iget-boolean p1, p0, Lcom/squareup/signature/SignatureDeterminer;->showSignatureForCardOnFile:Z

    if-nez p1, :cond_5

    :cond_3
    iget-object p1, p0, Lcom/squareup/signature/SignatureDeterminer;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne p1, v0, :cond_4

    iget-boolean p1, p0, Lcom/squareup/signature/SignatureDeterminer;->showSignatureForCardNotPresent:Z

    if-eqz p1, :cond_4

    goto :goto_0

    .line 76
    :cond_4
    iget-boolean p1, p0, Lcom/squareup/signature/SignatureDeterminer;->smartReaderPresentAndRequestsSignature:Z

    return p1

    :cond_5
    :goto_0
    return v2

    :cond_6
    :goto_1
    return v1
.end method
