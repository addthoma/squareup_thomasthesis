.class public interface abstract Lcom/squareup/signature/SignatureSmoothingStrategy;
.super Ljava/lang/Object;
.source "SignatureSmoothingStrategy.java"


# static fields
.field public static final NONE:Lcom/squareup/signature/SignatureSmoothingStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/signature/-$$Lambda$SignatureSmoothingStrategy$GYOlmXIpO9Aqs6daUyzdOLIUm3U;->INSTANCE:Lcom/squareup/signature/-$$Lambda$SignatureSmoothingStrategy$GYOlmXIpO9Aqs6daUyzdOLIUm3U;

    sput-object v0, Lcom/squareup/signature/SignatureSmoothingStrategy;->NONE:Lcom/squareup/signature/SignatureSmoothingStrategy;

    return-void
.end method


# virtual methods
.method public abstract includeNextPoint()Z
.end method
