.class public interface abstract Lcom/squareup/sqldelight/Transacter;
.super Ljava/lang/Object;
.source "Transacter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqldelight/Transacter$Transaction;,
        Lcom/squareup/sqldelight/Transacter$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\nJ+\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0017\u0010\u0006\u001a\u0013\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\u0002\u0008\tH&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/sqldelight/Transacter;",
        "",
        "transaction",
        "",
        "noEnclosing",
        "",
        "body",
        "Lkotlin/Function1;",
        "Lcom/squareup/sqldelight/Transacter$Transaction;",
        "Lkotlin/ExtensionFunctionType;",
        "Transaction",
        "runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract transaction(ZLkotlin/jvm/functions/Function1;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/Transacter$Transaction;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method
