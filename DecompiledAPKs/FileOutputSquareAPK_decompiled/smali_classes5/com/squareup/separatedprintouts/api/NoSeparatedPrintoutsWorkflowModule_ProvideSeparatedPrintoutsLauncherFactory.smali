.class public final Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsLauncherFactory;
.super Ljava/lang/Object;
.source "NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsLauncherFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsLauncherFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsLauncherFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsLauncherFactory$InstanceHolder;->access$000()Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsLauncherFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSeparatedPrintoutsLauncher()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;->provideSeparatedPrintoutsLauncher()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsLauncherFactory;->provideSeparatedPrintoutsLauncher()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsLauncherFactory;->get()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    move-result-object v0

    return-object v0
.end method
