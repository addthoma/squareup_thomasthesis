.class public interface abstract Lcom/squareup/securetouch/SecureTouchWorkflowRunner;
.super Ljava/lang/Object;
.source "SecureTouchWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;,
        Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u0000 \u00032\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0003\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "Companion",
        "NoSecureTouchWorkflowRunner",
        "secure-touch-workflow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner;->Companion:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;

    return-void
.end method
