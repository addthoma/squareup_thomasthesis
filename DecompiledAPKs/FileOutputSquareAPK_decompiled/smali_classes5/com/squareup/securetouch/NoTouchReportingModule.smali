.class public abstract Lcom/squareup/securetouch/NoTouchReportingModule;
.super Ljava/lang/Object;
.source "NoTouchReportingModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideNoTouchReporting()Lcom/squareup/securetouch/TouchReporting;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 10
    sget-object v0, Lcom/squareup/securetouch/NoTouchReporting;->INSTANCE:Lcom/squareup/securetouch/NoTouchReporting;

    return-object v0
.end method
