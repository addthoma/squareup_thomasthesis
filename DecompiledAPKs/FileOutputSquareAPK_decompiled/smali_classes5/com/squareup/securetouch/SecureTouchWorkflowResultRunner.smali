.class public final Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;
.super Ljava/lang/Object;
.source "SecureTouchWorkflowResultRunner.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureTouchWorkflowResultRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureTouchWorkflowResultRunner.kt\ncom/squareup/securetouch/SecureTouchWorkflowResultRunner\n*L\n1#1,64:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\r\u001a\u00020\u000eH\u0002J\u0008\u0010\u000f\u001a\u00020\u000eH\u0002J\u0010\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u000e\u0010\u001c\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u001dJ\u0010\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020\u001bH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;",
        "",
        "cardreader",
        "Lcom/squareup/securetouch/SecureTouchFeature;",
        "secureTouchWorkflowResultRelay",
        "Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;",
        "currentSecureTouchMode",
        "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
        "analyticsFactory",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;",
        "(Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;Lcom/squareup/securetouch/CurrentSecureTouchMode;Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;)V",
        "analytics",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;",
        "onCancelEnableAccessibleKeypadDialog",
        "",
        "onEnableSecureTouchAccessibilityMode",
        "onFinish",
        "result",
        "Lcom/squareup/securetouch/FinishedSecureTouchResult;",
        "onLogKeyPress",
        "keyPressEvent",
        "Lcom/squareup/securetouch/SecureTouchKeyPressEvent;",
        "onMadeKeypadActive",
        "keypadActive",
        "Lcom/squareup/securetouch/SecureTouchKeypadActive;",
        "onReleaseEvent",
        "point",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "onResult",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "onTouchEvent",
        "secureTouchPoint",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

.field private final cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

.field private final currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

.field private final secureTouchWorkflowResultRelay:Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;Lcom/squareup/securetouch/CurrentSecureTouchMode;Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardreader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "secureTouchWorkflowResultRelay"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentSecureTouchMode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->secureTouchWorkflowResultRelay:Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    const/4 p1, 0x0

    const/4 p2, 0x1

    .line 15
    invoke-static {p4, p1, p2, p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;->create$default(Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    return-void
.end method

.method private final onCancelEnableAccessibleKeypadDialog()V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    sget-object v1, Lcom/squareup/securetouch/InitiateSecureTouch;->INSTANCE:Lcom/squareup/securetouch/InitiateSecureTouch;

    check-cast v1, Lcom/squareup/securetouch/SecureTouchApplicationEvent;

    invoke-interface {v0, v1}, Lcom/squareup/securetouch/SecureTouchFeature;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    return-void
.end method

.method private final onEnableSecureTouchAccessibilityMode()V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    invoke-virtual {v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logAccessibilityStart()V

    .line 60
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    sget-object v1, Lcom/squareup/securetouch/SecureTouchMode$AccessibilityMode;->INSTANCE:Lcom/squareup/securetouch/SecureTouchMode$AccessibilityMode;

    check-cast v1, Lcom/squareup/securetouch/SecureTouchMode;

    invoke-virtual {v0, v1}, Lcom/squareup/securetouch/CurrentSecureTouchMode;->setSecureTouchMode(Lcom/squareup/securetouch/SecureTouchMode;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    sget-object v1, Lcom/squareup/securetouch/InitiateSecureTouch;->INSTANCE:Lcom/squareup/securetouch/InitiateSecureTouch;

    check-cast v1, Lcom/squareup/securetouch/SecureTouchApplicationEvent;

    invoke-interface {v0, v1}, Lcom/squareup/securetouch/SecureTouchFeature;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    return-void
.end method

.method private final onFinish(Lcom/squareup/securetouch/FinishedSecureTouchResult;)V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    invoke-virtual {v0}, Lcom/squareup/securetouch/CurrentSecureTouchMode;->getSecureTouchMode()Lcom/squareup/securetouch/SecureTouchMode;

    move-result-object v0

    .line 32
    instance-of v1, v0, Lcom/squareup/securetouch/SecureTouchMode$RegularMode;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    invoke-virtual {v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logFinish()V

    goto :goto_0

    .line 33
    :cond_0
    instance-of v0, v0, Lcom/squareup/securetouch/SecureTouchMode$AccessibilityMode;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    invoke-virtual {v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logAccessibilityFinish()V

    .line 35
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->secureTouchWorkflowResultRelay:Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;

    check-cast p1, Lcom/squareup/securetouch/SecureTouchResult;

    invoke-interface {v0, p1}, Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;->finishWithResult(Lcom/squareup/securetouch/SecureTouchResult;)V

    return-void
.end method

.method private final onLogKeyPress(Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logKeyPressEvent(Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)V

    return-void
.end method

.method private final onMadeKeypadActive(Lcom/squareup/securetouch/SecureTouchKeypadActive;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    check-cast p1, Lcom/squareup/securetouch/SecureTouchApplicationEvent;

    invoke-interface {v0, p1}, Lcom/squareup/securetouch/SecureTouchFeature;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    return-void
.end method

.method private final onReleaseEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {v0, p1}, Lcom/squareup/securetouch/SecureTouchFeature;->onUnexpectedReleaseEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V

    return-void
.end method

.method private final onTouchEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {v0, p1}, Lcom/squareup/securetouch/SecureTouchFeature;->onUnexpectedTouchEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V

    return-void
.end method


# virtual methods
.method public final onResult(Lcom/squareup/securetouch/SecureTouchResult;)V
    .locals 2

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    instance-of v0, p1, Lcom/squareup/securetouch/FinishedSecureTouchResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/FinishedSecureTouchResult;

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->onFinish(Lcom/squareup/securetouch/FinishedSecureTouchResult;)V

    goto :goto_0

    .line 20
    :cond_0
    instance-of v0, p1, Lcom/squareup/securetouch/MadeKeypadActive;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/securetouch/MadeKeypadActive;

    invoke-virtual {p1}, Lcom/squareup/securetouch/MadeKeypadActive;->getKeypadActive()Lcom/squareup/securetouch/SecureTouchKeypadActive;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->onMadeKeypadActive(Lcom/squareup/securetouch/SecureTouchKeypadActive;)V

    goto :goto_0

    .line 21
    :cond_1
    instance-of v0, p1, Lcom/squareup/securetouch/MotionTouchEventDevOnly;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/securetouch/MotionTouchEventDevOnly;

    invoke-virtual {p1}, Lcom/squareup/securetouch/MotionTouchEventDevOnly;->getAtSecureTouchPoint()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->onTouchEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V

    goto :goto_0

    .line 22
    :cond_2
    instance-of v0, p1, Lcom/squareup/securetouch/MotionReleaseEventDevOnly;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/securetouch/MotionReleaseEventDevOnly;

    invoke-virtual {p1}, Lcom/squareup/securetouch/MotionReleaseEventDevOnly;->getAtSecureTouchPoint()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->onReleaseEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V

    goto :goto_0

    .line 23
    :cond_3
    instance-of v0, p1, Lcom/squareup/securetouch/LogKeyPress;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/securetouch/LogKeyPress;

    invoke-virtual {p1}, Lcom/squareup/securetouch/LogKeyPress;->getKeyPressEvent()Lcom/squareup/securetouch/SecureTouchKeyPressEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->onLogKeyPress(Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)V

    goto :goto_0

    .line 24
    :cond_4
    instance-of v0, p1, Lcom/squareup/securetouch/CancelEnableAccessibleKeypadDialog;

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->onCancelEnableAccessibleKeypadDialog()V

    goto :goto_0

    .line 25
    :cond_5
    instance-of v0, p1, Lcom/squareup/securetouch/EnableSecureTouchAccessibilityMode;

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;->onEnableSecureTouchAccessibilityMode()V

    :goto_0
    return-void

    .line 26
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unhandled result: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
