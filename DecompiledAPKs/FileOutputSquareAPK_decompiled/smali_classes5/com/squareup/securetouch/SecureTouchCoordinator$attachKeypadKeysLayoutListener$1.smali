.class final Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SecureTouchCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/SecureTouchCoordinator;->attachKeypadKeysLayoutListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/SecureTouchCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/SecureTouchCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1;->this$0:Lcom/squareup/securetouch/SecureTouchCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 152
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1;->this$0:Lcom/squareup/securetouch/SecureTouchCoordinator;

    invoke-static {v0}, Lcom/squareup/securetouch/SecureTouchCoordinator;->access$getKeypadKeys$p(Lcom/squareup/securetouch/SecureTouchCoordinator;)Lcom/squareup/securetouch/Keypad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/securetouch/Keypad;->onLayoutChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 153
    new-instance v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1$1;

    iget-object v2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1;->this$0:Lcom/squareup/securetouch/SecureTouchCoordinator;

    invoke-static {v2}, Lcom/squareup/securetouch/SecureTouchCoordinator;->access$getOnKeypadKeysLayoutChanged$p(Lcom/squareup/securetouch/SecureTouchCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1$1;-><init>(Lcom/jakewharton/rxrelay2/PublishRelay;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/securetouch/SecureTouchCoordinatorKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "keypadKeys.onLayoutChang\u2026eysLayoutChanged::accept)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
