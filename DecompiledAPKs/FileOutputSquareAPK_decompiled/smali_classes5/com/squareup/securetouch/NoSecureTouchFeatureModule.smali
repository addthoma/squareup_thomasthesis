.class public abstract Lcom/squareup/securetouch/NoSecureTouchFeatureModule;
.super Ljava/lang/Object;
.source "NoSecureTouchFeatureModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideNoSecureTouchFeature()Lcom/squareup/securetouch/SecureTouchFeature;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 10
    sget-object v0, Lcom/squareup/securetouch/NoSecureTouchFeature;->INSTANCE:Lcom/squareup/securetouch/NoSecureTouchFeature;

    return-object v0
.end method
