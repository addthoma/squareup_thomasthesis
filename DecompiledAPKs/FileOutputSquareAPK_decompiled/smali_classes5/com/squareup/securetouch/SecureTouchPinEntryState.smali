.class public abstract Lcom/squareup/securetouch/SecureTouchPinEntryState;
.super Ljava/lang/Object;
.source "SecureTouchPinEntryState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0003\u0004\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "",
        "()V",
        "Lcom/squareup/securetouch/PinFirstTry;",
        "Lcom/squareup/securetouch/PinRetry;",
        "Lcom/squareup/securetouch/PinLastTry;",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/squareup/securetouch/SecureTouchPinEntryState;-><init>()V

    return-void
.end method
