.class final Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$createDialog$2;
.super Ljava/lang/Object;
.source "EnableAccessibleKeypadDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$createDialog$2;->$screen:Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 61
    iget-object p1, p0, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$createDialog$2;->$screen:Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;

    invoke-virtual {p1}, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object p2, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent$HideDialog;->INSTANCE:Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$EnableAccessibleKeypadDialogEvent$HideDialog;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
