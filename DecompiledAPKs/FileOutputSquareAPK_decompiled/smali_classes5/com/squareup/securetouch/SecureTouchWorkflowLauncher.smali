.class public final Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;
.super Ljava/lang/Object;
.source "SecureTouchWorkflowLauncher.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;",
        "",
        "flow",
        "Lflow/Flow;",
        "currentSecureTouchMode",
        "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
        "(Lflow/Flow;Lcom/squareup/securetouch/CurrentSecureTouchMode;)V",
        "launch",
        "",
        "parentScope",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "secureTouchPinRequestData",
        "Lcom/squareup/securetouch/SecureTouchPinRequestData;",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/securetouch/CurrentSecureTouchMode;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentSecureTouchMode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    return-void
.end method

.method public static final synthetic access$getCurrentSecureTouchMode$p(Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;)Lcom/squareup/securetouch/CurrentSecureTouchMode;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    return-object p0
.end method


# virtual methods
.method public final launch(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 3

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "secureTouchPinRequestData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;-><init>(Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string p1, "launchSecureTouchWorkflow"

    invoke-direct {v1, p1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
