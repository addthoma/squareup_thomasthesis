.class public final Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;
.super Ljava/lang/Object;
.source "SecureTouchAnalyticsLogger.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;,
        Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureTouchAnalyticsLogger.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureTouchAnalyticsLogger.kt\ncom/squareup/securetouch/SecureTouchAnalyticsLogger\n*L\n1#1,150:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0006\u0010\n\u001a\u00020\u000bJ\u0006\u0010\u000c\u001a\u00020\u000bJ\u0010\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0006\u0010\u0010\u001a\u00020\u000bJ\u000e\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "countryCode",
        "",
        "sessionToken",
        "versionNumber",
        "",
        "(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;I)V",
        "logAccessibilityFinish",
        "",
        "logAccessibilityStart",
        "logEvent",
        "event",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;",
        "logFinish",
        "logKeyPressEvent",
        "keyPressEvent",
        "Lcom/squareup/securetouch/SecureTouchKeyPressEvent;",
        "logStart",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "Companion",
        "Factory",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Companion;

.field private static final NO_STRING:Ljava/lang/String; = "<unknown>"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final countryCode:Ljava/lang/String;

.field private final sessionToken:Ljava/lang/String;

.field private final versionNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->Companion:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->countryCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->sessionToken:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->versionNumber:I

    return-void
.end method

.method private final logEvent(Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;)V
    .locals 12

    .line 136
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 137
    new-instance v11, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;

    .line 138
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->getEventType()Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->toString()Ljava/lang/String;

    move-result-object v3

    .line 139
    iget-object v6, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->countryCode:Ljava/lang/String;

    .line 140
    iget-object v7, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->sessionToken:Ljava/lang/String;

    .line 141
    iget v8, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->versionNumber:I

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v9, 0xd

    const/4 v10, 0x0

    move-object v1, v11

    .line 137
    invoke-direct/range {v1 .. v10}, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 142
    invoke-virtual {v11, p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->copyWith(Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;)Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;

    move-result-object p1

    check-cast p1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 136
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method


# virtual methods
.method public final logAccessibilityFinish()V
    .locals 1

    .line 114
    sget-object v0, Lcom/squareup/securetouch/FinishAccessibilityFlow;->INSTANCE:Lcom/squareup/securetouch/FinishAccessibilityFlow;

    check-cast v0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;

    invoke-direct {p0, v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logEvent(Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;)V

    return-void
.end method

.method public final logAccessibilityStart()V
    .locals 1

    .line 111
    sget-object v0, Lcom/squareup/securetouch/StartAccessibilityFlow;->INSTANCE:Lcom/squareup/securetouch/StartAccessibilityFlow;

    check-cast v0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;

    invoke-direct {p0, v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logEvent(Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;)V

    return-void
.end method

.method public final logFinish()V
    .locals 1

    .line 117
    sget-object v0, Lcom/squareup/securetouch/FinishFlow;->INSTANCE:Lcom/squareup/securetouch/FinishFlow;

    check-cast v0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;

    invoke-direct {p0, v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logEvent(Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;)V

    return-void
.end method

.method public final logKeyPressEvent(Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)V
    .locals 2

    const-string v0, "keyPressEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchUserCancelled;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/securetouch/CancelButton;->INSTANCE:Lcom/squareup/securetouch/CancelButton;

    move-object v1, p1

    check-cast v1, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;

    goto :goto_0

    .line 123
    :cond_0
    sget-object v0, Lcom/squareup/securetouch/SecureTouchUserDone;->INSTANCE:Lcom/squareup/securetouch/SecureTouchUserDone;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/securetouch/DoneButton;->INSTANCE:Lcom/squareup/securetouch/DoneButton;

    move-object v1, p1

    check-cast v1, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;

    goto :goto_0

    .line 124
    :cond_1
    sget-object v0, Lcom/squareup/securetouch/KeypadClearPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadClearPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 125
    :cond_2
    sget-object v0, Lcom/squareup/securetouch/KeypadDeletePressed;->INSTANCE:Lcom/squareup/securetouch/KeypadDeletePressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/securetouch/BackButton;->INSTANCE:Lcom/squareup/securetouch/BackButton;

    move-object v1, p1

    check-cast v1, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;

    goto :goto_0

    .line 126
    :cond_3
    sget-object v0, Lcom/squareup/securetouch/KeypadAccessibilityPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadAccessibilityPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/securetouch/AccessibilityButton;->INSTANCE:Lcom/squareup/securetouch/AccessibilityButton;

    move-object v1, p1

    check-cast v1, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;

    goto :goto_0

    .line 127
    :cond_4
    sget-object v0, Lcom/squareup/securetouch/KeypadDigitPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadDigitPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_0

    .line 128
    :cond_5
    instance-of v0, p1, Lcom/squareup/securetouch/AccessibilityKeypadCenter;

    if-eqz v0, :cond_6

    goto :goto_0

    .line 129
    :cond_6
    instance-of p1, p1, Lcom/squareup/securetouch/KeypadInvalidAction;

    if-eqz p1, :cond_8

    :goto_0
    if-eqz v1, :cond_7

    .line 131
    invoke-direct {p0, v1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logEvent(Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;)V

    :cond_7
    return-void

    .line 129
    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final logStart(Lcom/squareup/securetouch/SecureTouchPinEntryState;)V
    .locals 2

    const-string v0, "pinEntryState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    new-instance v0, Lcom/squareup/securetouch/StartFlow;

    .line 102
    sget-object v1, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    .line 103
    :cond_0
    sget-object v1, Lcom/squareup/securetouch/PinRetry;->INSTANCE:Lcom/squareup/securetouch/PinRetry;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p1, 0x2

    goto :goto_0

    .line 104
    :cond_1
    sget-object v1, Lcom/squareup/securetouch/PinLastTry;->INSTANCE:Lcom/squareup/securetouch/PinLastTry;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x3

    .line 100
    :goto_0
    invoke-direct {v0, p1}, Lcom/squareup/securetouch/StartFlow;-><init>(I)V

    check-cast v0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;

    .line 99
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logEvent(Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;)V

    return-void

    .line 104
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
