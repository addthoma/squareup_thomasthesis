.class final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->handleCardreaderEvents(Lcom/squareup/workflow/RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "+",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "event",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;

    invoke-direct {v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchUserCancelled;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$Cancel;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$Cancel;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 181
    :cond_0
    instance-of v0, p1, Lcom/squareup/securetouch/AccessibilityKeypadCenter;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;

    check-cast p1, Lcom/squareup/securetouch/AccessibilityKeypadCenter;

    invoke-virtual {p1}, Lcom/squareup/securetouch/AccessibilityKeypadCenter;->getCenter()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/securetouch/AccessibilityKeypadCenter;->getKeypadType()Lcom/squareup/securetouch/AccessibilityKeypadType;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;Lcom/squareup/securetouch/AccessibilityKeypadType;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 182
    :cond_1
    instance-of v0, p1, Lcom/squareup/securetouch/KeypadDigitPressed;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadDigitSelected;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadDigitSelected;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 183
    :cond_2
    instance-of v0, p1, Lcom/squareup/securetouch/KeypadClearPressed;

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ClearDigits;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$ClearDigits;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 184
    :cond_3
    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchUserDone;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$Done;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$Done;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 185
    :cond_4
    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchFailure;

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$Fail;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$Fail;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 186
    :cond_5
    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchDisabled;

    if-eqz v0, :cond_6

    sget-object p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$SecureTouchDisabled;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$SecureTouchDisabled;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 187
    :cond_6
    instance-of p1, p1, Lcom/squareup/securetouch/KeypadInvalidAction;

    if-eqz p1, :cond_7

    sget-object p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$InvalidAction;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$InvalidAction;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 189
    :cond_7
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;->invoke(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
