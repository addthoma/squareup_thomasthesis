.class public final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"

# interfaces
.implements Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSecureTouchAccessibilityPinEntryWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSecureTouchAccessibilityPinEntryWorkflow.kt\ncom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,339:1\n197#1,4:340\n199#1:344\n201#1,2:350\n19#2:345\n19#2:381\n41#3:346\n56#3,2:347\n41#3:377\n56#3,2:378\n41#3:382\n56#3,2:383\n276#4:349\n276#4:380\n276#4:385\n149#5,5:352\n149#5,5:357\n149#5,5:362\n149#5,5:367\n149#5,5:372\n*E\n*S KotlinDebug\n*F\n+ 1 RealSecureTouchAccessibilityPinEntryWorkflow.kt\ncom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow\n*L\n92#1,4:340\n92#1:344\n92#1,2:350\n92#1:345\n199#1:381\n92#1:346\n92#1,2:347\n178#1:377\n178#1,2:378\n200#1:382\n200#1,2:383\n92#1:349\n178#1:380\n200#1:385\n98#1,5:352\n108#1,5:357\n127#1,5:362\n147#1,5:367\n167#1,5:372\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001,B!\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u0008\u0010\u0014\u001a\u00020\u0013H\u0002J \u0010\u0015\u001a\u00020\u00162\u0016\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0018j\u0002`\u0019H\u0002J\u001a\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u00032\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J \u0010\u001e\u001a\u00020\u00162\u0016\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0018j\u0002`\u0019H\u0002JR\u0010\u001f\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001b\u001a\u00020\u00032\u0006\u0010 \u001a\u00020\u00042\u0016\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0018j\u0002`\u0019H\u0016J\u0010\u0010!\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u0004H\u0016J^\u0010\"\u001a\u00020\u0016\"\n\u0008\u0000\u0010#\u0018\u0001*\u00020$*\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0018j\u0002`\u001923\u0008\u0004\u0010%\u001a-\u0012\u0013\u0012\u0011H#\u00a2\u0006\u000c\u0008\'\u0012\u0008\u0008(\u0012\u0004\u0008\u0008()\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050*j\u0002`+0&H\u0082\u0008R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "cardreader",
        "Lcom/squareup/securetouch/SecureTouchFeature;",
        "audioPlayer",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;",
        "mainThread",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;Lio/reactivex/Scheduler;)V",
        "destroyAudioPlayer",
        "Lio/reactivex/Completable;",
        "cardreaderEventToAudioCompletable",
        "handleCardreaderEvents",
        "",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/securetouch/accessibility/AccessiblePinEntryContext;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "observeCardreaderEventsAndMaybePlayAudio",
        "render",
        "state",
        "snapshotState",
        "onReaderOutput",
        "T",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "handler",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "event",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/securetouch/accessibility/AccessiblePinEntryAction;",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final audioPlayer:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

.field private final cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

.field private final destroyAudioPlayer:Lio/reactivex/Completable;

.field private final mainThread:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardreader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioPlayer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->audioPlayer:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->mainThread:Lio/reactivex/Scheduler;

    .line 75
    new-instance p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$destroyAudioPlayer$1;

    invoke-direct {p1, p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$destroyAudioPlayer$1;-><init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)V

    check-cast p1, Lio/reactivex/CompletableOnSubscribe;

    invoke-static {p1}, Lio/reactivex/Completable;->create(Lio/reactivex/CompletableOnSubscribe;)Lio/reactivex/Completable;

    move-result-object p1

    const-string p2, "Completable.create { it.\u2026audioPlayer.destroy() } }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->destroyAudioPlayer:Lio/reactivex/Completable;

    return-void
.end method

.method public static final synthetic access$getAudioPlayer$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->audioPlayer:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    return-object p0
.end method

.method public static final synthetic access$getCardreader$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    return-object p0
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lio/reactivex/Scheduler;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->mainThread:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method private final cardreaderEventToAudioCompletable()Lio/reactivex/Completable;
    .locals 2

    .line 213
    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;

    invoke-direct {v0, p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;-><init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)V

    check-cast v0, Lio/reactivex/CompletableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Completable;->create(Lio/reactivex/CompletableOnSubscribe;)Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "Completable.create { emi\u2026b.dispose()\n      }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final handleCardreaderEvents(Lcom/squareup/workflow/RenderContext;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)V"
        }
    .end annotation

    .line 178
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {v0}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object v0

    .line 377
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v1, "this.toFlowable(BUFFER)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_0

    .line 379
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 380
    const-class v1, Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 178
    sget-object v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$handleCardreaderEvents$1;

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p1

    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    .line 379
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final observeCardreaderEventsAndMaybePlayAudio(Lcom/squareup/workflow/RenderContext;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)V"
        }
    .end annotation

    .line 207
    invoke-direct {p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->cardreaderEventToAudioCompletable()Lio/reactivex/Completable;

    move-result-object v0

    .line 208
    invoke-static {v0}, Lcom/squareup/workflow/rx2/RxWorkersKt;->asWorker(Lio/reactivex/Completable;)Lcom/squareup/workflow/Worker;

    move-result-object v0

    const-string v1, "observeCardreaderEventsAndMaybePlayAudio"

    .line 207
    invoke-static {p1, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    return-void
.end method

.method private final synthetic onReaderOutput(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "+",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;>;)V"
        }
    .end annotation

    .line 198
    invoke-static {p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "T"

    const/4 v2, 0x4

    .line 381
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v2, "ofType(T::class.java)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 382
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v2, "this.toFlowable(BUFFER)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_0

    .line 384
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    const/4 v2, 0x6

    .line 385
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const/4 v1, 0x0

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 201
    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$onReaderOutput$1;

    invoke-direct {v0, p2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$onReaderOutput$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p1

    .line 197
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    .line 384
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;
    .locals 3

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    new-instance p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p2, v0, p1, v1, v2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;-><init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->initialState(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;

    check-cast p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->render(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "props"

    move-object/from16 v4, p1

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v3, v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->destroyAudioPlayer:Lio/reactivex/Completable;

    invoke-static {v3}, Lcom/squareup/workflow/rx2/RxWorkersKt;->asWorker(Lio/reactivex/Completable;)Lcom/squareup/workflow/Worker;

    move-result-object v3

    const-string v4, "destroyAudioPlayer"

    invoke-static {v2, v3, v4}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 91
    instance-of v3, v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;

    const-string v8, ""

    if-eqz v3, :cond_1

    .line 341
    invoke-static/range {p0 .. p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object v3

    .line 345
    const-class v4, Lcom/squareup/securetouch/SecureTouchEnabled;

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "ofType(T::class.java)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 346
    sget-object v4, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v3

    const-string/jumbo v4, "this.toFlowable(BUFFER)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lorg/reactivestreams/Publisher;

    if-eqz v3, :cond_0

    .line 348
    invoke-static {v3}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v3

    .line 349
    const-class v4, Lcom/squareup/securetouch/SecureTouchEnabled;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    new-instance v5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v5, v4, v3}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v5

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 350
    new-instance v5, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$$inlined$onReaderOutput$1;

    invoke-direct {v5}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$$inlined$onReaderOutput$1;-><init>()V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v2, p3

    .line 340
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 94
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 95
    new-instance v3, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    .line 96
    sget-object v4, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$2;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$2;

    move-object v10, v4

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 97
    new-instance v4, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForReader;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v16

    const/16 v17, 0x3

    const/16 v18, 0x0

    move-object v13, v4

    invoke-direct/range {v13 .. v18}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForReader;-><init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v13, v4

    check-cast v13, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    const/4 v14, 0x6

    move-object v9, v3

    .line 95
    invoke-direct/range {v9 .. v15}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 353
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 354
    const-class v4, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 355
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 353
    invoke-direct {v1, v4, v3, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 94
    invoke-virtual {v2, v1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 348
    :cond_0
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 101
    :cond_1
    instance-of v3, v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$DisplayedAccessibleKeypad;

    if-eqz v3, :cond_2

    .line 102
    sget-object v3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 103
    new-instance v4, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    .line 104
    new-instance v5, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$3;

    invoke-direct {v5, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$3;-><init>(Lcom/squareup/workflow/RenderContext;)V

    move-object v10, v5

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 107
    new-instance v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForKeypadLayout;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v16

    const/16 v17, 0x3

    const/16 v18, 0x0

    move-object v13, v2

    invoke-direct/range {v13 .. v18}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForKeypadLayout;-><init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v13, v2

    check-cast v13, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    const/4 v14, 0x6

    move-object v9, v4

    .line 103
    invoke-direct/range {v9 .. v15}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 358
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 359
    const-class v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 360
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 358
    invoke-direct {v1, v2, v4, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 102
    invoke-virtual {v3, v1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 112
    :cond_2
    instance-of v3, v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$UsingAccessibleKeypad;

    if-eqz v3, :cond_3

    .line 113
    invoke-direct {v0, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->handleCardreaderEvents(Lcom/squareup/workflow/RenderContext;)V

    .line 114
    invoke-direct {v0, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->observeCardreaderEventsAndMaybePlayAudio(Lcom/squareup/workflow/RenderContext;)V

    .line 116
    sget-object v3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 117
    new-instance v4, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    .line 118
    sget-object v5, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$4;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$4;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 119
    new-instance v6, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$5;

    invoke-direct {v6, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$5;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 122
    sget-object v2, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$6;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 123
    new-instance v7, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;

    .line 124
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getDigitsEntered()I

    move-result v10

    const/4 v11, 0x0

    .line 125
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v12

    const/4 v13, 0x2

    const/4 v14, 0x0

    move-object v9, v7

    .line 123
    invoke-direct/range {v9 .. v14}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;-><init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v7, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    .line 117
    invoke-direct {v4, v5, v6, v2, v7}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 363
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 364
    const-class v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 365
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 363
    invoke-direct {v1, v2, v4, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 116
    invoke-virtual {v3, v1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 131
    :cond_3
    instance-of v3, v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;

    if-eqz v3, :cond_4

    .line 132
    invoke-direct {v0, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->handleCardreaderEvents(Lcom/squareup/workflow/RenderContext;)V

    .line 133
    invoke-direct {v0, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->observeCardreaderEventsAndMaybePlayAudio(Lcom/squareup/workflow/RenderContext;)V

    .line 135
    sget-object v3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 136
    new-instance v4, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    .line 137
    sget-object v5, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 138
    sget-object v6, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$8;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$8;

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 139
    new-instance v7, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$9;

    invoke-direct {v7, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$9;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 142
    new-instance v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$DisplayingDigits;

    .line 143
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getDigitsEntered()I

    move-result v9

    .line 144
    new-instance v10, Lcom/squareup/securetouch/accessibility/DigitsOneThroughNine;

    move-object v11, v1

    check-cast v11, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;

    invoke-virtual {v11}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getCenter()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/squareup/securetouch/accessibility/DigitsOneThroughNine;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;)V

    check-cast v10, Lcom/squareup/securetouch/accessibility/InputDigits;

    .line 145
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    .line 142
    invoke-direct {v2, v9, v10, v1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$DisplayingDigits;-><init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;)V

    check-cast v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    .line 136
    invoke-direct {v4, v5, v6, v7, v2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 368
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 369
    const-class v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 370
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 368
    invoke-direct {v1, v2, v4, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 135
    invoke-virtual {v3, v1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    .line 151
    :cond_4
    instance-of v3, v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitZero;

    if-eqz v3, :cond_5

    .line 152
    invoke-direct {v0, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->handleCardreaderEvents(Lcom/squareup/workflow/RenderContext;)V

    .line 153
    invoke-direct {v0, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->observeCardreaderEventsAndMaybePlayAudio(Lcom/squareup/workflow/RenderContext;)V

    .line 155
    sget-object v3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 156
    new-instance v4, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    .line 157
    sget-object v5, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$10;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$10;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 158
    sget-object v6, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$11;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$11;

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 159
    new-instance v7, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$12;

    invoke-direct {v7, v2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$12;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 162
    new-instance v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$DisplayingDigits;

    .line 163
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getDigitsEntered()I

    move-result v9

    .line 164
    new-instance v10, Lcom/squareup/securetouch/accessibility/DigitZero;

    move-object v11, v1

    check-cast v11, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitZero;

    invoke-virtual {v11}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitZero;->getCenter()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/squareup/securetouch/accessibility/DigitZero;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;)V

    check-cast v10, Lcom/squareup/securetouch/accessibility/InputDigits;

    .line 165
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    .line 162
    invoke-direct {v2, v9, v10, v1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$DisplayingDigits;-><init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;)V

    check-cast v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    .line 156
    invoke-direct {v4, v5, v6, v7, v2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 373
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 374
    const-class v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 375
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 373
    invoke-direct {v1, v2, v4, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 155
    invoke-virtual {v3, v1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_5
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->snapshotState(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
