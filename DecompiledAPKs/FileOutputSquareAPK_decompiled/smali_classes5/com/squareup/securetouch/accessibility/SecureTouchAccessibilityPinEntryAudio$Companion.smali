.class public final Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$Companion;
.super Ljava/lang/Object;
.source "SecureTouchAccessibilityPinEntryAudio.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$Companion;",
        "",
        "()V",
        "secureTouchEventToAudio",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;",
        "event",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final secureTouchEventToAudio(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object v0, Lcom/squareup/securetouch/SecureTouchUserDone;->INSTANCE:Lcom/squareup/securetouch/SecureTouchUserDone;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 28
    :cond_0
    sget-object v0, Lcom/squareup/securetouch/KeypadClearPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadClearPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 29
    :cond_1
    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchUserCancelled$SecureTouchUserSwipeCancelled;

    if-eqz v0, :cond_2

    :goto_0
    sget-object p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$SwipeAudio;->INSTANCE:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$SwipeAudio;

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;

    goto :goto_1

    .line 30
    :cond_2
    sget-object v0, Lcom/squareup/securetouch/KeypadDigitPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadDigitPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$RecognizedEntryAudio;->INSTANCE:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$RecognizedEntryAudio;

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;

    goto :goto_1

    .line 31
    :cond_3
    instance-of v0, p1, Lcom/squareup/securetouch/AccessibilityKeypadCenter;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TouchDownAudio;->INSTANCE:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TouchDownAudio;

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;

    goto :goto_1

    .line 32
    :cond_4
    sget-object v0, Lcom/squareup/securetouch/KeypadInvalidAction$Error;->INSTANCE:Lcom/squareup/securetouch/KeypadInvalidAction$Error;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$ErrorAudio;->INSTANCE:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$ErrorAudio;

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;

    goto :goto_1

    .line 33
    :cond_5
    sget-object v0, Lcom/squareup/securetouch/KeypadInvalidAction$TooManyDigits;->INSTANCE:Lcom/squareup/securetouch/KeypadInvalidAction$TooManyDigits;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TooManyDigitsAudio;->INSTANCE:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TooManyDigitsAudio;

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;

    goto :goto_1

    .line 34
    :cond_6
    sget-object v0, Lcom/squareup/securetouch/KeypadInvalidAction$TooFewDigits;->INSTANCE:Lcom/squareup/securetouch/KeypadInvalidAction$TooFewDigits;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    sget-object p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TooFewDigitsAudio;->INSTANCE:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$TooFewDigitsAudio;

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;

    goto :goto_1

    .line 35
    :cond_7
    sget-object p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$NoAudioRequired;->INSTANCE:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$NoAudioRequired;

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;

    :goto_1
    return-object p1
.end method
