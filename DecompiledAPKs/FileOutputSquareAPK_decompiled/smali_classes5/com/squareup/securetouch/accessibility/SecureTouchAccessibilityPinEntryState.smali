.class public abstract Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;
.super Ljava/lang/Object;
.source "SecureTouchAccessibilityPinEntryState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;,
        Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$DisplayedAccessibleKeypad;,
        Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$UsingAccessibleKeypad;,
        Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;,
        Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitZero;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\r\u000e\u000f\u0010\u0011B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0005\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "",
        "()V",
        "digitsEntered",
        "",
        "getDigitsEntered",
        "()I",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "getPinEntryState",
        "()Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "toString",
        "",
        "DisplayedAccessibleKeypad",
        "EnablingKeypad",
        "ShowingDigitZero",
        "ShowingDigitsOneThroughNine",
        "UsingAccessibleKeypad",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$EnablingKeypad;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$DisplayedAccessibleKeypad;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$UsingAccessibleKeypad;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitZero;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDigitsEntered()I
.end method

.method public abstract getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
