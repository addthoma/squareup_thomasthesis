.class public final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$onReaderOutput$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->onReaderOutput(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TT;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "+",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSecureTouchAccessibilityPinEntryWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSecureTouchAccessibilityPinEntryWorkflow.kt\ncom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$onReaderOutput$1\n*L\n1#1,339:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u0004\"\n\u0008\u0000\u0010\u0005\u0018\u0001*\u00020\u00062\u0006\u0010\u0007\u001a\u0002H\u0005H\n\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "Lcom/squareup/securetouch/accessibility/AccessiblePinEntryAction;",
        "T",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "event",
        "invoke",
        "(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $handler:Lkotlin/jvm/functions/Function1;


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$onReaderOutput$1;->$handler:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$onReaderOutput$1;->$handler:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$onReaderOutput$1;->invoke(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
