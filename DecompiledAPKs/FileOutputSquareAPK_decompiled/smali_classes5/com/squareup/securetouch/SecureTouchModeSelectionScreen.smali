.class public final Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;
.super Ljava/lang/Object;
.source "SecureTouchModeSelectionScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$SecureTouchModeSelectionEvent;,
        Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001c\u001dB)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\u0015\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007H\u00c6\u0003J3\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0014\u0008\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00052\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "amount",
        "",
        "usingHighContrastMode",
        "",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$SecureTouchModeSelectionEvent;",
        "",
        "(Ljava/lang/CharSequence;ZLkotlin/jvm/functions/Function1;)V",
        "getAmount",
        "()Ljava/lang/CharSequence;",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "getUsingHighContrastMode",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "SecureTouchModeSelectionEvent",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final amount:Ljava/lang/CharSequence;

.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$SecureTouchModeSelectionEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final usingHighContrastMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->Companion:Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$Companion;

    .line 20
    const-class v0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;ZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$SecureTouchModeSelectionEvent;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->amount:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->usingHighContrastMode:Z

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;Ljava/lang/CharSequence;ZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->amount:Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->usingHighContrastMode:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->copy(Ljava/lang/CharSequence;ZLkotlin/jvm/functions/Function1;)Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->amount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->usingHighContrastMode:Z

    return v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$SecureTouchModeSelectionEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;ZLkotlin/jvm/functions/Function1;)Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$SecureTouchModeSelectionEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;"
        }
    .end annotation

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;-><init>(Ljava/lang/CharSequence;ZLkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->amount:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->amount:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->usingHighContrastMode:Z

    iget-boolean v1, p1, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->usingHighContrastMode:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Ljava/lang/CharSequence;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->amount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$SecureTouchModeSelectionEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getUsingHighContrastMode()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->usingHighContrastMode:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->amount:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->usingHighContrastMode:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SecureTouchModeSelectionScreen(amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->amount:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", usingHighContrastMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->usingHighContrastMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
