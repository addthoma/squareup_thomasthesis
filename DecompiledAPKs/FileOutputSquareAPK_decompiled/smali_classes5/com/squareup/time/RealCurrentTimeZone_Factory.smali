.class public final Lcom/squareup/time/RealCurrentTimeZone_Factory;
.super Ljava/lang/Object;
.source "RealCurrentTimeZone_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/time/RealCurrentTimeZone;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/TimeInfoChangedMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/TimeInfoChangedMonitor;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/time/RealCurrentTimeZone_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/time/RealCurrentTimeZone_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/TimeInfoChangedMonitor;",
            ">;)",
            "Lcom/squareup/time/RealCurrentTimeZone_Factory;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/time/RealCurrentTimeZone_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/time/RealCurrentTimeZone_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/time/TimeInfoChangedMonitor;)Lcom/squareup/time/RealCurrentTimeZone;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/time/RealCurrentTimeZone;

    invoke-direct {v0, p0}, Lcom/squareup/time/RealCurrentTimeZone;-><init>(Lcom/squareup/time/TimeInfoChangedMonitor;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/time/RealCurrentTimeZone;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/time/RealCurrentTimeZone_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/time/TimeInfoChangedMonitor;

    invoke-static {v0}, Lcom/squareup/time/RealCurrentTimeZone_Factory;->newInstance(Lcom/squareup/time/TimeInfoChangedMonitor;)Lcom/squareup/time/RealCurrentTimeZone;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/time/RealCurrentTimeZone_Factory;->get()Lcom/squareup/time/RealCurrentTimeZone;

    move-result-object v0

    return-object v0
.end method
