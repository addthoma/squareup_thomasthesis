.class final Lcom/squareup/textappearance/FontFamily$2$1$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "FontFamily.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/textappearance/FontFamily;-><init>(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/android/xml/StyledAttributesVisitor;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/android/xml/StyledAttributesVisitor;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $fontId:Lkotlin/jvm/internal/Ref$IntRef;

.field final synthetic $style:Lkotlin/jvm/internal/Ref$ObjectRef;

.field final synthetic $weight:Lkotlin/jvm/internal/Ref$IntRef;


# direct methods
.method constructor <init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$IntRef;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/textappearance/FontFamily$2$1$1$1;->$style:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p2, p0, Lcom/squareup/textappearance/FontFamily$2$1$1$1;->$weight:Lkotlin/jvm/internal/Ref$IntRef;

    iput-object p3, p0, Lcom/squareup/textappearance/FontFamily$2$1$1$1;->$fontId:Lkotlin/jvm/internal/Ref$IntRef;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/android/xml/StyledAttributesVisitor;

    invoke-virtual {p0, p1}, Lcom/squareup/textappearance/FontFamily$2$1$1$1;->invoke(Lcom/squareup/android/xml/StyledAttributesVisitor;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/android/xml/StyledAttributesVisitor;)V
    .locals 3

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x5c71855e

    if-eq v1, v2, :cond_2

    const v2, -0x2bc67c59

    if-eq v1, v2, :cond_1

    const v2, 0x300c4f

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "font"

    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 51
    iget-object v0, p0, Lcom/squareup/textappearance/FontFamily$2$1$1$1;->$fontId:Lkotlin/jvm/internal/Ref$IntRef;

    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getStyled()Landroid/content/res/TypedArray;

    move-result-object p1

    sget v1, Lcom/squareup/textappearance/R$styleable;->FontFamilyFont_font:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    iput p1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    goto :goto_0

    :cond_1
    const-string v1, "fontWeight"

    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 49
    iget-object v0, p0, Lcom/squareup/textappearance/FontFamily$2$1$1$1;->$weight:Lkotlin/jvm/internal/Ref$IntRef;

    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getStyled()Landroid/content/res/TypedArray;

    move-result-object p1

    sget v1, Lcom/squareup/textappearance/R$styleable;->FontFamilyFont_fontWeight:I

    iget-object v2, p0, Lcom/squareup/textappearance/FontFamily$2$1$1$1;->$weight:Lkotlin/jvm/internal/Ref$IntRef;

    iget v2, v2, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    iput p1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    goto :goto_0

    :cond_2
    const-string v1, "fontStyle"

    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 47
    iget-object v0, p0, Lcom/squareup/textappearance/FontFamily$2$1$1$1;->$style:Lkotlin/jvm/internal/Ref$ObjectRef;

    .line 45
    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getStyled()Landroid/content/res/TypedArray;

    move-result-object p1

    .line 46
    sget v1, Lcom/squareup/textappearance/R$styleable;->FontFamilyFont_fontStyle:I

    .line 47
    sget-object v2, Lcom/squareup/textappearance/Style;->NORMAL:Lcom/squareup/textappearance/Style;

    invoke-virtual {v2}, Lcom/squareup/textappearance/Style;->getAttributeValue()I

    move-result v2

    .line 45
    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    .line 47
    invoke-static {p1}, Lcom/squareup/textappearance/FontFamilyKt;->access$toStyle(I)Lcom/squareup/textappearance/Style;

    move-result-object p1

    iput-object p1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    :cond_3
    :goto_0
    return-void
.end method
