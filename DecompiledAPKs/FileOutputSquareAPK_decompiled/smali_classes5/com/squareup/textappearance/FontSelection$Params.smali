.class final Lcom/squareup/textappearance/FontSelection$Params;
.super Ljava/lang/Object;
.source "FontSelection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/textappearance/FontSelection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Params"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFontSelection.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FontSelection.kt\ncom/squareup/textappearance/FontSelection$Params\n+ 2 StyledAttributes.kt\ncom/squareup/ui/internal/styles/StyledAttributesKt\n*L\n1#1,97:1\n61#2,6:98\n*E\n*S KotlinDebug\n*F\n+ 1 FontSelection.kt\ncom/squareup/textappearance/FontSelection$Params\n*L\n47#1,6:98\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000bR\u001a\u0010\u000c\u001a\u00020\rX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\t\"\u0004\u0008\u0014\u0010\u000b\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/textappearance/FontSelection$Params;",
        "",
        "context",
        "Landroid/content/Context;",
        "fontSelectionId",
        "",
        "(Landroid/content/Context;I)V",
        "boldWeight",
        "getBoldWeight",
        "()I",
        "setBoldWeight",
        "(I)V",
        "fontFamily",
        "Lcom/squareup/textappearance/FontFamily;",
        "getFontFamily",
        "()Lcom/squareup/textappearance/FontFamily;",
        "setFontFamily",
        "(Lcom/squareup/textappearance/FontFamily;)V",
        "normalWeight",
        "getNormalWeight",
        "setNormalWeight",
        "textappearance_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private boldWeight:I

.field public fontFamily:Lcom/squareup/textappearance/FontFamily;

.field private normalWeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1, p2}, Lcom/squareup/ui/internal/styles/StyledAttributesKt;->styleAsTheme(Landroid/content/Context;I)Landroid/content/res/Resources$Theme;

    move-result-object p2

    sget-object v0, Lcom/squareup/textappearance/R$styleable;->FontSelection:[I

    const-string v1, "R.styleable.FontSelection"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p2, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string v0, "a"

    .line 100
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget v0, Lcom/squareup/textappearance/R$styleable;->FontSelection_android_fontFamily:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 49
    sget-object v2, Lcom/squareup/textappearance/FontFamily;->Companion:Lcom/squareup/textappearance/FontFamily$Companion;

    invoke-virtual {v2, p1, v0}, Lcom/squareup/textappearance/FontFamily$Companion;->loadFromResource(Landroid/content/Context;I)Lcom/squareup/textappearance/FontFamily;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/textappearance/FontSelection$Params;->setFontFamily(Lcom/squareup/textappearance/FontFamily;)V

    .line 50
    sget p1, Lcom/squareup/textappearance/R$styleable;->FontSelection_sqNormalWeight:I

    invoke-virtual {p2, p1, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/textappearance/FontSelection$Params;->setNormalWeight(I)V

    .line 51
    sget p1, Lcom/squareup/textappearance/R$styleable;->FontSelection_sqBoldWeight:I

    invoke-virtual {p2, p1, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/textappearance/FontSelection$Params;->setBoldWeight(I)V

    .line 52
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method


# virtual methods
.method public final getBoldWeight()I
    .locals 1

    .line 44
    iget v0, p0, Lcom/squareup/textappearance/FontSelection$Params;->boldWeight:I

    return v0
.end method

.method public final getFontFamily()Lcom/squareup/textappearance/FontFamily;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/textappearance/FontSelection$Params;->fontFamily:Lcom/squareup/textappearance/FontFamily;

    if-nez v0, :cond_0

    const-string v1, "fontFamily"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getNormalWeight()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/squareup/textappearance/FontSelection$Params;->normalWeight:I

    return v0
.end method

.method public final setBoldWeight(I)V
    .locals 0

    .line 44
    iput p1, p0, Lcom/squareup/textappearance/FontSelection$Params;->boldWeight:I

    return-void
.end method

.method public final setFontFamily(Lcom/squareup/textappearance/FontFamily;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lcom/squareup/textappearance/FontSelection$Params;->fontFamily:Lcom/squareup/textappearance/FontFamily;

    return-void
.end method

.method public final setNormalWeight(I)V
    .locals 0

    .line 43
    iput p1, p0, Lcom/squareup/textappearance/FontSelection$Params;->normalWeight:I

    return-void
.end method
