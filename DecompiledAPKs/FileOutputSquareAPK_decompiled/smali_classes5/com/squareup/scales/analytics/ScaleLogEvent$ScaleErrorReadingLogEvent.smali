.class public final Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleErrorReadingLogEvent;
.super Lcom/squareup/scales/analytics/ScaleLogEvent;
.source "ScalesHardwareAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/analytics/ScaleLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScaleErrorReadingLogEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleErrorReadingLogEvent;",
        "Lcom/squareup/scales/analytics/ScaleLogEvent;",
        "eventValue",
        "Lcom/squareup/scales/analytics/ScaleEventValue;",
        "event",
        "Lcom/squareup/scales/ScaleEvent$NoReadingEvent;",
        "(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleEvent$NoReadingEvent;)V",
        "description",
        "",
        "getDescription",
        "()Ljava/lang/String;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final description:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleEvent$NoReadingEvent;)V
    .locals 2

    const-string v0, "eventValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;->getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/scales/analytics/ScaleLogEvent;-><init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 106
    invoke-virtual {p2}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;->getStatus()Lcom/squareup/scales/Status;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/scales/Status;->name()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleErrorReadingLogEvent;->description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getDescription()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleErrorReadingLogEvent;->description:Ljava/lang/String;

    return-object v0
.end method
