.class public final Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;
.super Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;
.source "StarScaleDiscoverer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/RealStarScaleDiscoverer;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1",
        "Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;",
        "onDiscoverScale",
        "",
        "connectionInfo",
        "Lcom/starmicronics/starmgsio/ConnectionInfo;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/scales/RealStarScaleDiscoverer;


# direct methods
.method constructor <init>(Lcom/squareup/scales/RealStarScaleDiscoverer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 48
    iput-object p1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer;

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onDiscoverScale(Lcom/starmicronics/starmgsio/ConnectionInfo;)V
    .locals 2

    const-string v0, "connectionInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer;

    invoke-static {v0}, Lcom/squareup/scales/RealStarScaleDiscoverer;->access$getMainThread$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    new-instance v1, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;-><init>(Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;Lcom/starmicronics/starmgsio/ConnectionInfo;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
