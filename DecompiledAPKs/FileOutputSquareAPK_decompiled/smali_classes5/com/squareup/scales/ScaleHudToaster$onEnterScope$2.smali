.class final Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ScaleHudToaster.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/ScaleHudToaster;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/Set<",
        "+",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScaleHudToaster.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScaleHudToaster.kt\ncom/squareup/scales/ScaleHudToaster$onEnterScope$2\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "newScaleKeys",
        "",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/scales/ScaleHudToaster;


# direct methods
.method constructor <init>(Lcom/squareup/scales/ScaleHudToaster;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;->this$0:Lcom/squareup/scales/ScaleHudToaster;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;->invoke(Ljava/util/Set;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            ">;)V"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;->this$0:Lcom/squareup/scales/ScaleHudToaster;

    invoke-static {v0}, Lcom/squareup/scales/ScaleHudToaster;->access$getCurrentScaleKeys$p(Lcom/squareup/scales/ScaleHudToaster;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    const-string v1, "newScaleKeys"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->subtract(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;->this$0:Lcom/squareup/scales/ScaleHudToaster;

    invoke-static {v0}, Lcom/squareup/scales/ScaleHudToaster;->access$showScaleDisconnected(Lcom/squareup/scales/ScaleHudToaster;)V

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;->this$0:Lcom/squareup/scales/ScaleHudToaster;

    invoke-static {v0}, Lcom/squareup/scales/ScaleHudToaster;->access$getCurrentScaleKeys$p(Lcom/squareup/scales/ScaleHudToaster;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->subtract(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;->this$0:Lcom/squareup/scales/ScaleHudToaster;

    invoke-static {v0}, Lcom/squareup/scales/ScaleHudToaster;->access$showScaleConnected(Lcom/squareup/scales/ScaleHudToaster;)V

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;->this$0:Lcom/squareup/scales/ScaleHudToaster;

    invoke-static {v0}, Lcom/squareup/scales/ScaleHudToaster;->access$getCurrentScaleKeys$p(Lcom/squareup/scales/ScaleHudToaster;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 51
    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;->this$0:Lcom/squareup/scales/ScaleHudToaster;

    invoke-static {v0}, Lcom/squareup/scales/ScaleHudToaster;->access$getCurrentScaleKeys$p(Lcom/squareup/scales/ScaleHudToaster;)Ljava/util/Set;

    move-result-object v0

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method
