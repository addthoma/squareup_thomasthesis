.class public interface abstract Lcom/squareup/scales/ScaleTracker;
.super Ljava/lang/Object;
.source "ScaleTracker.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/ScaleTracker$HardwareScale;,
        Lcom/squareup/scales/ScaleTracker$ConnectionType;,
        Lcom/squareup/scales/ScaleTracker$Manufacturer;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001:\u0003\t\n\u000bR$\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/scales/ScaleTracker;",
        "",
        "connectedScales",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "Lcom/squareup/scales/Scale;",
        "getConnectedScales",
        "()Lio/reactivex/Observable;",
        "ConnectionType",
        "HardwareScale",
        "Manufacturer",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getConnectedScales()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Map<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            "Lcom/squareup/scales/Scale;",
            ">;>;"
        }
    .end annotation
.end method
