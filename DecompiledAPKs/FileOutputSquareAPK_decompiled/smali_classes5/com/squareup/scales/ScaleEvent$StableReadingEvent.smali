.class public final Lcom/squareup/scales/ScaleEvent$StableReadingEvent;
.super Lcom/squareup/scales/ScaleEvent;
.source "ScaleEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/ScaleEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StableReadingEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\'\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J3\u0010\u0017\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\tH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/scales/ScaleEvent$StableReadingEvent;",
        "Lcom/squareup/scales/ScaleEvent;",
        "hardwareScale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "unitOfMeasurement",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "weight",
        "",
        "numberOfDecimalPlaces",
        "",
        "(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;DI)V",
        "getHardwareScale",
        "()Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "getNumberOfDecimalPlaces",
        "()I",
        "getUnitOfMeasurement",
        "()Lcom/squareup/scales/UnitOfMeasurement;",
        "getWeight",
        "()D",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

.field private final numberOfDecimalPlaces:I

.field private final unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

.field private final weight:D


# direct methods
.method public constructor <init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;DI)V
    .locals 1

    const-string/jumbo v0, "unitOfMeasurement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, v0}, Lcom/squareup/scales/ScaleEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    iput-object p2, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

    iput-wide p3, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->weight:D

    iput p5, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->numberOfDecimalPlaces:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/scales/ScaleEvent$StableReadingEvent;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;DIILjava/lang/Object;)Lcom/squareup/scales/ScaleEvent$StableReadingEvent;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-wide p3, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->weight:D

    :cond_2
    move-wide v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget p5, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->numberOfDecimalPlaces:I

    :cond_3
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-wide p5, v0

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->copy(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;DI)Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/scales/ScaleTracker$HardwareScale;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    return-object v0
.end method

.method public final component2()Lcom/squareup/scales/UnitOfMeasurement;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

    return-object v0
.end method

.method public final component3()D
    .locals 2

    iget-wide v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->weight:D

    return-wide v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->numberOfDecimalPlaces:I

    return v0
.end method

.method public final copy(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;DI)Lcom/squareup/scales/ScaleEvent$StableReadingEvent;
    .locals 7

    const-string/jumbo v0, "unitOfMeasurement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;DI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    iget-object v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    iget-object v1, p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

    iget-object v1, p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->weight:D

    iget-wide v2, p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->weight:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->numberOfDecimalPlaces:I

    iget p1, p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->numberOfDecimalPlaces:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    return-object v0
.end method

.method public final getNumberOfDecimalPlaces()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->numberOfDecimalPlaces:I

    return v0
.end method

.method public final getUnitOfMeasurement()Lcom/squareup/scales/UnitOfMeasurement;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

    return-object v0
.end method

.method public final getWeight()D
    .locals 2

    .line 13
    iget-wide v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->weight:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->weight:D

    invoke-static {v1, v2}, L$r8$java8methods$utility$Double$hashCode$ID;->hashCode(D)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->numberOfDecimalPlaces:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StableReadingEvent(hardwareScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", unitOfMeasurement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->unitOfMeasurement:Lcom/squareup/scales/UnitOfMeasurement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", weight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->weight:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", numberOfDecimalPlaces="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->numberOfDecimalPlaces:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
