.class public final Lcom/squareup/scales/RealScalesActionBarConfig;
.super Ljava/lang/Object;
.source "ScalesActionBarConfig.kt"

# interfaces
.implements Lcom/squareup/scales/ScalesActionBarConfig;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/scales/RealScalesActionBarConfig;",
        "Lcom/squareup/scales/ScalesActionBarConfig;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/util/Device;)V",
        "showBackButton",
        "",
        "getShowBackButton",
        "()Z",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/RealScalesActionBarConfig;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method public getShowBackButton()Z
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/scales/RealScalesActionBarConfig;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
