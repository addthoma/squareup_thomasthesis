.class public final Lcom/squareup/scales/ScaleTracker$HardwareScale;
.super Ljava/lang/Object;
.source "ScaleTracker.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/ScaleTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HardwareScale"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0008H\u00c6\u0003J3\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "",
        "deviceName",
        "",
        "macAddress",
        "manufacturer",
        "Lcom/squareup/scales/ScaleTracker$Manufacturer;",
        "connectionType",
        "Lcom/squareup/scales/ScaleTracker$ConnectionType;",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;)V",
        "getConnectionType",
        "()Lcom/squareup/scales/ScaleTracker$ConnectionType;",
        "getDeviceName",
        "()Ljava/lang/String;",
        "getMacAddress",
        "getManufacturer",
        "()Lcom/squareup/scales/ScaleTracker$Manufacturer;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

.field private final deviceName:Ljava/lang/String;

.field private final macAddress:Ljava/lang/String;

.field private final manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;)V
    .locals 1

    const-string v0, "deviceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manufacturer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionType"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->deviceName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->macAddress:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    iput-object p4, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/scales/ScaleTracker$HardwareScale;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;ILjava/lang/Object;)Lcom/squareup/scales/ScaleTracker$HardwareScale;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->deviceName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->macAddress:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;)Lcom/squareup/scales/ScaleTracker$HardwareScale;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/scales/ScaleTracker$Manufacturer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    return-object v0
.end method

.method public final component4()Lcom/squareup/scales/ScaleTracker$ConnectionType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;)Lcom/squareup/scales/ScaleTracker$HardwareScale;
    .locals 1

    const-string v0, "deviceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manufacturer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionType"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/scales/ScaleTracker$HardwareScale;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->deviceName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/scales/ScaleTracker$HardwareScale;->deviceName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->macAddress:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/scales/ScaleTracker$HardwareScale;->macAddress:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    iget-object v1, p1, Lcom/squareup/scales/ScaleTracker$HardwareScale;->manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    iget-object p1, p1, Lcom/squareup/scales/ScaleTracker$HardwareScale;->connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConnectionType()Lcom/squareup/scales/ScaleTracker$ConnectionType;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    return-object v0
.end method

.method public final getDeviceName()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public final getMacAddress()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final getManufacturer()Lcom/squareup/scales/ScaleTracker$Manufacturer;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->deviceName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->macAddress:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HardwareScale(deviceName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", macAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->macAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", manufacturer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->manufacturer:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", connectionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/ScaleTracker$HardwareScale;->connectionType:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
