.class final Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;
.super Ljava/lang/Object;
.source "StarScaleDiscoverer.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;->onDiscoverScale(Lcom/starmicronics/starmgsio/ConnectionInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStarScaleDiscoverer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StarScaleDiscoverer.kt\ncom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1\n*L\n1#1,75:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $connectionInfo:Lcom/starmicronics/starmgsio/ConnectionInfo;

.field final synthetic this$0:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;


# direct methods
.method constructor <init>(Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;Lcom/starmicronics/starmgsio/ConnectionInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;

    iput-object p2, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->$connectionInfo:Lcom/starmicronics/starmgsio/ConnectionInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .line 51
    iget-object v0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->$connectionInfo:Lcom/starmicronics/starmgsio/ConnectionInfo;

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "connectionInfo.deviceName"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->$connectionInfo:Lcom/starmicronics/starmgsio/ConnectionInfo;

    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    check-cast v1, Ljava/lang/String;

    .line 53
    invoke-static {}, Lcom/squareup/scales/RealStarScaleDiscoverer;->access$Companion()Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;->getMANUFACTURER()Lcom/squareup/scales/ScaleTracker$Manufacturer;

    move-result-object v2

    .line 54
    iget-object v3, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->$connectionInfo:Lcom/starmicronics/starmgsio/ConnectionInfo;

    invoke-virtual {v3}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getInterfaceType()Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    move-result-object v3

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    sget-object v4, Lcom/squareup/scales/RealStarScaleDiscoverer$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    .line 57
    :goto_0
    sget-object v3, Lcom/squareup/scales/ScaleTracker$ConnectionType;->INVALID:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    goto :goto_1

    .line 56
    :cond_2
    sget-object v3, Lcom/squareup/scales/ScaleTracker$ConnectionType;->USB:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    goto :goto_1

    .line 55
    :cond_3
    sget-object v3, Lcom/squareup/scales/ScaleTracker$ConnectionType;->BLE:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    .line 50
    :goto_1
    new-instance v4, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/squareup/scales/ScaleTracker$HardwareScale;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;

    iget-object v0, v0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer;

    invoke-static {v0}, Lcom/squareup/scales/RealStarScaleDiscoverer;->access$getHardwareScalesList$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 62
    iget-object v0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;

    iget-object v0, v0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer;

    invoke-static {v0}, Lcom/squareup/scales/RealStarScaleDiscoverer;->access$getHardwareScalesList$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v0, Lcom/squareup/scales/StarScale;

    iget-object v1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;

    iget-object v1, v1, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealStarScaleDiscoverer;->access$getApplication$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/scales/StarScale;-><init>(Landroid/app/Application;)V

    .line 64
    iget-object v1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->$connectionInfo:Lcom/starmicronics/starmgsio/ConnectionInfo;

    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    const-string v2, "connectionInfo.identifier"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->$connectionInfo:Lcom/starmicronics/starmgsio/ConnectionInfo;

    invoke-virtual {v2}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getInterfaceType()Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    move-result-object v2

    const-string v3, "connectionInfo.interfaceType"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/scales/StarScale;->startConnection$impl_release(Ljava/lang/String;Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;)V

    .line 65
    iget-object v1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1$onDiscoverScale$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;

    iget-object v1, v1, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;->this$0:Lcom/squareup/scales/RealStarScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealStarScaleDiscoverer;->access$getRealScaleTracker$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Lcom/squareup/scales/RealScaleTracker;

    move-result-object v1

    check-cast v0, Lcom/squareup/scales/Scale;

    invoke-virtual {v1, v4, v0}, Lcom/squareup/scales/RealScaleTracker;->addScale(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Scale;)V

    :cond_4
    return-void
.end method
