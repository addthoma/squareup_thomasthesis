.class public abstract Lcom/squareup/scales/ScalesWorkflowModule;
.super Ljava/lang/Object;
.source "ScalesWorkflowModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0005\u001a\u00020\tH!J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH!J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H!J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0005\u001a\u00020\u0014H!\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/scales/ScalesWorkflowModule;",
        "",
        "()V",
        "bindRealNoConnectedScalesWorkflow",
        "Lcom/squareup/scales/NoConnectedScalesWorkflow;",
        "realScalesWorkflow",
        "Lcom/squareup/scales/RealNoConnectedScalesWorkflow;",
        "bindRealScalesActionBarConfig",
        "Lcom/squareup/scales/ScalesActionBarConfig;",
        "Lcom/squareup/scales/RealScalesActionBarConfig;",
        "bindRealScalesWorkflow",
        "Lcom/squareup/scales/ScalesWorkflow;",
        "realLimitedScalesWorkflow",
        "Lcom/squareup/scales/RealLimitedScalesWorkflow;",
        "bindRealScalesWorkflowViewFactory",
        "Lcom/squareup/scales/ScalesWorkflowViewFactory;",
        "realLimitedScalesWorkflowViewFactory",
        "Lcom/squareup/scales/RealLimitedScalesWorkflowViewFactory;",
        "bindRealShowingConnectedScalesWorkflow",
        "Lcom/squareup/scales/ShowingConnectedScalesWorkflow;",
        "Lcom/squareup/scales/RealShowingConnectedScalesWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindRealNoConnectedScalesWorkflow(Lcom/squareup/scales/RealNoConnectedScalesWorkflow;)Lcom/squareup/scales/NoConnectedScalesWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRealScalesActionBarConfig(Lcom/squareup/scales/RealScalesActionBarConfig;)Lcom/squareup/scales/ScalesActionBarConfig;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRealScalesWorkflow(Lcom/squareup/scales/RealLimitedScalesWorkflow;)Lcom/squareup/scales/ScalesWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRealScalesWorkflowViewFactory(Lcom/squareup/scales/RealLimitedScalesWorkflowViewFactory;)Lcom/squareup/scales/ScalesWorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRealShowingConnectedScalesWorkflow(Lcom/squareup/scales/RealShowingConnectedScalesWorkflow;)Lcom/squareup/scales/ShowingConnectedScalesWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
