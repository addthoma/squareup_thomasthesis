.class final Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;
.super Ljava/lang/Object;
.source "RealDuplicateSkuResultController.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sku/RealDuplicateSkuResultController;->variationsWithDuplicateSku()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        "it",
        "Lcom/squareup/sku/RealDuplicateSkuResultController$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;

    invoke-direct {v0}, Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;-><init>()V

    sput-object v0, Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;->INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/sku/RealDuplicateSkuResultController$State;

    invoke-virtual {p0, p1}, Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;->apply(Lcom/squareup/sku/RealDuplicateSkuResultController$State;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/sku/RealDuplicateSkuResultController$State;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/sku/RealDuplicateSkuResultController$State;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    instance-of v0, p1, Lcom/squareup/sku/RealDuplicateSkuResultController$State$Started;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/sku/RealDuplicateSkuResultController$State$Started;

    invoke-virtual {p1}, Lcom/squareup/sku/RealDuplicateSkuResultController$State$Started;->getVariationsWithDuplicateSku()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 94
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/sku/RealDuplicateSkuResultController$State;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
