.class public interface abstract Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;
.super Ljava/lang/Object;
.source "RuleSetLoader.java"


# virtual methods
.method public abstract load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/rules/RuleSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Ljava/util/TimeZone;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;"
        }
    .end annotation
.end method

.method public abstract load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;)Lcom/squareup/shared/pricing/engine/rules/RuleSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Ljava/util/TimeZone;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;"
        }
    .end annotation
.end method

.method public abstract load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;Z)Lcom/squareup/shared/pricing/engine/rules/RuleSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Ljava/util/TimeZone;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;"
        }
    .end annotation
.end method
