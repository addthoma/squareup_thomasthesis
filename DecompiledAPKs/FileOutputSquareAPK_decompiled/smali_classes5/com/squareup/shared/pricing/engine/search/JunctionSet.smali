.class public abstract Lcom/squareup/shared/pricing/engine/search/JunctionSet;
.super Ljava/lang/Object;
.source "JunctionSet.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/search/Provider;


# instance fields
.field protected final children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Provider;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsID:Ljava/lang/String;

.field protected final parents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Provider;",
            ">;"
        }
    .end annotation
.end field

.field protected final rangeMax:Ljava/math/BigDecimal;

.field protected final rangeMin:Ljava/math/BigDecimal;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->children:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->parents:Ljava/util/List;

    .line 37
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->cogsID:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->rangeMin:Ljava/math/BigDecimal;

    .line 39
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->rangeMax:Ljava/math/BigDecimal;

    return-void
.end method

.method protected static populateGroup(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/math/BigDecimal;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/math/BigDecimal;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 63
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 64
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 65
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->add(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    .line 66
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getCaps()Ljava/util/Map;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->capacityExceeded(Ljava/util/Map;Ljava/util/Map;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->canSplit()Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 70
    :cond_0
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getQuantity()Ljava/math/BigDecimal;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz p3, :cond_1

    .line 74
    invoke-virtual {v0, p3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p2

    if-ltz p2, :cond_1

    move-object p2, v2

    goto :goto_1

    :cond_1
    move-object p2, v2

    goto :goto_0

    :cond_2
    :goto_1
    return-object p2
.end method

.method private static pullFromGroup(Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;Ljava/util/List;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;",
            "Ljava/math/BigDecimal;",
            ")",
            "Ljava/math/BigDecimal;"
        }
    .end annotation

    .line 239
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 242
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p2, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    .line 245
    :cond_1
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 249
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getFractional()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    .line 250
    sget-object v3, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v1, v2, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 252
    :cond_2
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    .line 255
    :cond_3
    invoke-virtual {p0, v0}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->canJoin(Lcom/squareup/shared/pricing/engine/search/Candidate;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 256
    invoke-interface {v0, v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->take(Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/search/Candidate;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->join(Lcom/squareup/shared/pricing/engine/search/Candidate;)V

    .line 257
    invoke-virtual {p2, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p2

    goto :goto_0

    :cond_4
    :goto_1
    return-object p2
.end method


# virtual methods
.method public addChild(Lcom/squareup/shared/pricing/engine/search/Provider;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-interface {p1, p0}, Lcom/squareup/shared/pricing/engine/search/Provider;->addParent(Lcom/squareup/shared/pricing/engine/search/Provider;)V

    return-void
.end method

.method public addParent(Lcom/squareup/shared/pricing/engine/search/Provider;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->parents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearParents()V
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->parents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public exhaust(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/math/BigDecimal;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;"
        }
    .end annotation

    move-object v7, p0

    move-object/from16 v8, p4

    .line 169
    iget-object v0, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    .line 173
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 175
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 176
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    move-object/from16 v10, p3

    move-object v11, v0

    move-object v12, v1

    .line 183
    :cond_1
    invoke-static {v10, v11}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->add(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->getExhaustGroups(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    .line 186
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 187
    sget-object v4, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 188
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 189
    invoke-interface {v5}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    move-object v1, v4

    goto :goto_0

    .line 191
    :cond_3
    invoke-virtual {v1, v4}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    goto :goto_0

    .line 196
    :cond_4
    iget-object v2, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->rangeMax:Ljava/math/BigDecimal;

    if-eqz v2, :cond_5

    .line 197
    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    goto :goto_2

    :cond_5
    move-object v2, v1

    :goto_2
    if-eqz v1, :cond_8

    .line 202
    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    if-lez v3, :cond_8

    iget-object v3, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->rangeMin:Ljava/math/BigDecimal;

    if-eqz v3, :cond_6

    .line 203
    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-gez v1, :cond_6

    goto :goto_4

    .line 207
    :cond_6
    new-instance v1, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;

    iget-object v3, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->cogsID:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v3, v4}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 209
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 212
    invoke-static {v1, v3, v2}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->pullFromGroup(Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;Ljava/util/List;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    goto :goto_3

    .line 215
    :cond_7
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v12, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v12

    .line 217
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->getApplications()Ljava/util/Map;

    move-result-object v0

    invoke-static {v11, v0}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->add(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v11

    if-eqz v8, :cond_1

    .line 218
    invoke-virtual {v8, v12}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gtz v0, :cond_1

    :cond_8
    :goto_4
    return-object v9
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Provider;",
            ">;"
        }
    .end annotation

    .line 225
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->children:Ljava/util/List;

    return-object v0
.end method

.method public abstract getExhaustGroups(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;>;"
        }
    .end annotation
.end method

.method public getParents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Provider;",
            ">;"
        }
    .end annotation

    .line 229
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->parents:Ljava/util/List;

    return-object v0
.end method

.method public abstract getSatisfyGroups(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;>;"
        }
    .end annotation
.end method

.method public satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;"
        }
    .end annotation

    move-object v7, p0

    .line 108
    iget-object v0, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 109
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 112
    :cond_0
    iget-object v0, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->rangeMin:Ljava/math/BigDecimal;

    if-eqz v0, :cond_4

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    .line 116
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 117
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object v9, v0

    .line 122
    :goto_0
    new-instance v10, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;

    iget-object v0, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->cogsID:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v10, v0, v1}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;-><init>(Ljava/lang/String;Ljava/util/List;)V

    move-object v11, p3

    .line 125
    invoke-static {p3, v9}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->add(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->getSatisfyGroups(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v0

    .line 127
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 128
    iget-object v2, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->rangeMin:Ljava/math/BigDecimal;

    .line 129
    invoke-static {v10, v1, v2}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->pullFromGroup(Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;Ljava/util/List;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 130
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-lez v1, :cond_2

    return-object v8

    .line 137
    :cond_3
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-virtual {v10}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->getApplications()Ljava/util/Map;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->add(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    goto :goto_0

    .line 113
    :cond_4
    :goto_1
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;

    iget-object v1, v7, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->cogsID:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
