.class public final enum Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;
.super Ljava/lang/Enum;
.source "ItemizationDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BackingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

.field public static final enum CUSTOM_AMOUNT:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

.field public static final enum CUSTOM_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

.field public static final enum ITEMS_SERVICE_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 244
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    const/4 v1, 0x0

    const-string v2, "CUSTOM_AMOUNT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    .line 245
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    const/4 v2, 0x1

    const-string v3, "ITEMS_SERVICE_ITEM_VARIATION"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->ITEMS_SERVICE_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    .line 246
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    const/4 v3, 0x2

    const-string v4, "CUSTOM_ITEM_VARIATION"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    .line 243
    sget-object v4, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->ITEMS_SERVICE_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->$VALUES:[Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 243
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;
    .locals 1

    .line 243
    const-class v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;
    .locals 1

    .line 243
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->$VALUES:[Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    invoke-virtual {v0}, [Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-object v0
.end method
