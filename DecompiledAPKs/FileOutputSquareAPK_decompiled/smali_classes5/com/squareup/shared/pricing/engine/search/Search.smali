.class public Lcom/squareup/shared/pricing/engine/search/Search;
.super Ljava/lang/Object;
.source "Search.java"


# instance fields
.field private final discounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            ">;"
        }
    .end annotation
.end field

.field private final junctions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/JunctionSet;",
            ">;"
        }
    .end annotation
.end field

.field private final metrics:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

.field private final periods:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;"
        }
    .end annotation
.end field

.field private final productSets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;"
        }
    .end annotation
.end field

.field private final rollups:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/Rollup;",
            ">;"
        }
    .end annotation
.end field

.field private final ruleNodes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/RuleNode;",
            ">;"
        }
    .end annotation
.end field

.field private final sources:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Lcom/squareup/shared/pricing/engine/search/Source;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/RuleNode;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Lcom/squareup/shared/pricing/engine/search/Source;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/JunctionSet;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/Rollup;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p6, p0, Lcom/squareup/shared/pricing/engine/search/Search;->productSets:Ljava/util/Map;

    .line 55
    iput-object p7, p0, Lcom/squareup/shared/pricing/engine/search/Search;->periods:Ljava/util/Map;

    .line 56
    iput-object p8, p0, Lcom/squareup/shared/pricing/engine/search/Search;->discounts:Ljava/util/Map;

    .line 57
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Search;->metrics:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 58
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/Search;->ruleNodes:Ljava/util/Map;

    .line 59
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/search/Search;->sources:Ljava/util/Map;

    .line 60
    iput-object p4, p0, Lcom/squareup/shared/pricing/engine/search/Search;->junctions:Ljava/util/Map;

    .line 61
    iput-object p5, p0, Lcom/squareup/shared/pricing/engine/search/Search;->rollups:Ljava/util/Map;

    return-void
.end method

.method private addRuleNodeToPartition(Ljava/util/Map;JLcom/squareup/shared/pricing/engine/search/LineItemRuleNode;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;",
            ">;>;J",
            "Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;",
            ")V"
        }
    .end annotation

    .line 395
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private bestLineItemApplications()Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 247
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 248
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 253
    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/shared/pricing/engine/search/Search;->getPartitions(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v3

    .line 254
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 257
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 258
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 259
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    const-wide/16 v9, 0x0

    .line 265
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 266
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/shared/pricing/engine/search/RuleNode;

    .line 267
    invoke-interface {v12}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v13

    invoke-interface {v13}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v13

    .line 268
    invoke-interface {v2, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    goto :goto_2

    .line 272
    :cond_1
    invoke-interface {v12}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->bestApplication()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    move-result-object v14

    if-nez v14, :cond_2

    .line 274
    invoke-interface {v2, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 278
    :cond_2
    invoke-interface {v11, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    invoke-interface {v12}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v12

    invoke-interface {v12}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getStackable()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    move-result-object v12

    sget-object v13, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->STACKABLE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    if-ne v12, v13, :cond_0

    .line 284
    invoke-virtual {v14}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v12

    add-long/2addr v9, v12

    goto :goto_2

    .line 299
    :cond_3
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    .line 298
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v12, v6

    move-object v13, v12

    move-object v14, v13

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Map$Entry;

    .line 300
    invoke-interface {v15}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 302
    sget-object v16, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 303
    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v18, v2

    move-object/from16 v2, v17

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    .line 306
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v17

    .line 305
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move-object/from16 v19, v3

    move-object/from16 v3, v16

    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_5

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/Map$Entry;

    .line 307
    invoke-interface/range {v16 .. v16}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v21, v5

    move-object/from16 v5, v20

    check-cast v5, Ljava/lang/String;

    .line 308
    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    goto :goto_5

    .line 313
    :cond_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    .line 315
    invoke-virtual {v2, v5}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getCost(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)Ljava/math/BigDecimal;

    move-result-object v5

    .line 316
    invoke-virtual {v3, v5}, Ljava/math/BigDecimal;->max(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    :goto_5
    move-object/from16 v5, v21

    goto :goto_4

    :cond_5
    move-object/from16 v21, v5

    .line 321
    invoke-virtual {v2}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getUnitValue()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v5

    .line 322
    invoke-virtual {v5, v3}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 326
    iget-object v5, v0, Lcom/squareup/shared/pricing/engine/search/Search;->ruleNodes:Ljava/util/Map;

    invoke-interface {v5, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/pricing/engine/search/RuleNode;

    .line 327
    sget-object v15, Lcom/squareup/shared/pricing/engine/search/Search$1;->$SwitchMap$com$squareup$shared$pricing$engine$catalog$models$PricingRuleFacade$Stackable:[I

    invoke-interface {v5}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v5

    invoke-interface {v5}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getStackable()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->ordinal()I

    move-result v5

    aget v5, v15, v5

    const/4 v15, 0x1

    if-eq v5, v15, :cond_8

    const/4 v15, 0x2

    if-eq v5, v15, :cond_6

    goto :goto_6

    :cond_6
    if-eqz v13, :cond_7

    if-eqz v3, :cond_a

    .line 340
    invoke-direct {v0, v2, v3, v12, v13}, Lcom/squareup/shared/pricing/engine/search/Search;->compareApplicationsByScore(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;Ljava/math/BigDecimal;)I

    move-result v5

    if-lez v5, :cond_a

    :cond_7
    move-object v12, v2

    move-object v13, v3

    goto :goto_6

    :cond_8
    if-eqz v14, :cond_9

    if-eqz v3, :cond_a

    .line 331
    invoke-direct {v0, v2, v3, v6, v14}, Lcom/squareup/shared/pricing/engine/search/Search;->compareApplicationsByScore(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;Ljava/math/BigDecimal;)I

    move-result v5

    if-lez v5, :cond_a

    :cond_9
    move-object v6, v2

    move-object v14, v3

    :cond_a
    :goto_6
    move-object/from16 v2, v18

    move-object/from16 v3, v19

    move-object/from16 v5, v21

    goto/16 :goto_3

    :cond_b
    move-object/from16 v18, v2

    move-object/from16 v19, v3

    if-eqz v6, :cond_c

    .line 356
    invoke-virtual {v6}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v2

    cmp-long v5, v2, v9

    if-lez v5, :cond_c

    .line 357
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_c
    if-eqz v12, :cond_d

    .line 359
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    :goto_7
    move-object/from16 v2, v18

    move-object/from16 v3, v19

    goto/16 :goto_1

    :cond_e
    move-object/from16 v18, v2

    .line 365
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    if-eqz v6, :cond_10

    .line 366
    sget-object v4, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->TOTAL_VALUE:Ljava/util/Comparator;

    invoke-interface {v4, v3, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_f

    :cond_10
    move-object v6, v3

    goto :goto_8

    :cond_11
    if-eqz v6, :cond_12

    .line 372
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    invoke-direct {v0, v6}, Lcom/squareup/shared/pricing/engine/search/Search;->commitRuleApplication(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)V

    move-object/from16 v2, v18

    goto/16 :goto_0

    :cond_12
    return-object v1
.end method

.method private bestWholePurchaseApplication()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
    .locals 8

    .line 227
    sget-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    invoke-virtual {p0, v0}, Lcom/squareup/shared/pricing/engine/search/Search;->getRuleNodesByDiscountTargetScope(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;)Ljava/util/List;

    move-result-object v0

    .line 231
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/RuleNode;

    .line 232
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->bestApplication()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    .line 234
    invoke-virtual {v2}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v3

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-lez v7, :cond_0

    :cond_1
    move-object v1, v2

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private commitRuleApplication(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)V
    .locals 5

    .line 88
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Search;->ruleNodes:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/RuleNode;

    invoke-interface {v0, p1}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->commitMatch(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)V

    .line 89
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getMatched()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 90
    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/Search;->sources:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Source;

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    invoke-virtual {v2, v3, v1}, Lcom/squareup/shared/pricing/engine/search/Source;->commitMatch(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Search;->metrics:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/Search;->discounts:Ljava/util/Map;

    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/Search;->productSets:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/shared/pricing/engine/search/Search;->periods:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->recordRuleApplied(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 93
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Search;->metrics:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->recordDiscountImproved(J)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    return-void
.end method

.method private compareApplicationsByScore(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;Ljava/math/BigDecimal;)I
    .locals 0

    .line 411
    invoke-virtual {p2, p4}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p2

    if-nez p2, :cond_0

    .line 414
    sget-object p2, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->TIEBREAKERS:Ljava/util/Comparator;

    invoke-interface {p2, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p1

    return p1

    :cond_0
    return p2
.end method

.method private convert(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;"
        }
    .end annotation

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    .line 71
    new-instance v2, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;

    invoke-direct {v2}, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;-><init>()V

    .line 72
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->setRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;

    move-result-object v2

    .line 73
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getApplied()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/models/ClientServerIds;

    .line 74
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getApplied()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/math/BigDecimal;

    invoke-virtual {v5}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object v5

    .line 75
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getOffsets()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/math/BigDecimal;

    invoke-virtual {v6}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object v6

    .line 76
    invoke-virtual {v2, v4, v6, v5}, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->addApplication(Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;

    goto :goto_1

    .line 78
    :cond_0
    invoke-virtual {v2}, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->build()Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public bestApplications()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;"
        }
    .end annotation

    .line 101
    invoke-direct {p0}, Lcom/squareup/shared/pricing/engine/search/Search;->bestWholePurchaseApplication()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    move-result-object v0

    .line 102
    invoke-direct {p0}, Lcom/squareup/shared/pricing/engine/search/Search;->bestLineItemApplications()Ljava/util/List;

    move-result-object v1

    if-nez v0, :cond_0

    .line 105
    invoke-direct {p0, v1}, Lcom/squareup/shared/pricing/engine/search/Search;->convert(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    const-wide/16 v2, 0x0

    .line 108
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    .line 109
    invoke-virtual {v5}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v5

    add-long/2addr v2, v5

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-lez v6, :cond_2

    .line 113
    invoke-direct {p0, v1}, Lcom/squareup/shared/pricing/engine/search/Search;->convert(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 115
    :cond_2
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/shared/pricing/engine/search/Search;->convert(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public breakCycles()V
    .locals 2

    .line 211
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Search;->junctions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    .line 212
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->clearParents()V

    goto :goto_0

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Search;->rollups:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Rollup;

    .line 215
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/Rollup;->clearParents()V

    goto :goto_1

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Search;->sources:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Source;

    .line 218
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/Source;->clearParents()V

    goto :goto_2

    :cond_2
    return-void
.end method

.method public getPartitions(Ljava/util/Set;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;",
            ">;>;"
        }
    .end annotation

    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 131
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 133
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 134
    sget-object v3, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->LINE_ITEM:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    invoke-virtual {p0, v3}, Lcom/squareup/shared/pricing/engine/search/Search;->getRuleNodesByDiscountTargetScope(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/search/RuleNode;

    .line 136
    invoke-interface {v4}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v5

    invoke-interface {v5}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 137
    check-cast v4, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    :cond_1
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 143
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/search/RuleNode;

    .line 144
    iget-object v5, p0, Lcom/squareup/shared/pricing/engine/search/Search;->junctions:Ljava/util/Map;

    invoke-interface {v4}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v4

    invoke-interface {v4}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    invoke-interface {p1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 147
    :cond_2
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 148
    iget-object v4, p0, Lcom/squareup/shared/pricing/engine/search/Search;->junctions:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    .line 151
    invoke-virtual {v5}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->getChildren()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 155
    invoke-virtual {v5}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->getParents()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {p1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 156
    :cond_4
    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    const-wide/16 v4, 0x0

    .line 162
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;

    .line 163
    iget-object v6, p0, Lcom/squareup/shared/pricing/engine/search/Search;->junctions:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v7

    invoke-interface {v7}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    .line 165
    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 168
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {p0, v1, v6, v7, v2}, Lcom/squareup/shared/pricing/engine/search/Search;->addRuleNodeToPartition(Ljava/util/Map;JLcom/squareup/shared/pricing/engine/search/LineItemRuleNode;)V

    goto :goto_3

    .line 173
    :cond_6
    invoke-direct {p0, v1, v4, v5, v2}, Lcom/squareup/shared/pricing/engine/search/Search;->addRuleNodeToPartition(Ljava/util/Map;JLcom/squareup/shared/pricing/engine/search/LineItemRuleNode;)V

    .line 175
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 176
    new-instance v7, Ljava/util/LinkedList;

    iget-object v2, v2, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v2, v2, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->children:Ljava/util/List;

    invoke-direct {v7, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 178
    :cond_7
    :goto_4
    invoke-interface {v7}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 179
    invoke-interface {v7}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Provider;

    .line 180
    invoke-interface {v6, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    goto :goto_4

    .line 188
    :cond_8
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v0, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 192
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Provider;->getChildren()Ljava/util/List;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 193
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Provider;->getParents()Ljava/util/List;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 194
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/shared/pricing/engine/search/Provider;

    .line 195
    invoke-interface {v6, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 196
    invoke-interface {v7, v8}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_a
    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    goto/16 :goto_3

    :cond_b
    return-object v1
.end method

.method protected getRuleNodesByDiscountTargetScope(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/RuleNode;",
            ">;"
        }
    .end annotation

    .line 382
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 383
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/Search;->ruleNodes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/RuleNode;

    .line 384
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/RuleNode;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountTargetScope()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 385
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method
