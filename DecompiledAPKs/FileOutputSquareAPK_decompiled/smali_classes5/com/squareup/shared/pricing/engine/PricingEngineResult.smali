.class public Lcom/squareup/shared/pricing/engine/PricingEngineResult;
.super Ljava/lang/Object;
.source "PricingEngineResult.java"


# instance fields
.field private final applications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;"
        }
    .end annotation
.end field

.field private final wholePurchaseApplications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->wholePurchaseApplications:Ljava/util/List;

    .line 20
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->applications:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getApplications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->applications:Ljava/util/List;

    return-object v0
.end method

.method public getBlocks()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->applications:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/shared/pricing/engine/rules/Collator;->collate(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getWholePurchaseApplications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->wholePurchaseApplications:Ljava/util/List;

    return-object v0
.end method
