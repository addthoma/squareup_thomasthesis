.class public Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;
.super Ljava/lang/Object;
.source "ProductSetWrapper.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;


# instance fields
.field private catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogProductSet;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    return-void
.end method

.method private transformObjectIds(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;",
            ">;"
        }
    .end annotation

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 84
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/sync/ObjectId;

    .line 85
    new-instance v2, Lcom/squareup/shared/pricing/engine/catalog/client/ObjectIdWrapper;

    invoke-direct {v2, v1}, Lcom/squareup/shared/pricing/engine/catalog/client/ObjectIdWrapper;-><init>(Lcom/squareup/api/sync/ObjectId;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public getEffectiveMax()Ljava/math/BigDecimal;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getEffectiveMax()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getEffectiveMin()Ljava/math/BigDecimal;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getEffectiveMin()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getFlagAllProducts()Z
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getAllProducts()Z

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProducts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProducts()Ljava/util/List;

    move-result-object v0

    .line 37
    invoke-direct {p0, v0}, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->transformObjectIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getProductsAll()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAll()Ljava/util/List;

    move-result-object v0

    .line 42
    invoke-direct {p0, v0}, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->transformObjectIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getProductsAny()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAny()Ljava/util/List;

    move-result-object v0

    .line 47
    invoke-direct {p0, v0}, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->transformObjectIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getQuantityExact()Ljava/math/BigDecimal;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityExact()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getQuantityMin()Ljava/math/BigDecimal;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityMin()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public hasProductsAll()Z
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->hasProductsAll()Z

    move-result v0

    return v0
.end method

.method public hasProductsAny()Z
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->hasProductsAny()Z

    move-result v0

    return v0
.end method

.method public isExact()Z
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isExact()Z

    move-result v0

    return v0
.end method

.method public isGreedy()Z
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isGreedy()Z

    move-result v0

    return v0
.end method

.method public isMin()Z
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;->catalogProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isMin()Z

    move-result v0

    return v0
.end method
