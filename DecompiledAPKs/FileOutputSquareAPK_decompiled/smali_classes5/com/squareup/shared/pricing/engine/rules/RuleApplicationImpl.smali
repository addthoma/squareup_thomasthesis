.class public Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;
.super Ljava/lang/Object;
.source "RuleApplicationImpl.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/rules/RuleApplication;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;
    }
.end annotation


# instance fields
.field private final applications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;",
            ">;"
        }
    .end annotation
.end field

.field private final rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 27
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;->applications:Ljava/util/List;

    .line 28
    iget-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;->applications:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/List;Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$1;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/List;)V

    return-void
.end method

.method public static newBuilder()Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getApplications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;->applications:Ljava/util/List;

    return-object v0
.end method

.method public getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    return-object v0
.end method
