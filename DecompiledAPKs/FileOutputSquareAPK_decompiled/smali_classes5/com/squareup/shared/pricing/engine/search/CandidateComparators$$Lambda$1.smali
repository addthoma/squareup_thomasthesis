.class final synthetic Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final arg$1:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

.field private final arg$2:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;


# direct methods
.method constructor <init>(Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$1;->arg$1:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$1;->arg$2:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$1;->arg$1:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$1;->arg$2:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    check-cast p1, Lcom/squareup/shared/pricing/engine/search/Candidate;

    check-cast p2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    invoke-static {v0, v1, p1, p2}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->lambda$exhaustComparator$1$CandidateComparators(Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I

    move-result p1

    return p1
.end method
