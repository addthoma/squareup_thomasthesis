.class public final enum Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;
.super Ljava/lang/Enum;
.source "ObjectIdFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

.field public static final enum DISCOUNT:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

.field public static final enum ITEM:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

.field public static final enum ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

.field public static final enum MENU_CATEGORY:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

.field public static final enum PRICING_RULE:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

.field public static final enum PRODUCT_SET:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

.field public static final enum TIME_PERIOD:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 12
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    const/4 v1, 0x0

    const-string v2, "DISCOUNT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->DISCOUNT:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    .line 13
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    const/4 v2, 0x1

    const-string v3, "ITEM"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->ITEM:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    .line 14
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    const/4 v3, 0x2

    const-string v4, "ITEM_VARIATION"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    .line 15
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    const/4 v4, 0x3

    const-string v5, "MENU_CATEGORY"

    invoke-direct {v0, v5, v4}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->MENU_CATEGORY:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    .line 16
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    const/4 v5, 0x4

    const-string v6, "PRICING_RULE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->PRICING_RULE:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    .line 17
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    const/4 v6, 0x5

    const-string v7, "PRODUCT_SET"

    invoke-direct {v0, v7, v6}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->PRODUCT_SET:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    .line 18
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    const/4 v7, 0x6

    const-string v8, "TIME_PERIOD"

    invoke-direct {v0, v8, v7}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->TIME_PERIOD:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    .line 11
    sget-object v8, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->DISCOUNT:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->ITEM:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->MENU_CATEGORY:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->PRICING_RULE:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->PRODUCT_SET:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->TIME_PERIOD:Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->$VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->$VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    invoke-virtual {v0}, [Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    return-object v0
.end method
