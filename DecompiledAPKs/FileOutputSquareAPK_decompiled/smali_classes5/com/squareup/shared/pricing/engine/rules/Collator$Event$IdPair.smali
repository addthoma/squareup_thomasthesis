.class Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;
.super Ljava/lang/Object;
.source "Collator.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/rules/Collator$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IdPair"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;",
        ">;"
    }
.end annotation


# instance fields
.field public final discountId:Ljava/lang/String;

.field public final ruleId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->ruleId:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->discountId:Ljava/lang/String;

    return-void
.end method

.method private getDiscountIdOrEmpty()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->discountId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;)I
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->ruleId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->ruleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 57
    :cond_0
    invoke-direct {p0}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->getDiscountIdOrEmpty()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->getDiscountIdOrEmpty()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->compareTo(Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 63
    instance-of v0, p1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 64
    check-cast p1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;

    .line 65
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->ruleId:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->ruleId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->getDiscountIdOrEmpty()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->getDiscountIdOrEmpty()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 74
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->ruleId:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->discountId:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
