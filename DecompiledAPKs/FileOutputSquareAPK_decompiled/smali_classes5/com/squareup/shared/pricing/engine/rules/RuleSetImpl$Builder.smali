.class public Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;
.super Ljava/lang/Object;
.source "RuleSetImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private end:Ljava/util/Date;

.field private rules:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation
.end field

.field private start:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;
    .locals 4

    .line 60
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->start:Ljava/util/Date;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->end:Ljava/util/Date;

    if-eqz v1, :cond_1

    .line 63
    invoke-virtual {v1, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->start:Ljava/util/Date;

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->end:Ljava/util/Date;

    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->rules:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;-><init>(Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;)V

    return-object v0

    .line 64
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "end date may not be before start date."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "start and end must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setEnd(Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->end:Ljava/util/Date;

    return-object p0
.end method

.method public setRules(Ljava/util/Set;)Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;"
        }
    .end annotation

    .line 53
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->rules:Ljava/util/Set;

    return-object p0
.end method

.method public setStart(Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->start:Ljava/util/Date;

    return-object p0
.end method
