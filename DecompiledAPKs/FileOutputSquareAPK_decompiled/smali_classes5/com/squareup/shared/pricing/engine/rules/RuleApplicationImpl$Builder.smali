.class public Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;
.super Ljava/lang/Object;
.source "RuleApplicationImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private applications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;",
            ">;"
        }
    .end annotation
.end field

.field private rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->applications:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addApplication(Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;
    .locals 1

    .line 58
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p0, p1, v0, p2}, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->addApplication(Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;

    move-result-object p1

    return-object p1
.end method

.method public addApplication(Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;
    .locals 2

    if-eqz p1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->applications:Ljava/util/List;

    new-instance v1, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;

    invoke-direct {v1, p1, p3, p2}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;-><init>(Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 64
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "target may not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public build()Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;
    .locals 4

    .line 71
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->applications:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    new-instance v0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->applications:Ljava/util/List;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/List;Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$1;)V

    return-object v0

    .line 76
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Applications must have at least one IdPair"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Rule must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 53
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplicationImpl$Builder;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    return-object p0

    .line 51
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "rule may not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
