.class public Lcom/squareup/shared/i18n/Key;
.super Ljava/lang/Object;
.source "Key.java"


# instance fields
.field private final androidKey:Ljava/lang/String;

.field private final iosKey:Ljava/lang/String;

.field private final tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/shared/i18n/Key;->androidKey:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/squareup/shared/i18n/Key;->iosKey:Ljava/lang/String;

    .line 40
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/i18n/Key;->tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public androidKey()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/shared/i18n/Key;->androidKey:Ljava/lang/String;

    return-object v0
.end method

.method public iosKey()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/shared/i18n/Key;->iosKey:Ljava/lang/String;

    return-object v0
.end method

.method public tokens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/shared/i18n/Key;->tokens:Ljava/util/List;

    return-object v0
.end method
