.class public Lcom/squareup/shared/catalog/utils/LanguageUtils;
.super Ljava/lang/Object;
.source "LanguageUtils.java"


# static fields
.field public static final ACCENTED_LATIN_END:C = '\u00ff'

.field public static final ACCENTED_LATIN_START:C = '\u00c0'

.field public static final FULLWIDTH_ASCII_END:C = '\uff5e'

.field public static final FULLWIDTH_ASCII_OFFSET:I = 0xfee0

.field public static final FULLWIDTH_ASCII_START:C = '\uff01'

.field private static final FULLWIDTH_KATAKANA:Ljava/lang/String; = "\u30fb\u30f2\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30cf\u30d2\u30d5\u30d8\u30db\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f3"

.field private static final FULLWIDTH_KATAKANA_DAKUTEN:Ljava/lang/String; = "\u30fb\u30fa\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30f4\u30a8\u30aa\u30ac\u30ae\u30b0\u30b2\u30b4\u30b6\u30b8\u30ba\u30bc\u30be\u30c0\u30c2\u30c5\u30c7\u30c9\u30ca\u30cb\u30cc\u30cd\u30ce\u30d0\u30d3\u30d6\u30d9\u30dc\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30f7\u30f3"

.field private static final FULLWIDTH_KATAKANA_HANDAKUTEN:Ljava/lang/String; = "\u30fb\u30f2\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30d1\u30d4\u30d7\u30da\u30dd\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f3"

.field public static final HALFWIDTH_KATAKANA_DAKUTEN:I = 0xff9e

.field public static final HALFWIDTH_KATAKANA_END:I = 0xff9f

.field public static final HALFWIDTH_KATAKANA_HANDAKUTEN:I = 0xff9f

.field public static final HALFWIDTH_KATAKANA_START:I = 0xff65

.field private static final HIRAGANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

.field public static final HIRAGANA_END:I = 0x309f

.field public static final HIRAGANA_START:I = 0x3041

.field public static final HIRAGANA_SYMBOLS_START:I = 0x3097

.field private static final JAPANESE_COLLATION_SECTION_INDEX_END:I = 0x24

.field private static final JAPANESE_COLLATION_SECTION_INDEX_START:I = 0x1b

.field public static final KANJI_END:I = 0x9faf

.field public static final KANJI_RARE_END:I = 0x4dbf

.field public static final KANJI_RARE_START:I = 0x3400

.field public static final KANJI_START:I = 0x4e00

.field private static final KATAKANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

.field public static final KATAKANA_END:I = 0x30ff

.field public static final KATAKANA_START:I = 0x30a1

.field public static final KATAKANA_SYMBOLS_START:I = 0x30fb

.field private static final LATIN_COLLATION_SECTION_INDEX_ALPHA_START:I = 0x1

.field private static final LATIN_COLLATION_SECTION_INDEX_END:I = 0x1a

.field private static final ROMAJI:[Ljava/lang/String;

.field private static final UNACCENTED_CHARACTERS:Ljava/lang/String; = "aaaaaaaceeeeiiiidnooooo ouuuuytsaaaaaaaceeeeiiiidnooooo ouuuuyty"


# direct methods
.method static constructor <clinit>()V
    .locals 103

    const/16 v0, 0xa

    new-array v1, v0, [Ljava/lang/Character;

    const/16 v2, 0x3042

    .line 23
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/16 v2, 0x304b

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/16 v2, 0x3055

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const/16 v2, 0x305f

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v6, 0x3

    aput-object v2, v1, v6

    const/16 v2, 0x306a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    const/16 v2, 0x306f

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v8, 0x5

    aput-object v2, v1, v8

    const/16 v2, 0x307e

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v9, 0x6

    aput-object v2, v1, v9

    const/16 v2, 0x3084

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/4 v10, 0x7

    aput-object v2, v1, v10

    const/16 v2, 0x3089

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v11, 0x8

    aput-object v2, v1, v11

    const/16 v2, 0x308f

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v12, 0x9

    aput-object v2, v1, v12

    sput-object v1, Lcom/squareup/shared/catalog/utils/LanguageUtils;->HIRAGANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

    new-array v0, v0, [Ljava/lang/Character;

    const/16 v1, 0x30a2

    .line 27
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0x30ab

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v4

    const/16 v1, 0x30b5

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v5

    const/16 v1, 0x30bf

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v6

    const/16 v1, 0x30ca

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v7

    const/16 v1, 0x30cf

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v8

    const/16 v1, 0x30de

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v9

    const/16 v1, 0x30e4

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v10

    const/16 v1, 0x30e9

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v11

    const/16 v1, 0x30ef

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    aput-object v1, v0, v12

    sput-object v0, Lcom/squareup/shared/catalog/utils/LanguageUtils;->KATAKANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

    const-string v13, "a"

    const-string v14, "a"

    const-string v15, "i"

    const-string v16, "i"

    const-string/jumbo v17, "u"

    const-string/jumbo v18, "u"

    const-string v19, "e"

    const-string v20, "e"

    const-string v21, "o"

    const-string v22, "o"

    const-string v23, "ka"

    const-string v24, "ga"

    const-string v25, "ki"

    const-string v26, "gi"

    const-string v27, "ku"

    const-string v28, "gu"

    const-string v29, "ke"

    const-string v30, "ge"

    const-string v31, "ko"

    const-string v32, "go"

    const-string v33, "sa"

    const-string/jumbo v34, "za"

    const-string v35, "shi"

    const-string v36, "ji"

    const-string v37, "su"

    const-string/jumbo v38, "zu"

    const-string v39, "se"

    const-string/jumbo v40, "ze"

    const-string v41, "so"

    const-string/jumbo v42, "zo"

    const-string v43, "ta"

    const-string v44, "da"

    const-string v45, "chi"

    const-string v46, "ji"

    const-string/jumbo v47, "tsu"

    const-string/jumbo v48, "tsu"

    const-string/jumbo v49, "zu"

    const-string v50, "te"

    const-string v51, "de"

    const-string/jumbo v52, "to"

    const-string v53, "do"

    const-string v54, "na"

    const-string v55, "ni"

    const-string v56, "nu"

    const-string v57, "ne"

    const-string v58, "no"

    const-string v59, "ha"

    const-string v60, "ba"

    const-string v61, "pa"

    const-string v62, "hi"

    const-string v63, "bi"

    const-string v64, "pi"

    const-string v65, "fu"

    const-string v66, "bu"

    const-string v67, "pu"

    const-string v68, "he"

    const-string v69, "be"

    const-string v70, "pe"

    const-string v71, "ho"

    const-string v72, "bo"

    const-string v73, "po"

    const-string v74, "ma"

    const-string v75, "mi"

    const-string v76, "mu"

    const-string v77, "me"

    const-string v78, "mo"

    const-string v79, "a"

    const-string/jumbo v80, "ya"

    const-string/jumbo v81, "u"

    const-string/jumbo v82, "yu"

    const-string v83, "o"

    const-string/jumbo v84, "yo"

    const-string v85, "ra"

    const-string v86, "ri"

    const-string v87, "ru"

    const-string v88, "re"

    const-string v89, "ro"

    const-string/jumbo v90, "wa"

    const-string/jumbo v91, "wa"

    const-string/jumbo v92, "wi"

    const-string/jumbo v93, "we"

    const-string/jumbo v94, "wo"

    const-string v95, "n"

    const-string/jumbo v96, "vu"

    const-string v97, "ka"

    const-string v98, "ke"

    const-string/jumbo v99, "va"

    const-string/jumbo v100, "vi"

    const-string/jumbo v101, "ve"

    const-string/jumbo v102, "vo"

    .line 51
    filled-new-array/range {v13 .. v102}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/utils/LanguageUtils;->ROMAJI:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static collationSectionIndex(Ljava/lang/String;)I
    .locals 5

    const/4 v0, 0x0

    if-eqz p0, :cond_14

    .line 341
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_5

    .line 345
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v3, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 349
    :goto_1
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->toFullwidthKatakana(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 351
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    return v0

    .line 355
    :cond_3
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    .line 358
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKanji(C)Z

    move-result v3

    const/16 v4, 0x24

    if-eqz v3, :cond_5

    if-eqz v1, :cond_4

    return v4

    :cond_4
    const/16 p0, 0x1a

    return p0

    :cond_5
    if-eqz v1, :cond_e

    .line 367
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKatakana(C)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHiragana(C)Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_6
    :goto_2
    const/16 v1, 0x1b

    const/16 v2, 0x9

    if-ge v0, v2, :cond_a

    .line 369
    sget-object v2, Lcom/squareup/shared/catalog/utils/LanguageUtils;->KATAKANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    if-lt p0, v2, :cond_7

    sget-object v2, Lcom/squareup/shared/catalog/utils/LanguageUtils;->KATAKANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    .line 370
    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    if-lt p0, v2, :cond_8

    :cond_7
    sget-object v2, Lcom/squareup/shared/catalog/utils/LanguageUtils;->HIRAGANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

    aget-object v2, v2, v0

    .line 371
    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    if-lt p0, v2, :cond_9

    sget-object v2, Lcom/squareup/shared/catalog/utils/LanguageUtils;->HIRAGANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    .line 372
    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    if-ge p0, v2, :cond_9

    :cond_8
    add-int/2addr v0, v1

    return v0

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 376
    :cond_a
    sget-object v0, Lcom/squareup/shared/catalog/utils/LanguageUtils;->KATAKANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-lt p0, v0, :cond_b

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKatakanaLetter(C)Z

    move-result v0

    if-nez v0, :cond_c

    :cond_b
    sget-object v0, Lcom/squareup/shared/catalog/utils/LanguageUtils;->HIRAGANA_COLLATION_SECTION_INDEX:[Ljava/lang/Character;

    aget-object v0, v0, v2

    .line 377
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-lt p0, v0, :cond_d

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHiraganaLetter(C)Z

    move-result p0

    if-eqz p0, :cond_d

    :cond_c
    return v4

    :cond_d
    return v1

    .line 387
    :cond_e
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isFullwidthAscii(C)Z

    move-result v1

    if-eqz v1, :cond_f

    const v1, 0xfee0

    sub-int/2addr p0, v1

    int-to-char p0, p0

    .line 391
    :cond_f
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKatakanaLetter(C)Z

    move-result v1

    if-nez v1, :cond_12

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHiraganaLetter(C)Z

    move-result v1

    if-eqz v1, :cond_10

    goto :goto_3

    .line 394
    :cond_10
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->removeDiacritics(C)Ljava/lang/String;

    move-result-object v1

    .line 395
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_11

    .line 396
    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, p0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    goto :goto_4

    .line 398
    :cond_11
    invoke-static {p0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object p0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    goto :goto_4

    .line 392
    :cond_12
    :goto_3
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->romanize(C)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    .line 402
    :goto_4
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isAccentedLatin(C)Z

    move-result v1

    if-eqz v1, :cond_13

    add-int/lit16 p0, p0, -0xc0

    const-string v1, "aaaaaaaceeeeiiiidnooooo ouuuuytsaaaaaaaceeeeiiiidnooooo ouuuuyty"

    .line 403
    invoke-virtual {v1, p0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    .line 409
    :cond_13
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->firstCharForLatinLigature(C)C

    move-result p0

    const/16 v1, 0x61

    if-lt p0, v1, :cond_14

    const/16 v3, 0x7a

    if-gt p0, v3, :cond_14

    sub-int/2addr p0, v1

    add-int/2addr p0, v2

    return p0

    :cond_14
    :goto_5
    return v0
.end method

.method public static endsWithI(Ljava/lang/String;)Z
    .locals 2

    .line 306
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    const/16 v0, 0x69

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private static firstCharForLatinLigature(C)C
    .locals 1

    const/16 v0, 0x152

    if-eq p0, v0, :cond_1

    const/16 v0, 0x153

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    return p0

    :cond_1
    :goto_0
    const/16 p0, 0x6f

    return p0
.end method

.method public static isAccentedLatin(C)Z
    .locals 1

    const/16 v0, 0xc0

    if-lt p0, v0, :cond_0

    const/16 v0, 0xff

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isFullwidthAscii(C)Z
    .locals 1

    const v0, 0xff01

    if-lt p0, v0, :cond_0

    const v0, 0xff5e

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isHalfwidthKatakana(C)Z
    .locals 1

    const v0, 0xff65

    if-lt p0, v0, :cond_0

    const v0, 0xff9f

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isHiragana(C)Z
    .locals 1

    const/16 v0, 0x3041

    if-lt p0, v0, :cond_0

    const/16 v0, 0x309f

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isHiraganaLetter(C)Z
    .locals 1

    const/16 v0, 0x3041

    if-lt p0, v0, :cond_0

    const/16 v0, 0x3097

    if-ge p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isKanji(C)Z
    .locals 1

    const/16 v0, 0x4e00

    if-lt p0, v0, :cond_0

    const v0, 0x9faf

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x3400

    if-lt p0, v0, :cond_2

    const/16 v0, 0x4dbf

    if-gt p0, v0, :cond_2

    :cond_1
    const/4 p0, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isKatakana(C)Z
    .locals 1

    const/16 v0, 0x30a1

    if-lt p0, v0, :cond_0

    const/16 v0, 0x30ff

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isKatakanaLetter(C)Z
    .locals 1

    const/16 v0, 0x30a1

    if-lt p0, v0, :cond_0

    const/16 v0, 0x30fb

    if-ge p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isVowel(Ljava/lang/String;I)Z
    .locals 2

    .line 311
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    if-lt p1, v0, :cond_0

    return v1

    .line 314
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p0

    .line 315
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHiraganaLetter(C)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKatakanaLetter(C)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHalfwidthKatakana(C)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    .line 319
    :cond_1
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->romanize(C)Ljava/lang/String;

    move-result-object p0

    .line 320
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_2

    return v1

    .line 323
    :cond_2
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result p0

    const/16 p1, 0x61

    if-eq p0, p1, :cond_3

    const/16 p1, 0x65

    if-eq p0, p1, :cond_3

    const/16 p1, 0x69

    if-eq p0, p1, :cond_3

    const/16 p1, 0x6f

    if-eq p0, p1, :cond_3

    const/16 p1, 0x75

    if-eq p0, p1, :cond_3

    const/16 p1, 0x79

    if-ne p0, p1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method public static normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 251
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 253
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->toFullwidthKatakana(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 254
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_a

    .line 256
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 258
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isFullwidthAscii(C)Z

    move-result v5

    if-eqz v5, :cond_1

    const v5, 0xfee0

    sub-int/2addr v4, v5

    int-to-char v4, v4

    .line 263
    :cond_1
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHiragana(C)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKatakana(C)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_1

    .line 289
    :cond_2
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isAccentedLatin(C)Z

    move-result v5

    if-eqz v5, :cond_3

    add-int/lit16 v4, v4, -0xc0

    const-string v5, "aaaaaaaceeeeiiiidnooooo ouuuuytsaaaaaaaceeeeiiiidnooooo ouuuuyty"

    .line 290
    invoke-virtual {v5, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 291
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 293
    :cond_3
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->removeDiacritics(C)Ljava/lang/String;

    move-result-object v5

    .line 294
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_4

    .line 295
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 297
    :cond_4
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 265
    :cond_5
    :goto_1
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHiraganaLetter(C)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKatakanaLetter(C)Z

    move-result v5

    if-nez v5, :cond_6

    .line 266
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 270
    :cond_6
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->romanize(C)Ljava/lang/String;

    move-result-object v4

    .line 276
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->endsWithI(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    add-int/lit8 v5, v3, 0x1

    invoke-static {p0, v5}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isVowel(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9

    const-string v5, "ji"

    .line 277
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v4, "j"

    goto :goto_2

    .line 279
    :cond_7
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_8

    .line 281
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x1

    invoke-virtual {v4, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v4, "y"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 284
    :cond_8
    invoke-virtual {v4, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 288
    :cond_9
    :goto_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 302
    :cond_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static removeDiacritics(C)Ljava/lang/String;
    .locals 5

    .line 421
    invoke-static {p0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {p0, v0}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object p0

    .line 422
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 423
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 424
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 425
    invoke-static {v2}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    sget-object v4, Ljava/lang/Character$UnicodeBlock;->COMBINING_DIACRITICAL_MARKS:Ljava/lang/Character$UnicodeBlock;

    if-eq v3, v4, :cond_0

    .line 426
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 429
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static romanize(C)Ljava/lang/String;
    .locals 2

    .line 448
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKatakana(C)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_1

    .line 449
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isKatakanaLetter(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    sget-object v0, Lcom/squareup/shared/catalog/utils/LanguageUtils;->ROMAJI:[Ljava/lang/String;

    add-int/lit16 p0, p0, -0x30a1

    aget-object p0, v0, p0

    return-object p0

    :cond_0
    return-object v1

    .line 454
    :cond_1
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHiragana(C)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 455
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHiraganaLetter(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 456
    sget-object v0, Lcom/squareup/shared/catalog/utils/LanguageUtils;->ROMAJI:[Ljava/lang/String;

    add-int/lit16 p0, p0, -0x3041

    aget-object p0, v0, p0

    return-object p0

    :cond_2
    return-object v1

    .line 461
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static toFullwidthKatakana(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_6

    .line 204
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 205
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->isHalfwidthKatakana(C)Z

    move-result v5

    if-eqz v5, :cond_4

    const v5, 0xff9e

    if-eq v4, v5, :cond_5

    const v6, 0xff9f

    if-ne v4, v6, :cond_0

    goto :goto_3

    :cond_0
    add-int/lit8 v7, v3, 0x1

    if-ge v7, v1, :cond_1

    .line 216
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    const v9, 0xff65

    sub-int/2addr v4, v9

    if-ne v8, v5, :cond_2

    const-string/jumbo v3, "\u30fb\u30fa\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30f4\u30a8\u30aa\u30ac\u30ae\u30b0\u30b2\u30b4\u30b6\u30b8\u30ba\u30bc\u30be\u30c0\u30c2\u30c5\u30c7\u30c9\u30ca\u30cb\u30cc\u30cd\u30ce\u30d0\u30d3\u30d6\u30d9\u30dc\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30f7\u30f3"

    .line 219
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_2
    move v3, v7

    goto :goto_3

    :cond_2
    if-ne v8, v6, :cond_3

    const-string/jumbo v3, "\u30fb\u30f2\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30d1\u30d4\u30d7\u30da\u30dd\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f3"

    .line 222
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    const-string/jumbo v5, "\u30fb\u30f2\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30cf\u30d2\u30d5\u30d8\u30db\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f3"

    .line 225
    invoke-virtual {v5, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 229
    :cond_4
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_5
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 233
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
