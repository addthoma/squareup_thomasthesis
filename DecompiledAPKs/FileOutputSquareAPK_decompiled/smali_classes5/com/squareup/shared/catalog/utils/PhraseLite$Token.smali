.class abstract Lcom/squareup/shared/catalog/utils/PhraseLite$Token;
.super Ljava/lang/Object;
.source "PhraseLite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/PhraseLite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Token"
.end annotation


# instance fields
.field private next:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

.field private final prev:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;


# direct methods
.method protected constructor <init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)V
    .locals 0

    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250
    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->prev:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    if-eqz p1, :cond_0

    .line 251
    iput-object p0, p1, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->next:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$Token;
    .locals 0

    .line 245
    iget-object p0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->next:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    return-object p0
.end method


# virtual methods
.method abstract expand(Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation
.end method

.method abstract getFormattedLength()I
.end method

.method final getFormattedStart()I
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->prev:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 267
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->getFormattedStart()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->prev:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->getFormattedLength()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
