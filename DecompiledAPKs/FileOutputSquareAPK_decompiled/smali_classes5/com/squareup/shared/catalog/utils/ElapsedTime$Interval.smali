.class public final enum Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;
.super Ljava/lang/Enum;
.source "ElapsedTime.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/ElapsedTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Interval"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

.field public static final enum DAYS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

.field public static final enum HOURS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

.field public static final enum MILLISECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

.field public static final enum MINUTES:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

.field public static final enum SECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

.field public static final enum WEEKS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

.field public static final enum YEARS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;


# instance fields
.field private final maxValue:J

.field private final milliseconds:J


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 59
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    const/4 v1, 0x0

    const-string v2, "MILLISECONDS"

    const-wide/16 v3, 0x1

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->MILLISECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    .line 60
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    const/4 v2, 0x1

    const-string v3, "SECONDS"

    const-wide/16 v4, 0x3e8

    invoke-direct {v0, v3, v2, v4, v5}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->SECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    .line 61
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    sget-object v3, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->SECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    const-wide/16 v4, 0x3c

    invoke-virtual {v3, v4, v5}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->asMilliseconds(J)J

    move-result-wide v6

    const/4 v3, 0x2

    const-string v8, "MINUTES"

    invoke-direct {v0, v8, v3, v6, v7}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->MINUTES:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    .line 62
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    sget-object v6, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->MINUTES:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-virtual {v6, v4, v5}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->asMilliseconds(J)J

    move-result-wide v4

    const/4 v6, 0x3

    const-string v7, "HOURS"

    invoke-direct {v0, v7, v6, v4, v5}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->HOURS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    .line 63
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    sget-object v4, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->HOURS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    const-wide/16 v7, 0x18

    invoke-virtual {v4, v7, v8}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->asMilliseconds(J)J

    move-result-wide v4

    const/4 v7, 0x4

    const-string v8, "DAYS"

    invoke-direct {v0, v8, v7, v4, v5}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->DAYS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    .line 64
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    sget-object v4, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->DAYS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    const-wide/16 v8, 0x7

    invoke-virtual {v4, v8, v9}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->asMilliseconds(J)J

    move-result-wide v4

    const/4 v8, 0x5

    const-string v9, "WEEKS"

    invoke-direct {v0, v9, v8, v4, v5}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->WEEKS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    .line 65
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    sget-object v4, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->DAYS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    const-wide/16 v9, 0x16d

    invoke-virtual {v4, v9, v10}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->asMilliseconds(J)J

    move-result-wide v4

    const/4 v9, 0x6

    const-string v10, "YEARS"

    invoke-direct {v0, v10, v9, v4, v5}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->YEARS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    .line 58
    sget-object v4, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->MILLISECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->SECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->MINUTES:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->HOURS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->DAYS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->WEEKS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->YEARS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->$VALUES:[Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput-wide p3, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->milliseconds:J

    const-wide p1, 0x7fffffffffffffffL

    .line 78
    div-long/2addr p1, p3

    iput-wide p1, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->maxValue:J

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)J
    .locals 2

    .line 58
    iget-wide v0, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->maxValue:J

    return-wide v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;
    .locals 1

    .line 58
    const-class v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->$VALUES:[Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    return-object v0
.end method


# virtual methods
.method public asMilliseconds(J)J
    .locals 3

    .line 89
    iget-wide v0, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->maxValue:J

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    .line 92
    iget-wide v0, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->milliseconds:J

    mul-long p1, p1, v0

    return-wide p1

    .line 90
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The given value is too large: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
