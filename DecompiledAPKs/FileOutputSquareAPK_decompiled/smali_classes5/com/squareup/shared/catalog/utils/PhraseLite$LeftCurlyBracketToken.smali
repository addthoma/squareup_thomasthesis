.class Lcom/squareup/shared/catalog/utils/PhraseLite$LeftCurlyBracketToken;
.super Lcom/squareup/shared/catalog/utils/PhraseLite$Token;
.source "PhraseLite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/PhraseLite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LeftCurlyBracketToken"
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)V
    .locals 0

    .line 293
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;-><init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)V

    return-void
.end method


# virtual methods
.method expand(Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .line 297
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite$LeftCurlyBracketToken;->getFormattedStart()I

    move-result p2

    add-int/lit8 v0, p2, 0x2

    const-string/jumbo v1, "{"

    .line 298
    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->replace(IILjava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;

    return-void
.end method

.method getFormattedLength()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
