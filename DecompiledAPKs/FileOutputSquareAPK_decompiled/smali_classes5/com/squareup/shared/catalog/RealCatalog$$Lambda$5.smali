.class final synthetic Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final arg$1:Lcom/squareup/shared/catalog/RealCatalog;

.field private final arg$2:Ljava/util/concurrent/Callable;

.field private final arg$3:Lcom/squareup/shared/catalog/CatalogCallback;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/RealCatalog;Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;->arg$1:Lcom/squareup/shared/catalog/RealCatalog;

    iput-object p2, p0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;->arg$2:Ljava/util/concurrent/Callable;

    iput-object p3, p0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;->arg$3:Lcom/squareup/shared/catalog/CatalogCallback;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;->arg$1:Lcom/squareup/shared/catalog/RealCatalog;

    iget-object v1, p0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;->arg$2:Ljava/util/concurrent/Callable;

    iget-object v2, p0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;->arg$3:Lcom/squareup/shared/catalog/CatalogCallback;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/RealCatalog;->lambda$executeOnFileThread$6$RealCatalog(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method
