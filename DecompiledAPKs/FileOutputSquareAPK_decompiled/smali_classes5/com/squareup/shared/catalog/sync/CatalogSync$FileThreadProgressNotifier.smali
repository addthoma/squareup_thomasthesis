.class final Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;
.super Ljava/lang/Object;
.source "CatalogSync.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FileThreadProgressNotifier"
.end annotation


# instance fields
.field private final delegate:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

.field private final mainThread:Ljava/util/concurrent/Executor;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;)V
    .locals 0

    .line 667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;->mainThread:Ljava/util/concurrent/Executor;

    .line 669
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;->delegate:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;Lcom/squareup/shared/catalog/sync/CatalogSync$1;)V
    .locals 0

    .line 663
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;-><init>(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;)V

    return-void
.end method


# virtual methods
.method final synthetic lambda$onNext$0$CatalogSync$FileThreadProgressNotifier(I)V
    .locals 1

    .line 673
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;->delegate:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;->onNext(I)V

    return-void
.end method

.method public onNext(I)V
    .locals 2

    .line 673
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;->mainThread:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier$$Lambda$0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
