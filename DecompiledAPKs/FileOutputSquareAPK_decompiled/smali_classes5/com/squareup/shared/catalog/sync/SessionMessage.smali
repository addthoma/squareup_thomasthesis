.class final Lcom/squareup/shared/catalog/sync/SessionMessage;
.super Lcom/squareup/shared/catalog/sync/CatalogMessage;
.source "SessionMessage.java"


# instance fields
.field private final catalog:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field private final sessionCallback:Lcom/squareup/shared/catalog/sync/SyncCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/api/rpc/Request;Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/Request;",
            "Lcom/squareup/shared/catalog/sync/CatalogSync;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogMessage;-><init>(Lcom/squareup/api/rpc/Request;)V

    .line 17
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/SessionMessage;->catalog:Lcom/squareup/shared/catalog/sync/CatalogSync;

    .line 18
    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/SessionMessage;->sessionCallback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-void
.end method

.method private static throwError(Ljava/lang/String;)V
    .locals 1

    .line 43
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/Response;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 22
    iget-object v0, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/SessionMessage;->catalog:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-object p1, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->handleRpcError(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/api/rpc/Error;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    .line 24
    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    .line 27
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    if-nez v0, :cond_1

    const-string v0, "No CreateSessionResponse in Catalog start session response."

    .line 28
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SessionMessage;->throwError(Ljava/lang/String;)V

    .line 31
    :cond_1
    iget-object p1, p1, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    .line 32
    iget-object v0, p1, Lcom/squareup/api/sync/CreateSessionResponse;->session_id:Ljava/lang/Long;

    if-nez v0, :cond_2

    const-string v0, "No session ID in Catalog create session response."

    .line 33
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SessionMessage;->throwError(Ljava/lang/String;)V

    .line 36
    :cond_2
    iget-object p1, p1, Lcom/squareup/api/sync/CreateSessionResponse;->session_id:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 37
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/SessionMessage;->sessionCallback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    .line 39
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
