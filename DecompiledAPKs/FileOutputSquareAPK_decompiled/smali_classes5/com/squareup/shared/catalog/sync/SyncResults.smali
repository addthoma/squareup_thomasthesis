.class public Lcom/squareup/shared/catalog/sync/SyncResults;
.super Ljava/lang/Object;
.source "SyncResults.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static empty()Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 12
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v0

    return-object v0
.end method

.method public static error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/sync/SyncError;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;"
        }
    .end annotation

    .line 16
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/squareup/shared/catalog/sync/SyncResult;-><init>(Ljava/lang/Object;Lcom/squareup/shared/catalog/sync/SyncError;)V

    return-object v0
.end method

.method public static of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;"
        }
    .end annotation

    .line 8
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncResult;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/shared/catalog/sync/SyncResult;-><init>(Ljava/lang/Object;Lcom/squareup/shared/catalog/sync/SyncError;)V

    return-object v0
.end method
