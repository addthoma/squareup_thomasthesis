.class Lcom/squareup/shared/catalog/sync/CatalogSync$3;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;->foregroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$3;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
            ">;"
        }
    .end annotation

    .line 175
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locked()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$3;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$100(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    move-result-object v0

    .line 182
    iget-boolean v2, v0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->requiresSyntheticTableRebuild:Z

    if-eqz v2, :cond_2

    .line 184
    new-instance v2, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$3;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    .line 185
    invoke-static {v3}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/Executor;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$3;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {v4}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$300(Lcom/squareup/shared/catalog/sync/CatalogSync;)Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    move-result-object v4

    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;-><init>(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;Lcom/squareup/shared/catalog/sync/CatalogSync$1;)V

    .line 184
    invoke-virtual {p1, v2}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->buildSyntheticTablesFromLocal(Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;)V

    .line 186
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->requiresSyntheticTableRebuild()Z

    move-result p1

    if-nez p1, :cond_1

    .line 190
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$3;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$500(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object p1

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 187
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "CatalogStore still requires synthetic table rebuild after attempting rebuild."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 192
    :cond_2
    :goto_0
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method
