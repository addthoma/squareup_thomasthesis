.class public final Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;
.super Ljava/lang/Object;
.source "CatalogMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final menu:Lcom/squareup/api/items/Menu$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 60
    new-instance v0, Lcom/squareup/api/items/Menu$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Menu$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Menu$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Menu$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;->menu:Lcom/squareup/api/items/Menu$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogMenu;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;->menu:Lcom/squareup/api/items/Menu$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/Menu$Builder;->build()Lcom/squareup/api/items/Menu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu(Lcom/squareup/api/items/Menu;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 75
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogMenu;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogMenu;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;->menu:Lcom/squareup/api/items/Menu$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Menu$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Menu$Builder;

    return-object p0
.end method

.method public setRootTag(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenu$Builder;->menu:Lcom/squareup/api/items/Menu$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Menu$Builder;->root_tag(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Menu$Builder;

    return-object p0
.end method
