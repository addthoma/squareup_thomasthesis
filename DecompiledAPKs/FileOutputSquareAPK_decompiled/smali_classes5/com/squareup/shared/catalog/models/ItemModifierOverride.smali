.class public final Lcom/squareup/shared/catalog/models/ItemModifierOverride;
.super Ljava/lang/Object;
.source "ItemModifierOverride.java"


# instance fields
.field public final hiddenFromCustomer:Z

.field public final maxSelectedModifiers:I

.field public final minSelectedModifiers:I

.field public final optionOverride:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;IIZ)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->optionOverride:Ljava/util/List;

    .line 22
    iput p2, p0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->minSelectedModifiers:I

    .line 23
    iput p3, p0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->maxSelectedModifiers:I

    .line 24
    iput-boolean p4, p0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->hiddenFromCustomer:Z

    return-void
.end method
