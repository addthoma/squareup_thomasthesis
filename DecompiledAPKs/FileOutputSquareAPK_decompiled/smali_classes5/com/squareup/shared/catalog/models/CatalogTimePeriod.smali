.class public final Lcom/squareup/shared/catalog/models/CatalogTimePeriod;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogTimePeriod.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/TimePeriod;",
        ">;"
    }
.end annotation


# static fields
.field private static recurrenceRuleConverter:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;


# instance fields
.field private cachedRecurrenceValueKey:Ljava/lang/String;

.field private endsAt:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;

    invoke-direct {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->recurrenceRuleConverter:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    const/4 p1, 0x0

    .line 31
    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->endsAt:Ljava/util/Date;

    .line 33
    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->cachedRecurrenceValueKey:Ljava/lang/String;

    return-void
.end method

.method private recurrenceRule(Ljava/util/TimeZone;Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;
    .locals 3

    .line 229
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getRRule()Ljava/lang/String;

    move-result-object v0

    .line 230
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDurationMillis()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    .line 234
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Time period has a duration but no starts at"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    if-nez p2, :cond_3

    if-eqz v0, :cond_3

    .line 236
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 237
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Time period has a recurrence rule but no starts at"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    if-nez v1, :cond_5

    if-eqz v0, :cond_5

    .line 239
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_2

    .line 240
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Time period has a recurrence rule but no duration"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_2
    const/4 v2, 0x0

    if-eqz p2, :cond_8

    if-nez v1, :cond_6

    goto :goto_3

    :cond_6
    if-eqz v0, :cond_8

    .line 249
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    goto :goto_3

    .line 253
    :cond_7
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->recurrenceRuleConverter:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;

    invoke-virtual {v1, v0, p1, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseRecurrenceRule(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;

    move-result-object p1

    return-object p1

    :cond_8
    :goto_3
    return-object v2
.end method


# virtual methods
.method public activeAt(Ljava/util/Date;Ljava/util/TimeZone;)Z
    .locals 5

    .line 104
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDurationMillis()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 107
    invoke-virtual {p0, p2}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDates(Ljava/util/TimeZone;)Ljava/util/Iterator;

    move-result-object p2

    .line 108
    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 109
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 110
    invoke-virtual {v0, p1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    .line 113
    :cond_1
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDurationMillis()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_2
    return v1

    .line 105
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Duration must not be null or empty."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public endsAt(Ljava/util/TimeZone;)Ljava/util/Date;
    .locals 4

    .line 125
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->endsAt:Ljava/util/Date;

    if-eqz v0, :cond_0

    return-object v0

    .line 129
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->startsAt(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v0

    .line 130
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDurationMillis()Ljava/lang/Long;

    move-result-object v1

    .line 131
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->recurrenceRule(Ljava/util/TimeZone;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;

    move-result-object p1

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    if-nez p1, :cond_2

    .line 140
    new-instance p1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v2, v0

    invoke-direct {p1, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object p1

    .line 143
    :cond_2
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getCount()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_3

    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getUntil()Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_3

    return-object v2

    .line 150
    :cond_3
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    goto :goto_0

    .line 153
    :cond_4
    new-instance p1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v2, v0

    invoke-direct {p1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->endsAt:Ljava/util/Date;

    .line 154
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->endsAt:Ljava/util/Date;

    return-object p1

    :cond_5
    :goto_1
    return-object v2
.end method

.method public getDates(Ljava/util/TimeZone;)Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            ")",
            "Ljava/util/Iterator<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .line 85
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getStartsAt()Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/util/Date;

    const-wide/high16 v1, -0x8000000000000000L

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    .line 87
    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseDateTime(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v0

    .line 89
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getRRule()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    return-object p1

    .line 94
    :cond_1
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->recurrenceRuleConverter:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;

    .line 95
    invoke-virtual {v2, v1, p1, v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseRecurrenceRule(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;

    move-result-object p1

    .line 96
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;-><init>(Lcom/squareup/shared/ical/rrule/RecurrenceRule;Ljava/util/Date;)V

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 2

    .line 67
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TimePeriod;

    iget-object v0, v0, Lcom/squareup/api/items/TimePeriod;->duration:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDurationMillis()Ljava/lang/Long;
    .locals 2

    .line 74
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDuration()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 78
    :cond_0
    invoke-static {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseDuration(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 46
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TimePeriod;

    iget-object v0, v0, Lcom/squareup/api/items/TimePeriod;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRRule()Ljava/lang/String;
    .locals 2

    .line 64
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TimePeriod;

    iget-object v0, v0, Lcom/squareup/api/items/TimePeriod;->rrule:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSchedule(Ljava/util/TimeZone;)Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;
    .locals 9

    .line 179
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->recurrenceRule(Ljava/util/TimeZone;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 184
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->startsAt(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v2

    .line 185
    invoke-static {p1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v3

    .line 186
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 187
    new-instance v4, Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDurationMillis()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    add-long/2addr v5, v7

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 188
    invoke-static {p1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object p1

    .line 189
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 192
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$1;->$SwitchMap$com$squareup$shared$ical$rrule$RecurrenceRule$Frequency:[I

    invoke-interface {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getFrequency()Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->ordinal()I

    move-result v4

    aget v2, v2, v4

    const/4 v4, 0x1

    if-eq v2, v4, :cond_3

    const/4 v4, 0x2

    if-eq v2, v4, :cond_1

    goto/16 :goto_0

    .line 204
    :cond_1
    invoke-interface {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByDay()Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 207
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x7

    .line 208
    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 209
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->values()[Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v1

    invoke-virtual {v3}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v2

    aget-object v1, v1, v2

    .line 210
    new-instance v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;-><init>(Ljava/lang/Integer;Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)V

    .line 211
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 212
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 194
    :cond_3
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 195
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->SU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->MO:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 197
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->TU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 198
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->WE:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 199
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->TH:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->FR:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 201
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->SA:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_4
    :goto_0
    new-instance v0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    invoke-direct {v0, v1, v3, p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;-><init>(Ljava/util/Set;Ljava/util/Calendar;Ljava/util/Calendar;)V

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStartsAt()Ljava/lang/String;
    .locals 2

    .line 49
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TimePeriod;

    iget-object v0, v0, Lcom/squareup/api/items/TimePeriod;->starts_at:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public recurrenceRule(Ljava/util/TimeZone;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;
    .locals 1

    .line 225
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->startsAt(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->recurrenceRule(Ljava/util/TimeZone;Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;

    move-result-object p1

    return-object p1
.end method

.method public recurrenceRuleValueKey()Ljava/lang/String;
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->cachedRecurrenceValueKey:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getStartsAt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getRRule()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDuration()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Murmur3;->hash64([B)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->cachedRecurrenceValueKey:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->cachedRecurrenceValueKey:Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->cachedRecurrenceValueKey:Ljava/lang/String;

    return-object v0
.end method

.method public startsAt(Ljava/util/TimeZone;)Ljava/util/Date;
    .locals 2

    .line 57
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getStartsAt()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 58
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 61
    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseDateTime(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 159
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getStartsAt()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getRRule()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDuration()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "%s{id=%s, name=%s, starts_at=%s, rrule=%s, duration=%s}"

    .line 158
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
