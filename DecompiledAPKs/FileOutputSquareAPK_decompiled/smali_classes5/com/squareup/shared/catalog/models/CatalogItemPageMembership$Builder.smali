.class public final Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
.super Ljava/lang/Object;
.source "CatalogItemPageMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 121
    new-instance v0, Lcom/squareup/api/items/ItemPageMembership$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemPageMembership$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    .line 122
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->setPageId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;

    .line 123
    invoke-direct {p0, p2}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;

    return-void
.end method

.method private setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    return-object p0
.end method

.method private setPageId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->page(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->build()Lcom/squareup/api/items/ItemPageMembership;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership(Lcom/squareup/api/items/ItemPageMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 153
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setColumn(I)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->column(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    return-object p0
.end method

.method public setHeight(I)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->height(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    return-object p0
.end method

.method public setPosition(I)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->position(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    return-object p0
.end method

.method public setRow(I)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->row(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    return-object p0
.end method

.method public setWidth(I)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;->itemPageMembership:Lcom/squareup/api/items/ItemPageMembership$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->width(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    return-object p0
.end method
