.class public final Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogItemItemModifierListMembership.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/ItemItemModifierListMembership;",
        ">;"
    }
.end annotation


# instance fields
.field private final hasOptionOverrideOnByDefault:Z


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 2

    .line 139
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    .line 142
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->hasObjectExtension()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 143
    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getModifierOptionOverrides()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    .line 144
    iget-object v1, v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ItemModifierOptionOverride;->DEFAULT_ON_BY_DEFAULT:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 150
    :goto_0
    iput-boolean p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->hasOptionOverrideOnByDefault:Z

    return-void
.end method

.method private getMaxSelectedModifiers()I
    .locals 2

    .line 80
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierListMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private getMinSelectedModifiers()I
    .locals 2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierListMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private getModifierOptionOverrides()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;"
        }
    .end annotation

    .line 75
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierListMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    .line 76
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 75
    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private shouldHideFromCustomer()Z
    .locals 2

    .line 88
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierListMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_HIDDEN_FROM_CUSTOMER:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getItemId()Ljava/lang/String;
    .locals 2

    .line 53
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierListMembership;

    .line 54
    iget-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getItemModifierOverride()Lcom/squareup/shared/catalog/models/ItemModifierOverride;
    .locals 5

    .line 59
    new-instance v0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getModifierOptionOverrides()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getMinSelectedModifiers()I

    move-result v2

    .line 60
    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getMaxSelectedModifiers()I

    move-result v3

    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->shouldHideFromCustomer()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/shared/catalog/models/ItemModifierOverride;-><init>(Ljava/util/List;IIZ)V

    return-object v0
.end method

.method public getModifierListId()Ljava/lang/String;
    .locals 2

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierListMembership;

    .line 48
    iget-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 49
    iget-object v0, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Type;

    .line 43
    sget-object v1, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 37
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getItemId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getModifierListId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public isAdvancedModifierEnabled()Z
    .locals 2

    .line 68
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->hasOptionOverrideOnByDefault:Z

    if-nez v0, :cond_1

    .line 69
    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getMaxSelectedModifiers()I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 70
    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getMinSelectedModifiers()I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 71
    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->shouldHideFromCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isEnabled()Z
    .locals 2

    .line 64
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierListMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
