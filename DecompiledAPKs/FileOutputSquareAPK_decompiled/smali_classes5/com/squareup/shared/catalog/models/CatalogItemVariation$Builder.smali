.class public final Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
.super Ljava/lang/Object;
.source "CatalogItemVariation.java"

# interfaces
.implements Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItemVariation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private dirty:Z

.field private final variation:Lcom/squareup/api/items/ItemVariation$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method private constructor <init>(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lcom/squareup/api/items/ItemVariation$Builder;Z)V
    .locals 0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 198
    iput-object p2, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    .line 199
    iput-boolean p3, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V
    .locals 0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 181
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemVariation;->newBuilder()Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 171
    new-instance v0, Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemVariation$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    .line 172
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->clearPrice()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    const/4 p1, 0x0

    .line 175
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    const/4 p1, 0x1

    .line 176
    iput-boolean p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    return-void
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 3

    .line 189
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->dirtyFlag([B)Z

    move-result v0

    .line 190
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->fromByteArrayWithDirtyFlag([B)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 191
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    if-nez v1, :cond_0

    .line 192
    new-instance v1, Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/ItemVariation$Builder;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemVariation;->newBuilder()Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v1

    .line 193
    :goto_0
    new-instance v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-direct {v2, p0, v1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lcom/squareup/api/items/ItemVariation$Builder;Z)V

    return-object v2
.end method


# virtual methods
.method public bridge synthetic addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;
    .locals 0

    .line 163
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    return-object p1
.end method

.method public addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 2

    .line 232
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getItemOptionID()Ljava/lang/String;

    move-result-object v0

    .line 233
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object p1

    .line 234
    new-instance v1, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    invoke-direct {v1, v0, p1}, Lcom/squareup/api/items/ItemOptionValueForItemVariation;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;
    .locals 2

    .line 441
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemVariation$Builder;->build()Lcom/squareup/api/items/ItemVariation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 442
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public clearPrice()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 2

    const/4 v0, 0x1

    .line 361
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 362
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;

    .line 363
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    sget-object v1, Lcom/squareup/api/items/PricingType;->VARIABLE_PRICING:Lcom/squareup/api/items/PricingType;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public copyOptionValueIdPairs()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;"
        }
    .end annotation

    .line 456
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 458
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 459
    new-instance v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    iget-object v4, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    iget-object v2, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-direct {v3, v4, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getAvailableOnOnlineBookingSite()Z
    .locals 2

    .line 330
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->available_on_online_booking_site:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_AVAILABLE_ON_ONLINE_BOOKING_SITE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getCatalogMeasurementUnitToken()Ljava/lang/String;
    .locals 2

    .line 451
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->measurement_unit_token:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .line 286
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->service_duration:Ljava/lang/Long;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_SERVICE_DURATION:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getEmployeeTokens()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 341
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->employee_tokens:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getIntermissions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation

    .line 351
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->intermissions:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1

    .line 212
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getName(Ljava/util/Map;)Ljava/lang/String;
    .locals 0

    invoke-static {p0, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination$$CC;->getName$$dflt$$(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNoShowFee()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 316
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/Money;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getPrice()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 374
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->isVariablePricing()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/Money;

    .line 377
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getPriceDescription()Ljava/lang/String;
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->price_description:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSku()Ljava/lang/String;
    .locals 2

    .line 222
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->sku:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTransitionTime()J
    .locals 2

    .line 306
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->transition_time:Ljava/lang/Long;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_TRANSITION_TIME:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public isDirty()Z
    .locals 1

    .line 396
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    return v0
.end method

.method public isVariablePricing()Z
    .locals 3

    .line 368
    sget-object v0, Lcom/squareup/api/items/PricingType;->VARIABLE_PRICING:Lcom/squareup/api/items/PricingType;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v1, v1, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    sget-object v2, Lcom/squareup/api/items/ItemVariation;->DEFAULT_PRICING_TYPE:Lcom/squareup/api/items/PricingType;

    .line 369
    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 368
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/PricingType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public optionValueCount()I
    .locals 1

    invoke-static {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination$$CC;->optionValueCount$$dflt$$(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)I

    move-result v0

    return v0
.end method

.method public removeAllOptionValues()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object p0
.end method

.method public bridge synthetic removeOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;
    .locals 0

    .line 163
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->removeOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    return-object p1
.end method

.method public removeOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 2

    .line 251
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getItemOptionID()Ljava/lang/String;

    move-result-object v0

    .line 252
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object p1

    .line 253
    new-instance v1, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    invoke-direct {v1, v0, p1}, Lcom/squareup/api/items/ItemOptionValueForItemVariation;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic reorderOption(Ljava/lang/String;II)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;
    .locals 0

    .line 163
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->reorderOption(Ljava/lang/String;II)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    return-object p1
.end method

.method public reorderOption(Ljava/lang/String;II)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 4

    .line 241
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    .line 242
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 243
    iget-object v1, v0, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "itemOption with id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is unavailable in the variation."

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 245
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 246
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {p1, p3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public setAvailableOnOnlineBookingSite(Z)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 335
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 336
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->available_on_online_booking_site(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setCatalogMeasurementUnitToken(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    .line 446
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->measurement_unit_token(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 290
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 291
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->service_duration(Ljava/lang/Long;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setEmployeeTokens(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 345
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 346
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->employee_tokens(Ljava/util/List;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 424
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 425
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setIntermissions(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 355
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 356
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->intermissions(Ljava/util/List;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setInventoryAlertThreshold(Ljava/lang/Long;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 412
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 413
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->inventory_alert_threshold(Ljava/lang/Long;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setInventoryAlertType(Lcom/squareup/api/items/ItemVariation$InventoryAlertType;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 406
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 407
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->inventory_alert_type(Lcom/squareup/api/items/ItemVariation$InventoryAlertType;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 2

    const/4 v0, 0x1

    .line 270
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 271
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setMerchantCatalogObjectReference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 216
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 217
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setNoShowFee(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 320
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    if-nez p1, :cond_0

    .line 322
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/ItemVariation$Builder;->no_show_fee_money(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;

    goto :goto_0

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->no_show_fee_money(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;

    :goto_0
    return-object p0
.end method

.method public setOrClearPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 389
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->clearPrice()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    return-object p1

    .line 391
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 280
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 281
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 381
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 382
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;

    .line 383
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    sget-object v0, Lcom/squareup/api/items/PricingType;->FIXED_PRICING:Lcom/squareup/api/items/PricingType;

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setPriceDescription(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 300
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 301
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->price_description(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setSku(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 226
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 227
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->sku(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setTrackInventory(Ljava/lang/Boolean;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 400
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 401
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->track_inventory(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public setTransitionTime(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 310
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    .line 311
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->variation:Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->transition_time(Ljava/lang/Long;)Lcom/squareup/api/items/ItemVariation$Builder;

    return-object p0
.end method

.method public toByteArray()[B
    .locals 2

    .line 185
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->dirty:Z

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogObject;->toByteArrayWithDirtyPrefix(Lcom/squareup/api/sync/ObjectWrapper;Z)[B

    move-result-object v0

    return-object v0
.end method
