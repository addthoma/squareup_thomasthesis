.class public final Lcom/squareup/shared/catalog/models/CatalogPage;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogPage$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Page;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public copyWithIndex(I)Lcom/squareup/shared/catalog/models/CatalogPage;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPage;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Page;

    invoke-virtual {v0}, Lcom/squareup/api/items/Page;->newBuilder()Lcom/squareup/api/items/Page$Builder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Page$Builder;->page_index(Ljava/lang/Integer;)Lcom/squareup/api/items/Page$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/items/Page$Builder;->build()Lcom/squareup/api/items/Page;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogPage;->copy(Lcom/squareup/api/items/Page;)Lcom/squareup/shared/catalog/models/CatalogPage;

    move-result-object p1

    return-object p1
.end method

.method public getMenuGroup()Lcom/squareup/api/sync/ObjectId;
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPage;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Page;

    iget-object v0, v0, Lcom/squareup/api/items/Page;->tag:Lcom/squareup/api/sync/ObjectId;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 31
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPage;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Page;

    iget-object v0, v0, Lcom/squareup/api/items/Page;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPageIndex()I
    .locals 2

    .line 35
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPage;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Page;

    iget-object v0, v0, Lcom/squareup/api/items/Page;->page_index:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/Page;->DEFAULT_PAGE_INDEX:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getPageLayout()Lcom/squareup/api/items/PageLayout;
    .locals 2

    .line 39
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPage;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Page;

    iget-object v0, v0, Lcom/squareup/api/items/Page;->layout:Lcom/squareup/api/items/PageLayout;

    sget-object v1, Lcom/squareup/api/items/Page;->DEFAULT_LAYOUT:Lcom/squareup/api/items/PageLayout;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PageLayout;

    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    .line 59
    sget-object v0, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 51
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPage;->getMenuGroup()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 53
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_PAGE_MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPage;->getMenuGroup()Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method
