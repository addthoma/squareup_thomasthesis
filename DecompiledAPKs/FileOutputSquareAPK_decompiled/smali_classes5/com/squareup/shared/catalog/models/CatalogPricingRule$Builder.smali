.class public final Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
.super Ljava/lang/Object;
.source "CatalogPricingRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogPricingRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRICING_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 223
    new-instance v0, Lcom/squareup/api/items/PricingRule$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/PricingRule$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/PricingRule$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogPricingRule;)V
    .locals 0

    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 228
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    invoke-virtual {p1}, Lcom/squareup/api/items/PricingRule;->newBuilder()Lcom/squareup/api/items/PricingRule$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogPricingRule;
    .locals 2

    .line 378
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/PricingRule$Builder;->build()Lcom/squareup/api/items/PricingRule;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule(Lcom/squareup/api/items/PricingRule;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 379
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public getApplicationMode()Lcom/squareup/api/items/PricingRule$ApplicationMode;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    return-object v0
.end method

.method public getApplyProducts()Ljava/lang/String;
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->apply_products:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->apply_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountId()Ljava/lang/String;
    .locals 1

    .line 352
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->discount_id:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->discount_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountTargetScope()Lcom/squareup/api/items/PricingRule$DiscountTargetScope;
    .locals 1

    .line 367
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    return-object v0
.end method

.method public getExcludeProducts()Ljava/lang/String;
    .locals 1

    .line 345
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getMatchProducts()Ljava/lang/String;
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->match_products:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->match_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getStackable()Lcom/squareup/api/items/PricingRule$Stackable;
    .locals 1

    .line 359
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    return-object v0
.end method

.method public getValidity()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 322
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/PricingRule$Builder;->validity:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 323
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 324
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/sync/ObjectId;

    .line 325
    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public setApplicationMode(Lcom/squareup/api/items/PricingRule$ApplicationMode;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->application_mode(Lcom/squareup/api/items/PricingRule$ApplicationMode;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setApplyProducts(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 2

    if-nez p1, :cond_0

    .line 260
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/PricingRule$Builder;->apply_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->apply_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setDiscount(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 2

    if-nez p1, :cond_0

    .line 278
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/PricingRule$Builder;->discount_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->discount_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setDiscountTargetSccope(Lcom/squareup/api/items/PricingRule$DiscountTargetScope;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->discount_target_scope(Lcom/squareup/api/items/PricingRule$DiscountTargetScope;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setExcludeProducts(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 2

    if-nez p1, :cond_0

    .line 269
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/PricingRule$Builder;->exclude_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->exclude_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setIdForTest(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 2

    .line 371
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 372
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 373
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setMatchProducts(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 2

    if-nez p1, :cond_0

    .line 251
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/PricingRule$Builder;->match_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->match_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setMaxApplicationsPerAttachment(I)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->max_applications_per_attachment(Ljava/lang/Integer;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setStackable(Lcom/squareup/api/items/PricingRule$Stackable;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->stackable(Lcom/squareup/api/items/PricingRule$Stackable;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setValidFrom(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->valid_from(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setValidUntil(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/PricingRule$Builder;->valid_until(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method

.method public setValidity(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 238
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/PricingRule$Builder;->validity(Ljava/util/List;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0

    .line 241
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 242
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 243
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TIME_PERIOD:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v2, v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 245
    :cond_1
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule$Builder;->pricingRule:Lcom/squareup/api/items/PricingRule$Builder;

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/PricingRule$Builder;->validity(Ljava/util/List;)Lcom/squareup/api/items/PricingRule$Builder;

    return-object p0
.end method
