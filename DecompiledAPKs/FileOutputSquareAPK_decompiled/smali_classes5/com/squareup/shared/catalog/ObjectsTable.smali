.class public Lcom/squareup/shared/catalog/ObjectsTable;
.super Ljava/lang/Object;
.source "ObjectsTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/ObjectsTable$Query;
    }
.end annotation


# static fields
.field static final COLUMN_ID:Ljava/lang/String; = "object_id"

.field static final COLUMN_ITEM_TYPE:Ljava/lang/String; = "item_type"

.field static final COLUMN_OBJECT:Ljava/lang/String; = "object_blob"

.field static final COLUMN_OBJECT_TYPE:Ljava/lang/String; = "object_type"

.field static final COLUMN_SORT_TEXT:Ljava/lang/String; = "sort_text"

.field static final COLUMN_TOKEN:Ljava/lang/String; = "object_token"

.field private static final COUNT_ALL_OBJECTS:Ljava/lang/String; = "SELECT count(*) FROM objects"

.field private static INSTANCE:Lcom/squareup/shared/catalog/ObjectsTable; = null

.field static final NAME:Ljava/lang/String; = "objects"

.field private static final READ_ALL_OBJECTS:Ljava/lang/String; = "SELECT object_blob FROM objects"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/shared/catalog/ObjectsTable;
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable;->INSTANCE:Lcom/squareup/shared/catalog/ObjectsTable;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/ObjectsTable;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable;->INSTANCE:Lcom/squareup/shared/catalog/ObjectsTable;

    .line 50
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable;->INSTANCE:Lcom/squareup/shared/catalog/ObjectsTable;

    return-object v0
.end method


# virtual methods
.method countAllObjects(Lcom/squareup/shared/sql/SQLDatabase;)I
    .locals 2

    const-string v0, "SELECT count(*) FROM objects"

    const/4 v1, 0x0

    .line 228
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/Queries;->intFromCountCursor(Lcom/squareup/shared/sql/SQLCursor;)I

    move-result p1

    return p1
.end method

.method countItemsWithTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation

    .line 232
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->COUNT_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {p1, v0, p2, v1}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 233
    invoke-static {p1}, Lcom/squareup/shared/catalog/Queries;->intFromCountCursor(Lcom/squareup/shared/sql/SQLCursor;)I

    move-result p1

    return p1
.end method

.method countObjectsWithType(Lcom/squareup/shared/sql/SQLDatabase;I)I
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 222
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 223
    sget-object p2, Lcom/squareup/shared/catalog/ObjectsTable$Query;->COUNT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 224
    invoke-static {p1}, Lcom/squareup/shared/catalog/Queries;->intFromCountCursor(Lcom/squareup/shared/sql/SQLCursor;)I

    move-result p1

    return p1
.end method

.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 215
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 216
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_OBJECT_AND_ITEM_TYPE_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 217
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_TYPE_AND_SORT_TEXT_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 218
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_TOKEN_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V
    .locals 1

    .line 237
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->DELETE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 238
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 239
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method insertOrReplace(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/String;[B)V
    .locals 1

    .line 248
    invoke-static {p6}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p6

    .line 250
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 251
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 252
    invoke-virtual {p1, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 253
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 254
    invoke-virtual {p1, p5}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 255
    invoke-virtual {p1, p6}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 256
    invoke-virtual {p1, p7}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindBlob([B)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 257
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method readAllObjects(Lcom/squareup/shared/sql/SQLDatabase;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    const-string v0, "SELECT object_blob FROM objects"

    const/4 v1, 0x0

    .line 261
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method selectObjectById(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 266
    sget-object p2, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECT_BY_ID:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method selectObjectByToken(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 271
    sget-object p2, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECT_BY_TOKEN:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method selectObjectByTokens(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 275
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 278
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 279
    aput-object v3, v0, v2

    move v2, v4

    goto :goto_0

    .line 282
    :cond_0
    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_TOKENS_AND_OBJECTS_BY_TOKENS:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 283
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v1

    .line 284
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    invoke-static {p2}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p2

    const-string v2, "%tokens%"

    invoke-virtual {v1, v2, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 282
    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method selectObjectIds(Lcom/squareup/shared/sql/SQLDatabase;ILjava/util/List;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "I",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 306
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    aput-object p2, v0, v1

    if-eqz p3, :cond_1

    .line 309
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 313
    :cond_0
    sget-object p2, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_FOR_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2, p3, v0}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1

    .line 310
    :cond_1
    :goto_0
    sget-object p2, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_FOR_OBJECTS_OF_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method selectObjectsByIds(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 289
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 291
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 292
    aput-object v3, v0, v2

    move v2, v4

    goto :goto_0

    .line 294
    :cond_0
    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_AND_OBJECTS_BY_IDS:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 295
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    invoke-static {p2}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p2

    const-string v2, "%ids%"

    invoke-virtual {v1, v2, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 294
    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method selectObjectsByType(Lcom/squareup/shared/sql/SQLDatabase;I)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 300
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 301
    sget-object p2, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method
