.class public Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;
.super Ljava/lang/Object;
.source "BatchUpsertCatalogConnectV2ObjectsResult.java"


# instance fields
.field public final idMappings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;"
        }
    .end annotation
.end field

.field public final objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;->objects:Ljava/util/List;

    .line 21
    iput-object p2, p0, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;->idMappings:Ljava/util/List;

    return-void
.end method
