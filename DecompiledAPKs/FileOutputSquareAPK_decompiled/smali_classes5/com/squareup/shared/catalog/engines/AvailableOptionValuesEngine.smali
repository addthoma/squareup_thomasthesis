.class public Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;
.super Ljava/lang/Object;
.source "AvailableOptionValuesEngine.java"


# instance fields
.field private final itemOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end field

.field private observer:Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;

.field private final optionIdToSelectedOptionValueIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final optionValueIdsToOptionIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final optionValueIdsToStates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/engines/ItemOptionValueState;",
            ">;"
        }
    .end annotation
.end field

.field private final variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->validateItemOptionsAndVariations(Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->itemOptions:Ljava/util/List;

    .line 58
    iput-object p2, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->variations:Ljava/util/List;

    .line 59
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToOptionIds:Ljava/util/Map;

    .line 60
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    .line 61
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    .line 63
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 64
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 65
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToOptionIds:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 69
    :cond_1
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->initializeItemOptionValueAvailability()V

    .line 72
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->selectOptionValuesForSingleVariation()V

    return-void

    .line 54
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Item Options and Variations are not valid."

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private findSelectedVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation;
    .locals 6

    .line 295
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->itemOptions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 299
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    .line 300
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 303
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    const/4 v3, 0x1

    .line 305
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 306
    iget-object v5, v5, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v3, 0x0

    :cond_3
    if-eqz v3, :cond_1

    return-object v2

    .line 316
    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "All item options are selected, but no matching variation is found. Selected option values: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " variations: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->variations:Ljava/util/List;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private initializeItemOptionValueAvailability()V
    .locals 5

    .line 200
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->itemOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 201
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 202
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->HIDDEN:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 208
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 209
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    iget-object v2, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    sget-object v4, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-void
.end method

.method private isSelectable(Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 265
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 267
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 268
    iget-object v5, v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 269
    iget-object v3, v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_0

    return v4

    :cond_3
    return v2
.end method

.method private notifyObserver()V
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->observer:Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;

    if-eqz v0, :cond_0

    .line 283
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;->onUpdate(Ljava/util/Map;)V

    .line 286
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->findSelectedVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 288
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->observer:Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;

    invoke-interface {v1, v0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;->onVariationSelected(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    :cond_0
    return-void
.end method

.method private selectOptionValuesForSingleVariation()V
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 217
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->variations:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->selectOptionValuesForVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    :cond_0
    return-void
.end method

.method private selectOptionValuesForVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V
    .locals 4

    .line 223
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 224
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    iget-object v2, v0, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    sget-object v3, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTED:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    iget-object v2, v0, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateItemOptionValueAvailability()V
    .locals 8

    .line 233
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->initializeItemOptionValueAvailability()V

    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->itemOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 239
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 241
    new-instance v3, Ljava/util/LinkedHashMap;

    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    invoke-direct {v3, v4}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 243
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 249
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 250
    iget-object v6, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTED:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 251
    :cond_3
    iget-object v6, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    sget-object v7, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->HIDDEN:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    if-eq v6, v7, :cond_2

    .line 254
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    invoke-direct {p0, v3}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->isSelectable(Ljava/util/Map;)Z

    move-result v6

    .line 256
    iget-object v7, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v5

    if-eqz v6, :cond_4

    .line 257
    sget-object v6, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    goto :goto_1

    :cond_4
    sget-object v6, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->UNSELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    .line 256
    :goto_1
    invoke-interface {v7, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    return-void
.end method

.method private validateItemOptionsAndVariations(Ljava/util/List;Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;)Z"
        }
    .end annotation

    .line 150
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 151
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 153
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 155
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 156
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 162
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 164
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object p2

    .line 166
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 167
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 168
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 169
    iget-object v5, v4, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v4, v4, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 174
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result p2

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    const/4 v5, 0x0

    if-ne p2, v4, :cond_5

    .line 175
    invoke-interface {v0, v2}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result p2

    if-nez p2, :cond_4

    goto :goto_2

    .line 180
    :cond_4
    invoke-interface {v1, v3}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result p2

    if-nez p2, :cond_2

    :cond_5
    :goto_2
    return v5

    :cond_6
    const/4 p1, 0x1

    return p1
.end method

.method private validateOptionValueState(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/ItemOptionValueState;)V
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToStates:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, p2, :cond_0

    return-void

    .line 190
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The option value must be "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public deselectOptionValue(Ljava/lang/String;)V
    .locals 1

    .line 102
    sget-object v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTED:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->validateOptionValueState(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/ItemOptionValueState;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToOptionIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 106
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->updateItemOptionValueAvailability()V

    .line 110
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->notifyObserver()V

    return-void
.end method

.method public selectOptionValue(Ljava/lang/String;)V
    .locals 2

    .line 83
    sget-object v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->validateOptionValueState(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/ItemOptionValueState;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionValueIdsToOptionIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->optionIdToSelectedOptionValueIds:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->updateItemOptionValueAvailability()V

    .line 91
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->notifyObserver()V

    return-void
.end method

.method public selectVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V
    .locals 3

    .line 119
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->variations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->initializeItemOptionValueAvailability()V

    .line 127
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->selectOptionValuesForVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    .line 129
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->updateItemOptionValueAvailability()V

    .line 131
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->notifyObserver()V

    return-void

    .line 120
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 121
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "The provided variation %s was not found."

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setObserver(Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;)V
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->observer:Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;

    .line 137
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->notifyObserver()V

    return-void
.end method

.method public unsetObserver()V
    .locals 1

    const/4 v0, 0x0

    .line 141
    iput-object v0, p0, Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngine;->observer:Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;

    return-void
.end method
