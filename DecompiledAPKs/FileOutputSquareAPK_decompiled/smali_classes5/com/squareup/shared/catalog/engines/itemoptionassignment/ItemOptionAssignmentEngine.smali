.class public Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
.super Ljava/lang/Object;
.source "ItemOptionAssignmentEngine.java"


# instance fields
.field final state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;)V"
        }
    .end annotation

    move-object/from16 v2, p1

    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "existing variations"

    .line 77
    invoke-static {v2, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 79
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 81
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 82
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    invoke-direct {v5, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 85
    :cond_0
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    .line 87
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    move-object v6, v0

    const/4 v0, 0x0

    .line 90
    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ge v0, v7, :cond_6

    .line 92
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 93
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->optionValueCount()I

    move-result v9

    const-string v10, "Variation with id "

    if-nez v0, :cond_1

    .line 96
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getItemId()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 98
    :cond_1
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getItemId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, " is in a different item from the first variation."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 98
    invoke-static {v11, v12}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    :goto_2
    const/4 v11, 0x0

    :goto_3
    if-ge v11, v9, :cond_5

    .line 105
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 107
    iget-object v13, v12, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-interface {v5, v13}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    .line 108
    iget-object v13, v12, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v5, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :cond_2
    iget-object v13, v12, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    .line 111
    invoke-interface {v5, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Set;

    .line 112
    invoke-interface {v13, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-nez v0, :cond_3

    .line 116
    iget-object v12, v12, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-interface {v4, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 120
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v13

    if-ne v13, v9, :cond_4

    const/4 v13, 0x1

    goto :goto_4

    :cond_4
    const/4 v13, 0x0

    :goto_4
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, " has different number of option values as the first variation"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 120
    invoke-static {v13, v14}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 123
    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    iget-object v12, v12, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, " has different order of option values as the first variation"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 123
    invoke-static {v12, v13}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    :goto_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 131
    :cond_6
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_8

    :cond_7
    const/4 v1, 0x1

    :cond_8
    const-string v0, "If the item has no item option before, it could only get item option and value added on POS when it only has one flat variation."

    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 135
    new-instance v8, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    move-object v0, v8

    move-object v1, v6

    move-object/from16 v2, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    return-void
.end method

.method public static create(Ljava/util/List;Ljava/util/List;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;"
        }
    .end annotation

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 60
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 61
    new-instance v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-direct {v2, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 63
    :cond_0
    new-instance p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V

    return-object p0
.end method

.method private findAdditionalValuesByComparingIntendedAndAssignedValues(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;"
        }
    .end annotation

    .line 789
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 790
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 791
    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 792
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private findCombinations(Ljava/util/List;ILjava/util/Map;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation

    .line 849
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "indexOfOptionAtStart out of range"

    invoke-static {v0, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 851
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 853
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 852
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "optionIdsInOrder should be the same set of ids as the key set of availableOptionValueIdPairsByOptionIds"

    .line 851
    invoke-static {v0, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 857
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 858
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 861
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v2

    if-ne p2, v4, :cond_1

    .line 863
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 864
    new-instance p2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {p2, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;-><init>(Ljava/util/List;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    add-int/2addr p2, v2

    .line 868
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->findCombinations(Ljava/util/List;ILjava/util/Map;)Ljava/util/List;

    move-result-object p1

    .line 872
    :goto_1
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 875
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    .line 878
    new-instance v4, Ljava/util/ArrayList;

    .line 879
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 880
    invoke-interface {v4, v1, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 881
    new-instance v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    invoke-direct {v3, v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;-><init>(Ljava/util/List;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    return-object v0
.end method

.method private findMissingValuesByComparingIntendedAndAssignedValues(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;"
        }
    .end annotation

    .line 800
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 801
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 802
    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 803
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 714
    new-instance v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->fromByteArray([B)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;-><init>(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;)V

    return-object v0
.end method

.method private generateCombinationsWhenAddAdditionalOptionAndValues(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string/jumbo v1, "valueAddedToExistingVariations should be non-null."

    .line 767
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 770
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 772
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 773
    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 776
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v2, v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 777
    new-instance v4, Ljava/util/ArrayList;

    .line 778
    invoke-interface {v3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 779
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 780
    new-instance v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    invoke-direct {v3, v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;-><init>(Ljava/util/List;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private generateCombinationsWhenAddValues(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation

    .line 736
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 738
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "intended option values must be within an assigned option."

    .line 740
    invoke-static {v1, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 743
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 746
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v2, v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 748
    invoke-interface {v3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v3

    .line 749
    invoke-interface {v3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 750
    invoke-interface {v3, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 751
    new-instance v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    invoke-direct {v4, v3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;-><init>(Ljava/util/List;)V

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 755
    :cond_2
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    .line 756
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getOptionValueCombinationComparator()Ljava/util/Comparator;

    move-result-object p1

    .line 757
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 758
    invoke-static {p2, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object p2
.end method

.method private generateCombinationsWhenCreatingFromScratch(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation

    .line 722
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "intendedOptionIdsInOrder shouldn\'t be empty when creating variations with options from scratch."

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 725
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "intendedOptionValueIdPairsByOptionIds shouldn\'t be empty when creating variations with options from scratch."

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 728
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    .line 729
    invoke-direct {p0, p1, v1, p2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->findCombinations(Ljava/util/List;ILjava/util/Map;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method static final synthetic lambda$getItemOptionsNotUsedByItem$0$ItemOptionAssignmentEngine(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)I
    .locals 0

    .line 507
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method private reorderOptionIds(Ljava/util/List;Ljava/lang/String;IIZ)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "IIZ)Z"
        }
    .end annotation

    .line 816
    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 818
    invoke-interface {p1, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, p3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "fromIndex should match the index of the optionId in assignedOptionIdsInOrder"

    invoke-static {v0, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 821
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p4, v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    const-string/jumbo v0, "toIndex should not exceed the range of assignedOptionIdsInOrder"

    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 824
    invoke-interface {p1, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 825
    invoke-interface {p1, p4, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    if-eqz p5, :cond_3

    .line 828
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object p1, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p5

    if-eqz p5, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 829
    invoke-interface {p5, p2, p3, p4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->reorderOption(Ljava/lang/String;II)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    goto :goto_1

    .line 831
    :cond_2
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object p1, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    iget-object p2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getOptionValueCombinationComparator()Ljava/util/Comparator;

    move-result-object p2

    invoke-static {p1, p2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_3
    return v2

    :cond_4
    return v1
.end method


# virtual methods
.method public addNewItemOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)V
    .locals 3

    .line 664
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addNewValuesInExistingOption(Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;",
            ">;)V"
        }
    .end annotation

    .line 694
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    .line 696
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->getAllValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, -0x1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 697
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getOrdinal()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-le v3, v1, :cond_0

    .line 698
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getOrdinal()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 702
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 703
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    invoke-direct {v2, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)V

    .line 704
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->setOrdinal(Ljava/lang/Integer;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v0

    .line 703
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->addValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public addOrReplaceIntendedValues(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;)V"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "intended option to add should be among all options."

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 184
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 185
    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "intended values should be in the intended option."

    invoke-static {v1, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    goto :goto_0

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public canGenerateCombinations()Z
    .locals 1

    const/4 v0, 0x0

    .line 440
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->canGenerateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Z

    move-result v0

    return v0
.end method

.method public canGenerateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Z
    .locals 0

    .line 453
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public cleanUpIntendedOptionsAndValues()V
    .locals 1

    .line 462
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 463
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public commitDeletionOfVariationsAndUpdateExistingVariations()V
    .locals 8

    .line 599
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->findAllVariationsToDelete()Ljava/util/List;

    move-result-object v0

    .line 601
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 602
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v2, v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 604
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 606
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 607
    invoke-interface {v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 610
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    .line 611
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 613
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    .line 615
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v4, v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 617
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 618
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 619
    invoke-interface {v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    if-eqz v0, :cond_1

    .line 622
    invoke-interface {v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 623
    iget-object v7, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    .line 624
    invoke-virtual {v7, v6}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->findCatalogOptionValueByOptionValuePair(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v6

    .line 623
    invoke-interface {v5, v6}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->removeOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    goto :goto_1

    .line 627
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    .line 634
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_4
    return-void
.end method

.method public deleteUnusedItemOption(Ljava/lang/String;)V
    .locals 2

    const-string v0, "Item option id"

    .line 674
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 675
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getItemOptionValuesInUseByOptionIds()Ljava/util/Map;

    move-result-object v0

    .line 676
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 677
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "is assigned to the item. You should not delete it."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public findAllVariationsToDelete()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;",
            ">;"
        }
    .end annotation

    .line 362
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    const-string v0, "intendedOptionValueIdPairsByOptionIds should have and only have ONE key when finding variations to delete, because variation deletions should be incremental."

    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 370
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 371
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 375
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 378
    :cond_2
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v2, v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    .line 379
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 381
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 383
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    const-string v5, "doesn\'t match the order of options in the item."

    const-string v6, "The order of values of the variation called "

    if-eqz v4, :cond_4

    .line 391
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 393
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v4, v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 395
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v8

    .line 396
    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    iget-object v9, v9, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    .line 398
    invoke-virtual {v11}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v11

    invoke-interface {v7, v11}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getName(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 396
    invoke-static {v9, v10}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 400
    invoke-interface {v8, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 402
    invoke-interface {v2, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 404
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 407
    :cond_3
    invoke-interface {v2, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 412
    :cond_4
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v4, v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    .line 413
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    .line 415
    invoke-direct {p0, v2, v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->findMissingValuesByComparingIntendedAndAssignedValues(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 417
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v4, v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 419
    invoke-interface {v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 420
    iget-object v9, v8, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    .line 422
    invoke-virtual {v11}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v11

    invoke-interface {v7, v11}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getName(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 420
    invoke-static {v9, v10}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 424
    invoke-interface {v2, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 425
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    return-object v3
.end method

.method public generateCombinations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 239
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public generateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation

    .line 280
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    .line 281
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getSortedIntendedOptionValueIdPairsByOptionIds()Ljava/util/Map;

    move-result-object v0

    .line 284
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    if-nez p1, :cond_0

    const/4 v2, 0x1

    :cond_0
    const-string/jumbo p1, "valueAddedToExistingVariations should be null when there is no intention of option values."

    .line 285
    invoke-static {v2, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 287
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 290
    :cond_1
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    const-string/jumbo p1, "valueAddedToExistingVariations should be null when creating from scratch. Please call generateCombinations()."

    .line 292
    invoke-static {v2, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 295
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object p1, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    const-string v1, "assignedOptionValueIdPairsByOptionIds should be empty when creating from scratch."

    invoke-static {p1, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 298
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object p1, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinationsWhenCreatingFromScratch(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 302
    :cond_3
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :goto_0
    const-string v4, "sortedIntendedOptionValueIdPairsByOptionIds should have and only have ONE key when adding variations incrementally"

    invoke-static {v1, v4}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 306
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 308
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 310
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v4, v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    if-nez p1, :cond_5

    const/4 v2, 0x1

    :cond_5
    const-string/jumbo p1, "valueAddedToExistingVariations should be null. Please call generateCombinations()."

    .line 312
    invoke-static {v2, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 315
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object p1, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    .line 316
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    const-string v2, "assigned option values"

    invoke-static {p1, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    .line 320
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->findAdditionalValuesByComparingIntendedAndAssignedValues(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object p1

    .line 323
    invoke-direct {p0, v1, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinationsWhenAddValues(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 326
    :cond_6
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_7

    const/4 v2, 0x1

    :cond_7
    const-string v1, "intendedOptionIdsInOrder should only have ONE element when adding option to an item."

    invoke-static {v2, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 331
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinationsWhenAddAdditionalOptionAndValues(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public generateVariationNameWithOptionValueCombination(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->getName(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAllItemOptions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    .line 516
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 517
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    .line 518
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getAllVariations()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    .line 643
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 645
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 646
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->getName(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    .line 647
    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 649
    invoke-virtual {v3, v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v3

    .line 650
    invoke-virtual {v3, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v3

    .line 651
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v3

    .line 647
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getItemOptionValuesInUseByOptionIds()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;",
            ">;>;"
        }
    .end annotation

    .line 529
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 531
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 532
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 534
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getSortedIntendedOptionValueIdPairsByOptionIds()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 536
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getSortedIntendedOptionValueIdPairsByOptionIds()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 537
    iget-object v6, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v6, v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->findCatalogOptionValueByOptionValuePair(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 541
    :cond_0
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getSortedAssignedOptionValueIdPairsByOptionIds()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 542
    iget-object v6, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v6, v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->findCatalogOptionValueByOptionValuePair(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 545
    :cond_1
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 548
    :cond_2
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 549
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 551
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getSortedIntendedOptionValueIdPairsByOptionIds()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 552
    iget-object v6, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v6, v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->findCatalogOptionValueByOptionValuePair(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 554
    :cond_3
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    return-object v0
.end method

.method public getItemOptionsNotUsedByItem()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    .line 493
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 494
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 495
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getItemOptionsUsedByItem()Ljava/util/List;

    move-result-object v2

    .line 497
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 498
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 501
    :cond_0
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v2, v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 502
    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 503
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v4, v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 507
    :cond_2
    sget-object v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine$$Lambda$0;->$instance:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public getItemOptionsUsedByItem()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    .line 471
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 473
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 474
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v3, v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 477
    :cond_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 478
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v3, v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Option with id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " is still registered as intended after it had been assigned. You probably forgot to run method #cleanUpIntendedOptionsAndValues()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 482
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v3, v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method public intendToRemoveOptionFromItem(Ljava/lang/String;)V
    .locals 3

    .line 207
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->addOrReplaceIntendedValues(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "option with id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is not in use by the item."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    :goto_0
    return-void
.end method

.method public needsToDeleteVariations()Z
    .locals 1

    .line 343
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->findAllVariationsToDelete()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public reorderOption(Ljava/lang/String;II)V
    .locals 8

    .line 150
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v2, "When performing a reorder, at least one of intendedOptionIdsInOrder and assignedOptionIdsInOrder should be empty."

    invoke-static {v0, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v3, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    const/4 v7, 0x1

    move-object v2, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->reorderOptionIds(Ljava/util/List;Ljava/lang/String;IIZ)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v3, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    const/4 v7, 0x0

    move-object v2, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->reorderOptionIds(Ljava/util/List;Ljava/lang/String;IIZ)Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    const-string p1, "option to be reordered should be in either assignedOptionIdsInOrder, or intendedOptionIdsInOrder."

    .line 166
    invoke-static {v1, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    return-void
.end method

.method public shouldAddOptionToExistingVariations(Ljava/lang/String;)Z
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    iget-object v0, v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public toByteArray()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 710
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public updateExistingVariationsAndCreateNewVariations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;)V"
        }
    .end annotation

    .line 574
    new-instance v0, Ljava/util/HashSet;

    .line 575
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 577
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    .line 578
    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "the chosenCombination named \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    .line 581
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->getName(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\" is not among the combinations generated earlier."

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 578
    invoke-static {v3, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 586
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->updateExistingVariationsWithValue(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)V

    .line 589
    :cond_1
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->state:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->addVariationsAndCommitOptionAssignments(Ljava/util/List;)V

    return-void
.end method
