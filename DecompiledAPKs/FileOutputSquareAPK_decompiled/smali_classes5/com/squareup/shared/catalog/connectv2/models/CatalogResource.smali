.class public final Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;
.super Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
.source "CatalogResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 1

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    .line 21
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    const-string v0, "Resource data"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getResourceDescription()Ljava/lang/String;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;->description:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getResourceName()Ljava/lang/String;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public newBuilder()Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;Lcom/squareup/shared/catalog/connectv2/models/CatalogResource$1;)V

    return-object v0
.end method
