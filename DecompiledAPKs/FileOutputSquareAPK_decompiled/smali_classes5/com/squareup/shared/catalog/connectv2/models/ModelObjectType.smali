.class public interface abstract Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;
.super Ljava/lang/Object;
.source "ModelObjectType.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "TT;>;"
    }
.end annotation


# virtual methods
.method public abstract compareTo(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation
.end method

.method public abstract getStableIdentifier()J
.end method
