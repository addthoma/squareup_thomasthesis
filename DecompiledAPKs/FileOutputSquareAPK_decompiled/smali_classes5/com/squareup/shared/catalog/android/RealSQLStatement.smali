.class Lcom/squareup/shared/catalog/android/RealSQLStatement;
.super Ljava/lang/Object;
.source "RealSQLStatement.java"

# interfaces
.implements Lcom/squareup/shared/sql/SQLStatement;


# instance fields
.field private final statement:Landroid/database/sqlite/SQLiteStatement;


# direct methods
.method constructor <init>(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/shared/catalog/android/RealSQLStatement;->statement:Landroid/database/sqlite/SQLiteStatement;

    return-void
.end method


# virtual methods
.method public bindBlob([BI)V
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLStatement;->statement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0, p2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    return-void
.end method

.method public bindLong(JI)V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLStatement;->statement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0, p3, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    return-void
.end method

.method public bindNull(I)V
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLStatement;->statement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    return-void
.end method

.method public bindString(Ljava/lang/String;I)V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLStatement;->statement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0, p2, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    return-void
.end method

.method public clearBindings()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLStatement;->statement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    return-void
.end method

.method public execute()V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLStatement;->statement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    return-void
.end method

.method public executeUpdateDelete()I
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLStatement;->statement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v0

    return v0
.end method
