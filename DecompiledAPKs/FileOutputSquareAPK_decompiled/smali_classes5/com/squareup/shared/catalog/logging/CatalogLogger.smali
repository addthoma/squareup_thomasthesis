.class public interface abstract Lcom/squareup/shared/catalog/logging/CatalogLogger;
.super Ljava/lang/Object;
.source "CatalogLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;
    }
.end annotation


# virtual methods
.method public abstract debug(Ljava/lang/String;)V
.end method

.method public abstract error(Ljava/lang/Throwable;Ljava/lang/String;)V
.end method

.method public abstract remoteLog(Ljava/lang/Throwable;)V
.end method

.method public abstract remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V
.end method

.method public abstract warn(Ljava/lang/String;)V
.end method
