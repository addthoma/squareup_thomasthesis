.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;
.super Ljava/lang/Object;
.source "ItemVariationLookupTableReader.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SkuLookupInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final isSingleVariation:Z

.field public final itemId:Ljava/lang/String;

.field public final itemName:Ljava/lang/String;

.field public final sku:Ljava/lang/String;

.field public final type:Lcom/squareup/api/items/Item$Type;

.field public final variationId:Ljava/lang/String;

.field public final variationName:Ljava/lang/String;

.field public final variationOrdinal:I

.field public final variationPrice:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/Item$Type;IZ)V
    .locals 0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->sku:Ljava/lang/String;

    .line 226
    iput-object p2, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    .line 227
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationId:Ljava/lang/String;

    .line 228
    iput-object p4, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemName:Ljava/lang/String;

    .line 229
    iput-object p5, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationName:Ljava/lang/String;

    .line 230
    iput-object p6, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationPrice:Lcom/squareup/protos/common/Money;

    .line 231
    iput-object p7, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->type:Lcom/squareup/api/items/Item$Type;

    .line 232
    iput p8, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationOrdinal:I

    .line 233
    iput-boolean p9, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->isSingleVariation:Z

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)I
    .locals 3

    .line 237
    invoke-static {}, Lcom/squareup/shared/catalog/utils/ChainComparison;->start()Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemName:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemName:Ljava/lang/String;

    .line 238
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/ChainComparison;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    .line 239
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/ChainComparison;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object v0

    iget v1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationOrdinal:I

    iget v2, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationOrdinal:I

    .line 240
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/ChainComparison;->compare(II)Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationName:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationName:Ljava/lang/String;

    .line 241
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/ChainComparison;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationId:Ljava/lang/String;

    .line 242
    invoke-virtual {v0, v1, p1}, Lcom/squareup/shared/catalog/utils/ChainComparison;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/shared/catalog/utils/ChainComparison;

    move-result-object p1

    .line 243
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/utils/ChainComparison;->result()I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 210
    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->compareTo(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)I

    move-result p1

    return p1
.end method
