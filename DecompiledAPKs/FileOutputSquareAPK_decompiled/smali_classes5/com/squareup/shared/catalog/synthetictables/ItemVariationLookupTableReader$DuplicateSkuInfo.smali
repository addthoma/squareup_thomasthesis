.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;
.super Ljava/lang/Object;
.source "ItemVariationLookupTableReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DuplicateSkuInfo"
.end annotation


# instance fields
.field private final skuLookupInfosByNormalizedSkus:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final skusByNormalizedSkus:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    .line 263
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skusByNormalizedSkus:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)V
    .locals 0

    .line 254
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->addSkuLookupInfo(Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;)V
    .locals 0

    .line 254
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->merge(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;)V

    return-void
.end method

.method private addSkuLookupInfo(Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)V
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private merge(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;)V
    .locals 4

    .line 273
    iget-object v0, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 274
    iget-object v2, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 275
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SKU "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " exists in both "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " and "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 279
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public getAllDuplicateNormalizedSkus()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 287
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 288
    iget-object v1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 289
    iget-object v3, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    .line 290
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getSkuLookupInfosByItemIdsForNormalizedSku(Ljava/lang/String;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;>;"
        }
    .end annotation

    .line 304
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 305
    iget-object v1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->skuLookupInfosByNormalizedSkus:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_1

    .line 307
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 308
    iget-object v2, v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 309
    iget-object v2, v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    :cond_0
    iget-object v2, v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method
