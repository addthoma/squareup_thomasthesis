.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;
.super Ljava/lang/Object;
.source "ItemVariationLookupTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;
    }
.end annotation


# static fields
.field static final COLUMN_ITEM_ID:Ljava/lang/String; = "item_id"

.field static final COLUMN_SKU:Ljava/lang/String; = "sku"

.field static final COLUMN_VARIATION_ID:Ljava/lang/String; = "variation_id"

.field static final COLUMN_VARIATION_NAME:Ljava/lang/String; = "variation_name"

.field static final COLUMN_VARIATION_ORDINAL:Ljava/lang/String; = "variation_ordinal"

.field static final COLUMN_VARIATION_PRICE_AMOUNT:Ljava/lang/String; = "variation_price_amount"

.field static final COLUMN_VARIATION_PRICE_CURRENCY:Ljava/lang/String; = "variation_price_currency"

.field static final NAME:Ljava/lang/String; = "variation_lookup"

.field private static final VERSION:J = 0x3L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private deleteByItemId(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V
    .locals 1

    .line 192
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->DELETE_BY_ITEM_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 193
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private deleteByVariationId(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V
    .locals 1

    .line 198
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->DELETE_BY_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 199
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 200
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private getSkuForVariationId(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 206
    sget-object p2, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->LOOKUP_SKU_FOR_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 210
    :try_start_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 211
    invoke-interface {p1, v1}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 214
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p2

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p2
.end method

.method private updateItemVariationLookup(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;>;Z)V"
        }
    .end annotation

    .line 165
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 166
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 167
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getSku()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez p3, :cond_1

    .line 169
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;->deleteByVariationId(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_0

    .line 172
    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;->upsert(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private upsert(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V
    .locals 3

    .line 179
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 180
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 181
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getSku()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 182
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 183
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 184
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v2, v1

    goto :goto_0

    .line 185
    :cond_0
    iget-object v2, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    :goto_0
    invoke-virtual {p1, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    if-nez v0, :cond_1

    goto :goto_1

    .line 186
    :cond_1
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0}, Lcom/squareup/protos/common/CurrencyCode;->getValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    invoke-virtual {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 187
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getOrdinal()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method


# virtual methods
.method public applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/DeletedCatalogObjects;)V
    .locals 4

    .line 145
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedItemIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedItemIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 147
    invoke-direct {p0, p1, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;->deleteByItemId(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_0
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 152
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 153
    iget-object v2, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 155
    iget-object v3, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedItemIds:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 156
    invoke-direct {p0, p1, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;->deleteByVariationId(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V
    .locals 0

    .line 138
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_0

    .line 139
    iget-object p2, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;->updateItemVariationLookup(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V

    :cond_0
    return-void
.end method

.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 131
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 132
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE_ITEM_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 133
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE_SKU_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public tableName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "variation_lookup"

    return-object v0
.end method

.method public tableVersion()J
    .locals 2

    const-wide/16 v0, 0x3

    return-wide v0
.end method
