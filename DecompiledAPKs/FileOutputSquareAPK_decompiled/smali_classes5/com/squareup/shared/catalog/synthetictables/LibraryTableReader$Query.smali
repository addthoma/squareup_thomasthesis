.class public final enum Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;
.super Ljava/lang/Enum;
.source "LibraryTableReader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum COUNT_ITEMS_AND_DISCOUNTS_FOR_SEARCH_TERM_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum COUNT_ITEMS_FOR_ALL_NON_EMPTY_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum COUNT_ITEMS_IN_CATEGORY_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum COUNT_OBJECTS_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum FIND_ITEMS_FOR_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_ALL_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_ALL_CATEGORY_DISCOUNT_ITEM_ORDER_BY_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_ALL_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_ALL_ITEMS_AVAILABLE_FOR_PICKUP:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_ALL_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_ALL_MODIFIER_LISTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_ALL_NONEMPTY_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_ALL_TICKET_GROUPS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum READ_OBJECTS_WITH_TYPES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_FOR_ITEMS_OR_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_WITH_ITEM_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum WORD_PREFIX_SEARCH_BY_NAME_FOR_ITEMS_OR_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum WORD_PREFIX_SEARCH_BY_NAME_WITH_ITEM_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum WORD_PREFIX_SEARCH_BY_NAME_WITH_OBJECT_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_AND_VARIATION_NAME_IN_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

.field public static final enum WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_IN_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .line 129
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const-string v1, "SELECT {category_id}, count(*) FROM {table} WHERE {name} IS NOT NULL AND {object_type} = \'{item}\' AND {item_type} IN (%item_types%) GROUP BY {category_id}"

    .line 130
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "library"

    const-string v3, "table"

    .line 135
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "category_id"

    .line 136
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "name"

    .line 137
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "object_type"

    .line 138
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "item_type"

    .line 139
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v8, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 140
    invoke-static {v8}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "item"

    invoke-virtual {v1, v9, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 141
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 142
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "COUNT_ITEMS_FOR_ALL_NON_EMPTY_CATEGORIES"

    const/4 v10, 0x0

    invoke-direct {v0, v8, v10, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_FOR_ALL_NON_EMPTY_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 144
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const-string v1, "SELECT {collation_section_index}, count(*) FROM {table} WHERE {object_type} IN (%object_types%) OR (   {object_type} = \'{item}\' AND    {item_type} IN (%item_types%)    )GROUP BY {collation_section_index} ORDER BY {collation_section_index}"

    .line 145
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v8, "collation_section_index"

    .line 154
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 155
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 156
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v10, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 157
    invoke-static {v10}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 158
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 160
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "COUNT_OBJECTS_FOR_COLLATION_SECTION_INDEX"

    const/4 v11, 0x1

    invoke-direct {v0, v10, v11, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_OBJECTS_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 162
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const-string v1, "SELECT {collation_section_index}, count(*) FROM {table} WHERE {category_id} = ? AND {search_words} IS NOT NULL AND {object_type} = \'{item}\' AND {item_type} IN (%item_types%) GROUP BY {collation_section_index} ORDER BY {collation_section_index}"

    .line 163
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 169
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 170
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 171
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "search_words"

    .line 172
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 173
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 174
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v9, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 175
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 176
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 177
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "COUNT_ITEMS_IN_CATEGORY_FOR_COLLATION_SECTION_INDEX"

    const/4 v12, 0x2

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_IN_CATEGORY_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 179
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const-string v1, "SELECT {collation_section_index}, count(DISTINCT object_id) FROM {table} LEFT OUTER JOIN {sku_table} ON {library_item_id} = {item_id} WHERE      ({sku} LIKE ? ESCAPE \'^\' COLLATE NOCASE OR ((%word_predicates%)))      AND ({object_type} = {discount}            OR ({object_type} = \'{item}\' AND {item_type} IN (%item_types%))) GROUP BY {collation_section_index} ORDER BY {collation_section_index}"

    .line 180
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 189
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 190
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v11, "variation_lookup"

    const-string v12, "sku_table"

    .line 191
    invoke-virtual {v1, v12, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "library_item_id"

    const-string v14, "object_id"

    .line 192
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v15, "item_id"

    .line 193
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v16, v11

    const-string v11, "sku"

    .line 194
    invoke-virtual {v1, v11, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 195
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v17, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-object/from16 v18, v12

    .line 196
    invoke-static/range {v17 .. v17}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v17, v11

    const-string v11, "discount"

    invoke-virtual {v1, v11, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 197
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v9, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 198
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 199
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 200
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "COUNT_ITEMS_AND_DISCOUNTS_FOR_SEARCH_TERM_FOR_COLLATION_SECTION_INDEX"

    const/4 v12, 0x3

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_AND_DISCOUNTS_FOR_SEARCH_TERM_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 202
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 203
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "WHERE {category_id} = ? AND {search_words} IS NOT NULL AND {object_type} = \'{item}\' AND {item_type} IN (%item_types%) ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 207
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 208
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 209
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 210
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 211
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v9, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 212
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 213
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 214
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "FIND_ITEMS_FOR_CATEGORY"

    const/4 v12, 0x4

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->FIND_ITEMS_FOR_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 216
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 217
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "WHERE {search_words} IS NOT NULL AND ({object_type} = \'{discount}\' OR ({object_type} = \'{item}\' AND {item_type} IN (%item_types%)) OR {object_type} = \'{category}\') ORDER BY {object_type}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 223
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 224
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 225
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "discount"

    invoke-virtual {v1, v12, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 226
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v9, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 227
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "category"

    invoke-virtual {v1, v12, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 228
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 229
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 230
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "READ_ALL_CATEGORY_DISCOUNT_ITEM_ORDER_BY_TYPE"

    const/4 v12, 0x5

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_CATEGORY_DISCOUNT_ITEM_ORDER_BY_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 232
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 233
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$100()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "WHERE {search_words} IS NOT NULL AND {object_type} = \'{category}\' ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 236
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 237
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 238
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "category"

    invoke-virtual {v1, v12, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 239
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 240
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 241
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "READ_ALL_CATEGORIES"

    const/4 v12, 0x6

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 243
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$200()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "FROM (SELECT * FROM {table} WHERE {object_type} = \'{category}\') JOIN (SELECT {category_id} as itemCategoryId       FROM {table}       WHERE {object_type} = \'{item}\' AND {name} IS NOT NULL       AND {item_type} IN (%item_types%)) ON {id} = itemCategoryId GROUP BY {id} ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 253
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "category"

    invoke-virtual {v1, v12, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 254
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v11, "id"

    .line 255
    invoke-virtual {v1, v11, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v11, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 256
    invoke-static {v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v9, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 257
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 258
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 259
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 260
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 261
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 262
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 263
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 264
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "READ_ALL_NONEMPTY_CATEGORIES"

    const/4 v3, 0x7

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_NONEMPTY_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 266
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 267
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$100()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "WHERE {search_words} IS NOT NULL AND {object_type} = \'{discount}\' ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 270
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 271
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 272
    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "discount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 273
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 274
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 275
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "READ_ALL_DISCOUNTS"

    const/16 v3, 0x8

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 277
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 278
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "WHERE {search_words} IS NOT NULL AND {object_type} = \'{item}\' AND {item_type} IN (%item_types%) ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 282
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 283
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 284
    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v9, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 285
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 286
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 287
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 288
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "READ_ALL_ITEMS_WITH_TYPES"

    const/16 v3, 0x9

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 290
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 291
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "WHERE {search_words} IS NOT NULL AND {object_type} = \'{item}\' AND {available_for_pickup} = 1 ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 295
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 296
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 297
    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v9, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "available_for_pickup"

    const-string v3, "available_for_pickup"

    .line 298
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 299
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 300
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 301
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "READ_ALL_ITEMS_AVAILABLE_FOR_PICKUP"

    const/16 v3, 0xa

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_ITEMS_AVAILABLE_FOR_PICKUP:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 303
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 304
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$100()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "WHERE {name} IS NOT NULL AND {object_type} = \'{item_modifier_list}\' ORDER BY {ordinal}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 307
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "ordinal"

    const-string v3, "ordinal"

    .line 308
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 309
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 310
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 311
    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "item_modifier_list"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 312
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 313
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "READ_ALL_MODIFIER_LISTS"

    const/16 v3, 0xb

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_MODIFIER_LISTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 315
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 316
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$100()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "WHERE {name} IS NOT NULL AND {object_type} = \'{ticket_group}\' ORDER BY {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 319
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 320
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 321
    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "ticket_group"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 322
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 323
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 324
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "READ_ALL_TICKET_GROUPS"

    const/16 v3, 0xc

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_TICKET_GROUPS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 326
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 327
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "WHERE {object_type} IN (%object_types%) OR (   {object_type} = {item} AND    {item_type} IN (%item_types%)    )ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 334
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 335
    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v9, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 336
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 337
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 338
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 339
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 340
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "READ_OBJECTS_WITH_TYPES"

    const/16 v3, 0xd

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_OBJECTS_WITH_TYPES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 342
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 343
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "LEFT OUTER JOIN {sku_and_name_table} ON {library_item_id} = {item_id} WHERE ({object_type} = ? OR {object_type} = ?) AND (      {sku} LIKE ? ESCAPE \'^\' COLLATE NOCASE      OR ((%word_predicates%))      OR ((%variation_word_predicates%)) ) AND (      {item_type} IS NULL    OR      ({item_type} IN (%item_types%))) GROUP BY {library_item_id} ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 359
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "sku_and_name_table"

    const-string/jumbo v3, "variation_sku_name"

    .line 360
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 361
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 362
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v2, v17

    .line 363
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 364
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 365
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 366
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 367
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 368
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_FOR_ITEMS_OR_DISCOUNTS"

    const/16 v5, 0xe

    invoke-direct {v0, v3, v5, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_FOR_ITEMS_OR_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 370
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 371
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "LEFT OUTER JOIN {sku_table} ON {library_item_id} = {item_id} WHERE ({object_type} = ? OR {object_type} = ?) AND ({sku} LIKE ? ESCAPE \'^\' COLLATE NOCASE OR ((%word_predicates%))) AND (     {item_type} IS NULL    OR      ({item_type} IN (%item_types%))) GROUP BY {library_item_id} ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 383
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v3, v16

    move-object/from16 v5, v18

    .line 384
    invoke-virtual {v1, v5, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 385
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 386
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 387
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 388
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 389
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 390
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 391
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 392
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "WORD_PREFIX_SEARCH_BY_NAME_FOR_ITEMS_OR_DISCOUNTS"

    const/16 v12, 0xf

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_FOR_ITEMS_OR_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 394
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 395
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "LEFT OUTER JOIN {sku_and_name_table} ON {library_item_id} = {item_id} WHERE      {object_type} = ?      AND (          {sku} LIKE ? ESCAPE \'^\' COLLATE NOCASE          OR ((%word_predicates%))          OR ((%variation_word_predicates%))      )      AND {item_type} IN (%item_types%) GROUP BY {library_item_id} ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 407
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v11, "sku_and_name_table"

    const-string/jumbo v12, "variation_sku_name"

    .line 408
    invoke-virtual {v1, v11, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 409
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 410
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 411
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 412
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 413
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 414
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 415
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 416
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_WITH_ITEM_TYPE"

    const/16 v12, 0x10

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_WITH_ITEM_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 418
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 419
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "LEFT OUTER JOIN {sku_table} ON {library_item_id} = {item_id} WHERE      {object_type} = ?      AND ({sku} LIKE ? ESCAPE \'^\' COLLATE NOCASE OR ((%word_predicates%)))      AND {item_type} IN (%item_types%) GROUP BY {library_item_id} ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 427
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 428
    invoke-virtual {v1, v5, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 429
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 430
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 431
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 432
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 433
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 434
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 435
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 436
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "WORD_PREFIX_SEARCH_BY_NAME_WITH_ITEM_TYPE"

    const/16 v12, 0x11

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_WITH_ITEM_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 438
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 439
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "WHERE {object_type} = ? AND (%word_predicates%) ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE ASC"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 443
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 444
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 445
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 446
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 447
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "WORD_PREFIX_SEARCH_BY_NAME_WITH_OBJECT_TYPE"

    const/16 v12, 0x12

    invoke-direct {v0, v11, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_WITH_OBJECT_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 449
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 450
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "LEFT OUTER JOIN {sku_table} ON {library_item_id} = {item_id} WHERE      {category_id} = ?      AND ({sku} LIKE ? ESCAPE \'^\' COLLATE NOCASE OR ((%word_predicates%)))      AND {object_type} = \'{item}\'      AND {item_type} IN (%item_types%) GROUP BY {library_item_id} ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 459
    invoke-virtual {v1, v5, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 460
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 461
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 462
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 463
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 464
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 465
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 466
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 467
    invoke-static {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v9, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 468
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 469
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 470
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_IN_CATEGORY"

    const/16 v5, 0x13

    invoke-direct {v0, v3, v5, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_IN_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 472
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 473
    invoke-static {}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->access$000()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "LEFT OUTER JOIN {sku_and_name_table} ON {library_item_id} = {item_id} WHERE      {category_id} = ?      AND (         {sku} LIKE ? ESCAPE \'^\' COLLATE NOCASE          OR ((%word_predicates%))          OR ((%variation_word_predicates%))      )      AND {object_type} = \'{item}\'      AND {item_type} IN (%item_types%) GROUP BY {library_item_id} ORDER BY {collation_section_index}, {search_words} COLLATE NOCASE"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v3, "sku_and_name_table"

    const-string/jumbo v5, "variation_sku_name"

    .line 486
    invoke-virtual {v1, v3, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 487
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 488
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 489
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 490
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 491
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 492
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 493
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 494
    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v9, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 495
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 496
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 497
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_AND_VARIATION_NAME_IN_CATEGORY"

    const/16 v3, 0x14

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_AND_VARIATION_NAME_IN_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    .line 128
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_FOR_ALL_NON_EMPTY_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_OBJECTS_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_IN_CATEGORY_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->COUNT_ITEMS_AND_DISCOUNTS_FOR_SEARCH_TERM_FOR_COLLATION_SECTION_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->FIND_ITEMS_FOR_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_CATEGORY_DISCOUNT_ITEM_ORDER_BY_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_NONEMPTY_CATEGORIES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_ITEMS_AVAILABLE_FOR_PICKUP:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_MODIFIER_LISTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_ALL_TICKET_GROUPS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->READ_OBJECTS_WITH_TYPES:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_FOR_ITEMS_OR_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_FOR_ITEMS_OR_DISCOUNTS:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_AND_VARIATION_NAME_WITH_ITEM_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_WITH_ITEM_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_BY_NAME_WITH_OBJECT_TYPE:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_IN_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->WORD_PREFIX_SEARCH_FOR_ITEMS_BY_NAME_AND_VARIATION_NAME_IN_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 501
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 502
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;
    .locals 1

    .line 128
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;
    .locals 1

    .line 128
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 506
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$Query;->query:Ljava/lang/String;

    return-object v0
.end method
