.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;
.super Ljava/lang/Object;
.source "ItemVariationLookupTableReader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;,
        Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;,
        Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;
    }
.end annotation


# static fields
.field private static ITEM_TYPE_NOT_AVAILABLE:Lcom/squareup/api/items/Item$Type;

.field private static PRICE_NOT_AVAILABLE:Lcom/squareup/protos/common/Money;


# instance fields
.field private helper:Lcom/squareup/shared/sql/DatabaseHelper;

.field private final sourceTables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;Lcom/squareup/shared/catalog/synthetictables/LibraryTable;)V
    .locals 2

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 88
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->sourceTables:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public findDuplicateSkuInfoForItem(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;"
        }
    .end annotation

    .line 190
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;-><init>()V

    .line 194
    new-instance v1, Ljava/util/HashSet;

    .line 195
    invoke-interface {p5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 196
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Ljava/lang/String;

    move-object v3, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    .line 197
    invoke-virtual/range {v3 .. v9}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->findDuplicateSkuInfoForNormalizedSku(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;

    move-result-object v2

    .line 204
    invoke-static {v0, v2}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->access$100(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public findDuplicateSkuInfoForNormalizedSku(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;"
        }
    .end annotation

    move-object/from16 v0, p1

    .line 150
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;

    invoke-direct {v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;-><init>()V

    .line 151
    invoke-static/range {p1 .. p1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    .line 154
    :cond_0
    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v7, v3

    check-cast v7, Ljava/lang/String;

    move-object/from16 v3, p5

    .line 155
    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v9, v4

    check-cast v9, Ljava/lang/String;

    move-object/from16 v14, p6

    .line 156
    invoke-interface {v14, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v15, v4

    check-cast v15, Ljava/lang/String;

    .line 157
    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 158
    new-instance v13, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    sget-object v10, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->PRICE_NOT_AVAILABLE:Lcom/squareup/protos/common/Money;

    sget-object v11, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->ITEM_TYPE_NOT_AVAILABLE:Lcom/squareup/api/items/Item$Type;

    sget-object v4, Lcom/squareup/api/items/ItemVariation;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    .line 161
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    const/16 v16, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    const/16 v16, 0x0

    :goto_1
    move-object v4, v13

    move-object v5, v15

    move-object/from16 v6, p2

    move-object/from16 v8, p3

    move-object/from16 v17, v2

    move-object v2, v13

    move/from16 v13, v16

    invoke-direct/range {v4 .. v13}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/Item$Type;IZ)V

    .line 158
    invoke-static {v1, v15, v2}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->access$000(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)V

    goto :goto_2

    :cond_2
    move-object/from16 v17, v2

    :goto_2
    move-object/from16 v2, v17

    goto :goto_0

    :cond_3
    move-object/from16 v2, p0

    move-object/from16 v4, p4

    .line 165
    invoke-virtual {v2, v0, v4}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->getInfoForSku(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 166
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 171
    iget-object v5, v4, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    move-object/from16 v6, p2

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 172
    invoke-static {v1, v0, v4}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->access$000(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)V

    goto :goto_3

    :cond_5
    return-object v1
.end method

.method public getInfoForSku(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    .line 104
    iget-object v0, v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 106
    sget-object v5, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->LOOKUP_ITEM_FOR_SKU:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v6, p2

    invoke-static {v0, v5, v6, v3}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v3

    .line 108
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 109
    :goto_0
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 110
    invoke-interface {v3, v2}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x2

    .line 111
    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v5, 0x3

    .line 112
    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v5, 0x4

    .line 113
    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v5, 0x5

    .line 115
    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->isNull(I)Z

    move-result v6

    const/4 v11, 0x0

    if-eqz v6, :cond_0

    move-object v5, v11

    goto :goto_1

    :cond_0
    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    :goto_1
    if-eqz v5, :cond_1

    const/4 v6, 0x6

    .line 117
    invoke-interface {v3, v6}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 118
    new-instance v11, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v11}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    invoke-virtual {v11, v5}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v5

    .line 119
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/squareup/protos/common/CurrencyCode;->fromValue(I)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v6

    const-string v11, "currencyCode"

    invoke-static {v6, v11}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v5, v6}, Lcom/squareup/protos/common/Money$Builder;->currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v5

    .line 120
    invoke-virtual {v5}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object v5

    move-object v11, v5

    :cond_1
    const/4 v5, 0x7

    .line 122
    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/squareup/api/items/Item$Type;->fromValue(I)Lcom/squareup/api/items/Item$Type;

    move-result-object v12

    const/16 v5, 0x8

    .line 123
    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v13

    const/16 v5, 0x9

    .line 124
    invoke-interface {v3, v5}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v5

    if-ne v5, v2, :cond_2

    const/4 v14, 0x1

    goto :goto_2

    :cond_2
    const/4 v14, 0x0

    .line 125
    :goto_2
    new-instance v15, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    move-object v5, v15

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v14}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/Item$Type;IZ)V

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 129
    :cond_3
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw v0
.end method

.method public onRegistered(Lcom/squareup/shared/sql/DatabaseHelper;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    return-void
.end method

.method public sourceTables()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->sourceTables:Ljava/util/List;

    return-object v0
.end method
