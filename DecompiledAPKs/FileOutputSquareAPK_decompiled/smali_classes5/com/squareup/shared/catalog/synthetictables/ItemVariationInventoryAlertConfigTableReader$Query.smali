.class public final enum Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;
.super Ljava/lang/Enum;
.source "ItemVariationInventoryAlertConfigTableReader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

.field public static final enum ALL_VARIATION_ALERT_CONFIGS_UPDATED_AFTER_TIMESTAMP:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 29
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    const-string v1, "SELECT {variation_id}, {variation_token}, {item_id}, {tracking_state}, {alert_type}, {alert_threshold}, {deleted}, {server_timestamp} FROM {table} WHERE {server_timestamp} > ?"

    .line 30
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "table"

    const-string/jumbo v3, "variation_alert_config"

    .line 34
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_id"

    .line 35
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_token"

    .line 36
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "item_id"

    .line 37
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "tracking_state"

    const-string v3, "inventory_tracking_state"

    .line 38
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "alert_type"

    const-string v3, "inventory_alert_type"

    .line 39
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "alert_threshold"

    const-string v3, "inventory_alert_threshold"

    .line 40
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "deleted"

    .line 41
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "server_timestamp"

    .line 42
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 43
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 44
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "ALL_VARIATION_ALERT_CONFIGS_UPDATED_AFTER_TIMESTAMP"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->ALL_VARIATION_ALERT_CONFIGS_UPDATED_AFTER_TIMESTAMP:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    .line 28
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->ALL_VARIATION_ALERT_CONFIGS_UPDATED_AFTER_TIMESTAMP:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->query:Ljava/lang/String;

    return-object v0
.end method
