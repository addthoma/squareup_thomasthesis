.class public final Lcom/squareup/squid/SquareDeviceApps$SquareDeviceAppsHelper;
.super Ljava/lang/Object;
.source "SquareDeviceApps.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceApps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SquareDeviceAppsHelper"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceApps$SquareDeviceAppsHelper;",
        "",
        "()V",
        "SQUARE_DEVICE_PLACEHOLDER_ID",
        "",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/squid/SquareDeviceApps$SquareDeviceAppsHelper;

.field public static final SQUARE_DEVICE_PLACEHOLDER_ID:Ljava/lang/String; = "this.is.not.an.app.id"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/squid/SquareDeviceApps$SquareDeviceAppsHelper;

    invoke-direct {v0}, Lcom/squareup/squid/SquareDeviceApps$SquareDeviceAppsHelper;-><init>()V

    sput-object v0, Lcom/squareup/squid/SquareDeviceApps$SquareDeviceAppsHelper;->INSTANCE:Lcom/squareup/squid/SquareDeviceApps$SquareDeviceAppsHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
