.class public Lcom/squareup/ui/StatusBarHelper;
.super Lmortar/Presenter;
.source "StatusBarHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/container/ContainerActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private statusBarHeight:I


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    const/4 v0, -0x1

    .line 23
    iput v0, p0, Lcom/squareup/ui/StatusBarHelper;->statusBarHeight:I

    return-void
.end method

.method private computeStatusBarHeight()I
    .locals 6

    .line 40
    invoke-virtual {p0}, Lcom/squareup/ui/StatusBarHelper;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivity;

    invoke-interface {v0}, Lcom/squareup/container/ContainerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 42
    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_subtext:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 43
    sget v2, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 44
    sget v3, Lcom/squareup/marin/R$dimen;->marin_gap_tiny:I

    .line 45
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    const-string v3, "status_bar_height"

    const-string v4, "dimen"

    const-string v5, "android"

    .line 49
    invoke-virtual {v0, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    .line 51
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 52
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :cond_0
    return v1
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/container/ContainerActivity;)Lmortar/bundler/BundleService;
    .locals 0

    .line 29
    invoke-interface {p1}, Lcom/squareup/container/ContainerActivity;->getBundleService()Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/container/ContainerActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/StatusBarHelper;->extractBundleService(Lcom/squareup/container/ContainerActivity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public getStatusBarHeight()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/squareup/ui/StatusBarHelper;->statusBarHeight:I

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 34
    iget p1, p0, Lcom/squareup/ui/StatusBarHelper;->statusBarHeight:I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/StatusBarHelper;->computeStatusBarHeight()I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/StatusBarHelper;->statusBarHeight:I

    :cond_0
    return-void
.end method

.method public setStatusBarColor(I)V
    .locals 2

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/StatusBarHelper;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/StatusBarHelper;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivity;

    invoke-interface {v0}, Lcom/squareup/container/ContainerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 64
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    return-void

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/StatusBarHelper;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivity;

    invoke-interface {v0}, Lcom/squareup/container/ContainerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->setStatusBarColor(I)V

    return-void
.end method
