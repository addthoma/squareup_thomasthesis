.class public Lcom/squareup/ui/addressbook/PickAddressBookContactView;
.super Landroid/widget/LinearLayout;
.source "PickAddressBookContactView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

.field private emptyView:Lcom/squareup/ui/EmptyView;

.field private header:Landroid/view/ViewGroup;

.field presenter:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchEditText:Lcom/squareup/ui/XableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    const-class p2, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Component;->inject(Lcom/squareup/ui/addressbook/PickAddressBookContactView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/addressbook/PickAddressBookContactView;)Lcom/squareup/stickylistheaders/StickyListHeadersListView;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 338
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 339
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->contacts_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    iput-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    .line 340
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->contacts_empty_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/EmptyView;

    iput-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    .line 341
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->contacts_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->header:Landroid/view/ViewGroup;

    .line 342
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->contacts_search:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->searchEditText:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method private closeCursor()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getAdapter()Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    move-result-object v0

    check-cast v0, Landroidx/cursoradapter/widget/CursorAdapter;

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {v0}, Landroidx/cursoradapter/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method


# virtual methods
.method contactsLoaded(Landroid/database/MatrixCursor;Lcom/squareup/CountryCode;)V
    .locals 2

    .line 108
    invoke-virtual {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 109
    new-instance v1, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;

    invoke-direct {v1, v0, p1, p2}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;-><init>(Landroid/content/Context;Landroid/database/MatrixCursor;Lcom/squareup/CountryCode;)V

    if-eqz p1, :cond_0

    .line 111
    new-instance p2, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactView$d18l5bjHFKMGAmAWcGjKAoa4WHo;

    invoke-direct {p2, p0, v0}, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactView$d18l5bjHFKMGAmAWcGjKAoa4WHo;-><init>(Lcom/squareup/ui/addressbook/PickAddressBookContactView;Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->setFilterQueryProvider(Landroid/widget/FilterQueryProvider;)V

    :cond_0
    if-eqz p1, :cond_1

    .line 116
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->getCount()I

    move-result p1

    if-lez p1, :cond_1

    .line 118
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 119
    iget-object p2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Lcom/squareup/ui/EmptyView;->setGlyphVisibility(I)V

    .line 120
    iget-object p2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/EmptyView;->setTitleVisibility(I)V

    .line 121
    iget-object p2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    const/16 v0, 0x30

    invoke-virtual {p2, v0}, Lcom/squareup/ui/EmptyView;->setGravity(I)V

    .line 122
    iget-object p2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    const/4 v0, 0x0

    invoke-virtual {p2, v0, p1, v0, v0}, Lcom/squareup/ui/EmptyView;->setMessagePadding(IIII)V

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    sget p2, Lcom/squareup/crmupdatecustomer/R$string;->invoice_no_search_results_message:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/EmptyView;->setMessage(I)V

    .line 126
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    iget-object p2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    invoke-virtual {p1, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setEmptyView(Landroid/view/View;)V

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-virtual {p1, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setAdapter(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    return-void
.end method

.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getSearchText()Ljava/lang/CharSequence;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->searchEditText:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$contactsLoaded$2$PickAddressBookContactView(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->presenter:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->runSearchQuery(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onAttachedToWindow$0$PickAddressBookContactView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-virtual {p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getAdapter()Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;

    .line 70
    invoke-virtual {p1, p3}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getName(I)Ljava/lang/String;

    move-result-object p2

    .line 71
    invoke-virtual {p1, p3}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getEmail(I)Ljava/lang/String;

    move-result-object p4

    .line 72
    invoke-virtual {p1, p3}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getPhoneNumber(I)Ljava/lang/String;

    move-result-object p5

    .line 73
    invoke-virtual {p1, p3}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getPhoto(I)Landroid/net/Uri;

    move-result-object p1

    .line 74
    iget-object p3, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->presenter:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;

    invoke-virtual {p3, p2, p4, p5, p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->invoiceContactClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$PickAddressBookContactView(Landroid/view/View;II)V
    .locals 0

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    const/4 p3, 0x0

    invoke-virtual {p2, p3, p3, p3, p1}, Lcom/squareup/ui/EmptyView;->setPadding(IIII)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->bindViews()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->searchEditText:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/addressbook/PickAddressBookContactView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$1;-><init>(Lcom/squareup/ui/addressbook/PickAddressBookContactView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    new-instance v1, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactView$G29XMVzg5nXxw5Ed24esg6QX4Kg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactView$G29XMVzg5nXxw5Ed24esg6QX4Kg;-><init>(Lcom/squareup/ui/addressbook/PickAddressBookContactView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setGravity(I)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->USER_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    sget v1, Lcom/squareup/crmupdatecustomer/R$string;->invoice_no_contacts_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setTitle(I)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->emptyView:Lcom/squareup/ui/EmptyView;

    sget v1, Lcom/squareup/crmupdatecustomer/R$string;->invoice_no_contacts_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setMessage(I)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->header:Landroid/view/ViewGroup;

    new-instance v1, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactView$033LDC8qv-JW9O_36J7wD2Q-Cm4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactView$033LDC8qv-JW9O_36J7wD2Q-Cm4;-><init>(Lcom/squareup/ui/addressbook/PickAddressBookContactView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->presenter:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 99
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 v0, 0x0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 92
    invoke-direct {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->closeCursor()V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->presenter:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 95
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method updateSearchButton(Z)V
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->searchEditText:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    return-void
.end method
