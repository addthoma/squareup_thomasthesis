.class public Lcom/squareup/ui/RealDisplaysWarningScreen;
.super Ljava/lang/Object;
.source "RealDisplaysWarningScreen.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;


# instance fields
.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/TenderStarter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/ui/RealDisplaysWarningScreen;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    return-void
.end method


# virtual methods
.method public showNfcWarningScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/RealDisplaysWarningScreen;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0, p1}, Lcom/squareup/ui/tender/TenderStarter;->showNfcWarningScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method
