.class public final enum Lcom/squareup/ui/SquareActivity$Preconditions;
.super Ljava/lang/Enum;
.source "SquareActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/SquareActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Preconditions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/SquareActivity$Preconditions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/SquareActivity$Preconditions;

.field public static final enum LOGGED_IN:Lcom/squareup/ui/SquareActivity$Preconditions;

.field public static final enum NO_AUTH_NEEDED:Lcom/squareup/ui/SquareActivity$Preconditions;


# instance fields
.field final requiresAuth:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 83
    new-instance v0, Lcom/squareup/ui/SquareActivity$Preconditions;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "LOGGED_IN"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/SquareActivity$Preconditions;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->LOGGED_IN:Lcom/squareup/ui/SquareActivity$Preconditions;

    .line 84
    new-instance v0, Lcom/squareup/ui/SquareActivity$Preconditions;

    const-string v3, "NO_AUTH_NEEDED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/ui/SquareActivity$Preconditions;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->NO_AUTH_NEEDED:Lcom/squareup/ui/SquareActivity$Preconditions;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/SquareActivity$Preconditions;

    .line 82
    sget-object v3, Lcom/squareup/ui/SquareActivity$Preconditions;->LOGGED_IN:Lcom/squareup/ui/SquareActivity$Preconditions;

    aput-object v3, v0, v2

    sget-object v2, Lcom/squareup/ui/SquareActivity$Preconditions;->NO_AUTH_NEEDED:Lcom/squareup/ui/SquareActivity$Preconditions;

    aput-object v2, v0, v1

    sput-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->$VALUES:[Lcom/squareup/ui/SquareActivity$Preconditions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput-boolean p3, p0, Lcom/squareup/ui/SquareActivity$Preconditions;->requiresAuth:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/SquareActivity$Preconditions;
    .locals 1

    .line 82
    const-class v0, Lcom/squareup/ui/SquareActivity$Preconditions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/SquareActivity$Preconditions;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/SquareActivity$Preconditions;
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->$VALUES:[Lcom/squareup/ui/SquareActivity$Preconditions;

    invoke-virtual {v0}, [Lcom/squareup/ui/SquareActivity$Preconditions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/SquareActivity$Preconditions;

    return-object v0
.end method
