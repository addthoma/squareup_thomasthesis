.class Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;
.super Lcom/squareup/ui/EmptyAnimationListener;
.source "CartHeaderPhoneView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->initializeSaleAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

.field final synthetic val$fadeInNoSale:Landroid/view/animation/Animation;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;Landroid/view/animation/Animation;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;->this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    iput-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;->val$fadeInNoSale:Landroid/view/animation/Animation;

    invoke-direct {p0}, Lcom/squareup/ui/EmptyAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;->this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleContainer:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;->this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->noSale:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;->val$fadeInNoSale:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;->this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->noSale:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
