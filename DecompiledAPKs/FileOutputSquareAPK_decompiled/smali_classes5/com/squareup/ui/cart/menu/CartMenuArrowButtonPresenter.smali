.class public Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;
.super Lmortar/ViewPresenter;
.source "CartMenuArrowButtonPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/cart/menu/CartMenuArrowButton;",
        ">;"
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final menuPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;


# direct methods
.method public constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->menuPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;)Ljava/lang/Object;
    .locals 0

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;)Ljava/lang/Object;
    .locals 0

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private onCartUpdated()V
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->updateEnabledState()V

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/cart/menu/CartMenuArrowButton;)V
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->menuPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    .line 47
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->dropView(Lcom/squareup/ui/cart/menu/CartMenuArrowButton;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$CartMenuArrowButtonPresenter(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->onCartUpdated()V

    return-void
.end method

.method public onClick()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->menuPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->toggle()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/menu/-$$Lambda$CartMenuArrowButtonPresenter$2xXYNcGYyGNMShw9ORg7EJDcwhA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/-$$Lambda$CartMenuArrowButtonPresenter$2xXYNcGYyGNMShw9ORg7EJDcwhA;-><init>(Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 32
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->updateEnabledState()V

    .line 34
    iget-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->menuPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    new-instance v0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter$1;-><init>(Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    return-void
.end method

.method public updateEnabledState()V
    .locals 2

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;

    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->menuPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->hasAtLeastOneEnabledOption()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;->setEnabled(Z)V

    return-void
.end method
