.class public final enum Lcom/squareup/ui/cart/Appearance;
.super Ljava/lang/Enum;
.source "CartEntryViewModelFactory.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/cart/Appearance;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B#\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\tR\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/cart/Appearance;",
        "",
        "strikethrough",
        "",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "glyphColorStateListId",
        "",
        "(Ljava/lang/String;IZLcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Integer;)V",
        "Ljava/lang/Integer;",
        "DEFAULT",
        "DISCOUNTED",
        "REWARDED",
        "COMPED",
        "VOIDED",
        "WARNED",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/cart/Appearance;

.field public static final enum COMPED:Lcom/squareup/ui/cart/Appearance;

.field public static final enum DEFAULT:Lcom/squareup/ui/cart/Appearance;

.field public static final enum DISCOUNTED:Lcom/squareup/ui/cart/Appearance;

.field public static final enum REWARDED:Lcom/squareup/ui/cart/Appearance;

.field public static final enum VOIDED:Lcom/squareup/ui/cart/Appearance;

.field public static final enum WARNED:Lcom/squareup/ui/cart/Appearance;


# instance fields
.field public final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final glyphColorStateListId:Ljava/lang/Integer;

.field public final strikethrough:Z


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/cart/Appearance;

    new-instance v7, Lcom/squareup/ui/cart/Appearance;

    const-string v2, "DEFAULT"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v7

    .line 128
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/Appearance;-><init>(Ljava/lang/String;IZLcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Integer;)V

    sput-object v7, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/ui/cart/Appearance;

    .line 132
    sget-object v12, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_TINY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 133
    sget v2, Lcom/squareup/marin/R$color;->marin_text_selector_medium_gray_disabled_light_gray:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const-string v9, "DISCOUNTED"

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v8, v1

    .line 131
    invoke-direct/range {v8 .. v13}, Lcom/squareup/ui/cart/Appearance;-><init>(Ljava/lang/String;IZLcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Integer;)V

    sput-object v1, Lcom/squareup/ui/cart/Appearance;->DISCOUNTED:Lcom/squareup/ui/cart/Appearance;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/cart/Appearance;

    .line 138
    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_TINY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 139
    sget v2, Lcom/squareup/widgets/pos/R$color;->edit_item_yellow:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string v4, "REWARDED"

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v3, v1

    .line 137
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/Appearance;-><init>(Ljava/lang/String;IZLcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Integer;)V

    sput-object v1, Lcom/squareup/ui/cart/Appearance;->REWARDED:Lcom/squareup/ui/cart/Appearance;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/cart/Appearance;

    .line 144
    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_TINY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 145
    sget v2, Lcom/squareup/marin/R$color;->marin_text_selector_medium_gray_disabled_light_gray:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string v4, "COMPED"

    const/4 v5, 0x3

    move-object v3, v1

    .line 143
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/Appearance;-><init>(Ljava/lang/String;IZLcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Integer;)V

    sput-object v1, Lcom/squareup/ui/cart/Appearance;->COMPED:Lcom/squareup/ui/cart/Appearance;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/cart/Appearance;

    const-string v4, "VOIDED"

    const/4 v5, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    .line 149
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/Appearance;-><init>(Ljava/lang/String;IZLcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Integer;)V

    sput-object v1, Lcom/squareup/ui/cart/Appearance;->VOIDED:Lcom/squareup/ui/cart/Appearance;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/cart/Appearance;

    .line 153
    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_INFO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string v4, "WARNED"

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v3, v1

    .line 152
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/Appearance;-><init>(Ljava/lang/String;IZLcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Integer;)V

    sput-object v1, Lcom/squareup/ui/cart/Appearance;->WARNED:Lcom/squareup/ui/cart/Appearance;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/cart/Appearance;->$VALUES:[Lcom/squareup/ui/cart/Appearance;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZLcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 122
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lcom/squareup/ui/cart/Appearance;->strikethrough:Z

    iput-object p4, p0, Lcom/squareup/ui/cart/Appearance;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object p5, p0, Lcom/squareup/ui/cart/Appearance;->glyphColorStateListId:Ljava/lang/Integer;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/cart/Appearance;
    .locals 1

    const-class v0, Lcom/squareup/ui/cart/Appearance;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/cart/Appearance;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/cart/Appearance;
    .locals 1

    sget-object v0, Lcom/squareup/ui/cart/Appearance;->$VALUES:[Lcom/squareup/ui/cart/Appearance;

    invoke-virtual {v0}, [Lcom/squareup/ui/cart/Appearance;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/cart/Appearance;

    return-object v0
.end method
