.class public final Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;
.super Ljava/lang/Object;
.source "CartEntryView.kt"

# interfaces
.implements Lcom/squareup/util/OnMeasuredCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartEntryView;->updateSubLabel(Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCartEntryView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CartEntryView.kt\ncom/squareup/ui/cart/CartEntryView$updateSubLabel$1\n*L\n1#1,307:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/ui/cart/CartEntryView$updateSubLabel$1",
        "Lcom/squareup/util/OnMeasuredCallback;",
        "onMeasured",
        "",
        "view",
        "Landroid/view/View;",
        "width",
        "",
        "height",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $model:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

.field final synthetic this$0:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartEntryView;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;",
            ")V"
        }
    .end annotation

    .line 285
    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;->this$0:Lcom/squareup/ui/cart/CartEntryView;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;->$model:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMeasured(Landroid/view/View;II)V
    .locals 1

    const-string/jumbo p3, "view"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;->$model:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;->getValue()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;

    move-result-object p1

    .line 293
    instance-of p3, p1, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;

    if-eqz p3, :cond_0

    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;->this$0:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {p2}, Lcom/squareup/ui/cart/CartEntryView;->getSubLabelField()Landroid/widget/TextView;

    move-result-object p2

    check-cast p1, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;->getValue()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 294
    :cond_0
    instance-of p3, p1, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Ellipsized;

    if-eqz p3, :cond_1

    .line 299
    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;->this$0:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {p3}, Lcom/squareup/ui/cart/CartEntryView;->getSubLabelField()Landroid/widget/TextView;

    move-result-object p3

    check-cast p1, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Ellipsized;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Ellipsized;->getNameValuePairs()Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;->this$0:Lcom/squareup/ui/cart/CartEntryView;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartEntryView;->access$getRegularTextPaint$p(Lcom/squareup/ui/cart/CartEntryView;)Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/squareup/util/NameValuePairsKt;->ellipsizeAndJoin(Ljava/util/List;Landroid/text/TextPaint;I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method
