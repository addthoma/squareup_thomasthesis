.class public final Lcom/squareup/ui/cart/CartContainerScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "CartContainerScreen.java"

# interfaces
.implements Lcom/squareup/ui/seller/EnablesCardSwipes;
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/cart/CartContainerScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartContainerScreen$Module;,
        Lcom/squareup/ui/cart/CartContainerScreen$Component;,
        Lcom/squareup/ui/cart/CartContainerScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/cart/CartContainerScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/cart/CartContainerScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/ui/cart/CartContainerScreen;

    invoke-direct {v0}, Lcom/squareup/ui/cart/CartContainerScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/cart/CartContainerScreen;->INSTANCE:Lcom/squareup/ui/cart/CartContainerScreen;

    .line 194
    sget-object v0, Lcom/squareup/ui/cart/CartContainerScreen;->INSTANCE:Lcom/squareup/ui/cart/CartContainerScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/cart/CartContainerScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 197
    sget v0, Lcom/squareup/orderentry/R$layout;->cart_container_view:I

    return v0
.end method
