.class public final Lcom/squareup/ui/cart/DiscountFormatter_Factory;
.super Ljava/lang/Object;
.source "DiscountFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/cart/DiscountFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/cart/DiscountFormatter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/cart/DiscountFormatter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/cart/DiscountFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/ui/cart/DiscountFormatter_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/cart/DiscountFormatter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/cart/DiscountFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/cart/DiscountFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/ui/cart/DiscountFormatter;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/cart/DiscountFormatter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/cart/DiscountFormatter;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/cart/DiscountFormatter;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/cart/DiscountFormatter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/ui/cart/DiscountFormatter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1}, Lcom/squareup/ui/cart/DiscountFormatter_Factory;->newInstance(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/cart/DiscountFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiscountFormatter_Factory;->get()Lcom/squareup/ui/cart/DiscountFormatter;

    move-result-object v0

    return-object v0
.end method
