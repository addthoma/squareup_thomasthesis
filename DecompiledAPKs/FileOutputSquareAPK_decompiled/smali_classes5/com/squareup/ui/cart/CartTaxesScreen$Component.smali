.class public interface abstract Lcom/squareup/ui/cart/CartTaxesScreen$Component;
.super Ljava/lang/Object;
.source "CartTaxesScreen.java"

# interfaces
.implements Lcom/squareup/marin/widgets/MarinActionBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/marin/widgets/MarinActionBarModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartTaxesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/cart/CartTaxesView;)V
.end method
