.class public Lcom/squareup/ui/cart/TaxFormatter;
.super Ljava/lang/Object;
.source "TaxFormatter.java"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/cart/TaxFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Ljava/lang/CharSequence;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/TaxFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/widgets/R$string;->percent_character_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    iget-object p1, p1, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    .line 21
    invoke-static {p1}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "value"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 22
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/TaxFormatter;->format(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
