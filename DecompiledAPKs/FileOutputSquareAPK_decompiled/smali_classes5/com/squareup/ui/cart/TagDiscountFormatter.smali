.class public Lcom/squareup/ui/cart/TagDiscountFormatter;
.super Ljava/lang/Object;
.source "TagDiscountFormatter.java"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        ">;"
    }
.end annotation


# instance fields
.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/cart/TagDiscountFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/cart/TagDiscountFormatter;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/cart/TagDiscountFormatter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 31
    iput-object p4, p0, Lcom/squareup/ui/cart/TagDiscountFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/shared/catalog/models/CatalogDiscount;)Ljava/lang/CharSequence;
    .locals 3

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 37
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getPercentage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 40
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v1

    sget-object v2, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    if-eq v1, v2, :cond_3

    if-eqz v0, :cond_2

    .line 41
    iget-object p1, p0, Lcom/squareup/ui/cart/TagDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderentry/R$string;->variable_percent_discount_standalone_character:I

    .line 42
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/cart/TagDiscountFormatter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 43
    invoke-static {p1}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    if-eqz v0, :cond_4

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/cart/TagDiscountFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getPercentage()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 51
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/cart/TagDiscountFormatter;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/TagDiscountFormatter;->format(Lcom/squareup/shared/catalog/models/CatalogDiscount;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
