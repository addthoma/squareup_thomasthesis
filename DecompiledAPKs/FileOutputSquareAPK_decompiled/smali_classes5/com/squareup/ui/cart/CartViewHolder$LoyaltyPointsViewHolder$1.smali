.class Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;)V
    .locals 0

    .line 272
    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 274
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->access$300(Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->getAdapterPosition()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->onLoyaltyRowClicked(I)V

    return-void
.end method
