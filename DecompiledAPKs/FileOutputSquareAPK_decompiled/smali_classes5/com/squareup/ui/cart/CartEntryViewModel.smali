.class public final Lcom/squareup/ui/cart/CartEntryViewModel;
.super Ljava/lang/Object;
.source "CartEntryViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartEntryViewModel$Name;,
        Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;,
        Lcom/squareup/ui/cart/CartEntryViewModel$Amount;,
        Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;,
        Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0005\u001f !\"#B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tH\u00c6\u0003J7\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewModel;",
        "",
        "name",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Name;",
        "quantity",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;",
        "amount",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Amount;",
        "subLabel",
        "Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;",
        "(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V",
        "getAmount",
        "()Lcom/squareup/ui/cart/CartEntryViewModel$Amount;",
        "getName",
        "()Lcom/squareup/ui/cart/CartEntryViewModel$Name;",
        "getQuantity",
        "()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;",
        "getSubLabel",
        "()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Amount",
        "Name",
        "Quantity",
        "SubLabel",
        "SubLabelValue",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

.field private final name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

.field private final quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

.field private final subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    iput-object p3, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    iput-object p4, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;ILjava/lang/Object;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/cart/CartEntryViewModel;->copy(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/cart/CartEntryViewModel$Name;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/cart/CartEntryViewModel$Amount;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    return-object v0
.end method

.method public final component4()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/cart/CartEntryViewModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/cart/CartEntryViewModel;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel;->name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel;->quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel;->amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartEntryViewModel;->subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/ui/cart/CartEntryViewModel$Amount;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    return-object v0
.end method

.method public final getName()Lcom/squareup/ui/cart/CartEntryViewModel$Name;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    return-object v0
.end method

.method public final getQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    return-object v0
.end method

.method public final getSubLabel()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CartEntryViewModel(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->name:Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->quantity:Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->amount:Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subLabel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel;->subLabel:Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
