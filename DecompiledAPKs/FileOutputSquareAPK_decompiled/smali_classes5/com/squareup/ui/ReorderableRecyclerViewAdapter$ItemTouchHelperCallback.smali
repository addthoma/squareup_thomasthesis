.class Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;
.super Landroidx/recyclerview/widget/ItemTouchHelper$Callback;
.source "ReorderableRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ReorderableRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ItemTouchHelperCallback"
.end annotation


# static fields
.field private static final SCROLL_RATIO:F = 0.5f


# instance fields
.field private maxScroll:I

.field private onDragStateChanged:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lrx/subjects/PublishSubject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/subjects/PublishSubject<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Landroidx/recyclerview/widget/ItemTouchHelper$Callback;-><init>()V

    const/4 v0, -0x1

    .line 47
    iput v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;->maxScroll:I

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;->onDragStateChanged:Lrx/subjects/PublishSubject;

    return-void
.end method

.method private getMaxDragScroll(Landroidx/recyclerview/widget/RecyclerView;)I
    .locals 2

    .line 107
    iget v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;->maxScroll:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 108
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/widgets/R$dimen;->max_drag_scroll_per_frame:I

    .line 109
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;->maxScroll:I

    .line 111
    :cond_0
    iget p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;->maxScroll:I

    return p1
.end method


# virtual methods
.method public canDropOver(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 0

    .line 73
    check-cast p3, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;

    invoke-virtual {p3}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;->canDropOver()Z

    move-result p1

    return p1
.end method

.method public getMovementFlags(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)I
    .locals 0

    const/4 p1, 0x3

    const/4 p2, 0x0

    .line 68
    invoke-static {p1, p2}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;->makeMovementFlags(II)I

    move-result p1

    return p1
.end method

.method public interpolateOutOfBoundsScroll(Landroidx/recyclerview/widget/RecyclerView;IIIJ)I
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;->getMaxDragScroll(Landroidx/recyclerview/widget/RecyclerView;)I

    move-result p1

    .line 96
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p4

    int-to-float p5, p3

    .line 97
    invoke-static {p5}, Ljava/lang/Math;->signum(F)F

    move-result p5

    float-to-int p5, p5

    int-to-float p4, p4

    const/high16 p6, 0x3f800000    # 1.0f

    mul-float p4, p4, p6

    int-to-float p2, p2

    div-float/2addr p4, p2

    .line 98
    invoke-static {p6, p4}, Ljava/lang/Math;->min(FF)F

    move-result p2

    mul-int p5, p5, p1

    int-to-float p1, p5

    mul-float p1, p1, p2

    const/high16 p2, 0x3f000000    # 0.5f

    mul-float p1, p1, p2

    float-to-int p1, p1

    if-nez p1, :cond_1

    if-lez p3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :cond_1
    :goto_0
    return p1
.end method

.method public isItemViewSwipeEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isLongPressDragEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onMove(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 0

    .line 80
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;

    .line 81
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p2

    .line 82
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p3

    .line 81
    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->reorderDraggableItems(II)Z

    move-result p1

    return p1
.end method

.method public onSelectedChanged(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 1

    .line 62
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/ItemTouchHelper$Callback;->onSelectedChanged(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;->onDragStateChanged:Lrx/subjects/PublishSubject;

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onSwiped(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    return-void
.end method
