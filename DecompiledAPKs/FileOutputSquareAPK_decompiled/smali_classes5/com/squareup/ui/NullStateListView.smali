.class public Lcom/squareup/ui/NullStateListView;
.super Lcom/squareup/marin/widgets/HorizontalRuleListView;
.source "NullStateListView.java"


# instance fields
.field private final bounds:Landroid/graphics/Rect;

.field private final maxTextSize:I

.field private final minTextSize:I

.field private nullStateNegativeTopMargin:I

.field private nullStateText:Ljava/lang/String;

.field private final textPaint:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/HorizontalRuleListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/NullStateListView;->bounds:Landroid/graphics/Rect;

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/NullStateListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 37
    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_header_title:I

    .line 38
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/NullStateListView;->maxTextSize:I

    .line 39
    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_default:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/NullStateListView;->minTextSize:I

    .line 41
    new-instance v1, Landroid/text/TextPaint;

    const/16 v2, 0x81

    invoke-direct {v1, v2}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v1, p0, Lcom/squareup/ui/NullStateListView;->textPaint:Landroid/text/TextPaint;

    .line 42
    iget-object v1, p0, Lcom/squareup/ui/NullStateListView;->textPaint:Landroid/text/TextPaint;

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, v2}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 43
    iget-object v1, p0, Lcom/squareup/ui/NullStateListView;->textPaint:Landroid/text/TextPaint;

    sget v2, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/NullStateListView;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lcom/squareup/ui/NullStateListView;->maxTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/NullStateListView;->textPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 47
    sget-object v0, Lcom/squareup/widgets/R$styleable;->NullStateListView:[I

    .line 48
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 49
    sget p2, Lcom/squareup/widgets/R$styleable;->NullStateListView_nullStateText:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/ui/NullStateListView;->setNullStateText(Ljava/lang/String;)V

    .line 50
    sget p2, Lcom/squareup/widgets/R$styleable;->NullStateListView_nullStateNegativeTopMargin:I

    const/4 v0, 0x0

    .line 51
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    neg-int p2, p2

    iput p2, p0, Lcom/squareup/ui/NullStateListView;->nullStateNegativeTopMargin:I

    .line 53
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 1

    .line 76
    instance-of v0, p2, Lcom/squareup/ui/NullStateListRow;

    if-eqz v0, :cond_1

    .line 77
    iget-object p2, p0, Lcom/squareup/ui/NullStateListView;->nullStateText:Ljava/lang/String;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    iget p3, p0, Lcom/squareup/ui/NullStateListView;->nullStateNegativeTopMargin:I

    div-int/lit8 p3, p3, 0x2

    add-int/2addr p2, p3

    iget-object p3, p0, Lcom/squareup/ui/NullStateListView;->bounds:Landroid/graphics/Rect;

    .line 80
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result p3

    div-int/lit8 p3, p3, 0x2

    add-int/2addr p2, p3

    .line 81
    iget-object p3, p0, Lcom/squareup/ui/NullStateListView;->nullStateText:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result p4

    div-int/lit8 p4, p4, 0x2

    int-to-float p4, p4

    int-to-float p2, p2

    iget-object v0, p0, Lcom/squareup/ui/NullStateListView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, p3, p4, p2, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    const/4 p1, 0x1

    return p1

    .line 85
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/squareup/marin/widgets/HorizontalRuleListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p1

    return p1
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 62
    invoke-super {p0, p1, p2}, Lcom/squareup/marin/widgets/HorizontalRuleListView;->onMeasure(II)V

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/NullStateListView;->nullStateText:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/NullStateListView;->textPaint:Landroid/text/TextPaint;

    iget-object p2, p0, Lcom/squareup/ui/NullStateListView;->nullStateText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/ui/NullStateListView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/ui/NullStateListView;->minTextSize:I

    int-to-float v1, v1

    iget v2, p0, Lcom/squareup/ui/NullStateListView;->maxTextSize:I

    int-to-float v2, v2

    invoke-static {p1, p2, v0, v1, v2}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFF)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/NullStateListView;->textPaint:Landroid/text/TextPaint;

    iget-object p2, p0, Lcom/squareup/ui/NullStateListView;->nullStateText:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/NullStateListView;->bounds:Landroid/graphics/Rect;

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method public setNullStateNegativeTopMargin(I)V
    .locals 0

    .line 71
    iput p1, p0, Lcom/squareup/ui/NullStateListView;->nullStateNegativeTopMargin:I

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/NullStateListView;->invalidate()V

    return-void
.end method

.method public setNullStateText(Ljava/lang/String;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/NullStateListView;->nullStateText:Ljava/lang/String;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/NullStateListView;->invalidate()V

    return-void
.end method
