.class public Lcom/squareup/ui/ErrorsBarPresenter;
.super Lmortar/ViewPresenter;
.source "ErrorsBarPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ErrorsBarPresenter$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ErrorsBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static final BATCHED_KEY:Ljava/lang/String; = "batched"

.field private static final BATCHED_MESSAGE_BUNDLE_KEY:Ljava/lang/String; = "errors_bar_batched_message"

.field private static final DEFAULT_MAX_MESSAGES:I = 0x3

.field private static final MAX_MESSAGES_BUNDLE_KEY:Ljava/lang/String; = "errors_bar_max_messages"

.field private static final MESSAGES_BUNDLE_KEY:Ljava/lang/String; = "errors_bar_messages"


# instance fields
.field private batchedErrorMessage:Ljava/lang/String;

.field private final errorMessages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private loaded:Z

.field private maxMessages:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 28
    iput-boolean v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->loaded:Z

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    const/4 v0, 0x3

    .line 32
    iput v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->maxMessages:I

    return-void
.end method


# virtual methods
.method public addError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget v1, p0, Lcom/squareup/ui/ErrorsBarPresenter;->maxMessages:I

    if-le v0, v1, :cond_2

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarView;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/ErrorsBarView;->removeRow(Ljava/lang/String;)Z

    goto :goto_0

    .line 70
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ErrorsBarPresenter;->batchedErrorMessage:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ErrorsBarView;

    iget-object p2, p0, Lcom/squareup/ui/ErrorsBarPresenter;->batchedErrorMessage:Ljava/lang/String;

    const-string v0, "batched"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/ErrorsBarView;->addRow(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 71
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "batchedErrorMessage may not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 75
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarView;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/ErrorsBarView;->addRow(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public clearErrors()V
    .locals 2

    .line 103
    new-instance v0, Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getErrorKeys()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 104
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 105
    invoke-virtual {p0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getErrorKeys()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x1

    .line 36
    iput-boolean v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->loaded:Z

    if-eqz p1, :cond_0

    .line 37
    iget-boolean v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->loaded:Z

    if-nez v0, :cond_0

    const-string v0, "errors_bar_batched_message"

    .line 38
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->batchedErrorMessage:Ljava/lang/String;

    const-string v0, "errors_bar_max_messages"

    .line 39
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->maxMessages:I

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    const-string v1, "errors_bar_messages"

    .line 42
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    .line 41
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 44
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ErrorsBarView;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/ErrorsBarView;->addRow(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    const-string v1, "errors_bar_messages"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->batchedErrorMessage:Ljava/lang/String;

    const-string v1, "errors_bar_batched_message"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->maxMessages:I

    const-string v1, "errors_bar_max_messages"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public removeError(Ljava/lang/String;)Z
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget v1, p0, Lcom/squareup/ui/ErrorsBarPresenter;->maxMessages:I

    if-ne v0, v1, :cond_2

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ErrorsBarView;

    const-string v0, "batched"

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ErrorsBarView;->removeRow(Ljava/lang/String;)Z

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/ErrorsBarPresenter;->errorMessages:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ErrorsBarView;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/ErrorsBarView;->addRow(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1

    .line 91
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ErrorsBarView;->removeRow(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public setBatchedErrorMessage(Ljava/lang/String;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/ErrorsBarPresenter;->batchedErrorMessage:Ljava/lang/String;

    return-void
.end method

.method public setMaxMessages(I)V
    .locals 0

    .line 60
    iput p1, p0, Lcom/squareup/ui/ErrorsBarPresenter;->maxMessages:I

    return-void
.end method

.method public shakeErrors()V
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarView;

    invoke-virtual {v0}, Lcom/squareup/ui/ErrorsBarView;->shakeErrors()V

    return-void
.end method
