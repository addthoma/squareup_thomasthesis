.class public Lcom/squareup/ui/DialogCardView;
.super Landroid/widget/LinearLayout;
.source "DialogCardView.java"


# instance fields
.field private final isLandscape:Z

.field private final isTablet:Z

.field private final landscapePhoneAsCard:Z

.field private final panelHeight:I

.field private final panelWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/DialogCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/ScreenParameters;->getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 31
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/squareup/ui/DialogCardView;->panelWidth:I

    .line 32
    iget v0, v0, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/squareup/ui/DialogCardView;->panelHeight:I

    .line 33
    iget v0, p0, Lcom/squareup/ui/DialogCardView;->panelWidth:I

    iget v1, p0, Lcom/squareup/ui/DialogCardView;->panelHeight:I

    const/4 v2, 0x1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/ui/DialogCardView;->isLandscape:Z

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/DialogCardView;->isTablet:Z

    .line 36
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->DialogCardView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 37
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->DialogCardView_landscapePhoneAsCard:I

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/ui/DialogCardView;->landscapePhoneAsCard:Z

    .line 38
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic lambda$dialogUseFullScreenWidth$0(Landroid/app/Dialog;Landroid/content/DialogInterface;)V
    .locals 1

    .line 44
    new-instance p1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {p1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 45
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p0

    .line 46
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    const/4 v0, -0x1

    .line 47
    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 48
    invoke-virtual {p0, p1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public dialogUseFullScreenWidth(Landroid/app/Dialog;)V
    .locals 1

    .line 42
    iget-boolean v0, p0, Lcom/squareup/ui/DialogCardView;->isLandscape:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/DialogCardView;->isTablet:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/DialogCardView;->landscapePhoneAsCard:Z

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/squareup/ui/-$$Lambda$DialogCardView$Agh0hdm5s82zpeGl_GHdHrLvQJU;

    invoke-direct {v0, p1}, Lcom/squareup/ui/-$$Lambda$DialogCardView$Agh0hdm5s82zpeGl_GHdHrLvQJU;-><init>(Landroid/app/Dialog;)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .line 54
    iget-boolean v0, p0, Lcom/squareup/ui/DialogCardView;->isLandscape:Z

    const/high16 v1, 0x40000000    # 2.0f

    if-eqz v0, :cond_2

    .line 55
    iget-boolean v0, p0, Lcom/squareup/ui/DialogCardView;->isTablet:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/DialogCardView;->landscapePhoneAsCard:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 61
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 60
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_1

    .line 56
    :cond_1
    :goto_0
    iget p1, p0, Lcom/squareup/ui/DialogCardView;->panelHeight:I

    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 57
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 56
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_1

    .line 64
    :cond_2
    iget-boolean p1, p0, Lcom/squareup/ui/DialogCardView;->isTablet:Z

    if-eqz p1, :cond_3

    .line 66
    iget p1, p0, Lcom/squareup/ui/DialogCardView;->panelWidth:I

    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    iget p2, p0, Lcom/squareup/ui/DialogCardView;->panelWidth:I

    .line 67
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 66
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_1

    .line 70
    :cond_3
    iget p1, p0, Lcom/squareup/ui/DialogCardView;->panelWidth:I

    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 71
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 70
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    :goto_1
    return-void
.end method
