.class public final Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;
.super Ljava/lang/Object;
.source "InvoiceSentSavedPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUrlHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionMetricsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p6, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p7, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p8, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p9, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->invoiceUrlHelperProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p10, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;)",
            "Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;"
        }
    .end annotation

    .line 81
    new-instance v11, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;)Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;
    .locals 12

    .line 89
    new-instance v11, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;
    .locals 11

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->invoiceUrlHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/invoices/InvoiceUrlHelper;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/url/InvoiceShareUrlLauncher;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;)Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter_Factory;->get()Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;

    move-result-object v0

    return-object v0
.end method
