.class public final Lcom/squareup/ui/balance/BalanceAppletEntryPoint;
.super Lcom/squareup/applet/AppletEntryPoint;
.source "BalanceAppletEntryPoint.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001BA\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/balance/BalanceAppletEntryPoint;",
        "Lcom/squareup/applet/AppletEntryPoint;",
        "preferences",
        "Landroid/content/SharedPreferences;",
        "gatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "squareCardActivitySection",
        "Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;",
        "manageSquareCardSection",
        "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
        "transferReportsSection",
        "Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;",
        "capitalFlexLoanSection",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
        "unifiedActivitySection",
        "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;",
        "(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gatekeeper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardActivitySection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manageSquareCardSection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transferReportsSection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalFlexLoanSection"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unifiedActivitySection"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletEntryPointKt;->access$getLAST_SECTION_NAME_KEY$p()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    .line 30
    invoke-static {p3, p7}, Lcom/squareup/ui/balance/BalanceAppletEntryPointKt;->access$determineDefaultSection(Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;)Lcom/squareup/applet/AppletSection;

    move-result-object p1

    const/4 p3, 0x4

    new-array p3, p3, [Lcom/squareup/applet/AppletSection;

    .line 31
    check-cast p5, Lcom/squareup/applet/AppletSection;

    const/4 v1, 0x0

    aput-object p5, p3, v1

    .line 32
    check-cast p4, Lcom/squareup/applet/AppletSection;

    const/4 p5, 0x1

    aput-object p4, p3, p5

    .line 33
    check-cast p6, Lcom/squareup/applet/AppletSection;

    const/4 p4, 0x2

    aput-object p6, p3, p4

    .line 34
    check-cast p7, Lcom/squareup/applet/AppletSection;

    const/4 p4, 0x3

    aput-object p7, p3, p4

    .line 27
    invoke-direct {p0, v0, p2, p1, p3}, Lcom/squareup/applet/AppletEntryPoint;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/applet/AppletSection;[Lcom/squareup/applet/AppletSection;)V

    return-void
.end method
