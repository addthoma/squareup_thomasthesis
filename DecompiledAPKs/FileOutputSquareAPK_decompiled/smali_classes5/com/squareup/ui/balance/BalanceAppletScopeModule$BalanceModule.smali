.class abstract Lcom/squareup/ui/balance/BalanceAppletScopeModule$BalanceModule;
.super Ljava/lang/Object;
.source "BalanceAppletScopeModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceAppletScopeModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "BalanceModule"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindLinkBankAccountRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindLinkDebitCardRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/transfer/LinkDebitCardDialog$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindPriceChangeRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsAddMoneyWorkflowResultHandler(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsBalanceMasterScreenRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsBalanceTransactionDetailRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsBalanceTransactionsRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsBalanceTransferToBankRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsConfirmTransferRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsInstantDepositResultRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsLinkBankAccountWorkflowResultHandler(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$LinkBankAccountWorkflowResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsLinkDebitCardWorkflowResultHandler(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$LinkDebitCardWorkflowResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsManageSquareCardWorkflowResultHandler(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsTransferResultsRunner(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
