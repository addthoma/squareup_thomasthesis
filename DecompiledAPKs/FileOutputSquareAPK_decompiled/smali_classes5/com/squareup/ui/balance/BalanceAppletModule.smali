.class public abstract Lcom/squareup/ui/balance/BalanceAppletModule;
.super Ljava/lang/Object;
.source "BalanceAppletModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000cJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H!\u00a2\u0006\u0002\u0008\u0011J\u0015\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H!\u00a2\u0006\u0002\u0008\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/balance/BalanceAppletModule;",
        "",
        "()V",
        "bindsBalanceAppletClientTranslator",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
        "balanceAppletClientActionTranslator",
        "Lcom/squareup/ui/balance/BalanceAppletClientActionTranslator;",
        "bindsBalanceAppletClientTranslator$impl_wiring_release",
        "bindsBalanceAppletGateway",
        "Lcom/squareup/ui/balance/BalanceAppletGateway;",
        "realBalanceAppletGateway",
        "Lcom/squareup/ui/balance/RealBalanceAppletGateway;",
        "bindsBalanceAppletGateway$impl_wiring_release",
        "bindsCapitalSection",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
        "realCapitalSection",
        "Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;",
        "bindsCapitalSection$impl_wiring_release",
        "bindsManageSquareCardSection",
        "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
        "realManageSquareCardSection",
        "Lcom/squareup/ui/balance/bizbanking/squarecard/RealManageSquareCardSection;",
        "bindsManageSquareCardSection$impl_wiring_release",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindsBalanceAppletClientTranslator$impl_wiring_release(Lcom/squareup/ui/balance/BalanceAppletClientActionTranslator;)Lcom/squareup/clientactiontranslation/ClientActionTranslator;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindsBalanceAppletGateway$impl_wiring_release(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)Lcom/squareup/ui/balance/BalanceAppletGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsCapitalSection$impl_wiring_release(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;)Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsManageSquareCardSection$impl_wiring_release(Lcom/squareup/ui/balance/bizbanking/squarecard/RealManageSquareCardSection;)Lcom/squareup/balance/squarecard/ManageSquareCardSection;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
