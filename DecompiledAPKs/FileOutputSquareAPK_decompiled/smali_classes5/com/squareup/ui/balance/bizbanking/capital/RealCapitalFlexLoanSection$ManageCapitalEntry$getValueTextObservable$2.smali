.class final Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanSection.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->getValueTextObservable()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "status",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;->this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;->apply(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)Ljava/lang/String;
    .locals 2

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;->this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-static {v0}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->access$getCapitalOfferFormatter$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;

    move-result-object v0

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;->offerLimit(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 77
    :cond_0
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$NoOffer;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 79
    :cond_1
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;

    if-eqz v0, :cond_2

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;->this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->access$getRes$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->square_capital_flex_status_current_plan:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 81
    :cond_2
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;

    if-eqz v0, :cond_3

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;->this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->access$getRes$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->square_capital_flex_status_previous_plan:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 83
    :cond_3
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;

    if-eqz v0, :cond_4

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;->this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->access$getRes$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->square_capital_flex_status_financing_request:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 85
    :cond_4
    instance-of p1, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;

    if-eqz p1, :cond_5

    :goto_0
    return-object v1

    .line 86
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
