.class final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$3;
.super Ljava/lang/Object;
.source "TransferToBankRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->updateFee(Lcom/squareup/protos/common/Money;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
        "feeDetailsResponse",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$3;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 10

    const-string v0, "feeDetailsResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$3;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-static {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->access$latestState(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v4, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->copy$default(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$3;->apply(Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    return-object p1
.end method
