.class public final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;
.super Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;
.source "BalanceTransactionsScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;,
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;,
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceTransactionsScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceTransactionsScreen.kt\ncom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,123:1\n43#2:124\n24#3,4:125\n*E\n*S KotlinDebug\n*F\n+ 1 BalanceTransactionsScreen.kt\ncom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen\n*L\n34#1:124\n39#1,4:125\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0003\u0012\u0013\u0014B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005J\u0012\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016R\u0016\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;",
        "Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "CardActivityRow",
        "Runner",
        "ScreenData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;

.field private static final section:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;

    .line 30
    const-class v0, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;->section:Ljava/lang/Class;

    .line 125
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 128
    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 30
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;->section:Ljava/lang/Class;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    const-class v0, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    .line 35
    invoke-interface {p1}, Lcom/squareup/ui/balance/BalanceAppletScope$Component;->balanceTransactionsCoordinator()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/balance/applet/impl/R$layout;->balance_transactions_view:I

    return v0
.end method
