.class public interface abstract Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;
.super Ljava/lang/Object;
.source "ConfirmTransferScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract confirmTransferScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onBackFromConfirmTransfer()V
.end method

.method public abstract onBankAccountSettingsClicked()V
.end method

.method public abstract onDepositsSettingsClickedFromConfirmTransfer()V
.end method

.method public abstract onTransferClicked(Ljava/lang/String;)V
.end method
