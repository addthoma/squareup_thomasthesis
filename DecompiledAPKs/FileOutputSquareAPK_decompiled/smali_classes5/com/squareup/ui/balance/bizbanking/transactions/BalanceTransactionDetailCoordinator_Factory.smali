.class public final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;
.super Ljava/lang/Object;
.source "BalanceTransactionDetailCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;

    invoke-static {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;->newInstance(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator_Factory;->get()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;

    move-result-object v0

    return-object v0
.end method
