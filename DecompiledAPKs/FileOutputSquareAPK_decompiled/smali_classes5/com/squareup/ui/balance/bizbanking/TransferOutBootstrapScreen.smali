.class public final Lcom/squareup/ui/balance/bizbanking/TransferOutBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "TransferOutBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/TransferOutBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "transferOutWorkflowProps",
        "Lcom/squareup/balance/transferout/TransferOutWorkflowProps;",
        "(Lcom/squareup/balance/transferout/TransferOutWorkflowProps;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final transferOutWorkflowProps:Lcom/squareup/balance/transferout/TransferOutWorkflowProps;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/transferout/TransferOutWorkflowProps;)V
    .locals 1

    const-string/jumbo v0, "transferOutWorkflowProps"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/TransferOutBootstrapScreen;->transferOutWorkflowProps:Lcom/squareup/balance/transferout/TransferOutWorkflowProps;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget-object v0, Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;->Companion:Lcom/squareup/balance/transferout/TransferOutWorkflowRunner$Companion;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/TransferOutBootstrapScreen;->transferOutWorkflowProps:Lcom/squareup/balance/transferout/TransferOutWorkflowProps;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/balance/transferout/TransferOutWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/balance/transferout/TransferOutWorkflowProps;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/balance/transferout/TransferOutScope;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutScope;

    return-object v0
.end method
