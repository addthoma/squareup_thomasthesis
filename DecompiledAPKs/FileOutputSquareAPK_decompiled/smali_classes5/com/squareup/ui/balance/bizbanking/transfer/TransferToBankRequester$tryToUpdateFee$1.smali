.class final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$tryToUpdateFee$1;
.super Ljava/lang/Object;
.source "TransferToBankRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->tryToUpdateFee(Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
        "received",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $request:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;

.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$tryToUpdateFee$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$tryToUpdateFee$1;->$request:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
            ">;)",
            "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;"
        }
    .end annotation

    const-string v0, "received"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    goto :goto_0

    .line 197
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$tryToUpdateFee$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$tryToUpdateFee$1;->$request:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;->deposit_amount:Lcom/squareup/protos/common/Money;

    const-string v1, "request.deposit_amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->access$getDefaultFeeResponse(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$tryToUpdateFee$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    move-result-object p1

    return-object p1
.end method
