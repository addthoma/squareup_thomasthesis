.class public Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;
.super Ljava/lang/Object;
.source "BalanceTransactionDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenData"
.end annotation


# instance fields
.field public final date:Ljava/lang/String;

.field public final description:Ljava/lang/String;

.field public final isPersonalExpense:Z

.field public final time:Ljava/lang/String;

.field public final total:Ljava/lang/String;

.field public final transactionToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->total:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->description:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->date:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->time:Ljava/lang/String;

    .line 55
    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->transactionToken:Ljava/lang/String;

    .line 56
    iput-boolean p6, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->isPersonalExpense:Z

    return-void
.end method
