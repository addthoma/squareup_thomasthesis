.class final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;
.super Ljava/lang/Object;
.source "TransferToBankRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->deposit(Ljava/lang/String;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $currentState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;->$currentState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/deposits/CreateTransferResponse;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;->$currentState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    invoke-static {p1, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->access$onDepositSuccess(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    goto :goto_0

    .line 160
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;->$currentState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v0, v1, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->access$onDepositFailure(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    return-object p1
.end method
