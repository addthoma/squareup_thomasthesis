.class public final Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory;
.super Ljava/lang/Object;
.source "BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory$InstanceHolder;->access$000()Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideOrderConfirmationConfiguration()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;
    .locals 2

    .line 28
    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule$OrderSquareCardModule;->provideOrderConfirmationConfiguration()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory;->provideOrderConfirmationConfiguration()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory;->get()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method
