.class public final Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "AddMoneyWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyProps;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddMoneyWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddMoneyWorkflow.kt\ncom/squareup/ui/balance/addmoney/AddMoneyWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,149:1\n32#2,12:150\n*E\n*S KotlinDebug\n*F\n+ 1 AddMoneyWorkflow.kt\ncom/squareup/ui/balance/addmoney/AddMoneyWorkflow\n*L\n73#1,12:150\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t0\u0001:\u0001\u001aB\u001f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u001a\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00022\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016JH\u0010\u0015\u001a\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t2\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u00032\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0003H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyProps;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyScreen;",
        "pendingSourceWorkflow",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;",
        "enterAmountWorkflow",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;",
        "submitAmountWorkflow",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;",
        "(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enterAmountWorkflow:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;

.field private final pendingSourceWorkflow:Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;

.field private final submitAmountWorkflow:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pendingSourceWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enterAmountWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "submitAmountWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->pendingSourceWorkflow:Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->enterAmountWorkflow:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->submitAmountWorkflow:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/balance/addmoney/AddMoneyProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/AddMoneyState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 150
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 155
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 157
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 158
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 159
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 161
    :cond_3
    check-cast v1, Lcom/squareup/ui/balance/addmoney/AddMoneyState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 73
    :cond_4
    sget-object p1, Lcom/squareup/ui/balance/addmoney/AddMoneyState$HandlingLinkedCard;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyState$HandlingLinkedCard;

    move-object v1, p1

    check-cast v1, Lcom/squareup/ui/balance/addmoney/AddMoneyState;

    .line 74
    :goto_2
    instance-of p1, v1, Lcom/squareup/ui/balance/addmoney/AddMoneyState$SubmittingAmount;

    if-eqz p1, :cond_5

    .line 76
    sget-object p1, Lcom/squareup/ui/balance/addmoney/AddMoneyState$HandlingLinkedCard;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyState$HandlingLinkedCard;

    check-cast p1, Lcom/squareup/ui/balance/addmoney/AddMoneyState;

    return-object p1

    :cond_5
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->initialState(Lcom/squareup/ui/balance/addmoney/AddMoneyProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/AddMoneyState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;

    check-cast p2, Lcom/squareup/ui/balance/addmoney/AddMoneyState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->render(Lcom/squareup/ui/balance/addmoney/AddMoneyProps;Lcom/squareup/ui/balance/addmoney/AddMoneyState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/balance/addmoney/AddMoneyProps;Lcom/squareup/ui/balance/addmoney/AddMoneyState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyProps;",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
            "-",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    sget-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyState$HandlingLinkedCard;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyState$HandlingLinkedCard;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;->getLinkedCardInfo()Lcom/squareup/ui/balance/addmoney/LinkedCardInfo;

    move-result-object p2

    .line 90
    sget-object v0, Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$PendingVerificationCard;->INSTANCE:Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$PendingVerificationCard;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->pendingSourceWorkflow:Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$1;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$1;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 94
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;->getLinkedCardInfo()Lcom/squareup/ui/balance/addmoney/LinkedCardInfo;

    move-result-object p2

    instance-of v0, p2, Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$AvailableCard;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    move-object p2, v1

    :cond_1
    check-cast p2, Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$AvailableCard;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$AvailableCard;->getCardInfo()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object v1

    .line 96
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->enterAmountWorkflow:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;

    move-object v3, p2

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 97
    new-instance v4, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;

    .line 99
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;->getMinAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 100
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;->getMaxAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 97
    invoke-direct {v4, v1, p2, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;-><init>(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    const/4 v5, 0x0

    .line 102
    sget-object p1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$2;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$2;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 95
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 113
    :cond_3
    instance-of p1, p2, Lcom/squareup/ui/balance/addmoney/AddMoneyState$SubmittingAmount;

    if-eqz p1, :cond_4

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->submitAmountWorkflow:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 116
    new-instance v2, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;

    check-cast p2, Lcom/squareup/ui/balance/addmoney/AddMoneyState$SubmittingAmount;

    invoke-virtual {p2}, Lcom/squareup/ui/balance/addmoney/AddMoneyState$SubmittingAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/ui/balance/addmoney/AddMoneyState$SubmittingAmount;->getInstrumentToken()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v2, p1, p2}, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 117
    sget-object p1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 114
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/balance/addmoney/AddMoneyState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/ui/balance/addmoney/AddMoneyState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->snapshotState(Lcom/squareup/ui/balance/addmoney/AddMoneyState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
