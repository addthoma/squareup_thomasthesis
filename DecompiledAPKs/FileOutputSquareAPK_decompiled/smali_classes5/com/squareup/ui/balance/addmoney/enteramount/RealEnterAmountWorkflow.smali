.class public final Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealEnterAmountWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEnterAmountWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEnterAmountWorkflow.kt\ncom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,161:1\n149#2,5:162\n149#2,5:167\n149#2,5:172\n*E\n*S KotlinDebug\n*F\n+ 1 RealEnterAmountWorkflow.kt\ncom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow\n*L\n91#1,5:162\n108#1,5:167\n125#1,5:172\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n0\u0002:\u0001#B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u000bJD\u0010\u000c\u001a\u0018\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u0008j\u0008\u0012\u0004\u0012\u00020\r`\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00132\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0002J<\u0010\u0018\u001a\u0018\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u0008j\u0008\u0012\u0004\u0012\u00020\r`\u000f2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00132\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0002J\u001a\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016JH\u0010\u001d\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u00042\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050 H\u0016JB\u0010!\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00132\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0002J\u0010\u0010\"\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\u0004H\u0016\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyScreen;",
        "()V",
        "enterAmountSourcedScreen",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "card",
        "Lcom/squareup/protos/client/deposits/CardInfo;",
        "minAllowedAmount",
        "Lcom/squareup/protos/common/Money;",
        "maxAllowedAmount",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;",
        "enterAmountUnsourcedScreen",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showDebitCardRequiredScreen",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method

.method private final enterAmountSourcedScreen(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/deposits/CardInfo;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 81
    new-instance v6, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    .line 82
    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData$Sourced;

    .line 83
    invoke-static {p1}, Lcom/squareup/balance/squarecard/utility/CardFormatterKt;->getFormattedBrandAndUnmaskedDigits(Lcom/squareup/protos/client/deposits/CardInfo;)Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-direct {v0, v1, p2, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData$Sourced;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    .line 87
    new-instance p2, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$1;

    invoke-direct {p2, p4, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$1;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/protos/client/deposits/CardInfo;)V

    move-object v2, p2

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 88
    new-instance p1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$2;

    invoke-direct {p1, p4}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 89
    new-instance p1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$3;

    invoke-direct {p1, p4}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$3;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 90
    new-instance p1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$4;

    invoke-direct {p1, p4}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function0;

    move-object v0, v6

    .line 81
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v6, Lcom/squareup/workflow/legacy/V2Screen;

    .line 163
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 164
    const-class p2, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 165
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 163
    invoke-direct {p1, p2, v6, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final enterAmountUnsourcedScreen(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 99
    new-instance v6, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    .line 100
    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData$Unsourced;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData$Unsourced;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    .line 104
    new-instance p1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountUnsourcedScreen$1;

    invoke-direct {p1, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountUnsourcedScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v2, p1

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 105
    new-instance p1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountUnsourcedScreen$2;

    invoke-direct {p1, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountUnsourcedScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 106
    new-instance p1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountUnsourcedScreen$3;

    invoke-direct {p1, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountUnsourcedScreen$3;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 107
    new-instance p1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountUnsourcedScreen$4;

    invoke-direct {p1, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountUnsourcedScreen$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function0;

    move-object v0, v6

    .line 99
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v6, Lcom/squareup/workflow/legacy/V2Screen;

    .line 168
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 169
    const-class p2, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 170
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 168
    invoke-direct {p1, p2, v6, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final showDebitCardRequiredScreen(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 116
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;->enterAmountUnsourcedScreen(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 122
    new-instance p2, Lcom/squareup/ui/balance/addmoney/enteramount/DebitCardRequiredDialogScreen;

    .line 123
    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$showDebitCardRequiredScreen$dialog$1;

    invoke-direct {v0, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$showDebitCardRequiredScreen$dialog$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 124
    new-instance v1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$showDebitCardRequiredScreen$dialog$2;

    invoke-direct {v1, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$showDebitCardRequiredScreen$dialog$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 122
    invoke-direct {p2, v0, v1}, Lcom/squareup/ui/balance/addmoney/enteramount/DebitCardRequiredDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 173
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 174
    const-class v0, Lcom/squareup/ui/balance/addmoney/enteramount/DebitCardRequiredDialogScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 175
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 173
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 127
    sget-object p2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/Pair;

    .line 128
    sget-object v1, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v1, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 129
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    const/4 p3, 0x1

    aput-object p1, v0, p3

    .line 127
    invoke-virtual {p2, v0}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingEnterAmount;->INSTANCE:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingEnterAmount;

    check-cast p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;->initialState(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;

    check-cast p2, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;->render(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;",
            "-",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    .line 49
    sget-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingEnterAmount;->INSTANCE:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingEnterAmount;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->getCardInfo()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 52
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->getCardInfo()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object p2

    .line 53
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->getMinAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 54
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->getMaxAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 51
    invoke-direct {p0, p2, v0, p1, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;->enterAmountSourcedScreen(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->getMinAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 60
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->getMaxAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 58
    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;->enterAmountUnsourcedScreen(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 63
    :goto_0
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 65
    :cond_1
    sget-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingDebitCardRequiredDialog;->INSTANCE:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingDebitCardRequiredDialog;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 66
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->getMinAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 67
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->getMaxAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 65
    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;->showDebitCardRequiredScreen(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;->snapshotState(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
