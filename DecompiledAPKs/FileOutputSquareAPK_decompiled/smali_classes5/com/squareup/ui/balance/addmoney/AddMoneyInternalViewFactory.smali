.class public final Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "AddMoneyViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "enterAmountCoordinatorFactory",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;",
        "(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "enterAmountCoordinatorFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 33
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 34
    sget-object v2, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->Companion:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 35
    sget v3, Lcom/squareup/balance/applet/impl/R$layout;->add_amount_to_account:I

    .line 36
    new-instance v4, Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory$1;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 33
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 38
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 39
    sget-object p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->Companion:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;

    invoke-virtual {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;->getSHEET_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 40
    sget v4, Lcom/squareup/balance/core/R$layout;->bizbank_progress_layout:I

    .line 41
    sget-object p1, Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory$2;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory$2;

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 38
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 43
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 44
    sget-object v1, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;->Companion:Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 45
    sget-object v2, Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory$3;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 43
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 47
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 48
    sget-object v1, Lcom/squareup/ui/balance/addmoney/enteramount/DebitCardRequiredDialogScreen;->Companion:Lcom/squareup/ui/balance/addmoney/enteramount/DebitCardRequiredDialogScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/balance/addmoney/enteramount/DebitCardRequiredDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 49
    sget-object v2, Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory$4;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 47
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x3

    aput-object p1, v0, v1

    .line 32
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
