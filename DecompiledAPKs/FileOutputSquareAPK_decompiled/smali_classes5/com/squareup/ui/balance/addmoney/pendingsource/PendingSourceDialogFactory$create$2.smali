.class final Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2;
.super Ljava/lang/Object;
.source "PendingSourceDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;)Landroid/app/AlertDialog;
    .locals 3

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2;->$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 24
    sget v1, Lcom/squareup/balance/applet/impl/R$string;->add_money_pending_card_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 25
    sget v1, Lcom/squareup/balance/applet/impl/R$string;->add_money_pending_card_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 26
    sget v1, Lcom/squareup/balance/applet/impl/R$string;->add_money_pending_card_primary_button:I

    new-instance v2, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2$1;-><init>(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 29
    sget v1, Lcom/squareup/common/strings/R$string;->dismiss:I

    new-instance v2, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2$2;

    invoke-direct {v2, p1}, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2$2;-><init>(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2$3;

    invoke-direct {v1, p1}, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2$3;-><init>(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, -0x1

    .line 33
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPrimaryButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogFactory$create$2;->apply(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
