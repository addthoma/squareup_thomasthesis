.class public Lcom/squareup/ui/StarGroup;
.super Landroid/view/View;
.source "StarGroup.java"


# static fields
.field public static final DURATION:I = 0x12c


# instance fields
.field private final circlePaint:Landroid/graphics/Paint;

.field private final correctColor:I

.field private drawingGap:F

.field private drawingRadius:F

.field private drawingStep:F

.field private drawingWiggleRoom:F

.field private emptyRadius:F

.field private expectedStars:I

.field private final fillPaint:Landroid/graphics/Paint;

.field private final incorrectColor:I

.field private starCount:I

.field private final starGap:I

.field private final starRadius:I

.field private strokeWidth:F

.field private wiggle:F

.field private wiggleAnimator:Landroid/animation/ValueAnimator;

.field private wiggleRoom:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    .line 38
    iput v0, p0, Lcom/squareup/ui/StarGroup;->expectedStars:I

    const/4 v1, 0x0

    .line 39
    iput v1, p0, Lcom/squareup/ui/StarGroup;->wiggle:F

    .line 45
    invoke-virtual {p0, v0}, Lcom/squareup/ui/StarGroup;->setWillNotDraw(Z)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/StarGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 49
    sget-object v1, Lcom/squareup/widgets/pos/R$styleable;->StarGroup:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 50
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->StarGroup_starGap:I

    sget v1, Lcom/squareup/widgets/pos/R$dimen;->pin_star_gap:I

    .line 51
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 50
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/StarGroup;->starGap:I

    .line 52
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->StarGroup_starRadius:I

    sget v1, Lcom/squareup/widgets/pos/R$dimen;->pin_radius:I

    .line 53
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 52
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/StarGroup;->starRadius:I

    .line 54
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->StarGroup_starSuccessColor:I

    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    .line 55
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 54
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/StarGroup;->correctColor:I

    .line 56
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->StarGroup_starFailColor:I

    sget v1, Lcom/squareup/marin/R$color;->marin_red:I

    .line 57
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 56
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/StarGroup;->incorrectColor:I

    .line 58
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    iget p1, p0, Lcom/squareup/ui/StarGroup;->starRadius:I

    mul-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    iput p1, p0, Lcom/squareup/ui/StarGroup;->wiggleRoom:F

    .line 61
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    const/4 p2, 0x1

    .line 62
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setFlags(I)V

    .line 63
    sget-object p2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 65
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2, p1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object p2, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/ui/StarGroup;->correctColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 68
    new-instance p1, Landroid/graphics/Paint;

    iget-object p2, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object p1, p0, Lcom/squareup/ui/StarGroup;->circlePaint:Landroid/graphics/Paint;

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/StarGroup;->circlePaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 70
    sget p1, Lcom/squareup/widgets/pos/R$dimen;->pin_star_stroke:I

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/squareup/ui/StarGroup;->strokeWidth:F

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/StarGroup;->circlePaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/ui/StarGroup;->strokeWidth:F

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/StarGroup;->buildAnimation()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/StarGroup;)Landroid/graphics/Paint;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/StarGroup;I)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/ui/StarGroup;->starCount(I)V

    return-void
.end method

.method private buildAnimation()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [F

    .line 194
    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/StarGroup;->wiggleAnimator:Landroid/animation/ValueAnimator;

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->wiggleAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->wiggleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/squareup/ui/-$$Lambda$StarGroup$zSDpXNISlgnsRDOJ0hkgomE6MZE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/-$$Lambda$StarGroup$zSDpXNISlgnsRDOJ0hkgomE6MZE;-><init>(Lcom/squareup/ui/StarGroup;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->wiggleAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/squareup/ui/StarGroup$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/StarGroup$1;-><init>(Lcom/squareup/ui/StarGroup;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
    .end array-data
.end method

.method private starCount(I)V
    .locals 1

    .line 159
    iget v0, p0, Lcom/squareup/ui/StarGroup;->expectedStars:I

    if-lez v0, :cond_1

    if-lt v0, p1, :cond_0

    goto :goto_0

    .line 160
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "starCount exceeds expectedStars"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 162
    :cond_1
    :goto_0
    iput p1, p0, Lcom/squareup/ui/StarGroup;->starCount:I

    .line 163
    invoke-virtual {p0}, Lcom/squareup/ui/StarGroup;->invalidate()V

    .line 164
    invoke-virtual {p0}, Lcom/squareup/ui/StarGroup;->requestLayout()V

    return-void
.end method


# virtual methods
.method public getStarCount()I
    .locals 1

    .line 150
    iget v0, p0, Lcom/squareup/ui/StarGroup;->starCount:I

    return v0
.end method

.method public synthetic lambda$buildAnimation$0$StarGroup(Landroid/animation/ValueAnimator;)V
    .locals 2

    .line 197
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/squareup/ui/StarGroup;->wiggle:F

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    const/high16 p1, 0x437f0000    # 255.0f

    mul-float v1, v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 199
    invoke-virtual {p0}, Lcom/squareup/ui/StarGroup;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 128
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 130
    iget v0, p0, Lcom/squareup/ui/StarGroup;->starCount:I

    iget v1, p0, Lcom/squareup/ui/StarGroup;->expectedStars:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 131
    iget v1, p0, Lcom/squareup/ui/StarGroup;->drawingRadius:F

    iget v2, p0, Lcom/squareup/ui/StarGroup;->drawingWiggleRoom:F

    iget v3, p0, Lcom/squareup/ui/StarGroup;->wiggle:F

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    mul-float v2, v2, v3

    add-float/2addr v1, v2

    .line 132
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_3

    .line 134
    iget v4, p0, Lcom/squareup/ui/StarGroup;->expectedStars:I

    if-lez v4, :cond_1

    .line 135
    iget v4, p0, Lcom/squareup/ui/StarGroup;->starCount:I

    if-ge v3, v4, :cond_0

    iget v4, p0, Lcom/squareup/ui/StarGroup;->emptyRadius:F

    iget-object v5, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 136
    :cond_0
    iget v4, p0, Lcom/squareup/ui/StarGroup;->emptyRadius:F

    iget-object v5, p0, Lcom/squareup/ui/StarGroup;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 138
    :cond_1
    iget v4, p0, Lcom/squareup/ui/StarGroup;->drawingRadius:F

    iget-object v5, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 140
    :goto_1
    iget v4, p0, Lcom/squareup/ui/StarGroup;->starCount:I

    if-ge v3, v4, :cond_2

    .line 141
    iget v4, p0, Lcom/squareup/ui/StarGroup;->drawingRadius:F

    iget-object v5, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 143
    :cond_2
    iget v4, p0, Lcom/squareup/ui/StarGroup;->emptyRadius:F

    iget-object v5, p0, Lcom/squareup/ui/StarGroup;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 145
    :goto_2
    iget v4, p0, Lcom/squareup/ui/StarGroup;->drawingStep:F

    add-float/2addr v1, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .line 76
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 77
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 78
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 79
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 81
    iget v2, p0, Lcom/squareup/ui/StarGroup;->starCount:I

    iget v3, p0, Lcom/squareup/ui/StarGroup;->expectedStars:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    .line 83
    iget v4, p0, Lcom/squareup/ui/StarGroup;->starGap:I

    mul-int v3, v3, v4

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    .line 84
    iget v5, p0, Lcom/squareup/ui/StarGroup;->starRadius:I

    mul-int/lit8 v5, v5, 0x2

    mul-int v5, v5, v2

    int-to-float v2, v5

    add-float/2addr v2, v3

    iget v3, p0, Lcom/squareup/ui/StarGroup;->wiggleRoom:F

    const/high16 v5, 0x40000000    # 2.0f

    mul-float v3, v3, v5

    add-float/2addr v2, v3

    int-to-float v3, p1

    cmpl-float v6, v2, v3

    if-lez v6, :cond_0

    const/4 v4, 0x1

    :cond_0
    const/high16 v6, -0x80000000

    const/high16 v7, 0x40000000    # 2.0f

    if-ne v0, v7, :cond_1

    goto :goto_0

    :cond_1
    if-ne v0, v6, :cond_2

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_2
    float-to-double v8, v2

    .line 94
    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int p1, v8

    :goto_0
    if-eqz v4, :cond_3

    div-float/2addr v3, v2

    .line 99
    iget v0, p0, Lcom/squareup/ui/StarGroup;->starRadius:I

    int-to-float v0, v0

    mul-float v0, v0, v3

    iput v0, p0, Lcom/squareup/ui/StarGroup;->drawingRadius:F

    .line 100
    iget v0, p0, Lcom/squareup/ui/StarGroup;->starGap:I

    int-to-float v0, v0

    mul-float v0, v0, v3

    iput v0, p0, Lcom/squareup/ui/StarGroup;->drawingGap:F

    .line 101
    iget v0, p0, Lcom/squareup/ui/StarGroup;->wiggleRoom:F

    mul-float v0, v0, v3

    iput v0, p0, Lcom/squareup/ui/StarGroup;->drawingWiggleRoom:F

    goto :goto_1

    .line 103
    :cond_3
    iget v0, p0, Lcom/squareup/ui/StarGroup;->starRadius:I

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/ui/StarGroup;->drawingRadius:F

    .line 104
    iget v0, p0, Lcom/squareup/ui/StarGroup;->starGap:I

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/ui/StarGroup;->drawingGap:F

    .line 105
    iget v0, p0, Lcom/squareup/ui/StarGroup;->wiggleRoom:F

    iput v0, p0, Lcom/squareup/ui/StarGroup;->drawingWiggleRoom:F

    .line 107
    :goto_1
    iget v0, p0, Lcom/squareup/ui/StarGroup;->drawingGap:F

    iget v2, p0, Lcom/squareup/ui/StarGroup;->drawingRadius:F

    mul-float v2, v2, v5

    add-float/2addr v0, v2

    iput v0, p0, Lcom/squareup/ui/StarGroup;->drawingStep:F

    .line 110
    iget v0, p0, Lcom/squareup/ui/StarGroup;->starRadius:I

    iget v2, p0, Lcom/squareup/ui/StarGroup;->starGap:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x2

    if-eq v1, v6, :cond_4

    if-eq v1, v7, :cond_5

    move p2, v0

    goto :goto_2

    .line 116
    :cond_4
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 123
    :cond_5
    :goto_2
    iget v0, p0, Lcom/squareup/ui/StarGroup;->drawingRadius:F

    iget v1, p0, Lcom/squareup/ui/StarGroup;->strokeWidth:F

    div-float/2addr v1, v5

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/squareup/ui/StarGroup;->emptyRadius:F

    .line 124
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/StarGroup;->setMeasuredDimension(II)V

    return-void
.end method

.method public setCorrect(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/squareup/ui/StarGroup;->correctColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 170
    iget-object p1, p0, Lcom/squareup/ui/StarGroup;->circlePaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/squareup/ui/StarGroup;->correctColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 172
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/squareup/ui/StarGroup;->incorrectColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/StarGroup;->circlePaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/squareup/ui/StarGroup;->incorrectColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 175
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/StarGroup;->invalidate()V

    return-void
.end method

.method public setCorrectColor(I)V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 190
    invoke-virtual {p0}, Lcom/squareup/ui/StarGroup;->invalidate()V

    return-void
.end method

.method public setExpectedStarCount(I)V
    .locals 0

    .line 179
    iput p1, p0, Lcom/squareup/ui/StarGroup;->expectedStars:I

    return-void
.end method

.method public setStarCount(I)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->wiggleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->wiggleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 155
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/StarGroup;->starCount(I)V

    return-void
.end method

.method public wiggleClear()V
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/ui/StarGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->vibrate(Landroid/content/Context;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/StarGroup;->wiggleAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method
