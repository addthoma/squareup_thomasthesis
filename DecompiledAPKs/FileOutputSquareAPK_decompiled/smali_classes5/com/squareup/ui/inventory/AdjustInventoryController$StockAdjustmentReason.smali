.class Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;
.super Ljava/lang/Object;
.source "AdjustInventoryController.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StockAdjustmentReason"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field final isRecount:Z

.field final labelResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 539
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason$1;

    invoke-direct {v0}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V
    .locals 0

    .line 533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534
    iput p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->labelResId:I

    .line 535
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 536
    iput-boolean p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->isRecount:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 559
    iget p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->labelResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 560
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {p2}, Lcom/squareup/protos/client/InventoryAdjustmentReason;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 561
    iget-boolean p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->isRecount:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
