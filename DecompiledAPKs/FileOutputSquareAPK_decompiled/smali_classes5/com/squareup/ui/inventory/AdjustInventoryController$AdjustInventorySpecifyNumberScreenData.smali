.class Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;
.super Ljava/lang/Object;
.source "AdjustInventoryController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AdjustInventorySpecifyNumberScreenData"
.end annotation


# instance fields
.field final isInitialReceive:Z

.field final precision:I

.field final reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

.field final serverCount:Ljava/math/BigDecimal;

.field final shouldShowUnitCost:Z

.field final unitAbbreviation:Ljava/lang/String;

.field final userInputCount:Ljava/math/BigDecimal;

.field final userInputUnitCost:Lcom/squareup/protos/common/Money;


# direct methods
.method constructor <init>(ZLcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;ZLjava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V
    .locals 0

    .line 502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 503
    iput-boolean p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->isInitialReceive:Z

    .line 504
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 505
    iput-boolean p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->shouldShowUnitCost:Z

    .line 506
    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->serverCount:Ljava/math/BigDecimal;

    .line 507
    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->userInputCount:Ljava/math/BigDecimal;

    .line 508
    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    .line 509
    iput p7, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->precision:I

    .line 510
    iput-object p8, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->unitAbbreviation:Ljava/lang/String;

    return-void
.end method
