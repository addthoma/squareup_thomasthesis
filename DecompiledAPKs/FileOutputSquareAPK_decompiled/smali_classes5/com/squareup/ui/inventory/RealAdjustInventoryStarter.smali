.class public Lcom/squareup/ui/inventory/RealAdjustInventoryStarter;
.super Ljava/lang/Object;
.source "RealAdjustInventoryStarter.java"

# interfaces
.implements Lcom/squareup/ui/inventory/AdjustInventoryStarter;


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/inventory/RealAdjustInventoryStarter;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public startAdjustInventoryFlow(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V
    .locals 11

    move-object v0, p0

    .line 49
    new-instance v10, Lcom/squareup/ui/inventory/AdjustInventoryScope;

    move-object v1, v10

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/inventory/AdjustInventoryScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V

    if-eqz p2, :cond_0

    .line 60
    iget-object v1, v0, Lcom/squareup/ui/inventory/RealAdjustInventoryStarter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;

    invoke-direct {v2, v10}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :cond_0
    iget-object v1, v0, Lcom/squareup/ui/inventory/RealAdjustInventoryStarter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberScreen;

    invoke-direct {v2, v10}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberScreen;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public startAdjustInventoryFlowWithWholeNumberCount(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/common/Money;)V
    .locals 9

    if-nez p5, :cond_0

    const/4 p5, 0x0

    goto :goto_0

    .line 35
    :cond_0
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p5

    :goto_0
    move-object v5, p5

    const/4 v7, 0x0

    const-string v8, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p6

    .line 33
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/ui/inventory/RealAdjustInventoryStarter;->startAdjustInventoryFlow(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V

    return-void
.end method
