.class public final Lcom/squareup/ui/AndroidNfcState_Factory;
.super Ljava/lang/Object;
.source "AndroidNfcState_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/AndroidNfcState;",
        ">;"
    }
.end annotation


# instance fields
.field private final nfcAdapterShimProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/NfcAdapterShim;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/NfcAdapterShim;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/AndroidNfcState_Factory;->nfcAdapterShimProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/AndroidNfcState_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/NfcAdapterShim;",
            ">;)",
            "Lcom/squareup/ui/AndroidNfcState_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/AndroidNfcState_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/AndroidNfcState_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/NfcAdapterShim;)Lcom/squareup/ui/AndroidNfcState;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/AndroidNfcState;

    invoke-direct {v0, p0}, Lcom/squareup/ui/AndroidNfcState;-><init>(Lcom/squareup/util/NfcAdapterShim;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/AndroidNfcState;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/AndroidNfcState_Factory;->nfcAdapterShimProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/NfcAdapterShim;

    invoke-static {v0}, Lcom/squareup/ui/AndroidNfcState_Factory;->newInstance(Lcom/squareup/util/NfcAdapterShim;)Lcom/squareup/ui/AndroidNfcState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/AndroidNfcState_Factory;->get()Lcom/squareup/ui/AndroidNfcState;

    move-result-object v0

    return-object v0
.end method
