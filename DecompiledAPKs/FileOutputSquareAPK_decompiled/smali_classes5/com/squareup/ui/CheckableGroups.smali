.class public abstract Lcom/squareup/ui/CheckableGroups;
.super Ljava/lang/Object;
.source "CheckableGroups.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addAsRows(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;Ljava/util/List;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lcom/squareup/widgets/CheckableGroup;",
            "Ljava/util/List<",
            "*>;Z)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 31
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 32
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v0, v1, p3}, Lcom/squareup/ui/CheckableGroups;->addCheckableRow(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;ILjava/lang/CharSequence;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static addAsRows(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;[Ljava/lang/Object;Z)V
    .locals 2

    const/4 v0, 0x0

    .line 42
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 43
    aget-object v1, p2, v0

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v0, v1, p3}, Lcom/squareup/ui/CheckableGroups;->addCheckableRow(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;ILjava/lang/CharSequence;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static addCheckableRow(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;ILjava/lang/CharSequence;Z)V
    .locals 2

    .line 13
    sget v0, Lcom/squareup/widgets/pos/R$layout;->single_choice_list_item:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 14
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setId(I)V

    .line 15
    invoke-virtual {p0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p4, :cond_0

    .line 17
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_pressed:I

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0

    .line 20
    :cond_0
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_list:I

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 22
    :goto_0
    invoke-virtual {p1, p0}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;)V

    return-void
.end method
