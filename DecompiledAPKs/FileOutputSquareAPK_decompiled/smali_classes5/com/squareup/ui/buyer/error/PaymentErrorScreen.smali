.class public final Lcom/squareup/ui/buyer/error/PaymentErrorScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "PaymentErrorScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/error/PaymentErrorScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/error/PaymentErrorScreen$Component;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/error/PaymentErrorScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final authFailed:Z

.field public final message:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/ui/buyer/error/-$$Lambda$PaymentErrorScreen$2p4rvgvBh2Zg4C-iXyLdAcVRxSc;->INSTANCE:Lcom/squareup/ui/buyer/error/-$$Lambda$PaymentErrorScreen$2p4rvgvBh2Zg4C-iXyLdAcVRxSc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    const-string/jumbo p1, "title"

    .line 59
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->title:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->message:Ljava/lang/String;

    .line 61
    iput-boolean p4, p0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->authFailed:Z

    return-void
.end method

.method public static authFailed(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/buyer/error/PaymentErrorScreen;
    .locals 1

    .line 32
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    sget p2, Lcom/squareup/ui/buyerflow/R$string;->authorization_failed:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 35
    :cond_0
    new-instance p1, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    const/4 v0, 0x1

    invoke-direct {p1, p0, p2, p3, v0}, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object p1
.end method

.method public static clientError(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/util/Res;)Lcom/squareup/ui/buyer/error/PaymentErrorScreen;
    .locals 3

    .line 45
    new-instance v0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->authorization_failed:I

    .line 46
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->server_error_message:I

    .line 47
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/error/PaymentErrorScreen;
    .locals 4

    .line 79
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 80
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v3, 0x1

    if-ne p0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 83
    :goto_0
    new-instance p0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object p0
.end method

.method public static offlineError(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/util/Res;)Lcom/squareup/ui/buyer/error/PaymentErrorScreen;
    .locals 3

    .line 39
    new-instance v0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->offline_payment_failed:I

    .line 40
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->offline_payment_failed_message:I

    .line 41
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 71
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 73
    iget-object p2, p0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    iget-object p2, p0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->message:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget-boolean p2, p0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->authFailed:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 87
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->payment_error_view:I

    return v0
.end method
