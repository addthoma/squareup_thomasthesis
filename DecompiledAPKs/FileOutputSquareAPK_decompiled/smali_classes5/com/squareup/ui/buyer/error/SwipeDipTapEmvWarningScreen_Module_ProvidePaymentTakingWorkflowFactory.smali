.class public final Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;
.super Ljava/lang/Object;
.source "SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final buttonFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final emvPaymentInputHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final emvRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final initialViewDataProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->module:Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->buttonFlowStarterProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->initialViewDataProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->emvPaymentInputHandlerProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p5, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->emvRunnerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p6, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->tenderStarterProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p7, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p8, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;)",
            "Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;"
        }
    .end annotation

    .line 72
    new-instance v9, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;-><init>(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static providePaymentTakingWorkflow(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
    .locals 0

    .line 80
    invoke-virtual/range {p0 .. p7}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;->providePaymentTakingWorkflow(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
    .locals 8

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->module:Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->buttonFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    iget-object v2, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->initialViewDataProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/errors/WarningScreenData;

    iget-object v3, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->emvPaymentInputHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;

    iget-object v4, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->emvRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iget-object v5, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v6, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v7, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->providePaymentTakingWorkflow(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->get()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object v0

    return-object v0
.end method
