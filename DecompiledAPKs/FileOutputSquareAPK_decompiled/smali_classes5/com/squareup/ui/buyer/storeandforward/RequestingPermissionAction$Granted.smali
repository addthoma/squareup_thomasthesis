.class public final Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;
.super Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction;
.source "StoreAndForwardQuickEnableWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Granted"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u0004*\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;",
        "Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction;",
        "()V",
        "apply",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 110
    new-instance v0, Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;->INSTANCE:Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 110
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
            ">;)",
            "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    sget-object p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult$Enable;->INSTANCE:Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult$Enable;

    check-cast p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 110
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;

    move-result-object p1

    return-object p1
.end method
