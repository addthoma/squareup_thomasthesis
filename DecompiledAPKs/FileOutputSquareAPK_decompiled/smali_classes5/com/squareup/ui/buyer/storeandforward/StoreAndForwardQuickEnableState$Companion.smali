.class public final Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$Companion;
.super Ljava/lang/Object;
.source "StoreAndForwardQuickEnableState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$Companion;",
        "",
        "()V",
        "start",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final start()Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$ShowingScreen;->INSTANCE:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$ShowingScreen;

    check-cast v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;

    return-object v0
.end method
