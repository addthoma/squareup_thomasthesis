.class public Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;
.super Ljava/lang/Object;
.source "RealBuyerFlowReceiptManager.java"

# interfaces
.implements Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;


# instance fields
.field private final hasSeparatedPrintouts:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final printingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final receiptSender:Lcom/squareup/receipt/ReceiptSender;

.field private final receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

.field private final separatedPrintoutsEnabled:Z

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/receipt/ReceiptValidator;Lcom/squareup/print/PrinterStations;Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->SEPARATED_PRINTOUTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->separatedPrintoutsEnabled:Z

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->transaction:Lcom/squareup/payment/Transaction;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

    .line 60
    iput-object p7, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 61
    iput-object p8, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->hasSeparatedPrintouts:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    return-void
.end method


# virtual methods
.method public getSeparatedPrintoutsStartArgsForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->transaction:Lcom/squareup/payment/Transaction;

    .line 231
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getOrderDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 232
    sget-object v1, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager$1;->$SwitchMap$com$squareup$checkoutflow$receipt$ReceiptResult$ReceiptSelectionType:[I

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 238
    invoke-static {v0, p2}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->noPaperReceipt(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    return-object p1

    .line 236
    :cond_0
    sget-object p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;->FORMAL:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;

    invoke-static {v0, p2, p1}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->withPaperReceipt(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    return-object p1

    .line 234
    :cond_1
    sget-object p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;->NORMAL:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;

    invoke-static {v0, p2, p1}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->withPaperReceipt(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$maybeAutoPrintReceipt$1$RealBuyerFlowReceiptManager(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 150
    invoke-static {p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 153
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithPaperReceipt()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v1, v2}, Lcom/squareup/print/PrinterStations;->getEnabledExternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v1, v2}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v1

    .line 161
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-interface {v2, p1, v0, v1}, Lcom/squareup/receipt/ReceiptSender;->autoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Ljava/util/Collection;)V

    const/4 p1, 0x1

    .line 162
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$maybePrintReceipt$0$RealBuyerFlowReceiptManager(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithPaperReceipt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getEnabledExternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v0

    .line 124
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 126
    invoke-static {p2}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v2

    .line 124
    invoke-virtual {v1, p2, v2, p1, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->printItemizedReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)V

    const/4 p1, 0x1

    .line 131
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public maybeAutoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 145
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->canAutoPrintReceipt()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 146
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

    invoke-virtual {v0, p1}, Lcom/squareup/receipt/ReceiptValidator;->waitForReceiptNumber(Lcom/squareup/payment/PaymentReceipt;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$ZxdsUZbQDsDBV7_TdVb5QYHz6lo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$ZxdsUZbQDsDBV7_TdVb5QYHz6lo;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;)V

    .line 149
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public maybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

    invoke-virtual {v0, p1}, Lcom/squareup/receipt/ReceiptValidator;->waitForReceiptNumber(Lcom/squareup/payment/PaymentReceipt;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$Ly9G3zJn-BUsP13qWb2v6d19f1c;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$Ly9G3zJn-BUsP13qWb2v6d19f1c;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    .line 113
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public printOrderStubAndTicket()V
    .locals 3

    .line 173
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->separatedPrintoutsEnabled:Z

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->transaction:Lcom/squareup/payment/Transaction;

    .line 176
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->transaction:Lcom/squareup/payment/Transaction;

    .line 177
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicketExternalOnly(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->printingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->transaction:Lcom/squareup/payment/Transaction;

    .line 181
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->transaction:Lcom/squareup/payment/Transaction;

    .line 182
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v2

    .line 180
    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public shouldReceiptScreenSkipSelection()Z
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithPaperReceipt()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->shouldSkipReceipt()Z

    move-result v0

    return v0
.end method

.method public shouldSkipReceiptScreen(Z)Z
    .locals 2

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithPaperReceipt()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->shouldPrintReceiptToSign()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->transaction:Lcom/squareup/payment/Transaction;

    .line 80
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->shouldSkipReceipt()Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public willSeparatedPrintoutsLaunchForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)Z
    .locals 1

    .line 215
    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->FORMAL_PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithNoPaperReceipt()Z

    move-result p1

    return p1

    .line 216
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithPaperReceipt()Z

    move-result p1

    return p1
.end method

.method public willSeparatedPrintoutsLaunchWithNoPaperReceipt()Z
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->hasSeparatedPrintouts:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    invoke-interface {v0}, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;->hasPrintoutsWithNoPaperReceipt()Z

    move-result v0

    return v0
.end method

.method public willSeparatedPrintoutsLaunchWithPaperReceipt()Z
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->hasSeparatedPrintouts:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    invoke-interface {v0}, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;->hasPrintoutsWithPaperReceipt()Z

    move-result v0

    return v0
.end method
