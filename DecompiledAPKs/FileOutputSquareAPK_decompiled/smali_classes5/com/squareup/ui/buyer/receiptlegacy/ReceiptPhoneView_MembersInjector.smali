.class public final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;
.super Ljava/lang/Object;
.source "ReceiptPhoneView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;",
        ">;"
    }
.end annotation


# instance fields
.field private final curatedImageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumberScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->curatedImageProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;",
            ">;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCuratedImage(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/merchantimages/CuratedImage;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    return-void
.end method

.method public static injectPhoneNumberScrubber(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/text/InsertingScrubber;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Ljava/lang/Object;)V
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->injectPresenter(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Ljava/lang/Object;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/InsertingScrubber;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->injectPhoneNumberScrubber(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/text/InsertingScrubber;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->curatedImageProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/merchantimages/CuratedImage;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->injectCuratedImage(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/merchantimages/CuratedImage;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->injectRes(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView_MembersInjector;->injectMembers(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    return-void
.end method
