.class public Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;
.super Lcom/squareup/register/widgets/ScrollToBottomView;
.source "ReceiptPhoneView.java"

# interfaces
.implements Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private bottomPanel:Landroid/view/View;

.field curatedImage:Lcom/squareup/merchantimages/CuratedImage;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

.field private customerButton:Lcom/squareup/glyph/SquareGlyphView;

.field private emailDisclaimer:Lcom/squareup/widgets/MessageView;

.field private emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

.field private frame:Landroid/view/ViewGroup;

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private merchantImageView:Landroid/widget/ImageView;

.field private newSaleButton:Landroid/widget/TextView;

.field private noReceiptButton:Landroid/widget/TextView;

.field private onNewSaleClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private printButton:Landroid/widget/TextView;

.field private printFormalReceiptButton:Landroid/view/View;

.field private receiptHint:Lcom/squareup/widgets/MessageView;

.field private remainingBalance:Lcom/squareup/widgets/MessageView;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private smsContainer:Landroid/view/View;

.field private smsDisclaimer:Lcom/squareup/widgets/MessageView;

.field private smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

.field private ticketName:Landroid/widget/TextView;

.field private title:Lcom/squareup/widgets/ScalingTextView;

.field private topPanel:Landroid/view/View;

.field private topPanelLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 107
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/ScrollToBottomView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 108
    const-class p2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$PhoneComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$PhoneComponent;

    invoke-interface {p1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$PhoneComponent;->inject(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->onReceiptAnimationEnd()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->udpateViewAppearanceForMerchantImage()V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;II)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setMerchantImageViewSize(II)V

    return-void
.end method

.method private animate(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V
    .locals 6

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->bottomPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    const/4 v1, 0x2

    div-int/2addr v0, v1

    const/4 v2, 0x1

    xor-int/2addr p2, v2

    if-eqz p2, :cond_0

    .line 353
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->flipTo(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 355
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 359
    :cond_1
    :goto_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->convertTopPanelToFixedHeight(I)V

    new-array p1, v1, [F

    .line 361
    fill-array-data p1, :array_0

    const-string p2, "alpha"

    invoke-static {p2, p1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object p1

    new-array p2, v1, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v3, p2, v4

    int-to-float v0, v0

    aput v0, p2, v2

    const-string/jumbo v0, "translationY"

    .line 362
    invoke-static {v0, p2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object p2

    .line 364
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 365
    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->bottomPanel:Landroid/view/View;

    new-array v5, v1, [Landroid/animation/PropertyValuesHolder;

    aput-object p1, v5, v4

    aput-object p2, v5, v2

    invoke-static {v3, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    new-array v3, v2, [Landroid/animation/PropertyValuesHolder;

    aput-object p2, v3, v4

    invoke-static {p1, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object p1

    .line 368
    invoke-static {p1, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 369
    new-instance p2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$6;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$6;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    invoke-virtual {p1, p2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 374
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array p1, v1, [I

    .line 376
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p2

    aput p2, p1, v4

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->getWindowHeight()I

    move-result p2

    aput p2, p1, v2

    invoke-static {p1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object p1

    .line 377
    new-instance p2, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptPhoneView$cp4D3XDt4BGnLTO-BTYebsTM8jU;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptPhoneView$cp4D3XDt4BGnLTO-BTYebsTM8jU;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 382
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    new-instance p1, Landroid/animation/AnimatorSet;

    invoke-direct {p1}, Landroid/animation/AnimatorSet;-><init>()V

    const-wide/16 v1, 0x12c

    .line 385
    invoke-virtual {p1, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 386
    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 388
    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private convertTopPanelToFixedHeight(I)V
    .locals 3

    .line 392
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 393
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/lit8 v2, p1, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    neg-int p1, p1

    .line 394
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 395
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    const/4 p1, 0x0

    .line 396
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 397
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private disableInputFields()V
    .locals 2

    .line 306
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;->setEnabled(Z)V

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/library/GlyphButtonEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setEnabled(Z)V

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanelLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanelLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    const/4 v0, 0x0

    .line 315
    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanelLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    :cond_0
    return-void
.end method

.method private getWindowHeight()I
    .locals 3

    .line 430
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->getDisplaySize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 432
    invoke-virtual {p0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->getLocationOnScreen([I)V

    const/4 v2, 0x1

    .line 433
    aget v1, v1, v2

    sub-int/2addr v0, v1

    return v0
.end method

.method private initEmailInput()V
    .locals 4

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$4;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/text/EmailScrubber;->watcher(Lcom/squareup/text/HasSelectableText;)Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    new-instance v1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setAutoCompleteAdapter(Landroid/widget/ArrayAdapter;)V

    return-void
.end method

.method private initSmsInput()V
    .locals 3

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$5;

    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    invoke-direct {v1, p0, v2, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$5;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private onReceiptAnimationEnd()V
    .locals 2

    .line 438
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->frame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 439
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 440
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->frame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 442
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 443
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 444
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 446
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->bottomPanel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private setMerchantImageViewSize(II)V
    .locals 2

    .line 509
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 510
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v1, p1, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v1, p2, :cond_1

    .line 511
    :cond_0
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 512
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 513
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method private setTopPanelSize(II)V
    .locals 2

    .line 518
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 519
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v1, p1, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v1, p2, :cond_1

    .line 520
    :cond_0
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 521
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 522
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method private udpateViewAppearanceForMerchantImage()V
    .locals 3

    .line 491
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 492
    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 493
    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_black_transparent_fifty_pressed:I

    .line 495
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->title:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v2, v0}, Lcom/squareup/widgets/ScalingTextView;->setTextColor(I)V

    .line 496
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->remainingBalance:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v2, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 497
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v2, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 498
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->newSaleButton:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 499
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->newSaleButton:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 501
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 502
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    .line 504
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 505
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    return-void
.end method


# virtual methods
.method public asView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public displayPostSelectionStage(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V
    .locals 0

    .line 299
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->disableInputFields()V

    .line 302
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->animate(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    return-void
.end method

.method public displayPostSelectionStageImmediately(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 281
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->disableInputFields()V

    if-eqz p1, :cond_0

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 286
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->bottomPanel:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 288
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    const/4 v0, -0x1

    .line 289
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 290
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 291
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    .line 292
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 293
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 295
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->requestLayout()V

    return-void
.end method

.method public enableClickAnywhereToFinish()V
    .locals 3

    .line 401
    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$7;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$7;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    .line 407
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->frame:Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 408
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphView;->setClickable(Z)V

    .line 409
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->title:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/ScalingTextView;->setClickable(Z)V

    .line 410
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->remainingBalance:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setClickable(Z)V

    .line 411
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setClickable(Z)V

    .line 413
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->frame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 414
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->title:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/ScalingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 416
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->remainingBalance:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 417
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public enableNoReceiptButton()V
    .locals 2

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->noReceiptButton:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public enableSendEmailButton(Z)V
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setButtonEnabled(Z)V

    return-void
.end method

.method public enableSendSmsButton(Z)V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/library/GlyphButtonEditText;->setButtonEnabled(Z)V

    return-void
.end method

.method public getEmailString()Ljava/lang/String;
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSmsString()Ljava/lang/String;
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/library/GlyphButtonEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hideGlyphView()V
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method public isPrintButtonVisible()Z
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->printButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSmsInputVisible()Z
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$animate$0$ReceiptPhoneView(Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 379
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 380
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public synthetic lambda$setBackgroundImage$1$ReceiptPhoneView(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
    .locals 1

    .line 471
    invoke-direct {p0, p4, p3}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setMerchantImageViewSize(II)V

    .line 472
    invoke-direct {p0, p4, p3}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setTopPanelSize(II)V

    .line 474
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;

    invoke-direct {v0, p0, p4, p3}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;II)V

    invoke-virtual {p1, p2, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->dropView(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Lcom/squareup/merchantimages/CuratedImage;->cancelRequest(Landroid/widget/ImageView;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanelLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanelLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    const/4 v0, 0x0

    .line 176
    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanelLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 178
    :cond_0
    invoke-super {p0}, Lcom/squareup/register/widgets/ScrollToBottomView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 112
    invoke-super {p0}, Lcom/squareup/register/widgets/ScrollToBottomView;->onFinishInflate()V

    .line 113
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->receipt_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->title:Lcom/squareup/widgets/ScalingTextView;

    .line 114
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->remaining_balance:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->remainingBalance:Lcom/squareup/widgets/MessageView;

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->remainingBalance:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setFreezesText(Z)V

    .line 116
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->receipt_subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setFreezesText(Z)V

    .line 118
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->receipt_top_panel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    .line 119
    sget v0, Lcom/squareup/checkout/R$id;->merchant_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    .line 120
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->bottom_panel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->bottomPanel:Landroid/view/View;

    .line 121
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->sms_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsContainer:Landroid/view/View;

    .line 122
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->sms_receipt_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/GlyphButtonEditText;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

    .line 123
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->sms_disclaimer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsDisclaimer:Lcom/squareup/widgets/MessageView;

    .line 124
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_receipt_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    .line 125
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_disclaimer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailDisclaimer:Lcom/squareup/widgets/MessageView;

    .line 126
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 127
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->no_receipt_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->noReceiptButton:Landroid/widget/TextView;

    .line 128
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_send_receipt_digital_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->receiptHint:Lcom/squareup/widgets/MessageView;

    .line 129
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->new_sale:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->newSaleButton:Landroid/widget/TextView;

    .line 130
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->customer_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 131
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->customer_add_card_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 132
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_ticket_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->ticketName:Landroid/widget/TextView;

    .line 133
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->receipt_frame:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->frame:Landroid/view/ViewGroup;

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->frame:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 135
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->print_receipt_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->printButton:Landroid/widget/TextView;

    .line 136
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->print_formal_receipt_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->printFormalReceiptButton:Landroid/view/View;

    .line 137
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->switch_language_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->newSaleButton:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->onNewSaleClicked:Lrx/Observable;

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$1;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$2;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$3;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->initEmailInput()V

    .line 161
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->initSmsInput()V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->takeView(Ljava/lang/Object;)V

    .line 168
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->focus_grabber:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onNewSaleClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 537
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->onNewSaleClicked:Lrx/Observable;

    return-object v0
.end method

.method public resourcesUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 3

    .line 545
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 546
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 547
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    sget v2, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    .line 548
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v2, 0x0

    .line 547
    invoke-virtual {v1, v0, v2, v2, v2}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 550
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->printButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$9;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$9;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 556
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->printFormalReceiptButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$10;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$10;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 562
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->noReceiptButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$11;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$11;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$12;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$12;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 573
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$13;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$13;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 583
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$14;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$14;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$15;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$15;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    return-void
.end method

.method public setAmountSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->remainingBalance:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->remainingBalance:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setBackgroundImage(Lcom/squareup/picasso/RequestCreator;)V
    .locals 2

    .line 459
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->hideGlyphView()V

    .line 460
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->merchantImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 462
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->shouldSkipReceipt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->bottomPanel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->topPanel:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptPhoneView$73msv9wS-nS3WBXz1rA8VdVg6Cg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptPhoneView$73msv9wS-nS3WBXz1rA8VdVg6Cg;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/picasso/RequestCreator;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method public setDigitalReceiptHint(Ljava/lang/CharSequence;)V
    .locals 2

    .line 332
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->receiptHint:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_0

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->receiptHint:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->receiptHint:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public setEmailInputHint(Ljava/lang/String;)V
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailInput:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method public setEmailReceiptDisclaimer(Ljava/lang/CharSequence;)V
    .locals 1

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->emailDisclaimer:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setNewSaleText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 527
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->newSaleButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setNoReceiptText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->noReceiptButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPrintButtonText(Ljava/lang/String;)V
    .locals 1

    .line 604
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->printButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSmsInputHint(Ljava/lang/String;)V
    .locals 1

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsInput:Lcom/squareup/ui/library/GlyphButtonEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/library/GlyphButtonEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method public setSmsReceiptDisclaimer(Ljava/lang/CharSequence;)V
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsDisclaimer:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSubtitle(ILcom/squareup/util/Res;)V
    .locals 1

    if-gez p1, :cond_0

    .line 261
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_0

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setThanks(Lcom/squareup/util/Res;)V
    .locals 3

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->title:Lcom/squareup/widgets/ScalingTextView;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_headline:I

    .line 275
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    .line 274
    invoke-virtual {v0, v2, v1}, Lcom/squareup/widgets/ScalingTextView;->setTextSize(IF)V

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->title:Lcom/squareup/widgets/ScalingTextView;

    sget v1, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, v2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setTicketName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 532
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->ticketName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->ticketName:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 3

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->title:Lcom/squareup/widgets/ScalingTextView;

    .line 250
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$dimen;->marin_text_header_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    .line 249
    invoke-virtual {v0, v2, v1}, Lcom/squareup/widgets/ScalingTextView;->setTextSize(IF)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->title:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showBuyerLanguageSelection()V
    .locals 2

    .line 600
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method public showCustomerAddCardButton(Z)V
    .locals 1

    .line 455
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showCustomerButton(Z)V
    .locals 1

    .line 450
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 451
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method public showPrintFormalReceiptButton()V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->printFormalReceiptButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showPrintReceiptButton()V
    .locals 2

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->printButton:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public showSmsInput(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 205
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->smsContainer:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public updateEmail(Lcom/squareup/util/Res;)V
    .locals 1

    .line 421
    sget v0, Lcom/squareup/activity/R$string;->receipt_email:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setEmailInputHint(Ljava/lang/String;)V

    return-void
.end method

.method public updateSms(Lcom/squareup/util/Res;)V
    .locals 1

    .line 425
    sget v0, Lcom/squareup/activity/R$string;->receipt_sms:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setSmsInputHint(Ljava/lang/String;)V

    return-void
.end method
