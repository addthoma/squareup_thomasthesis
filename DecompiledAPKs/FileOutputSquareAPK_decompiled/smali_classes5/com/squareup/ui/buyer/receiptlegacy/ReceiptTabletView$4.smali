.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$4;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "ReceiptTabletView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->initializeEmailInput()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$4;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 225
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$4;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->updateEmailEnabledStates()V

    return-void
.end method
