.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$5;
.super Lcom/squareup/text/ScrubbingTextWatcher;
.source "ReceiptPhoneView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->initSmsInput()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$5;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    invoke-direct {p0, p2, p3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 198
    invoke-super {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 199
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$5;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->updateSmsEnabledStates()V

    return-void
.end method
