.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;
.super Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;
.source "ReceiptPhonePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter<",
        "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private selectionGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/print/PrinterStations;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V
    .locals 32
    .param p17    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p18    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/receipt/ReceiptSender;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/merchantimages/CuratedImage;",
            "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
            "Lcom/squareup/settings/server/Features;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/print/PrintSettings;",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v15, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v22, p16

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move-object/from16 v16, p19

    move-object/from16 v19, p20

    move-object/from16 v20, p21

    move-object/from16 v21, p22

    move-object/from16 v23, p23

    move-object/from16 v24, p24

    move-object/from16 v25, p25

    move-object/from16 v26, p26

    move-object/from16 v27, p27

    move-object/from16 v28, p28

    move-object/from16 v29, p29

    move-object/from16 v30, p30

    move-object/from16 v31, p31

    .line 90
    invoke-direct/range {v0 .. v31}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/print/PrinterStations;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/payment/OfflineModeMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    .line 72
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->selectionGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-void
.end method

.method private finishSendReceipt(ILcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 320
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    if-nez v0, :cond_0

    return-void

    .line 322
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->isReceiptDeferred()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 323
    sget p1, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_all_done:I

    .line 325
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->setThanksOrRemoveCard(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 326
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setSubtitle(ILcom/squareup/util/Res;)V

    .line 327
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->onShowingThanks()V

    return-void
.end method

.method private onReceiptOptionSelected(ILcom/squareup/glyph/GlyphTypeface$Glyph;ZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Z)V
    .locals 0

    .line 333
    invoke-direct {p0, p1, p4}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->finishSendReceipt(ILcom/squareup/locale/LocaleOverrideFactory;)V

    .line 334
    invoke-direct {p0, p2, p3, p5, p6}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->setPostSelectionStage(Lcom/squareup/glyph/GlyphTypeface$Glyph;ZLcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Z)V

    .line 335
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {p1}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    .line 336
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->receiptOptionSelected()V

    return-void
.end method

.method private printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 8

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    .line 237
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithPaperReceipt()Z

    move-result v7

    .line 238
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->tryPrintReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Z

    move-result v0

    .line 239
    sget-object v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-ne p1, v1, :cond_0

    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->FORMAL_PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    :goto_0
    if-eqz v7, :cond_1

    const/4 v2, -0x1

    .line 246
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p2

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->onReceiptOptionSelected(ILcom/squareup/glyph/GlyphTypeface$Glyph;ZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Z)V

    .line 248
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->maybeStartSeparatedPrintouts(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    .line 250
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->scheduleFinishIfHasReceiptScreenTimeout()V

    .line 251
    sget v2, Lcom/squareup/billhistoryui/R$string;->buyer_send_receipt_printed:I

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p2

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->onReceiptOptionSelected(ILcom/squareup/glyph/GlyphTypeface$Glyph;ZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Z)V

    :cond_2
    :goto_1
    return-void
.end method

.method private scheduleFinishIfHasReceiptScreenTimeout()V
    .locals 1

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->hasReceiptAutoCloseOverride()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->doScheduleFinish()V

    :cond_0
    return-void
.end method

.method private setPostSelectionStage(Lcom/squareup/glyph/GlyphTypeface$Glyph;ZLcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Z)V
    .locals 0

    if-nez p4, :cond_0

    .line 310
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->endTransaction()V

    .line 312
    :cond_0
    iget-object p4, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p4, p3}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setReceiptSelectionMade(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    .line 313
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->selectionGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 314
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->displayPostSelectionStage(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 315
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->maybeEnableClickAnywhereToFinish()V

    .line 316
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->maybeGoToLoyaltyOrEmailCollectionScreen()V

    return-void
.end method

.method private setPostSelectionStageImmediately(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 293
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    .line 294
    invoke-direct {p0, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->setThanksOrRemoveCard(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 295
    sget v1, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_all_done:I

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setSubtitle(ILcom/squareup/util/Res;)V

    .line 296
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->endTransaction()V

    .line 297
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p2}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result p2

    if-nez p2, :cond_0

    .line 298
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->NO_RECEIPT:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setReceiptSelectionMade(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    .line 300
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->displayPostSelectionStageImmediately(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 301
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->maybeEnableClickAnywhereToFinish()V

    .line 302
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->onShowingThanks()V

    .line 303
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->maybeGoToLoyaltyOrEmailCollectionScreen()V

    return-void
.end method

.method private setSubtitle(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 277
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->receiptSelectionMade()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_all_done:I

    goto :goto_0

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_1

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_digital_subtitle:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/squareup/checkout/R$string;->buyer_send_receipt_subtitle:I

    .line 284
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    invoke-virtual {v1, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setSubtitle(ILcom/squareup/util/Res;)V

    return-void
.end method

.method private setThanksOrRemoveCard(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 340
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    .line 341
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    .line 342
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->shouldRemoveCard()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 343
    sget v1, Lcom/squareup/cardreader/R$string;->please_remove_card_title:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 345
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setThanks(Lcom/squareup/util/Res;)V

    :goto_0
    return-void
.end method


# virtual methods
.method noReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 8

    .line 258
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->tryDeclineReceipt()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    .line 260
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithNoPaperReceipt()Z

    move-result v0

    .line 261
    sget v2, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_all_done:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    sget-object v6, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->NO_RECEIPT:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-object v1, p0

    move-object v5, p1

    move v7, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->onReceiptOptionSelected(ILcom/squareup/glyph/GlyphTypeface$Glyph;ZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Z)V

    if-eqz v0, :cond_0

    .line 264
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->NO_RECEIPT:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->maybeStartSeparatedPrintouts(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    goto :goto_0

    .line 266
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->scheduleFinishIfHasReceiptScreenTimeout()V

    :cond_1
    :goto_0
    return-void
.end method

.method onNewSaleClicked()V
    .locals 3

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getReceiptSelectionMade()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    .line 162
    invoke-interface {v1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result v1

    if-nez v1, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->tryDeclineReceipt()Z

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->uniqueKey:Ljava/lang/String;

    .line 171
    invoke-interface {v1, v0, v2}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->getSeparatedPrintoutsStartArgsForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->startSeparatedPrintoutsWorkflow(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    goto :goto_0

    .line 176
    :cond_1
    invoke-super {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->onNewSaleClicked()V

    :goto_0
    return-void
.end method

.method printFormalReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 218
    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 219
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINT_FORMAL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method printReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 223
    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 224
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINT_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method sendEmailReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 8

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_EMAIL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 182
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->trySendEmailReceipt()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->logEmailReceiptSent()V

    .line 184
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->useValidEmailAddress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getValidEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidEmailAddress(Ljava/lang/String;)V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    .line 188
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithNoPaperReceipt()Z

    move-result v0

    .line 189
    sget v2, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_all_done_email:I

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v4, 0x0

    sget-object v6, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->DIGITAL:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-object v1, p0

    move-object v5, p1

    move v7, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->onReceiptOptionSelected(ILcom/squareup/glyph/GlyphTypeface$Glyph;ZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Z)V

    if-eqz v0, :cond_1

    .line 192
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->DIGITAL:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->maybeStartSeparatedPrintouts(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    goto :goto_0

    .line 194
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->scheduleFinishIfHasReceiptScreenTimeout()V

    :cond_2
    :goto_0
    return-void
.end method

.method sendSmsReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 8

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_SMS_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 201
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->trySendSmsReceipt()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->useValidSmsNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getValidSmsNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidSmsNumber(Ljava/lang/String;)V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    .line 206
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithNoPaperReceipt()Z

    move-result v0

    .line 207
    sget v2, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_all_done_text:I

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_SMS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v4, 0x0

    sget-object v6, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->DIGITAL:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-object v1, p0

    move-object v5, p1

    move v7, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->onReceiptOptionSelected(ILcom/squareup/glyph/GlyphTypeface$Glyph;ZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Z)V

    if-eqz v0, :cond_1

    .line 210
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->DIGITAL:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->maybeStartSeparatedPrintouts(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    goto :goto_0

    .line 212
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->scheduleFinishIfHasReceiptScreenTimeout()V

    :cond_2
    :goto_0
    return-void
.end method

.method showAllDone(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->shouldSkipReceipt()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->shouldShowEmailSent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->setPostSelectionStageImmediately(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/locale/LocaleOverrideFactory;)V

    goto :goto_0

    .line 107
    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->setPostSelectionStageImmediately(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/locale/LocaleOverrideFactory;)V

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->selectionGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->setPostSelectionStageImmediately(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 112
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->scheduleFinishIfHasReceiptScreenTimeout()V

    return-void
.end method

.method showReceiptSection()V
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->enableNoReceiptButton()V

    return-void
.end method

.method showTenderAmountAndTip(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    return-void
.end method

.method updateMessages(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 10

    .line 120
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    .line 121
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/PaymentReceipt;->asReturnsChange()Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    move-result-object v2

    .line 123
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v3

    .line 124
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v4

    const-wide/16 v5, 0x0

    if-eqz v2, :cond_2

    .line 127
    invoke-interface {v2}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    const-string/jumbo v1, "total"

    cmp-long v9, v7, v5

    if-eqz v9, :cond_0

    .line 128
    sget v5, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_title_cash_change:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 129
    invoke-interface {v2}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    const-string v7, "amount"

    invoke-virtual {v5, v7, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 130
    invoke-interface {v2}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 131
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 128
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 132
    :cond_0
    invoke-interface {v2}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v7

    iget-object v7, v7, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v9, v7, v5

    if-eqz v9, :cond_1

    .line 133
    sget v5, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_title_cash_no_change:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 134
    invoke-interface {v2}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 135
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 133
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 137
    :cond_1
    sget v1, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change:I

    .line 138
    invoke-interface {v4, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    .line 140
    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v7, v1, v5

    if-lez v7, :cond_3

    .line 141
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 143
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->setThanksOrRemoveCard(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 145
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->setSubtitle(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 146
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->isReceiptPrintingAvailable()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 147
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->hideGlyphView()V

    .line 150
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->isPrintButtonVisible()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 151
    sget p1, Lcom/squareup/activity/R$string;->receipt_paper:I

    invoke-interface {v4, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->setPrintButtonText(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method updateStrings(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 156
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->updateMessages(Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method
