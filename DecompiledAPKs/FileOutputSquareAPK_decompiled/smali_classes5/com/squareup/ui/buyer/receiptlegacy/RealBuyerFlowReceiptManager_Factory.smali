.class public final Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;
.super Ljava/lang/Object;
.source "RealBuyerFlowReceiptManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hasSeparatedPrintoutsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final printingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->printingDispatcherProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->receiptSenderProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->receiptValidatorProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->hasSeparatedPrintoutsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
            ">;)",
            "Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;"
        }
    .end annotation

    .line 71
    new-instance v9, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/receipt/ReceiptValidator;Lcom/squareup/print/PrinterStations;Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;)Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;
    .locals 10

    .line 78
    new-instance v9, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/receipt/ReceiptValidator;Lcom/squareup/print/PrinterStations;Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;
    .locals 9

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->printingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->receiptSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/receipt/ReceiptSender;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->receiptValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/receipt/ReceiptValidator;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->hasSeparatedPrintoutsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/receipt/ReceiptValidator;Lcom/squareup/print/PrinterStations;Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;)Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager_Factory;->get()Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;

    move-result-object v0

    return-object v0
.end method
