.class public final synthetic Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;

.field private final synthetic f$1:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final synthetic f$2:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final synthetic f$3:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;->f$0:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;

    iput-object p2, p0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;->f$1:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p3, p0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;->f$2:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iput-object p4, p0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;->f$3:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;->f$0:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;

    iget-object v1, p0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;->f$1:Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v2, p0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;->f$2:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v3, p0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;->f$3:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;

    move-object v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;->lambda$create$0$CancelNonEmvPaymentScreen$Factory(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;Landroid/content/DialogInterface;I)V

    return-void
.end method
