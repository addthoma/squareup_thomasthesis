.class public Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Module;
.super Ljava/lang/Object;
.source "PartialAuthWarningScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideLastAddedTender(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/tender/BaseCardTender;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 26
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/tender/BaseCardTender;

    return-object p0
.end method
