.class public final Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "RetryNonEmvTenderScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Component;,
        Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Module;,
        Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRetryNonEmvTenderScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RetryNonEmvTenderScreen.kt\ncom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,50:1\n24#2,4:51\n*E\n*S KotlinDebug\n*F\n+ 1 RetryNonEmvTenderScreen.kt\ncom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen\n*L\n45#1,4:51\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \r2\u00020\u00012\u00020\u0002:\u0003\r\u000e\u000fB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0014J\u0008\u0010\u000c\u001a\u00020\u000bH\u0017\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen;",
        "Lcom/squareup/ui/main/InBuyerScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "parent",
        "Lcom/squareup/ui/buyer/BuyerScope;",
        "(Lcom/squareup/ui/buyer/BuyerScope;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "screenLayout",
        "Companion",
        "Component",
        "Module",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen;->Companion:Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Companion;

    .line 51
    new-instance v0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 54
    sput-object v0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 42
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->retry_tender_view:I

    return v0
.end method
