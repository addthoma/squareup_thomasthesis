.class public final Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;
.super Ljava/lang/Object;
.source "RetryTenderView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/buyer/retry/RetryTenderView;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->apiTransactionStateProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/buyer/retry/RetryTenderView;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectApiTransactionState(Lcom/squareup/ui/buyer/retry/RetryTenderView;Lcom/squareup/api/ApiTransactionState;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/buyer/retry/RetryTenderView;Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->presenter:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/buyer/retry/RetryTenderView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->injectPresenter(Lcom/squareup/ui/buyer/retry/RetryTenderView;Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiTransactionState;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->injectApiTransactionState(Lcom/squareup/ui/buyer/retry/RetryTenderView;Lcom/squareup/api/ApiTransactionState;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->injectRes(Lcom/squareup/ui/buyer/retry/RetryTenderView;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/buyer/retry/RetryTenderView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/retry/RetryTenderView_MembersInjector;->injectMembers(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V

    return-void
.end method
