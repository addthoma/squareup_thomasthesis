.class public Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;
.super Lmortar/ViewPresenter;
.source "BuyerOrderTicketNamePresenter.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;,
        Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;",
        ">;",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;"
    }
.end annotation


# static fields
.field private static final CARDHOLDER_NAME_KEY:Ljava/lang/String; = "cardholder_name_key"


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private cardholderName:Ljava/lang/String;

.field private final cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

.field private final displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final readerHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final res:Lcom/squareup/util/Res;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/ui/buyer/DisplayNameProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 81
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 84
    iput-object p3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    .line 85
    iput-object p4, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 86
    iput-object p5, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    .line 87
    iput-object p10, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->readerHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    .line 88
    iput-object p11, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 89
    iput-object p7, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 90
    iput-object p8, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    .line 91
    iput-object p6, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    .line 92
    iput-object p9, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    .line 93
    iput-object p12, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 94
    iput-object p13, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    .line 95
    iput-object p14, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 96
    iput-object p15, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/cardreader/dipper/ActiveCardReader;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->handlePaymentTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method

.method private handlePaymentTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 194
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_CANCELED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 196
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 197
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->messageId:I

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 198
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 199
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->titleId:I

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 200
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 201
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 202
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method private setNameOnTextField(Ljava/lang/String;)V
    .locals 1

    .line 275
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setOrderTicketName(Ljava/lang/String;)V

    .line 276
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->selectAllText()V

    .line 277
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderClickable(Z)V

    .line 278
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    return-void
.end method

.method private shouldStartPaymentForCardholderName()Z
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasOrderTicketName()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 283
    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 284
    invoke-virtual {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartSingleTenderEmvInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private startCardholderNameFetchIfNeeded()V
    .locals 2

    .line 259
    invoke-direct {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->shouldStartPaymentForCardholderName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;->setUpEmvPayment(Z)V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    .line 263
    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminerFactory;->createForCurrentTender()Lcom/squareup/payment/TipDeterminer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminer;->showPreAuthTipScreen()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->ABORTED_PRE_AUTH:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    goto :goto_0

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;->startPaymentOnActiveReader()V

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_FETCHING:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method protected handleCardholderError()V
    .locals 4

    .line 182
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 185
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setProgressVisible(Z)V

    .line 186
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    iget-object v2, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_unavailable:I

    .line 187
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 186
    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 188
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderClickable(Z)V

    return-void
.end method

.method onBackPressed()Z
    .locals 2

    .line 219
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->hideKeyboard()V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 3

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 234
    iget-object v1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 236
    :cond_0
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    sget-object v2, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 240
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lcom/squareup/cardreader/R$string;->contactless_reader_disconnected_title:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/checkout/R$string;->emv_reader_disconnected_title:I

    .line 243
    :goto_0
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 245
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 246
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/checkout/R$string;->emv_reader_disconnected_msg:I

    .line 247
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 248
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_2
    return-void
.end method

.method onCardholderStatusClicked()V
    .locals 2

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    const-string v1, "cardholderName"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->setNameOnTextField(Ljava/lang/String;)V

    return-void
.end method

.method onCommitOrderName()V
    .locals 3

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    .line 208
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->getOrderTicketName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 209
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->hideKeyboard()V

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/payment/Transaction;->setCanRenameCart(Z)V

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setOrderTicketName(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/DisplayNameProvider;->setName(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/DisplayNameProvider;->setShouldShowDisplayName(Z)V

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstScreenAfterTicketName()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    new-instance v1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$1;)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->readerHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->scopeAttachListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    new-instance v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;

    invoke-direct {v0, p0, v2}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$1;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->setListener(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;)V

    .line 107
    invoke-direct {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->startCardholderNameFetchIfNeeded()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    if-eqz p1, :cond_0

    const-string v0, "cardholder_name_key"

    .line 112
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_8

    .line 118
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setTotal(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setSubtitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 121
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderClickable(Z)V

    .line 122
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setProgressVisible(Z)V

    const/4 v1, 0x0

    .line 125
    iget-object v2, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v2, v3}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    .line 127
    iget-object v3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->hasOrderTicketName()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 128
    iget-object v1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrderTicketName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    move-object v1, v2

    goto :goto_0

    .line 131
    :cond_2
    iget-object v3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 132
    iget-object v1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 133
    :cond_3
    iget-object v3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v3}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->ABORTED_PRE_AUTH:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 135
    iget-object v3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_unavailable:I

    .line 136
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 135
    invoke-virtual {v0, v3}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :cond_4
    iget-object v3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v3}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_FETCHING:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 139
    iget-object v3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_retrieving:I

    .line 140
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 139
    invoke-virtual {v0, v3}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    const/4 v3, 0x1

    .line 142
    invoke-virtual {v0, v3}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setProgressVisible(Z)V

    :cond_5
    :goto_0
    if-eqz v1, :cond_6

    .line 146
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setOrderTicketName(Ljava/lang/String;)V

    if-eqz v2, :cond_6

    .line 148
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->selectAllText()V

    :cond_6
    if-nez p1, :cond_7

    .line 153
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->requestInitialFocus()V

    :cond_7
    return-void

    .line 115
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Should never see an order ticket name screen with an open ticket active for a cart!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 158
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    const-string v1, "cardholder_name_key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected processNameReceived(Ljava/lang/String;)V
    .locals 3

    .line 163
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    const/4 v1, 0x0

    .line 167
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setProgressVisible(Z)V

    .line 168
    iget-object v1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v1, p1}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    .line 169
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->getOrderTicketName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 170
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->setNameOnTextField(Ljava/lang/String;)V

    goto :goto_0

    .line 172
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_name_order:I

    .line 173
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    const-string v2, "name"

    .line 174
    invoke-virtual {p1, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 175
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 176
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 172
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 177
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->setCardholderClickable(Z)V

    :goto_0
    return-void
.end method
