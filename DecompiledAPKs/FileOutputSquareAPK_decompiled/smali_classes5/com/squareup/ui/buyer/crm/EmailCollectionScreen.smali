.class public final Lcom/squareup/ui/buyer/crm/EmailCollectionScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "EmailCollectionScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Component;,
        Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMAIL_COLLECTION:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 73
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE_CROSS_FADE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 69
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->email_collection_view:I

    return v0
.end method
