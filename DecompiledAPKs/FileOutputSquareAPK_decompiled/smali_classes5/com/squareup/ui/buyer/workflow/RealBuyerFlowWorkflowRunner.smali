.class public final Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealBuyerFlowWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/ui/buyer/BuyerScopeInput;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        ">;",
        "Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0014J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0003H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000RL\u0010\u000c\u001a:\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u000f\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0010j\u0002`\u00110\u000ej\n\u0012\u0006\u0008\u0001\u0012\u00020\u000f`\u00120\r8TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;",
        "Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/ui/buyer/BuyerScopeInput;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        "buyerScopeWorkflow",
        "Lcom/squareup/ui/buyer/BuyerScopeWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "viewFactory",
        "Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;",
        "(Lcom/squareup/ui/buyer/BuyerScopeWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;)V",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "isWorkflowRunning",
        "",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "start",
        "initEvent",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerScopeWorkflow:Lcom/squareup/ui/buyer/BuyerScopeWorkflow;

.field private final container:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerScopeWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    const-class v0, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v0, "BuyerFlowWorkflowRunner::class.java.name"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-interface {p2}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 28
    move-object v4, p3

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 25
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->buyerScopeWorkflow:Lcom/squareup/ui/buyer/BuyerScopeWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "Lcom/squareup/ui/buyer/BuyerScopeInput;",
            "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->buyerScopeWorkflow:Lcom/squareup/ui/buyer/BuyerScopeWorkflow;

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public isWorkflowRunning()Z
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->isRunning()Z

    move-result v0

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public start(Lcom/squareup/ui/buyer/BuyerScopeInput;)V
    .locals 1

    const-string v0, "initEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->stop()V

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;->startOrRestart()V

    return-void
.end method
