.class public final Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;
.super Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;
.source "SignScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HasTip"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u001b\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0099\u0001\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012!\u0010\u0005\u001a\u001d\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u0012\u0004\u0012\u00020\u00040\u0006\u0012!\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u000c\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\r\u0012\u0004\u0012\u00020\u00040\u0006\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u0012\u0012\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0012\u0012\u0010\u0008\u0002\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015\u0012\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0002\u0010\u0018J\u000f\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J$\u0010(\u001a\u001d\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u0012\u0004\u0012\u00020\u00040\u0006H\u00c6\u0003J$\u0010)\u001a\u001d\u0012\u0013\u0012\u00110\u000c\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\r\u0012\u0004\u0012\u00020\u00040\u0006H\u00c6\u0003J\u000f\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u00c6\u0003J\t\u0010+\u001a\u00020\u0012H\u00c6\u0003J\t\u0010,\u001a\u00020\u0012H\u00c6\u0003J\u0011\u0010-\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015H\u00c6\u0003J\u000b\u0010.\u001a\u0004\u0018\u00010\u0016H\u00c6\u0003J\u00a5\u0001\u0010/\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032#\u0008\u0002\u0010\u0005\u001a\u001d\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u0012\u0004\u0012\u00020\u00040\u00062#\u0008\u0002\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u000c\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\r\u0012\u0004\u0012\u00020\u00040\u00062\u000e\u0008\u0002\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00122\u0010\u0008\u0002\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00152\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0016H\u00c6\u0001J\u0013\u00100\u001a\u00020\u00122\u0008\u00101\u001a\u0004\u0018\u000102H\u00d6\u0003J\t\u00103\u001a\u00020\u0007H\u00d6\u0001J\t\u00104\u001a\u000205H\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0019\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R,\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u000c\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\r\u0012\u0004\u0012\u00020\u00040\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R,\u0010\u0005\u001a\u001d\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u0012\u0004\u0012\u00020\u00040\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\"R\u0017\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R\u0011\u0010\u0013\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u001e\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;",
        "clearTip",
        "Lkotlin/Function0;",
        "",
        "setTip",
        "Lkotlin/Function1;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "index",
        "setCustomTip",
        "",
        "tip",
        "tipInfo",
        "",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;",
        "hasAutoGratuity",
        "",
        "useCustomTip",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "customTipMaxMoney",
        "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V",
        "getClearTip",
        "()Lkotlin/jvm/functions/Function0;",
        "getCustomTipMaxMoney",
        "()Lcom/squareup/protos/common/Money;",
        "getHasAutoGratuity",
        "()Z",
        "getMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "getSetCustomTip",
        "()Lkotlin/jvm/functions/Function1;",
        "getSetTip",
        "getTipInfo",
        "()Ljava/util/List;",
        "getUseCustomTip",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clearTip:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final customTipMaxMoney:Lcom/squareup/protos/common/Money;

.field private final hasAutoGratuity:Z

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final setCustomTip:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/CharSequence;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final setTip:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final tipInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final useCustomTip:Z


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;",
            ">;ZZ",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    const-string v0, "clearTip"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setTip"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setCustomTip"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tipInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->clearTip:Lkotlin/jvm/functions/Function0;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setTip:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setCustomTip:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->tipInfo:Ljava/util/List;

    iput-boolean p5, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->hasAutoGratuity:Z

    iput-boolean p6, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->useCustomTip:Z

    iput-object p7, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p8, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->customTipMaxMoney:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x10

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v8, 0x0

    goto :goto_0

    :cond_0
    move/from16 v8, p5

    :goto_0
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_1

    const/4 v9, 0x0

    goto :goto_1

    :cond_1
    move/from16 v9, p6

    :goto_1
    and-int/lit8 v1, v0, 0x40

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 48
    move-object v1, v2

    check-cast v1, Lcom/squareup/text/Formatter;

    move-object v10, v1

    goto :goto_2

    :cond_2
    move-object/from16 v10, p7

    :goto_2
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_3

    .line 49
    move-object v0, v2

    check-cast v0, Lcom/squareup/protos/common/Money;

    move-object v11, v0

    goto :goto_3

    :cond_3
    move-object/from16 v11, p8

    :goto_3
    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v7, p4

    invoke-direct/range {v3 .. v11}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->clearTip:Lkotlin/jvm/functions/Function0;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setTip:Lkotlin/jvm/functions/Function1;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setCustomTip:Lkotlin/jvm/functions/Function1;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->tipInfo:Ljava/util/List;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->hasAutoGratuity:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->useCustomTip:Z

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->moneyFormatter:Lcom/squareup/text/Formatter;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->customTipMaxMoney:Lcom/squareup/protos/common/Money;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move p5, v6

    move p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->copy(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->clearTip:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setTip:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/CharSequence;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setCustomTip:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->tipInfo:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->hasAutoGratuity:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->useCustomTip:Z

    return v0
.end method

.method public final component7()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public final component8()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->customTipMaxMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final copy(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;",
            ">;ZZ",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;"
        }
    .end annotation

    const-string v0, "clearTip"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setTip"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setCustomTip"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tipInfo"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    move-object v1, v0

    move v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->clearTip:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->clearTip:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setTip:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setTip:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setCustomTip:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setCustomTip:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->tipInfo:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->tipInfo:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->hasAutoGratuity:Z

    iget-boolean v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->hasAutoGratuity:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->useCustomTip:Z

    iget-boolean v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->useCustomTip:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->customTipMaxMoney:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->customTipMaxMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getClearTip()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->clearTip:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->customTipMaxMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getHasAutoGratuity()Z
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->hasAutoGratuity:Z

    return v0
.end method

.method public final getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public final getSetCustomTip()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/CharSequence;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setCustomTip:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getSetTip()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setTip:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getTipInfo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->tipInfo:Ljava/util/List;

    return-object v0
.end method

.method public final getUseCustomTip()Z
    .locals 1

    .line 47
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->useCustomTip:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->clearTip:Lkotlin/jvm/functions/Function0;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setTip:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setCustomTip:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->tipInfo:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->hasAutoGratuity:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->useCustomTip:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->moneyFormatter:Lcom/squareup/text/Formatter;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->customTipMaxMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HasTip(clearTip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->clearTip:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", setTip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setTip:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", setCustomTip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->setCustomTip:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tipInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->tipInfo:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hasAutoGratuity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->hasAutoGratuity:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", useCustomTip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->useCustomTip:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", moneyFormatter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", customTipMaxMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->customTipMaxMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
