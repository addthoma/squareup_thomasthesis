.class public interface abstract Lcom/squareup/ui/buyer/tip/BillTipWorkflow;
.super Ljava/lang/Object;
.source "BillTipWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/ui/buyer/tip/BillTipResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u000b2&\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00050\u0004j\u0006\u0012\u0002\u0008\u0003`\u00060\u0001:\u0001\u000bJX\u0010\u0007\u001aR\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00050\u00040\t\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0008j&\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00050\u0004j\u0006\u0012\u0002\u0008\u0003`\u0006`\nH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "Lcom/squareup/ui/buyer/tip/BillTipResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "Companion",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;->$$INSTANCE:Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;

    sput-object v0, Lcom/squareup/ui/buyer/tip/BillTipWorkflow;->Companion:Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;

    return-void
.end method


# virtual methods
.method public abstract asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;"
        }
    .end annotation
.end method
