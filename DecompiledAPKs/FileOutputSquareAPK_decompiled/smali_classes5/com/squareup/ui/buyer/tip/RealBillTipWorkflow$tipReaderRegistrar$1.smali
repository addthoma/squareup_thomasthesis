.class public final Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "RealBillTipWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/tip/TipReaderHandler;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lkotlinx/coroutines/CoroutineDispatcher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "onStarted",
        "",
        "onStopped",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;->this$0:Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;->this$0:Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->access$getTipReaderHandler$p(Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;)Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->register()V

    return-void
.end method

.method public onStopped()V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;->this$0:Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->access$getTipReaderHandler$p(Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;)Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->unregister()V

    return-void
.end method
