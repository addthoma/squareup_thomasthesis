.class public interface abstract Lcom/squareup/ui/buyer/BuyerFlowStarter;
.super Ljava/lang/Object;
.source "BuyerFlowStarter.java"

# interfaces
.implements Lmortar/Scoped;


# virtual methods
.method public abstract goToEmvAuthorizingScreen(Lcom/squareup/cardreader/CardReaderId;)V
.end method

.method public abstract goToPreparingPaymentScreen(Lcom/squareup/cardreader/CardReaderId;)V
.end method

.method public abstract isInBuyerScope(Lcom/squareup/container/ContainerTreeKey;)Z
.end method

.method public abstract isInTransitionToBuyerFlow()Z
.end method

.method public abstract isShowing()Z
.end method

.method public abstract startBuyerFlow()V
.end method

.method public abstract startBuyerFlow(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V
.end method

.method public abstract startBuyerFlowForHardwarePinRequest(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
.end method

.method public abstract startBuyerFlowRecreatingSellerFlow(Z)V
.end method

.method public abstract startEmoneyBuyerFlow(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V
.end method
