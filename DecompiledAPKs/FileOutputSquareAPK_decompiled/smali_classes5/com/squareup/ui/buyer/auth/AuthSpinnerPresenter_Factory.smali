.class public final Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;
.super Ljava/lang/Object;
.source "AuthSpinnerPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerAmountTextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)V"
        }
    .end annotation

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p2, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p3, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p4, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p5, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p6, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p7, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p8, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p9, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p10, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p11, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p12, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p13, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    .line 85
    iput-object p14, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 86
    iput-object p15, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)",
            "Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;"
        }
    .end annotation

    .line 106
    new-instance v16, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/giftcard/GiftCards;Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/util/Device;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;
    .locals 17

    .line 116
    new-instance v16, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/giftcard/GiftCards;Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/util/Device;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-object v16
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;
    .locals 17

    move-object/from16 v0, p0

    .line 91
    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/giftcard/GiftCards;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/log/OhSnapLogger;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-static/range {v2 .. v16}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/giftcard/GiftCards;Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/util/Device;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter_Factory;->get()Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;

    move-result-object v0

    return-object v0
.end method
