.class public Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;
.super Lmortar/ViewPresenter;
.source "AuthSpinnerPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/auth/AuthSpinnerView;",
        ">;"
    }
.end annotation


# instance fields
.field private addTenderDisposable:Lio/reactivex/disposables/Disposable;

.field private addTenderStateObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private authMessage:Ljava/lang/String;

.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private buyerRes:Lcom/squareup/util/Res;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private isInitialized:Z

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private proceedAfterAuthDisposable:Lio/reactivex/disposables/Disposable;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

.field private showingApprovedIndicatorSince:J

.field private showingLoadingIndicatorSince:J

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/giftcard/GiftCards;Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/util/Device;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 3
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 106
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const-wide/16 v1, -0x1

    .line 93
    iput-wide v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->showingApprovedIndicatorSince:J

    const/4 v1, 0x0

    .line 96
    iput-boolean v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->isInitialized:Z

    move-object v1, p1

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-object v1, p2

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    move-object v1, p3

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p4

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p5

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    move-object v1, p6

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    move-object v1, p7

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    move-object v1, p8

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->flow:Lflow/Flow;

    move-object v1, p9

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p10

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    move-object v1, p11

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-object v1, p12

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    move-object/from16 v1, p13

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    move-object/from16 v1, p14

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->device:Lcom/squareup/util/Device;

    .line 121
    invoke-interface/range {p15 .. p15}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerRes:Lcom/squareup/util/Res;

    return-void
.end method

.method private authorizePayment()V
    .locals 3

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->prepareToAuthorize()Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->addTenderStateObservable:Lio/reactivex/Observable;

    .line 157
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buildCardAuthMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->authMessage:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2}, Lio/reactivex/Scheduler;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->showingLoadingIndicatorSince:J

    .line 159
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->authorize()V

    .line 160
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->showAuthorizing()V

    return-void
.end method

.method private buildCardAuthMessage()Ljava/lang/String;
    .locals 3

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasTenderInFlight()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 215
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 216
    instance-of v1, v0, Lcom/squareup/payment/tender/MagStripeTender;

    if-eqz v1, :cond_2

    .line 217
    check-cast v0, Lcom/squareup/payment/tender/MagStripeTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/MagStripeTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    .line 218
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v1, v0}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 220
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    .line 221
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v1}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/text/CardBrandResources;->buyerBrandNameId:I

    .line 222
    invoke-virtual {v0}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v0

    .line 221
    invoke-static {v2, v1, v0}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    return-object v2
.end method

.method private emvReaderWasPresent(Lcom/squareup/protos/client/bills/AddTendersResponse;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 358
    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 362
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-nez p1, :cond_1

    return v0

    .line 367
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-nez p1, :cond_2

    return v0

    .line 372
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz p1, :cond_4

    .line 373
    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    if-nez v1, :cond_3

    goto :goto_0

    .line 377
    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    :cond_4
    :goto_0
    return v0
.end method

.method private hasInstrumentTender(Lcom/squareup/protos/client/bills/AddTendersResponse;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 397
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    if-eqz p1, :cond_2

    .line 398
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 402
    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender;

    .line 403
    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    if-eqz v1, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne p1, v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    :goto_0
    return v0
.end method

.method public static synthetic lambda$ZmmDbqEQEpZg31usbY5iG2VhDvA(Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->onSuccess(Lcom/squareup/protos/client/bills/AddTendersResponse;)V

    return-void
.end method

.method private logCardOnFileEvent(Lcom/squareup/analytics/RegisterActionName;)V
    .locals 9

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 382
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 383
    instance-of v1, v0, Lcom/squareup/payment/tender/InstrumentTender;

    if-eqz v1, :cond_0

    .line 384
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    .line 385
    iget-object v0, v0, Lcom/squareup/protos/client/bills/PaymentInstrument;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    .line 386
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v8, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;

    iget-object v2, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getCustomerId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/protos/client/bills/StoredInstrument;->instrument_token:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 387
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v6

    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 388
    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v7

    move-object v2, v8

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 386
    invoke-interface {v1, v8}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    return-void
.end method

.method private onClientError()V
    .locals 2

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->screen:Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    iget-object v0, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->clientError(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/util/Res;)Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->removeAuthSpinnerScreenAndGoTo(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method private onFailure(Lcom/squareup/protos/client/bills/AddTendersResponse;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->SERVER_CALL:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v4, 0x2

    aput-object p3, v2, v4

    const-string v4, "AuthSpinnerPresenter: onFailureAfterMinimumDelay. response: %s; error: \'%s\', \'%s\'"

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 316
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->hasInstrumentTender(Lcom/squareup/protos/client/bills/AddTendersResponse;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->logCardOnFileEvent(Lcom/squareup/analytics/RegisterActionName;)V

    .line 320
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->emvReaderWasPresent(Lcom/squareup/protos/client/bills/AddTendersResponse;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 323
    iget-object p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {p1, v3, v0}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 324
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 326
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 327
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 328
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 329
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 330
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 331
    iget-object p2, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance p3, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {p3, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    goto :goto_0

    .line 334
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->screen:Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    iget-object p1, p1, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->res:Lcom/squareup/util/Res;

    .line 335
    invoke-static {p1, v0, p2, p3}, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->authFailed(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    move-result-object p1

    .line 334
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->removeAuthSpinnerScreenAndGoTo(Lcom/squareup/ui/main/RegisterTreeKey;)V

    :goto_0
    return-void
.end method

.method private onRemoteError()V
    .locals 2

    .line 345
    new-instance v0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen;

    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->screen:Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    iget-object v1, v1, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->removeAuthSpinnerScreenAndGoTo(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method private onSuccess(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 5

    .line 288
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;

    .line 289
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerRes:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->buyer_approved:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->authMessage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;->setText(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->hasInstrumentTender(Lcom/squareup/protos/client/bills/AddTendersResponse;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 292
    sget-object p1, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->logCardOnFileEvent(Lcom/squareup/analytics/RegisterActionName;)V

    .line 295
    :cond_0
    iget-wide v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->showingApprovedIndicatorSince:J

    const-wide/16 v3, -0x1

    cmp-long p1, v1, v3

    if-nez p1, :cond_1

    .line 297
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;->transitionToCheck()V

    goto :goto_0

    .line 301
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;->setToCheck()V

    .line 306
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->waitSomeMoreAfterSuccess()V

    return-void
.end method

.method private removeAuthSpinnerScreenAndGoTo(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 4

    .line 353
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, p1, v1}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method private showAuthorizing()V
    .locals 3

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->isLocalTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerRes:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->buyer_processing:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->authMessage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;->setText(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 235
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerRes:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->buyer_authorizing:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->authMessage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;->setText(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private updateViewAndObserveAddTenderState()V
    .locals 8

    .line 164
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;

    .line 165
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getBuyerFormattedTotalAmount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;->setTotal(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getBuyerFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->addTenderStateObservable:Lio/reactivex/Observable;

    .line 169
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2}, Lio/reactivex/Scheduler;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v1

    iget-wide v3, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->showingLoadingIndicatorSince:J

    sub-long/2addr v1, v3

    sget-object v3, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x320

    .line 170
    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    cmp-long v3, v1, v6

    if-gez v3, :cond_0

    .line 172
    sget-object v1, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 173
    invoke-static {v4, v5, v1, v2}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->addTenderDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v1, :cond_1

    .line 177
    invoke-interface {v1}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 185
    :cond_1
    new-instance v1, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$snvjCTOjLXo0XgGesrxzMmo4a_g;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$snvjCTOjLXo0XgGesrxzMmo4a_g;-><init>(Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;)V

    .line 186
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->addTenderDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private waitSomeMoreAfterSuccess()V
    .locals 6

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->proceedAfterAuthDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 242
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 245
    :cond_0
    new-instance v0, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$lkL_4kKyLnOcwbtDtlAV_NskCec;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$lkL_4kKyLnOcwbtDtlAV_NskCec;-><init>(Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;)V

    .line 271
    iget-wide v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->showingApprovedIndicatorSince:J

    const-wide/16 v3, -0x1

    cmp-long v5, v1, v3

    if-nez v5, :cond_1

    .line 272
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2}, Lio/reactivex/Scheduler;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->showingApprovedIndicatorSince:J

    .line 275
    :cond_1
    iget-wide v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->showingApprovedIndicatorSince:J

    sget-object v3, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x320

    .line 276
    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    add-long/2addr v1, v3

    iget-object v3, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 277
    invoke-virtual {v3, v4}, Lio/reactivex/Scheduler;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_2

    .line 279
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 280
    invoke-static {v1, v2, v3, v4}, Lio/reactivex/Completable;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/buyer/auth/-$$Lambda$Zb5ZoLJVNDv7BEiByJ3QDd0v7nI;

    invoke-direct {v2, v0}, Lcom/squareup/ui/buyer/auth/-$$Lambda$Zb5ZoLJVNDv7BEiByJ3QDd0v7nI;-><init>(Ljava/lang/Runnable;)V

    .line 281
    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->proceedAfterAuthDisposable:Lio/reactivex/disposables/Disposable;

    goto :goto_0

    .line 283
    :cond_2
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/buyer/auth/AuthSpinnerView;)V
    .locals 2

    .line 139
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->addTenderDisposable:Lio/reactivex/disposables/Disposable;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 141
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 142
    iput-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->addTenderDisposable:Lio/reactivex/disposables/Disposable;

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->proceedAfterAuthDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_1

    .line 145
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 146
    iput-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->proceedAfterAuthDisposable:Lio/reactivex/disposables/Disposable;

    .line 149
    :cond_1
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->dropView(Lcom/squareup/ui/buyer/auth/AuthSpinnerView;)V

    return-void
.end method

.method public synthetic lambda$null$0$AuthSpinnerPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 188
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 190
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-nez v0, :cond_2

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 192
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_1

    .line 193
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->onClientError()V

    goto :goto_1

    .line 194
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_3

    .line 195
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    .line 196
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersResponse;

    .line 197
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->onFailure(Lcom/squareup/protos/client/bills/AddTendersResponse;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 191
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->onRemoteError()V

    :cond_3
    :goto_1
    return-void
.end method

.method public synthetic lambda$null$2$AuthSpinnerPresenter(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->matchesOtherTenderIdPair(Lcom/squareup/protos/client/IdPair;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 262
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->paymentComplete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToLoyaltyScreen(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$updateViewAndObserveAddTenderState$1$AuthSpinnerPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 186
    new-instance v0, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$ZmmDbqEQEpZg31usbY5iG2VhDvA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$ZmmDbqEQEpZg31usbY5iG2VhDvA;-><init>(Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;)V

    new-instance v1, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$WwgHk1MJ4uwZCkNhEVRi-O8uLuM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$WwgHk1MJ4uwZCkNhEVRi-O8uLuM;-><init>(Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$waitSomeMoreAfterSuccess$3$AuthSpinnerPresenter()V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstScreenAfterAuthSpinner()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->authorizePayment()V

    .line 253
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->updateViewAndObserveAddTenderState()V

    goto :goto_0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasReceiptForLastPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->loyaltyEvent()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$z5oWURImB6OQg-mAOHqpoC36BQE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/auth/-$$Lambda$AuthSpinnerPresenter$z5oWURImB6OQg-mAOHqpoC36BQE;-><init>(Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;)V

    .line 258
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    :cond_1
    :goto_0
    return-void
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 125
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 126
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    iput-object p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->screen:Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown AuthSpinnerScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 131
    iget-boolean p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->isInitialized:Z

    if-nez p1, :cond_0

    .line 132
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->authorizePayment()V

    const/4 p1, 0x1

    .line 133
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->isInitialized:Z

    .line 135
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;->updateViewAndObserveAddTenderState()V

    return-void
.end method
