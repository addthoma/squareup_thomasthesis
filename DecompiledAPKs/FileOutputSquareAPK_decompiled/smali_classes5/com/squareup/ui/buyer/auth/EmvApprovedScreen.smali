.class public final Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "EmvApprovedScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;,
        Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 172
    sget-object v0, Lcom/squareup/ui/buyer/auth/-$$Lambda$EmvApprovedScreen$FhRU2THuQjWkbM257Kiu6iqQuR4;->INSTANCE:Lcom/squareup/ui/buyer/auth/-$$Lambda$EmvApprovedScreen$FhRU2THuQjWkbM257Kiu6iqQuR4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;
    .locals 1

    .line 173
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 174
    new-instance v0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 168
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 52
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 48
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->emv_approved_view:I

    return v0
.end method
