.class public final enum Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;
.super Ljava/lang/Enum;
.source "ChooseEmvOptionScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

.field public static final enum STRING:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

.field public static final enum STRING_RESOURCE_ID:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 84
    new-instance v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    const/4 v1, 0x0

    const-string v2, "STRING"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->STRING:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    .line 85
    new-instance v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    const/4 v2, 0x1

    const-string v3, "STRING_RESOURCE_ID"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->STRING_RESOURCE_ID:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    .line 83
    sget-object v3, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->STRING:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->STRING_RESOURCE_ID:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->$VALUES:[Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;
    .locals 1

    .line 83
    const-class v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->$VALUES:[Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    invoke-virtual {v0}, [Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    return-object v0
.end method
