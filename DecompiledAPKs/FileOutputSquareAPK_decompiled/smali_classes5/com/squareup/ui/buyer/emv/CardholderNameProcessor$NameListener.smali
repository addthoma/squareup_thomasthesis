.class Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;
.super Ljava/lang/Object;
.source "CardholderNameProcessor.java"

# interfaces
.implements Lcom/squareup/cardreader/EmvListener;
.implements Lcom/squareup/cardreader/PaymentCompletionListener;
.implements Lcom/squareup/cardreader/PinRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NameListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;)V
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$1;)V
    .locals 0

    .line 256
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;-><init>(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;)V

    return-void
.end method


# virtual methods
.method public onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public onCardError()V
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleOnCardError()V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleOnCardRemovedDuringPayment()V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleOnCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 0

    .line 344
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleOnListApplications([Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public onMagFallbackSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "onMagFallbackSwipeFailed in CardholdernameProcessor"

    .line 305
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onMagFallbackSwipeSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 301
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "onMagSwipeFallbackSuccess should not be called here."

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    if-eqz p4, :cond_0

    .line 315
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleSendAuthorization([B)V

    return-void

    .line 312
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "How can you get payment approval before auth?"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 325
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleOnPaymentDeclined()V

    return-void
.end method

.method public onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 320
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "How can you get payment approval before auth?"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 333
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleOnPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method

.method public onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Payment terminated due to swipe"

    .line 338
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSigRequested()V
    .locals 2

    .line 287
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "onSigRequested should not be called here."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V
    .locals 0

    .line 350
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V

    return-void
.end method

.method public onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 1

    .line 270
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "onSwipeForFallback should not be called here."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onUseChipCardDuringFallback()V
    .locals 2

    .line 266
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "onUseChipCardDuringFallback should not be called here."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 0

    .line 292
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-static {p3}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->access$100(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object p3

    .line 293
    invoke-interface {p3}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p3

    .line 294
    invoke-virtual {p3, p2}, Lcom/squareup/cardreader/CardReaderInfo;->setCardPresenceRequired(Z)V

    .line 295
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;->this$0:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->handleSendAuthorization([B)V

    return-void
.end method
