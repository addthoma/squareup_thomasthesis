.class public Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;
.super Landroid/widget/LinearLayout;
.source "EmvProgressView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/progress/EmvProgressView$Component;
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

.field private spinnerMessage:Lcom/squareup/widgets/MessageView;

.field private spinnerTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const-class p2, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView$Component;->inject(Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;)V

    .line 44
    sget p2, Lcom/squareup/checkout/R$layout;->auth_spinner_glyph:I

    const/4 v0, 0x0

    .line 45
    invoke-static {p1, p2, v0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 103
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 104
    sget v0, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->spinnerTitle:Landroid/widget/TextView;

    .line 105
    sget v0, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    .line 106
    sget v0, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 61
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public onBackPressed()Z
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->presenter:Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->presenter:Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->dropView(Ljava/lang/Object;)V

    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 50
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->bindViews()V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->presenter:Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setSpinnerMessageText(I)V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 69
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    .line 70
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method public showCardRemovalPrompt(ZZ)V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->spinnerTitle:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    sget p2, Lcom/squareup/ui/crm/R$string;->emv_do_not_remove_card:I

    goto :goto_0

    :cond_0
    sget p2, Lcom/squareup/cardreader/R$string;->please_remove_card_title:I

    :goto_0
    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_1

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->spinnerTitle:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 89
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->spinnerTitle:Landroid/widget/TextView;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void
.end method
