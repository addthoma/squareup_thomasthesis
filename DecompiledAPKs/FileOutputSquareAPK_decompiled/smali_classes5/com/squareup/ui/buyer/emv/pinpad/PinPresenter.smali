.class public Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
.super Lmortar/ViewPresenter;
.source "PinPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Component;,
        Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Factory;,
        Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;,
        Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinScreen;,
        Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;,
        Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/emv/pinpad/PinView;",
        ">;"
    }
.end annotation


# static fields
.field public static final MAX_PIN_LENGTH:I = 0xc

.field public static final MIN_PIN_LENGTH:I = 0x4


# instance fields
.field private canSkip:Z

.field private cardInfo:Lcom/squareup/cardreader/CardInfo;

.field private listener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

.field private res:Lcom/squareup/util/Res;

.field private final resStream:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private starCount:I

.field private final starPresenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

.field private state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;Lio/reactivex/Observable;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;",
            ")V"
        }
    .end annotation

    .line 108
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starPresenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    .line 110
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->resStream:Lio/reactivex/Observable;

    .line 111
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->listener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    return-void
.end method

.method private setCardInformation()V
    .locals 5

    .line 178
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    if-nez v0, :cond_1

    .line 182
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->hideCardInfo()V

    return-void

    .line 186
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardInfo;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->pinCard(Lcom/squareup/Card$Brand;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    .line 187
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->showCardInfo()V

    .line 188
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/pinpad/R$string;->emv_pin_card_info:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    .line 189
    invoke-virtual {v3}, Lcom/squareup/cardreader/CardInfo;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/Card$Brand;->getHumanName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "card_brand"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    .line 190
    invoke-virtual {v3}, Lcom/squareup/cardreader/CardInfo;->getLast4()Ljava/lang/String;

    move-result-object v3

    const-string v4, "last_four"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 191
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 192
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 188
    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setCardInfo(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    return-void
.end method

.method private updateKeypadStatus()V
    .locals 6

    .line 206
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    if-nez v0, :cond_0

    return-void

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->updatePadlock(Lcom/squareup/util/Res;)V

    .line 215
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starPresenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    iget v2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starCount:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->setStarCount(I)V

    .line 216
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v2, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v2, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT_FINAL:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    .line 220
    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starPresenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    invoke-virtual {v2}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->showStarGroup()V

    .line 222
    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starPresenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    invoke-virtual {v2, v4}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->setStarGroupCorrect(Z)V

    .line 223
    sget v2, Lcom/squareup/pinpad/R$string;->enter_your_pin:I

    iget-object v5, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2, v5}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setTitle(ILcom/squareup/util/Res;)V

    goto :goto_2

    .line 225
    :cond_2
    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starPresenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->setStarGroupCorrect(Z)V

    .line 226
    sget v2, Lcom/squareup/pinpad/R$string;->incorrect_pin:I

    iget-object v5, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2, v5}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setTitle(ILcom/squareup/util/Res;)V

    .line 228
    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v5, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-ne v2, v5, :cond_3

    .line 229
    sget v2, Lcom/squareup/pinpad/R$string;->please_try_again:I

    iget-object v5, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2, v5}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setMessage(ILcom/squareup/util/Res;)V

    goto :goto_2

    .line 231
    :cond_3
    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v5, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT_FINAL:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-ne v2, v5, :cond_4

    const/4 v2, 0x1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_1
    const-string v5, "PinPresenter::updateKeypadStatus state is not incorrect final"

    invoke-static {v2, v5}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 233
    sget v2, Lcom/squareup/pinpad/R$string;->emv_pin_final_retry:I

    iget-object v5, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2, v5}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setMessage(ILcom/squareup/util/Res;)V

    .line 238
    :goto_2
    iget v2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starCount:I

    if-eqz v2, :cond_8

    if-nez v1, :cond_5

    goto :goto_4

    .line 245
    :cond_5
    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setPinPadLeftButtonState(Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;)V

    .line 246
    iget v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starCount:I

    const/16 v2, 0xc

    if-ge v1, v2, :cond_6

    const/4 v3, 0x1

    :cond_6
    invoke-virtual {v0, v3}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setDigitsEnabled(Z)V

    .line 247
    iget v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starCount:I

    const/4 v2, 0x4

    if-lt v1, v2, :cond_7

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_VALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    goto :goto_3

    :cond_7
    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_INVALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    :goto_3
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setPinPadRightButtonState(Lcom/squareup/padlock/Padlock$PinPadRightButtonState;)V

    goto :goto_6

    .line 240
    :cond_8
    :goto_4
    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setPinPadLeftButtonState(Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;)V

    .line 241
    invoke-virtual {v0, v4}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setDigitsEnabled(Z)V

    .line 242
    iget-boolean v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->canSkip:Z

    if-eqz v1, :cond_9

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    goto :goto_5

    :cond_9
    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_INVALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    :goto_5
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setPinPadRightButtonState(Lcom/squareup/padlock/Padlock$PinPadRightButtonState;)V

    .line 251
    :goto_6
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->setCardInformation()V

    return-void
.end method


# virtual methods
.method clearPinSelected()V
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->listener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;->onClear()V

    .line 166
    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->AWAITING_INPUT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    const/4 v0, 0x0

    .line 167
    iput v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starCount:I

    .line 168
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->updateKeypadStatus()V

    return-void
.end method

.method public synthetic lambda$null$0$PinPresenter(Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 139
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->res:Lcom/squareup/util/Res;

    .line 140
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->updateKeypadStatus()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$PinPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->resStream:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$PinPresenter$D3-Y-AhseD3PYChUcFaE7ACjEUw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$PinPresenter$D3-Y-AhseD3PYChUcFaE7ACjEUw;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 119
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinScreen;

    .line 120
    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinScreen;->params()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    .line 121
    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinScreen;->params()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->canSkip:Z

    iput-boolean v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->canSkip:Z

    .line 123
    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinScreen;->params()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->isFinalRetry:Z

    if-eqz v0, :cond_0

    .line 126
    sget-object p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT_FINAL:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    goto :goto_0

    .line 127
    :cond_0
    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinScreen;->params()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    move-result-object p1

    iget-boolean p1, p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->isRetry:Z

    if-eqz p1, :cond_1

    .line 128
    sget-object p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    goto :goto_0

    .line 130
    :cond_1
    sget-object p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->AWAITING_INPUT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 135
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 137
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$PinPresenter$hoxB3QL9D9b43Tyle49En3AbGkY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$PinPresenter$hoxB3QL9D9b43Tyle49En3AbGkY;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT_FINAL:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-ne p1, v0, :cond_1

    .line 144
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starPresenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->showErrorMessage(Z)V

    :cond_1
    return-void
.end method

.method onSubmit()V
    .locals 2

    .line 172
    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->SUBMITTED:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->setAllButtonsEnabled(Z)V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->listener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;->onSubmit()V

    return-void
.end method

.method pinCancelClicked()V
    .locals 1

    .line 196
    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->SUBMITTED:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->listener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;->onCancel()V

    return-void
.end method

.method pinDigitEntered(I)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->listener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;->onPinEntered(I)V

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT_FINAL:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->SUBMITTED:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-ne p1, v0, :cond_1

    .line 157
    :cond_0
    sget-object p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->AWAITING_INPUT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    const/4 p1, 0x0

    .line 158
    iput p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starCount:I

    .line 160
    :cond_1
    iget p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->starCount:I

    .line 161
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->updateKeypadStatus()V

    return-void
.end method

.method pinSkipClicked()V
    .locals 1

    .line 201
    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->SUBMITTED:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->listener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;->onSkip()V

    return-void
.end method

.method windowFocusLost()V
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->state:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    sget-object v1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->SUBMITTED:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    if-eq v0, v1, :cond_0

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->listener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;->onFocusLost()V

    :cond_0
    return-void
.end method
