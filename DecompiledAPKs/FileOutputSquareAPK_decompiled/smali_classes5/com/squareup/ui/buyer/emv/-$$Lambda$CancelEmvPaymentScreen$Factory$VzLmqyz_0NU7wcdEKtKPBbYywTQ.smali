.class public final synthetic Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;

.field private final synthetic f$1:Landroid/content/Context;

.field private final synthetic f$2:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final synthetic f$3:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;Landroid/content/Context;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;->f$0:Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;

    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;->f$1:Landroid/content/Context;

    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;->f$2:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;->f$3:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;->f$0:Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;->f$1:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;->f$2:Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;->f$3:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    move-object v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;->lambda$create$1$CancelEmvPaymentScreen$Factory(Landroid/content/Context;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Landroid/content/DialogInterface;I)V

    return-void
.end method
