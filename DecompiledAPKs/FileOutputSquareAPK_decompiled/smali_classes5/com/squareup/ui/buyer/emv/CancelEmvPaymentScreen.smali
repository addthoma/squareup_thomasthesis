.class public final Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;
.super Lcom/squareup/ui/buyer/emv/InEmvScope;
.source "CancelEmvPaymentScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$HdG9up3yKffhSBNi07QSlI9n0NY;->INSTANCE:Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$HdG9up3yKffhSBNi07QSlI9n0NY;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;
    .locals 1

    .line 73
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 74
    new-instance v0, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 68
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/InEmvScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
