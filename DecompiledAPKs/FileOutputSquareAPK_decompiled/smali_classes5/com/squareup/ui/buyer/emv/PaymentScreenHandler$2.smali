.class Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "PaymentScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;->cancelPayment()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;)V
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$2;->this$0:Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$2;->this$0:Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;

    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cancelPayment()V

    return-void
.end method
