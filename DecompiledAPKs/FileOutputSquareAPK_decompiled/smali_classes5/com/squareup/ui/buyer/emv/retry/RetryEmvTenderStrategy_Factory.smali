.class public final Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;
.super Ljava/lang/Object;
.source "RetryEmvTenderStrategy_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final emvScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;->emvScopeRunnerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/buyer/BuyerScopeRunner;)Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;->emvScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;->newInstance(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/buyer/BuyerScopeRunner;)Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy_Factory;->get()Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;

    move-result-object v0

    return-object v0
.end method
