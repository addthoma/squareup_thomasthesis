.class public interface abstract Lcom/squareup/ui/buyer/actionbar/BuyerActionBarHost;
.super Ljava/lang/Object;
.source "BuyerActionBarHost.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/mortar/HasContext;


# virtual methods
.method public abstract hideUpButton()V
.end method

.method public abstract setCallToAction(Ljava/lang/CharSequence;)V
.end method

.method public abstract setOnUpClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V
.end method

.method public abstract setSubtitle(Ljava/lang/CharSequence;)V
.end method

.method public abstract setTicketName(Ljava/lang/String;)V
.end method

.method public abstract setTotal(Ljava/lang/CharSequence;)V
.end method

.method public abstract showUpAsBack()V
.end method
