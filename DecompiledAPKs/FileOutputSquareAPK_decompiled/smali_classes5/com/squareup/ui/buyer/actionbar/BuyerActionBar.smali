.class public Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;
.super Landroid/widget/RelativeLayout;
.source "BuyerActionBar.java"


# static fields
.field private static final MAX_TABLET_BLOCK_WIDTH:F = 0.5f

.field private static final NEW_SALE_IMAGE_BACKGROUND_ID:I


# instance fields
.field private final callToAction:Landroid/widget/TextView;

.field private customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

.field private customerButton:Lcom/squareup/glyph/SquareGlyphView;

.field private final isPortrait:Z

.field private final isTablet:Z

.field private leftFlank:Landroid/widget/LinearLayout;

.field private newSaleButton:Landroid/widget/TextView;

.field private final orderTicketName:Landroid/widget/TextView;

.field private final showNewSale:Z

.field private final subtitle:Landroid/widget/TextView;

.field private final total:Landroid/widget/TextView;

.field private upGlyphButton:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_black_transparent_fifty_border_white_transparent_twenty:I

    sput v0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->NEW_SALE_IMAGE_BACKGROUND_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 61
    sget v1, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->isTablet:Z

    .line 62
    sget v1, Lcom/squareup/utilities/ui/R$bool;->sq_is_portrait:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->isPortrait:Z

    .line 64
    sget-object v0, Lcom/squareup/ui/buyerflow/R$styleable;->BuyerActionBar:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 65
    sget v0, Lcom/squareup/ui/buyerflow/R$styleable;->BuyerActionBar_showNewSale:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showNewSale:Z

    .line 67
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->isTablet:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showNewSale:Z

    if-eqz v0, :cond_1

    .line 68
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->isPortrait:Z

    if-eqz v0, :cond_0

    .line 69
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->buyer_flow_action_bar_new_sale_portrait:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_0

    .line 71
    :cond_0
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->buyer_flow_action_bar_new_sale:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 73
    :goto_0
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_new_sale_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    .line 74
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_customer_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 75
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_customer_add_card_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 76
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_left_flank:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->leftFlank:Landroid/widget/LinearLayout;

    goto :goto_1

    .line 78
    :cond_1
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->buyer_flow_action_bar:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 79
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_up_glyph:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->upGlyphButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 82
    :goto_1
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_total:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->total:Landroid/widget/TextView;

    .line 83
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_name:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    .line 84
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_subtitle:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->subtitle:Landroid/widget/TextView;

    .line 85
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->buyer_actionbar_call_to_action:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->callToAction:Landroid/widget/TextView;

    .line 87
    sget p1, Lcom/squareup/ui/buyerflow/R$styleable;->BuyerActionBar_callToAction:I

    invoke-virtual {p2, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 88
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setCallToAction(Ljava/lang/CharSequence;)V

    .line 89
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private setMeasurements(Landroid/view/View;III)V
    .locals 1

    .line 275
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 281
    invoke-virtual {p1, p3, p4}, Landroid/view/View;->measure(II)V

    goto :goto_0

    :cond_0
    const/high16 p2, 0x40000000    # 2.0f

    const/4 p3, 0x0

    .line 284
    invoke-static {p2, p3, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getChildMeasureSpec(III)I

    move-result p2

    invoke-virtual {p1, p2, p4}, Landroid/view/View;->measure(II)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getNewSaleButton()Landroid/widget/TextView;
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    return-object v0
.end method

.method public getNewSaleButtonRect(Landroid/graphics/Rect;)V
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->getHitRect(Landroid/graphics/Rect;)V

    return-void
.end method

.method public getSaveCardButtonRect(Landroid/graphics/Rect;)V
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->getHitRect(Landroid/graphics/Rect;)V

    return-void
.end method

.method public hideUpButton()V
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->upGlyphButton:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .line 115
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 117
    iget-boolean p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->isTablet:Z

    if-nez p2, :cond_0

    return-void

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 123
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    int-to-float v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v0, v0, v1

    float-to-int v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 125
    invoke-static {v1, v2, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getChildMeasureSpec(III)I

    move-result v3

    .line 126
    iget-boolean v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showNewSale:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->leftFlank:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v4

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    .line 132
    :goto_0
    iget-object v5, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->total:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    .line 133
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 132
    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 134
    invoke-static {v1, v2, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getChildMeasureSpec(III)I

    move-result p2

    .line 136
    iget-object v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->total:Landroid/widget/TextView;

    invoke-direct {p0, v4, v3, v0, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setMeasurements(Landroid/view/View;III)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->total:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    .line 140
    invoke-static {v1, v2, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getChildMeasureSpec(III)I

    move-result v3

    .line 142
    iget-object v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_2

    .line 143
    iget-object v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    invoke-direct {p0, v4, v0, v3, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setMeasurements(Landroid/view/View;III)V

    .line 147
    :cond_2
    iget-boolean v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showNewSale:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    .line 148
    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    if-eq v4, v5, :cond_4

    iget-object v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 149
    invoke-virtual {v4}, Lcom/squareup/glyph/SquareGlyphView;->getVisibility()I

    move-result v4

    if-eq v4, v5, :cond_4

    .line 154
    div-int/lit8 v0, p1, 0x2

    .line 155
    invoke-static {v1, v2, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getChildMeasureSpec(III)I

    move-result v3

    .line 158
    iget-object v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->leftFlank:Landroid/widget/LinearLayout;

    invoke-direct {p0, v4, v0, v3, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setMeasurements(Landroid/view/View;III)V

    .line 160
    iget-object v4, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    invoke-direct {p0, v4, v0, v3, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setMeasurements(Landroid/view/View;III)V

    .line 163
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->isPortrait:Z

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->leftFlank:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr p1, v0

    .line 164
    :goto_1
    invoke-static {v1, v2, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getChildMeasureSpec(III)I

    move-result v0

    .line 167
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->total:Landroid/widget/TextView;

    invoke-direct {p0, v1, p1, v0, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setMeasurements(Landroid/view/View;III)V

    goto :goto_2

    .line 168
    :cond_4
    iget-boolean p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showNewSale:Z

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getVisibility()I

    move-result p1

    if-eq p1, v5, :cond_5

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->leftFlank:Landroid/widget/LinearLayout;

    invoke-direct {p0, p1, v0, v3, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setMeasurements(Landroid/view/View;III)V

    :cond_5
    :goto_2
    return-void
.end method

.method public setAppearanceForImageBackground()V
    .locals 3

    .line 229
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 230
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 231
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->total:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 232
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->subtitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 233
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 234
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->callToAction:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 236
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    sget v2, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->NEW_SALE_IMAGE_BACKGROUND_ID:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    .line 238
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz v1, :cond_0

    .line 239
    invoke-virtual {v1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 241
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 242
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    sget v2, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->NEW_SALE_IMAGE_BACKGROUND_ID:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz v1, :cond_1

    .line 246
    invoke-virtual {v1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    sget v1, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->NEW_SALE_IMAGE_BACKGROUND_ID:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    :cond_1
    return-void
.end method

.method public setCallToAction(Ljava/lang/CharSequence;)V
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->callToAction:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->callToAction:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x8

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setNewSaleButtonText(Ljava/lang/String;)V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnCustomerAddCardClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnCustomerClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnNewSaleClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->newSaleButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnUpGlyphClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->upGlyphButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->subtitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->subtitle:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x8

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setTicketName(Ljava/lang/CharSequence;)V
    .locals 3

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 193
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->isTablet:Z

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Numbers;->isOrderNumber(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->orderTicketName:Landroid/widget/TextView;

    .line 195
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/ui/buyerflow/R$dimen;->text_checkout_headline:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 194
    invoke-virtual {p1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_1
    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->total:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->total:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x8

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public showBackArrow()V
    .locals 2

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->upGlyphButton:Lcom/squareup/glyph/SquareGlyphView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public showCustomerAddCardButton(Z)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerAddCardButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showCustomerButton(Z)V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->customerButton:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method
