.class Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;
.super Lmortar/ViewPresenter;
.source "LoyaltyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;,
        Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;,
        Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/loyalty/LoyaltyView;",
        ">;"
    }
.end annotation


# static fields
.field private static final ANIMATING_STATE_KEY:Ljava/lang/String; = "ANIMATING_STATE_KEY"


# instance fields
.field private accumulateRequestSent:Z

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final busyEnrolling:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final clock:Lcom/squareup/util/Clock;

.field private final curatedImage:Lcom/squareup/merchantimages/CuratedImage;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private enrollLoyaltySubscription:Lrx/Subscription;

.field private final enrollment:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private isAnimating:Z

.field private final locale:Ljava/util/Locale;

.field private final loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

.field private loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

.field private final loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private noThanksClicked:Z

.field private final offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

.field private final phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private final pointsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private reasonForPointAccrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field private final receipt:Lcom/squareup/payment/PaymentReceipt;

.field private final receiptPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final rewardStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;",
            ">;"
        }
    .end annotation
.end field

.field private screen:Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;

.field private screenCreationTimeMillis:J

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final showClaimYourStar:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

.field private final transactionEmployeeToken:Ljava/lang/String;

.field private final viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/util/Locale;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/picasso/Picasso;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)V
    .locals 5
    .param p16    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p17    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 327
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 293
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 294
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollment:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 295
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->rewardStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 296
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 297
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receiptPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    .line 298
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->busyEnrolling:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 299
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->showClaimYourStar:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v2, ""

    .line 300
    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v3, 0x0

    .line 302
    iput-object v3, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->reasonForPointAccrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 303
    iput-boolean v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->noThanksClicked:Z

    .line 304
    iput-boolean v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->accumulateRequestSent:Z

    .line 305
    iput-boolean v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->isAnimating:Z

    .line 316
    invoke-static {}, Lrx/subscriptions/Subscriptions;->empty()Lrx/Subscription;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollLoyaltySubscription:Lrx/Subscription;

    move-object v1, p1

    .line 328
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-object v1, p2

    .line 329
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p3

    .line 330
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    move-object v1, p4

    .line 331
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    move-object v1, p5

    .line 332
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    move-object v1, p6

    .line 333
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p7

    .line 334
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

    move-object v1, p8

    .line 335
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p9

    .line 336
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->clock:Lcom/squareup/util/Clock;

    move-object v1, p10

    .line 337
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    move-object/from16 v1, p11

    .line 338
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p12

    .line 339
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-object/from16 v1, p13

    .line 340
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 341
    invoke-virtual/range {p14 .. p14}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    move-object/from16 v1, p15

    .line 342
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    .line 343
    new-instance v1, Lcom/squareup/util/RxWatchdog;

    move-object/from16 v3, p16

    move-object/from16 v4, p17

    invoke-direct {v1, v3, v4}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    move-object/from16 v1, p18

    .line 344
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->locale:Ljava/util/Locale;

    move-object/from16 v1, p19

    .line 345
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    move-object/from16 v1, p20

    .line 346
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    move-object/from16 v1, p21

    .line 347
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    move-object/from16 v1, p22

    .line 348
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->picasso:Lcom/squareup/picasso/Picasso;

    .line 350
    invoke-virtual/range {p14 .. p14}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p14 .. p14}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getEmployeeToken()Ljava/lang/String;

    move-result-object v2

    :cond_0
    iput-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->transactionEmployeeToken:Ljava/lang/String;

    move-object/from16 v1, p23

    .line 351
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->pointsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    move-object/from16 v1, p24

    .line 352
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    return-void
.end method

.method static synthetic access$1200(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->showClaimYourStar:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method private beginEnrollment(Z)V
    .locals 3

    .line 706
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 707
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->defaultPhoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receiptPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 708
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->receiptPhoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    .line 710
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->hasValue()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receiptPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 712
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->loyalty_enroll_phone_edit_prefilled_phone_hint:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneEditHint(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    goto :goto_2

    .line 714
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->loyalty_enroll_phone_edit_hint:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneEditHint(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    .line 717
    :goto_2
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setEnrollLoyaltyStatus(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;Z)V

    .line 718
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setClaimText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;Z)V

    .line 719
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setRewardRequirementText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V

    .line 720
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setClaimHelpText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V

    .line 722
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getLoyaltyProgramName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->programName(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    .line 724
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollment:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->build()Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 726
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ENROLLMENT_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 727
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-virtual {p1, v0}, Lcom/squareup/loyalty/LoyaltyAnalytics;->logViewEnroll(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    return-void
.end method

.method private checkAndEnqueueMissedOpportunity()V
    .locals 2

    .line 892
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 893
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->OFFLINE_MODE:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    goto :goto_0

    .line 894
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    sget-object v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->ENROLL_WITHOUT_EARNING_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget-object v0, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    sget-object v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->SHOW_STATUS_WITHOUT_EARNING_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    if-ne v0, v1, :cond_2

    .line 896
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->PURCHASE_DID_NOT_QUALIFY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    goto :goto_0

    .line 897
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->accumulateRequestSent:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->noThanksClicked:Z

    if-eqz v0, :cond_3

    goto :goto_0

    .line 902
    :cond_3
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_LOYALTY_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    :cond_4
    :goto_0
    return-void
.end method

.method private enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V
    .locals 5

    .line 907
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 908
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    .line 909
    invoke-virtual {v2}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;-><init>()V

    .line 911
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->reason(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;

    move-result-object p1

    .line 912
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->event_time(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->transactionEmployeeToken:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v4, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    .line 913
    invoke-static {v2, v3, v4}, Lcom/squareup/activity/refund/CreatorDetailsHelper;->forEmployeeToken(Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;)Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;

    move-result-object p1

    .line 914
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    move-result-object p1

    .line 910
    invoke-virtual {v1, p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->missedLoyaltyOpportunity(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;)Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;

    move-result-object p1

    .line 915
    invoke-virtual {p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->build()Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;

    move-result-object p1

    .line 908
    invoke-interface {v0, p1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private getElapsedTime()D
    .locals 4

    .line 833
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->screenCreationTimeMillis:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private getRequestCreatorFromUrl(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;
    .locals 1

    .line 888
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    return-object p1
.end method

.method private handleLoyaltyResult(Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;)V
    .locals 3

    .line 878
    instance-of v0, p1, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult$ShowRewardProgress;

    if-eqz v0, :cond_0

    .line 879
    invoke-virtual {p1}, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;->getLoyaltyEvent()Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->showRewardProgress(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    goto :goto_0

    .line 880
    :cond_0
    instance-of v0, p1, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult$ShowAllDone;

    if-eqz v0, :cond_1

    .line 881
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->showAllDone()V

    :goto_0
    return-void

    .line 883
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled AccumulateLoyaltyStateResult: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic lambda$null$11(Lkotlin/Unit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$null$14(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 0

    if-nez p0, :cond_1

    .line 481
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-static {p1, p2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 480
    :goto_1
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$15(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$null$18(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lkotlin/Unit;)V
    .locals 0

    .line 490
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 491
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showClaimYourStar()V

    return-void
.end method

.method static synthetic lambda$null$20(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;
    .locals 0

    .line 498
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->buildUpon()Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object p0

    .line 499
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object p0

    .line 500
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->build()Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$21(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;)V
    .locals 0

    .line 502
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 503
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->setEnrollment(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;)V

    return-void
.end method

.method static synthetic lambda$null$25(Lkotlin/Unit;Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;)Ljava/lang/String;
    .locals 0

    .line 529
    iget-object p0, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->phoneToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$null$31(Lkotlin/Unit;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p1
.end method

.method static synthetic lambda$null$32(Ljava/lang/Boolean;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 627
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$null$8(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3

    .line 447
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 448
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_1

    if-nez p3, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$9(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/lang/Boolean;)V
    .locals 0

    .line 453
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 454
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->setClaimYourStarEnabled(Z)V

    return-void
.end method

.method public static synthetic lambda$rTHehfwNQ967VtuGfX0DD_DcJec(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->handleLoyaltyResult(Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;)V

    return-void
.end method

.method private onEnrollLoyaltyClicked(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)V
    .locals 7

    .line 768
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollLoyaltySubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 769
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->restartScreenTimeout()V

    .line 771
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    .line 777
    invoke-virtual {v0}, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->generateTosAcceptance()Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    move-result-object v6

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    .line 772
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->accumulateLoyaltyWhenEnrolling(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 778
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$PjyqphhZaT1gjgxAs6DX539YEUk;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$PjyqphhZaT1gjgxAs6DX539YEUk;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)V

    .line 779
    invoke-virtual {p1, p2}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$rTHehfwNQ967VtuGfX0DD_DcJec;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$rTHehfwNQ967VtuGfX0DD_DcJec;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)V

    .line 782
    invoke-virtual {p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollLoyaltySubscription:Lrx/Subscription;

    .line 784
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {p1}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    .line 785
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getElapsedTime()D

    move-result-wide v0

    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->logActionEnrollSubmit(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;D)V

    const/4 p1, 0x1

    .line 787
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->accumulateRequestSent:Z

    return-void
.end method

.method private setClaimHelpText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V
    .locals 2

    .line 756
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->legalText(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimHelpText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    return-void
.end method

.method private setClaimText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;Z)V
    .locals 1

    if-eqz p2, :cond_1

    .line 745
    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget p2, p2, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    .line 746
    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->pointsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->loyalty_enroll_button_multiple_points:I

    invoke-virtual {p2, v0}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    goto :goto_0

    .line 748
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->pointsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->loyalty_enroll_button_one_point:I

    invoke-virtual {p2, v0}, Lcom/squareup/loyalty/PointsTermsFormatter;->singular(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    goto :goto_0

    .line 751
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->loyalty_enroll_button_no_stars:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    :goto_0
    return-void
.end method

.method private setEnrollLoyaltyStatus(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;Z)V
    .locals 2

    if-eqz p2, :cond_0

    .line 736
    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->pointsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget v0, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->loyalty_enroll_qualify:I

    invoke-virtual {p2, v0, v1}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->enrollmentLoyaltyStatus(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    goto :goto_0

    .line 739
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getLoyaltyProgramName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->enrollmentLoyaltyStatus(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    :goto_0
    return-void
.end method

.method private setRewardRequirementText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V
    .locals 1

    .line 760
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    invoke-virtual {v0}, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->loyaltyProgramRequirements()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardRequirementText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    return-void
.end method

.method private setRewardTiersLoyaltyStatus(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V
    .locals 2

    .line 731
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->pointsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->loyalty_enroll_almost_there:I

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardTiersLoyaltyStatus(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    return-void
.end method

.method private showAllDone()V
    .locals 2

    .line 814
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ALL_DONE_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private showNonQualifyingRewardTiers()V
    .locals 4

    .line 687
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 688
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->defaultPhoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receiptPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 689
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->receiptPhoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    .line 691
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setRewardTiersLoyaltyStatus(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V

    const/4 v1, 0x0

    .line 692
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setEnrollLoyaltyStatus(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;Z)V

    .line 693
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setRewardRequirementText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V

    .line 694
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/buyerflow/R$string;->loyalty_enroll_phone_edit_hint:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneEditHint(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    .line 695
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getLoyaltyProgramName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->programName(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    .line 696
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v2}, Lcom/squareup/loyalty/LoyaltySettings;->rewardTiers()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardTiers(Ljava/util/List;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    .line 698
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setClaimText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;Z)V

    .line 699
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setClaimHelpText(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V

    .line 700
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->NON_QUALIFYING_VIEW_TIERS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 702
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollment:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->build()Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private showRewardProgress(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    .line 791
    iget-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    iget-object v3, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->locale:Ljava/util/Locale;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CASH_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    .line 792
    invoke-interface {v1, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    iget-object v5, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->pointsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    iget-object v6, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    move-object/from16 v1, p1

    .line 791
    invoke-static/range {v1 .. v6}, Lcom/squareup/loyalty/LoyaltyRewardProgress;->fromLoyaltyEvent(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/util/Res;Ljava/util/Locale;ZLcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/loyalty/LoyaltyRewardProgress;

    move-result-object v1

    .line 795
    iget-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

    iget-object v3, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-virtual {v2, v3}, Lcom/squareup/loyalty/LoyaltyAnalytics;->logViewStatusProgressReward(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    .line 797
    iget-boolean v2, v7, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v7, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    sget-object v4, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->EARN_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    if-eq v2, v4, :cond_0

    const/4 v13, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    const/4 v13, 0x0

    .line 799
    :goto_0
    iget-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v2}, Lcom/squareup/loyalty/LoyaltySettings;->pointsTerms()Lcom/squareup/loyalty/PointsTerms;

    move-result-object v2

    iget v4, v7, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    invoke-virtual {v2, v4}, Lcom/squareup/loyalty/PointsTerms;->getTerm(I)Ljava/lang/String;

    move-result-object v6

    .line 800
    iget v2, v7, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    if-ne v2, v3, :cond_1

    iget-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/loyalty/R$string;->loyalty_rewards_term_singular:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    iget-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/loyalty/R$string;->loyalty_rewards_term_plural:I

    .line 802
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v10, v2

    .line 804
    iget-object v2, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->rewardStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v3, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;

    iget-object v5, v1, Lcom/squareup/loyalty/LoyaltyRewardProgress;->rewardText:Lcom/squareup/loyalty/LoyaltyRewardText;

    .line 806
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getLoyaltyProgramName()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v1, Lcom/squareup/loyalty/LoyaltyRewardProgress;->congratulationsText:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/loyalty/LoyaltyRewardProgress;->rewardSubtext:Lcom/squareup/loyalty/LoyaltyRewardText;

    iget-object v11, v7, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    iget-object v12, v7, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    iget-boolean v14, v7, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    const/4 v15, 0x0

    move-object v4, v3

    move-object v7, v8

    move-object v8, v9

    move-object v9, v1

    invoke-direct/range {v4 .. v15}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;-><init>(Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;)V

    .line 804
    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 810
    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->REWARD_STATUS_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method getDefaultPhone()Ljava/lang/String;
    .locals 1

    .line 828
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedPoints(J)Ljava/lang/String;
    .locals 2

    .line 920
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->pointsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->loyalty_reward_tier_requirement:I

    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getLoyaltyProgramName()Ljava/lang/String;
    .locals 3

    .line 860
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 864
    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getSubunitName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 865
    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getSubunitName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 866
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 867
    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 871
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->loyalty_program_name:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string/jumbo v2, "unit_name"

    .line 872
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 873
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 874
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method joinLoyaltyClicked()V
    .locals 2

    .line 673
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->restartScreenTimeout()V

    .line 674
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ENROLLMENT_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$12$LoyaltyPresenter(Ljava/lang/String;)V
    .locals 2

    .line 463
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 465
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultSms()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 466
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->reasonForPointAccrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->onEnrollLoyaltyClicked(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)V

    return-void
.end method

.method public synthetic lambda$null$16$LoyaltyPresenter(Ljava/lang/Boolean;)V
    .locals 1

    .line 484
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->showClaimYourStar:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$2$LoyaltyPresenter(Lkotlin/Unit;)V
    .locals 0

    .line 423
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->onNewSaleClicked()V

    return-void
.end method

.method public synthetic lambda$null$23$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;)V
    .locals 9

    .line 510
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 513
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showBuyerLoyaltyAccountInformation()V

    .line 514
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->obfuscatedPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidSmsNumber(Ljava/lang/String;)V

    .line 515
    iget-object v3, p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->programName:Ljava/lang/String;

    iget-object v4, p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->pointsAmount:Lcom/squareup/loyalty/LoyaltyRewardText;

    iget-object v5, p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->pointsTerm:Ljava/lang/String;

    iget-object v6, p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->rewardsAmount:Lcom/squareup/loyalty/LoyaltyRewardText;

    iget-object v7, p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->rewardsTerm:Ljava/lang/String;

    iget-object v8, p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->congratulationsTitle:Ljava/lang/String;

    move-object v2, p1

    invoke-virtual/range {v2 .. v8}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->setRewardStatusPoints(Ljava/lang/String;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    iget-boolean p1, p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->newZeroStarEnrollment:Z

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->isAnimating:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 522
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->isAnimating:Z

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$26$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/lang/String;)V
    .locals 2

    .line 532
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 533
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;-><init>()V

    .line 534
    invoke-virtual {v1, p2}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->phoneToken(Ljava/lang/String;)Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 535
    invoke-interface {v1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;

    move-result-object p2

    .line 536
    invoke-virtual {p2}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->build()Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;

    move-result-object p2

    .line 533
    invoke-interface {v0, p2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 538
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->updateBuyerLoyaltyAccountInformation()V

    .line 539
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->showLoyaltyStatusDialog()V

    return-void
.end method

.method public synthetic lambda$null$29$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lkotlin/Unit;)V
    .locals 0

    .line 594
    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {p2}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/Payment;->hasCustomer()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showCustomerButton(Z)V

    return-void
.end method

.method public synthetic lambda$null$33$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 629
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->onNewSaleClicked()V

    .line 630
    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/crm/events/CrmScreenTimeoutEvent;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/crm/events/CrmScreenTimeoutEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$null$4$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/payment/Payment;Lkotlin/Unit;)V
    .locals 1

    .line 430
    iget-object p3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-static {p2, p3, v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->shouldShowAddCardButton(Lcom/squareup/payment/Payment;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/crm/CustomerManagementSettings;)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showAddCardButton(Z)V

    return-void
.end method

.method public synthetic lambda$null$6$LoyaltyPresenter(Lkotlin/Unit;)V
    .locals 0

    .line 435
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->onAddCardClicked()V

    return-void
.end method

.method public synthetic lambda$onEnrollLoyaltyClicked$35$LoyaltyPresenter()V
    .locals 2

    .line 780
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 781
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->busyEnrolling:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-interface {v0}, Lcom/squareup/merchantimages/CuratedImage;->loadDarkened()Lio/reactivex/Observable;

    move-result-object v0

    .line 406
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$CjdO4ICm3GByghZOoQpyXyalPW0;

    invoke-direct {v1, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$CjdO4ICm3GByghZOoQpyXyalPW0;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 407
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$1$LoyaltyPresenter(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 418
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    invoke-interface {v0, p1}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public synthetic lambda$onLoad$10$LoyaltyPresenter(Lrx/Observable;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 4

    .line 440
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->busyEnrolling:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v2, 0x0

    .line 442
    invoke-static {v2}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->startWith(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 443
    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$tkVTYFSOQdjjmCYUQwwuF_Hne90;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$tkVTYFSOQdjjmCYUQwwuF_Hne90;

    .line 440
    invoke-static {v0, v1, v2, p1, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object p1

    .line 451
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$NWAue_w2P-CaGk5Kd8Wmkkh7l1k;

    invoke-direct {v0, p2}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$NWAue_w2P-CaGk5Kd8Wmkkh7l1k;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 452
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$13$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lrx/Observable;)Lrx/Subscription;
    .locals 1

    .line 459
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onClaimYourStarClicked()Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$0RvC-jNa1T8Y_KhI_VMwo8Lj9Ow;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$0RvC-jNa1T8Y_KhI_VMwo8Lj9Ow;

    .line 460
    invoke-virtual {p1, p2, v0}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$peilTZQWCHnLGoP2FqSMoO4DQ8E;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$peilTZQWCHnLGoP2FqSMoO4DQ8E;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)V

    .line 462
    invoke-virtual {p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$17$LoyaltyPresenter(Lrx/Observable;)Lrx/Subscription;
    .locals 3

    .line 475
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    .line 477
    invoke-static {v1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->startWith(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 478
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$xKabqJTGAvhU-sl6z5OSaiBq6YQ;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$xKabqJTGAvhU-sl6z5OSaiBq6YQ;

    .line 475
    invoke-static {p1, v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$IgoS8gWx99XSkGr_xRIs3Xsx10s;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$IgoS8gWx99XSkGr_xRIs3Xsx10s;

    .line 482
    invoke-virtual {p1, v0}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x1

    .line 483
    invoke-virtual {p1, v0}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$bnJElKFkmuupOv7xJECyb55PkOw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$bnJElKFkmuupOv7xJECyb55PkOw;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)V

    .line 484
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$19$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 2

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->showClaimYourStar:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 488
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$jkWoZAYHSF-FpK3YicWEaUGqnGA;

    invoke-direct {v1, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$jkWoZAYHSF-FpK3YicWEaUGqnGA;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 489
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$22$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 3

    .line 496
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollment:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$7hYIeOKAs03gobOfETiqz7eB3rs;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$7hYIeOKAs03gobOfETiqz7eB3rs;

    .line 497
    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$KZ4VJXrJHFG8bPd43SpiAPZYZqs;

    invoke-direct {v1, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$KZ4VJXrJHFG8bPd43SpiAPZYZqs;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 501
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$24$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 2

    .line 508
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->rewardStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$wNoMc5SBPtbYmBx4QWJ-OBpdplg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$wNoMc5SBPtbYmBx4QWJ-OBpdplg;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 509
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$27$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 3

    .line 528
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onSendBuyerLoyaltyStatusButtonClicked()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->rewardStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$x9YpZFvPfUL_PtAgpqc4PeiDIXA;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$x9YpZFvPfUL_PtAgpqc4PeiDIXA;

    .line 529
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 530
    invoke-virtual {v0, v1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$0_58kUWP6DwyuUDWinFaqaUs_Xs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$0_58kUWP6DwyuUDWinFaqaUs_Xs;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 531
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$28$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/util/concurrent/atomic/AtomicBoolean;)Lrx/Subscription;
    .locals 2

    .line 544
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 545
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 1

    .line 422
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onNewSaleClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$VQaKutNyeJpQD3CH-Vy0d1BPARI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$VQaKutNyeJpQD3CH-Vy0d1BPARI;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)V

    .line 423
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$30$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 2

    .line 593
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$5-nVPLxqJsLPf79pie9WK-9x9sQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$5-nVPLxqJsLPf79pie9WK-9x9sQ;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 594
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$34$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 624
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->screen:Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;

    .line 625
    invoke-virtual {v1, v2}, Lcom/squareup/ui/main/TopScreenChecker;->unobscured(Lcom/squareup/container/ContainerTreeKey;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$SS9hugRpfWLOnZB3WUxBuvTntsQ;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$SS9hugRpfWLOnZB3WUxBuvTntsQ;

    .line 624
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$v4Ccv2UUrDma5Xg8yLsdGjjkpjE;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$v4Ccv2UUrDma5Xg8yLsdGjjkpjE;

    .line 627
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$QHQrMvCwBf_4eo1MfycBIDkP_pE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$QHQrMvCwBf_4eo1MfycBIDkP_pE;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 628
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$LoyaltyPresenter(Lcom/squareup/payment/Payment;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 2

    .line 429
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$mf_tpWVy4qstkJ8tp0NGulJskpk;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$mf_tpWVy4qstkJ8tp0NGulJskpk;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/payment/Payment;)V

    .line 430
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$LoyaltyPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lrx/Subscription;
    .locals 1

    .line 434
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onAddCardClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$wDKXhrrQC1QRjb8otWf84vGPWzw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$wDKXhrrQC1QRjb8otWf84vGPWzw;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)V

    .line 435
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method noThanksClicked()V
    .locals 4

    .line 678
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->showAllDone()V

    .line 679
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getElapsedTime()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/loyalty/LoyaltyAnalytics;->logActionNoThanks(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;D)V

    const/4 v0, 0x1

    .line 681
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->noThanksClicked:Z

    .line 683
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->BUYER_DECLINED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    return-void
.end method

.method onAddCardClicked()V
    .locals 2

    .line 669
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstAddCardScreen(Lcom/squareup/payment/PaymentReceipt;)V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method onCustomerClicked()V
    .locals 2

    .line 664
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstCrmScreen(Lcom/squareup/payment/PaymentReceipt;)V

    .line 665
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 356
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 357
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->screen:Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;

    .line 359
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultSms()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 360
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->defaultPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultSms()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 361
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultSms()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 362
    sget-object p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_RECEIPT_PREFERENCE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->reasonForPointAccrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 365
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->hasValidSmsNumber()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 366
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receiptPhoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getValidSmsNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 367
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getValidSmsNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 368
    sget-object p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_RECEIPT_PREFERENCE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->reasonForPointAccrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 371
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->screen:Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;

    iget-object p1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    .line 373
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    if-eqz p1, :cond_3

    .line 374
    iget-boolean p1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    if-eqz p1, :cond_2

    .line 376
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    .line 377
    invoke-virtual {p1, v0}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->accumulateLoyaltyWhenAlreadyEnrolled(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$rTHehfwNQ967VtuGfX0DD_DcJec;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$rTHehfwNQ967VtuGfX0DD_DcJec;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)V

    .line 378
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollLoyaltySubscription:Lrx/Subscription;

    const/4 p1, 0x1

    .line 379
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->accumulateRequestSent:Z

    goto :goto_0

    .line 381
    :cond_2
    invoke-static {}, Lrx/subscriptions/Subscriptions;->empty()Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollLoyaltySubscription:Lrx/Subscription;

    goto :goto_0

    .line 387
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ALL_DONE_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 392
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->enrollLoyaltySubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 396
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    .line 398
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    if-eqz p1, :cond_0

    const-string v3, "ANIMATING_STATE_KEY"

    .line 400
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 401
    iput-boolean v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->isAnimating:Z

    .line 404
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantProfileSettings()Lcom/squareup/settings/server/MerchantProfileSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/MerchantProfileSettings;->shouldUseCuratedImageForReceipt()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 405
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$OkA4zvyPQ0wSwV0CZ-PUa1I2xVI;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$OkA4zvyPQ0wSwV0CZ-PUa1I2xVI;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 411
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getProfileImageUrl()Ljava/lang/String;

    move-result-object p1

    .line 412
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 413
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getRequestCreatorFromUrl(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->setLogoImage(Lcom/squareup/picasso/RequestCreator;)V

    .line 416
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 417
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    new-instance v3, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$R3NN5Jju7McevlaKoFwlkrCBRTM;

    invoke-direct {v3, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$R3NN5Jju7McevlaKoFwlkrCBRTM;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)V

    .line 418
    invoke-virtual {p1, v3}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 419
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    .line 422
    new-instance v3, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$jd0kDBacaUdd7vRd2eR3-piYu4I;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$jd0kDBacaUdd7vRd2eR3-piYu4I;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 426
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v3}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardPostTransactionEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v3}, Lcom/squareup/payment/PaymentReceipt;->isCard()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 427
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v3}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v3

    .line 428
    new-instance v4, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$t-Ndm6f-tLXTPM2S8DmHUwp_BmY;

    invoke-direct {v4, p0, v3, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$t-Ndm6f-tLXTPM2S8DmHUwp_BmY;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/payment/Payment;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, v4}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 434
    new-instance v3, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$dcg7wjGc5P_c45Xd2gSyZW0JCzM;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$dcg7wjGc5P_c45Xd2gSyZW0JCzM;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 439
    :cond_3
    new-instance v3, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$FNtxnVWQH5JRAAnP9kQWeYhDmPk;

    invoke-direct {v3, p0, p1, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$FNtxnVWQH5JRAAnP9kQWeYhDmPk;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lrx/Observable;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 458
    new-instance v3, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$1IxtWQJS5hP7fZlStze-0eiKuOo;

    invoke-direct {v3, p0, v0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$1IxtWQJS5hP7fZlStze-0eiKuOo;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lrx/Observable;)V

    invoke-static {v0, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 474
    new-instance v3, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$QYdFL9G29asGEnRI1qK5PmyVo3E;

    invoke-direct {v3, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$QYdFL9G29asGEnRI1qK5PmyVo3E;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lrx/Observable;)V

    invoke-static {v0, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 486
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$oz1f2DUtg_sqKXs-yb2g5piuhos;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$oz1f2DUtg_sqKXs-yb2g5piuhos;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 495
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$8qUCq4hK0aCKndP6Cu3Rv7nVgnU;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$8qUCq4hK0aCKndP6Cu3Rv7nVgnU;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 507
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$JvqtJdWIOS9Y1L6zUbNpnPQCW-0;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$JvqtJdWIOS9Y1L6zUbNpnPQCW-0;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 527
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$hUhuQ3prnQkTD-9kLCD8VtGejOA;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$hUhuQ3prnQkTD-9kLCD8VtGejOA;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 543
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$hHIxzAaTa_f669zToXkPZBxbEPM;

    invoke-direct {p1, p0, v0, v1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$hHIxzAaTa_f669zToXkPZBxbEPM;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 585
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 586
    sget p1, Lcom/squareup/checkout/R$string;->new_sale:I

    goto :goto_0

    :cond_4
    sget p1, Lcom/squareup/common/strings/R$string;->continue_label:I

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->setNewSaleText(I)V

    .line 590
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {p1}, Lcom/squareup/crm/CustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    instance-of p1, p1, Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;

    if-nez p1, :cond_5

    .line 592
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$PJqlPFbx-cfIfPECNJB711Xd338;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$PJqlPFbx-cfIfPECNJB711Xd338;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_5
    const/4 p1, 0x0

    .line 597
    invoke-virtual {v1, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 599
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->viewContents:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->hasValue()Z

    move-result v1

    if-nez v1, :cond_9

    .line 600
    sget-object v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$loyalty$MaybeLoyaltyEvent$LoyaltyEvent$Type:[I

    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget-object v3, v3, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    invoke-virtual {v3}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->ordinal()I

    move-result v3

    aget v1, v1, v3

    if-eq v1, v2, :cond_8

    const/4 v3, 0x2

    if-eq v1, v3, :cond_7

    const/4 v3, 0x3

    if-ne v1, v3, :cond_6

    goto :goto_1

    .line 613
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget-object v2, v2, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    aput-object v2, v1, p1

    const-string p1, "Unsupported loyalty event type %s"

    .line 614
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 608
    :cond_7
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->showNonQualifyingRewardTiers()V

    goto :goto_1

    .line 602
    :cond_8
    iput-boolean v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->isAnimating:Z

    .line 603
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget-boolean p1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    if-nez p1, :cond_9

    .line 604
    invoke-direct {p0, v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->beginEnrollment(Z)V

    .line 618
    :cond_9
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->screenCreationTimeMillis:J

    .line 620
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->restartScreenTimeout()V

    .line 623
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$tjQT1hWivtVcFXEKMC4JQ_EGmik;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyPresenter$tjQT1hWivtVcFXEKMC4JQ_EGmik;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onNewSaleClicked()V
    .locals 9

    .line 635
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 637
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v1

    .line 638
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 639
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v0, v3}, Lcom/squareup/log/CheckoutInformationEventLogger;->finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V

    .line 642
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->startSeparatedPrintoutsOrCompleteBuyerFlow()V

    .line 644
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    if-eqz v0, :cond_1

    .line 645
    iget-boolean v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->accumulateRequestSent:Z

    if-nez v1, :cond_1

    iget-object v0, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    sget-object v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->EARN_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->noThanksClicked:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 648
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getValidEmailAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v8, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 651
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getValidEmailAddress()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    .line 653
    invoke-virtual {v1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    .line 654
    invoke-virtual {v1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v5

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget-object v6, v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    iget-object v7, v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;)V

    .line 649
    invoke-interface {v0, v8}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 660
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->checkAndEnqueueMissedOpportunity()V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 837
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 838
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->isAnimating:Z

    if-eqz v0, :cond_0

    const-string v1, "ANIMATING_STATE_KEY"

    .line 839
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method restartScreenTimeout()V
    .locals 5

    .line 849
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v2}, Lcom/squareup/loyalty/LoyaltySettings;->getLoyaltyScreenTimeoutSeconds()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method setPhoneNumber(Ljava/lang/String;)V
    .locals 1

    .line 818
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 821
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_PHONE_KEYED_IN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->reasonForPointAccrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 823
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->phoneNumber:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 824
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->restartScreenTimeout()V

    return-void
.end method

.method shouldShowCashAppBanner()Z
    .locals 2

    .line 854
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CASH_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
