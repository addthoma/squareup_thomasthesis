.class Lcom/squareup/ui/buyer/loyalty/LoyaltyView$3;
.super Lcom/squareup/text/ScrubbingTextWatcher;
.source "LoyaltyView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$3;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-direct {p0, p2, p3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 149
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$3;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    iget-object v1, v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getDefaultPhone()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    invoke-super {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$3;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    iget-object v0, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->setPhoneNumber(Ljava/lang/String;)V

    return-void
.end method
