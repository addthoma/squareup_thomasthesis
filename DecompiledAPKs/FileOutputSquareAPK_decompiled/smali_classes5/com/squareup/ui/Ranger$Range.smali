.class Lcom/squareup/ui/Ranger$Range;
.super Ljava/lang/Object;
.source "Ranger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/Ranger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Range"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Enum;",
        ":",
        "Lcom/squareup/ui/Ranger$RowType;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final endExclusive:I

.field final rangeType:Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field final startInclusive:I


# direct methods
.method constructor <init>(IILjava/lang/Enum;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IITR;)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput p1, p0, Lcom/squareup/ui/Ranger$Range;->startInclusive:I

    .line 62
    iput p2, p0, Lcom/squareup/ui/Ranger$Range;->endExclusive:I

    .line 63
    iput-object p3, p0, Lcom/squareup/ui/Ranger$Range;->rangeType:Ljava/lang/Enum;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .line 67
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/ui/Ranger$Range;->rangeType:Ljava/lang/Enum;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/squareup/ui/Ranger$Range;->startInclusive:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget v2, p0, Lcom/squareup/ui/Ranger$Range;->endExclusive:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string v2, "%s [%d, %d)"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
