.class public Lcom/squareup/ui/account/view/SmartLineRow;
.super Landroid/widget/LinearLayout;
.source "SmartLineRow.java"


# static fields
.field private static final UNSET_TEXT_APPEARANCE:I = -0x1


# instance fields
.field private final badgeView:Landroid/widget/ImageView;

.field private final badgedIconBlock:Landroid/view/View;

.field private final chevronLeftMargin:I

.field private chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

.field private final colorError:I

.field private final endGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private glyphColor:Landroid/content/res/ColorStateList;

.field private final glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private isError:Z

.field private preserveValueText:Z

.field private subtitleColor:Landroid/content/res/ColorStateList;

.field private final subtitleField:Landroid/widget/TextView;

.field private titleColor:Landroid/content/res/ColorStateList;

.field private final titleField:Lcom/squareup/marketfont/MarketTextView;

.field private valueColor:Landroid/content/res/ColorStateList;

.field private final valueField:Lcom/squareup/widgets/ShorteningTextView;

.field private final valueSubtitleField:Landroid/widget/TextView;

.field private final vectorImage:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11

    .line 95
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    sget v0, Lcom/squareup/widgets/pos/R$layout;->smart_line_row_contents:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/account/view/SmartLineRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 98
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 99
    sget v0, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_chevronVisibility:I

    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 100
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ChevronVisibility;->ordinal()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 101
    invoke-static {}, Lcom/squareup/marin/widgets/ChevronVisibility;->values()[Lcom/squareup/marin/widgets/ChevronVisibility;

    move-result-object v1

    aget-object v0, v1, v0

    .line 102
    sget v1, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_sq_valueColor:I

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 103
    sget v2, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_sq_titleTextAppearance:I

    const/4 v3, -0x1

    .line 104
    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 105
    sget v4, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_sq_subtitleTextAppearance:I

    .line 106
    invoke-virtual {p2, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 107
    sget v5, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_sq_valueTextAppearance:I

    .line 108
    invoke-virtual {p2, v5, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 109
    sget v6, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_titleTextColor:I

    invoke-virtual {p2, v6}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    .line 110
    sget v7, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_titleText:I

    invoke-virtual {p2, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 111
    sget v8, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_subtitleText:I

    invoke-virtual {p2, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 112
    sget v9, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow_weight:I

    invoke-virtual {p2, v9, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v9

    .line 113
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 115
    sget p2, Lcom/squareup/widgets/pos/R$id;->badged_icon_block:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 116
    sget p2, Lcom/squareup/widgets/pos/R$id;->glyph:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 117
    sget p2, Lcom/squareup/widgets/pos/R$id;->vector_image:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->vectorImage:Landroid/widget/ImageView;

    .line 118
    sget p2, Lcom/squareup/widgets/pos/R$id;->badge:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgeView:Landroid/widget/ImageView;

    .line 119
    sget p2, Lcom/squareup/widgets/pos/R$id;->title:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 120
    sget p2, Lcom/squareup/widgets/pos/R$id;->subtitle:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    .line 121
    sget p2, Lcom/squareup/widgets/pos/R$id;->value:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/widgets/ShorteningTextView;

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    .line 122
    sget p2, Lcom/squareup/widgets/pos/R$id;->value_subtitle:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    .line 123
    sget p2, Lcom/squareup/widgets/pos/R$id;->chevron_block:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 126
    sget v10, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    .line 127
    invoke-virtual {p2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, p0, Lcom/squareup/ui/account/view/SmartLineRow;->chevronLeftMargin:I

    .line 128
    sget v10, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-virtual {p2, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->colorError:I

    .line 129
    iget-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphView;->getColorStateList()Landroid/content/res/ColorStateList;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphColor:Landroid/content/res/ColorStateList;

    if-eqz v6, :cond_0

    goto :goto_0

    .line 131
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2}, Lcom/squareup/marketfont/MarketTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v6

    :goto_0
    iput-object v6, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleColor:Landroid/content/res/ColorStateList;

    .line 132
    iget-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleColor:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_1

    goto :goto_1

    .line 133
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p2}, Lcom/squareup/widgets/ShorteningTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueColor:Landroid/content/res/ColorStateList;

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->setDefaultColors()V

    if-eq v2, v3, :cond_2

    .line 137
    iget-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2, p1, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_2
    if-eq v4, v3, :cond_3

    .line 141
    iget-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {p2, p1, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_3
    if-eq v5, v3, :cond_4

    .line 145
    iget-object p2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p2, p1, v5}, Lcom/squareup/widgets/ShorteningTextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_4
    const/4 p1, 0x1

    .line 149
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleEnabled(Z)V

    .line 150
    invoke-static {v7}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_5

    .line 151
    invoke-virtual {p0, v7}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    :cond_5
    if-eq v9, v3, :cond_6

    .line 154
    invoke-static {}, Lcom/squareup/marketfont/MarketFont$Weight;->values()[Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object p2

    aget-object p2, p2, v9

    invoke-virtual {p0, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    :cond_6
    const/4 p2, 0x0

    .line 156
    invoke-virtual {p0, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisible(Z)V

    .line 157
    invoke-virtual {p0, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgeVisible(Z)V

    .line 159
    invoke-static {v8}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_7

    .line 161
    invoke-virtual {p0, v8}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 163
    :cond_7
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 164
    invoke-virtual {p0, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 165
    invoke-virtual {p0, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    return-void
.end method

.method public static inflateForLayout(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 3

    .line 84
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/pos/R$layout;->smart_line_row:I

    const/4 v2, 0x0

    .line 85
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p0
.end method

.method public static inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 3

    .line 90
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/pos/R$layout;->smart_line_row_listitem:I

    const/4 v2, 0x0

    .line 91
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p0
.end method

.method private refreshChevron()V
    .locals 4

    .line 517
    sget-object v0, Lcom/squareup/ui/account/view/SmartLineRow$1;->$SwitchMap$com$squareup$marin$widgets$ChevronVisibility:[I

    iget-object v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ChevronVisibility;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x8

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    const/4 v3, 0x0

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 525
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v3}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    goto :goto_0

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    goto :goto_0

    .line 532
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized chevronVisibility attribute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 522
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v3}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    goto :goto_0

    .line 519
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private static setGlyphViewColorStateList(Lcom/squareup/glyph/SquareGlyphView;Landroid/content/res/ColorStateList;)V
    .locals 2

    .line 582
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphView;->getDrawableState()[I

    move-result-object v0

    .line 583
    invoke-virtual {p1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    .line 584
    invoke-virtual {p1, v0, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result p1

    .line 585
    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    return-void
.end method

.method private updateColors()V
    .locals 2

    .line 561
    iget-boolean v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->isError:Z

    if-eqz v0, :cond_1

    .line 563
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->colorError:I

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->vectorImage:Landroid/widget/ImageView;

    iget v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->colorError:I

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 567
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    iget v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->colorError:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    goto :goto_0

    .line 570
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 571
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphColor:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setGlyphViewColorStateList(Lcom/squareup/glyph/SquareGlyphView;Landroid/content/res/ColorStateList;)V

    .line 573
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->vectorImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 574
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 576
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 577
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    iget-object v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ShorteningTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method


# virtual methods
.method public getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    return-object v0
.end method

.method public getStartGlyphView()Lcom/squareup/glyph/SquareGlyphView;
    .locals 1

    .line 398
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    return-object v0
.end method

.method public getSubtitleText()Ljava/lang/CharSequence;
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .line 440
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .line 261
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getMeasuredWidth()I

    move-result p1

    .line 262
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getMeasuredHeight()I

    move-result p2

    .line 263
    div-int/lit8 p3, p2, 0x2

    .line 264
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getPaddingLeft()I

    move-result p4

    .line 265
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getPaddingRight()I

    move-result p5

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 267
    :goto_0
    iget-object v4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    if-eq v4, v2, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 268
    :goto_1
    iget-object v5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v5}, Lcom/squareup/glyph/SquareGlyphView;->getVisibility()I

    move-result v5

    if-eq v5, v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    sub-int/2addr p1, p4

    sub-int/2addr p1, p5

    .line 269
    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 272
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result p5

    sub-int/2addr p1, p5

    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 273
    invoke-virtual {p5}, Lcom/squareup/glyph/SquareGlyphView;->getMeasuredWidth()I

    move-result p5

    sub-int/2addr p1, p5

    if-eqz v1, :cond_3

    .line 276
    iget p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->chevronLeftMargin:I

    sub-int/2addr p1, p5

    .line 281
    :cond_3
    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 284
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p4

    .line 281
    invoke-virtual {p5, p4, v3, v2, p2}, Landroid/view/View;->layout(IIII)V

    if-eqz v0, :cond_4

    .line 290
    iget-object p4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p4}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result p4

    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {p5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p5

    add-int/2addr p4, p5

    .line 291
    div-int/lit8 p4, p4, 0x2

    .line 293
    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 294
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    sub-int v5, p3, p4

    iget-object v6, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 296
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v6

    iget-object v7, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v7}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 297
    invoke-virtual {v7}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v5

    .line 293
    invoke-virtual {p5, v2, v5, v6, v7}, Lcom/squareup/marketfont/MarketTextView;->layout(IIII)V

    .line 299
    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 300
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    iget-object v6, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 301
    invoke-virtual {v6}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 302
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v6

    iget-object v7, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr p4, p3

    .line 299
    invoke-virtual {p5, v2, v5, v6, p4}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_3

    .line 305
    :cond_4
    iget-object p4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 306
    invoke-virtual {p5}, Landroid/view/View;->getRight()I

    move-result p5

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 307
    invoke-virtual {v2}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, p3, v2

    iget-object v5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 308
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v5

    iget-object v6, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v6}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 309
    invoke-virtual {v6}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, p3

    .line 305
    invoke-virtual {p4, p5, v2, v5, v6}, Lcom/squareup/marketfont/MarketTextView;->layout(IIII)V

    .line 312
    iget-object p4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {p4, v3, v3, v3, v3}, Landroid/widget/TextView;->layout(IIII)V

    :goto_3
    if-eqz v0, :cond_5

    .line 317
    iget-object p4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    .line 318
    invoke-virtual {p4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result p4

    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p5}, Lcom/squareup/widgets/ShorteningTextView;->getMeasuredWidth()I

    move-result p5

    sub-int p5, p1, p5

    if-gt p4, p5, :cond_6

    :cond_5
    if-eqz v4, :cond_7

    .line 320
    :cond_6
    iget-object p4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    .line 321
    invoke-virtual {p4}, Lcom/squareup/widgets/ShorteningTextView;->getMeasuredHeight()I

    move-result p4

    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-virtual {p5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p5

    add-int/2addr p4, p5

    .line 322
    div-int/lit8 p4, p4, 0x2

    .line 324
    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 325
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    add-int/2addr v0, p1

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v2}, Lcom/squareup/widgets/ShorteningTextView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    .line 326
    invoke-virtual {v2}, Landroid/widget/TextView;->getTop()I

    move-result v2

    iget-object v5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v5}, Lcom/squareup/widgets/ShorteningTextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v2, v5

    iget-object v5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 327
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v5

    add-int/2addr v5, p1

    iget-object v6, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    .line 328
    invoke-virtual {v6}, Landroid/widget/TextView;->getTop()I

    move-result v6

    .line 324
    invoke-virtual {p5, v0, v2, v5, v6}, Lcom/squareup/widgets/ShorteningTextView;->layout(IIII)V

    if-eqz v4, :cond_8

    .line 331
    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 332
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    add-int/2addr v0, p1

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    sub-int v2, p3, p4

    iget-object v4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 333
    invoke-virtual {v4}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 334
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v4, p1

    add-int/2addr p3, p4

    .line 331
    invoke-virtual {p5, v0, v2, v4, p3}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_4

    .line 338
    :cond_7
    iget-object p4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 339
    invoke-virtual {p5}, Landroid/view/View;->getRight()I

    move-result p5

    add-int/2addr p5, p1

    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/ShorteningTextView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr p5, v0

    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    .line 340
    invoke-virtual {v0}, Lcom/squareup/widgets/ShorteningTextView;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p3, v0

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 341
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, p1

    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    .line 342
    invoke-virtual {p1}, Lcom/squareup/widgets/ShorteningTextView;->getMeasuredHeight()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    add-int/2addr p3, p1

    .line 338
    invoke-virtual {p4, p5, v0, v2, p3}, Lcom/squareup/widgets/ShorteningTextView;->layout(IIII)V

    :cond_8
    :goto_4
    if-eqz v1, :cond_9

    .line 347
    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    iget-object p3, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    .line 348
    invoke-virtual {p3}, Lcom/squareup/widgets/ShorteningTextView;->getRight()I

    move-result p3

    iget p4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->chevronLeftMargin:I

    add-int/2addr p3, p4

    iget-object p4, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    .line 350
    invoke-virtual {p4}, Lcom/squareup/widgets/ShorteningTextView;->getRight()I

    move-result p4

    iget p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->chevronLeftMargin:I

    add-int/2addr p4, p5

    iget-object p5, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p5}, Lcom/squareup/glyph/SquareGlyphView;->getMeasuredWidth()I

    move-result p5

    add-int/2addr p4, p5

    .line 347
    invoke-virtual {p1, p3, v3, p4, p2}, Lcom/squareup/glyph/SquareGlyphView;->layout(IIII)V

    :cond_9
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .line 169
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 170
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 171
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/4 v3, 0x1

    const/16 v4, 0x8

    const/4 v5, 0x0

    if-eq v2, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 172
    :goto_0
    iget-object v6, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v6}, Lcom/squareup/glyph/SquareGlyphView;->getVisibility()I

    move-result v6

    if-eq v6, v4, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    .line 173
    :goto_1
    iget-object v7, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v7}, Lcom/squareup/widgets/ShorteningTextView;->getVisibility()I

    move-result v7

    if-eq v7, v4, :cond_2

    const/4 v7, 0x1

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    .line 174
    :goto_2
    iget-object v8, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getVisibility()I

    move-result v8

    if-eq v8, v4, :cond_3

    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    :goto_3
    const/high16 v4, 0x40000000    # 2.0f

    .line 177
    invoke-static {v5, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    if-eqz v2, :cond_4

    .line 180
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 181
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v5, v8}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v8

    iget-object v9, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 182
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v9}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v9

    .line 180
    invoke-virtual {v2, v8, v9}, Landroid/view/View;->measure(II)V

    goto :goto_4

    .line 184
    :cond_4
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    invoke-virtual {v2, v4, v4}, Landroid/view/View;->measure(II)V

    :goto_4
    if-eqz v6, :cond_5

    .line 188
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 189
    invoke-virtual {v2}, Lcom/squareup/glyph/SquareGlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v5, v8}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result p1

    iget-object v8, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 190
    invoke-virtual {v8}, Lcom/squareup/glyph/SquareGlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v8}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v8

    .line 188
    invoke-virtual {v2, p1, v8}, Lcom/squareup/glyph/SquareGlyphView;->measure(II)V

    goto :goto_5

    .line 192
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, v4, v4}, Lcom/squareup/glyph/SquareGlyphView;->measure(II)V

    .line 197
    :goto_5
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getPaddingLeft()I

    move-result p1

    sub-int p1, v0, p1

    .line 198
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getPaddingRight()I

    move-result v2

    sub-int/2addr p1, v2

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    .line 199
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr p1, v2

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 200
    invoke-virtual {v2}, Lcom/squareup/glyph/SquareGlyphView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr p1, v2

    if-eqz v6, :cond_6

    .line 203
    iget v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->chevronLeftMargin:I

    sub-int/2addr p1, v2

    .line 208
    :cond_6
    iget-boolean v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->preserveValueText:Z

    const/high16 v6, -0x80000000

    if-eqz v2, :cond_7

    if-eqz v7, :cond_7

    .line 210
    invoke-static {p1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 211
    iget-object v7, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    .line 212
    invoke-virtual {v7}, Lcom/squareup/widgets/ShorteningTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v8}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v8

    .line 211
    invoke-virtual {v7, v2, v8}, Lcom/squareup/widgets/ShorteningTextView;->measure(II)V

    .line 215
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v2}, Lcom/squareup/widgets/ShorteningTextView;->getMeasuredWidth()I

    move-result v2

    sub-int v2, p1, v2

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 216
    iget-object v7, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 217
    invoke-virtual {v7}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v8}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v8

    .line 216
    invoke-virtual {v7, v2, v8}, Lcom/squareup/marketfont/MarketTextView;->measure(II)V

    goto :goto_6

    .line 221
    :cond_7
    invoke-static {p1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 222
    iget-object v8, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v8}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v2, v5, v9}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v2

    iget-object v9, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 223
    invoke-virtual {v9}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v9}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v9

    .line 222
    invoke-virtual {v8, v2, v9}, Lcom/squareup/marketfont/MarketTextView;->measure(II)V

    if-eqz v7, :cond_8

    .line 227
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v2}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v2

    sub-int v2, p1, v2

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 228
    iget-object v7, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    .line 229
    invoke-virtual {v7}, Lcom/squareup/widgets/ShorteningTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v8}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v8

    .line 228
    invoke-virtual {v7, v2, v8}, Lcom/squareup/widgets/ShorteningTextView;->measure(II)V

    goto :goto_6

    .line 231
    :cond_8
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v2, v4, v4}, Lcom/squareup/widgets/ShorteningTextView;->measure(II)V

    :goto_6
    if-eqz v3, :cond_9

    .line 238
    invoke-static {p1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 239
    iget-object v3, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    .line 240
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result v4

    .line 239
    invoke-virtual {v3, v2, v4}, Landroid/widget/TextView;->measure(II)V

    .line 243
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    .line 244
    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr p1, v2

    invoke-static {p1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 245
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    .line 246
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result p2

    .line 245
    invoke-virtual {v2, p1, p2}, Landroid/widget/TextView;->measure(II)V

    goto :goto_7

    .line 248
    :cond_9
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-virtual {v2, v4, v4}, Landroid/widget/TextView;->measure(II)V

    .line 251
    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-static {p1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    iget-object v3, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    .line 252
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 251
    invoke-static {p1, v5, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result p1

    iget-object v3, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    .line 253
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v5, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->getChildMeasureSpec(III)I

    move-result p2

    .line 251
    invoke-virtual {v2, p1, p2}, Landroid/widget/TextView;->measure(II)V

    .line 257
    :goto_7
    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setMeasuredDimension(II)V

    return-void
.end method

.method public setBadgeImage(I)V
    .locals 1

    .line 411
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgeView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public setBadgeVisible(Z)V
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgeView:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setBadgedIconBlockVisibility(I)V
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setBadgedIconBlockVisible(Z)V
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->badgedIconBlock:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V
    .locals 0

    .line 512
    iput-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 513
    invoke-direct {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->refreshChevron()V

    return-void
.end method

.method public setDefaultColors()V
    .locals 1

    const/4 v0, 0x0

    .line 556
    iput-boolean v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->isError:Z

    .line 557
    invoke-direct {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->updateColors()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 356
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 357
    invoke-direct {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->refreshChevron()V

    return-void
.end method

.method public setEndGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->endGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public setErrorColors()V
    .locals 1

    const/4 v0, 0x1

    .line 551
    iput-boolean v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->isError:Z

    .line 552
    invoke-direct {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->updateColors()V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 382
    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 383
    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->vectorImage:Landroid/widget/ImageView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 386
    invoke-direct {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->updateColors()V

    return-void
.end method

.method public setPreserveValueText(Z)V
    .locals 0

    .line 365
    iput-boolean p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->preserveValueText:Z

    .line 366
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->requestLayout()V

    .line 367
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->invalidate()V

    return-void
.end method

.method public setSubtitleColor(I)V
    .locals 2

    .line 456
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setSubtitleText(I)V
    .locals 1

    .line 460
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 452
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSubtitleTypeface(Landroid/graphics/Typeface;I)V
    .locals 1

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    return-void
.end method

.method public setSubtitleVisible(Z)V
    .locals 1

    .line 468
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->subtitleField:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setTitleBottomPadding(I)V
    .locals 5

    .line 435
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v2}, Lcom/squareup/marketfont/MarketTextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    .line 436
    invoke-virtual {v3}, Lcom/squareup/marketfont/MarketTextView;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    .line 435
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/marketfont/MarketTextView;->setPadding(IIII)V

    return-void
.end method

.method public setTitleColor(I)V
    .locals 2

    .line 538
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setTitleEnabled(Z)V
    .locals 1

    .line 448
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    return-void
.end method

.method public setTitleText(I)V
    .locals 1

    .line 423
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 419
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitleTypeface(Landroid/graphics/Typeface;I)V
    .locals 1

    .line 431
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    return-void
.end method

.method public setTitleWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->titleField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method public setValueColor(I)V
    .locals 1

    .line 543
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColorStateList(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setValueColorStateList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 547
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setValueShortText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 476
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setValueStrikeThrough(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 489
    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1}, Lcom/squareup/widgets/ShorteningTextView;->getPaintFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x10

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ShorteningTextView;->setPaintFlags(I)V

    goto :goto_0

    .line 491
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1}, Lcom/squareup/widgets/ShorteningTextView;->getPaintFlags()I

    move-result v0

    and-int/lit8 v0, v0, -0x11

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ShorteningTextView;->setPaintFlags(I)V

    :goto_0
    return-void
.end method

.method public setValueSubtitleColor(I)V
    .locals 2

    .line 508
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setValueSubtitleText(I)V
    .locals 1

    .line 496
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setValueSubtitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 500
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setValueSubtitleVisible(Z)V
    .locals 1

    .line 504
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueSubtitleField:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setValueText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 472
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setValueTruncateShortText(Z)V
    .locals 1

    .line 480
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setShowShortTextWhenEllipsized(Z)V

    return-void
.end method

.method public setValueVisible(Z)V
    .locals 1

    .line 484
    iget-object v0, p0, Lcom/squareup/ui/account/view/SmartLineRow;->valueField:Lcom/squareup/widgets/ShorteningTextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setVectorImage(I)V
    .locals 3

    .line 371
    new-instance v0, Landroid/view/ContextThemeWrapper;

    .line 372
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$style;->Theme_Noho:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 373
    iget-object v1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->vectorImage:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 374
    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->vectorImage:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 375
    iget-object p1, p0, Lcom/squareup/ui/account/view/SmartLineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 377
    invoke-direct {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->updateColors()V

    return-void
.end method
