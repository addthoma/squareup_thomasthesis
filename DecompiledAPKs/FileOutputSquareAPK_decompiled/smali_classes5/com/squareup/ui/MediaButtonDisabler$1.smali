.class Lcom/squareup/ui/MediaButtonDisabler$1;
.super Landroid/content/BroadcastReceiver;
.source "MediaButtonDisabler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/MediaButtonDisabler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/MediaButtonDisabler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/MediaButtonDisabler;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/MediaButtonDisabler$1;->this$0:Lcom/squareup/ui/MediaButtonDisabler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Intercepted media button."

    .line 27
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/ui/MediaButtonDisabler$1;->abortBroadcast()V

    return-void
.end method
