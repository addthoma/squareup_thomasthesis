.class public Lcom/squareup/ui/LocationActivity;
.super Lcom/squareup/ui/SquareActivity;
.source "LocationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/LocationActivity$MessageType;,
        Lcom/squareup/ui/LocationActivity$Component;
    }
.end annotation


# static fields
.field private static final DURATION_MS:I = 0x7d0


# instance fields
.field private anim:Landroid/view/animation/Animation;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private image:Landroid/view/View;

.field locationCanceledHandler:Lcom/squareup/ui/LocationActivityBackHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private locationSettings:Landroid/view/View;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->LOGGED_IN:Lcom/squareup/ui/SquareActivity$Preconditions;

    invoke-direct {p0, v0}, Lcom/squareup/ui/SquareActivity;-><init>(Lcom/squareup/ui/SquareActivity$Preconditions;)V

    return-void
.end method

.method public static synthetic lambda$1kP5JgiU1wN4kskgl0SBTctyaZI(Lcom/squareup/ui/LocationActivity;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/LocationActivity;->onLocationChanged(Landroid/location/Location;)V

    return-void
.end method

.method private loadBackground()V
    .locals 6

    .line 68
    invoke-static {p0}, Lcom/squareup/util/Views;->getDisplaySize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 70
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/squareup/ui/LocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ui/LocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/common/bootstrap/R$drawable;->location_background:I

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v3, v4, v5, v0}, Lcom/squareup/ui/Bitmaps;->decodeSampledBitmapFromResource(Landroid/content/res/Resources;III)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/LocationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private onLocationChanged(Landroid/location/Location;)V
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/LocationActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v0}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->isEnabled()Z

    move-result v0

    .line 112
    iget-object v1, p0, Lcom/squareup/ui/LocationActivity;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ENFORCE_LOCATION_FIX:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    const/4 p1, -0x1

    .line 116
    invoke-virtual {p0, p1}, Lcom/squareup/ui/LocationActivity;->setResult(I)V

    .line 117
    invoke-virtual {p0}, Lcom/squareup/ui/LocationActivity;->finish()V

    goto :goto_2

    .line 119
    :cond_3
    invoke-direct {p0}, Lcom/squareup/ui/LocationActivity;->updateMessage()V

    :goto_2
    return-void
.end method

.method private updateMessage()V
    .locals 4

    .line 143
    sget-object v0, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_DISABLED:Lcom/squareup/ui/LocationActivity$MessageType;

    .line 144
    iget-object v1, p0, Lcom/squareup/ui/LocationActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->isGpsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/LocationActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->isNetworkEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    sget-object v0, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_ENABLED_BOTH:Lcom/squareup/ui/LocationActivity$MessageType;

    goto :goto_0

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/LocationActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 149
    sget-object v0, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_ENABLED_ONE:Lcom/squareup/ui/LocationActivity$MessageType;

    .line 152
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/LocationActivity;->titleView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/ui/LocationActivity$MessageType;->access$000(Lcom/squareup/ui/LocationActivity$MessageType;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 153
    new-instance v1, Landroid/text/SpannableString;

    invoke-static {v0}, Lcom/squareup/ui/LocationActivity$MessageType;->access$100(Lcom/squareup/ui/LocationActivity$MessageType;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/squareup/ui/LocationActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 154
    sget v2, Lcom/squareup/common/bootstrap/R$string;->location_use_gps_satellites:I

    .line 156
    invoke-virtual {p0, v2}, Lcom/squareup/ui/LocationActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 157
    invoke-static {p0, v3}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v3

    .line 154
    invoke-static {v1, v2, v3}, Lcom/squareup/text/Spannables;->spanFirstIfPresent(Landroid/text/Spannable;Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    .line 158
    sget v2, Lcom/squareup/common/bootstrap/R$string;->location_use_wireless_networks:I

    .line 160
    invoke-virtual {p0, v2}, Lcom/squareup/ui/LocationActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 161
    invoke-static {p0, v3}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v3

    .line 158
    invoke-static {v1, v2, v3}, Lcom/squareup/text/Spannables;->spanFirstIfPresent(Landroid/text/Spannable;Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    .line 162
    iget-object v2, p0, Lcom/squareup/ui/LocationActivity;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v2, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v1, p0, Lcom/squareup/ui/LocationActivity;->locationSettings:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/ui/LocationActivity$MessageType;->access$200(Lcom/squareup/ui/LocationActivity$MessageType;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 164
    invoke-static {}, Lcom/squareup/util/Views;->noAnimationsForInstrumentation()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, Lcom/squareup/ui/LocationActivity$MessageType;->access$300(Lcom/squareup/ui/LocationActivity$MessageType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/LocationActivity;->image:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/LocationActivity;->anim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/LocationActivity;->image:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    :goto_1
    return-void
.end method


# virtual methods
.method protected doBackPressed()V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/LocationActivity;->locationCanceledHandler:Lcom/squareup/ui/LocationActivityBackHandler;

    invoke-interface {v0, p0}, Lcom/squareup/ui/LocationActivityBackHandler;->onLocationActivityBackPressed(Lcom/squareup/ui/LocationActivity;)V

    return-void
.end method

.method protected doCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 77
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->doCreate(Landroid/os/Bundle;)V

    .line 79
    sget p1, Lcom/squareup/common/bootstrap/R$layout;->location_activity:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/LocationActivity;->setContentView(I)V

    .line 81
    sget p1, Lcom/squareup/common/bootstrap/R$id;->image:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/LocationActivity;->image:Landroid/view/View;

    .line 82
    invoke-direct {p0}, Lcom/squareup/ui/LocationActivity;->loadBackground()V

    .line 84
    sget p1, Lcom/squareup/common/bootstrap/R$id;->location_settings_button:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/LocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/LocationActivity;->locationSettings:Landroid/view/View;

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/LocationActivity;->locationSettings:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/LocationActivity$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/LocationActivity$1;-><init>(Lcom/squareup/ui/LocationActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    sget p1, Lcom/squareup/common/bootstrap/R$id;->title:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/LocationActivity;->findById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/LocationActivity;->titleView:Landroid/widget/TextView;

    .line 92
    sget p1, Lcom/squareup/common/bootstrap/R$id;->message:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/LocationActivity;->findById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/LocationActivity;->messageView:Lcom/squareup/widgets/MessageView;

    .line 94
    new-instance p1, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object p1, p0, Lcom/squareup/ui/LocationActivity;->anim:Landroid/view/animation/Animation;

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/LocationActivity;->anim:Landroid/view/animation/Animation;

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/LocationActivity;->anim:Landroid/view/animation/Animation;

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/LocationActivity;->anim:Landroid/view/animation/Animation;

    const-wide/16 v0, 0x7d0

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/LocationActivity;->image:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/-$$Lambda$LocationActivity$p-tEqAkgaKBlh6jKTQ9yf86P5CI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/-$$Lambda$LocationActivity$p-tEqAkgaKBlh6jKTQ9yf86P5CI;-><init>(Lcom/squareup/ui/LocationActivity;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected inject(Lmortar/MortarScope;)V
    .locals 1

    .line 64
    const-class v0, Lcom/squareup/ui/LocationActivity$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/LocationActivity$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/LocationActivity$Component;->inject(Lcom/squareup/ui/LocationActivity;)V

    return-void
.end method

.method public synthetic lambda$doCreate$0$LocationActivity()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/LocationActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v0}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->onLocation()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/-$$Lambda$LocationActivity$1kP5JgiU1wN4kskgl0SBTctyaZI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/-$$Lambda$LocationActivity$1kP5JgiU1wN4kskgl0SBTctyaZI;-><init>(Lcom/squareup/ui/LocationActivity;)V

    .line 102
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onResume()V
    .locals 1

    .line 106
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onResume()V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/LocationActivity;->locationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-direct {p0, v0}, Lcom/squareup/ui/LocationActivity;->onLocationChanged(Landroid/location/Location;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .line 124
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_0

    .line 130
    invoke-virtual {p0}, Lcom/squareup/ui/LocationActivity;->requestFrequentLocationUpdates()V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/LocationActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {p1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->start()V

    goto :goto_0

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/LocationActivity;->stopFrequentLocationUpdates()V

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/LocationActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {p1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->pause()V

    :goto_0
    return-void
.end method

.method protected requiresLocation()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
