.class public final Lcom/squareup/ui/buyercart/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final buyer_cart_default_padding:I = 0x7f070092

.field public static final buyer_cart_item_description_right_padding:I = 0x7f070093

.field public static final buyer_cart_item_description_top_padding:I = 0x7f070094

.field public static final buyer_cart_item_padding_bottom:I = 0x7f070095

.field public static final buyer_cart_label_text:I = 0x7f070096

.field public static final buyer_cart_title_text:I = 0x7f070097

.field public static final buyer_cart_title_text_default_bottom_padding:I = 0x7f070098

.field public static final buyer_cart_title_text_default_top_padding:I = 0x7f070099

.field public static final cart_item_description_right_padding:I = 0x7f0700b7

.field public static final cart_item_description_top_padding:I = 0x7f0700b8

.field public static final cart_item_diff_highlight_height:I = 0x7f0700b9

.field public static final cart_item_diff_highlight_width:I = 0x7f0700ba

.field public static final cart_item_padding_bottom:I = 0x7f0700bb

.field public static final cart_total_bottom_padding:I = 0x7f0700bf

.field public static final cart_total_top_padding:I = 0x7f0700c0

.field public static final confirm_and_pay_button_height:I = 0x7f0700d4

.field public static final confirm_and_pay_button_text_size:I = 0x7f0700d5

.field public static final default_padding:I = 0x7f0700f2

.field public static final display_text:I = 0x7f07013f

.field public static final label_text:I = 0x7f0701e4

.field public static final title_text:I = 0x7f070539

.field public static final title_text_default_bottom_padding:I = 0x7f07053a

.field public static final title_text_default_top_padding:I = 0x7f07053b


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
