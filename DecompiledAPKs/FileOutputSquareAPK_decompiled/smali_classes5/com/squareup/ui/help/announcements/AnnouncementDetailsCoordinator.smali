.class public Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AnnouncementDetailsCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private button1:Landroid/widget/TextView;

.field private button2:Landroid/widget/TextView;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private imageView:Landroid/widget/ImageView;

.field private final longNoYearDateFormat:Ljava/text/DateFormat;

.field private messageView:Landroid/widget/TextView;

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/picasso/Picasso;Ljava/text/DateFormat;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->longNoYearDateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 120
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 121
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 122
    sget v0, Lcom/squareup/applet/help/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->titleView:Landroid/widget/TextView;

    .line 123
    sget v0, Lcom/squareup/applet/help/R$id;->message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->messageView:Landroid/widget/TextView;

    .line 124
    sget v0, Lcom/squareup/applet/help/R$id;->image:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->imageView:Landroid/widget/ImageView;

    .line 125
    sget v0, Lcom/squareup/applet/help/R$id;->button1:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->button1:Landroid/widget/TextView;

    .line 126
    sget v0, Lcom/squareup/applet/help/R$id;->button2:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->button2:Landroid/widget/TextView;

    return-void
.end method

.method private getActionBarConfig(Landroid/content/res/Resources;Lcom/squareup/server/messages/Message;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    .line 110
    iget-object v1, p2, Lcom/squareup/server/messages/Message;->timestamp:Ljava/util/Date;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->longNoYearDateFormat:Ljava/text/DateFormat;

    iget-object p2, p2, Lcom/squareup/server/messages/Message;->timestamp:Ljava/util/Date;

    .line 113
    invoke-virtual {p1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    sget p2, Lcom/squareup/applet/help/R$string;->announcements:I

    .line 114
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 116
    :goto_1
    iget-object p2, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreenData;)Lcom/squareup/server/messages/Message;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 55
    iget-object p0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreenData;->announcement:Lcom/squareup/server/messages/Message;

    return-object p0
.end method

.method private setButtons(Lcom/squareup/server/messages/Message;)V
    .locals 5

    .line 86
    invoke-virtual {p1}, Lcom/squareup/server/messages/Message;->getButtons()Ljava/util/List;

    move-result-object v0

    .line 87
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 90
    iget-object v2, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->button1:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/messages/Message$Button;

    iget-object v4, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v4}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->showButton(Landroid/widget/TextView;Lcom/squareup/server/messages/Message$Button;Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->button2:Landroid/widget/TextView;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/messages/Message$Button;

    iget-object p1, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    invoke-direct {p0, v1, v0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->showButton(Landroid/widget/TextView;Lcom/squareup/server/messages/Message$Button;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private setImage(Ljava/lang/String;)V
    .locals 4

    .line 73
    invoke-static {p1}, Lcom/squareup/util/Urls;->validateImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    sget-object v0, Lcom/squareup/picasso/MemoryPolicy;->NO_CACHE:Lcom/squareup/picasso/MemoryPolicy;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/squareup/picasso/MemoryPolicy;

    sget-object v3, Lcom/squareup/picasso/MemoryPolicy;->NO_STORE:Lcom/squareup/picasso/MemoryPolicy;

    aput-object v3, v2, v1

    .line 77
    invoke-virtual {p1, v0, v2}, Lcom/squareup/picasso/RequestCreator;->memoryPolicy(Lcom/squareup/picasso/MemoryPolicy;[Lcom/squareup/picasso/MemoryPolicy;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$drawable;->marin_photo_placeholder:I

    .line 79
    invoke-virtual {p1, v0}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->imageView:Landroid/widget/ImageView;

    .line 80
    invoke-virtual {p1, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    :cond_0
    return-void
.end method

.method private showButton(Landroid/widget/TextView;Lcom/squareup/server/messages/Message$Button;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 100
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v0, p2, Lcom/squareup/server/messages/Message$Button;->label:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object p2, p2, Lcom/squareup/server/messages/Message$Button;->url:Ljava/lang/String;

    .line 104
    new-instance v0, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsCoordinator$evylp-IFTWaRTSAFhU2ANx6p1uo;

    invoke-direct {v0, p0, p3, p2}, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsCoordinator$evylp-IFTWaRTSAFhU2ANx6p1uo;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private updateViews(Landroid/content/Context;Lcom/squareup/server/messages/Message;)V
    .locals 1

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->getActionBarConfig(Landroid/content/res/Resources;Lcom/squareup/server/messages/Message;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->titleView:Landroid/widget/TextView;

    iget-object v0, p2, Lcom/squareup/server/messages/Message;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->messageView:Landroid/widget/TextView;

    iget-object v0, p2, Lcom/squareup/server/messages/Message;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    invoke-virtual {p2}, Lcom/squareup/server/messages/Message;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->setImage(Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, p2}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->setButtons(Lcom/squareup/server/messages/Message;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->bindViews(Landroid/view/View;)V

    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;

    .line 54
    new-instance v1, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsCoordinator$FaMKs97OZyEl9M_NA1j1watdSjs;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsCoordinator$FaMKs97OZyEl9M_NA1j1watdSjs;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;Landroid/view/View;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$2$AnnouncementDetailsCoordinator(Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;Landroid/view/View;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->announcementDetailsScreenData(Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsCoordinator$5he6fkDaVfIOgUHeQp6xaBfqgmo;->INSTANCE:Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsCoordinator$5he6fkDaVfIOgUHeQp6xaBfqgmo;

    .line 55
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsCoordinator$3dCAtGdvVY2P_aWZ6ek_Cx1l4rw;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsCoordinator$3dCAtGdvVY2P_aWZ6ek_Cx1l4rw;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;Landroid/view/View;)V

    .line 56
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$AnnouncementDetailsCoordinator(Landroid/view/View;Lcom/squareup/server/messages/Message;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->updateViews(Landroid/content/Context;Lcom/squareup/server/messages/Message;)V

    return-void
.end method

.method public synthetic lambda$showButton$3$AnnouncementDetailsCoordinator(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {v0, p3, p1, p2}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->announcementButtonClicked(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
