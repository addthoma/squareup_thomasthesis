.class public Lcom/squareup/ui/help/announcements/events/TapAnnouncementButtonEvent;
.super Lcom/squareup/analytics/event/v1/TapEvent;
.source "TapAnnouncementButtonEvent.java"


# instance fields
.field final tracker_token:Ljava/lang/String;

.field final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->MESSAGE_CENTER_MESSAGE_LINK:Lcom/squareup/analytics/RegisterTapName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TapEvent;-><init>(Lcom/squareup/analytics/EventNamedTap;)V

    .line 12
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/events/TapAnnouncementButtonEvent;->tracker_token:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/squareup/ui/help/announcements/events/TapAnnouncementButtonEvent;->url:Ljava/lang/String;

    return-void
.end method
