.class public final Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;
.super Ljava/lang/Object;
.source "PosHelpContentStaticLinks.kt"

# interfaces
.implements Lcom/squareup/ui/help/api/HelpContentStaticLinks;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;",
        "Lcom/squareup/ui/help/api/HelpContentStaticLinks;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;)V",
        "forFrequentlyAskedQuestions",
        "",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "forLearnMore",
        "forTroubleShooting",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountStatusSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public forFrequentlyAskedQuestions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "activationFeatures"

    .line 34
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->isEligibleForSquareCardPayments()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    sget-object v0, Lcom/squareup/ui/help/help/ChangeBankAccountStaticLinkData;->INSTANCE:Lcom/squareup/ui/help/help/ChangeBankAccountStaticLinkData;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    sget-object v0, Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;->INSTANCE:Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    sget-object v0, Lcom/squareup/ui/help/help/InAppSummariesStaticLinkData;->INSTANCE:Lcom/squareup/ui/help/help/InAppSummariesStaticLinkData;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v0, Lcom/squareup/ui/help/help/SposTroubleShootingStaticLinkData;->INSTANCE:Lcom/squareup/ui/help/help/SposTroubleShootingStaticLinkData;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public forLearnMore()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "accountStatusSettings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 22
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 23
    sget-object v2, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v2, :cond_0

    .line 24
    new-instance v0, Lcom/squareup/ui/help/help/AskTheCommunityStaticLinkData;

    iget-object v2, p0, Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v2}, Lcom/squareup/ui/help/help/AskTheCommunityStaticLinkData;-><init>(Lcom/squareup/util/Res;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    :cond_0
    new-instance v0, Lcom/squareup/ui/help/help/VisitSupportCenterStaticLinkData;

    iget-object v2, p0, Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v2}, Lcom/squareup/ui/help/help/VisitSupportCenterStaticLinkData;-><init>(Lcom/squareup/util/Res;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    new-instance v0, Lcom/squareup/ui/help/help/SystemStatusStaticLinkData;

    iget-object v2, p0, Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v2}, Lcom/squareup/ui/help/help/SystemStatusStaticLinkData;-><init>(Lcom/squareup/util/Res;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public forTroubleShooting()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation

    .line 44
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
