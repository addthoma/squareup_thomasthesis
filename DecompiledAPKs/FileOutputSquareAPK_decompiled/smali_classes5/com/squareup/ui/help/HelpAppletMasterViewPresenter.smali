.class public Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;
.super Lcom/squareup/applet/AppletMasterViewPresenter;
.source "HelpAppletMasterViewPresenter.java"


# instance fields
.field private final appletSectionsListPresenter:Lcom/squareup/applet/AppletSectionsListPresenter;

.field private final runner:Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method constructor <init>(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;Lcom/squareup/applet/AppletSelection;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/applet/AppletSectionsListPresenter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    sget v0, Lcom/squareup/applet/help/R$string;->support_title:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2, p4}, Lcom/squareup/applet/AppletMasterViewPresenter;-><init>(Lcom/squareup/applet/ActionBarNavigationHelper;Ljava/lang/String;Lcom/squareup/applet/AppletSelection;)V

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->runner:Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;

    .line 34
    iput-object p5, p0, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 35
    iput-object p6, p0, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->appletSectionsListPresenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    return-void
.end method

.method public static synthetic lambda$jcJYl1qqEFcBOBfme85TQOTc7PQ(Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;Lcom/squareup/onboarding/BannerButton;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->updateBanner(Lcom/squareup/onboarding/BannerButton;)V

    return-void
.end method

.method private updateBanner(Lcom/squareup/onboarding/BannerButton;)V
    .locals 4

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletMasterView;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletMasterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    sget v2, Lcom/squareup/pos/container/R$id;->settings_section_banner:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/applet/AppletBanner;

    .line 55
    invoke-virtual {p1}, Lcom/squareup/onboarding/BannerButton;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    new-instance v2, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$Gbu1ratBVYP8951-UzRlIkr9VZw;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$Gbu1ratBVYP8951-UzRlIkr9VZw;-><init>(Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;Lcom/squareup/onboarding/BannerButton;Landroid/content/res/Resources;)V

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/applet/AppletBanner;->setCallToActionClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    new-instance v2, Lcom/squareup/applet/BannerVisibility$ShowBanner;

    invoke-virtual {p1, v0}, Lcom/squareup/onboarding/BannerButton;->callToAction(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0}, Lcom/squareup/onboarding/BannerButton;->subtitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v3, p1}, Lcom/squareup/applet/BannerVisibility$ShowBanner;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_0
    sget-object v2, Lcom/squareup/applet/BannerVisibility$HideBanner;->INSTANCE:Lcom/squareup/applet/BannerVisibility$HideBanner;

    .line 65
    :goto_0
    invoke-virtual {v1, v2}, Lcom/squareup/applet/AppletBanner;->setVisibility(Lcom/squareup/applet/BannerVisibility;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$1$HelpAppletMasterViewPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->appletSectionsListPresenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->refresh()V

    return-void
.end method

.method public synthetic lambda$onLoad$0$HelpAppletMasterViewPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->runner:Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;->bannerButtonObservable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$jcJYl1qqEFcBOBfme85TQOTc7PQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$jcJYl1qqEFcBOBfme85TQOTc7PQ;-><init>(Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$2$HelpAppletMasterViewPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->onRefreshed()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$NWyVPJwN0mK6sLfIihG8lWH1nm4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$NWyVPJwN0mK6sLfIihG8lWH1nm4;-><init>(Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;)V

    .line 46
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$updateBanner$3$HelpAppletMasterViewPresenter(Lcom/squareup/onboarding/BannerButton;Landroid/content/res/Resources;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->runner:Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;

    invoke-virtual {p1, p2}, Lcom/squareup/onboarding/BannerButton;->url(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;->onBannerClicked(Ljava/lang/String;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 39
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletMasterViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$OvNkdML7nAEnT2libG8NLHRE4gE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$OvNkdML7nAEnT2libG8NLHRE4gE;-><init>(Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$5-ifkOaNNl3M5hwaPBLFinxQduE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/-$$Lambda$HelpAppletMasterViewPresenter$5-ifkOaNNl3M5hwaPBLFinxQduE;-><init>(Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
