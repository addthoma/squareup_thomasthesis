.class public final Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;
.super Lcom/squareup/referrals/ReferralScreen;
.source "ReferralMasterDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/referrals/ReferralScreen$Component;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static INSTANCE:Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;->INSTANCE:Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;

    .line 29
    sget-object v0, Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;->INSTANCE:Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;

    .line 30
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 18
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScope;->INSTANCE:Lcom/squareup/ui/help/HelpAppletScope;

    sget-object v1, Lcom/squareup/referrals/ReferralScreen$Style;->ACTION_BAR_MAYBE_MASTER_DETAIL:Lcom/squareup/referrals/ReferralScreen$Style;

    invoke-direct {p0, v0, v1}, Lcom/squareup/referrals/ReferralScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/referrals/ReferralScreen$Style;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 22
    const-class v0, Lcom/squareup/ui/help/referrals/ReferralsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 33
    sget v0, Lcom/squareup/referrals/R$layout;->referral_view:I

    return v0
.end method
