.class public final Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;
.super Ljava/lang/Object;
.source "ReferralsVisibility_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hasViewedReferralProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;->hasViewedReferralProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;)",
            "Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/help/referrals/ReferralsVisibility;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/ui/help/referrals/ReferralsVisibility;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/referrals/ReferralsVisibility;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;->hasViewedReferralProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0, v1}, Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility_Factory;->get()Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    move-result-object v0

    return-object v0
.end method
