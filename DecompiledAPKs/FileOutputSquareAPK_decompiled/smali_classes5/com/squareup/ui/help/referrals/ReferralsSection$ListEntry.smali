.class public final Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;
.super Lcom/squareup/ui/help/HelpAppletSectionsListEntry;
.source "ReferralsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/referrals/ReferralsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ListEntry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0016J\n\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;",
        "Lcom/squareup/ui/help/HelpAppletSectionsListEntry;",
        "section",
        "Lcom/squareup/ui/help/referrals/ReferralsSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "referralsVisibility",
        "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
        "(Lcom/squareup/ui/help/referrals/ReferralsSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/referrals/ReferralsVisibility;)V",
        "badgeCount",
        "Lio/reactivex/Observable;",
        "",
        "getValueText",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/referrals/ReferralsSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/referrals/ReferralsVisibility;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "referralsVisibility"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    check-cast p1, Lcom/squareup/applet/AppletSection;

    sget v0, Lcom/squareup/referrals/R$string;->referral_get_free_processing:I

    .line 25
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    iput-object p3, p0, Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    return-void
.end method


# virtual methods
.method public badgeCount()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    invoke-virtual {v0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->badgeCount()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "referralsVisibility.badgeCount()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    invoke-virtual {v0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->hasViewedReferral()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/applet/help/R$string;->new_word:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
