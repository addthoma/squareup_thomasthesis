.class public final Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;
.super Lcom/squareup/ui/help/HelpAppletContent;
.source "CreateItemTutorial.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/Res;)V",
        "shouldDisplay",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/Res;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryAppletGateway"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget v2, Lcom/squareup/applet/help/R$string;->create_item_tutorial:I

    .line 18
    sget v0, Lcom/squareup/applet/help/R$string;->create_item_tutorial_subtext:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    move-object v3, p3

    check-cast v3, Ljava/lang/CharSequence;

    .line 19
    sget-object v5, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_CREATE_ITEM_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x14

    const/4 v8, 0x0

    move-object v1, p0

    .line 16
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/help/HelpAppletContent;-><init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    return-void
.end method


# virtual methods
.method public shouldDisplay()Z
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasOrderEntryApplet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
