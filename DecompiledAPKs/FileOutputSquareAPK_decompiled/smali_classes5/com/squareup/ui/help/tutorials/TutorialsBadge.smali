.class public Lcom/squareup/ui/help/tutorials/TutorialsBadge;
.super Lcom/squareup/settings/IntegerLocalSetting;
.source "TutorialsBadge.java"


# static fields
.field private static final HELP_TUTORIALS_COUNT_KEY:Ljava/lang/String; = "HELP_TUTORIALS_COUNT_KEY"


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "HELP_TUTORIALS_COUNT_KEY"

    .line 15
    invoke-direct {p0, p1, v0}, Lcom/squareup/settings/IntegerLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-void
.end method
