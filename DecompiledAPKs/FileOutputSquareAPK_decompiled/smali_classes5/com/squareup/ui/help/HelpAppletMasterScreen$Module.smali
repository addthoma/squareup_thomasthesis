.class public abstract Lcom/squareup/ui/help/HelpAppletMasterScreen$Module;
.super Ljava/lang/Object;
.source "HelpAppletMasterScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/HelpAppletMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAppletMasterViewPresenter(Lcom/squareup/ui/help/HelpAppletMasterViewPresenter;)Lcom/squareup/applet/AppletMasterViewPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAppletSectionsListPresenter(Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;)Lcom/squareup/applet/AppletSectionsListPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
