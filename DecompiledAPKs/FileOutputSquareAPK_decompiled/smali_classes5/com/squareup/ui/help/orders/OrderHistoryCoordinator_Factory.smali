.class public final Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;
.super Ljava/lang/Object;
.source "OrderHistoryCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final orderHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrderHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final shortDateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final spinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrderHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->orderHistoryProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->spinnerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->shortDateProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrderHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/orders/OrderHistory;Lcom/squareup/register/widgets/GlassSpinner;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Ljava/util/Locale;)Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            "Lcom/squareup/ui/help/orders/OrderHistory;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/util/Locale;",
            ")",
            "Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;"
        }
    .end annotation

    .line 61
    new-instance v7, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/orders/OrderHistory;Lcom/squareup/register/widgets/GlassSpinner;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Ljava/util/Locale;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/help/HelpAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->orderHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/help/orders/OrderHistory;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->spinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->shortDateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Locale;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->newInstance(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/orders/OrderHistory;Lcom/squareup/register/widgets/GlassSpinner;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Ljava/util/Locale;)Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator_Factory;->get()Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    move-result-object v0

    return-object v0
.end method
