.class public Lcom/squareup/ui/help/orders/OrdersScreenData;
.super Ljava/lang/Object;
.source "OrdersScreenData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;
    }
.end annotation


# instance fields
.field public final showOrderHistory:Z

.field public final showOrderLegacyReader:Z

.field public final showOrderReader:Z


# direct methods
.method private constructor <init>(ZZZ)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean p1, p0, Lcom/squareup/ui/help/orders/OrdersScreenData;->showOrderReader:Z

    .line 11
    iput-boolean p2, p0, Lcom/squareup/ui/help/orders/OrdersScreenData;->showOrderLegacyReader:Z

    .line 12
    iput-boolean p3, p0, Lcom/squareup/ui/help/orders/OrdersScreenData;->showOrderHistory:Z

    return-void
.end method

.method synthetic constructor <init>(ZZZLcom/squareup/ui/help/orders/OrdersScreenData$1;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/help/orders/OrdersScreenData;-><init>(ZZZ)V

    return-void
.end method
