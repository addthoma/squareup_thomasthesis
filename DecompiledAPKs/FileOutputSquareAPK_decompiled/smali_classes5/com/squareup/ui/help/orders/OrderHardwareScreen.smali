.class public Lcom/squareup/ui/help/orders/OrderHardwareScreen;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "OrderHardwareScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/help/orders/OrderHardwareScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/orders/OrderHardwareScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/orders/OrderHardwareScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/orders/OrderHardwareScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/help/orders/OrderHardwareScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/orders/OrderHardwareScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/orders/OrderHardwareScreen;->INSTANCE:Lcom/squareup/ui/help/orders/OrderHardwareScreen;

    .line 36
    sget-object v0, Lcom/squareup/ui/help/orders/OrderHardwareScreen;->INSTANCE:Lcom/squareup/ui/help/orders/OrderHardwareScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/orders/OrderHardwareScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_ORDER_READER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 25
    const-class v0, Lcom/squareup/ui/help/orders/OrdersSection;

    return-object v0
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 19
    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/orders/OrderHardwareScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;
    .locals 1

    .line 33
    const-class v0, Lcom/squareup/ui/help/orders/OrderHardwareScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/orders/OrderHardwareScreen$Component;

    invoke-interface {p1}, Lcom/squareup/ui/help/orders/OrderHardwareScreen$Component;->getCoordinator()Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 45
    sget v0, Lcom/squareup/applet/help/R$layout;->order_hardware:I

    return v0
.end method
