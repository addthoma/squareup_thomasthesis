.class public final Lcom/squareup/ui/help/orders/OrderHistory_Factory;
.super Ljava/lang/Object;
.source "OrderHistory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/orders/OrderHistory;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final ordersServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/orders/OrdersService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/orders/OrdersService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistory_Factory;->ordersServiceProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/help/orders/OrderHistory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/orders/OrderHistory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/orders/OrdersService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/ui/help/orders/OrderHistory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/help/orders/OrderHistory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/orders/OrderHistory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/orders/OrdersService;Lio/reactivex/Scheduler;)Lcom/squareup/ui/help/orders/OrderHistory;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/help/orders/OrderHistory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/orders/OrderHistory;-><init>(Lcom/squareup/server/orders/OrdersService;Lio/reactivex/Scheduler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/orders/OrderHistory;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory_Factory;->ordersServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/orders/OrdersService;

    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrderHistory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    invoke-static {v0, v1}, Lcom/squareup/ui/help/orders/OrderHistory_Factory;->newInstance(Lcom/squareup/server/orders/OrdersService;Lio/reactivex/Scheduler;)Lcom/squareup/ui/help/orders/OrderHistory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrderHistory_Factory;->get()Lcom/squareup/ui/help/orders/OrderHistory;

    move-result-object v0

    return-object v0
.end method
