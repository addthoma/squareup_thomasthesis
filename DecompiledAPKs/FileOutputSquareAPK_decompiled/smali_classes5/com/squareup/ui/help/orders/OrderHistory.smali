.class public Lcom/squareup/ui/help/orders/OrderHistory;
.super Ljava/lang/Object;
.source "OrderHistory.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;
    }
.end annotation


# static fields
.field private static final ERROR_SENTINEL:Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;


# instance fields
.field private final mainScheduler:Lio/reactivex/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end field

.field private final onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;",
            ">;"
        }
    .end annotation
.end field

.field private final ordersService:Lcom/squareup/server/orders/OrdersService;

.field private final request:Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse$Builder;-><init>()V

    .line 27
    invoke-virtual {v0}, Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse$Builder;->build()Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/orders/OrderHistory;->ERROR_SENTINEL:Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;

    return-void
.end method

.method constructor <init>(Lcom/squareup/server/orders/OrdersService;Lio/reactivex/Scheduler;)V
    .locals 2
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory;->orderHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 31
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 32
    new-instance v0, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;-><init>()V

    const/16 v1, 0xa

    .line 33
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;->max_orders_to_retrieve(Ljava/lang/Integer;)Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;->build()Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory;->request:Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/help/orders/OrderHistory;->mainScheduler:Lio/reactivex/Scheduler;

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistory;->ordersService:Lcom/squareup/server/orders/OrdersService;

    return-void
.end method

.method static synthetic access$000()Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/ui/help/orders/OrderHistory;->ERROR_SENTINEL:Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;

    return-object v0
.end method

.method static synthetic lambda$null$0(Ljava/lang/Throwable;)Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 46
    sget-object p0, Lcom/squareup/ui/help/orders/OrderHistory;->ERROR_SENTINEL:Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$1$OrderHistory(Lkotlin/Unit;)Lio/reactivex/ObservableSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 44
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistory;->ordersService:Lcom/squareup/server/orders/OrdersService;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory;->request:Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;

    invoke-interface {p1, v0}, Lcom/squareup/server/orders/OrdersService;->getOrders(Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory;->mainScheduler:Lio/reactivex/Scheduler;

    .line 45
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistory$UOPOOmUSafod-VEnX40_DtVwuvQ;->INSTANCE:Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistory$UOPOOmUSafod-VEnX40_DtVwuvQ;

    .line 46
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/help/orders/-$$Lambda$ik_wxPhJ7h0nY0t8hU_30VeHM-g;->INSTANCE:Lcom/squareup/ui/help/orders/-$$Lambda$ik_wxPhJ7h0nY0t8hU_30VeHM-g;

    .line 47
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistory$oDv_8Bsj-CWEY0HODFQpX7KEZ1I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistory$oDv_8Bsj-CWEY0HODFQpX7KEZ1I;-><init>(Lcom/squareup/ui/help/orders/OrderHistory;)V

    .line 44
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrderHistory;->orderHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/help/orders/-$$Lambda$UWhJ85yNQDzFAHypPsefs7y4wFM;

    invoke-direct {v2, v1}, Lcom/squareup/ui/help/orders/-$$Lambda$UWhJ85yNQDzFAHypPsefs7y4wFM;-><init>(Lcom/jakewharton/rxrelay2/BehaviorRelay;)V

    .line 48
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 43
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrderHistory;->reload()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public reload()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method snapshot()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistory;->orderHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method
