.class Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "OrderHistoryCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OrderViewHolder"
.end annotation


# static fields
.field private static final DAYS_OF_TRACKING:I = 0x1f


# instance fields
.field private final count:Landroid/widget/TextView;

.field private final date:Landroid/widget/TextView;

.field private final divider:Landroid/view/View;

.field private final number:Landroid/widget/TextView;

.field private final status:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

.field private final total:Landroid/widget/TextView;

.field private final track:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Landroid/view/View;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    .line 194
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 195
    sget p1, Lcom/squareup/applet/help/R$id;->divider:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->divider:Landroid/view/View;

    .line 196
    sget p1, Lcom/squareup/applet/help/R$id;->order_date:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->date:Landroid/widget/TextView;

    .line 197
    sget p1, Lcom/squareup/applet/help/R$id;->order_count:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->count:Landroid/widget/TextView;

    .line 198
    sget p1, Lcom/squareup/applet/help/R$id;->order_total:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->total:Landroid/widget/TextView;

    .line 199
    sget p1, Lcom/squareup/applet/help/R$id;->order_status:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->status:Landroid/widget/TextView;

    .line 200
    sget p1, Lcom/squareup/applet/help/R$id;->order_number:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->number:Landroid/widget/TextView;

    .line 201
    sget p1, Lcom/squareup/applet/help/R$id;->order_track:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->track:Landroid/widget/TextView;

    return-void
.end method

.method private getStatusColor(Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;)I
    .locals 1

    .line 282
    sget-object v0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$1;->$SwitchMap$com$squareup$protos$client$solidshop$Order$UserFacingStatus:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    .line 303
    sget p1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    return p1

    .line 288
    :pswitch_0
    sget p1, Lcom/squareup/marin/R$color;->marin_green:I

    return p1

    .line 294
    :cond_0
    :pswitch_1
    sget p1, Lcom/squareup/marin/R$color;->marin_red:I

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private getStatusText(Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;)I
    .locals 1

    .line 242
    sget-object v0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$1;->$SwitchMap$com$squareup$protos$client$solidshop$Order$UserFacingStatus:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 277
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_unknown:I

    return p1

    .line 274
    :pswitch_0
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_returned_and_refunded:I

    return p1

    .line 272
    :pswitch_1
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_returned:I

    return p1

    .line 270
    :pswitch_2
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_awaiting_return:I

    return p1

    .line 268
    :pswitch_3
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_shipped_with_errors:I

    return p1

    .line 266
    :pswitch_4
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_partially_delivered_with_errors:I

    return p1

    .line 264
    :pswitch_5
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_shipping_error:I

    return p1

    .line 262
    :pswitch_6
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_returned_to_sender:I

    return p1

    .line 260
    :pswitch_7
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_delivered:I

    return p1

    .line 258
    :pswitch_8
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_partially_delivered:I

    return p1

    .line 256
    :pswitch_9
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_out_for_delivery:I

    return p1

    .line 254
    :pswitch_a
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_shipped:I

    return p1

    .line 252
    :pswitch_b
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_processing:I

    return p1

    .line 250
    :pswitch_c
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_declined_and_refunded:I

    return p1

    .line 248
    :pswitch_d
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_declined_refund_pending:I

    return p1

    .line 246
    :pswitch_e
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_declined:I

    return p1

    .line 244
    :pswitch_f
    sget p1, Lcom/squareup/applet/help/R$string;->order_status_canceled:I

    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getTrackingUrl(Lcom/squareup/protos/client/solidshop/Order;)Ljava/lang/String;
    .locals 1

    .line 308
    iget-object v0, p1, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/solidshop/Carton;

    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/Carton;->tracking_url:Ljava/lang/String;

    :goto_0
    return-object p1
.end method


# virtual methods
.method bind(ZLcom/squareup/protos/client/solidshop/Order;)V
    .locals 7

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->divider:Landroid/view/View;

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const/16 p1, 0x8

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 207
    iget-object p1, p2, Lcom/squareup/protos/client/solidshop/Order;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->access$100(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->date:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    invoke-static {v3}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->access$200(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->count:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 212
    iget-object v3, p2, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "number"

    if-nez v3, :cond_1

    .line 213
    sget v3, Lcom/squareup/applet/help/R$string;->order_items_none:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 214
    :cond_1
    iget-object v3, p2, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_2

    .line 215
    sget v3, Lcom/squareup/applet/help/R$string;->order_items_one:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 217
    :cond_2
    sget v3, Lcom/squareup/applet/help/R$string;->order_items_some:I

    invoke-static {v0, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v5, p2, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    .line 218
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 219
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    .line 221
    :goto_1
    iget-object v5, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->count:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    iget-object v3, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->total:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    invoke-static {v5}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->access$300(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v5

    iget-object v6, p2, Lcom/squareup/protos/client/solidshop/Order;->total:Lcom/squareup/protos/common/Money;

    invoke-interface {v5, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v3, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->status:Landroid/widget/TextView;

    iget-object v5, p2, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    invoke-direct {p0, v5}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->getStatusText(Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 226
    iget-object v3, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->status:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p2, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    invoke-direct {p0, v6}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->getStatusColor(Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;)I

    move-result v6

    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 228
    iget-object v3, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->number:Landroid/widget/TextView;

    sget v5, Lcom/squareup/applet/help/R$string;->order_number:I

    invoke-static {v0, v5}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v5, p2, Lcom/squareup/protos/client/solidshop/Order;->number:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    invoke-direct {p0, p2}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->getTrackingUrl(Lcom/squareup/protos/client/solidshop/Order;)Ljava/lang/String;

    move-result-object p2

    .line 232
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 233
    invoke-static {p1, v0}, Lcom/squareup/util/Times;->countDaysBetween(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v3

    const-wide/16 v5, 0x1f

    cmp-long p1, v3, v5

    if-gez p1, :cond_3

    .line 234
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->track:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 235
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->track:Landroid/widget/TextView;

    new-instance v0, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$OrderViewHolder$5ELSCFzpaIjsmPLsU3pIr81pH-U;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$OrderViewHolder$5ELSCFzpaIjsmPLsU3pIr81pH-U;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 237
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->track:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method public synthetic lambda$bind$0$OrderHistoryCoordinator$OrderViewHolder(Ljava/lang/String;Landroid/view/View;)V
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->access$400(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Lcom/squareup/ui/help/HelpAppletScopeRunner;

    move-result-object v0

    invoke-static {p2}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object p2

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->launchTrack(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method
