.class public final Lcom/squareup/ui/help/HelpAppletMasterScreen;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "HelpAppletMasterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/ui/help/HelpApplet;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/HelpAppletMasterScreen$ParentComponent;,
        Lcom/squareup/ui/help/HelpAppletMasterScreen$Module;,
        Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/HelpAppletMasterScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/HelpAppletMasterScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/help/HelpAppletMasterScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/HelpAppletMasterScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/HelpAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/help/HelpAppletMasterScreen;

    .line 54
    sget-object v0, Lcom/squareup/ui/help/HelpAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/help/HelpAppletMasterScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/HelpAppletMasterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 29
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 25
    sget v0, Lcom/squareup/applet/help/R$layout;->help_applet_master_view:I

    return v0
.end method
