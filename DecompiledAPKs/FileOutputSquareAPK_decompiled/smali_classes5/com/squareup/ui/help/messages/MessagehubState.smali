.class public final Lcom/squareup/ui/help/messages/MessagehubState;
.super Ljava/lang/Object;
.source "MessagehubState.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00110\u00100\u000fH\u0002J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000fH\u0002J\u0014\u0010\u0013\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00140\u00100\u000fH\u0002J\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000cH\u0002J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000fH\u0002J\u000e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000fH\u0002J\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u000fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u0010\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u000c0\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/MessagehubState;",
        "",
        "messagehubService",
        "Lcom/squareup/server/messagehub/MessagehubService;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "pushServiceRegistrationSettingValue",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
        "(Lcom/squareup/server/messagehub/MessagehubService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;)V",
        "credentialStateRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/ui/help/messages/MessagingCredentialsState;",
        "kotlin.jvm.PlatformType",
        "allowConversation",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/messagehub/service/AllowConversationResponse;",
        "fetchCredentialsState",
        "messagingToken",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;",
        "processMessagingEligibilityState",
        "Lcom/squareup/ui/help/messages/MessagingEligibilityState;",
        "allowState",
        "Lcom/squareup/ui/help/messages/ConversationAllowableState;",
        "credentialState",
        "singleAllowConversation",
        "singleMessagingCredentialsState",
        "singleMessagingEligibilityState",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final credentialStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/help/messages/MessagingCredentialsState;",
            ">;"
        }
    .end annotation
.end field

.field private final messagehubService:Lcom/squareup/server/messagehub/MessagehubService;

.field private final pushServiceRegistrationSettingValue:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/server/messagehub/MessagehubService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/messagehub/MessagehubService;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "messagehubService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushServiceRegistrationSettingValue"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/MessagehubState;->messagehubService:Lcom/squareup/server/messagehub/MessagehubService;

    iput-object p2, p0, Lcom/squareup/ui/help/messages/MessagehubState;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/ui/help/messages/MessagehubState;->pushServiceRegistrationSettingValue:Lcom/squareup/settings/LocalSetting;

    .line 27
    sget-object p1, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Invalid;->INSTANCE:Lcom/squareup/ui/help/messages/MessagingCredentialsState$Invalid;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026CredentialsState.Invalid)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/MessagehubState;->credentialStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$processMessagingEligibilityState(Lcom/squareup/ui/help/messages/MessagehubState;Lcom/squareup/ui/help/messages/ConversationAllowableState;Lcom/squareup/ui/help/messages/MessagingCredentialsState;)Lcom/squareup/ui/help/messages/MessagingEligibilityState;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/help/messages/MessagehubState;->processMessagingEligibilityState(Lcom/squareup/ui/help/messages/ConversationAllowableState;Lcom/squareup/ui/help/messages/MessagingCredentialsState;)Lcom/squareup/ui/help/messages/MessagingEligibilityState;

    move-result-object p0

    return-object p0
.end method

.method private final allowConversation()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/messagehub/service/AllowConversationResponse;",
            ">;>;"
        }
    .end annotation

    .line 173
    new-instance v0, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;-><init>()V

    .line 174
    invoke-virtual {v0}, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;->build()Lcom/squareup/protos/messagehub/service/AllowConversationRequest;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/squareup/ui/help/messages/MessagehubState;->messagehubService:Lcom/squareup/server/messagehub/MessagehubService;

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/messagehub/MessagehubService;->allowConversation(Lcom/squareup/protos/messagehub/service/AllowConversationRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 176
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private final fetchCredentialsState()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/help/messages/MessagingCredentialsState;",
            ">;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/help/messages/MessagehubState;->credentialStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/help/messages/MessagehubState;->credentialStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.just(credentialStateRelay.value)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/MessagehubState;->singleMessagingCredentialsState()Lio/reactivex/Single;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final messagingToken()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;",
            ">;>;"
        }
    .end annotation

    .line 159
    new-instance v0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;-><init>()V

    .line 160
    iget-object v1, p0, Lcom/squareup/ui/help/messages/MessagehubState;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "accountStatusSettings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->email_for_hmac(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/squareup/ui/help/messages/MessagehubState;->pushServiceRegistrationSettingValue:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;

    iget-object v1, v1, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->device_push_token(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->build()Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lcom/squareup/ui/help/messages/MessagehubState;->messagehubService:Lcom/squareup/server/messagehub/MessagehubService;

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/messagehub/MessagehubService;->getMessagingToken(Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private final processMessagingEligibilityState(Lcom/squareup/ui/help/messages/ConversationAllowableState;Lcom/squareup/ui/help/messages/MessagingCredentialsState;)Lcom/squareup/ui/help/messages/MessagingEligibilityState;
    .locals 9

    .line 54
    instance-of v0, p1, Lcom/squareup/ui/help/messages/ConversationAllowableState$Allowed;

    if-eqz v0, :cond_1

    .line 57
    instance-of v0, p2, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;

    .line 59
    check-cast p2, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;

    invoke-virtual {p2}, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;->getApiKey()Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-virtual {p2}, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;->getAppId()Ljava/lang/String;

    move-result-object v3

    .line 61
    invoke-virtual {p2}, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;->getDomainName()Ljava/lang/String;

    move-result-object v4

    .line 62
    invoke-virtual {p2}, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;->getHmacToken()Ljava/lang/String;

    move-result-object v5

    .line 63
    invoke-virtual {p2}, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;->getRecentActive()Z

    move-result v6

    .line 64
    invoke-virtual {p2}, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;->getDeviceLookupToken()Ljava/lang/String;

    move-result-object v7

    .line 65
    check-cast p1, Lcom/squareup/ui/help/messages/ConversationAllowableState$Allowed;

    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/ConversationAllowableState$Allowed;->getCustomIssueFields()Ljava/util/List;

    move-result-object v8

    move-object v1, v0

    .line 58
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;)V

    check-cast v0, Lcom/squareup/ui/help/messages/MessagingEligibilityState;

    goto :goto_0

    .line 69
    :cond_0
    sget-object p1, Lcom/squareup/ui/help/messages/MessagingEligibilityState$ServerError;->INSTANCE:Lcom/squareup/ui/help/messages/MessagingEligibilityState$ServerError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/messages/MessagingEligibilityState;

    goto :goto_0

    .line 73
    :cond_1
    instance-of p2, p1, Lcom/squareup/ui/help/messages/ConversationAllowableState$ServerError;

    if-eqz p2, :cond_2

    sget-object p1, Lcom/squareup/ui/help/messages/MessagingEligibilityState$ServerError;->INSTANCE:Lcom/squareup/ui/help/messages/MessagingEligibilityState$ServerError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/messages/MessagingEligibilityState;

    goto :goto_0

    .line 75
    :cond_2
    instance-of p2, p1, Lcom/squareup/ui/help/messages/ConversationAllowableState$Disallowed;

    if-eqz p2, :cond_3

    .line 76
    new-instance p2, Lcom/squareup/ui/help/messages/MessagingEligibilityState$Ineligible;

    .line 77
    check-cast p1, Lcom/squareup/ui/help/messages/ConversationAllowableState$Disallowed;

    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/ConversationAllowableState$Disallowed;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/ConversationAllowableState$Disallowed;->getHelpTipTitle()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/ConversationAllowableState$Disallowed;->getHelpTipSubtitle()Ljava/lang/String;

    move-result-object p1

    .line 76
    invoke-direct {p2, v0, v1, p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$Ineligible;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p2

    check-cast v0, Lcom/squareup/ui/help/messages/MessagingEligibilityState;

    :goto_0
    return-object v0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final singleAllowConversation()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/help/messages/ConversationAllowableState;",
            ">;"
        }
    .end annotation

    .line 133
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/MessagehubState;->allowConversation()Lio/reactivex/Single;

    move-result-object v0

    .line 134
    sget-object v1, Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;->INSTANCE:Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "allowConversation()\n    \u2026ror\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final singleMessagingCredentialsState()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/help/messages/MessagingCredentialsState;",
            ">;"
        }
    .end annotation

    .line 99
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/MessagehubState;->messagingToken()Lio/reactivex/Single;

    move-result-object v0

    .line 100
    sget-object v1, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;->INSTANCE:Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "messagingToken()\n       \u2026lid\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final singleMessagingEligibilityState()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/help/messages/MessagingEligibilityState;",
            ">;"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/MessagehubState;->singleAllowConversation()Lio/reactivex/Single;

    move-result-object v0

    check-cast v0, Lio/reactivex/SingleSource;

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/MessagehubState;->fetchCredentialsState()Lio/reactivex/Single;

    move-result-object v1

    check-cast v1, Lio/reactivex/SingleSource;

    .line 38
    new-instance v2, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingEligibilityState$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingEligibilityState$1;-><init>(Lcom/squareup/ui/help/messages/MessagehubState;)V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 35
    invoke-static {v0, v1, v2}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    const-string/jumbo v1, "zip<ConversationAllowabl\u2026ialState)\n        }\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
