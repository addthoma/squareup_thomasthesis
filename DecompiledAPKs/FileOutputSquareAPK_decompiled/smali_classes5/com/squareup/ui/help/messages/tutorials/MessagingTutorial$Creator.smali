.class public final Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;
.super Lcom/squareup/tutorialv2/TutorialCreator;
.source "MessagingTutorial.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Creator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator$Seed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u000eB\u0015\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00080\rH\u0016R\u001c\u0010\u0006\u001a\u0010\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
        "Lcom/squareup/tutorialv2/TutorialCreator;",
        "tutorialProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;",
        "(Ljavax/inject/Provider;)V",
        "seeds",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "kotlin.jvm.PlatformType",
        "ready",
        "",
        "triggeredTutorial",
        "Lio/reactivex/Observable;",
        "Seed",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final seeds:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "tutorialProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialCreator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;->tutorialProvider:Ljavax/inject/Provider;

    .line 120
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<TutorialSeed>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getTutorialProvider$p(Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)Ljavax/inject/Provider;
    .locals 0

    .line 116
    iget-object p0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;->tutorialProvider:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public final ready()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator$Seed;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator$Seed;-><init>(Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public triggeredTutorial()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
