.class public final Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "HelpMessagesBootstrapScreen.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHelpMessagesBootstrapScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HelpMessagesBootstrapScreen.kt\ncom/squareup/ui/help/messages/HelpMessagesBootstrapScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,24:1\n35#2:25\n*E\n*S KotlinDebug\n*F\n+ 1 HelpMessagesBootstrapScreen.kt\ncom/squareup/ui/help/messages/HelpMessagesBootstrapScreen\n*L\n20#1:25\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\u000c\u001a\n \u000e*\u0004\u0018\u00010\r0\rH\u0016R\u0018\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/help/HelpAppletScope;",
        "kotlin.jvm.PlatformType",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;

.field private static final section:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;->INSTANCE:Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;

    .line 17
    const-class v0, Lcom/squareup/ui/help/help/HelpSection;

    sput-object v0, Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;->section:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    const-class v0, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 20
    check-cast p1, Lcom/squareup/ui/help/HelpAppletScope$Component;

    .line 21
    invoke-interface {p1}, Lcom/squareup/ui/help/HelpAppletScope$Component;->helpAppletScopeRunner()Lcom/squareup/ui/help/HelpAppletScopeRunner;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->launchMessagingActivityOverHelpApplet()V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/help/HelpAppletScope;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScope;->INSTANCE:Lcom/squareup/ui/help/HelpAppletScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;->getParentKey()Lcom/squareup/ui/help/HelpAppletScope;

    move-result-object v0

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 17
    sget-object v0, Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;->section:Ljava/lang/Class;

    return-object v0
.end method
