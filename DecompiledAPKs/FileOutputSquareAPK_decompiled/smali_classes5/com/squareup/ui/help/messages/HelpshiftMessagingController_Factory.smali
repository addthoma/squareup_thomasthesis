.class public final Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;
.super Ljava/lang/Object;
.source "HelpshiftMessagingController_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/messages/HelpshiftMessagingController;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final helpshiftServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/helpshift/api/HelpshiftService;",
            ">;"
        }
    .end annotation
.end field

.field private final messagehubStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagehubState;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private final messagingNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/SupportMessagingNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagehubState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/helpshift/api/HelpshiftService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/SupportMessagingNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->messagehubStateProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->helpshiftServiceProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->messagingNotifierProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->messagesVisibilityProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagehubState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/helpshift/api/HelpshiftService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/SupportMessagingNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/help/messages/MessagehubState;Lcom/squareup/helpshift/api/HelpshiftService;Lcom/squareup/ui/help/SupportMessagingNotifier;Lcom/squareup/ui/help/messages/MessagesVisibility;Landroid/app/Application;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/help/messages/HelpshiftMessagingController;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;-><init>(Lcom/squareup/ui/help/messages/MessagehubState;Lcom/squareup/helpshift/api/HelpshiftService;Lcom/squareup/ui/help/SupportMessagingNotifier;Lcom/squareup/ui/help/messages/MessagesVisibility;Landroid/app/Application;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/messages/HelpshiftMessagingController;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->messagehubStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/help/messages/MessagehubState;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->helpshiftServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/helpshift/api/HelpshiftService;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->messagingNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/help/SupportMessagingNotifier;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->messagesVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/help/messages/MessagesVisibility;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->newInstance(Lcom/squareup/ui/help/messages/MessagehubState;Lcom/squareup/helpshift/api/HelpshiftService;Lcom/squareup/ui/help/SupportMessagingNotifier;Lcom/squareup/ui/help/messages/MessagesVisibility;Landroid/app/Application;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/help/messages/HelpshiftMessagingController;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController_Factory;->get()Lcom/squareup/ui/help/messages/HelpshiftMessagingController;

    move-result-object v0

    return-object v0
.end method
