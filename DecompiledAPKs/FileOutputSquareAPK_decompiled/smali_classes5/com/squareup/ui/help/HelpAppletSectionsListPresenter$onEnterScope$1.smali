.class final Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$onEnterScope$1;
.super Ljava/lang/Object;
.source "HelpAppletSectionsListPresenter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$onEnterScope$1;->this$0:Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/util/DeviceScreenSizeInfo;)V
    .locals 0

    .line 33
    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$onEnterScope$1;->this$0:Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;

    invoke-static {p1}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->access$getMessagingController$p(Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;)Lcom/squareup/ui/help/MessagingController;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/help/MessagingController;->helpshiftConversationActive()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 35
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$onEnterScope$1;->this$0:Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;

    invoke-static {p1}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->access$getMessagingTutorialCreator$p(Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;)Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;->ready()V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/util/DeviceScreenSizeInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$onEnterScope$1;->accept(Lcom/squareup/util/DeviceScreenSizeInfo;)V

    return-void
.end method
