.class public Lcom/squareup/ui/help/legal/PosLegalSection;
.super Lcom/squareup/ui/help/AbstractHelpSection;
.source "PosLegalSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    new-instance v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {v0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    invoke-direct {p0, v0}, Lcom/squareup/ui/help/AbstractHelpSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/help/legal/PosLegalSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/ui/help/legal/PosLegalScreen;->INSTANCE:Lcom/squareup/ui/help/legal/PosLegalScreen;

    return-object v0
.end method

.method public tapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LEGAL:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method
