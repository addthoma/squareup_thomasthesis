.class public final Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;
.super Ljava/lang/Object;
.source "JumbotronServiceKey.kt"

# interfaces
.implements Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;",
        "Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;",
        "()V",
        "inAppMessageKey",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;

    invoke-direct {v0}, Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;->INSTANCE:Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inAppMessageKey()Ljava/lang/String;
    .locals 1

    const-string v0, "Register"

    return-object v0
.end method
