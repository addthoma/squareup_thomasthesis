.class public final Lcom/squareup/ui/help/jumbotron/T2JumbotronServiceKey;
.super Ljava/lang/Object;
.source "JumbotronServiceKey.kt"

# interfaces
.implements Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/help/jumbotron/T2JumbotronServiceKey;",
        "Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "inAppMessageKey",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/jumbotron/T2JumbotronServiceKey;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public inAppMessageKey()Ljava/lang/String;
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/T2JumbotronServiceKey;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->JUMBOTRON_SERVICE_KEY_T2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "T2"

    goto :goto_0

    :cond_0
    const-string v0, "Register"

    :goto_0
    return-object v0
.end method
