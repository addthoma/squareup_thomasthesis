.class public final Lcom/squareup/ui/help/about/LibrariesCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "LibrariesCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibrariesCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibrariesCoordinator.kt\ncom/squareup/ui/help/about/LibrariesCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,74:1\n1288#2:75\n1313#2,3:76\n1316#2,3:86\n347#3,7:79\n*E\n*S KotlinDebug\n*F\n+ 1 LibrariesCoordinator.kt\ncom/squareup/ui/help/about/LibrariesCoordinator\n*L\n41#1:75\n41#1,3:76\n41#1,3:86\n41#1,7:79\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000cH\u0002J\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/LibrariesCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "helpAppletScopeRunner",
        "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
        "(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "librariesView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "attach",
        "",
        "rootView",
        "Landroid/view/View;",
        "bindViews",
        "view",
        "displayLibraries",
        "screenData",
        "Lcom/squareup/ui/help/about/LibrariesScreenData;",
        "getActionBarConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config;",
        "res",
        "Landroid/content/res/Resources;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private librariesView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "helpAppletScopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-void
.end method

.method public static final synthetic access$displayLibraries(Lcom/squareup/ui/help/about/LibrariesCoordinator;Lcom/squareup/ui/help/about/LibrariesScreenData;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/about/LibrariesCoordinator;->displayLibraries(Lcom/squareup/ui/help/about/LibrariesScreenData;)V

    return-void
.end method

.method public static final synthetic access$getHelpAppletScopeRunner$p(Lcom/squareup/ui/help/about/LibrariesCoordinator;)Lcom/squareup/ui/help/HelpAppletScopeRunner;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 4

    .line 68
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 69
    sget v0, Lcom/squareup/applet/help/R$id;->libraries:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator;->librariesView:Landroidx/recyclerview/widget/RecyclerView;

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator;->librariesView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_0

    const-string v1, "librariesView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    return-void
.end method

.method private final displayLibraries(Lcom/squareup/ui/help/about/LibrariesScreenData;)V
    .locals 6

    .line 40
    iget-object v0, p1, Lcom/squareup/ui/help/about/LibrariesScreenData;->libraries:Ljava/util/List;

    const-string v1, "screenData.libraries"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 75
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 76
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 77
    move-object v3, v2

    check-cast v3, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 41
    iget-object v3, v3, Lcom/squareup/ui/help/about/AndroidLibrary;->license:Lcom/squareup/ui/help/about/License;

    .line 79
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 78
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 82
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 86
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 42
    :cond_1
    invoke-static {v1}, Lkotlin/collections/MapsKt;->toList(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 44
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    invoke-static {v1}, Lkotlin/text/StringsKt;->getCASE_INSENSITIVE_ORDER(Lkotlin/jvm/internal/StringCompanionObject;)Ljava/util/Comparator;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$$inlined$compareBy$1;

    invoke-direct {v2, v1}, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$$inlined$compareBy$1;-><init>(Ljava/util/Comparator;)V

    check-cast v2, Ljava/util/Comparator;

    .line 43
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator;->librariesView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v1, :cond_2

    const-string v2, "librariesView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v2, Lcom/squareup/ui/help/about/LibrariesListAdapter;

    .line 49
    iget-boolean v3, p1, Lcom/squareup/ui/help/about/LibrariesScreenData;->canFollowLinks:Z

    .line 50
    new-instance v4, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$1;

    invoke-direct {v4, p0, p1}, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$1;-><init>(Lcom/squareup/ui/help/about/LibrariesCoordinator;Lcom/squareup/ui/help/about/LibrariesScreenData;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 55
    new-instance v5, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$2;

    invoke-direct {v5, p0, p1}, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$2;-><init>(Lcom/squareup/ui/help/about/LibrariesCoordinator;Lcom/squareup/ui/help/about/LibrariesScreenData;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 47
    invoke-direct {v2, v0, v3, v4, v5}, Lcom/squareup/ui/help/about/LibrariesListAdapter;-><init>(Ljava/util/List;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method private final getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    sget v1, Lcom/squareup/applet/help/R$string;->libraries:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    const-string v0, "helpAppletScopeRunner.ge\u2026.string.libraries), true)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/about/LibrariesCoordinator;->bindViews(Landroid/view/View;)V

    .line 26
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v1, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const-string v2, "res"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/help/about/LibrariesCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 30
    new-instance v0, Lcom/squareup/ui/help/about/LibrariesCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/about/LibrariesCoordinator$attach$1;-><init>(Lcom/squareup/ui/help/about/LibrariesCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
