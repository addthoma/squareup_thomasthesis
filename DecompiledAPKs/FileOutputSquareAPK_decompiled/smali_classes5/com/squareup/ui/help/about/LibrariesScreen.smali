.class public final Lcom/squareup/ui/help/about/LibrariesScreen;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "LibrariesScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/about/LibrariesScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/about/LibrariesScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/ui/help/about/LibrariesScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/about/LibrariesScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/about/LibrariesScreen;->INSTANCE:Lcom/squareup/ui/help/about/LibrariesScreen;

    .line 42
    sget-object v0, Lcom/squareup/ui/help/about/LibrariesScreen;->INSTANCE:Lcom/squareup/ui/help/about/LibrariesScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/about/LibrariesScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_LIBRARIES:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 27
    const-class v0, Lcom/squareup/ui/help/about/AboutSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 39
    const-class v0, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/help/HelpAppletScope$Component;->librariesCoordinator()Lcom/squareup/ui/help/about/LibrariesCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/applet/help/R$layout;->libraries:I

    return v0
.end method
