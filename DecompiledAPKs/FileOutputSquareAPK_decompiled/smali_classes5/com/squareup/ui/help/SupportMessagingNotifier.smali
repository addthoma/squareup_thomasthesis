.class public interface abstract Lcom/squareup/ui/help/SupportMessagingNotifier;
.super Ljava/lang/Object;
.source "SupportMessagingNotifier.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/SupportMessagingNotifier$NoSupportMessagingNotifier;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0004J\u0008\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/help/SupportMessagingNotifier;",
        "Lmortar/Scoped;",
        "removeNotification",
        "",
        "NoSupportMessagingNotifier",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract removeNotification()V
.end method
