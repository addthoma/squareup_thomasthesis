.class public Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;
.super Lcom/squareup/settings/StringLocalSetting;
.source "HelpAppletSelectedSectionSetting.java"


# static fields
.field private static final LAST_HELP_SECTION_NAME_KEY:Ljava/lang/String; = "last-help-applet-section-name"


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "last-help-applet-section-name"

    .line 12
    invoke-direct {p0, p1, v0}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-void
.end method
