.class public final Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "JediWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediWorkflowRunner.kt\ncom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$Companion\n*L\n1#1,63:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "getNAME",
        "()Ljava/lang/String;",
        "startNewWorkflow",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "startArgs",
        "Lcom/squareup/jedi/JediWorkflowProps;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNAME()Ljava/lang/String;
    .locals 1

    .line 52
    invoke-static {}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;->access$getNAME$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/jedi/JediWorkflowProps;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startArgs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p1

    .line 56
    check-cast p1, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;

    .line 57
    invoke-static {p1, p2}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;->access$setProps$p(Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;Lcom/squareup/jedi/JediWorkflowProps;)V

    .line 58
    invoke-static {p1}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;->access$ensureWorkflow(Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;)V

    return-void
.end method
