.class public final Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "HelpJediWorkflowScope.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHelpJediWorkflowScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HelpJediWorkflowScope.kt\ncom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,29:1\n35#2:30\n*E\n*S KotlinDebug\n*F\n+ 1 HelpJediWorkflowScope.kt\ncom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope\n*L\n16#1:30\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;",
        "Lcom/squareup/ui/help/InHelpAppletScope;",
        "Lcom/squareup/container/MaybePersistent;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "ParentComponent",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    new-instance v0, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;

    invoke-direct {v0}, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;->INSTANCE:Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;

    .line 27
    sget-object v0, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;->INSTANCE:Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(HelpJediWorkflowScope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    const-class v0, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope$ParentComponent;

    .line 16
    invoke-interface {v0}, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope$ParentComponent;->workflowRunner()Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;

    move-result-object v0

    .line 18
    invoke-super {p0, p1}, Lcom/squareup/ui/help/InHelpAppletScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p1

    const-string v1, "it"

    .line 19
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string v0, "super.buildScope(parentS\u2026er.registerServices(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    .line 11
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
