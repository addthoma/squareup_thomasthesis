.class final Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;
.super Ljava/lang/Object;
.source "EditCustomerBootstrapScreen.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->doRegister(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
        "result",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;->this$0:Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/crm/edit/EditCustomerOutput;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;
    .locals 4

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    instance-of v0, p1, Lcom/squareup/ui/crm/edit/EditCustomerOutput$EditCustomerCanceled;

    if-eqz v0, :cond_0

    .line 24
    new-instance p1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;->this$0:Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;

    invoke-static {v0}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->access$getProps$p(Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;)Lcom/squareup/ui/crm/edit/EditCustomerProps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;->this$0:Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;

    invoke-static {v2}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->access$getProps$p(Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;)Lcom/squareup/ui/crm/edit/EditCustomerProps;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getUpdateCustomerResultKey()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1$1;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;-><init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 25
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/crm/edit/EditCustomerOutput$EditCustomerSaved;

    if-eqz v0, :cond_1

    .line 26
    new-instance v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;->this$0:Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;

    invoke-static {v1}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->access$getProps$p(Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;)Lcom/squareup/ui/crm/edit/EditCustomerProps;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    move-result-object v1

    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerOutput$EditCustomerSaved;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerOutput$EditCustomerSaved;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;->this$0:Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;

    invoke-static {v2}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->access$getProps$p(Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;)Lcom/squareup/ui/crm/edit/EditCustomerProps;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getUpdateCustomerResultKey()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1$2;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;-><init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lkotlin/jvm/functions/Function1;)V

    move-object p1, v0

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;->apply(Lcom/squareup/ui/crm/edit/EditCustomerOutput;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;

    move-result-object p1

    return-object p1
.end method
