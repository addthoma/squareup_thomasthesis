.class public final Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;
.super Ljava/lang/Object;
.source "CustomersDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomersHistoryFactory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomersDeepLinkHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomersDeepLinkHandler.kt\ncom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,78:1\n1587#2,3:79\n*E\n*S KotlinDebug\n*F\n+ 1 CustomersDeepLinkHandler.kt\ncom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory\n*L\n56#1,3:79\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u001a\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000bH\u0016J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0016J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "screens",
        "",
        "Lcom/squareup/container/LayoutScreen;",
        "(Ljava/util/List;)V",
        "getScreens",
        "()Ljava/util/List;",
        "component1",
        "copy",
        "createHistory",
        "Lflow/History;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "currentHistory",
        "equals",
        "",
        "other",
        "",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "hashCode",
        "",
        "toString",
        "",
        "crm-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/container/LayoutScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/LayoutScreen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->copy(Ljava/util/List;)Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/container/LayoutScreen;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/util/List;)Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/LayoutScreen;",
            ">;)",
            "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    const-string v0, "home"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 56
    invoke-static {p1, p2}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p1

    .line 80
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/LayoutScreen;

    .line 56
    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    const-string v0, "builder.push(screen)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    const-string p2, "screens\n        .fold(ho\u2026creen) }\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 59
    sget-object v0, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final getScreens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/container/LayoutScreen;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomersHistoryFactory(screens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;->screens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
