.class public interface abstract Lcom/squareup/ui/crm/applet/CreateFilterScope$Component;
.super Ljava/lang/Object;
.source "CreateFilterScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/applet/CreateFilterScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/CreateFilterScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract chooseFiltersCardScreen()Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Component;
.end method

.method public abstract editFilterCardScreen()Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;
.end method

.method public abstract filterTemplateLoader()Lcom/squareup/crm/FilterTemplateLoader;
.end method
