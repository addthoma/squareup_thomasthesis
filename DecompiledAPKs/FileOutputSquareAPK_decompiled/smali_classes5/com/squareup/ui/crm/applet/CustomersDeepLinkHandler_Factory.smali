.class public final Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;
.super Ljava/lang/Object;
.source "CustomersDeepLinkHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final customerConversationTokenHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final customersAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;->customersAppletProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;->customerConversationTokenHolderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;)",
            "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;)Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;-><init>(Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;->customersAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/applet/CustomersApplet;

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;->customerConversationTokenHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;->newInstance(Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;)Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler_Factory;->get()Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method
