.class public interface abstract Lcom/squareup/ui/crm/applet/CustomersAppletScope$Component;
.super Ljava/lang/Object;
.source "CustomersAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/CrmScope$ParentCustomersAppletComponent;
.implements Lcom/squareup/ui/crm/flow/UpdateCustomerScope$ParentComponent;
.implements Lcom/squareup/ui/crm/edit/EditCustomerScope$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/crm/MergeProposalLoader$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/applet/CustomersAppletScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/CustomersAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract allCustomersMasterScreen()Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen$Component;
.end method

.method public abstract chooseFiltersScope()Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;
.end method

.method public abstract conversationDetail()Lcom/squareup/ui/crm/applet/ConversationDetailScope$Component;
.end method

.method public abstract createManualGroupScreen()Lcom/squareup/ui/crm/v2/CreateManualGroupScreen$Component;
.end method

.method public abstract createMessageListScreenV2()Lcom/squareup/ui/crm/v2/MessageListScreenV2$Component;
.end method

.method public abstract mergeCustomersFlow()Lcom/squareup/ui/crm/flow/MergeCustomersFlow;
.end method

.method public abstract mergeProposalLoader()Lcom/squareup/crm/MergeProposalLoader;
.end method

.method public abstract mergingDuplicatesScreen()Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Component;
.end method

.method public abstract noCustomerSelectedDetailScreen()Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen$Component;
.end method

.method public abstract resolveDuplicatesFlow()Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow$Component;
.end method

.method public abstract resolveDuplicatesScreen()Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;
.end method

.method public abstract selectCustomersScope()Lcom/squareup/ui/crm/applet/SelectCustomersScope$Component;
.end method

.method public abstract updateGroup2Scope()Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;
.end method

.method public abstract viewGroupMasterScreen()Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen$Component;
.end method

.method public abstract viewGroupsListScreen()Lcom/squareup/ui/crm/v2/ViewGroupsListScreen$Component;
.end method
