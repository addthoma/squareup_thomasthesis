.class public interface abstract Lcom/squareup/ui/crm/applet/CreateGroupScope$Component;
.super Ljava/lang/Object;
.source "CreateGroupScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/applet/CreateGroupScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/CreateGroupScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract createGroupScreen()Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;
.end method
