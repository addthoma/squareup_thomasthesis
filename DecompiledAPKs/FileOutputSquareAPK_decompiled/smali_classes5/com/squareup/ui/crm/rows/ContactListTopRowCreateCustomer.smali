.class public Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;
.super Landroid/widget/LinearLayout;
.source "ContactListTopRowCreateCustomer.java"


# instance fields
.field private createNewButton:Lcom/squareup/marketfont/MarketButton;

.field private final onCreateNewButtonClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->onCreateNewButtonClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->onCreateNewButtonClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method


# virtual methods
.method public onCreateNewButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->onCreateNewButtonClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 34
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 36
    sget v0, Lcom/squareup/crm/R$id;->crm_create_new_customer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->createNewButton:Lcom/squareup/marketfont/MarketButton;

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->createNewButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer$1;-><init>(Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setCreateNewButtonLabel(Ljava/lang/String;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->createNewButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
