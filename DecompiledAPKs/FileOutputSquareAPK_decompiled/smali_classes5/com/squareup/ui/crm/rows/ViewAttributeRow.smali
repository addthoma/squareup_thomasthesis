.class public Lcom/squareup/ui/crm/rows/ViewAttributeRow;
.super Landroid/widget/LinearLayout;
.source "ViewAttributeRow.java"


# instance fields
.field private final titleText:Landroid/widget/TextView;

.field private final valueText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 16
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 17
    sget v0, Lcom/squareup/crm/R$layout;->crm_view_customer_attribute_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/crm/rows/ViewAttributeRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 19
    sget p1, Lcom/squareup/crm/R$id;->crm_view_attribute_title:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ViewAttributeRow;->titleText:Landroid/widget/TextView;

    .line 20
    sget p1, Lcom/squareup/crm/R$id;->crm_view_attribute_value:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ViewAttributeRow;->valueText:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public showTitle(Ljava/lang/String;)V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ViewAttributeRow;->titleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showValue(Ljava/lang/String;)V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ViewAttributeRow;->valueText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
