.class public final Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;
.super Ljava/lang/Object;
.source "CardOnFileRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/rows/CardOnFileRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0016\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B9\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u000bH\u00c6\u0003JG\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u00082\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020\u0003H\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0013\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;",
        "",
        "cardIcon",
        "",
        "cardNameAndNumber",
        "",
        "cardStatus",
        "statusRed",
        "",
        "clickable",
        "onClickedEmittable",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZLcom/squareup/protos/client/instruments/InstrumentSummary;)V",
        "getCardIcon",
        "()I",
        "getCardNameAndNumber",
        "()Ljava/lang/CharSequence;",
        "getCardStatus",
        "getClickable",
        "()Z",
        "getOnClickedEmittable",
        "()Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "getStatusRed",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardIcon:I

.field private final cardNameAndNumber:Ljava/lang/CharSequence;

.field private final cardStatus:Ljava/lang/CharSequence;

.field private final clickable:Z

.field private final onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

.field private final statusRed:Z


# direct methods
.method public constructor <init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZLcom/squareup/protos/client/instruments/InstrumentSummary;)V
    .locals 1

    const-string v0, "cardNameAndNumber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickedEmittable"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardIcon:I

    iput-object p2, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardNameAndNumber:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardStatus:Ljava/lang/CharSequence;

    iput-boolean p4, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->statusRed:Z

    iput-boolean p5, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->clickable:Z

    iput-object p6, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZLcom/squareup/protos/client/instruments/InstrumentSummary;ILjava/lang/Object;)Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget p1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardIcon:I

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardNameAndNumber:Ljava/lang/CharSequence;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardStatus:Ljava/lang/CharSequence;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->statusRed:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->clickable:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move p3, p1

    move-object p4, p8

    move-object p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->copy(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZLcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardIcon:I

    return v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardNameAndNumber:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardStatus:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->statusRed:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->clickable:Z

    return v0
.end method

.method public final component6()Lcom/squareup/protos/client/instruments/InstrumentSummary;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-object v0
.end method

.method public final copy(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZLcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;
    .locals 8

    const-string v0, "cardNameAndNumber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickedEmittable"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;

    move-object v1, v0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZLcom/squareup/protos/client/instruments/InstrumentSummary;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;

    iget v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardIcon:I

    iget v1, p1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardIcon:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardNameAndNumber:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardNameAndNumber:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardStatus:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardStatus:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->statusRed:Z

    iget-boolean v1, p1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->statusRed:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->clickable:Z

    iget-boolean v1, p1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->clickable:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iget-object p1, p1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCardIcon()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardIcon:I

    return v0
.end method

.method public final getCardNameAndNumber()Ljava/lang/CharSequence;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardNameAndNumber:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getCardStatus()Ljava/lang/CharSequence;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardStatus:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getClickable()Z
    .locals 1

    .line 71
    iget-boolean v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->clickable:Z

    return v0
.end method

.method public final getOnClickedEmittable()Lcom/squareup/protos/client/instruments/InstrumentSummary;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-object v0
.end method

.method public final getStatusRed()Z
    .locals 1

    .line 70
    iget-boolean v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->statusRed:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardIcon:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardNameAndNumber:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardStatus:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->statusRed:Z

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->clickable:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewData(cardIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardIcon:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", cardNameAndNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardNameAndNumber:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", cardStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->cardStatus:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", statusRed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->statusRed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", clickable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->clickable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onClickedEmittable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->onClickedEmittable:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
