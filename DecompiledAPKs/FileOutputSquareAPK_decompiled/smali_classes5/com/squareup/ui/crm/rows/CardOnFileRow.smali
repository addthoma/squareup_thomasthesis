.class public final Lcom/squareup/ui/crm/rows/CardOnFileRow;
.super Landroid/widget/RelativeLayout;
.source "CardOnFileRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardOnFileRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardOnFileRow.kt\ncom/squareup/ui/crm/rows/CardOnFileRow\n*L\n1#1,75:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0010J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000b0\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/crm/rows/CardOnFileRow;",
        "Landroid/widget/RelativeLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "glyphView",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "nameView",
        "Landroid/widget/TextView;",
        "onUnlinkClicked",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "kotlin.jvm.PlatformType",
        "statusView",
        "unlinkButton",
        "Landroid/view/View;",
        "Lio/reactivex/Observable;",
        "setViewData",
        "",
        "data",
        "Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;",
        "ViewData",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private final nameView:Landroid/widget/TextView;

.field private final onUnlinkClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final statusView:Landroid/widget/TextView;

.field private final unlinkButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 27
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create<InstrumentSummary>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->onUnlinkClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 30
    sget v0, Lcom/squareup/crm/R$layout;->crm_v2_cardonfile_card_row:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 32
    move-object p1, p0

    check-cast p1, Landroid/view/View;

    sget v0, Lcom/squareup/crm/R$id;->crm_cardonfile_card_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->nameView:Landroid/widget/TextView;

    .line 33
    sget v0, Lcom/squareup/crm/R$id;->crm_cardonfile_card_status:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->statusView:Landroid/widget/TextView;

    .line 34
    sget v0, Lcom/squareup/crm/R$id;->crm_cardonfile_card_glyph:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 35
    sget v0, Lcom/squareup/crm/R$id;->crm_cardonfile_unlink_card_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->unlinkButton:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$getOnUnlinkClicked$p(Lcom/squareup/ui/crm/rows/CardOnFileRow;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->onUnlinkClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method


# virtual methods
.method public final onUnlinkClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->onUnlinkClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final setViewData(Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;)V
    .locals 5

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/CardOnFileRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->getCardIcon()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->nameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->getCardNameAndNumber()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->getCardStatus()Ljava/lang/CharSequence;

    move-result-object v0

    const/16 v2, 0x8

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->getCardStatus()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->statusView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->statusView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->getCardStatus()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->getStatusRed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->statusView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/CardOnFileRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 51
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->statusView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/CardOnFileRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 44
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->statusView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 54
    :goto_2
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;->getClickable()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->unlinkButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->unlinkButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/crm/rows/CardOnFileRow$setViewData$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow$setViewData$1;-><init>(Lcom/squareup/ui/crm/rows/CardOnFileRow;Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 58
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/CardOnFileRow;->unlinkButton:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    return-void
.end method
