.class public Lcom/squareup/ui/crm/ChooseCustomerScope$Module;
.super Ljava/lang/Object;
.source "ChooseCustomerScope.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/crm/ChooseCustomerScope$Module$WithBinds;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/ChooseCustomerScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/ChooseCustomerScope$Module$WithBinds;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method recentRolodexContactLoader(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/crm/RolodexContactLoader;
    .locals 1
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 102
    new-instance v0, Lcom/squareup/crm/RolodexContactLoader;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/crm/RolodexContactLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V

    return-object v0
.end method

.method rolodexContactLoader(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/crm/RolodexContactLoader;
    .locals 1
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/crm/RolodexContactLoader;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/crm/RolodexContactLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V

    return-object v0
.end method
