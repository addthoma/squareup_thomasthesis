.class public Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;
.super Ljava/lang/Object;
.source "RightPaneDataRenderer.java"


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public createRightPaneData(IZIZIZ)Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;
    .locals 1

    if-eqz p2, :cond_0

    .line 22
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-eqz p6, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    .line 28
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    .line 30
    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_0
    const-string p3, ""

    if-nez p1, :cond_3

    .line 35
    new-instance p1, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/crm/applet/R$string;->crm_rightpane_default_many:I

    invoke-interface {p2, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p3, p2}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 37
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p5

    const/4 v0, -0x1

    if-ne p5, v0, :cond_4

    .line 39
    new-instance p1, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;

    invoke-direct {p1, p3, p3}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 41
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p3

    if-nez p3, :cond_8

    if-nez p2, :cond_6

    if-nez p4, :cond_6

    if-eqz p6, :cond_5

    goto :goto_1

    .line 50
    :cond_5
    new-instance p1, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/crm/applet/R$string;->crm_empty_directory_title:I

    .line 51
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/crm/applet/R$string;->crm_empty_directory_subtitle:I

    .line 52
    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 43
    :cond_6
    :goto_1
    new-instance p3, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;

    .line 44
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p4, p0, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;->res:Lcom/squareup/util/Res;

    if-eqz p2, :cond_7

    sget p2, Lcom/squareup/crm/applet/R$string;->crm_rightpane_multiselect_zero:I

    goto :goto_2

    :cond_7
    sget p2, Lcom/squareup/crm/applet/R$string;->crm_rightpane_default_zero_noresults:I

    .line 45
    :goto_2
    invoke-interface {p4, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p3

    .line 54
    :cond_8
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p3

    const/4 p4, 0x1

    if-ne p3, p4, :cond_a

    .line 56
    new-instance p3, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;

    .line 57
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p4, p0, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;->res:Lcom/squareup/util/Res;

    if-eqz p2, :cond_9

    sget p2, Lcom/squareup/crm/applet/R$string;->crm_rightpane_multiselect_one:I

    goto :goto_3

    :cond_9
    sget p2, Lcom/squareup/crm/applet/R$string;->crm_rightpane_default_one:I

    .line 58
    :goto_3
    invoke-interface {p4, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p3

    .line 62
    :cond_a
    new-instance p3, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;

    .line 63
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p4, p0, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;->res:Lcom/squareup/util/Res;

    if-eqz p2, :cond_b

    sget p2, Lcom/squareup/crm/applet/R$string;->crm_rightpane_multiselect_many:I

    goto :goto_4

    :cond_b
    sget p2, Lcom/squareup/crm/applet/R$string;->crm_rightpane_default_many:I

    .line 64
    :goto_4
    invoke-interface {p4, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p3
.end method
