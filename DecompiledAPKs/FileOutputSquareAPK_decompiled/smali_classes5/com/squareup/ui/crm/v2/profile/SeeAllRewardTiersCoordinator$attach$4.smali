.class final Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;
.super Ljava/lang/Object;
.source "SeeAllRewardTiersCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
        "points",
        "",
        "call",
        "(Ljava/lang/Integer;)Ljava/util/List;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;->call(Ljava/lang/Integer;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Integer;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->access$getRewardRecyclerViewHelper$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {v1, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->access$getSpendablePoints(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;I)I

    move-result p1

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;->getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;

    move-result-object v1

    .line 67
    new-instance v2, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4$1;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 70
    new-instance v3, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4$2;

    invoke-direct {v3, p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4$2;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 64
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->createRowData(ILcom/squareup/checkout/HoldsCoupons;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
