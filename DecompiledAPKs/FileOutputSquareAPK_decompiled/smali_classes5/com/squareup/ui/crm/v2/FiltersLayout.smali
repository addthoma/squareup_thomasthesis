.class public Lcom/squareup/ui/crm/v2/FiltersLayout;
.super Landroid/widget/HorizontalScrollView;
.source "FiltersLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/FiltersLayout$Component;
    }
.end annotation


# instance fields
.field private deleteClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private filterClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private filterContainer:Landroid/widget/LinearLayout;

.field filterHelper:Lcom/squareup/crm/filters/FilterHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->deleteClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 30
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->filterClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 34
    const-class p2, Lcom/squareup/ui/crm/v2/FiltersLayout$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/FiltersLayout$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/v2/FiltersLayout$Component;->inject(Lcom/squareup/ui/crm/v2/FiltersLayout;)V

    .line 36
    sget p2, Lcom/squareup/crm/applet/R$layout;->crm_v2_filter_layout:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/FiltersLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    sget p1, Lcom/squareup/crm/applet/R$id;->crm_filter_layout_linearlayout:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->filterContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/crm/v2/FilterBubbleView;Lcom/squareup/protos/client/rolodex/Filter;Ljava/lang/String;)V
    .locals 0

    .line 53
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/crm/v2/FilterBubbleView;->setFilter(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$setFilters$1$FiltersLayout(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/ui/crm/v2/FilterBubbleView;)Lrx/Subscription;
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->filterHelper:Lcom/squareup/crm/filters/FilterHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/filters/FilterHelper;->nameAndValueSingeLine(Lcom/squareup/protos/client/rolodex/Filter;)Lrx/Observable;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$FiltersLayout$ACSREEmYrutveM3VU-mNggLnI08;

    invoke-direct {v1, p2, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$FiltersLayout$ACSREEmYrutveM3VU-mNggLnI08;-><init>(Lcom/squareup/ui/crm/v2/FilterBubbleView;Lcom/squareup/protos/client/rolodex/Filter;)V

    .line 53
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public onDeleteClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->deleteClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onFilterClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->filterClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public setFilters(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->filterContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 44
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter;

    .line 45
    new-instance v1, Lcom/squareup/ui/crm/v2/FilterBubbleView;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/FiltersLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/FilterBubbleView;-><init>(Landroid/content/Context;)V

    .line 48
    iget-object v2, v0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/crm/v2/FilterBubbleView;->setFilter(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Filter;)V

    .line 50
    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$FiltersLayout$kbfvSw2pTrltm6I-oLOWXjhdeYk;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$FiltersLayout$kbfvSw2pTrltm6I-oLOWXjhdeYk;-><init>(Lcom/squareup/ui/crm/v2/FiltersLayout;Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/ui/crm/v2/FilterBubbleView;)V

    invoke-static {v1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 55
    invoke-virtual {v1}, Lcom/squareup/ui/crm/v2/FilterBubbleView;->onFilterClicked()Lrx/Observable;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->filterClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 56
    invoke-virtual {v1}, Lcom/squareup/ui/crm/v2/FilterBubbleView;->onDeleteClicked()Lrx/Observable;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->deleteClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->filterContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method
