.class public Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ViewGroupsListCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private groupsListAdapter:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

.field private groupsListRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private progressBar:Landroid/widget/ProgressBar;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;

.field private warningMessage:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 107
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_groups_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 108
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_progress:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->progressBar:Landroid/widget/ProgressBar;

    .line 109
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_warning_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private groupsListToItemList(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;",
            ">;"
        }
    .end annotation

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 186
    invoke-static {p1}, Lcom/squareup/util/SquareCollections;->isNullOrEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    new-array v2, v1, [Lcom/squareup/protos/client/rolodex/GroupType;

    .line 187
    sget-object v3, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p1, v2}, Lcom/squareup/crm/util/RolodexGroupHelper;->filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;

    move-result-object v2

    new-array v3, v1, [Lcom/squareup/protos/client/rolodex/GroupType;

    .line 188
    sget-object v5, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    aput-object v5, v3, v4

    invoke-static {p1, v3}, Lcom/squareup/crm/util/RolodexGroupHelper;->filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;

    move-result-object p1

    .line 191
    new-instance v3, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget-object v5, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/crm/applet/R$string;->crm_groups_manual_groups_uppercase:I

    .line 192
    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;-><init>(ILjava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V

    .line 191
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 194
    new-instance v2, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/crm/applet/R$string;->crm_create_first_manual_group:I

    .line 195
    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3, v6}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;-><init>(ILjava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V

    .line 194
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 197
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Group;

    .line 198
    new-instance v5, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget-object v7, v3, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-direct {v5, v1, v7, v3}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;-><init>(ILjava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 203
    :cond_1
    :goto_1
    new-instance v2, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/crm/applet/R$string;->crm_groups_smart_groups_uppercase:I

    .line 204
    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v4, v3, v6}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;-><init>(ILjava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V

    .line 203
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Group;

    .line 206
    new-instance v3, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget-object v4, v2, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-direct {v3, v1, v4, v2}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;-><init>(ILjava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    return-object v0
.end method

.method public static synthetic lambda$Q0QpYIb1baHouCKYGbUhUK43aTU(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;Ljava/util/List;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListToItemList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$oEzKQaAt3GKBfuPAS6J9snL-K5M(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->showProgress(Z)V

    return-void
.end method

.method static synthetic lambda$setupActionBar$5(Landroid/widget/ImageView;Landroid/content/Context;)Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method private setupActionBar(Landroid/view/View;)V
    .locals 7

    .line 114
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 116
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 117
    sget v2, Lcom/squareup/crm/R$drawable;->crm_action_overflow:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 118
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 120
    sget v2, Lcom/squareup/crm/applet/R$id;->crm_drop_down_container:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/DropDownContainer;

    .line 122
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/crm/applet/R$string;->crm_groups_title:I

    .line 123
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 124
    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    iget-object v5, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$mTmkwkXBmi-SFRKIVN6BvOrM3BE;

    invoke-direct {v6, v5}, Lcom/squareup/ui/crm/v2/-$$Lambda$mTmkwkXBmi-SFRKIVN6BvOrM3BE;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;)V

    .line 125
    invoke-virtual {v3, v6}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    new-instance v5, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$ptUhOx7hkdcn22J8gLPyJvaLlAI;

    invoke-direct {v5, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$ptUhOx7hkdcn22J8gLPyJvaLlAI;-><init>(Landroid/widget/ImageView;)V

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/crmviewcustomer/R$string;->open_menu:I

    .line 127
    invoke-interface {v1, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 126
    invoke-virtual {v3, v5, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/v2/-$$Lambda$N2lr1VXOkwWrM3-jBtlpC2fmDlA;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$N2lr1VXOkwWrM3-jBtlpC2fmDlA;-><init>(Lcom/squareup/ui/DropDownContainer;)V

    .line 128
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 122
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 137
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_action_create_manual_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 138
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 139
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$MRBqvPLW7SjpIBaNUwumlEmlW20;

    invoke-direct {v1, p0, v0, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$MRBqvPLW7SjpIBaNUwumlEmlW20;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;Landroid/view/View;Lcom/squareup/ui/DropDownContainer;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private showMessage(ZLjava/lang/String;)V
    .locals 2

    const/16 v0, 0x8

    if-eqz p1, :cond_0

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    const/high16 v1, 0x10e0000

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 175
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 177
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private showProgress(Z)V
    .locals 3

    const/16 v0, 0x8

    if-eqz p1, :cond_0

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->progressBar:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    const/high16 v2, 0x10e0000

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 167
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 69
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->bindViews(Landroid/view/View;)V

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->setupActionBar(Landroid/view/View;)V

    .line 73
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;-><init>(Ljava/util/List;Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListAdapter:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListAdapter:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 77
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$nUl6Bd18muTPPJdlZZ_AelLvPMc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$nUl6Bd18muTPPJdlZZ_AelLvPMc;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 87
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$i_LzkNIlyDKI-DI4G7Ie3NHUq5M;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$i_LzkNIlyDKI-DI4G7Ie3NHUq5M;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 93
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$5-S4-qmgTzQ9YjEAxzabqdp6ihA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$5-S4-qmgTzQ9YjEAxzabqdp6ihA;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 0

    .line 103
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method

.method gotoCreateManualGroupScreen(Z)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_CREATE_NEW:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;->gotoCreateManualGroupScreen(Z)V

    return-void
.end method

.method public synthetic lambda$attach$1$ViewGroupsListCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 79
    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->success()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$Q0QpYIb1baHouCKYGbUhUK43aTU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$Q0QpYIb1baHouCKYGbUhUK43aTU;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V

    .line 80
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$VtAixlptRUu3kNyByEVFWJju-1o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$VtAixlptRUu3kNyByEVFWJju-1o;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V

    .line 81
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$2$ViewGroupsListCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 89
    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->progress()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$oEzKQaAt3GKBfuPAS6J9snL-K5M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$oEzKQaAt3GKBfuPAS6J9snL-K5M;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V

    .line 90
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$4$ViewGroupsListCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 95
    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->failure()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$UljsRyZ9ARJ9H7qNlpcYLOyn7Jo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$UljsRyZ9ARJ9H7qNlpcYLOyn7Jo;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$ViewGroupsListCoordinator(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->groupsListAdapter:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->updateItems(Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$null$3$ViewGroupsListCoordinator(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 97
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 98
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmupdatecustomer/R$string;->crm_group_loading_error:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 97
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->showMessage(ZLjava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$6$ViewGroupsListCoordinator(Lcom/squareup/ui/DropDownContainer;Lkotlin/Unit;)V
    .locals 0

    .line 141
    invoke-virtual {p1}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    const/4 p1, 0x0

    .line 142
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->gotoCreateManualGroupScreen(Z)V

    return-void
.end method

.method public synthetic lambda$setupActionBar$7$ViewGroupsListCoordinator(Landroid/view/View;Lcom/squareup/ui/DropDownContainer;)Lrx/Subscription;
    .locals 1

    .line 140
    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$qL74QAecDFu6eGzToeVLPiK_JMY;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupsListCoordinator$qL74QAecDFu6eGzToeVLPiK_JMY;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;Lcom/squareup/ui/DropDownContainer;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method showGroup(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 2

    .line 152
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-ne v0, v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_MANUAL_GROUP:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_0

    .line 154
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/AudienceType;->GROUP_V2_SMART:Lcom/squareup/protos/client/rolodex/AudienceType;

    if-ne v0, v1, :cond_1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_SMART_GROUP:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_0

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_AUTO_SMART_GROUP:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 159
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;->showGroup(Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method
