.class public final Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;
.super Ljava/lang/Object;
.source "ManageCouponsAndRewardsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation


# instance fields
.field public final coupons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;->coupons:Ljava/util/List;

    return-void
.end method
