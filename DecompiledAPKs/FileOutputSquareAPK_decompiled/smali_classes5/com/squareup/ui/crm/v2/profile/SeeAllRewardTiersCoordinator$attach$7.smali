.class final Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;
.super Ljava/lang/Object;
.source "SeeAllRewardTiersCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "points",
        "",
        "call",
        "(Ljava/lang/Integer;)Lrx/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;->call(Ljava/lang/Integer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Integer;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;>;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->access$getRewardAdapterHelper$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {v1, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->access$getSpendablePoints(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const-string v1, "Observable.just(getSpendablePoints(points!!))"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;->getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;

    move-result-object v1

    .line 87
    new-instance v2, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7$1;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 84
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->createRewardWrapperList(Lrx/Observable;Lcom/squareup/checkout/HoldsCoupons;Lkotlin/jvm/functions/Function1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
