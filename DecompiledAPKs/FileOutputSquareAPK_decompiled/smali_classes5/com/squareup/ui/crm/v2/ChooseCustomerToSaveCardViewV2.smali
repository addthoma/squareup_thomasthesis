.class public Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;
.super Landroid/widget/LinearLayout;
.source "ChooseCustomerToSaveCardViewV2.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private bottomButtonRow:Landroid/view/View;

.field private contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

.field private createNew:Landroid/widget/Button;

.field presenter:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchBox:Lcom/squareup/ui/XableEditText;

.field private searchMessage:Lcom/squareup/widgets/MessageView;

.field private searchProgressBar:Landroid/widget/ProgressBar;

.field private final searchTermChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchTermChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 51
    const-class p2, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Component;->inject(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->shortAnimTimeMs:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchTermChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)Lcom/squareup/ui/XableEditText;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 151
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 152
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_search_box:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    .line 153
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_contact_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    .line 154
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_search_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    .line 155
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_search_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    .line 156
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_bottom_button_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->bottomButtonRow:Landroid/view/View;

    .line 157
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_savecard_new_customer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->createNew:Landroid/widget/Button;

    return-void
.end method


# virtual methods
.method contactList()Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    return-object v0
.end method

.method public synthetic lambda$showTopButtonRow$0$ChooseCustomerToSaveCardViewV2(Landroid/view/View;)V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->presenter:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;

    check-cast p1, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->bindTopButtonRow(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 89
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->presenter:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->presenter:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->dropView(Ljava/lang/Object;)V

    .line 95
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->bindViews()V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2$1;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2$2;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->createNew:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2$3;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/crm/R$string;->crm_search_customers_hint:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method searchTermChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchTermChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setSearchBox(Ljava/lang/String;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showBottomButtonRow(Z)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->bottomButtonRow:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showContactList(Z)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showCreateNewButton(Z)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->createNew:Landroid/widget/Button;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showMessage(ZLjava/lang/String;)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 114
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method showTopButtonRow(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 133
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    sget v0, Lcom/squareup/crm/R$layout;->crm_v2_contact_list_top_row_create_customer:I

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveCardViewV2$lFdumf7h23FiksoyErwoMIbmz1I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveCardViewV2$lFdumf7h23FiksoyErwoMIbmz1I;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->showTopRow(ILrx/functions/Action1;)V

    goto :goto_0

    .line 138
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->hideTopRow()V

    :goto_0
    return-void
.end method
