.class public final Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;
.super Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;
.source "LoyaltySectionViewData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00052\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\t\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;",
        "loyaltyStatusResponse",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "writeEnabled",
        "",
        "canRedeemPoints",
        "(Lcom/squareup/loyalty/LoyaltyStatusResponse;ZZ)V",
        "getCanRedeemPoints",
        "()Z",
        "getLoyaltyStatusResponse",
        "()Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "getWriteEnabled",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "crm-view-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canRedeemPoints:Z

.field private final loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

.field private final writeEnabled:Z


# direct methods
.method public constructor <init>(Lcom/squareup/loyalty/LoyaltyStatusResponse;ZZ)V
    .locals 1

    const-string v0, "loyaltyStatusResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    iput-boolean p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->writeEnabled:Z

    iput-boolean p3, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->canRedeemPoints:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;Lcom/squareup/loyalty/LoyaltyStatusResponse;ZZILjava/lang/Object;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->writeEnabled:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->canRedeemPoints:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->copy(Lcom/squareup/loyalty/LoyaltyStatusResponse;ZZ)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/loyalty/LoyaltyStatusResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->writeEnabled:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->canRedeemPoints:Z

    return v0
.end method

.method public final copy(Lcom/squareup/loyalty/LoyaltyStatusResponse;ZZ)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;
    .locals 1

    const-string v0, "loyaltyStatusResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;-><init>(Lcom/squareup/loyalty/LoyaltyStatusResponse;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->writeEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->writeEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->canRedeemPoints:Z

    iget-boolean p1, p1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->canRedeemPoints:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanRedeemPoints()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->canRedeemPoints:Z

    return v0
.end method

.method public final getLoyaltyStatusResponse()Lcom/squareup/loyalty/LoyaltyStatusResponse;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    return-object v0
.end method

.method public final getWriteEnabled()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->writeEnabled:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->writeEnabled:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->canRedeemPoints:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewData(loyaltyStatusResponse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->loyaltyStatusResponse:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", writeEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->writeEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", canRedeemPoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->canRedeemPoints:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
