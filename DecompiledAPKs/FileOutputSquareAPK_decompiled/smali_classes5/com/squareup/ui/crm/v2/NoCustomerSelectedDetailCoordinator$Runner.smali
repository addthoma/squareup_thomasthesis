.class public interface abstract Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;
.super Ljava/lang/Object;
.source "NoCustomerSelectedDetailCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract noCustomerSelectedScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onNoCustomerHardwareBackPressed()V
.end method
