.class Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;
.super Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;
.source "ViewGroupsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GroupViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;->this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    .line 58
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_groups_list_row:I

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;ILandroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public bind(I)V
    .locals 3

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/crm/applet/R$id;->crm_group_row_name:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 63
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;->this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->access$000(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget-object v1, v1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;->group:Lcom/squareup/protos/client/rolodex/Group;

    .line 65
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;->this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    invoke-static {v2}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->access$000(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 67
    new-instance p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;

    invoke-direct {p1, p0, v1}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;Lcom/squareup/protos/client/rolodex/Group;)V

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
