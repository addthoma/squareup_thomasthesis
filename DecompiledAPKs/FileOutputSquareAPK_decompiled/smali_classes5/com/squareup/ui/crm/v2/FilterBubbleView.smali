.class public Lcom/squareup/ui/crm/v2/FilterBubbleView;
.super Landroid/widget/LinearLayout;
.source "FilterBubbleView.java"


# instance fields
.field private deleteClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private deleteFilter:Lcom/squareup/glyph/SquareGlyphView;

.field private filter:Lcom/squareup/protos/client/rolodex/Filter;

.field private filterClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private filterName:Landroid/widget/TextView;

.field private filterNameContainer:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 26
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_filter_bubble:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/crm/v2/FilterBubbleView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 30
    sget p1, Lcom/squareup/crm/applet/R$id;->crm_filter_bubble_name:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filterName:Landroid/widget/TextView;

    .line 31
    sget p1, Lcom/squareup/crm/applet/R$id;->crm_filter_bubble_name_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filterNameContainer:Landroid/view/View;

    .line 32
    sget p1, Lcom/squareup/crm/applet/R$id;->crm_filter_bubble_x:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->deleteFilter:Lcom/squareup/glyph/SquareGlyphView;

    .line 33
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filterNameContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filterClicked:Lrx/Observable;

    .line 34
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->deleteFilter:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->deleteClicked:Lrx/Observable;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onDeleteClicked$0$FilterBubbleView(Lkotlin/Unit;)Lcom/squareup/protos/client/rolodex/Filter;
    .locals 0

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-object p1
.end method

.method public synthetic lambda$onFilterClicked$1$FilterBubbleView(Lkotlin/Unit;)Lcom/squareup/protos/client/rolodex/Filter;
    .locals 0

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-object p1
.end method

.method public onDeleteClicked()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->deleteClicked:Lrx/Observable;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$FilterBubbleView$fHqtRegJ5RPJTBbdnmqN8cxfDig;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$FilterBubbleView$fHqtRegJ5RPJTBbdnmqN8cxfDig;-><init>(Lcom/squareup/ui/crm/v2/FilterBubbleView;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onFilterClicked()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filterClicked:Lrx/Observable;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$FilterBubbleView$ojPm4NZo52lYBz78E9dt0WBd7z0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$FilterBubbleView$ojPm4NZo52lYBz78E9dt0WBd7z0;-><init>(Lcom/squareup/ui/crm/v2/FilterBubbleView;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public setFilter(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filterName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/FilterBubbleView;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-void
.end method
