.class public interface abstract Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;
.super Ljava/lang/Object;
.source "AllCouponsAndRewardsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeAllCouponsAndRewardsScreen()V
.end method

.method public abstract getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;
.end method

.method public abstract loyaltyStatus()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showManageCouponsAndRewardsScreen()V
.end method
