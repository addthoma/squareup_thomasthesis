.class public Lcom/squareup/ui/crm/v2/profile/ContactEditView;
.super Landroid/widget/LinearLayout;
.source "ContactEditView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;,
        Lcom/squareup/ui/crm/v2/profile/ContactEditView$Component;,
        Lcom/squareup/ui/crm/v2/profile/ContactEditView$SharedScope;
    }
.end annotation


# instance fields
.field private addFromAddressBook:Lcom/squareup/marketfont/MarketButton;

.field private addressField:Lcom/squareup/address/AddressLayout;

.field private attributeRows:Landroid/widget/LinearLayout;

.field private birthdayField:Lcom/squareup/marketfont/MarketTextView;

.field private final birthdayPopup:Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

.field private final company:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private companyField:Lcom/squareup/ui/XableEditText;

.field private final email:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

.field private emailTextWatcher:Landroid/text/TextWatcher;

.field private final firstName:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private firstNameField:Lcom/squareup/ui/XableEditText;

.field private groupsField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private final lastName:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private lastNameField:Lcom/squareup/ui/XableEditText;

.field private final note:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private noteField:Lcom/squareup/marketfont/MarketEditText;

.field private onAddFromAddressBookClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onAttributeChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;",
            ">;"
        }
    .end annotation
.end field

.field private final onDateAttributeClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;",
            ">;"
        }
    .end annotation
.end field

.field private final onEnumAttributeClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/rows/EnumAttribute;",
            ">;"
        }
    .end annotation
.end field

.field private onGroupsClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final phone:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private phoneField:Lcom/squareup/ui/XableEditText;

.field phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private phoneTextWatcher:Landroid/text/TextWatcher;

.field presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final referenceId:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private referenceIdField:Lcom/squareup/ui/XableEditText;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 125
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstName:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 97
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lastName:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 98
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->email:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 99
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phone:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 100
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->company:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 101
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->referenceId:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 102
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->note:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 114
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAttributeChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 115
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onEnumAttributeClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 116
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onDateAttributeClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 126
    const-class p2, Lcom/squareup/ui/crm/v2/profile/ContactEditView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/profile/ContactEditView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$Component;->inject(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    .line 127
    sget p2, Lcom/squareup/crmupdatecustomer/R$layout;->crm_v2_contact_edit_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p2, 0x1

    .line 128
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setOrientation(I)V

    .line 130
    new-instance p2, Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

    invoke-direct {p2, p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayPopup:Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->note:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method private addObservableToEditText(Lcom/squareup/ui/BaseXableEditText;Lcom/jakewharton/rxrelay/PublishRelay;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/BaseXableEditText;",
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 483
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$3;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$3;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/jakewharton/rxrelay/PublishRelay;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/BaseXableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 468
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_add_from_address_book:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addFromAddressBook:Lcom/squareup/marketfont/MarketButton;

    .line 469
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_first_name_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstNameField:Lcom/squareup/ui/XableEditText;

    .line 470
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_last_name_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lastNameField:Lcom/squareup/ui/XableEditText;

    .line 471
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_email_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableAutoCompleteEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    .line 472
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_phone_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneField:Lcom/squareup/ui/XableEditText;

    .line 473
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_company_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->companyField:Lcom/squareup/ui/XableEditText;

    .line 474
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_reference_id_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->referenceIdField:Lcom/squareup/ui/XableEditText;

    .line 475
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_groups:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->groupsField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 476
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_address:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addressField:Lcom/squareup/address/AddressLayout;

    .line 477
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_note_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->noteField:Lcom/squareup/marketfont/MarketEditText;

    .line 478
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_birthday_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayField:Lcom/squareup/marketfont/MarketTextView;

    .line 479
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_edit_attributes:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic lambda$null$13(Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;Lkotlin/Unit;)Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$null$7(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;ILkotlin/Unit;)Lcom/squareup/ui/crm/rows/EnumAttribute;
    .locals 0

    if-eqz p0, :cond_0

    .line 378
    iget-object p3, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p3, p3, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    if-eqz p3, :cond_0

    new-instance p3, Ljava/util/ArrayList;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    invoke-direct {p3, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_0
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 382
    :goto_0
    new-instance p0, Lcom/squareup/ui/crm/rows/EnumAttribute;

    invoke-direct {p0, p1, p3, p2}, Lcom/squareup/ui/crm/rows/EnumAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Ljava/util/ArrayList;I)V

    return-object p0
.end method

.method static synthetic lambda$onFinishInflate$0(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .line 167
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result p1

    if-nez p1, :cond_0

    .line 168
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 169
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method addAttributeRow(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)V
    .locals 8

    .line 336
    sget-object v0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$4;->$SwitchMap$com$squareup$protos$client$rolodex$AttributeSchema$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    sget-object v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 426
    sget p3, Lcom/squareup/crm/R$layout;->crm_v2_edit_attribute_row_unknown:I

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    .line 427
    invoke-static {p3, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/crm/rows/UnknownAttributeRow;

    .line 429
    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/crm/rows/UnknownAttributeRow;->showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 430
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 411
    :pswitch_0
    sget v0, Lcom/squareup/crm/R$layout;->crm_v2_edit_attribute_row_date:I

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    .line 412
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;

    .line 413
    new-instance v1, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)V

    if-eqz p2, :cond_0

    .line 414
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    .line 415
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->getFormattedDateAttribute(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 414
    :goto_0
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->showAttribute(Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;Ljava/lang/String;)V

    .line 417
    new-instance p1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ARPgpFcgbyVfKIHqkXX-WzLUpzQ;

    invoke-direct {p1, p0, v0, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ARPgpFcgbyVfKIHqkXX-WzLUpzQ;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/ui/crm/rows/EditDateAttributeRow;Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 421
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 400
    :pswitch_1
    sget p3, Lcom/squareup/crm/R$layout;->crm_v2_edit_attribute_row_email:I

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    .line 401
    invoke-static {p3, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;

    .line 402
    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 403
    new-instance p2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$RR8k0-o-h8mlgYXbKtS4UdKdzv0;

    invoke-direct {p2, p0, p3, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$RR8k0-o-h8mlgYXbKtS4UdKdzv0;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)V

    invoke-static {p3, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 407
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 389
    :pswitch_2
    sget p3, Lcom/squareup/crm/R$layout;->crm_v2_edit_attribute_row_phone:I

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    .line 390
    invoke-static {p3, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;

    .line 391
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    invoke-virtual {p3, p1, p2, v0}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/text/InsertingScrubber;)V

    .line 392
    new-instance p2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$b292tI-LkuA12Jy3LPZV3QVTfVE;

    invoke-direct {p2, p0, p3, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$b292tI-LkuA12Jy3LPZV3QVTfVE;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)V

    invoke-static {p3, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 396
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 372
    :pswitch_3
    sget v0, Lcom/squareup/crm/R$layout;->crm_v2_edit_attribute_row_enum:I

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    .line 373
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;

    .line 374
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 375
    new-instance v7, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;

    move-object v1, v7

    move-object v2, p0

    move-object v3, v0

    move-object v4, p2

    move-object v5, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;I)V

    invoke-static {v0, v7}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 385
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 361
    :pswitch_4
    sget p3, Lcom/squareup/crm/R$layout;->crm_v2_edit_attribute_row_text:I

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    .line 362
    invoke-static {p3, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/crm/rows/EditTextAttributeRow;

    .line 363
    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/crm/rows/EditTextAttributeRow;->showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 364
    new-instance p2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$dWw7YTGU3uS1mYqwAbbY1d629V4;

    invoke-direct {p2, p0, p3, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$dWw7YTGU3uS1mYqwAbbY1d629V4;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/ui/crm/rows/EditTextAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)V

    invoke-static {p3, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 368
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 350
    :pswitch_5
    sget p3, Lcom/squareup/crm/R$layout;->crm_v2_edit_attribute_row_boolean:I

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    .line 351
    invoke-static {p3, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;

    .line 353
    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 354
    new-instance p2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;)V

    invoke-virtual {p3, p2}, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 357
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 338
    :pswitch_6
    sget p3, Lcom/squareup/crm/R$layout;->crm_v2_edit_attribute_row_number:I

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    .line 339
    invoke-static {p3, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;

    .line 341
    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 342
    new-instance p2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$kZAILFou7g7HxQdtM1DyAW0uHrM;

    invoke-direct {p2, p0, p3, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$kZAILFou7g7HxQdtM1DyAW0uHrM;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)V

    invoke-static {p3, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 346
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method address()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addressField:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->address()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method clearAttributeRows()V
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method company()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->company:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public contact()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method email()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->email:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method firstName()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstName:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method getAttributes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ">;"
        }
    .end annotation

    .line 441
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 442
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 443
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/rows/HasAttribute;

    invoke-interface {v2}, Lcom/squareup/ui/crm/rows/HasAttribute;->getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 445
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public synthetic lambda$addAttributeRow$10$ContactEditView(Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lrx/Subscription;
    .locals 2

    .line 393
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->onPhoneChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$KpwxnKMxZWRhtEUVWJ26Py7WJdI;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$KpwxnKMxZWRhtEUVWJ26Py7WJdI;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;)V

    .line 394
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$addAttributeRow$12$ContactEditView(Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lrx/Subscription;
    .locals 2

    .line 404
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->onEmailChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$5jCX5j7fOjPl_2WtD3e5Dte5Nc0;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$5jCX5j7fOjPl_2WtD3e5Dte5Nc0;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;)V

    .line 405
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$addAttributeRow$14$ContactEditView(Lcom/squareup/ui/crm/rows/EditDateAttributeRow;Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)Lrx/Subscription;
    .locals 1

    .line 418
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->onClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$_gF_OWwX23jObeEWGSdE1q68zgI;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$_gF_OWwX23jObeEWGSdE1q68zgI;-><init>(Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)V

    .line 419
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onDateAttributeClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 420
    invoke-virtual {p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$addAttributeRow$3$ContactEditView(Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lrx/Subscription;
    .locals 2

    .line 343
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->onTextChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ubmTUm2wBehGF1kMq3RJAhSiMiI;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ubmTUm2wBehGF1kMq3RJAhSiMiI;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;)V

    .line 344
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$addAttributeRow$4$ContactEditView(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 355
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAttributeChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance p4, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    .line 356
    invoke-virtual {p2}, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p2

    invoke-direct {p4, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 355
    invoke-virtual {p3, p4}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$addAttributeRow$6$ContactEditView(Lcom/squareup/ui/crm/rows/EditTextAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lrx/Subscription;
    .locals 2

    .line 365
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/EditTextAttributeRow;->onTextChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$OiRSPQ26kyYGPSu6bk9wV9MUBac;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$OiRSPQ26kyYGPSu6bk9wV9MUBac;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditTextAttributeRow;)V

    .line 366
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$addAttributeRow$8$ContactEditView(Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;I)Lrx/Subscription;
    .locals 1

    .line 376
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->onClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$6XJZM_bo9M_w3b6vgLCfugCT0Kg;

    invoke-direct {v0, p2, p3, p4}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$6XJZM_bo9M_w3b6vgLCfugCT0Kg;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;I)V

    .line 377
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onEnumAttributeClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 384
    invoke-virtual {p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$11$ContactEditView(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;Lkotlin/Unit;)V
    .locals 1

    .line 405
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAttributeChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    .line 406
    invoke-virtual {p2}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 405
    invoke-virtual {p3, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$2$ContactEditView(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;Lkotlin/Unit;)V
    .locals 1

    .line 344
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAttributeChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    .line 345
    invoke-virtual {p2}, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 344
    invoke-virtual {p3, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$5$ContactEditView(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditTextAttributeRow;Lkotlin/Unit;)V
    .locals 1

    .line 366
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAttributeChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    .line 367
    invoke-virtual {p2}, Lcom/squareup/ui/crm/rows/EditTextAttributeRow;->getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 366
    invoke-virtual {p3, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$9$ContactEditView(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;Lkotlin/Unit;)V
    .locals 1

    .line 394
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAttributeChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    .line 395
    invoke-virtual {p2}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 394
    invoke-virtual {p3, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$setInitialFocus$1$ContactEditView()V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method lastName()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lastName:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method note()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->note:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onAddFromAddressBookClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAddFromAddressBookClicked:Lrx/Observable;

    return-object v0
.end method

.method onAttributeChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;",
            ">;"
        }
    .end annotation

    .line 456
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAttributeChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onDateAttributeClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;",
            ">;"
        }
    .end annotation

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onDateAttributeClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->birthdayPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayPopup:Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->dropView(Ljava/lang/Object;)V

    .line 188
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onEnumAttributeClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/rows/EnumAttribute;",
            ">;"
        }
    .end annotation

    .line 460
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onEnumAttributeClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 134
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 135
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->bindViews()V

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addressField:Lcom/squareup/address/AddressLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/address/AddressLayout;->showManualCityEntry(Z)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/text/EmailScrubber;->watcher(Lcom/squareup/text/HasSelectableText;)Landroid/text/TextWatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailTextWatcher:Landroid/text/TextWatcher;

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    new-instance v1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableAutoCompleteEditText;->setAutoCompleteAdapter(Landroid/widget/ArrayAdapter;)V

    .line 143
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneField:Lcom/squareup/ui/XableEditText;

    invoke-direct {v0, v1, v2}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneTextWatcher:Landroid/text/TextWatcher;

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneField:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstNameField:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstName:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addObservableToEditText(Lcom/squareup/ui/BaseXableEditText;Lcom/jakewharton/rxrelay/PublishRelay;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lastNameField:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lastName:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addObservableToEditText(Lcom/squareup/ui/BaseXableEditText;Lcom/jakewharton/rxrelay/PublishRelay;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->email:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addObservableToEditText(Lcom/squareup/ui/BaseXableEditText;Lcom/jakewharton/rxrelay/PublishRelay;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneField:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phone:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addObservableToEditText(Lcom/squareup/ui/BaseXableEditText;Lcom/jakewharton/rxrelay/PublishRelay;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->companyField:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->company:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addObservableToEditText(Lcom/squareup/ui/BaseXableEditText;Lcom/jakewharton/rxrelay/PublishRelay;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->referenceIdField:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->referenceId:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addObservableToEditText(Lcom/squareup/ui/BaseXableEditText;Lcom/jakewharton/rxrelay/PublishRelay;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->noteField:Lcom/squareup/marketfont/MarketEditText;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/ContactEditView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$1;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayField:Lcom/squareup/marketfont/MarketTextView;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/ContactEditView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView$2;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayField:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$xW-CiBcH3hx5_ileKuadsO13kFU;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$xW-CiBcH3hx5_ileKuadsO13kFU;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->groupsField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onGroupsClicked:Lrx/Observable;

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addFromAddressBook:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAddFromAddressBookClicked:Lrx/Observable;

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->takeView(Ljava/lang/Object;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->birthdayPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayPopup:Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onGroupsClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onGroupsClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 221
    instance-of v0, p1, Lcom/squareup/mortar/BundleSavedState;

    if-eqz v0, :cond_1

    .line 222
    check-cast p1, Lcom/squareup/mortar/BundleSavedState;

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayPopup:Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayPopup:Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

    iget-object v1, p1, Lcom/squareup/mortar/BundleSavedState;->bundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 228
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mortar/BundleSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 230
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 212
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayPopup:Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    new-instance v1, Lcom/squareup/mortar/BundleSavedState;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayPopup:Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/squareup/mortar/BundleSavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    return-object v0
.end method

.method phone()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phone:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method referenceId()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->referenceId:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method setAddress(Lcom/squareup/address/Address;)V
    .locals 2

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addressField:Lcom/squareup/address/AddressLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;Z)V

    return-void
.end method

.method public setAnalyticsPathType(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;)V
    .locals 1

    .line 491
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->setAnalyticsPathType(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;)V

    return-void
.end method

.method setCompany(Ljava/lang/String;)V
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->companyField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->presenter:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->setContact(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method setEmail(Ljava/lang/String;)V
    .locals 2

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableAutoCompleteEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableAutoCompleteEditText;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lastNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->emailField:Lcom/squareup/ui/XableAutoCompleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableAutoCompleteEditText;->setEnabled(Z)V

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->companyField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->referenceIdField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->groupsField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addressField:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0, p1}, Lcom/squareup/address/AddressLayout;->setEnabled(Z)V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->noteField:Lcom/squareup/marketfont/MarketEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketEditText;->setEnabled(Z)V

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    return-void
.end method

.method setFirstName(Ljava/lang/String;)V
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setGroups(Ljava/lang/String;)V
    .locals 1

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->groupsField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setInitialFocus()V
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstNameField:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$OryCzoryEsRnxaQQGjU7eR3L24E;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$OryCzoryEsRnxaQQGjU7eR3L24E;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setLastName(Ljava/lang/String;)V
    .locals 1

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lastNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setNote(Ljava/lang/String;)V
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->noteField:Lcom/squareup/marketfont/MarketEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setPhone(Ljava/lang/String;)V
    .locals 1

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phoneField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setReferenceId(Ljava/lang/String;)V
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->referenceIdField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showAddFromAddressBookButton()V
    .locals 2

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addFromAddressBook:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method showAttributeRows()V
    .locals 2

    .line 452
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->attributeRows:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method showBirthday(Ljava/lang/String;)V
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->birthdayField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
