.class public final Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;
.super Ljava/lang/Object;
.source "MessageListScreenV2_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final conversationLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/ConversationLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/ConversationLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->conversationLoaderProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->timeFormatterProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->dateFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/ConversationLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;"
        }
    .end annotation

    .line 57
    new-instance v8, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/ConversationLoader;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;Ljava/util/Locale;Ljava/text/DateFormat;Ljava/text/DateFormat;)Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;
    .locals 9

    .line 63
    new-instance v8, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/ConversationLoader;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;Ljava/util/Locale;Ljava/text/DateFormat;Ljava/text/DateFormat;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;
    .locals 8

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->conversationLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/crm/ConversationLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->timeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->dateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/text/DateFormat;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/ConversationLoader;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;Ljava/util/Locale;Ljava/text/DateFormat;Ljava/text/DateFormat;)Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2_Presenter_Factory;->get()Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    move-result-object v0

    return-object v0
.end method
