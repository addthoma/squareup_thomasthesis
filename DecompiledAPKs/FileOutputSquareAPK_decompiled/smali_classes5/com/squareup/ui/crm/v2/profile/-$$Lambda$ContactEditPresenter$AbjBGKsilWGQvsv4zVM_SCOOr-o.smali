.class public final synthetic Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

.field private final synthetic f$1:Lrx/Observable;

.field private final synthetic f$2:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;

.field private final synthetic f$3:Z

.field private final synthetic f$4:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$1:Lrx/Observable;

    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$2:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;

    iput-boolean p4, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$3:Z

    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$4:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$1:Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$2:Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;

    iget-boolean v3, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$3:Z

    iget-object v4, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;->f$4:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->lambda$subscribeFieldChangeToContact$11$ContactEditPresenter(Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method
