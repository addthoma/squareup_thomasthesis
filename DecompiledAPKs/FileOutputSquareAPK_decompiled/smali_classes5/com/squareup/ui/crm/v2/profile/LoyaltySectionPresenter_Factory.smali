.class public final Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;
.super Ljava/lang/Object;
.source "LoyaltySectionPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsTermsFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rewardAdapterHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final rewardRecyclerViewHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->rewardAdapterHelperProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p10, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->rewardRecyclerViewHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;"
        }
    .end annotation

    .line 79
    new-instance v11, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;
    .locals 12

    .line 87
    new-instance v11, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;-><init>(Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;
    .locals 11

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/loyalty/PointsTermsFormatter;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->rewardAdapterHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->rewardRecyclerViewHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->newInstance(Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter_Factory;->get()Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    move-result-object v0

    return-object v0
.end method
