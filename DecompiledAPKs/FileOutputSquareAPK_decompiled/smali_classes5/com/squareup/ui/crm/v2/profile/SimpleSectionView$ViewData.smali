.class public Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;
.super Ljava/lang/Object;
.source "SimpleSectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewData"
.end annotation


# instance fields
.field final bottomButtonText:Ljava/lang/String;

.field final headerActionButtonState:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

.field final headerActionName:Ljava/lang/String;

.field final headerTitle:Ljava/lang/String;

.field final lineData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->headerTitle:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->headerActionName:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->headerActionButtonState:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->lineData:Ljava/util/List;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->bottomButtonText:Ljava/lang/String;

    return-void
.end method
