.class public Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "LoyaltySectionBottomDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;,
        Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Component;,
        Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 161
    sget-object v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$LoyaltySectionBottomDialogScreen$f4dXEQLf_WXzoM9UUzBmSoQ3sTk;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$LoyaltySectionBottomDialogScreen$f4dXEQLf_WXzoM9UUzBmSoQ3sTk;

    .line 162
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;
    .locals 1

    .line 163
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 164
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 157
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
