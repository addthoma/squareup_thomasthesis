.class public final Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;
.super Ljava/lang/Object;
.source "ChooseCustomer2ViewV2_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumberHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->phoneNumberHelperProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;",
            ">;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectDevice(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/util/Device;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectLocale(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/util/Locale;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->locale:Ljava/util/Locale;

    return-void
.end method

.method public static injectPhoneNumberHelper(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/text/PhoneNumberHelper;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/lang/Object;)V
    .locals 0

    .line 59
    check-cast p1, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->presenter:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/util/Res;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/lang/Object;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->injectDevice(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/util/Device;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->phoneNumberHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/PhoneNumberHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->injectPhoneNumberHelper(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/text/PhoneNumberHelper;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->injectLocale(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/util/Locale;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->injectRes(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    return-void
.end method
