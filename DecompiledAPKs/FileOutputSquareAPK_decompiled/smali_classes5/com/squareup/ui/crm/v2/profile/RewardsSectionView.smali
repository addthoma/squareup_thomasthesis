.class public Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;
.super Landroid/widget/LinearLayout;
.source "RewardsSectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/RewardsSectionView$Component;
    }
.end annotation


# instance fields
.field private final coupons:Landroid/widget/LinearLayout;

.field private final header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

.field private final onViewAllClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final shortAnimTimeMs:I

.field private final viewAll:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const-class p2, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView$Component;->inject(Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const/high16 v0, 0x10e0000

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->shortAnimTimeMs:I

    .line 47
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_rewards_section_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 48
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->setOrientation(I)V

    .line 50
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->rewards_section_header:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 51
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->rewards_section_coupon_rows:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->coupons:Landroid/widget/LinearLayout;

    .line 52
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->rewards_section_bottom_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->viewAll:Landroid/view/View;

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->viewAll:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->onViewAllClicked:Lio/reactivex/Observable;

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_coupons_uppercase:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_coupons_and_rewards_manage_action:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->ENABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    return-void
.end method


# virtual methods
.method addCouponRow()Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 2

    .line 109
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_smartlinerow_list_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->coupons:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 110
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->coupons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method clearCouponRows()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->coupons:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public manageClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->onActionClicked()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 78
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->dropView(Ljava/lang/Object;)V

    .line 84
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 89
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onViewAllClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->onViewAllClicked:Lio/reactivex/Observable;

    return-object v0
.end method

.method public setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public setViewAllVisible(Z)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->viewAll:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setVisible(ZZ)V
    .locals 0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 95
    iget p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->shortAnimTimeMs:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 97
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 p1, 0x8

    .line 100
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->setVisibility(I)V

    :goto_0
    return-void
.end method
