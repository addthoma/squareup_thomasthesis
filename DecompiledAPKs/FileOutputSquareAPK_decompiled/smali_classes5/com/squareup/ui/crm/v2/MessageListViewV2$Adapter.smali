.class Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;
.super Landroid/widget/BaseAdapter;
.source "MessageListViewV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/MessageListViewV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Adapter"
.end annotation


# instance fields
.field private final marketTypeface:Landroid/graphics/Typeface;

.field final synthetic this$0:Lcom/squareup/ui/crm/v2/MessageListViewV2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/MessageListViewV2;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->this$0:Lcom/squareup/ui/crm/v2/MessageListViewV2;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->this$0:Lcom/squareup/ui/crm/v2/MessageListViewV2;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->marketTypeface:Landroid/graphics/Typeface;

    return-void
.end method

.method private buildConversationListRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 143
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_message_list_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->this$0:Lcom/squareup/ui/crm/v2/MessageListViewV2;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->presenter:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->getConversations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/protos/client/dialogue/ConversationListItem;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->this$0:Lcom/squareup/ui/crm/v2/MessageListViewV2;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->presenter:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->getConversations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 102
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->getItem(I)Lcom/squareup/protos/client/dialogue/ConversationListItem;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    .line 114
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->getItem(I)Lcom/squareup/protos/client/dialogue/ConversationListItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/ConversationListItem;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 122
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->getItem(I)Lcom/squareup/protos/client/dialogue/ConversationListItem;

    move-result-object p1

    if-nez p2, :cond_0

    .line 124
    invoke-direct {p0, p3}, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->buildConversationListRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    goto :goto_0

    :cond_0
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 127
    :goto_0
    iget-object p3, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_content:Ljava/lang/String;

    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_1

    iget-object p3, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_content:Ljava/lang/String;

    goto :goto_1

    :cond_1
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->this$0:Lcom/squareup/ui/crm/v2/MessageListViewV2;

    .line 129
    invoke-virtual {p3}, Lcom/squareup/ui/crm/v2/MessageListViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_feedback_no_message:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 127
    :goto_1
    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->marketTypeface:Landroid/graphics/Typeface;

    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->opened:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-virtual {p2, p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleTypeface(Landroid/graphics/Typeface;I)V

    .line 131
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->this$0:Lcom/squareup/ui/crm/v2/MessageListViewV2;

    iget-object p3, p3, Lcom/squareup/ui/crm/v2/MessageListViewV2;->presenter:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {p3, v0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->formatCreatorTimestamp(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 134
    new-instance p3, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter$1;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter$1;-><init>(Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;Lcom/squareup/protos/client/dialogue/ConversationListItem;)V

    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
