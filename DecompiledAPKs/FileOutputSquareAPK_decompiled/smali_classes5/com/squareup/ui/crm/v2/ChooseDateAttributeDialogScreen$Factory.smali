.class public final Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ChooseDateAttributeDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/marin/widgets/MarinDatePicker;Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 84
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p2

    .line 85
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYear()I

    move-result p3

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getMonth()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getDayOfMonth()I

    move-result p0

    invoke-virtual {p2, p3, v0, p0}, Ljava/util/Calendar;->set(III)V

    .line 86
    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/ProtoTimes;->asDateTime(Ljava/util/Date;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;->onSave(Lcom/squareup/protos/common/time/DateTime;)V

    return-void
.end method

.method static synthetic lambda$create$1(ZLcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    .line 92
    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;->onSave(Lcom/squareup/protos/common/time/DateTime;)V

    goto :goto_0

    .line 94
    :cond_0
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;->onDismiss()V

    :goto_0
    return-void
.end method

.method static synthetic lambda$create$2(Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;Landroid/content/DialogInterface;)V
    .locals 0

    .line 98
    invoke-interface {p0}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;->onDismiss()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 56
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;

    .line 57
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;->chooseDateAttributeDialogScreen()Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Component;

    move-result-object v0

    .line 58
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Component;->runner()Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;

    move-result-object v0

    .line 60
    sget v1, Lcom/squareup/crmupdatecustomer/R$layout;->crm_custom_date_attr_popup:I

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 61
    sget v2, Lcom/squareup/crmupdatecustomer/R$id;->crm_custom_date_attr_datepicker:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/marin/widgets/MarinDatePicker;

    .line 63
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;->getDateAttrViewData()Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    move-result-object v3

    .line 64
    iget-object v4, v3, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    const/4 v5, 0x1

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_1

    .line 68
    iget-object v6, v3, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iget-object v6, v6, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object v6, v6, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;->getLocale()Ljava/util/Locale;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/squareup/util/ProtoTimes;->asCalendar(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v6

    goto :goto_1

    .line 71
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 75
    :goto_1
    invoke-virtual {v6, v5}, Ljava/util/Calendar;->get(I)I

    move-result v7

    const/4 v8, 0x2

    .line 76
    invoke-virtual {v6, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v9, 0x5

    .line 77
    invoke-virtual {v6, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 74
    invoke-virtual {v2, v7, v8, v6}, Lcom/squareup/marin/widgets/MarinDatePicker;->updateDate(III)V

    .line 79
    new-instance v6, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v6, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object p1, v3, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    .line 80
    invoke-virtual {v6, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 81
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v3, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseDateAttributeDialogScreen$Factory$jjFMlxBLU6fMLHgMpl_IsrCc8AA;

    invoke-direct {v3, v2, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseDateAttributeDialogScreen$Factory$jjFMlxBLU6fMLHgMpl_IsrCc8AA;-><init>(Lcom/squareup/marin/widgets/MarinDatePicker;Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;)V

    .line 82
    invoke-virtual {p1, v1, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    if-eqz v4, :cond_2

    sget v1, Lcom/squareup/crmscreens/R$string;->remove:I

    goto :goto_2

    :cond_2
    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    :goto_2
    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseDateAttributeDialogScreen$Factory$VH4PWkS5w94MBp2HY4g75YunafM;

    invoke-direct {v2, v4, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseDateAttributeDialogScreen$Factory$VH4PWkS5w94MBp2HY4g75YunafM;-><init>(ZLcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;)V

    .line 88
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 97
    invoke-virtual {p1, v5}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseDateAttributeDialogScreen$Factory$BAPf6fyGDo9unTV8k8FuiVK0Umo;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseDateAttributeDialogScreen$Factory$BAPf6fyGDo9unTV8k8FuiVK0Umo;-><init>(Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;)V

    .line 98
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 99
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 79
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
