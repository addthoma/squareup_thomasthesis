.class synthetic Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;
.super Ljava/lang/Object;
.source "InvoicesSectionDataRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$invoice$GetMetricsResponse$Metric$ValueType:[I

.field static final synthetic $SwitchMap$com$squareup$protos$client$invoice$MetricType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 79
    invoke-static {}, Lcom/squareup/protos/client/invoice/MetricType;->values()[Lcom/squareup/protos/client/invoice/MetricType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$MetricType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$MetricType:[I

    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/MetricType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$MetricType:[I

    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/MetricType;->ordinal()I

    move-result v2

    const/4 v3, 0x2

    aput v3, v1, v2
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$MetricType:[I

    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/MetricType;->ordinal()I

    move-result v2

    const/4 v3, 0x3

    aput v3, v1, v2
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$MetricType:[I

    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->UNPAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/MetricType;->ordinal()I

    move-result v2

    const/4 v3, 0x4

    aput v3, v1, v2
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 48
    :catch_3
    invoke-static {}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;->values()[Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    move-result-object v1

    array-length v1, v1

    new-array v1, v1, [I

    sput-object v1, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$GetMetricsResponse$Metric$ValueType:[I

    :try_start_4
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$GetMetricsResponse$Metric$ValueType:[I

    sget-object v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;->MONEY_VALUE:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    return-void
.end method
