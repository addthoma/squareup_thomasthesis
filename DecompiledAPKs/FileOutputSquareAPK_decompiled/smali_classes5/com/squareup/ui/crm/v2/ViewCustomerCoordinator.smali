.class public Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ViewCustomerCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;,
        Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;
    }
.end annotation


# static fields
.field private static final ERROR_TAG_LOAD_FAILED:Ljava/lang/String; = "Load"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private activityListSection:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

.field private addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

.field private additionalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final buyerSummaryDataRenderer:Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;

.field private buyerSummarySection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

.field private cardOnFileSection:Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;

.field private final cardOnFileViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;

.field private final clock:Lcom/squareup/util/Clock;

.field private contentContainer:Landroid/view/View;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final device:Lcom/squareup/util/Device;

.field private dropDownContainer:Lcom/squareup/ui/DropDownContainer;

.field private final errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private footerButtonContainer:Landroid/view/View;

.field private frequentItemsSection:Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;

.field private final frequentItemsSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;

.field private invoicesSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

.field private final invoicesSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;

.field private loyaltySection:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private messageFooterButton:Lcom/squareup/noho/NohoButton;

.field private notesSection:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

.field private final notesSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;

.field private pastAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

.field private personalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

.field private final personalInformationViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

.field private profileAttachmentsSection:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;

.field private profileContainer:Landroid/view/ViewGroup;

.field private progressBar:Landroid/view/View;

.field private final res:Lcom/squareup/util/Res;

.field private rewardsSection:Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;

.field private final runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

.field private upcomingAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

.field private final viewCustomerConfiguration:Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/util/Device;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 282
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    move-object v1, p1

    .line 283
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    move-object v1, p2

    .line 284
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->device:Lcom/squareup/util/Device;

    move-object v1, p3

    .line 285
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    move-object v1, p4

    .line 286
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    move-object v1, p5

    .line 287
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p6

    .line 288
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->clock:Lcom/squareup/util/Clock;

    move-object v1, p7

    .line 289
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    move-object v1, p8

    .line 290
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p9

    .line 291
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    move-object v1, p10

    .line 292
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->personalInformationViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    move-object v1, p11

    .line 293
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->cardOnFileViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;

    move-object v1, p12

    .line 294
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->notesSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;

    move-object/from16 v1, p13

    .line 295
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->viewCustomerConfiguration:Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

    move-object/from16 v1, p15

    .line 296
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->buyerSummaryDataRenderer:Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;

    move-object/from16 v1, p14

    .line 297
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->invoicesSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;

    move-object/from16 v1, p16

    .line 298
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->frequentItemsSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;

    .line 299
    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    return-object p0
.end method

.method private attachActivityList(Landroid/view/View;Lrx/Observable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 636
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$xzFCz7d8TIvT8PB3OO8bXOzLLa4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$xzFCz7d8TIvT8PB3OO8bXOzLLa4;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 641
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 642
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$IdwwZxbe0XM91817dJk4-UGqANs;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$IdwwZxbe0XM91817dJk4-UGqANs;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 647
    :cond_0
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$V1QcO1T62wCc-RA9IIoGJfmgXfs;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$V1QcO1T62wCc-RA9IIoGJfmgXfs;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 652
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$A0hL50tPsCecSMYBINRM4sWJ4d8;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$A0hL50tPsCecSMYBINRM4sWJ4d8;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 659
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->activityListSection:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private attachAdditionalInfo(Landroid/view/View;Lrx/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 664
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$YFBmxlRFDWlLogIVzI988NGL2c8;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$YFBmxlRFDWlLogIVzI988NGL2c8;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private attachAppointmentsSections(Landroid/view/View;Lrx/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 498
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$rnnf_EJqTaeC_gha36OSW3lkP3g;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$rnnf_EJqTaeC_gha36OSW3lkP3g;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 509
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$exmTe71LCw-FZlxbeJj7hFhH-bA;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$exmTe71LCw-FZlxbeJj7hFhH-bA;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 515
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$WMSZA8fpNBJfZGTEYAr5V01Qbog;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$WMSZA8fpNBJfZGTEYAr5V01Qbog;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 522
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$aEKOVnSRPUYzstgxRm7wkZVnhGI;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$aEKOVnSRPUYzstgxRm7wkZVnhGI;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 526
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$mtUtguUcOO23Ieh6Luhupge7c-o;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$mtUtguUcOO23Ieh6Luhupge7c-o;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private attachBuyerSummary(Landroid/view/View;Lrx/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 629
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$_TcrlD4sAl8P9EXTJI2aAnDox4Y;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$_TcrlD4sAl8P9EXTJI2aAnDox4Y;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private attachCardOnFile(Landroid/view/View;Lrx/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 437
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$zmTFgUoBpidpwclBUBfQiiWwCI4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$zmTFgUoBpidpwclBUBfQiiWwCI4;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 447
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ZxAuq_g8mhL2MSvFAcjt8guiSqY;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ZxAuq_g8mhL2MSvFAcjt8guiSqY;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 455
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ugdm6W4EgivzDFlcD1YLM0Cm-Wc;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ugdm6W4EgivzDFlcD1YLM0Cm-Wc;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 462
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->cardOnFileSection:Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->setVisibility(I)V

    goto :goto_0

    .line 464
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->cardOnFileSection:Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private attachFooterButton(Landroid/view/View;Lrx/Observable;Lrx/Observable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 689
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_0

    .line 690
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$4-TOGOH5eVmX_MJLq5mPLH3tE6M;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$4-TOGOH5eVmX_MJLq5mPLH3tE6M;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 735
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->footerButtonContainer:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private attachFrequentItemsSection(Landroid/view/View;Lrx/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 532
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$NxvWjrzEmziO7WWj8PEBeCxNKfc;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$NxvWjrzEmziO7WWj8PEBeCxNKfc;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 537
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$zRjWbqKCoHb7_X6IZQAZaqEeLD8;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$zRjWbqKCoHb7_X6IZQAZaqEeLD8;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private attachInvoicesSection(Landroid/view/View;Lrx/Observable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 573
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 574
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$csTlCjaSwn-3NZB75QGEZHXsqCk;

    invoke-direct {v1, p0, p2, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$csTlCjaSwn-3NZB75QGEZHXsqCk;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;Z)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 588
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$RrJh7XEiryCc9urgZ-vpG2fjNLo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$RrJh7XEiryCc9urgZ-vpG2fjNLo;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-eqz v0, :cond_0

    .line 593
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$uTn3MKGtcmORk8ugayaISlzQeYo;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$uTn3MKGtcmORk8ugayaISlzQeYo;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method private attachLoyaltySection(Landroid/view/View;Lrx/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 601
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 602
    invoke-interface {p1}, Lcom/squareup/loyalty/LoyaltySettings;->hasLoyaltyProgram()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 603
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->isInvoicePath(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 608
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->loyaltySection:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private attachNoteSection(Landroid/view/View;Lrx/Observable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 469
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_NOTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$1u2bzoPFkfg0xHgJLe0HE1IvsYM;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$1u2bzoPFkfg0xHgJLe0HE1IvsYM;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 475
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$NVLXFx44CBM6zFP4aD5ITjlMh1k;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$NVLXFx44CBM6zFP4aD5ITjlMh1k;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 482
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$zA8JM4R5RDmmZxhdmoLtyhr1ksQ;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$zA8JM4R5RDmmZxhdmoLtyhr1ksQ;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 486
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ZMlhbkafWl2RXwul4IWyUm3Hp_A;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ZMlhbkafWl2RXwul4IWyUm3Hp_A;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 493
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->notesSection:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private attachPersonalInformationSection(Landroid/view/View;Lrx/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 421
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$TtwNJnmGe9zYkb0_SBWmImPyUL4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$TtwNJnmGe9zYkb0_SBWmImPyUL4;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 428
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$NG_KSgo1DJV_R2_yEJjwtQOwx8g;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$NG_KSgo1DJV_R2_yEJjwtQOwx8g;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private attachProfileAttachmentsSection(Landroid/view/View;Lrx/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 547
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showProfileAttachmentsSection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$J0Tevp9bhv3HhuFgPFUEQruKMJY;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$J0Tevp9bhv3HhuFgPFUEQruKMJY;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 554
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$uO54XktmKuhDsUx-wW7FaBT1YOM;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$uO54XktmKuhDsUx-wW7FaBT1YOM;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 561
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$4Fu1RIeM2vhZI-MS_bl0PQIs2Q0;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$4Fu1RIeM2vhZI-MS_bl0PQIs2Q0;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 568
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->profileAttachmentsSection:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private attachRewardsSection(Landroid/view/View;Lrx/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 613
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$E_zyXkWAgsMCzok2qxatIPzs8NA;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$E_zyXkWAgsMCzok2qxatIPzs8NA;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 617
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$YMTwox809hTcOFk0gphZKYbDzRM;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$YMTwox809hTcOFk0gphZKYbDzRM;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 621
    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$MIWncyNemBH_oV6xJWSux9ljhzg;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$MIWncyNemBH_oV6xJWSux9ljhzg;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Z",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lrx/functions/Action0;",
            "Lcom/squareup/analytics/RegisterActionName;",
            ")V"
        }
    .end annotation

    .line 799
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p3, :cond_0

    .line 801
    new-instance p3, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$cA4BfBkgPZ0bkAOm-iIYOqbU9ks;

    invoke-direct {p3, p4, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$cA4BfBkgPZ0bkAOm-iIYOqbU9ks;-><init>(Lrx/Observable;Landroid/view/View;)V

    invoke-static {p1, p3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 804
    new-instance p3, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$bO59dip0cdwpWNnWCsiH7L345jE;

    invoke-direct {p3, p0, p2, p6, p5}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$bO59dip0cdwpWNnWCsiH7L345jE;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Landroid/view/View;Lcom/squareup/analytics/RegisterActionName;Lrx/functions/Action0;)V

    invoke-static {p1, p3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method private bindExposedActions(Landroid/view/View;Lrx/Observable;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 831
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_exposed_action:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 832
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_new_sale:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 834
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->showsOverflow()Z

    move-result v1

    const/16 v2, 0x8

    if-nez v1, :cond_0

    .line 836
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideCustomView()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 837
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    .line 838
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 839
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 842
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->getExposedActions()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 844
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_0

    .line 850
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->enoughRoomForExposedAction()Z

    move-result v1

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v10, 0x1

    if-eqz v1, :cond_3

    .line 853
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 855
    iget-object v5, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v6, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    iget v7, v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->shortTitle:I

    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)V

    .line 856
    iget-object v5, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$LZ23C8LEMHkGHBAdKBD0Vx3nuvo;

    invoke-direct {v6, p0, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$LZ23C8LEMHkGHBAdKBD0Vx3nuvo;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;)V

    invoke-virtual {v5, v6}, Lcom/squareup/marin/widgets/MarinActionBar;->showSecondaryButton(Ljava/lang/Runnable;)V

    .line 860
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$SatfnSx2kovV7udart890WtPgcY;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$SatfnSx2kovV7udart890WtPgcY;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lrx/Observable;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 863
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v10, :cond_2

    .line 866
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 867
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 868
    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-lt v1, v9, :cond_6

    .line 872
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 873
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 874
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 875
    iget v2, v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->longTitle:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v7, 0x1

    .line 877
    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->getExposedActionAction(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;)Lrx/functions/Action0;

    move-result-object v9

    iget-object v10, v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->registerActionName:Lcom/squareup/analytics/RegisterActionName;

    move-object v4, p0

    move-object v5, p1

    move-object v6, v0

    move-object v8, p2

    .line 876
    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_1

    .line 883
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    .line 884
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-lt v1, v10, :cond_4

    .line 886
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 887
    iget v2, v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->longTitle:I

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v4, 0x1

    .line 889
    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->getExposedActionAction(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;)Lrx/functions/Action0;

    move-result-object v6

    iget-object v7, v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->registerActionName:Lcom/squareup/analytics/RegisterActionName;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    .line 888
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    .line 892
    :cond_4
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v9, :cond_6

    .line 895
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 896
    iget v2, v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->longTitle:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v7, 0x1

    .line 898
    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->getExposedActionAction(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;)Lrx/functions/Action0;

    move-result-object v9

    iget-object v10, v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->registerActionName:Lcom/squareup/analytics/RegisterActionName;

    move-object v4, p0

    move-object v5, p1

    move-object v6, v0

    move-object v8, p2

    .line 897
    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_1

    .line 847
    :cond_5
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    .line 848
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 849
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_6
    :goto_1
    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 3

    .line 750
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 751
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->progressBar:Landroid/view/View;

    .line 752
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_content_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->contentContainer:Landroid/view/View;

    .line 754
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_personal_information_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->personalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    .line 755
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_section_card_on_file:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->cardOnFileSection:Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;

    .line 756
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_section_notes:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->notesSection:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    .line 757
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_upcoming_appointments_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->upcomingAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    .line 758
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_past_appointments_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->pastAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    .line 759
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_section_files:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->profileAttachmentsSection:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;

    .line 760
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_invoices_summary_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->invoicesSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    .line 761
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_section_loyalty:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->loyaltySection:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    .line 762
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_section_rewards:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->rewardsSection:Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;

    .line 763
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_buyer_summary_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->buyerSummarySection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    .line 764
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_activity_list_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->activityListSection:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

    .line 765
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_additional_info_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->additionalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    .line 766
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_section_frequent_items:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->frequentItemsSection:Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;

    .line 767
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_drop_down_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DropDownContainer;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    .line 768
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_footer_button_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->footerButtonContainer:Landroid/view/View;

    .line 769
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_footer_message_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->messageFooterButton:Lcom/squareup/noho/NohoButton;

    .line 770
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_footer_add_or_remove_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    .line 772
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 773
    sget p1, Lcom/squareup/crm/R$drawable;->crm_action_overflow:I

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 774
    sget-object p1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 776
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Spx0ZbP1AxJguFlie_oak7iQgB4;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Spx0ZbP1AxJguFlie_oak7iQgB4;-><init>(Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->open_menu:I

    .line 777
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$N2lr1VXOkwWrM3-jBtlpC2fmDlA;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$N2lr1VXOkwWrM3-jBtlpC2fmDlA;-><init>(Lcom/squareup/ui/DropDownContainer;)V

    .line 778
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 780
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_0

    .line 781
    sget v0, Lcom/squareup/marin/R$drawable;->marin_white_border_bottom_light_gray_1px:I

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setBackground(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private canMessageContact(Lcom/squareup/protos/client/rolodex/Contact;)Z
    .locals 1

    .line 740
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/EmailSubscriptions;->can_receive_direct_messaging:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/EmailSubscriptions;->can_receive_direct_messaging:Ljava/lang/Boolean;

    .line 742
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private enoughRoomForExposedAction()Z
    .locals 1

    .line 955
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private getExposedActionAction(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;)Lrx/functions/Action0;
    .locals 1

    .line 1012
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 1026
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid ExposedAction"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1024
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$rottMnujbjV99F8IPAXSeHkuUsA;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$rottMnujbjV99F8IPAXSeHkuUsA;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    return-object v0

    .line 1022
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$vy9P5s4_R-sknTaBQNRX0do3erQ;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$vy9P5s4_R-sknTaBQNRX0do3erQ;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    return-object v0

    .line 1018
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$9Gi9QHQDtY5fTavyRAqI5SzYfzU;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$9Gi9QHQDtY5fTavyRAqI5SzYfzU;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    return-object v0

    .line 1016
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$kSESii_LQLh5Hzf0Z-92kwhVQp8;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$kSESii_LQLh5Hzf0Z-92kwhVQp8;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    return-object v0

    .line 1014
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$sR_DJ0D994nhKQxBK9f-ECYhPc8;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$sR_DJ0D994nhKQxBK9f-ECYhPc8;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getExposedActions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;",
            ">;"
        }
    .end annotation

    .line 963
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isForTransferringLoyalty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 964
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->TransferLoyalty:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 966
    :cond_0
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1007
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid path type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 995
    :pswitch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 996
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->viewCustomerConfiguration:Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

    invoke-interface {v1}, Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;->getDisplayAposOverflow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 997
    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->NewAppointment:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 999
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->canStartNewSaleWithCustomerFromApplet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1000
    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->NewSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0

    :pswitch_1
    const/4 v0, 0x0

    return-object v0

    .line 984
    :pswitch_2
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromEstimate:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 981
    :pswitch_3
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromInvoice:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 977
    :pswitch_4
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 971
    :pswitch_5
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->AddToSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private hasBackButton()Z
    .locals 2

    .line 950
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 951
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private hideProfile(Landroid/view/View;)V
    .locals 2

    .line 788
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->profileContainer:Landroid/view/ViewGroup;

    .line 789
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->profileContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 790
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$Lh-Gl04OR7BC51pPcvh2wxIBnBM;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$Lh-Gl04OR7BC51pPcvh2wxIBnBM;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 792
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 p1, 0x0

    .line 793
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->updateActionBar(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$attach$2(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;)Ljava/lang/Boolean;
    .locals 1

    .line 340
    sget-object v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->VALID:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$attach$4(Lkotlin/Pair;)Ljava/lang/Boolean;
    .locals 1

    .line 347
    invoke-virtual {p0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->getLoyaltyTokenForContact(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$bindDropDownAction$71(Lrx/Observable;Landroid/view/View;)Lrx/Subscription;
    .locals 1

    .line 801
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$-zgxqVGHwry-1O7EGLiPN5cj5CU;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$-zgxqVGHwry-1O7EGLiPN5cj5CU;-><init>(Landroid/view/View;)V

    .line 802
    invoke-virtual {p0, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$bindViews$70(Landroid/widget/ImageView;Landroid/content/Context;)Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$null$24(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 500
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$null$45(Ljava/lang/Throwable;)V
    .locals 1

    .line 581
    instance-of v0, p0, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    return-void

    .line 584
    :cond_0
    new-instance v0, Lrx/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p0}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static synthetic lambda$null$49(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 0

    .line 595
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$null$65(Lcom/squareup/noho/EdgePainter;)Lkotlin/Unit;
    .locals 1

    const/4 v0, 0x2

    .line 698
    invoke-virtual {p0, v0}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    .line 699
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$null$66(Lcom/squareup/noho/EdgePainter;)Lkotlin/Unit;
    .locals 1

    const/4 v0, 0x2

    .line 711
    invoke-virtual {p0, v0}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    .line 712
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$null$67(Lcom/squareup/noho/EdgePainter;)Lkotlin/Unit;
    .locals 1

    const/4 v0, 0x0

    .line 723
    invoke-virtual {p0, v0}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    .line 724
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method public static synthetic lambda$ucKo5pUdsamo2jAhy8WY7SDk1II(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->updateActionBar(Ljava/lang/String;)V

    return-void
.end method

.method private showsOverflow()Z
    .locals 1

    .line 946
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isViewCustomerReadOnly()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private updateActionBar(Ljava/lang/String;)V
    .locals 2

    .line 816
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->hasBackButton()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 817
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->usesXGlyphUpButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 818
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 820
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    .line 822
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 823
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$Lh-Gl04OR7BC51pPcvh2wxIBnBM;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$Lh-Gl04OR7BC51pPcvh2wxIBnBM;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 825
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 826
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideUpButton()V

    :goto_1
    return-void
.end method

.method private usesXGlyphUpButton()Z
    .locals 2

    .line 939
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isViewCustomerReadOnly()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 940
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 941
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 942
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isForTransferringLoyalty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 12

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_DISABLE_BUYER_PROFILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->hideProfile(Landroid/view/View;)V

    return-void

    .line 307
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindViews(Landroid/view/View;)V

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$Lh-Gl04OR7BC51pPcvh2wxIBnBM;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$Lh-Gl04OR7BC51pPcvh2wxIBnBM;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 309
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->updateActionBar(Ljava/lang/String;)V

    .line 311
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$4lIDeRb_4M8YluT2bdqqma6GQIY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$4lIDeRb_4M8YluT2bdqqma6GQIY;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->contactForViewCustomerScreen()Lrx/Observable;

    move-result-object v0

    .line 339
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->contactLoadingState()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$3Zmzb5XMNzhmLrh6WnJvPyBNnSk;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$3Zmzb5XMNzhmLrh6WnJvPyBNnSk;

    .line 340
    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$GFylU1frcuq1x8POIVoBrbvDwvw;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$GFylU1frcuq1x8POIVoBrbvDwvw;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 341
    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 342
    invoke-virtual {v1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    .line 344
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isForTransferringLoyalty()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 346
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ljMmGiheoJ_ru_re7ul9kCof5lU;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ljMmGiheoJ_ru_re7ul9kCof5lU;

    .line 347
    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 348
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v1

    .line 349
    invoke-virtual {v1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    .line 353
    :cond_1
    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$VmTGd4sG0_bvB15UTr35MJgE0TA;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$VmTGd4sG0_bvB15UTr35MJgE0TA;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 357
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindExposedActions(Landroid/view/View;Lrx/Observable;)V

    .line 360
    sget v2, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_edit:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v9, Lcom/squareup/ui/crm/v2/-$$Lambda$JLjuFMMlPiLCLlo6U1_FJnmbNOo;

    invoke-direct {v9, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$JLjuFMMlPiLCLlo6U1_FJnmbNOo;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    sget-object v10, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_EDIT:Lcom/squareup/analytics/RegisterActionName;

    move-object v4, p0

    move-object v5, p1

    move-object v8, v1

    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    .line 365
    sget v2, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_add_card_on_file:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 366
    invoke-interface {v2}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v7

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v9, Lcom/squareup/ui/crm/v2/-$$Lambda$pzYM-Hk9sE8w6MgAnX-16zAqH_A;

    invoke-direct {v9, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$pzYM-Hk9sE8w6MgAnX-16zAqH_A;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    sget-object v10, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 365
    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    .line 371
    sget v2, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_add_note:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->CRM_NOTES:Lcom/squareup/settings/server/Features$Feature;

    .line 372
    invoke-interface {v2, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v9, Lcom/squareup/ui/crm/v2/-$$Lambda$Do35HLIaskdzGLt6yEhL567MU3A;

    invoke-direct {v9, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$Do35HLIaskdzGLt6yEhL567MU3A;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    sget-object v10, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_NOTE:Lcom/squareup/analytics/RegisterActionName;

    move-object v4, p0

    .line 371
    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    .line 377
    sget v2, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_upload_file:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 378
    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showProfileAttachmentsSection()Z

    move-result v7

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v9, Lcom/squareup/ui/crm/v2/-$$Lambda$6sTdL2rnkZXrNpsT20yC-zJGPG8;

    invoke-direct {v9, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$6sTdL2rnkZXrNpsT20yC-zJGPG8;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    sget-object v10, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_UPLOAD_FILE:Lcom/squareup/analytics/RegisterActionName;

    .line 377
    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    .line 382
    sget v2, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_send_message:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    .line 383
    invoke-interface {v2, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$tQeCzVyHTT7VjHHra8CAs1wOCCU;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$tQeCzVyHTT7VjHHra8CAs1wOCCU;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 384
    invoke-static {v1, v0, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v2

    .line 386
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v8

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v9, Lcom/squareup/ui/crm/v2/-$$Lambda$0TPwnCpulqSqmYnTVtKwSZrDRyE;

    invoke-direct {v9, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$0TPwnCpulqSqmYnTVtKwSZrDRyE;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    sget-object v10, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_SEND_MESSAGE:Lcom/squareup/analytics/RegisterActionName;

    move-object v4, p0

    .line 382
    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    .line 391
    sget v2, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_merge_customer:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->CRM_MERGE_CONTACTS:Lcom/squareup/settings/server/Features$Feature;

    .line 392
    invoke-interface {v2, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    const/4 v11, 0x1

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 393
    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v2

    sget-object v4, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v2, v4, :cond_2

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 394
    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v2

    sget-object v4, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v2, v4, :cond_3

    :cond_2
    const/4 v7, 0x1

    goto :goto_0

    :cond_3
    const/4 v7, 0x0

    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v9, Lcom/squareup/ui/crm/v2/-$$Lambda$ScPQ_Z7bLn27o05lC4MXtvrrFIw;

    invoke-direct {v9, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ScPQ_Z7bLn27o05lC4MXtvrrFIw;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    sget-object v10, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_MERGE_PROFILE:Lcom/squareup/analytics/RegisterActionName;

    move-object v4, p0

    move-object v5, p1

    move-object v8, v1

    .line 391
    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    .line 399
    sget v2, Lcom/squareup/crmviewcustomer/R$id;->crm_profile_action_delete_customer:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 400
    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v2

    sget-object v4, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v2, v4, :cond_5

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 401
    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v2

    sget-object v4, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v2, v4, :cond_4

    goto :goto_1

    :cond_4
    const/4 v7, 0x0

    goto :goto_2

    :cond_5
    :goto_1
    const/4 v7, 0x1

    :goto_2
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v9, Lcom/squareup/ui/crm/v2/-$$Lambda$hLpBCKNonhpx0zeBXVC_WvJin_0;

    invoke-direct {v9, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$hLpBCKNonhpx0zeBXVC_WvJin_0;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    sget-object v10, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_DELETE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    move-object v4, p0

    move-object v5, p1

    move-object v8, v1

    .line 399
    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;Lcom/squareup/analytics/RegisterActionName;)V

    .line 405
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachPersonalInformationSection(Landroid/view/View;Lrx/Observable;)V

    .line 406
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachCardOnFile(Landroid/view/View;Lrx/Observable;)V

    .line 407
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachNoteSection(Landroid/view/View;Lrx/Observable;)V

    .line 408
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachAppointmentsSections(Landroid/view/View;Lrx/Observable;)V

    .line 409
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachProfileAttachmentsSection(Landroid/view/View;Lrx/Observable;)V

    .line 410
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachInvoicesSection(Landroid/view/View;Lrx/Observable;)V

    .line 411
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachLoyaltySection(Landroid/view/View;Lrx/Observable;)V

    .line 412
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachRewardsSection(Landroid/view/View;Lrx/Observable;)V

    .line 413
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachBuyerSummary(Landroid/view/View;Lrx/Observable;)V

    .line 414
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachActivityList(Landroid/view/View;Lrx/Observable;)V

    .line 415
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachAdditionalInfo(Landroid/view/View;Lrx/Observable;)V

    .line 416
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachFrequentItemsSection(Landroid/view/View;Lrx/Observable;)V

    .line 417
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->attachFooterButton(Landroid/view/View;Lrx/Observable;Lrx/Observable;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 0

    .line 746
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$attach$1$ViewCustomerCoordinator()Lrx/Subscription;
    .locals 2

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->contactLoadingState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$LEbPmzT8ZiQFmhPMQjLHqkmVJn0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$LEbPmzT8ZiQFmhPMQjLHqkmVJn0;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 313
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$3$ViewCustomerCoordinator(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 341
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isViewCustomerReadOnly()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$5$ViewCustomerCoordinator()Lrx/Subscription;
    .locals 2

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->title()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ucKo5pUdsamo2jAhy8WY7SDk1II;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ucKo5pUdsamo2jAhy8WY7SDk1II;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 355
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$6$ViewCustomerCoordinator(Ljava/lang/Boolean;Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/Boolean;
    .locals 0

    .line 386
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->canMessageContact(Lcom/squareup/protos/client/rolodex/Contact;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachActivityList$57$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 638
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->activityListSection:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$98TOMPhZcR1-ZWW87MWjRJm45JU;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$98TOMPhZcR1-ZWW87MWjRJm45JU;-><init>(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;)V

    .line 639
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachActivityList$58$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 643
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->activityListSection:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->onConversationClicked()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$T9u-f79sV3xzX5Aqx2zEV911lGU;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$T9u-f79sV3xzX5Aqx2zEV911lGU;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    .line 644
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachActivityList$60$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 648
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->activityListSection:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->onPaymentClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$H6EWXMnrlK6m5OfOK0YxT85PmKM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$H6EWXMnrlK6m5OfOK0YxT85PmKM;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 649
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachActivityList$62$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 653
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->activityListSection:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->onViewAllClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Mf9YNC_2gGvKpZ-yGEEBl-RRqZk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Mf9YNC_2gGvKpZ-yGEEBl-RRqZk;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 654
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachAdditionalInfo$64$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 1

    .line 665
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Nn-gvD5DlA3xlAwSK7pGPxANbIk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Nn-gvD5DlA3xlAwSK7pGPxANbIk;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachAppointmentsSections$26$ViewCustomerCoordinator(Lrx/Observable;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 499
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$irRhmrCHiH8QzzHIeXll-fO7ZKk;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$irRhmrCHiH8QzzHIeXll-fO7ZKk;

    .line 500
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->viewCustomerConfiguration:Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

    .line 502
    invoke-interface {v0}, Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;->getCrmAppointmentsDataRenderer()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$NpOQAvwhjVlP62bjMGz2mUULBng;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$NpOQAvwhjVlP62bjMGz2mUULBng;-><init>(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;)V

    .line 501
    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$e_C7KoNjd6oQHiAPLezB0D9AiYQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$e_C7KoNjd6oQHiAPLezB0D9AiYQ;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 503
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachAppointmentsSections$28$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 510
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->upcomingAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->getSectionHeaderActionClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Ux6QZfW2dw3tda2D4y0GsYQziNs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Ux6QZfW2dw3tda2D4y0GsYQziNs;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 511
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachAppointmentsSections$30$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 516
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->upcomingAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->getAppointmentClicked()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->pastAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    .line 517
    invoke-virtual {v1}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->getAppointmentClicked()Lio/reactivex/Observable;

    move-result-object v1

    .line 516
    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$EwGki_FmWPD-CMdIQSBK4C3NtXQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$EwGki_FmWPD-CMdIQSBK4C3NtXQ;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 518
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachAppointmentsSections$32$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 523
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->upcomingAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->getOnViewAllClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ZQULxX6B8HNx8eTUP8pNuR7jy-o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ZQULxX6B8HNx8eTUP8pNuR7jy-o;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 524
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachAppointmentsSections$34$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 527
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->pastAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->getOnViewAllClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$VJ4xfO1INsAxxCn706n8LYm45sQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$VJ4xfO1INsAxxCn706n8LYm45sQ;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 528
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachBuyerSummary$56$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 630
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->buyerSummaryDataRenderer:Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ajAHdmWEFVJ92YnzROeV3n8ziyc;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ajAHdmWEFVJ92YnzROeV3n8ziyc;-><init>(Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;)V

    .line 631
    invoke-virtual {p1, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->buyerSummarySection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$bu9eNKHeP4CMLkJ5YNeA6gYDR8Y;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$bu9eNKHeP4CMLkJ5YNeA6gYDR8Y;-><init>(Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;)V

    .line 632
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachCardOnFile$12$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 441
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$taCTJJbRMoJ0oz5eBEav6N17ca0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$taCTJJbRMoJ0oz5eBEav6N17ca0;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 442
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->cardOnFileSection:Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$BMM2Qz-Fe6lSzb4KhQKcst7_Tz4;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$BMM2Qz-Fe6lSzb4KhQKcst7_Tz4;-><init>(Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;)V

    .line 444
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachCardOnFile$14$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 448
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->cardOnFileSection:Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->addCardClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ov_UGb--gh_vgaWK3EJlk8SNdBY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$ov_UGb--gh_vgaWK3EJlk8SNdBY;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 449
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachCardOnFile$16$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 456
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->cardOnFileSection:Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->unlinkInstrumentClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$LfEMiN6JtsvEOyDi7HbyW83eVGg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$LfEMiN6JtsvEOyDi7HbyW83eVGg;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 457
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachFooterButton$69$ViewCustomerCoordinator(Lrx/Observable;Lrx/Observable;)Lrx/Subscription;
    .locals 1

    .line 691
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$pvxXAglmglKmbP63zB7pvvRT77o;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$pvxXAglmglKmbP63zB7pvvRT77o;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 692
    invoke-static {p2}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachFrequentItemsSection$35$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 533
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->frequentItemsSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$epi63xU_VcbR5FpliEDUvva0r30;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$epi63xU_VcbR5FpliEDUvva0r30;-><init>(Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;)V

    .line 534
    invoke-virtual {p1, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->frequentItemsSection:Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$Et6uDTQxcxxurA1PRrtHKcXuNKM;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$Et6uDTQxcxxurA1PRrtHKcXuNKM;-><init>(Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;)V

    .line 535
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachFrequentItemsSection$37$ViewCustomerCoordinator()Lrx/Subscription;
    .locals 2

    .line 538
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->frequentItemsSection:Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->onViewAllClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$5dBhg4AD4XhJZc7_eBoAYd5Z2jk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$5dBhg4AD4XhJZc7_eBoAYd5Z2jk;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 539
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachInvoicesSection$46$ViewCustomerCoordinator(Lrx/Observable;Z)Lrx/Subscription;
    .locals 2

    .line 575
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$cMUsAew0Qb9K-I73E2cxZjldDTk;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$cMUsAew0Qb9K-I73E2cxZjldDTk;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    .line 576
    invoke-virtual {p1, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$oNhSJbgQIumgiBjdJm4VHtvGo8Y;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$oNhSJbgQIumgiBjdJm4VHtvGo8Y;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Z)V

    .line 577
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->invoicesSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$bu9eNKHeP4CMLkJ5YNeA6gYDR8Y;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$bu9eNKHeP4CMLkJ5YNeA6gYDR8Y;-><init>(Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;)V

    sget-object p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$t8SAaUdYIl7e8nD9oqSSAwWG_xA;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$t8SAaUdYIl7e8nD9oqSSAwWG_xA;

    .line 578
    invoke-virtual {p1, v0, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachInvoicesSection$48$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 589
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->invoicesSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->getCtaButtonClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$4w2Nuotz5EeMvZzIIDpO8oxi_vI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$4w2Nuotz5EeMvZzIIDpO8oxi_vI;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 590
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachInvoicesSection$50$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 594
    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$rIKLOdgsuQx3kdU4xI5Smx7n1yc;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$rIKLOdgsuQx3kdU4xI5Smx7n1yc;

    .line 595
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$cbXSWDRfx0XAihpCiUceWMByFdk;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$cbXSWDRfx0XAihpCiUceWMByFdk;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    .line 596
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachNoteSection$17$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 471
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->notesSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$nRlIyZDp5yh_9lSKJ99cshv6VKU;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$nRlIyZDp5yh_9lSKJ99cshv6VKU;-><init>(Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;)V

    .line 472
    invoke-virtual {p1, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->notesSection:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$PCONjJ_GYI7Lttw4UjG13Q8_Yz8;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$PCONjJ_GYI7Lttw4UjG13Q8_Yz8;-><init>(Lcom/squareup/ui/crm/v2/profile/NotesSectionView;)V

    .line 473
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachNoteSection$19$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 476
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->notesSection:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->onNoteClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Xmp5fUgbhjjheUo_fATkrQ65sGA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Xmp5fUgbhjjheUo_fATkrQ65sGA;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 477
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachNoteSection$21$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 483
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->notesSection:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->onAllNotesClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$-YnUTXrSfI_xzro9zafn0CZaK5I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$-YnUTXrSfI_xzro9zafn0CZaK5I;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 484
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachNoteSection$23$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->notesSection:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->onAddNoteClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$wXVOa2-i8YJNV5PkjNhzJv4QBug;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$wXVOa2-i8YJNV5PkjNhzJv4QBug;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 488
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachPersonalInformationSection$10$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 429
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->personalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->onEditButtonClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$HupejjZbCGgEtEGVkEKNci5fiL4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$HupejjZbCGgEtEGVkEKNci5fiL4;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 430
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachPersonalInformationSection$8$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 422
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$geeFAFzTkk6Mty0meMjElyptO1k;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$geeFAFzTkk6Mty0meMjElyptO1k;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 423
    invoke-virtual {p1, v0}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->personalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$bu9eNKHeP4CMLkJ5YNeA6gYDR8Y;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$bu9eNKHeP4CMLkJ5YNeA6gYDR8Y;-><init>(Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;)V

    .line 425
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachProfileAttachmentsSection$39$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 1

    .line 549
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Vx7pyU7bgRXwif0yuE2RALJHIUs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Vx7pyU7bgRXwif0yuE2RALJHIUs;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 550
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachProfileAttachmentsSection$41$ViewCustomerCoordinator()Lrx/Subscription;
    .locals 2

    .line 555
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->profileAttachmentsSection:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->getOnUploadFilesClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$E8v6G2Rbh7uCyGUhhQuPF8Bqhrs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$E8v6G2Rbh7uCyGUhhQuPF8Bqhrs;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 556
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachProfileAttachmentsSection$43$ViewCustomerCoordinator()Lrx/Subscription;
    .locals 2

    .line 562
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->profileAttachmentsSection:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->getOnViewAllClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$lPNsjSIgt7UDgU2gAakoQdnbF7o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$lPNsjSIgt7UDgU2gAakoQdnbF7o;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 563
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachRewardsSection$51$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 614
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->rewardsSection:Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$lHVLcW2vJA9YOFdh9gyFDXrGbHg;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$lHVLcW2vJA9YOFdh9gyFDXrGbHg;-><init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;)V

    .line 615
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attachRewardsSection$53$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 618
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->rewardsSection:Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->manageClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$yssDA4VrJWYBSTwoB9kGQ-97UUM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$yssDA4VrJWYBSTwoB9kGQ-97UUM;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 619
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attachRewardsSection$55$ViewCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 621
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->rewardsSection:Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->onViewAllClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$KZFc4aS19P8tX8FLZl2KS66YqSA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$KZFc4aS19P8tX8FLZl2KS66YqSA;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    .line 622
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$bindDropDownAction$73$ViewCustomerCoordinator(Landroid/view/View;Lcom/squareup/analytics/RegisterActionName;Lrx/functions/Action0;)Lrx/Subscription;
    .locals 1

    .line 805
    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Fy0eEgBNKZ2CNdVVvA_RL6wPCn4;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$Fy0eEgBNKZ2CNdVVvA_RL6wPCn4;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;Lcom/squareup/analytics/RegisterActionName;Lrx/functions/Action0;)V

    .line 806
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bindExposedActions$74$ViewCustomerCoordinator(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;)V
    .locals 2

    .line 857
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->registerActionName:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 858
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->getExposedActionAction(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;)Lrx/functions/Action0;

    move-result-object p1

    invoke-interface {p1}, Lrx/functions/Action0;->call()V

    return-void
.end method

.method public synthetic lambda$bindExposedActions$75$ViewCustomerCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 860
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$SEolDHl5B2_LAu6VkwPFWkiEers;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$SEolDHl5B2_LAu6VkwPFWkiEers;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 861
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$ViewCustomerCoordinator(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;)V
    .locals 4

    .line 314
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$AbstractCrmScopeRunner$ContactLoadingState:[I

    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p1, v0, :cond_2

    const/4 v2, 0x2

    if-eq p1, v2, :cond_2

    const/4 v2, 0x3

    const-string v3, "Load"

    if-eq p1, v2, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 329
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_contact_loading_error:I

    .line 330
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 329
    invoke-virtual {p1, v3, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->progressBar:Landroid/view/View;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 332
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->contentContainer:Landroid/view/View;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    .line 322
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1, v3}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    .line 323
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->progressBar:Landroid/view/View;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 324
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->contentContainer:Landroid/view/View;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    .line 317
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->progressBar:Landroid/view/View;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 318
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->contentContainer:Landroid/view/View;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$11$ViewCustomerCoordinator(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;
    .locals 2

    .line 442
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->cardOnFileViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 443
    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isViewCustomerReadOnly()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 442
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->viewData(Lcom/squareup/protos/client/rolodex/Contact;Z)Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$13$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 450
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 451
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showSaveCardToCustomerScreenV2()V

    return-void
.end method

.method public synthetic lambda$null$15$ViewCustomerCoordinator(Lcom/squareup/protos/client/instruments/InstrumentSummary;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 458
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_REMOVE_CARD:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showConfirmUnlinkInstrumentDialog(Lcom/squareup/protos/client/instruments/InstrumentSummary;)V

    return-void
.end method

.method public synthetic lambda$null$18$ViewCustomerCoordinator(Lcom/squareup/protos/client/rolodex/Note;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 478
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_EDIT_NOTE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 479
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V

    return-void
.end method

.method public synthetic lambda$null$20$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 484
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showAllNotesScreen()V

    return-void
.end method

.method public synthetic lambda$null$22$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 489
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_NOTE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 490
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showCreateNoteScreen()V

    return-void
.end method

.method public synthetic lambda$null$25$ViewCustomerCoordinator(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 504
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    .line 505
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->upcomingAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    iget-object v2, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->setViewData(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/ui/crm/flow/CrmScopeType;)V

    .line 506
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->pastAppointmentsSection:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    iget-object p1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->setViewData(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/ui/crm/flow/CrmScopeType;)V

    return-void
.end method

.method public synthetic lambda$null$27$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 512
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showCreateAppointment()V

    return-void
.end method

.method public synthetic lambda$null$29$ViewCustomerCoordinator(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 519
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    iget-object v1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->appointmentId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->occurrenceStart:Lorg/threeten/bp/Instant;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showAppointmentDetail(Ljava/lang/String;Lorg/threeten/bp/Instant;)V

    return-void
.end method

.method public synthetic lambda$null$31$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 524
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showAllUpcomingAppointmentsScreen()V

    return-void
.end method

.method public synthetic lambda$null$33$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 528
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showAllPastAppointmentsScreen()V

    return-void
.end method

.method public synthetic lambda$null$36$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1

    .line 540
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_FREQUENT_ITEMS:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 541
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showAllFrequentItemsScreen()V

    return-void
.end method

.method public synthetic lambda$null$38$ViewCustomerCoordinator(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 551
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->profileAttachmentsSection:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$null$40$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1

    .line 557
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_FILES:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 558
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showUploadFileBottomSheet()V

    return-void
.end method

.method public synthetic lambda$null$42$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1

    .line 564
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_FILES:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 565
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showProfileAttachmentsScreen()V

    return-void
.end method

.method public synthetic lambda$null$44$ViewCustomerCoordinator(ZLcom/squareup/protos/client/invoice/GetMetricsResponse;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;
    .locals 1

    .line 577
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->invoicesSectionDataRenderer:Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->generateViewData(Lcom/squareup/protos/client/invoice/GetMetricsResponse;Z)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$47$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 590
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showCustomerInvoiceScreen()V

    return-void
.end method

.method public synthetic lambda$null$52$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 619
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showManageCouponsAndRewardsScreen()V

    return-void
.end method

.method public synthetic lambda$null$54$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 623
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_COUPONS:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 624
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showAllCouponsAndRewardsScreen()V

    return-void
.end method

.method public synthetic lambda$null$59$ViewCustomerCoordinator(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 650
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;->title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;->token:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showBillHistoryScreen(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$61$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 655
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_ACTIVITY:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 656
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showCustomerActivityScreen()V

    return-void
.end method

.method public synthetic lambda$null$63$ViewCustomerCoordinator(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 13

    .line 667
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 668
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 669
    iget-object v5, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v6

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    .line 670
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x1

    sget-object v11, Lcom/squareup/util/ShortTimes$MaxUnit;->DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v12, 0x1

    .line 669
    invoke-static/range {v5 .. v12}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JJZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;

    move-result-object v0

    .line 671
    new-instance v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->crm_created:I

    .line 672
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    .line 671
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 674
    :cond_0
    new-instance v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_creation_source_title:I

    .line 675
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_creation_source_unknown:I

    .line 677
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    sget-object v2, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    .line 674
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 679
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->additionalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    new-instance v6, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_additional_info_title:I

    .line 680
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;Ljava/util/List;Ljava/lang/String;)V

    .line 679
    invoke-virtual {p1, v6}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->setViewData(Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;)V

    return-void
.end method

.method public synthetic lambda$null$68$ViewCustomerCoordinator(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/Boolean;)V
    .locals 4

    .line 693
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->footerButtonContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 695
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->messageFooterButton:Lcom/squareup/noho/NohoButton;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 696
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->messageFooterButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->canMessageContact(Lcom/squareup/protos/client/rolodex/Contact;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 697
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->messageFooterButton:Lcom/squareup/noho/NohoButton;

    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$s6kr8F-HfWi4gDNXEkRJD5TlJdE;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$s6kr8F-HfWi4gDNXEkRJD5TlJdE;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->configureEdges(Lkotlin/jvm/functions/Function1;)V

    .line 701
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->messageFooterButton:Lcom/squareup/noho/NohoButton;

    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$1;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 707
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {p2, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isCustomerAddedToSale(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 708
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    sget p2, Lcom/squareup/crmviewcustomer/R$string;->crm_remove_customer_from_sale:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 709
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    sget-object p2, Lcom/squareup/noho/NohoButtonType;->TERTIARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 710
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    sget-object p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$7Jws1yPmW9gBSUUOFMerSQGZlAI;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$7Jws1yPmW9gBSUUOFMerSQGZlAI;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->configureEdges(Lkotlin/jvm/functions/Function1;)V

    .line 714
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    new-instance p2, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$2;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 720
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    sget p2, Lcom/squareup/crmviewcustomer/R$string;->crm_add_customer_title:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 721
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    sget-object p2, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 722
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    sget-object p2, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$-Ko4MjTTgDd6wIJ8Y6QQpZHlb6w;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewCustomerCoordinator$-Ko4MjTTgDd6wIJ8Y6QQpZHlb6w;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->configureEdges(Lkotlin/jvm/functions/Function1;)V

    .line 726
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->addToSaleFooterButton:Lcom/squareup/noho/NohoButton;

    new-instance p2, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$3;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$3;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;)V

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$7$ViewCustomerCoordinator(Lcom/squareup/protos/client/rolodex/Contact;)Lrx/Observable;
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->personalInformationViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 424
    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->isViewCustomerReadOnly()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 423
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->toViewData(Lcom/squareup/protos/client/rolodex/Contact;Z)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$72$ViewCustomerCoordinator(Lcom/squareup/analytics/RegisterActionName;Lrx/functions/Action0;Lkotlin/Unit;)V
    .locals 0

    .line 807
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {p3}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    .line 808
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p3, p1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 809
    invoke-interface {p2}, Lrx/functions/Action0;->call()V

    return-void
.end method

.method public synthetic lambda$null$9$ViewCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 431
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_EDIT_PERSONAL:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 432
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showUpdateCustomerV2()V

    return-void
.end method
