.class Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CustomerActivityView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->bindActivityListRow(Lcom/squareup/ui/account/view/SmartLineRow;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;

.field final synthetic val$event:Lcom/squareup/protos/client/rolodex/Event;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;Lcom/squareup/protos/client/rolodex/Event;)V
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter$1;->this$1:Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter$1;->this$1:Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerActivityView;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->presenter:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->onActivityClicked(Lcom/squareup/protos/client/rolodex/Event;Landroid/view/View;)V

    return-void
.end method
