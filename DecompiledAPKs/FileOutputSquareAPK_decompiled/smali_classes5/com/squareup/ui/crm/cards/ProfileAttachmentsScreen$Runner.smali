.class public interface abstract Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;
.super Ljava/lang/Object;
.source "ProfileAttachmentsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closePreviewScreen()V
.end method

.method public abstract closeProfileAttachmentsScreen()V
.end method

.method public abstract closeUploadScreen()V
.end method

.method public abstract deleteAttachment(Landroid/content/DialogInterface;)V
.end method

.method public abstract downloadFile(Landroid/content/Context;)V
.end method

.method public abstract getAttachmentToken()Ljava/lang/String;
.end method

.method public abstract getContactName()Ljava/lang/String;
.end method

.method public abstract getCurrentFilename()Ljava/lang/String;
.end method

.method public abstract getFilename()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFormattedDateTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
.end method

.method public abstract getLoaderResults()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getUploadState()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUploadStatusText(Landroid/view/View;Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;)Ljava/lang/CharSequence;
.end method

.method public abstract isBusy()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadImage(Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V
.end method

.method public abstract openCamera()V
.end method

.method public abstract openGallery()V
.end method

.method public abstract renameFile(Landroid/content/DialogInterface;Ljava/lang/String;)V
.end method

.method public abstract setAttachment(Lcom/squareup/protos/client/rolodex/Attachment;)V
.end method

.method public abstract setBusy(Ljava/lang/Boolean;)V
.end method

.method public abstract setFailure()V
.end method

.method public abstract setSuccess()V
.end method

.method public abstract showDeleteFileConfirmation()V
.end method

.method public abstract showOverflowBottomSheet()V
.end method

.method public abstract showPreviewScreen(Lcom/squareup/protos/client/rolodex/Attachment;)V
.end method

.method public abstract showRenameFileDialog()V
.end method

.method public abstract showUploadBottomSheet()V
.end method

.method public abstract uploadFile(Landroid/content/Context;)V
.end method
