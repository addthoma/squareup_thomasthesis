.class public final enum Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;
.super Ljava/lang/Enum;
.source "AllAppointmentsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

.field public static final enum PAST:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

.field public static final enum UPCOMING:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 25
    new-instance v0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    const/4 v1, 0x0

    const-string v2, "PAST"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->PAST:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    new-instance v0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    const/4 v2, 0x1

    const-string v3, "UPCOMING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->UPCOMING:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    .line 24
    sget-object v3, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->PAST:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->UPCOMING:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->$VALUES:[Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;
    .locals 1

    .line 24
    const-class v0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->$VALUES:[Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    return-object v0
.end method
