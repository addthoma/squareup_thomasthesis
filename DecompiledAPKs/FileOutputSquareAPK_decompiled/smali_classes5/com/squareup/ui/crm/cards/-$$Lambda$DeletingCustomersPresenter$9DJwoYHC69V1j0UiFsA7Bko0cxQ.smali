.class public final synthetic Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$9DJwoYHC69V1j0UiFsA7Bko0cxQ;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;

.field private final synthetic f$1:Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$9DJwoYHC69V1j0UiFsA7Bko0cxQ;->f$0:Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$9DJwoYHC69V1j0UiFsA7Bko0cxQ;->f$1:Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$9DJwoYHC69V1j0UiFsA7Bko0cxQ;->f$0:Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$9DJwoYHC69V1j0UiFsA7Bko0cxQ;->f$1:Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;

    check-cast p1, Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->lambda$null$1$DeletingCustomersPresenter(Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;)V

    return-void
.end method
