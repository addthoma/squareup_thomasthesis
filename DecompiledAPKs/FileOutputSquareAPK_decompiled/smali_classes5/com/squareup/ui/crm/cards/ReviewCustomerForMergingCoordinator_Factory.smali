.class public final Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;
.super Ljava/lang/Object;
.source "ReviewCustomerForMergingCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final personalInformationViewDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;->personalInformationViewDataRendererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;)Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;-><init>(Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;->personalInformationViewDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;->newInstance(Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;)Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator_Factory;->get()Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;

    move-result-object v0

    return-object v0
.end method
