.class public final Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;
.super Ljava/lang/Object;
.source "CustomerActivityHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/CustomerActivityHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;->resProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;->serverProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/http/Server;)Lcom/squareup/ui/crm/cards/CustomerActivityHelper;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;-><init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/http/Server;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/CustomerActivityHelper;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/http/Server;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;->newInstance(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/http/Server;)Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CustomerActivityHelper_Factory;->get()Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    move-result-object v0

    return-object v0
.end method
