.class public Lcom/squareup/ui/crm/cards/CustomerActivityHelper;
.super Ljava/lang/Object;
.source "CustomerActivityHelper.java"


# instance fields
.field private final baseUrl:Ljava/lang/String;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/http/Server;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->res:Lcom/squareup/util/Res;

    .line 22
    sget-object p1, Lcom/squareup/http/Endpoints$Server;->STAGING:Lcom/squareup/http/Endpoints$Server;

    invoke-virtual {p3}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    move-result-object p3

    if-ne p1, p3, :cond_0

    sget p1, Lcom/squareup/crmscreens/R$string;->staging_base_url:I

    .line 23
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/crmscreens/R$string;->prod_base_url:I

    .line 24
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->baseUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isActivityClickable(Lcom/squareup/protos/client/rolodex/Event;)Z
    .locals 3

    .line 28
    sget-object v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$EventType:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    .line 36
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v1

    return p1

    :cond_0
    return v1

    .line 31
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v1

    return p1
.end method

.method public onActivityClicked(Lcom/squareup/protos/client/rolodex/Event;)V
    .locals 4

    .line 41
    sget-object v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$EventType:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    const-string v2, "base_url"

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 68
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->square_relative_url:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->baseUrl:Ljava/lang/String;

    .line 72
    invoke-virtual {v1, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    const-string v2, "link_relative_url"

    .line 73
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 71
    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->loyalty_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->baseUrl:Ljava/lang/String;

    .line 63
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 55
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->feedback_url:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->baseUrl:Ljava/lang/String;

    .line 56
    invoke-virtual {v1, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    const-string v2, "conversation_token"

    .line 57
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 55
    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_0

    .line 46
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$string;->transactions_url:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    const-string v2, "payment_token"

    .line 47
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 46
    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
