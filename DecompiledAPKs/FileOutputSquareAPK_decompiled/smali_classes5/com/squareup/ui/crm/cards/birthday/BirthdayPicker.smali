.class public Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;
.super Landroid/widget/LinearLayout;
.source "BirthdayPicker.java"


# static fields
.field private static final KEY_SAVED_YEAR:Ljava/lang/String; = "savedYear"

.field private static final LEAP_YEAR:I = 0x7d0

.field private static final MIN_YEAR:I = 0x767

.field private static final MIN_YEARS_OLD:I = 0xd


# instance fields
.field private datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

.field private hideYearAnimator:Landroid/animation/Animator;

.field private provideYear:Landroid/widget/LinearLayout;

.field private provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

.field private savedYear:I

.field private final shortAnimTimeMs:I

.field private showYearAnimator:Landroid/animation/Animator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const/high16 v0, 0x10e0000

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->shortAnimTimeMs:I

    .line 52
    sget p2, Lcom/squareup/crmscreens/R$layout;->crm_birthday_picker_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Lcom/squareup/marin/widgets/MarinCheckBox;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Landroid/animation/Animator;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->hideYearAnimator:Landroid/animation/Animator;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Landroid/animation/Animator;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->showYearAnimator:Landroid/animation/Animator;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->onMeasuredOnce()V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)I
    .locals 0

    .line 26
    iget p0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->savedYear:I

    return p0
.end method

.method static synthetic access$402(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;I)I
    .locals 0

    .line 26
    iput p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->savedYear:I

    return p1
.end method

.method static synthetic access$500(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Lcom/squareup/marin/widgets/MarinDatePicker;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 234
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_birthday_picker_provide_year:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYear:Landroid/widget/LinearLayout;

    .line 235
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_birthday_picker_provide_year_check:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinCheckBox;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    .line 236
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_birthday_picker_date_picker:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinDatePicker;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    return-void
.end method

.method private buildHideYearAnimator(I)Landroid/animation/Animator;
    .locals 7

    .line 172
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 173
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    const/4 v1, 0x2

    new-array v2, v1, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    int-to-float p1, p1

    const/4 v4, 0x1

    aput p1, v2, v4

    .line 174
    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    const-string/jumbo p1, "translationX"

    .line 175
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 177
    new-instance p1, Landroid/animation/ObjectAnimator;

    invoke-direct {p1}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 178
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYearPicker()Landroid/widget/NumberPicker;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    new-array v2, v1, [F

    .line 179
    fill-array-data v2, :array_0

    invoke-virtual {p1, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    const-string v2, "alpha"

    .line 180
    invoke-virtual {p1, v2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 182
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    new-array v5, v1, [Landroid/animation/Animator;

    aput-object v0, v5, v3

    aput-object p1, v5, v4

    .line 183
    invoke-virtual {v2, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 184
    iget p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->shortAnimTimeMs:I

    int-to-long v5, p1

    invoke-virtual {v2, v5, v6}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 186
    new-instance p1, Landroid/animation/ObjectAnimator;

    invoke-direct {p1}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYearPicker()Landroid/widget/NumberPicker;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    new-array v0, v4, [F

    const/high16 v5, 0x41000000    # 8.0f

    aput v5, v0, v3

    .line 188
    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    const-string/jumbo v0, "visibility"

    .line 189
    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 191
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 192
    new-instance v5, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-array v1, v1, [Landroid/animation/Animator;

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    .line 193
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    return-object v0

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private buildShowYearAnimator(I)Landroid/animation/Animator;
    .locals 7

    .line 198
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 199
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 200
    iget v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->shortAnimTimeMs:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v2, v1, [F

    int-to-float p1, p1

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 p1, 0x0

    const/4 v4, 0x1

    aput p1, v2, v4

    .line 201
    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    const-string/jumbo v2, "translationX"

    .line 202
    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 204
    new-instance v2, Landroid/animation/ObjectAnimator;

    invoke-direct {v2}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 205
    iget-object v5, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v5}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYearPicker()Landroid/widget/NumberPicker;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 206
    iget v5, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->shortAnimTimeMs:I

    int-to-long v5, v5

    invoke-virtual {v2, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-array v5, v1, [F

    .line 207
    fill-array-data v5, :array_0

    invoke-virtual {v2, v5}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    const-string v5, "alpha"

    .line 208
    invoke-virtual {v2, v5}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 210
    new-instance v5, Landroid/animation/ObjectAnimator;

    invoke-direct {v5}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 211
    iget-object v6, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v6}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYearPicker()Landroid/widget/NumberPicker;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    new-array v6, v4, [F

    aput p1, v6, v3

    .line 212
    invoke-virtual {v5, v6}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    const-string/jumbo p1, "visibility"

    .line 213
    invoke-virtual {v5, p1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 215
    new-instance p1, Landroid/animation/AnimatorSet;

    invoke-direct {p1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 216
    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v6}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const/4 v6, 0x3

    new-array v6, v6, [Landroid/animation/Animator;

    aput-object v0, v6, v3

    aput-object v2, v6, v4

    aput-object v5, v6, v1

    .line 217
    invoke-virtual {p1, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    return-object p1

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static newCalendarForNowMinusYears(I)Ljava/util/Calendar;
    .locals 3

    .line 228
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    .line 229
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    return-object v0
.end method

.method private static newCalendarForStartOf(I)Ljava/util/Calendar;
    .locals 8

    .line 222
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move v1, p0

    .line 223
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    return-object v7
.end method

.method private onMeasuredOnce()V
    .locals 3

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinDatePicker;->setMinimumWidth(I)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYearPicker()Landroid/widget/NumberPicker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinDatePicker;->getInnerMargin()I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinDatePicker;->isYearLeftmost()Z

    move-result v1

    if-eqz v1, :cond_0

    neg-int v0, v0

    .line 118
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->buildHideYearAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->hideYearAnimator:Landroid/animation/Animator;

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->hideYearAnimator:Landroid/animation/Animator;

    new-instance v2, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$3;-><init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 135
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->buildShowYearAnimator(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->showYearAnimator:Landroid/animation/Animator;

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->showYearAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$4;-><init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinCheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->hideYearAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->hideYearAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    :cond_1
    return-void
.end method


# virtual methods
.method public getBirthday()Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;
    .locals 7

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->clearFocus()V

    .line 167
    new-instance v0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinCheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYear()I

    move-result v1

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->savedYear:I

    :goto_0
    move v2, v1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    .line 168
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinDatePicker;->getMonth()I

    move-result v3

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinDatePicker;->getDayOfMonth()I

    move-result v4

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinCheckBox;->isChecked()Z

    move-result v6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;-><init>(IIIZZ)V

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$BirthdayPicker(Landroid/widget/CalendarView;III)V
    .locals 0

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->isChecked()Z

    move-result p1

    if-nez p1, :cond_0

    const/16 p1, 0x7d0

    if-eq p2, p1, :cond_0

    .line 65
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {p2, p1, p3, p4}, Lcom/squareup/marin/widgets/MarinDatePicker;->updateDate(III)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->bindViews()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    const/16 v1, 0x767

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->newCalendarForStartOf(I)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinDatePicker;->setMinDate(J)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    const/16 v1, 0xd

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->newCalendarForNowMinusYears(I)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinDatePicker;->setMaxDate(J)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/birthday/-$$Lambda$BirthdayPicker$tqD6y2ZbkoJrphSk3t4EEkqtm_o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/birthday/-$$Lambda$BirthdayPicker$tqD6y2ZbkoJrphSk3t4EEkqtm_o;-><init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V

    invoke-virtual {v0, v1}, Landroid/widget/CalendarView;->setOnDateChangeListener(Landroid/widget/CalendarView$OnDateChangeListener;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYear:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;-><init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    new-instance v1, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$2;-><init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 101
    instance-of v0, p1, Lcom/squareup/mortar/BundleSavedState;

    if-eqz v0, :cond_0

    .line 102
    check-cast p1, Lcom/squareup/mortar/BundleSavedState;

    .line 103
    iget-object v0, p1, Lcom/squareup/mortar/BundleSavedState;->bundle:Landroid/os/Bundle;

    const-string v1, "savedYear"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->savedYear:I

    .line 104
    invoke-virtual {p1}, Lcom/squareup/mortar/BundleSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 106
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 95
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 96
    iget v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->savedYear:I

    const-string v2, "savedYear"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    new-instance v1, Lcom/squareup/mortar/BundleSavedState;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/squareup/mortar/BundleSavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public setBirthday(Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;)V
    .locals 4

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    iget v1, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->year:I

    iget v2, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->month:I

    iget v3, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->day:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/marin/widgets/MarinDatePicker;->updateDate(III)V

    .line 160
    iget-boolean p1, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->yearIsKnown:Z

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinCheckBox;->isChecked()Z

    move-result v0

    xor-int/2addr p1, v0

    if-eqz p1, :cond_0

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->provideYearCheckbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->toggle()V

    :cond_0
    return-void
.end method
