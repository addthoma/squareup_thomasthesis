.class final Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$actionBar$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AdjustPointsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/loyalty/PointsTermsFormatter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$actionBar$2;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$actionBar$2;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getView$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$actionBar$2;->invoke()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method
