.class public Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "UpdateLoyaltyPhoneScreen.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Component;,
        Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneScreen$1Q1Zwtpk228SZj7RjKYeiSootn4;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneScreen$1Q1Zwtpk228SZj7RjKYeiSootn4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;
    .locals 1

    .line 73
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 74
    new-instance v0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 68
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 39
    const-class v0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Component;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Component;->updateLoyaltyPhoneCoordinator()Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 35
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_update_loyalty_phone:I

    return v0
.end method
