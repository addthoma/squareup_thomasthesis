.class public interface abstract Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;
.super Ljava/lang/Object;
.source "CustomerInvoiceScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeCustomerInvoiceScreen()V
.end method

.method public abstract invoiceResults()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract loadMoreInvoices(Ljava/lang/Integer;)V
.end method

.method public abstract loadingInvoices()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end method

.method public abstract showInvoiceReadOnlyScreen(Ljava/lang/String;)V
.end method
