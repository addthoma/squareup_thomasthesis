.class public interface abstract Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;
.super Ljava/lang/Object;
.source "CustomerSaveCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelSaveCardToCustomerScreen()V
.end method

.method public abstract getContactForSaveCardScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;
.end method

.method public abstract getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;
.end method

.method public abstract isCrmDipSupported()Z
.end method

.method public abstract onFailedDip()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onFailedSwipe()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onSuccessfulDip()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onSuccessfulSwipe()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showCustomerEmailScreen()V
.end method
