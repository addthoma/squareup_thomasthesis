.class Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CustomerSaveCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/CustomerSaveCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cardConverter:Lcom/squareup/payment/CardConverter;

.field private final crmDipSupported:Ljava/lang/Boolean;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private firstNameValid:Z

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private lastNameValid:Z

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/CardConverter;Lcom/squareup/hudtoaster/HudToaster;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 129
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 124
    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->firstNameValid:Z

    .line 125
    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->lastNameValid:Z

    .line 130
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    .line 131
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 132
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 133
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 134
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 135
    iput-object p6, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 136
    iput-object p7, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->cardConverter:Lcom/squareup/payment/CardConverter;

    .line 137
    iput-object p8, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 138
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->isCrmDipSupported()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->crmDipSupported:Ljava/lang/Boolean;

    return-void
.end method

.method private cardFromCardInfo(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/Card;
    .locals 2

    .line 269
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardInfo;->getLast4()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardInfo;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 270
    :goto_0
    new-instance v1, Lcom/squareup/Card$Builder;

    invoke-direct {v1}, Lcom/squareup/Card$Builder;-><init>()V

    .line 271
    invoke-virtual {v1, v0}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object v0

    .line 272
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->name(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    .line 273
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardInfo;->getLast4()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 274
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    return-object p1
.end method

.method private getFormattedBrandAndUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 2

    .line 321
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v0

    .line 322
    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    sget-object p1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    invoke-static {p1, v0}, Lcom/squareup/text/Cards;->formattedMask(Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {v0, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$98RsQ21peBQAvHkz2nWHYGF6xIA(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onSuccessfulSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public static synthetic lambda$H8-LCDnfMvZIUF-BpOZyI_dPelg(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onSuccessfulDip(Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;)V

    return-void
.end method

.method public static synthetic lambda$f3qqPUeY3WxsYAQGQlIGuSfXnic(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->verifyAndLinkCard()V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/squareup/Card$PanWarning;)Lcom/squareup/Card;
    .locals 0

    const/4 p0, 0x0

    return-object p0
.end method

.method private onFailedDip()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->crmDipSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "dip is not supported"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->DIP_FAILED_COF_CRM:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method private onFailedSwipe()V
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->clearCard()V

    .line 330
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_COF_CRM:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method private onFieldChanged()V
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->firstNameValid:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->lastNameValid:Z

    if-eqz v0, :cond_0

    .line 348
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setActionBarPrimaryButtonEnabled(Z)V

    goto :goto_0

    .line 350
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setActionBarPrimaryButtonEnabled(Z)V

    :goto_0
    return-void
.end method

.method private onSuccessfulCardReaderEvent(Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 3

    .line 288
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;

    if-nez v0, :cond_0

    return-void

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v1

    .line 294
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v2, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 297
    invoke-virtual {p1}, Lcom/squareup/Card;->toBuilder()Lcom/squareup/Card$Builder;

    move-result-object p1

    sget-object v2, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 298
    invoke-virtual {p1, v2}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 299
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    .line 301
    :cond_1
    invoke-virtual {v1, p1, p2, p3}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->setCard(Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 303
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onFieldChanged()V

    .line 305
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p2, p3}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p2

    .line 306
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->getFormattedBrandAndUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object p3

    .line 305
    invoke-virtual {v0, p2, p3}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->showCardToSave(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 308
    invoke-virtual {p1}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/text/Cards;->parseCardOwnerName(Ljava/lang/String;)Lcom/squareup/text/Cards$CardOwnerName;

    move-result-object p2

    .line 311
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p1

    sget-object p3, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-eq p1, p3, :cond_2

    if-eqz p2, :cond_2

    .line 312
    iget-object p1, p2, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setFirstName(Ljava/lang/CharSequence;)V

    .line 313
    iget-object p1, p2, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setLastName(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method private onSuccessfulDip(Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;)V
    .locals 2

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->crmDipSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "dip is not supported"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 262
    iget-object v0, p1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->cardFromCardInfo(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/Card;

    move-result-object v0

    .line 263
    iget-object v1, p1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iget-object p1, p1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->encyptedCardData:[B

    invoke-static {v1, p1}, Lcom/squareup/payment/CardConverterUtils;->createCardDataFromBytes(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    .line 265
    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onSuccessfulCardReaderEvent(Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    return-void
.end method

.method private onSuccessfulSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 283
    iget-object v0, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->cardConverter:Lcom/squareup/payment/CardConverter;

    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-virtual {v1, p1}, Lcom/squareup/payment/CardConverter;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onSuccessfulCardReaderEvent(Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    return-void
.end method

.method private verifyAndLinkCard()V
    .locals 5

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "Cannot verify card without a card."

    .line 336
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 338
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    .line 339
    invoke-interface {v4}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->getContactForSaveCardScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v0

    invoke-direct {v2, v3, v4, v0}, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 338
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->showCustomerEmailScreen()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$listenForDipEvents$12$CustomerSaveCardScreen$Presenter()Lrx/Subscription;
    .locals 2

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->onSuccessfulDip()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$H8-LCDnfMvZIUF-BpOZyI_dPelg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$H8-LCDnfMvZIUF-BpOZyI_dPelg;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForDipEvents$14$CustomerSaveCardScreen$Presenter()Lrx/Subscription;
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->onFailedDip()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$0mA5HTSrFvssZgGx4tLiwjfp8GQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$0mA5HTSrFvssZgGx4tLiwjfp8GQ;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$1$CustomerSaveCardScreen$Presenter(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onFailedSwipe()V

    return-void
.end method

.method public synthetic lambda$null$10$CustomerSaveCardScreen$Presenter(Lcom/squareup/ui/crm/flow/SaveCardSharedState;Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lkotlin/Unit;)V
    .locals 1

    .line 218
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 220
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p3, v0}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p3

    .line 221
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->getFormattedBrandAndUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object p1

    .line 220
    invoke-virtual {p2, p3, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->showCardToSave(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$13$CustomerSaveCardScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 257
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onFailedDip()V

    return-void
.end method

.method public synthetic lambda$null$4$CustomerSaveCardScreen$Presenter(Lcom/squareup/ui/crm/flow/SaveCardSharedState;Lcom/squareup/Card;)V
    .locals 2

    if-nez p2, :cond_0

    .line 188
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->clearCard()V

    goto :goto_0

    .line 190
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/Card;->isManual()Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->cardConverter:Lcom/squareup/payment/CardConverter;

    invoke-virtual {v0, p2}, Lcom/squareup/payment/CardConverter;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->setCard(Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 193
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onFieldChanged()V

    return-void
.end method

.method public synthetic lambda$null$6$CustomerSaveCardScreen$Presenter(Lcom/squareup/ui/crm/flow/SaveCardSharedState;Ljava/lang/String;)V
    .locals 1

    .line 200
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->firstNameValid:Z

    .line 201
    iput-object p2, p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->firstName:Ljava/lang/String;

    .line 202
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onFieldChanged()V

    return-void
.end method

.method public synthetic lambda$null$8$CustomerSaveCardScreen$Presenter(Lcom/squareup/ui/crm/flow/SaveCardSharedState;Ljava/lang/String;)V
    .locals 1

    .line 209
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->lastNameValid:Z

    .line 210
    iput-object p2, p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->lastName:Ljava/lang/String;

    .line 211
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onFieldChanged()V

    return-void
.end method

.method public synthetic lambda$onLoad$0$CustomerSaveCardScreen$Presenter()Lrx/Subscription;
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->onSuccessfulSwipe()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$98RsQ21peBQAvHkz2nWHYGF6xIA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$98RsQ21peBQAvHkz2nWHYGF6xIA;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    .line 173
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$11$CustomerSaveCardScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)Lrx/Subscription;
    .locals 2

    .line 216
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->onCardEntryDone()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$xaZqhDHnB-1m9vtUolGcB_q_egM;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$xaZqhDHnB-1m9vtUolGcB_q_egM;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/flow/SaveCardSharedState;Lcom/squareup/ui/crm/cards/CustomerSaveCardView;)V

    .line 217
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$2$CustomerSaveCardScreen$Presenter()Lrx/Subscription;
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->onFailedSwipe()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$HkHHsJ6waeq3joaD2lk7vXYtWwE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$HkHHsJ6waeq3joaD2lk7vXYtWwE;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    .line 176
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$5$CustomerSaveCardScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)Lrx/Subscription;
    .locals 2

    .line 183
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->onCardValid()Lrx/Observable;

    move-result-object v0

    .line 185
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->onCardInvalid()Lrx/Observable;

    move-result-object p1

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$DXaxobCk611sHGqWE1OgkHgDx_8;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$DXaxobCk611sHGqWE1OgkHgDx_8;

    invoke-virtual {p1, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 182
    invoke-static {v0, p1}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$x5CubB9VU43bzja4K9psGgst0xM;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$x5CubB9VU43bzja4K9psGgst0xM;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)V

    .line 186
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$CustomerSaveCardScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)Lrx/Subscription;
    .locals 1

    .line 198
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->firstName()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$WsjYFU3xZ4zRHsmYFA1gcTGb3Hw;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$WsjYFU3xZ4zRHsmYFA1gcTGb3Hw;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)V

    .line 199
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$9$CustomerSaveCardScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)Lrx/Subscription;
    .locals 1

    .line 207
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->lastName()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$800Cdg52fpvQSQWHrGnPS-hz9lM;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$800Cdg52fpvQSQWHrGnPS-hz9lM;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)V

    .line 208
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method listenForDipEvents(Landroid/view/View;)V
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->crmDipSupported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 253
    :cond_0
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$kkml0JvNT4DvDSD_Yv1pLP4IFUk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$kkml0JvNT4DvDSD_Yv1pLP4IFUk;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 256
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$Tck9FljBgt2HC2NOMk8tLdcEPBg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$Tck9FljBgt2HC2NOMk8tLdcEPBg;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->cancelSaveCardToCustomerScreen()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 147
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 148
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;

    .line 150
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/ui/crm/R$string;->crm_cardonfile_savecard_header:I

    .line 151
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$BoEnpkJiDkPUQrTCwXTVW6H2TCM;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/cards/-$$Lambda$BoEnpkJiDkPUQrTCwXTVW6H2TCM;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;)V

    .line 152
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 153
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->next:I

    .line 154
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$f3qqPUeY3WxsYAQGQlIGuSfXnic;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$f3qqPUeY3WxsYAQGQlIGuSfXnic;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    .line 155
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 156
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 150
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 158
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCodeOrNull()Lcom/squareup/CountryCode;

    move-result-object v1

    .line 159
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->initCardEditor(Lcom/squareup/CountryCode;)V

    .line 160
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->crmDipSupported:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    sget v1, Lcom/squareup/common/strings/R$string;->ce_card_number_only_hint:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setCardEditorHint(I)V

    .line 162
    sget v1, Lcom/squareup/ui/crm/R$string;->ce_save_card_to_customer_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setTitle(I)V

    .line 163
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->showFooter()V

    goto :goto_0

    .line 165
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->hideTitle()V

    .line 166
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->hideFooter()V

    .line 169
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v1

    .line 172
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$qP_iXZVlK0D7QbyViGCa73Lu5JA;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$qP_iXZVlK0D7QbyViGCa73Lu5JA;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    invoke-static {v0, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 175
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$9RzSw872qfUn-QwALQTYqhO4zLY;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$9RzSw872qfUn-QwALQTYqhO4zLY;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;)V

    invoke-static {v0, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 178
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->listenForDipEvents(Landroid/view/View;)V

    .line 181
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$atleIwlA3GwtBHskjyGLlfYd9AE;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$atleIwlA3GwtBHskjyGLlfYd9AE;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)V

    invoke-static {v0, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 197
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$2XaU8W77RrdPyoHALCScpXplxkQ;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$2XaU8W77RrdPyoHALCScpXplxkQ;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)V

    invoke-static {v0, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 206
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$r7KkYv6UzkQXblaG5RMNFqs4KgA;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$r7KkYv6UzkQXblaG5RMNFqs4KgA;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)V

    invoke-static {v0, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 215
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$buiGDihl4pERKlfcVU_2q-a7pAY;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerSaveCardScreen$Presenter$buiGDihl4pERKlfcVU_2q-a7pAY;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerSaveCardView;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)V

    invoke-static {v0, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 225
    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 227
    invoke-virtual {v2}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v3, v4}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v3

    .line 228
    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->getFormattedBrandAndUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v2

    .line 227
    invoke-virtual {v0, v3, v2}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->showCardToSave(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 230
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->showCardEntryField()V

    .line 233
    :goto_1
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;->getContactForSaveCardScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 235
    iget-object v3, v2, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v3, :cond_3

    if-nez p1, :cond_2

    .line 238
    iget-object p1, v2, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    .line 239
    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    iput-object v2, v1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->firstName:Ljava/lang/String;

    .line 240
    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    iput-object v2, v1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->lastName:Ljava/lang/String;

    .line 241
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    iput-object p1, v1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->email:Ljava/lang/String;

    .line 243
    :cond_2
    iget-object p1, v1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->firstName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setFirstName(Ljava/lang/CharSequence;)V

    .line 244
    iget-object p1, v1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->lastName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->setLastName(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method
