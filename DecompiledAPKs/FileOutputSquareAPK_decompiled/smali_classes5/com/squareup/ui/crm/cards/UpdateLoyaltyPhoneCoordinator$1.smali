.class Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "UpdateLoyaltyPhoneCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;

.field final synthetic val$actionBar:Lcom/squareup/marin/widgets/MarinActionBar;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;->this$0:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;->val$actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 79
    invoke-super {p0, p1}, Lcom/squareup/debounce/DebouncedTextWatcher;->doAfterTextChanged(Landroid/text/Editable;)V

    .line 80
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;->this$0:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->access$000(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;)Lcom/squareup/text/PhoneNumberHelper;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;->val$actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 84
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;->this$0:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->access$100(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;->this$0:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->access$100(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;->val$actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    :cond_1
    :goto_0
    return-void
.end method
