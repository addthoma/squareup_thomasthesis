.class public Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;
.super Landroid/widget/LinearLayout;
.source "ResolveDuplicatesCardView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private mergeProposals:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

.field presenter:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const-class p2, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Component;->inject(Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;)V

    return-void
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method mergeProposals()Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->mergeProposals:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->presenter:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->presenter:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 29
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 30
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 31
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_merge_proposal_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->mergeProposals:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

    return-void
.end method
