.class Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;
.super Lmortar/ViewPresenter;
.source "ContactListPresenterV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_PAGE_SIZE:I = 0x32

.field static final NEAR_END_OF_LIST:I = 0x32


# instance fields
.field private bundleKey:Ljava/lang/String;

.field private checkedByDefault:Z

.field private contactCount:I

.field private contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation
.end field

.field private groupToken:Ljava/lang/String;

.field private inMultiSelectMode:Z

.field private loadedContactMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final locale:Ljava/util/Locale;

.field private final onCheckedChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onNearEndOfLists:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

.field private final res:Lcom/squareup/util/Res;

.field private selectedContactCount:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private showGreyHeaderRows:Z

.field private final xoredContactTokens:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 84
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 58
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onNearEndOfLists:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 59
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->contacts:Ljava/util/List;

    .line 60
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->loadedContactMap:Ljava/util/Map;

    const/4 v0, 0x0

    .line 63
    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->showGreyHeaderRows:Z

    .line 66
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onCheckedChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 67
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->selectedContactCount:Lrx/Observable;

    .line 72
    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->checkedByDefault:Z

    .line 82
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->res:Lcom/squareup/util/Res;

    .line 86
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->locale:Ljava/util/Locale;

    .line 87
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    return-void
.end method

.method private bindBottomRow(Lcom/squareup/ui/crm/rows/ContactListBottomRow;Lcom/squareup/crm/RolodexContactLoader;)V
    .locals 1

    .line 310
    new-instance v0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$07WnyLAdXO_CvU4s6bqWa6h_WUY;

    invoke-direct {v0, p2, p1}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$07WnyLAdXO_CvU4s6bqWa6h_WUY;-><init>(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/ui/crm/rows/ContactListBottomRow;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private isChecked(Ljava/lang/String;)Z
    .locals 2

    .line 292
    iget-boolean v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->checkedByDefault:Z

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v0

    return p1
.end method

.method static synthetic lambda$bindBottomRow$15(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/ui/crm/rows/ContactListBottomRow;)Lrx/Subscription;
    .locals 2

    .line 312
    invoke-virtual {p0}, Lcom/squareup/crm/RolodexContactLoader;->progress()Lrx/Observable;

    move-result-object v0

    .line 313
    invoke-virtual {p0}, Lcom/squareup/crm/RolodexContactLoader;->failure()Lrx/Observable;

    move-result-object p0

    sget-object v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$v65dFWD_FG23JgqOvJhQKTA3Aj0;->INSTANCE:Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$v65dFWD_FG23JgqOvJhQKTA3Aj0;

    .line 311
    invoke-static {v0, p0, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p0

    .line 316
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$N5v4qhUaaV3ZGtD3iDv5vXVSrXU;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$N5v4qhUaaV3ZGtD3iDv5vXVSrXU;-><init>(Lcom/squareup/ui/crm/rows/ContactListBottomRow;)V

    .line 317
    invoke-virtual {p0, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$init$5(Lrx/subscriptions/CompositeSubscription;)Lrx/Subscription;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/crm/RolodexContactLoader;Lkotlin/Unit;)V
    .locals 0

    .line 126
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/16 p1, 0x32

    .line 127
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/crm/RolodexContactLoader;->loadMore(Ljava/lang/Integer;)V

    return-void
.end method

.method static synthetic lambda$null$13(Lcom/squareup/util/Optional;Lcom/squareup/util/Optional;)Ljava/lang/Boolean;
    .locals 0

    .line 315
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$14(Lcom/squareup/ui/crm/rows/ContactListBottomRow;Ljava/lang/Boolean;)V
    .locals 1

    .line 318
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/ContactListBottomRow;->showProgress(Z)V

    .line 319
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/rows/ContactListBottomRow;->showMessage(Z)V

    return-void
.end method

.method static synthetic lambda$null$6(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .line 185
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sub-int/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method bindContact(Lcom/squareup/ui/crm/rows/CustomerUnitRow;I)V
    .locals 2

    .line 226
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getContact(I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    .line 230
    new-instance v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$QEG64Mz59qG5g8WphnBblWFwj-8;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$QEG64Mz59qG5g8WphnBblWFwj-8;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;Lcom/squareup/ui/crm/rows/CustomerUnitRow;Lcom/squareup/protos/client/rolodex/Contact;I)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method bindHeader(Landroid/view/View;I)V
    .locals 1

    .line 215
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getContact(I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p2

    .line 216
    iget-boolean v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->showGreyHeaderRows:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 217
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 220
    :cond_0
    sget v0, Lcom/squareup/crm/R$id;->crm_list_header_row_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 221
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getHeaderValue(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method checkedByDefault()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onCheckedChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 265
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$_vOz7ZMDqFcR4PpXzuc04oQls30;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$_vOz7ZMDqFcR4PpXzuc04oQls30;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;)V

    .line 266
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method getContact(I)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 3

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->contacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 195
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 196
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    return-object p1

    .line 199
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr p1, v1

    goto :goto_0

    .line 202
    :cond_1
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method getContactCount()I
    .locals 1

    .line 206
    iget v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->contactCount:I

    return v0
.end method

.method getHeaderId(I)J
    .locals 2

    .line 210
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getContact(I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    .line 211
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getHeaderValue(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method getHeaderValue(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->locale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getInitials(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 298
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_default_display_header_label:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 301
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->bundleKey:Ljava/lang/String;

    return-object v0
.end method

.method public getMultiSelectedContactSet()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;>;"
        }
    .end annotation

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->selectedContactCount:Lrx/Observable;

    new-instance v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$n5GJa1CctZ4XNvTfni8Vd_i0yQA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$n5GJa1CctZ4XNvTfni8Vd_i0yQA;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;)V

    .line 275
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method init(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;IZ)V
    .locals 1

    .line 120
    sget-object v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {p2, v0}, Lcom/squareup/crm/RolodexContactLoader;->setSortType(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)V

    .line 121
    iput-boolean p5, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->inMultiSelectMode:Z

    .line 123
    new-instance p5, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$NxZ5QW7MhSmdx-IPaqMKPIqkxPQ;

    invoke-direct {p5, p0, p2}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$NxZ5QW7MhSmdx-IPaqMKPIqkxPQ;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;Lcom/squareup/crm/RolodexContactLoader;)V

    invoke-static {p1, p5}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 130
    new-instance p5, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-direct {p5, p4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    .line 132
    new-instance p4, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;

    invoke-direct {p4, p0, p2, p1, p5}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-static {p1, p4}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-eqz p3, :cond_0

    .line 177
    new-instance p4, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p4}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    .line 178
    new-instance p5, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$KoHHo7yILUjhKL6GoQQs2Z0Ecr8;

    invoke-direct {p5, p4}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$KoHHo7yILUjhKL6GoQQs2Z0Ecr8;-><init>(Lrx/subscriptions/CompositeSubscription;)V

    invoke-static {p1, p5}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 181
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->checkedByDefault()Lrx/Observable;

    move-result-object p1

    const/4 p5, 0x0

    .line 182
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p5

    invoke-virtual {p1, p5}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    new-instance p5, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$jTd4cYCCMl_s771jukH7BvSOb88;

    invoke-direct {p5, p0, p2, p3, p4}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$jTd4cYCCMl_s771jukH7BvSOb88;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lrx/subscriptions/CompositeSubscription;)V

    .line 183
    invoke-virtual {p1, p5}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 187
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 188
    invoke-virtual {p1, p2}, Lrx/Observable;->replay(I)Lrx/observables/ConnectableObservable;

    move-result-object p1

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p3, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$LCFL_O8Smpcyj8Fwyue7A61jTJc;

    invoke-direct {p3, p4}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$LCFL_O8Smpcyj8Fwyue7A61jTJc;-><init>(Lrx/subscriptions/CompositeSubscription;)V

    .line 189
    invoke-virtual {p1, p2, p3}, Lrx/observables/ConnectableObservable;->autoConnect(ILrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->selectedContactCount:Lrx/Observable;

    :cond_0
    return-void
.end method

.method public synthetic lambda$bindContact$9$ContactListPresenterV2(Lcom/squareup/ui/crm/rows/CustomerUnitRow;Lcom/squareup/protos/client/rolodex/Contact;I)Lrx/Subscription;
    .locals 2

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onCheckedChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 232
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$U1v-Z8uI3O687hyIYRejxXy32Tk;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$U1v-Z8uI3O687hyIYRejxXy32Tk;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;Lcom/squareup/ui/crm/rows/CustomerUnitRow;Lcom/squareup/protos/client/rolodex/Contact;I)V

    .line 233
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$checkedByDefault$11$ContactListPresenterV2(Lkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0

    .line 266
    iget-boolean p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->checkedByDefault:Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getMultiSelectedContactSet$12$ContactListPresenterV2(Ljava/lang/Integer;)Lkotlin/Pair;
    .locals 4

    .line 275
    new-instance v0, Lkotlin/Pair;

    iget-boolean v1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->checkedByDefault:Z

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->groupToken:Ljava/lang/String;

    .line 276
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    invoke-static {v1, v2, p1, v3}, Lcom/squareup/crm/util/RolodexContactHelper;->buildContactSet(ZLjava/lang/String;ILjava/util/Collection;)Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->loadedContactMap:Ljava/util/Map;

    invoke-direct {v0, p1, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public synthetic lambda$init$1$ContactListPresenterV2(Lcom/squareup/crm/RolodexContactLoader;)Lrx/Subscription;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onNearEndOfLists:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {v0}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$R3N6GJEoTTqgz0Opvr3h0ER0cfg;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$R3N6GJEoTTqgz0Opvr3h0ER0cfg;-><init>(Lcom/squareup/crm/RolodexContactLoader;)V

    .line 125
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$init$4$ContactListPresenterV2(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Ljava/util/concurrent/atomic/AtomicReference;)Lrx/Subscription;
    .locals 2

    .line 133
    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoader;->results()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$4qSwI9H1scqk5IQuuiEZcSDgZZ8;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$4qSwI9H1scqk5IQuuiEZcSDgZZ8;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/crm/RolodexContactLoader;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 134
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$init$7$ContactListPresenterV2(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lrx/subscriptions/CompositeSubscription;Ljava/lang/Boolean;)Lrx/Observable;
    .locals 1

    .line 183
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p4

    if-eqz p4, :cond_0

    iget-object p4, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->groupToken:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$LCFL_O8Smpcyj8Fwyue7A61jTJc;

    invoke-direct {v0, p3}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$LCFL_O8Smpcyj8Fwyue7A61jTJc;-><init>(Lrx/subscriptions/CompositeSubscription;)V

    .line 184
    invoke-static {p1, p2, p4, v0}, Lcom/squareup/crm/RolodexContactLoaderHelper;->getTotalCountFound(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Ljava/lang/String;Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    .line 185
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactCount()Lrx/Observable;

    move-result-object p2

    sget-object p3, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$m9qZ7vek-uG2_SwokoW9VND8K4s;->INSTANCE:Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$m9qZ7vek-uG2_SwokoW9VND8K4s;

    .line 184
    invoke-static {p1, p2, p3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactCount()Lrx/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public synthetic lambda$null$2$ContactListPresenterV2(Lcom/squareup/crm/RolodexContactLoader;Landroid/view/View;)V
    .locals 0

    .line 151
    check-cast p2, Lcom/squareup/ui/crm/rows/ContactListBottomRow;

    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->bindBottomRow(Lcom/squareup/ui/crm/rows/ContactListBottomRow;Lcom/squareup/crm/RolodexContactLoader;)V

    return-void
.end method

.method public synthetic lambda$null$3$ContactListPresenterV2(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/crm/RolodexContactLoader;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)V
    .locals 5

    .line 135
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 136
    iget-object v0, p4, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->contacts:Ljava/util/List;

    .line 137
    invoke-virtual {p4}, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->itemCount()I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->contactCount:I

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->contacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 140
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    .line 141
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->loadedContactMap:Ljava/util/Map;

    iget-object v4, v2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 148
    :cond_1
    iget-boolean v0, p4, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->hasMore:Z

    if-nez v0, :cond_2

    .line 149
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->hideBottomRow()Z

    move-result p2

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$PnNhZiJwjaSMsjKImy-7oC2dujQ;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$PnNhZiJwjaSMsjKImy-7oC2dujQ;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;Lcom/squareup/crm/RolodexContactLoader;)V

    .line 150
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->showBottomRow(Lrx/functions/Action1;)Z

    move-result p2

    :goto_1
    if-nez p2, :cond_3

    .line 155
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->notifyDataSetChanged()V

    .line 158
    :cond_3
    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    if-eqz p2, :cond_4

    .line 160
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollToPosition(I)V

    const/4 p2, 0x0

    .line 161
    invoke-virtual {p3, p2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    goto :goto_2

    .line 162
    :cond_4
    invoke-virtual {p4}, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->isFirstPage()Z

    move-result p2

    if-eqz p2, :cond_5

    .line 163
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollToTop()V

    .line 166
    :cond_5
    :goto_2
    iget-boolean p2, p4, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->hasMore:Z

    if-eqz p2, :cond_6

    .line 168
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onNearEndOfLists:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onNearEndOfList()Lrx/Observable;

    move-result-object p1

    const/4 p3, 0x1

    invoke-virtual {p1, p3}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    :cond_6
    return-void
.end method

.method public synthetic lambda$null$8$ContactListPresenterV2(Lcom/squareup/ui/crm/rows/CustomerUnitRow;Lcom/squareup/protos/client/rolodex/Contact;ILkotlin/Unit;)V
    .locals 7

    .line 234
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->locale:Ljava/util/Locale;

    iget-boolean v5, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->inMultiSelectMode:Z

    iget-object p4, p2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 236
    invoke-direct {p0, p4}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->isChecked(Ljava/lang/String;)Z

    move-result v6

    move-object v0, p2

    move v1, p3

    .line 235
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getViewDataFromContact(Lcom/squareup/protos/client/rolodex/Contact;ILcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljava/util/Locale;ZZ)Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;

    move-result-object p2

    .line 234
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->setViewData(Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;)V

    return-void
.end method

.method public synthetic lambda$xoredContactCount$10$ContactListPresenterV2(Lkotlin/Unit;)Ljava/lang/Integer;
    .locals 0

    .line 259
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 91
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->bundleKey:Ljava/lang/String;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 103
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "checkedByDefault"

    .line 106
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->checkedByDefault:Z

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    const-string/jumbo v1, "xoredContactTokens"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 108
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onCheckedChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 112
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 114
    iget-boolean v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->checkedByDefault:Z

    const-string v1, "checkedByDefault"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string/jumbo v1, "xoredContactTokens"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method selectedContactCount()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->selectedContactCount:Lrx/Observable;

    return-object v0
.end method

.method setMultiSelect(ZLcom/squareup/protos/client/rolodex/ContactSet;)V
    .locals 2

    .line 240
    iput-boolean p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->inMultiSelectMode:Z

    .line 242
    iget-object p1, p2, Lcom/squareup/protos/client/rolodex/ContactSet;->type:Lcom/squareup/protos/client/rolodex/ContactSetType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/ContactSetType;->INCLUDED_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->checkedByDefault:Z

    .line 243
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    .line 244
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/ContactSet;->type:Lcom/squareup/protos/client/rolodex/ContactSetType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/ContactSetType;->INCLUDED_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    if-ne v0, v1, :cond_1

    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/ContactSet;->contact_tokens_included:Ljava/util/List;

    goto :goto_1

    :cond_1
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/ContactSet;->contact_tokens_excluded:Ljava/util/List;

    :goto_1
    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 247
    iget-object p1, p2, Lcom/squareup/protos/client/rolodex/ContactSet;->group_token:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->groupToken:Ljava/lang/String;

    .line 249
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onCheckedChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method setShowGreyHeaderRows(Z)V
    .locals 0

    .line 324
    iput-boolean p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->showGreyHeaderRows:Z

    return-void
.end method

.method toggleChecked(I)V
    .locals 1

    .line 282
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getContact(I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->xoredContactTokens:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 288
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onCheckedChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method xoredContactCount()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->onCheckedChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 258
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$XhYGZqBVMkoH97wV0EEyPGrrwD4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$XhYGZqBVMkoH97wV0EEyPGrrwD4;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;)V

    .line 259
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
