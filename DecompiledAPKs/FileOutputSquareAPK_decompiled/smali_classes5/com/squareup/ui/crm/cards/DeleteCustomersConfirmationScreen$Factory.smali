.class public final Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Factory;
.super Ljava/lang/Object;
.source "DeleteCustomersConfirmationScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 54
    invoke-interface {p0}, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;->deleteCustomers(Lcom/squareup/protos/client/rolodex/ContactSet;)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 59
    invoke-interface {p0}, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;->cancelDeleteCustomersConfirmationScreen()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 46
    const-class v0, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Component;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Component;->controller()Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;

    move-result-object v0

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 50
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/crm/applet/R$string;->crm_confirm_customer_deletion_loyalty_title:I

    .line 51
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 52
    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;->getDeleteCustomersConfirmationText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/crm/applet/R$string;->crm_delete:I

    .line 53
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$DeleteCustomersConfirmationScreen$Factory$iEyNtg55F341wQSyaBja7hqgzMQ;

    invoke-direct {v3, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeleteCustomersConfirmationScreen$Factory$iEyNtg55F341wQSyaBja7hqgzMQ;-><init>(Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;)V

    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/marin/R$drawable;->marin_selector_red:I

    .line 55
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/marin/R$color;->marin_white:I

    .line 56
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_cancel:I

    .line 57
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$DeleteCustomersConfirmationScreen$Factory$dhbwajUiWaGK13n2mttvCtyD5_w;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeleteCustomersConfirmationScreen$Factory$dhbwajUiWaGK13n2mttvCtyD5_w;-><init>(Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;)V

    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 60
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 50
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
