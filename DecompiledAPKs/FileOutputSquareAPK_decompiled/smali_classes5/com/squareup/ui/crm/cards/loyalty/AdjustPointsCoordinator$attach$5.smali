.class final Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;
.super Ljava/lang/Object;
.source "AdjustPointsCoordinator.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "newId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $reasons:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->$reasons:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 3

    const-string p3, "<anonymous parameter 0>"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    move-result-object p1

    .line 177
    sget p3, Lcom/squareup/crmscreens/R$id;->adjust_points_reason_1:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p2, p3, :cond_0

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->$reasons:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;

    goto :goto_0

    .line 178
    :cond_0
    sget p3, Lcom/squareup/crmscreens/R$id;->adjust_points_reason_2:I

    if-ne p2, p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->$reasons:Ljava/util/List;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;

    goto :goto_0

    .line 179
    :cond_1
    sget p3, Lcom/squareup/crmscreens/R$id;->adjust_points_reason_3:I

    if-ne p2, p3, :cond_2

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->$reasons:Ljava/util/List;

    const/4 p3, 0x2

    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;

    goto :goto_0

    .line 180
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->$reasons:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;

    .line 175
    :goto_0
    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->setAdjustReason(Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)V

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getActionBar$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 184
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {p2}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getAdjustmentAmount$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/noho/NohoEditRow;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    invoke-static {p2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_4

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {p2}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getAdjustmentAmount$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/noho/NohoEditRow;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object p3

    if-nez p3, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    const-string v2, "adjustmentAmount.text!!"

    invoke-static {p3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p3}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$scrubbedAdjustmentValue(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Landroid/text/Editable;)I

    move-result p2

    if-eqz p2, :cond_4

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    .line 183
    :goto_1
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
