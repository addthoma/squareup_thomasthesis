.class Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$2;
.super Lcom/squareup/debounce/DebouncedOnItemClickListener;
.source "ContactListViewV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$2;->this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnItemClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$2;->this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->access$200(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$2;->this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iget-object p1, p1, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    add-int/lit8 p3, p3, -0x2

    invoke-virtual {p1, p3}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->toggleChecked(I)V

    goto :goto_0

    .line 93
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$2;->this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->access$300(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$2;->this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iget-object p2, p2, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    add-int/lit8 p3, p3, -0x2

    invoke-virtual {p2, p3}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getContact(I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
