.class public interface abstract Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Component;
.super Ljava/lang/Object;
.source "SaveCardCustomerEmailScreen.java"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V
.end method
