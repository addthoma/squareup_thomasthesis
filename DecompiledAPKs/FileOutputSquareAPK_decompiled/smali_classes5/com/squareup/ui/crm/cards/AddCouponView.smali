.class public Lcom/squareup/ui/crm/cards/AddCouponView;
.super Landroid/widget/LinearLayout;
.source "AddCouponView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

.field presenter:Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field wholeUnitMoneyHelper:Lcom/squareup/money/WholeUnitMoneyHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const-class p2, Lcom/squareup/ui/crm/cards/AddCouponScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/AddCouponScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Component;->inject(Lcom/squareup/ui/crm/cards/AddCouponView;)V

    return-void
.end method


# virtual methods
.method couponIsBlank()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->isBlank(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method getCouponValue()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->wholeUnitMoneyHelper:Lcom/squareup/money/WholeUnitMoneyHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/WholeUnitMoneyHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$requestTextEditFocus$0$AddCouponView()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->presenter:Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->presenter:Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 42
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 48
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 49
    sget v0, Lcom/squareup/crmscreens/R$id;->price_input_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->wholeUnitMoneyHelper:Lcom/squareup/money/WholeUnitMoneyHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

    sget-object v2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NEVER_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AddCouponView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/crm/R$integer;->crm_coupon_max_price:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v3, v3

    .line 52
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/money/WholeUnitMoneyHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;J)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->wholeUnitMoneyHelper:Lcom/squareup/money/WholeUnitMoneyHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/money/WholeUnitMoneyHelper;->setHintToWholeMoney(Landroid/widget/TextView;J)V

    return-void
.end method

.method requestTextEditFocus()V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->couponValueInputField:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponView$sERj3S4KUqk2VerR3maV5iAwnpg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponView$sERj3S4KUqk2VerR3maV5iAwnpg;-><init>(Lcom/squareup/ui/crm/cards/AddCouponView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setPrimaryButtonEnabled(Z)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
