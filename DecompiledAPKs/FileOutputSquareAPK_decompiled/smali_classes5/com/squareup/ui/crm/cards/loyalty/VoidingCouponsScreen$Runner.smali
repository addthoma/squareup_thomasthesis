.class public interface abstract Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;
.super Ljava/lang/Object;
.source "VoidingCouponsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeVoidingCouponsScreen()V
.end method

.method public abstract getCouponsToBeVoided()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract success()V
.end method
