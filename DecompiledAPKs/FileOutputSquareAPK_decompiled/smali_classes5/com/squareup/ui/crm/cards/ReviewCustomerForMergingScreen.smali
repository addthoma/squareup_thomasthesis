.class public Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ReviewCustomerForMergingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Component;,
        Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$ComponentFactory;,
        Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 101
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ReviewCustomerForMergingScreen$GPlyIqC-sV2uqZ8NmIV-hfW5kBA;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ReviewCustomerForMergingScreen$GPlyIqC-sV2uqZ8NmIV-hfW5kBA;

    .line 102
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;
    .locals 1

    .line 103
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 104
    new-instance v0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 97
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_REVIEW_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 53
    const-class v0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Component;

    .line 54
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Component;->coordinator()Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 41
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_review_customer_for_merging_view:I

    return v0
.end method
