.class public interface abstract Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;
.super Ljava/lang/Object;
.source "AddCustomersToGroupScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract cancelAddCustomersToGroupScreen()V
.end method

.method public abstract getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
.end method

.method public abstract getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;
.end method

.method public abstract setGroupToAddTo(Lcom/squareup/protos/client/rolodex/Group;)V
.end method

.method public abstract showAddingCustomersToGroupScreen()V
.end method

.method public abstract showCreateGroupScreen()V
.end method
