.class public Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ViewLoyaltyBalanceCoordinator.java"


# instance fields
.field private final formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/PointsTermsFormatter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->res:Lcom/squareup/util/Res;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;)Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 35
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 36
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_points_balance:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 37
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$bFDAvJts8rROGv2w6s_3cn_wms8;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$bFDAvJts8rROGv2w6s_3cn_wms8;-><init>(Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$bFDAvJts8rROGv2w6s_3cn_wms8;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$bFDAvJts8rROGv2w6s_3cn_wms8;-><init>(Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;->getLoyaltyStatusPoints()I

    move-result v0

    .line 42
    sget v1, Lcom/squareup/crmscreens/R$id;->crm_add_points_button:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 43
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_adjust_points_add:I

    invoke-virtual {v2, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 44
    new-instance v2, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator$1;-><init>(Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    sget v1, Lcom/squareup/crmscreens/R$id;->crm_remove_points_button:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 51
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_adjust_points_remove:I

    invoke-virtual {v2, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    if-gtz v0, :cond_1

    .line 53
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;->canGoNegative()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 60
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 54
    :cond_1
    :goto_0
    new-instance v2, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    :goto_1
    sget v1, Lcom/squareup/crmscreens/R$id;->crm_points_balance_title:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_points_earned_format:I

    invoke-virtual {v1, v0, v2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
