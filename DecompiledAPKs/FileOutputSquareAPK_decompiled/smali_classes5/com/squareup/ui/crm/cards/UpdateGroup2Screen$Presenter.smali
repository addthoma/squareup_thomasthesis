.class Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;
.super Lmortar/ViewPresenter;
.source "UpdateGroup2Screen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/UpdateGroup2View;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_UNIQUE_KEY:Ljava/lang/String; = "uniqueKey"


# instance fields
.field private final busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

.field private final errorBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final filterHelper:Lcom/squareup/crm/filters/FilterHelper;

.field private final onDelete:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field

.field private final onSave:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private uniqueKey:Ljava/util/UUID;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/crm/filters/FilterHelper;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 109
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 102
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->onSave:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 103
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->onDelete:Lcom/jakewharton/rxrelay2/PublishRelay;

    const/4 v0, 0x0

    .line 105
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 110
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    .line 111
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->errorBar:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 112
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->res:Lcom/squareup/util/Res;

    .line 113
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->filterHelper:Lcom/squareup/crm/filters/FilterHelper;

    .line 114
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    return-void
.end method

.method private bind(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/protos/client/rolodex/Filter;I)V
    .locals 1

    .line 252
    iget-object v0, p3, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 253
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 256
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$9lbW7dIGfrL0lgP2zKrS_dvq6iA;

    invoke-direct {v0, p0, p3, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$9lbW7dIGfrL0lgP2zKrS_dvq6iA;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/ui/account/view/SmartLineRow;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 265
    new-instance p3, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Tn2ZdJmk7TdVfjTuJAOxIP1Q_kY;

    invoke-direct {p3, p0, p1, p4, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Tn2ZdJmk7TdVfjTuJAOxIP1Q_kY;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/ui/account/view/SmartLineRow;ILcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    invoke-static {p1, p3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private buildGroup(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)Lcom/squareup/protos/client/rolodex/Group;
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->getGroup()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Group;->newBuilder()Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    .line 276
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->getGroupName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object p1

    .line 277
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$21(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 203
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->showProgressBar(Z)V

    .line 204
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 205
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->setDeleteGroupEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$null$23(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 211
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$29(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/String;)V
    .locals 0

    .line 260
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    .line 261
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    return-void
.end method

.method static synthetic lambda$null$31(ILkotlin/Unit;)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 267
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$13(Lcom/squareup/protos/client/rolodex/Group;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 160
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/AudienceType;->GROUP_V2_SMART:Lcom/squareup/protos/client/rolodex/AudienceType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$onEnterScope$6(Lcom/squareup/protos/client/rolodex/Group;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 140
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$onLoad$24(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/marin/widgets/MarinActionBar;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 210
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->groupName()Lio/reactivex/Observable;

    move-result-object p0

    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$f6LN1qutj5vSyawds4e_XBvdVOM;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$f6LN1qutj5vSyawds4e_XBvdVOM;

    .line 211
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SxET--V_t5eSk3B_3IRcq3lnTMA;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SxET--V_t5eSk3B_3IRcq3lnTMA;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 212
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$bind$30$UpdateGroup2Screen$Presenter(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/ui/account/view/SmartLineRow;)Lrx/Subscription;
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->filterHelper:Lcom/squareup/crm/filters/FilterHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/filters/FilterHelper;->displayValueOf(Lcom/squareup/protos/client/rolodex/Filter;)Lrx/Observable;

    move-result-object p1

    .line 258
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Mauct-R7bKUSUiQXVCqdx6VWPE8;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Mauct-R7bKUSUiQXVCqdx6VWPE8;-><init>(Lcom/squareup/ui/account/view/SmartLineRow;)V

    .line 259
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bind$33$UpdateGroup2Screen$Presenter(Lcom/squareup/ui/account/view/SmartLineRow;ILcom/squareup/ui/crm/cards/UpdateGroup2View;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 266
    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$ZsQPi0rzC8cCmYyle_hNrhuDfjc;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$ZsQPi0rzC8cCmYyle_hNrhuDfjc;-><init>(I)V

    .line 267
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$yFoOaeFT-nGI44OES76PTAL9eYw;

    invoke-direct {p2, p0, p3}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$yFoOaeFT-nGI44OES76PTAL9eYw;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    .line 268
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$UpdateGroup2Screen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$1$UpdateGroup2Screen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$10$UpdateGroup2Screen$Presenter(Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->commitUpdateGroup2Screen(Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public synthetic lambda$null$11$UpdateGroup2Screen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 149
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;

    const-string p2, ""

    if-eqz p1, :cond_0

    .line 150
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->errorBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->errorBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_failed_to_save_group:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$14$UpdateGroup2Screen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$15$UpdateGroup2Screen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$17$UpdateGroup2Screen$Presenter(Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexGroupHelper;->toGroup(Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->commitUpdateGroup2Screen(Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public synthetic lambda$null$18$UpdateGroup2Screen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 169
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;

    const-string p2, ""

    if-eqz p1, :cond_0

    .line 170
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->errorBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->errorBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_failed_to_save_group:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$25$UpdateGroup2Screen$Presenter(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 235
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->buildGroup(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->setGroup(Lcom/squareup/protos/client/rolodex/Group;)V

    .line 236
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->showCreateFilterScreen()V

    return-void
.end method

.method public synthetic lambda$null$27$UpdateGroup2Screen$Presenter(Lcom/squareup/protos/client/rolodex/Group;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 244
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->onDelete:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$3$UpdateGroup2Screen$Presenter(Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->commitUpdateGroup2Screen(Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public synthetic lambda$null$32$UpdateGroup2Screen$Presenter(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Ljava/lang/Integer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->buildGroup(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->setGroup(Lcom/squareup/protos/client/rolodex/Group;)V

    .line 270
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->showUpdateFilterScreen(I)V

    return-void
.end method

.method public synthetic lambda$null$4$UpdateGroup2Screen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 129
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;

    const-string p2, ""

    if-eqz p1, :cond_0

    .line 130
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->errorBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->errorBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_failed_to_delete_group:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$7$UpdateGroup2Screen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$8$UpdateGroup2Screen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$12$UpdateGroup2Screen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 146
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$yMu5oIbtGssgSa-JJ56YnhxvDjU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$yMu5oIbtGssgSa-JJ56YnhxvDjU;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$eQdLRN2fDInmbHyMophNlHMV1M0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$eQdLRN2fDInmbHyMophNlHMV1M0;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$16$UpdateGroup2Screen$Presenter(Lcom/squareup/protos/client/rolodex/Group;)Lio/reactivex/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexGroupHelper;->toGroupV2(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/crm/RolodexServiceHelper;->upsertSmartGroup(Lcom/squareup/protos/client/rolodex/GroupV2;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$oDkLj1pKGB_gjYPstIgheyNo22U;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$oDkLj1pKGB_gjYPstIgheyNo22U;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 163
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$vwyTPdHBqB8KK7_NbZF0wNIPbFs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$vwyTPdHBqB8KK7_NbZF0wNIPbFs;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 164
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$19$UpdateGroup2Screen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 166
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Siv0y0T36Hgkm5celeypAVAgORk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Siv0y0T36Hgkm5celeypAVAgORk;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$_A8mZyQIv4YJPuBng2qmAuiU1wE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$_A8mZyQIv4YJPuBng2qmAuiU1wE;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$UpdateGroup2Screen$Presenter(Lcom/squareup/protos/client/rolodex/Group;)Lio/reactivex/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-interface {v0, p1}, Lcom/squareup/crm/RolodexServiceHelper;->deleteGroup(Lcom/squareup/protos/client/rolodex/Group;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$PRZDv5APtgUPfcjovRZAAW_7-oQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$PRZDv5APtgUPfcjovRZAAW_7-oQ;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 123
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$UofcsgYRPJd3ZOQJShQwH3aWM7Q;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$UofcsgYRPJd3ZOQJShQwH3aWM7Q;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 124
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$5$UpdateGroup2Screen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 126
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$KrSmPrEPGqaEDbWZJwbLfU0NnVY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$KrSmPrEPGqaEDbWZJwbLfU0NnVY;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$iPGEUgjXJq7vcQ_e0GigI8f7-ok;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$iPGEUgjXJq7vcQ_e0GigI8f7-ok;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$9$UpdateGroup2Screen$Presenter(Lcom/squareup/protos/client/rolodex/Group;)Lio/reactivex/SingleSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-interface {v0, p1, v1}, Lcom/squareup/crm/RolodexServiceHelper;->upsertManualGroup(Lcom/squareup/protos/client/rolodex/Group;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$dqe914ETckbbDs-u6_8tfRg_9CU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$dqe914ETckbbDs-u6_8tfRg_9CU;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 143
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$wS33BY0Y8fyS1eXfkEDYGLpQzzA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$wS33BY0Y8fyS1eXfkEDYGLpQzzA;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 144
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$20$UpdateGroup2Screen$Presenter(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->onSave:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->buildGroup(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$22$UpdateGroup2Screen$Presenter(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/marin/widgets/MarinActionBar;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 201
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$6zOwN0z4d5L7crtv8nbBufNPdyg;

    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$6zOwN0z4d5L7crtv8nbBufNPdyg;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 202
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$26$UpdateGroup2Screen$Presenter(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 233
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->onAddFilterClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$scFzbpfFIoVz2rC2kDHivZfYUPw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$scFzbpfFIoVz2rC2kDHivZfYUPw;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    .line 234
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$28$UpdateGroup2Screen$Presenter(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/protos/client/rolodex/Group;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 243
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->onDeleteGroupClicked()Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$9Jp8EwxT1lfWAZDAI9qLY0oC61o;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$9Jp8EwxT1lfWAZDAI9qLY0oC61o;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/protos/client/rolodex/Group;)V

    .line 244
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->onDelete:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Y9pv0NLy3re6rotfJ3TGsq2k_WQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Y9pv0NLy3re6rotfJ3TGsq2k_WQ;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 121
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$R-Fgf_Wd0BfoGOF7bpXdSaTxMag;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$R-Fgf_Wd0BfoGOF7bpXdSaTxMag;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 126
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 119
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->onSave:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$EWN9e2P74ICWZg-BMLqsgTQp61U;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$EWN9e2P74ICWZg-BMLqsgTQp61U;

    .line 140
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$36fXFYHhcUq3vTdRFv1qRwDX35o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$36fXFYHhcUq3vTdRFv1qRwDX35o;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 141
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$qvSSJo2zHxuj4FnBhdVgyVdYoK0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$qvSSJo2zHxuj4FnBhdVgyVdYoK0;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 146
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 138
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->onSave:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$aj-9840-SHGtcxXEr4FPy9tAoTo;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$aj-9840-SHGtcxXEr4FPy9tAoTo;

    .line 160
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$P2VCa9tvM1yCTsDmDNhvk-sj4dI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$P2VCa9tvM1yCTsDmDNhvk-sj4dI;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 161
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$_JeA5LBxG3uKIzoObY-dJIzCZ08;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$_JeA5LBxG3uKIzoObY-dJIzCZ08;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;)V

    .line 166
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 158
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 6

    .line 179
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 180
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;

    .line 181
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 182
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;->getGroup()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v2

    .line 184
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/crm/applet/R$string;->crm_edit_group_label:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 185
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/crm/cards/-$$Lambda$1V7lSYu6KvZjBZiN-HWf8X-9Jb8;

    invoke-direct {v4, v3}, Lcom/squareup/ui/crm/cards/-$$Lambda$1V7lSYu6KvZjBZiN-HWf8X-9Jb8;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;)V

    invoke-virtual {v1, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 186
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->controller:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/crm/cards/-$$Lambda$1V7lSYu6KvZjBZiN-HWf8X-9Jb8;

    invoke-direct {v4, v3}, Lcom/squareup/ui/crm/cards/-$$Lambda$1V7lSYu6KvZjBZiN-HWf8X-9Jb8;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;)V

    invoke-static {v0, v4}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 188
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 189
    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$sqIQ7UKCXunS7_IwLGPh3q0q67o;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$sqIQ7UKCXunS7_IwLGPh3q0q67o;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    if-nez p1, :cond_0

    .line 192
    iget-object p1, v2, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->setGroupName(Ljava/lang/String;)V

    .line 193
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->uniqueKey:Ljava/util/UUID;

    goto :goto_0

    :cond_0
    const-string/jumbo v3, "uniqueKey"

    .line 195
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->uniqueKey:Ljava/util/UUID;

    .line 199
    :goto_0
    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$WWkmQc27xkPuujmzo6ITYmUKK8g;

    invoke-direct {p1, p0, v0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$WWkmQc27xkPuujmzo6ITYmUKK8g;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 209
    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$FLKN-X7fOKHF82jo8JouPty2HuA;

    invoke-direct {p1, v0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$FLKN-X7fOKHF82jo8JouPty2HuA;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 214
    iget-object p1, v2, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    invoke-static {p1, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 215
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->showAddFilter()V

    const/4 p1, 0x0

    .line 218
    iget-object v1, v2, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 219
    iget-object v1, v2, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter;

    .line 220
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->addFilterRow()Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v4

    invoke-direct {p0, v4, v0, v3, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->bind(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/protos/client/rolodex/Filter;I)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_1
    if-lez p1, :cond_2

    .line 224
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->showFilters()V

    :cond_2
    const/16 v1, 0xa

    if-lt p1, v1, :cond_3

    .line 229
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->disableAddFilter()V

    goto :goto_2

    .line 232
    :cond_3
    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Dc7TBXrDtrBAtlvzMWpLsE_6F2E;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$Dc7TBXrDtrBAtlvzMWpLsE_6F2E;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 242
    :cond_4
    :goto_2
    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$tSxZRp30KoTWJ99RHOF6pTB82ps;

    invoke-direct {p1, p0, v0, v2}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$Presenter$tSxZRp30KoTWJ99RHOF6pTB82ps;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/protos/client/rolodex/Group;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uniqueKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
