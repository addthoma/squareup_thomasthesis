.class public Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "AllAppointmentsScreen.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;,
        Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final type:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsScreen$TF2jpkYwrk1PB9NVOZPUU4xCeCs;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsScreen$TF2jpkYwrk1PB9NVOZPUU4xCeCs;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;->type:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;
    .locals 2

    .line 60
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 61
    invoke-static {}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->values()[Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    aget-object p0, v1, p0

    .line 62
    new-instance v1, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 54
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 56
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;->type:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 48
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope$CustomerProfileComponent;

    .line 49
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$CustomerProfileComponent;

    .line 50
    invoke-interface {p1}, Lcom/squareup/ui/crm/flow/CrmScope$CustomerProfileComponent;->allAppointmentsCoordinatorFactory()Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;->type:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;->create(Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;)Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 44
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_customer_all_appointments_view:I

    return v0
.end method
