.class public Lcom/squareup/ui/crm/cards/CreateGroupView;
.super Landroid/widget/LinearLayout;
.source "CreateGroupView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private groupEdit:Lcom/squareup/ui/crm/cards/group/GroupEditView;

.field presenter:Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/view/View;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class p2, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;->inject(Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CreateGroupView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->shortAnimTimeMs:I

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 92
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 93
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_group_edit:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/group/GroupEditView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->groupEdit:Lcom/squareup/ui/crm/cards/group/GroupEditView;

    .line 94
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->progressBar:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 60
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method group()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->groupEdit:Lcom/squareup/ui/crm/cards/group/GroupEditView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/group/GroupEditView;->group()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->presenter:Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->presenter:Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CreateGroupView;->bindViews()V

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setActionBarPrimaryButtonEnabled(Z)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method setActionBarUpButtonEnabled(Z)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->groupEdit:Lcom/squareup/ui/crm/cards/group/GroupEditView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/group/GroupEditView;->setEnabled(Z)V

    return-void
.end method

.method setInitialFocus()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->groupEdit:Lcom/squareup/ui/crm/cards/group/GroupEditView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/group/GroupEditView;->setInitialFocus()V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 75
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method
