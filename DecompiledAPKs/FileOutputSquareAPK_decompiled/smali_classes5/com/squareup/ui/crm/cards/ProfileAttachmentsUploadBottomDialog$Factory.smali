.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory;
.super Ljava/lang/Object;
.source "ProfileAttachmentsUploadBottomDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Landroid/content/DialogInterface;)V
    .locals 1

    .line 57
    check-cast p0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 58
    sget v0, Lcom/google/android/material/R$id;->design_bottom_sheet:I

    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 59
    invoke-static {p0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->from(Landroid/view/View;)Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    move-result-object p0

    const/4 v0, 0x3

    .line 60
    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 34
    const-class v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 35
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 36
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;->runner()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-direct {v1, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    .line 39
    sget v2, Lcom/squareup/profileattachments/R$layout;->crm_profile_attachments_upload_bottom_sheet:I

    const/4 v3, 0x0

    .line 40
    invoke-static {p1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 42
    sget v2, Lcom/squareup/profileattachments/R$id;->take_photo:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 43
    new-instance v3, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory$1;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory$1;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    sget v2, Lcom/squareup/profileattachments/R$id;->upload_photo:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 50
    new-instance v3, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory$2;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory$2;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsUploadBottomDialog$Factory$4fU2f6ZAR0_EQo7MhugPgD-VlyA;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsUploadBottomDialog$Factory$4fU2f6ZAR0_EQo7MhugPgD-VlyA;

    invoke-virtual {v1, v0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 62
    invoke-virtual {v1, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 63
    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
