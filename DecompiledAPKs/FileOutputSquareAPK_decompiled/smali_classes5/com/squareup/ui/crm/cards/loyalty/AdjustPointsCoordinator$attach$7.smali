.class final Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;
.super Lkotlin/jvm/internal/Lambda;
.source "AdjustPointsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "state",
        "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;->invoke(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;)V
    .locals 4

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getProgressBar$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;->LOADING:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getContent$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;->LOADING:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;

    if-eq p1, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 231
    sget-object v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;->ERROR:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;

    const-string v1, "adjust-loyalty-points"

    if-ne p1, v0, :cond_2

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getErrorsBar$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object p1

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getRes$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_adjust_punches_error:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 232
    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 237
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getErrorsBar$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    :goto_2
    return-void
.end method
