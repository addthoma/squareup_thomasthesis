.class public final Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "AdjustPointsScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;,
        Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;,
        Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Component;,
        Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAdjustPointsScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AdjustPointsScreen.kt\ncom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,102:1\n43#2:103\n*E\n*S KotlinDebug\n*F\n+ 1 AdjustPointsScreen.kt\ncom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen\n*L\n41#1:103\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u0000 \u00182\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0004\u0017\u0018\u0019\u001aB\u000f\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0014J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\rH\u0016\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;",
        "Lcom/squareup/ui/crm/flow/InCrmScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/spot/HasSpot;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "crmPath",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getSpot",
        "Lcom/squareup/container/spot/Spot;",
        "context",
        "Landroid/content/Context;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "AdjustPointsLoadingState",
        "Companion",
        "Component",
        "Runner",
        "crm-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;->Companion:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Companion;

    .line 97
    sget-object v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel {\n      Adjus\u2026ava.classLoader)!!)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    const-string v0, "GROW_OVER"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    const-class v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Component;

    .line 41
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Component;->adjustPointsCoordinator()Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 36
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_adjust_points:I

    return v0
.end method
