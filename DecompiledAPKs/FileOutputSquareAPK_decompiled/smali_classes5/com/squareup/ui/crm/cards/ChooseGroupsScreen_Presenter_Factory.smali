.class public final Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ChooseGroupsScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final groupLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;->groupLoaderProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/util/Res;)Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;->groupLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/crm/RolodexGroupLoader;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/util/Res;)Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen_Presenter_Factory;->get()Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
