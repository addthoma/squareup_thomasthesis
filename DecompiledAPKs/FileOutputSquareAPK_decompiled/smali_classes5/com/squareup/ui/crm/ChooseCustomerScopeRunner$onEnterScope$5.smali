.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$5;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
        "Lcom/squareup/crm/RolodexContactLoader$Input;",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        ">;+",
        "Ljava/lang/Boolean;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u00b7\u0001\u0010\u0002\u001a\u00b2\u0001\u0012D\u0012B\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007 \u0006* \u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00080\u0008 \u0006*X\u0012D\u0012B\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007 \u0006* \u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00080\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/datafetch/Rx1AbstractLoader$Results;",
        "Lcom/squareup/crm/RolodexContactLoader$Input;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$5;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$5;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 157
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$5;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getMainThreadEnforcer$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 158
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$5;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    const-string v2, "results"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "displayErrorItemAtEnd"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {v1, v0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$generateItems(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;Z)V

    return-void
.end method
