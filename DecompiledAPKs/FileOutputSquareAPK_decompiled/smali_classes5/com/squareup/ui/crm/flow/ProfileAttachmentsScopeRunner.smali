.class public Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;
.super Ljava/lang/Object;
.source "ProfileAttachmentsScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;
    }
.end annotation


# static fields
.field private static final IMAGE_MIME_TYPE_OK_TO_PREVIEW:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PDF_MIME_TYPE:Ljava/lang/String; = "application/pdf"

.field private static final UPLOAD_SCREEN_TIMEOUT_SECONDS:J = 0x5L


# instance fields
.field private final attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field private final attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

.field private cameraListener:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;

.field private final contact:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatter:Ljava/text/DateFormat;

.field private externalFile:Ljava/io/File;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final locale:Ljava/util/Locale;

.field private path:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final timeFormatter:Ljava/text/DateFormat;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;

.field private uploadFileUri:Landroid/net/Uri;

.field private final uploadState:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 121
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "image/jpeg"

    const-string v2, "image/png"

    const-string v3, "image/bmp"

    const-string v4, "image/gif"

    filled-new-array {v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v1

    .line 122
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 121
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->IMAGE_MIME_TYPE_OK_TO_PREVIEW:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Lflow/Flow;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;Lcom/squareup/util/Res;Lcom/squareup/util/ToastFactory;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 2
    .param p12    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p13    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 107
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->contact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 108
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 109
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 116
    new-instance v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$1;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->cameraListener:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;

    .line 129
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    .line 130
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 131
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    .line 132
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    .line 133
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    .line 134
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->uploadState:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;

    .line 135
    iput-object p7, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->res:Lcom/squareup/util/Res;

    .line 136
    iput-object p8, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    .line 137
    iput-object p9, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->dateFormatter:Ljava/text/DateFormat;

    .line 138
    iput-object p10, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->timeFormatter:Ljava/text/DateFormat;

    .line 139
    iput-object p11, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->locale:Ljava/util/Locale;

    .line 140
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p12, p13}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->autoClose:Lcom/squareup/util/RxWatchdog;

    .line 141
    iput-object p13, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method static synthetic access$102(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->uploadFileUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)Lflow/Flow;
    .locals 0

    .line 90
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;
    .locals 0

    .line 90
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->path:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    return-object p0
.end method

.method private canPreviewImageMimeType(Ljava/lang/String;)Z
    .locals 1

    .line 446
    sget-object v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->IMAGE_MIME_TYPE_OK_TO_PREVIEW:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private displayFileInViewer(Landroid/content/Context;Lokhttp3/ResponseBody;Ljava/lang/String;)V
    .locals 1

    .line 382
    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->loadResponseToExternalFile(Lokhttp3/ResponseBody;)Ljava/io/File;

    move-result-object p2

    if-nez p2, :cond_0

    .line 384
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/ui/FileViewerErrorDialog;

    const/4 v0, 0x0

    invoke-direct {p0, p3, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getErrorType(Ljava/lang/String;Z)Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/ui/FileViewerErrorDialog;-><init>(Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 387
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->goToViewer(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method private getErrorType(Ljava/lang/String;Z)Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;
    .locals 1

    const-string v0, "application/pdf"

    .line 427
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 429
    sget-object p1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->NO_PDF_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-object p1

    .line 431
    :cond_0
    sget-object p1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_PDF_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-object p1

    .line 432
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->canPreviewImageMimeType(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 434
    sget-object p1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->NO_IMAGE_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-object p1

    .line 436
    :cond_2
    sget-object p1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_IMAGE_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-object p1

    :cond_3
    if-eqz p2, :cond_4

    .line 439
    sget-object p1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->DEFAULT_NO_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-object p1

    .line 441
    :cond_4
    sget-object p1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_DEFAULT_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-object p1
.end method

.method private getOrCreateExternalFile()Ljava/io/File;
    .locals 4

    .line 402
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->externalFile:Ljava/io/File;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->externalFile:Ljava/io/File;

    return-object v0

    .line 406
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->isExternalStorageWritable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    return-object v1

    .line 410
    :cond_1
    new-instance v0, Ljava/io/File;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    .line 411
    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 412
    invoke-virtual {v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 415
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-nez v2, :cond_2

    return-object v1

    .line 418
    :cond_2
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->externalFile:Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    return-object v1
.end method

.method private getOrCreateFilename(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .line 482
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->uploadFileUri:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 487
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    const-string v0, "_display_name"

    .line 488
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 489
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 490
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 493
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->res:Lcom/squareup/util/Res;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->locale:Ljava/util/Locale;

    invoke-static {p1, v0, v1}, Lcom/squareup/crm/DateTimeFormatHelper;->formatDateTimeForProfileAttachmentsFilename(Lcom/squareup/util/Res;Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private goToViewer(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V
    .locals 2

    .line 466
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 467
    invoke-static {p2, p1}, Lcom/squareup/util/Files;->getUriForFile(Ljava/io/File;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 468
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt p2, v1, :cond_0

    const/4 p2, 0x3

    .line 469
    invoke-virtual {v0, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const/high16 p2, 0x40000000    # 2.0f

    .line 471
    invoke-virtual {v0, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 473
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v1, Lcom/squareup/profileattachments/R$string;->open_file:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p2

    .line 474
    invoke-static {p2, p1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 475
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 477
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/ui/FileViewerErrorDialog;

    const/4 v0, 0x1

    invoke-direct {p0, p3, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getErrorType(Ljava/lang/String;Z)Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/ui/FileViewerErrorDialog;-><init>(Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private isExternalStorageWritable()Z
    .locals 2

    .line 450
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    .line 451
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private loadResponseToExternalFile(Lokhttp3/ResponseBody;)Ljava/io/File;
    .locals 1

    .line 391
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getOrCreateExternalFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 395
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->saveResponseBodyToFile(Lokhttp3/ResponseBody;Ljava/io/File;)V

    :cond_0
    return-object v0
.end method

.method private saveResponseBodyToFile(Lokhttp3/ResponseBody;Ljava/io/File;)V
    .locals 1

    .line 458
    :try_start_0
    invoke-static {p2}, Lokio/Okio;->sink(Ljava/io/File;)Lokio/Sink;

    move-result-object p2

    invoke-static {p2}, Lokio/Okio;->buffer(Lokio/Sink;)Lokio/BufferedSink;

    move-result-object p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    :try_start_1
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->source()Lokio/BufferedSource;

    move-result-object p1

    invoke-interface {p2, p1}, Lokio/BufferedSink;->writeAll(Lokio/Source;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p2, :cond_1

    .line 460
    :try_start_2
    invoke-interface {p2}, Lokio/BufferedSink;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 458
    :try_start_3
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    if-eqz p2, :cond_0

    .line 460
    :try_start_4
    invoke-interface {p2}, Lokio/BufferedSink;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception p2

    :try_start_5
    invoke-virtual {p1, p2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception p1

    const-string p2, "Failed to download response to file."

    .line 461
    invoke-static {p1, p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void
.end method


# virtual methods
.method public closePreviewScreen()V
    .locals 4

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexAttachmentLoader;->refresh()V

    return-void
.end method

.method public closeProfileAttachmentsScreen()V
    .locals 4

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexAttachmentLoader;->refresh()V

    return-void
.end method

.method public closeUploadScreen()V
    .locals 4

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexAttachmentLoader;->refresh()V

    return-void
.end method

.method public deleteAttachment(Landroid/content/DialogInterface;)V
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getAttachmentToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->deleteAttachment(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$ivkLAqv_mnSmRLNsYC2yA6O6FHw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$ivkLAqv_mnSmRLNsYC2yA6O6FHw;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/DialogInterface;)V

    .line 258
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public downloadFile(Landroid/content/Context;)V
    .locals 3

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/squareup/crm/RolodexServiceHelper;->downloadAttachment(Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$F4HhEHUX7-e3BpnTzqjFqXt03yU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$F4HhEHUX7-e3BpnTzqjFqXt03yU;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    .line 299
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$_rLRG4HFvIOTn8polVFmtWIcCLg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$_rLRG4HFvIOTn8polVFmtWIcCLg;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    .line 300
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$Hy84ABpb6bo0iEczV7_Pi7n8qo4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$Hy84ABpb6bo0iEczV7_Pi7n8qo4;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/Context;)V

    .line 301
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public getAttachmentToken()Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContactName()Ljava/lang/String;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->contact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->contact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentFilename()Ljava/lang/String;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFilename()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public getFormattedDateTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 4

    .line 374
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->dateFormatter:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->timeFormatter:Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->locale:Ljava/util/Locale;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/crm/DateTimeFormatHelper;->formatDateTimeForProfileAttachments(Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLoaderResults()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;>;"
        }
    .end annotation

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexAttachmentLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getUploadState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;",
            ">;"
        }
    .end annotation

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->uploadState:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;->uploadState()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getUploadStatusText(Landroid/view/View;Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;)Ljava/lang/CharSequence;
    .locals 2

    .line 359
    sget-object v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$1;->$SwitchMap$com$squareup$ui$crm$cards$ProfileAttachmentsUploadState$UploadState:[I

    invoke-virtual {p2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    const-string v1, "filename"

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    .line 369
    sget p2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_upload_in_progress:I

    invoke-static {p1, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 365
    :cond_0
    sget p2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_upload_failed:I

    invoke-static {p1, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 366
    invoke-virtual {p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 367
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 361
    :cond_1
    sget p2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_upload_succeeded:I

    invoke-static {p1, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 362
    invoke-virtual {p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 363
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public isBusy()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public synthetic lambda$deleteAttachment$2$ProfileAttachmentsScopeRunner(Landroid/content/DialogInterface;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 258
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$1tizw6ax6GBYtLZWcK217abq4_Y;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$1tizw6ax6GBYtLZWcK217abq4_Y;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/DialogInterface;)V

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$ke0_IutgSTUgplmssKoejsqUjjY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$ke0_IutgSTUgplmssKoejsqUjjY;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/DialogInterface;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$downloadFile$10$ProfileAttachmentsScopeRunner(Landroid/content/Context;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 301
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$e82107EPoGqrM00xboZyp_oKVA4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$e82107EPoGqrM00xboZyp_oKVA4;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/Context;)V

    new-instance p1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$uB7LtypUj7HhGW_EdRAJjhWom04;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$uB7LtypUj7HhGW_EdRAJjhWom04;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$downloadFile$6$ProfileAttachmentsScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p1, 0x1

    .line 299
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->setBusy(Ljava/lang/Boolean;)V

    return-void
.end method

.method public synthetic lambda$downloadFile$7$ProfileAttachmentsScopeRunner()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 300
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->setBusy(Ljava/lang/Boolean;)V

    return-void
.end method

.method public synthetic lambda$loadImage$11$ProfileAttachmentsScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p1, 0x1

    .line 314
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->setBusy(Ljava/lang/Boolean;)V

    return-void
.end method

.method public synthetic lambda$loadImage$12$ProfileAttachmentsScopeRunner()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 315
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->setBusy(Ljava/lang/Boolean;)V

    return-void
.end method

.method public synthetic lambda$loadImage$15$ProfileAttachmentsScopeRunner(Landroid/widget/ProgressBar;Landroid/widget/ImageView;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 316
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$mHb7iWcWu2dPoZcLZ-Dae1abFcg;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$mHb7iWcWu2dPoZcLZ-Dae1abFcg;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/widget/ProgressBar;Landroid/widget/ImageView;)V

    new-instance p1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$-8WKrHiawnQqD6Ar9MmPRLCPSHs;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$-8WKrHiawnQqD6Ar9MmPRLCPSHs;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {p3, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$0$ProfileAttachmentsScopeRunner(Landroid/content/DialogInterface;Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 259
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p2}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 260
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v0, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_delete_success:I

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    .line 261
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 262
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexAttachmentLoader;->refresh()V

    .line 263
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->closePreviewScreen()V

    return-void
.end method

.method public synthetic lambda$null$1$ProfileAttachmentsScopeRunner(Landroid/content/DialogInterface;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 265
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p2}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 266
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v0, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_general_failure:I

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    .line 267
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public synthetic lambda$null$13$ProfileAttachmentsScopeRunner(Landroid/widget/ProgressBar;Landroid/widget/ImageView;Lokhttp3/ResponseBody;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/16 v0, 0x8

    .line 318
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 319
    invoke-virtual {p3}, Lokhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 320
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public synthetic lambda$null$14$ProfileAttachmentsScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 322
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 323
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v0, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_general_failure:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    return-void
.end method

.method public synthetic lambda$null$16$ProfileAttachmentsScopeRunner(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 337
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->closeUploadScreen()V

    return-void
.end method

.method public synthetic lambda$null$18$ProfileAttachmentsScopeRunner(Lokhttp3/ResponseBody;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 340
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->setSuccess()V

    return-void
.end method

.method public synthetic lambda$null$19$ProfileAttachmentsScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 341
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->setFailure()V

    return-void
.end method

.method public synthetic lambda$null$3$ProfileAttachmentsScopeRunner(Landroid/content/DialogInterface;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 275
    iget-object p3, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p3}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 276
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 277
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexAttachmentLoader;->refresh()V

    .line 278
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$4$ProfileAttachmentsScopeRunner(Landroid/content/DialogInterface;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 280
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v0, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_general_failure:I

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    .line 281
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public synthetic lambda$null$8$ProfileAttachmentsScopeRunner(Landroid/content/Context;Lokhttp3/ResponseBody;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Attachment;->content_type:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->displayFileInViewer(Landroid/content/Context;Lokhttp3/ResponseBody;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$9$ProfileAttachmentsScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 304
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v0, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_general_failure:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    return-void
.end method

.method public synthetic lambda$renameFile$5$ProfileAttachmentsScopeRunner(Landroid/content/DialogInterface;Ljava/lang/String;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 274
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$1BuFY4v9Fz1cZPL5sTuN0-xKZXg;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$1BuFY4v9Fz1cZPL5sTuN0-xKZXg;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/DialogInterface;Ljava/lang/String;)V

    new-instance p2, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$BuxekN0BLHGwCDp-v0crnqpAx3Y;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$BuxekN0BLHGwCDp-v0crnqpAx3Y;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/DialogInterface;)V

    invoke-virtual {p3, v0, p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$uploadFile$17$ProfileAttachmentsScopeRunner()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x5

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$0LlaJupiWgFixPPv8cr9VV0gTxk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$0LlaJupiWgFixPPv8cr9VV0gTxk;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public synthetic lambda$uploadFile$20$ProfileAttachmentsScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 339
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$S4mJASIYk8rPD8V35eMyb5GIRRE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$S4mJASIYk8rPD8V35eMyb5GIRRE;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$8HKPga-4Jl9k_KPOzs8fT3W0Yt0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$8HKPga-4Jl9k_KPOzs8fT3W0Yt0;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public loadImage(Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V
    .locals 3

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/squareup/crm/RolodexServiceHelper;->downloadAttachment(Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$e0NKi3DnCcM9btfvE9p8WW4m3_s;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$e0NKi3DnCcM9btfvE9p8WW4m3_s;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    .line 314
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$xXiu08Z0wGOSfE1zDYVTj4pbcUI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$xXiu08Z0wGOSfE1zDYVTj4pbcUI;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    .line 315
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$lcJcPex9z2VFygABWlTPzkTJWJ0;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$lcJcPex9z2VFygABWlTPzkTJWJ0;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/widget/ProgressBar;Landroid/widget/ImageView;)V

    .line 316
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 145
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    .line 147
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->path:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    .line 148
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->contact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 151
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 156
    :cond_1
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 160
    :cond_2
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->uploadUri:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 161
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->uploadUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->uploadFileUri:Landroid/net/Uri;

    .line 163
    :cond_3
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    iget-object p1, p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/RolodexAttachmentLoader;->setContactToken(Ljava/lang/String;)V

    .line 168
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->cameraListener:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;

    invoke-interface {p1, v0}, Lcom/squareup/camerahelper/CameraHelper;->setListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->cameraListener:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;

    invoke-interface {v0, v1}, Lcom/squareup/camerahelper/CameraHelper;->dropListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    return-void
.end method

.method public openCamera()V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0}, Lcom/squareup/camerahelper/CameraHelper;->startCamera()V

    return-void
.end method

.method public openGallery()V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0}, Lcom/squareup/camerahelper/CameraHelper;->startGallery()V

    return-void
.end method

.method public renameFile(Landroid/content/DialogInterface;Ljava/lang/String;)V
    .locals 3

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getAttachmentToken()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->contact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v0, v1, p2, v2}, Lcom/squareup/crm/RolodexServiceHelper;->updateAttachment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$CdPaQj6fCgTnPsgU7DKfUl0YyBQ;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$CdPaQj6fCgTnPsgU7DKfUl0YyBQ;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/DialogInterface;Ljava/lang/String;)V

    .line 274
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public setAttachment(Lcom/squareup/protos/client/rolodex/Attachment;)V
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setBusy(Ljava/lang/Boolean;)V
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setFailure()V
    .locals 1

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->uploadState:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;->failedUpload()V

    return-void
.end method

.method public setSuccess()V
    .locals 1

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->uploadState:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;->successfulUpload()V

    return-void
.end method

.method public showDeleteFileConfirmation()V
    .locals 3

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsDeleteFileDialog;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->path:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsDeleteFileDialog;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showOverflowBottomSheet()V
    .locals 3

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->path:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showPreviewScreen(Lcom/squareup/protos/client/rolodex/Attachment;)V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->attachment:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 185
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->path:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewScreen;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showRenameFileDialog()V
    .locals 3

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->path:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showUploadBottomSheet()V
    .locals 3

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->path:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public uploadFile(Landroid/content/Context;)V
    .locals 3

    .line 328
    new-instance v0, Lcom/squareup/ui/crm/util/UriRequestBody;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->uploadFileUri:Landroid/net/Uri;

    .line 329
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/crm/util/UriRequestBody;-><init>(Landroid/net/Uri;Landroid/content/ContentResolver;)V

    .line 330
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getOrCreateFilename(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 331
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->fileName:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 334
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->contact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v1, v2, p1, v0}, Lcom/squareup/crm/RolodexServiceHelper;->uploadAttachment(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$LM86z02y01lX_8LF8AnjcP_tG5o;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$LM86z02y01lX_8LF8AnjcP_tG5o;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    .line 335
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$kfWX8MkUZdjYyNsZ5R6ghXyxuq4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScopeRunner$kfWX8MkUZdjYyNsZ5R6ghXyxuq4;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    .line 339
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
