.class public Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;
.super Ljava/lang/Object;
.source "UpdateGroup2Flow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;,
        Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$SharedScope;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

.field private filterIndex:Ljava/lang/Integer;

.field private final flow:Lflow/Flow;

.field private group:Lcom/squareup/protos/client/rolodex/Group;

.field private final onResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;",
            ">;"
        }
    .end annotation
.end field

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateFilterFlow;Lcom/squareup/ui/crm/flow/UpdateFilterFlow;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 90
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->flow:Lflow/Flow;

    .line 91
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    .line 92
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    .line 93
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic lambda$commitUpdateGroup2Screen$3(Lflow/History;)Lflow/History;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    .line 184
    const-class v0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 185
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Lkotlin/Pair;Lflow/History;)Lcom/squareup/container/Command;
    .locals 0

    .line 112
    invoke-virtual {p0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lrx/functions/Func1;

    invoke-interface {p0, p1}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lflow/History;

    .line 113
    sget-object p1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p0, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method private reset()V
    .locals 1

    const/4 v0, 0x0

    .line 204
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 205
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    .line 206
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public commitUpdateGroup2Screen(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 2

    if-nez p1, :cond_0

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v0, Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateGroup2Flow$4LSLO1Ru2Ly84Kd73_qoTC1MTlY;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateGroup2Flow$4LSLO1Ru2Ly84Kd73_qoTC1MTlY;

    invoke-static {v0}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->groupDeleted(Lrx/functions/Func1;)Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 187
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_DELETE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_0

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->groupUpdated(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 191
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SAVE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 193
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->reset()V

    return-void
.end method

.method public dismissUpdateGroup2Screen()V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->dismissed()Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 177
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->reset()V

    return-void
.end method

.method public getGroup()Lcom/squareup/protos/client/rolodex/Group;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 97
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$1$UpdateGroup2Flow(Lkotlin/Pair;)V
    .locals 3

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 105
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Group;->newBuilder()Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v1

    .line 108
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateGroup2Flow$YjQB31F9IsN6GTIjPhv_MzR1vhQ;

    invoke-direct {v2, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateGroup2Flow$YjQB31F9IsN6GTIjPhv_MzR1vhQ;-><init>(Lkotlin/Pair;)V

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$UpdateGroup2Flow(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 2

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    if-eqz p1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 125
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 128
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Group;->newBuilder()Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object p1

    .line 129
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object p1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    .line 102
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateGroup2Flow$QHdYav3u-86gI-ak48Iex4Lh_B4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateGroup2Flow$QHdYav3u-86gI-ak48Iex4Lh_B4;-><init>(Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;)V

    .line 103
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 101
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    .line 118
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateGroup2Flow$80Tv-xUPqxAD6wNy9IePijuR0vI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateGroup2Flow$80Tv-xUPqxAD6wNy9IePijuR0vI;-><init>(Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;)V

    .line 119
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 117
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "parentKey"

    .line 139
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 140
    sget-object v0, Lcom/squareup/protos/client/rolodex/Group;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "group"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Group;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    const/4 v0, -0x1

    const-string v1, "filterIndex"

    .line 141
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    .line 143
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    :cond_1
    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;",
            ">;"
        }
    .end annotation

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v1, "parentKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "group"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "filterIndex"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public setGroup(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method

.method public showCreateFilterScreen()V
    .locals 3

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    new-instance v1, Lcom/squareup/ui/crm/applet/CreateFilterScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/applet/CreateFilterScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/util/List;)V

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 1

    .line 197
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 198
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    const/4 p2, 0x0

    .line 199
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    .line 200
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showUpdateFilterScreen(I)V
    .locals 3

    .line 168
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->filterIndex:Ljava/lang/Integer;

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    new-instance v1, Lcom/squareup/ui/crm/applet/UpdateFilterScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/applet/UpdateFilterScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->group:Lcom/squareup/protos/client/rolodex/Group;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    .line 170
    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Filter;

    .line 169
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method
