.class public Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;
.super Ljava/lang/Object;
.source "ChooseGroupsFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;


# instance fields
.field private contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final createGroupFlow:Lcom/squareup/ui/crm/flow/CreateGroupFlow;

.field private createdGroup:Lcom/squareup/protos/client/rolodex/Group;

.field private final flow:Lflow/Flow;

.field private final onResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateGroupFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x0

    .line 57
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 58
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createdGroup:Lcom/squareup/protos/client/rolodex/Group;

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->flow:Lflow/Flow;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createGroupFlow:Lcom/squareup/ui/crm/flow/CreateGroupFlow;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-void
.end method


# virtual methods
.method public cancelChooseGroupsScreen()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 126
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 127
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createdGroup:Lcom/squareup/protos/client/rolodex/Group;

    .line 128
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public clearJustSavedGroupForChooseGroupsScreen()V
    .locals 1

    const/4 v0, 0x0

    .line 117
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createdGroup:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method

.method public closeChooseGroupsScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->cancelChooseGroupsScreen()V

    return-void
.end method

.method public getContactForChooseGroupsScreen()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getJustSavedGroupForChooseGroupsScreen()Lcom/squareup/protos/client/rolodex/Group;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createdGroup:Lcom/squareup/protos/client/rolodex/Group;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 81
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$ChooseGroupsFlow(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createdGroup:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createGroupFlow:Lcom/squareup/ui/crm/flow/CreateGroupFlow;

    .line 86
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseGroupsFlow$gguieBSJwH561n-RTQD1mje40aA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseGroupsFlow$gguieBSJwH561n-RTQD1mje40aA;-><init>(Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;)V

    .line 87
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 85
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 95
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "contact"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 96
    sget-object v0, Lcom/squareup/protos/client/rolodex/Group;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "createdGroup"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Group;

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createdGroup:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "contact"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createdGroup:Lcom/squareup/protos/client/rolodex/Group;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "createdGroup"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public setContactForChooseGroupsScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method

.method public showCreateGroupScreen()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->createGroupFlow:Lcom/squareup/ui/crm/flow/CreateGroupFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 70
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->sellerCreatingCustomer()Z

    return-void
.end method
