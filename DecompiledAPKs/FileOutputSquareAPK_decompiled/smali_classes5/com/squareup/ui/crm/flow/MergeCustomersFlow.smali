.class public Lcom/squareup/ui/crm/flow/MergeCustomersFlow;
.super Ljava/lang/Object;
.source "MergeCustomersFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/MergeCustomersFlow$SharedScope;
    }
.end annotation


# static fields
.field public static final EMPTY_LOYALTY_ACCOUNT_MAPPING:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;


# instance fields
.field private final chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

.field private chooseResult:Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

.field private contactA:Lcom/squareup/protos/client/rolodex/Contact;

.field private contactB:Lcom/squareup/protos/client/rolodex/Contact;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final loyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation
.end field

.field private final onResult:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;-><init>()V

    .line 51
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->EMPTY_LOYALTY_ACCOUNT_MAPPING:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    return-void
.end method

.method constructor <init>(Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/ChooseCustomerFlow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->onResult:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 57
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->loyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->flow:Lflow/Flow;

    .line 70
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->features:Lcom/squareup/settings/server/Features;

    .line 71
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    return-void
.end method

.method static synthetic lambda$mergeContact$4(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 1

    .line 122
    const-class v0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    return-object p0
.end method

.method static synthetic lambda$mergeContact$5(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)Ljava/lang/Boolean;
    .locals 1

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object p0

    sget-object v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->CUSTOMERS_APPLET_MERGE_TWO:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$1(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)Ljava/lang/Boolean;
    .locals 0

    .line 82
    instance-of p0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$2(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;
    .locals 0

    .line 83
    check-cast p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    return-object p0
.end method


# virtual methods
.method public cancelReviewCustomerForMergingScreen()V
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeMergingCustomersScreen()V
    .locals 4

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->flow:Lflow/Flow;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/crm/cards/MergingCustomersScreen;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public getContactForReviewCustomerForMergingScreen()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactB:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
    .locals 5

    .line 134
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/client/rolodex/ContactSetType;->INCLUDED_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    .line 135
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->type(Lcom/squareup/protos/client/rolodex/ContactSetType;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactA:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactB:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 136
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_included(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    move-result-object v0

    .line 137
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 75
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTargetLoyaltyAccountMapping()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->loyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$mergeContact$6$MergeCustomersFlow(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 2

    .line 128
    new-instance v0, Lcom/squareup/ui/crm/cards/MergingCustomersScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$3$MergeCustomersFlow(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;)V
    .locals 2

    .line 85
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactB:Lcom/squareup/protos/client/rolodex/Contact;

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->chooseResult:Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getParentKeyForChild()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public mergeContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 4

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->flow:Lflow/Flow;

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    const/4 v1, 0x3

    new-array v1, v1, [Lkotlin/jvm/functions/Function1;

    sget-object v2, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$mOGoWZGTOfYfzza9z68JnjBXfrI;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$mOGoWZGTOfYfzza9z68JnjBXfrI;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->chooseResult:Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    if-eqz v2, :cond_0

    .line 126
    invoke-virtual {v2}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getHistoryFuncForBackout()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$U6LWOV6hu1uINkUv7hO3iJFImUQ;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$U6LWOV6hu1uINkUv7hO3iJFImUQ;

    :goto_0
    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    new-instance v3, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$Py8ouzj3_3i6MyHLfjQ50vdlvBI;

    invoke-direct {v3, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$Py8ouzj3_3i6MyHLfjQ50vdlvBI;-><init>(Lcom/squareup/ui/crm/flow/MergeCustomersFlow;)V

    aput-object v3, v1, v2

    .line 119
    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    .line 80
    invoke-virtual {v0}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->getResults()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$dSsJoakdiQtUxpw5Nr1uiNzhcK4;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$dSsJoakdiQtUxpw5Nr1uiNzhcK4;

    .line 81
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$9rGHkN2xhJTQ6RByRy7JZglpb70;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$9rGHkN2xhJTQ6RByRy7JZglpb70;

    .line 82
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$iCSlYxQQNlCnj9voNNP-m8b1FTA;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$iCSlYxQQNlCnj9voNNP-m8b1FTA;

    .line 83
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$txfRyfuya6e4Umil95vcWC0NjT8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$MergeCustomersFlow$txfRyfuya6e4Umil95vcWC0NjT8;-><init>(Lcom/squareup/ui/crm/flow/MergeCustomersFlow;)V

    .line 84
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 79
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "parentKey"

    .line 97
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 98
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "contactA"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactA:Lcom/squareup/protos/client/rolodex/Contact;

    .line 99
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "contactB"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactB:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method

.method public onResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->onResult:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v1, "parentKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactA:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "contactA"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactB:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "contactB"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 7

    .line 164
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 165
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactA:Lcom/squareup/protos/client/rolodex/Contact;

    const/4 p2, 0x0

    .line 166
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactB:Lcom/squareup/protos/client/rolodex/Contact;

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->loyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->EMPTY_LOYALTY_ACCOUNT_MAPPING:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 168
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->chooseResult:Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    .line 170
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->CUSTOMERS_APPLET_MERGE_TWO:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_merge_customers_label:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->start(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    invoke-virtual {p2, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showMergeProposalScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)V
    .locals 1

    .line 187
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 188
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactA:Lcom/squareup/protos/client/rolodex/Contact;

    .line 189
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->contactB:Lcom/squareup/protos/client/rolodex/Contact;

    const/4 p2, 0x0

    .line 190
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->chooseResult:Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    .line 194
    sget-object p2, Lcom/squareup/crm/util/AppSpecificDataType;->LOYALTY_ACCOUNT_PHONE_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

    .line 195
    invoke-static {p3, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->getAppSpecificData(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/crm/util/AppSpecificDataType;)Lcom/squareup/protos/client/rolodex/AppField;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 196
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AppField;->text_value:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p2, ""

    .line 198
    :goto_0
    new-instance p3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;->TYPE_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    .line 199
    invoke-virtual {p3, v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->type(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    move-result-object p3

    .line 200
    invoke-virtual {p3, p2}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    move-result-object p2

    .line 201
    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->raw_id(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    move-result-object p2

    .line 202
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    move-result-object p2

    .line 203
    iget-object p3, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->loyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p3, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 204
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->flow:Lflow/Flow;

    new-instance p3, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;

    invoke-direct {p3, p1}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, p3}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public successMergedContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->onResult:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public supportsLoyaltyDirectoryIntegration()Z
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
