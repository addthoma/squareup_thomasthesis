.class Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "AbstractCrmScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showSaveCardToCustomerScreenV2()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V
    .locals 0

    .line 707
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 709
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v1, v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->showFirstScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method
