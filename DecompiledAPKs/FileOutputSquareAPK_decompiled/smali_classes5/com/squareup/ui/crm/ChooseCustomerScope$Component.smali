.class public interface abstract Lcom/squareup/ui/crm/ChooseCustomerScope$Component;
.super Ljava/lang/Object;
.source "ChooseCustomerScope.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/UpdateCustomerScope$ParentComponent;
.implements Lcom/squareup/ui/crm/edit/EditCustomerScope$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/ChooseCustomerScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/ChooseCustomerScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract chooseCustomerScreen()Lcom/squareup/ui/crm/ChooseCustomerScreen$Component;
.end method

.method public abstract runner()Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;
.end method
