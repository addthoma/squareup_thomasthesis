.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;
.super Ljava/lang/Object;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00fb\u0002\u0012\u00b5\u0001\u0012\u00b2\u0001\u0012D\u0012B\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00070\u0007 \u0005*X\u0012D\u0012B\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00020\u0002 \u0005*\u00bc\u0001\u0012\u00b5\u0001\u0012\u00b2\u0001\u0012D\u0012B\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00070\u0007 \u0005*X\u0012D\u0012B\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0008\u001a\n \u0005*\u0004\u0018\u00010\t0\tH\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "Lkotlin/Pair;",
        "Lcom/squareup/datafetch/Rx1AbstractLoader$Results;",
        "Lcom/squareup/crm/RolodexContactLoader$Input;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "",
        "loader",
        "Lcom/squareup/crm/RolodexContactLoader;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;

    invoke-direct {v0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;->call(Lcom/squareup/crm/RolodexContactLoader;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/crm/RolodexContactLoader;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .line 147
    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoader;->results()Lrx/Observable;

    move-result-object v0

    .line 149
    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoader;->progress()Lrx/Observable;

    move-result-object v1

    .line 151
    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoader;->failure()Lrx/Observable;

    move-result-object p1

    .line 152
    sget-object v2, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;

    check-cast v2, Lrx/functions/Func2;

    .line 148
    invoke-static {v1, p1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    .line 153
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    .line 154
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    .line 146
    invoke-static {v0, p1, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
