.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$2;->call(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/String;)Z
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getChooseCustomerScope$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/ui/crm/ChooseCustomerScope;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/ui/crm/ChooseCustomerScope;->sortByRecentContacts:Z

    if-eqz v0, :cond_0

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
