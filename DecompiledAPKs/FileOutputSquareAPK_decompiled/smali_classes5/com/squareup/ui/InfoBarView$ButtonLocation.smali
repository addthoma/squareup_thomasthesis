.class public final enum Lcom/squareup/ui/InfoBarView$ButtonLocation;
.super Ljava/lang/Enum;
.source "InfoBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/InfoBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonLocation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/InfoBarView$ButtonLocation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/InfoBarView$ButtonLocation;

.field public static final enum INLINE:Lcom/squareup/ui/InfoBarView$ButtonLocation;

.field public static final enum RIGHT_ALIGNED:Lcom/squareup/ui/InfoBarView$ButtonLocation;


# instance fields
.field private final layoutId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 17
    new-instance v0, Lcom/squareup/ui/InfoBarView$ButtonLocation;

    sget v1, Lcom/squareup/widgets/pos/R$layout;->info_bar_inline_button:I

    const/4 v2, 0x0

    const-string v3, "INLINE"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/InfoBarView$ButtonLocation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/InfoBarView$ButtonLocation;->INLINE:Lcom/squareup/ui/InfoBarView$ButtonLocation;

    new-instance v0, Lcom/squareup/ui/InfoBarView$ButtonLocation;

    sget v1, Lcom/squareup/widgets/pos/R$layout;->info_bar_right_aligned_button:I

    const/4 v3, 0x1

    const-string v4, "RIGHT_ALIGNED"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/InfoBarView$ButtonLocation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/InfoBarView$ButtonLocation;->RIGHT_ALIGNED:Lcom/squareup/ui/InfoBarView$ButtonLocation;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/InfoBarView$ButtonLocation;

    .line 16
    sget-object v1, Lcom/squareup/ui/InfoBarView$ButtonLocation;->INLINE:Lcom/squareup/ui/InfoBarView$ButtonLocation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/InfoBarView$ButtonLocation;->RIGHT_ALIGNED:Lcom/squareup/ui/InfoBarView$ButtonLocation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/InfoBarView$ButtonLocation;->$VALUES:[Lcom/squareup/ui/InfoBarView$ButtonLocation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lcom/squareup/ui/InfoBarView$ButtonLocation;->layoutId:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/InfoBarView$ButtonLocation;)I
    .locals 0

    .line 16
    iget p0, p0, Lcom/squareup/ui/InfoBarView$ButtonLocation;->layoutId:I

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/InfoBarView$ButtonLocation;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/ui/InfoBarView$ButtonLocation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/InfoBarView$ButtonLocation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/InfoBarView$ButtonLocation;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/ui/InfoBarView$ButtonLocation;->$VALUES:[Lcom/squareup/ui/InfoBarView$ButtonLocation;

    invoke-virtual {v0}, [Lcom/squareup/ui/InfoBarView$ButtonLocation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/InfoBarView$ButtonLocation;

    return-object v0
.end method
