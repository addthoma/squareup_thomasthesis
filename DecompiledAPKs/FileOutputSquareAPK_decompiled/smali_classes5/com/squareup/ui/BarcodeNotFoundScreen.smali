.class public final Lcom/squareup/ui/BarcodeNotFoundScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "BarcodeNotFoundScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/BarcodeNotFoundScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/BarcodeNotFoundScreen$Component;,
        Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/BarcodeNotFoundScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final sku:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/ui/-$$Lambda$BarcodeNotFoundScreen$CxitRgH9GR-OYPcxa1dcye7TJps;->INSTANCE:Lcom/squareup/ui/-$$Lambda$BarcodeNotFoundScreen$CxitRgH9GR-OYPcxa1dcye7TJps;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/BarcodeNotFoundScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/BarcodeNotFoundScreen;->sku:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/BarcodeNotFoundScreen;)Ljava/lang/String;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/BarcodeNotFoundScreen;->sku:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/BarcodeNotFoundScreen;
    .locals 1

    .line 70
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 71
    new-instance v0, Lcom/squareup/ui/BarcodeNotFoundScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/BarcodeNotFoundScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 66
    iget-object p2, p0, Lcom/squareup/ui/BarcodeNotFoundScreen;->sku:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 75
    sget v0, Lcom/squareup/orderentry/R$layout;->barcode_not_found:I

    return v0
.end method
