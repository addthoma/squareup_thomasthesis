.class public Lcom/squareup/ui/AndroidNfcState;
.super Lmortar/Presenter;
.source "AndroidNfcState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Landroid/app/Activity;",
        ">;"
    }
.end annotation


# instance fields
.field private final nfcAdapterShim:Lcom/squareup/util/NfcAdapterShim;


# direct methods
.method public constructor <init>(Lcom/squareup/util/NfcAdapterShim;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/AndroidNfcState;->nfcAdapterShim:Lcom/squareup/util/NfcAdapterShim;

    return-void
.end method


# virtual methods
.method protected extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;
    .locals 0

    .line 33
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 13
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/AndroidNfcState;->extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public isEnabled()Z
    .locals 2

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/AndroidNfcState;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/AndroidNfcState;->nfcAdapterShim:Lcom/squareup/util/NfcAdapterShim;

    invoke-virtual {p0}, Lcom/squareup/ui/AndroidNfcState;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/squareup/util/NfcAdapterShim;->isNfcEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
