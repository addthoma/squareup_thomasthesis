.class public Lcom/squareup/ui/MediaButtonDisabler;
.super Ljava/lang/Object;
.source "MediaButtonDisabler.java"


# instance fields
.field private final context:Landroid/app/Application;

.field private final receiver:Landroid/content/BroadcastReceiver;


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lcom/squareup/ui/MediaButtonDisabler$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/MediaButtonDisabler$1;-><init>(Lcom/squareup/ui/MediaButtonDisabler;)V

    iput-object v0, p0, Lcom/squareup/ui/MediaButtonDisabler;->receiver:Landroid/content/BroadcastReceiver;

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/MediaButtonDisabler;->context:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 2

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ui/MediaButtonDisabler;->context:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/ui/MediaButtonDisabler;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string/jumbo v1, "unregisterReceiver() failed, ignoring."

    .line 48
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .line 33
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const v1, 0x7fffffff

    .line 37
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 38
    iget-object v1, p0, Lcom/squareup/ui/MediaButtonDisabler;->context:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/ui/MediaButtonDisabler;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Application;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
