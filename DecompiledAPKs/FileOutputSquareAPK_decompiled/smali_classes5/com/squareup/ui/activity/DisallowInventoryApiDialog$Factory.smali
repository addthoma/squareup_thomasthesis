.class public final Lcom/squareup/ui/activity/DisallowInventoryApiDialog$Factory;
.super Ljava/lang/Object;
.source "DisallowInventoryApiDialog.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/DisallowInventoryApiDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/activity/DisallowInventoryApiDialog$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    const-class v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 23
    check-cast v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    .line 25
    invoke-interface {v0}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->issueRefundScopeRunner()Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object v0

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 29
    sget v2, Lcom/squareup/registerlib/R$string;->inventory_update_required_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 30
    sget v3, Lcom/squareup/registerlib/R$string;->inventory_update_required_message:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 31
    sget v4, Lcom/squareup/common/strings/R$string;->dismiss:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 32
    sget v5, Lcom/squareup/common/strings/R$string;->update:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 28
    invoke-static {v2, v3, v4, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v1

    const/4 v2, 0x0

    .line 35
    check-cast v2, Ljava/lang/Runnable;

    .line 36
    new-instance v3, Lcom/squareup/ui/activity/DisallowInventoryApiDialog$Factory$create$dialog$1;

    invoke-direct {v3, v0}, Lcom/squareup/ui/activity/DisallowInventoryApiDialog$Factory$create$dialog$1;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    check-cast v3, Ljava/lang/Runnable;

    .line 34
    invoke-virtual {v1, p1, v2, v3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 38
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(dialog)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
