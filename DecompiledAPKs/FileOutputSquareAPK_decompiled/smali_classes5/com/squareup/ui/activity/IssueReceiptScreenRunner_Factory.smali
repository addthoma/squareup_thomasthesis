.class public final Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;
.super Ljava/lang/Object;
.source "IssueReceiptScreenRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/IssueReceiptScreenRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumberHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tasksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)V"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p2, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p3, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p4, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p5, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p6, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p7, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p8, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->tasksProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p9, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p10, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p11, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p12, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->phoneNumberHelperProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p13, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)",
            "Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;"
        }
    .end annotation

    .line 93
    new-instance v14, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;Lcom/squareup/receipt/ReceiptAnalytics;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/ui/activity/IssueReceiptScreenRunner;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;",
            "Lcom/squareup/receipt/ReceiptAnalytics;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")",
            "Lcom/squareup/ui/activity/IssueReceiptScreenRunner;"
        }
    .end annotation

    .line 102
    new-instance v14, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;Lcom/squareup/receipt/ReceiptAnalytics;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/IssueReceiptScreenRunner;
    .locals 14

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/receipt/ReceiptAnalytics;

    iget-object v4, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->tasksProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->phoneNumberHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;Lcom/squareup/receipt/ReceiptAnalytics;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/ui/activity/IssueReceiptScreenRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->get()Lcom/squareup/ui/activity/IssueReceiptScreenRunner;

    move-result-object v0

    return-object v0
.end method
