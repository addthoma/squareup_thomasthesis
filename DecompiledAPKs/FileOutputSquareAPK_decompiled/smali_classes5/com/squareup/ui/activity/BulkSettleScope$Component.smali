.class public interface abstract Lcom/squareup/ui/activity/BulkSettleScope$Component;
.super Ljava/lang/Object;
.source "BulkSettleScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/BulkSettleScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract bulkSettleController()Lcom/squareup/ui/activity/BulkSettleRunner;
.end method

.method public abstract bulkSettleScreen()Lcom/squareup/ui/activity/BulkSettleScreen$Component;
.end method

.method public abstract bulkTipSettlementFailedDialog()Lcom/squareup/ui/activity/BulkTipSettlementFailedDialogScreen$Component;
.end method

.method public abstract closeWithoutSettlingDialog()Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen$Component;
.end method
