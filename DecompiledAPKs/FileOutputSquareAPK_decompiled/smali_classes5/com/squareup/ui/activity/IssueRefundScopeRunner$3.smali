.class Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;
.super Ljava/lang/Object;
.source "IssueRefundScopeRunner.java"

# interfaces
.implements Lcom/squareup/cardreader/EmvListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/IssueRefundScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V
    .locals 0

    .line 1097
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1117
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    .line 1118
    invoke-static {p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$200(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getFirstTenderRequiringCardAuthorization()Lcom/squareup/activity/refund/TenderDetails;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getAccountType()Lcom/squareup/protos/client/bills/CardTender$AccountType;

    move-result-object p1

    .line 1117
    invoke-static {p1}, Lcom/squareup/payment/tender/SmartCardTender;->cardreaderAccountTypeOrDefault(Lcom/squareup/protos/client/bills/CardTender$AccountType;)Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    move-result-object p1

    .line 1119
    sget-object p2, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    if-ne p1, p2, :cond_0

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string p3, "Account selection required: Falling back to default account type."

    .line 1120
    invoke-static {p3, p2}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1122
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {p2}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$000(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/cardreader/CardReader;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method public onCardError()V
    .locals 1

    .line 1099
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$100(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 1

    .line 1103
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$100(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 0

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 0

    return-void
.end method

.method public onMagFallbackSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0

    return-void
.end method

.method public onMagFallbackSwipeSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    return-void
.end method

.method public onSigRequested()V
    .locals 0

    return-void
.end method

.method public onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 0

    return-void
.end method

.method public onUseChipCardDuringFallback()V
    .locals 0

    return-void
.end method

.method public sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 0

    .line 1133
    iget-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    sget-object p3, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 1134
    invoke-static {p3, p1}, Lcom/squareup/ui/activity/ReaderResult;->authorizationOf(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p1

    .line 1133
    invoke-static {p2, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$300(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method
