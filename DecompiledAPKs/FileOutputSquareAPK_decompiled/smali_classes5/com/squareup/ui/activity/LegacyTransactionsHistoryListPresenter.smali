.class public Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
.super Lmortar/ViewPresenter;
.source "LegacyTransactionsHistoryListPresenter.java"

# interfaces
.implements Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;,
        Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;,
        Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Header;,
        Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;,
        Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;,
        Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/activity/TransactionsHistoryListView;",
        ">;",
        "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;"
    }
.end annotation


# static fields
.field private static final INSTANT_DEPOSIT_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

.field private static final LOAD_MORE_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

.field private static final OMIT_HEADER:I = -0x2

.field private static final SHOW_ALL_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

.field private static final USE_LAST_HEADER:I = -0x1


# instance fields
.field private final analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

.field private final checkIfEligibleForInstantDeposit:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final currentBill:Lcom/squareup/activity/CurrentBill;

.field private final dayAndDateFormatter:Lcom/squareup/text/DayAndDateFormatter;

.field private final device:Lcom/squareup/util/Device;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final headers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Header;",
            ">;"
        }
    .end annotation
.end field

.field private final instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

.field private final legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

.field private loadMoreState:Lcom/squareup/ui/LoadMoreState;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field private final rows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;",
            ">;"
        }
    .end annotation
.end field

.field private selectedPosition:I

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 157
    new-instance v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->INSTANT_DEPOSIT:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;Lcom/squareup/billhistory/model/BillHistory;I)V

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->INSTANT_DEPOSIT_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    .line 159
    new-instance v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->LOAD_MORE:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;Lcom/squareup/billhistory/model/BillHistory;I)V

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->LOAD_MORE_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    .line 160
    new-instance v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->SHOW_ALL:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;Lcom/squareup/billhistory/model/BillHistory;I)V

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->SHOW_ALL_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    return-void
.end method

.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;Lcom/squareup/activity/CurrentBill;Ljavax/inject/Provider;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/activity/CurrentBill;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            "Lcom/squareup/activity/ExpiryCalculator;",
            "Lcom/squareup/text/DayAndDateFormatter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 198
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 179
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->checkIfEligibleForInstantDeposit:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 180
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 185
    sget-object v1, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    move-object v1, p1

    .line 199
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->flow:Lflow/Flow;

    move-object v1, p13

    .line 200
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    move-object v1, p2

    .line 201
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->device:Lcom/squareup/util/Device;

    move-object v1, p3

    .line 202
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    move-object v1, p4

    .line 203
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object v1, p5

    .line 204
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    move-object v1, p6

    .line 205
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p7

    .line 206
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    move-object v1, p8

    .line 207
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 208
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    move-object v1, p10

    .line 209
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p11

    .line 210
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    move-object v1, p12

    .line 211
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    move-object/from16 v1, p14

    .line 212
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->dayAndDateFormatter:Lcom/squareup/text/DayAndDateFormatter;

    move-object/from16 v1, p15

    .line 213
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p16

    .line 214
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object/from16 v1, p17

    .line 215
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 217
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    .line 218
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/text/DayAndDateFormatter;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->dayAndDateFormatter:Lcom/squareup/text/DayAndDateFormatter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;ILcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->formatOfflinePayments(ILcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lflow/Flow;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->flow:Lflow/Flow;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->onInstantDepositRequested(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/instantdeposit/InstantDepositRunner;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lio/reactivex/disposables/CompositeDisposable;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/instantdeposit/InstantDepositAnalytics;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    return-object p0
.end method

.method private determineBadgeForBill(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;
    .locals 1

    .line 675
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/ExpiryCalculator;->isExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    sget-object p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->EXPIRING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    return-object p1

    .line 677
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 678
    sget-object p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->ERROR:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    return-object p1

    .line 679
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->hasMultipleBills()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->hasBillsThatAreNotFailedRefunds(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 680
    sget-object p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->REFUND:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    return-object p1

    .line 681
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->isAwaitingMerchantTip()Z

    move-result v0

    if-nez v0, :cond_4

    .line 682
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->isStoreAndForward()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p1, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-eqz v0, :cond_3

    .line 683
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->hasCardTender()Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    return-object p1

    .line 688
    :cond_4
    :goto_0
    sget-object p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->PENDING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    return-object p1
.end method

.method private formatDateForBill(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .line 721
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryListView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 722
    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 723
    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private formatNote(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;
    .locals 5

    .line 735
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryListView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 738
    iget-object v1, p1, Lcom/squareup/billhistory/model/BillHistory;->note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->localizedNote(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/String;

    move-result-object v1

    .line 741
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->hasNonZeroTip()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 742
    iget-object v2, p1, Lcom/squareup/billhistory/model/BillHistory;->tip:Lcom/squareup/protos/common/Money;

    .line 743
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string/jumbo v4, "tip"

    if-eqz v3, :cond_0

    .line 745
    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 746
    sget v1, Lcom/squareup/billhistoryui/R$string;->payment_note_tip:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 747
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 748
    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 749
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 750
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 753
    :cond_0
    sget p1, Lcom/squareup/billhistoryui/R$string;->payment_note_tip_itemized:I

    invoke-static {v0, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 754
    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "itemization"

    .line 755
    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 756
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 757
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private formatOfflinePayments(ILcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 3

    const-string v0, "amount"

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 709
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->uppercase_offline_payments_history_header_one:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 710
    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    .line 712
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->uppercase_offline_payments_history_header:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "count"

    .line 713
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 714
    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 716
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private hasBillsThatAreNotFailedRefunds(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    .line 698
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getRelatedBillsExceptFirst()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 699
    invoke-virtual {v0}, Lcom/squareup/server/payment/RelatedBillHistory;->isFailedOrRejected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private notifyDataSetChanged()V
    .locals 1

    .line 765
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryListView;

    if-eqz v0, :cond_0

    .line 767
    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method private onBillClicked(Lcom/squareup/billhistory/model/BillHistory;I)V
    .locals 0

    .line 564
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->setSelectedBill(Lcom/squareup/billhistory/model/BillHistory;I)V

    .line 567
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->notifyDataSetChanged()V

    .line 569
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 570
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryListView;

    .line 571
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 572
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->flow:Lflow/Flow;

    sget-object p2, Lcom/squareup/ui/activity/BillHistoryDetailScreen;->INSTANCE:Lcom/squareup/ui/activity/BillHistoryDetailScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private onCanMakeDeposit(Lcom/squareup/ui/activity/InstantDepositRowView;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 2

    .line 481
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->headerDisplayedShowingAvailableBalance()V

    const/4 v0, 0x0

    .line 483
    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/InstantDepositRowView;->setLoading(Z)V

    const/4 v0, 0x0

    .line 484
    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/InstantDepositRowView;->setErrorText(Ljava/lang/CharSequence;)V

    .line 485
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0, p2}, Lcom/squareup/instantdeposit/InstantDepositRunner;->instantDepositHint(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/InstantDepositRowView;->setHintText(Ljava/lang/CharSequence;)V

    .line 486
    iget-boolean v0, p2, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    if-eqz v0, :cond_0

    .line 487
    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->confirm_instant_transfer:I

    .line 488
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 487
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/InstantDepositRowView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 489
    invoke-virtual {p1}, Lcom/squareup/ui/activity/InstantDepositRowView;->setButtonColorBlue()V

    goto :goto_0

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_button_text:I

    .line 492
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 493
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "amount"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 494
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 491
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/InstantDepositRowView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 496
    invoke-virtual {p1}, Lcom/squareup/ui/activity/InstantDepositRowView;->setButtonColorWhite()V

    :goto_0
    return-void
.end method

.method private onInstantDepositRequested(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 4

    .line 541
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 543
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v2, Lcom/squareup/permissions/Permission;->USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

    new-instance v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;

    invoke-direct {v3, p0, p1, v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/protos/common/Money;)V

    invoke-virtual {v1, v2, v3}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method private onNotEligible(Lcom/squareup/ui/activity/InstantDepositRowView;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 1

    .line 501
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->headerDisplayedShowingError()V

    const/4 v0, 0x0

    .line 503
    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/InstantDepositRowView;->setLoading(Z)V

    .line 504
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/InstantDepositRowView;->setErrorText(Ljava/lang/CharSequence;)V

    .line 505
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/InstantDepositRowView;->setErrorHintText(Ljava/lang/String;)V

    .line 507
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityBlocker()Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    move-result-object p2

    .line 508
    sget-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$protos$client$instantdeposits$EligibilityBlocker:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    .line 521
    invoke-virtual {p1}, Lcom/squareup/ui/activity/InstantDepositRowView;->hideButton()V

    goto :goto_0

    .line 516
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/debitcard/R$string;->instant_deposits_resend_email:I

    .line 517
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 516
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/InstantDepositRowView;->setButtonText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 511
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->instant_deposits_update_debit_card_information:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/InstantDepositRowView;->setButtonText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private rebuildRows(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)V"
        }
    .end annotation

    .line 623
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 624
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 626
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->showInstantDepositRow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->INSTANT_DEPOSIT_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 633
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->getMaxBillsWithoutPermission()I

    move-result v0

    .line 634
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->isPermissionGranted()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_2

    .line 635
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_3

    .line 638
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-interface {p1, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 639
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->selectFirstBillIfNoneSelectedInList(Ljava/util/List;)V

    .line 642
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    move-object v2, v0

    move-object v4, v2

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/billhistory/model/BillHistory;

    .line 643
    iget-boolean v6, v5, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-nez v6, :cond_7

    invoke-virtual {v5}, Lcom/squareup/billhistory/model/BillHistory;->isStoreAndForward()Z

    move-result v6

    if-eqz v6, :cond_4

    goto :goto_3

    :cond_4
    if-eqz v2, :cond_5

    .line 651
    iget-object v6, v5, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    invoke-static {v2, v6}, Lcom/squareup/util/Times;->onDifferentDay(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 652
    :cond_5
    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 654
    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    new-instance v6, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    sget-object v7, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->HEADER_PADDING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    iget-object v8, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    sub-int/2addr v8, v3

    invoke-direct {v6, v7, v0, v8}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;Lcom/squareup/billhistory/model/BillHistory;I)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 658
    :cond_6
    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    new-instance v6, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;

    iget-object v7, v5, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    invoke-direct {v6, p0, v7}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Ljava/util/Date;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 659
    iget-object v2, v5, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    goto :goto_4

    :cond_7
    :goto_3
    if-nez v4, :cond_8

    .line 646
    new-instance v4, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;

    invoke-direct {v4, p0, v5}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 647
    iget-object v6, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 649
    :cond_8
    invoke-virtual {v4, v5}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->addBill(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 662
    :cond_9
    :goto_4
    iget-object v6, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    new-instance v7, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    sget-object v8, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->BILL:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    iget-object v9, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    sub-int/2addr v9, v3

    invoke-direct {v7, v8, v5, v9}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;Lcom/squareup/billhistory/model/BillHistory;I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_a
    if-nez v1, :cond_b

    .line 666
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    sget-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->SHOW_ALL_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 667
    :cond_b
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    iget-boolean p1, p1, Lcom/squareup/ui/LoadMoreState;->showsRow:Z

    if-eqz p1, :cond_c

    .line 668
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    sget-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->LOAD_MORE_ROW:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 671
    :cond_c
    :goto_5
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->notifyDataSetChanged()V

    return-void
.end method

.method private selectFirstBillIfNoneSelected(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)V"
        }
    .end annotation

    .line 596
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 601
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->isSet()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v1}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasBillWithId(Lcom/squareup/billhistory/model/BillHistoryId;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    .line 603
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->setSelectedBill(Lcom/squareup/billhistory/model/BillHistory;I)V

    :cond_2
    return-void
.end method

.method private selectFirstBillIfNoneSelectedInList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)V"
        }
    .end annotation

    .line 614
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory;

    .line 615
    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v2, v1}, Lcom/squareup/activity/CurrentBill;->isEqualTo(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 619
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->setSelectedBill(Lcom/squareup/billhistory/model/BillHistory;I)V

    return-void
.end method


# virtual methods
.method calculateLoadMoreState()Lcom/squareup/ui/LoadMoreState;
    .locals 3

    .line 441
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getState()Lcom/squareup/activity/LoaderState;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/LoaderState;->FAILED:Lcom/squareup/activity/LoaderState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 442
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasMore()Z

    move-result v1

    .line 444
    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v2}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBills()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 447
    sget-object v0, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    return-object v0

    :cond_1
    if-eqz v0, :cond_2

    .line 451
    sget-object v0, Lcom/squareup/ui/LoadMoreState;->NO_CONNECTION_RETRY:Lcom/squareup/ui/LoadMoreState;

    return-object v0

    :cond_2
    if-eqz v1, :cond_3

    .line 455
    sget-object v0, Lcom/squareup/ui/LoadMoreState;->LOADING:Lcom/squareup/ui/LoadMoreState;

    return-object v0

    .line 458
    :cond_3
    sget-object v0, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    return-object v0
.end method

.method doesBillHaveError(I)Z
    .locals 1

    .line 406
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 407
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/ExpiryCalculator;->isExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public dropView(Lcom/squareup/ui/activity/TransactionsHistoryListView;)V
    .locals 1

    .line 255
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->removeOnPermissionGrantedListener(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;)V

    .line 258
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryListView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->dropView(Lcom/squareup/ui/activity/TransactionsHistoryListView;)V

    return-void
.end method

.method getBillBadge(I)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->determineBadgeForBill(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    move-result-object p1

    return-object p1
.end method

.method getBillDateText(I)Ljava/lang/String;
    .locals 1

    .line 387
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 388
    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->formatDateForBill(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getBillIcon(I)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 392
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 393
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    return-object p1
.end method

.method getBillRowSubtitle(I)Ljava/lang/String;
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 374
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/ExpiryCalculator;->isStoredPaymentExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->payment_expiring:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 377
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->formatNote(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object p1

    .line 378
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    :cond_1
    return-object p1
.end method

.method getBillRowTitle(I)Ljava/lang/String;
    .locals 3

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 350
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    .line 351
    iget-object v1, p1, Lcom/squareup/billhistory/model/BillHistory;->refundAmount:Lcom/squareup/protos/common/Money;

    .line 352
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 353
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistory/R$string;->no_sale:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 354
    :cond_0
    iget-boolean v2, p1, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    if-eqz v2, :cond_1

    .line 355
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->void_transactions_history:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 356
    :cond_1
    iget-boolean p1, p1, Lcom/squareup/billhistory/model/BillHistory;->isRefund:Z

    if-eqz p1, :cond_2

    .line 357
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {v1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 359
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getBillRowTitleColor(I)I
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 365
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean p1, p1, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    if-eqz p1, :cond_0

    goto :goto_0

    .line 368
    :cond_0
    sget p1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    return p1

    .line 366
    :cond_1
    :goto_0
    sget p1, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    return p1
.end method

.method getBillVectorImageResId(I)I
    .locals 1

    .line 397
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 398
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getImageResId()I

    move-result p1

    return p1
.end method

.method getHeaderIndexForRow(I)I
    .locals 1

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->headerId:I

    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    return p1

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 426
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    :cond_1
    return p1
.end method

.method getHeaderText(I)Ljava/lang/CharSequence;
    .locals 1

    .line 343
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getHeaderIndexForRow(I)I

    move-result p1

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->headers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Header;

    .line 345
    invoke-interface {p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Header;->getHeaderText()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method getLoadMoreState()Lcom/squareup/ui/LoadMoreState;
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    return-object v0
.end method

.method getRowBillForUiTests(I)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-object p1
.end method

.method public getRowCount()I
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRowType(I)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->type:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    return-object p1
.end method

.method instantDepositRunnerSnapshot()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 462
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method isDisplayedAsSidebar()Z
    .locals 1

    .line 437
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method isHeaderOmitted(I)Z
    .locals 1

    .line 339
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getHeaderIndexForRow(I)I

    move-result p1

    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method isRowClickable(I)Z
    .locals 3

    .line 276
    sget-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$ui$activity$LegacyTransactionsHistoryListPresenter$RowType:[I

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->type:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    return v2

    :cond_0
    return v0

    .line 280
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->NO_CONNECTION_RETRY:Lcom/squareup/ui/LoadMoreState;

    if-ne p1, v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_0
    return v0
.end method

.method isRowSelected(I)Z
    .locals 1

    .line 411
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 413
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/CurrentBill;->isEqualTo(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$null$1$LegacyTransactionsHistoryListPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 243
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->updateView()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$LegacyTransactionsHistoryListPresenter(ZLkotlin/Unit;)Lio/reactivex/SingleSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 229
    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {p2, p1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$2$LegacyTransactionsHistoryListPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->onChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryListPresenter$gTqD2SduEZYnzhK6EDjAMSjU2ik;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryListPresenter$gTqD2SduEZYnzhK6EDjAMSjU2ik;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method loadMoreIfNecessary()V
    .locals 2

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->LOADING:Lcom/squareup/ui/LoadMoreState;

    if-ne v0, v1, :cond_0

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->loadMore()V

    :cond_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->checkIfEligibleForInstantDeposit:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryListPresenter$4nRvya7qdDV-CH7djkBSiin0Wn4;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryListPresenter$4nRvya7qdDV-CH7djkBSiin0Wn4;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Z)V

    .line 228
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 227
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 232
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->queryInstantDeposits()V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 237
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method onInstantDepositButtonClicked(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 3

    .line 526
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 241
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryListView;

    .line 243
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryListPresenter$bvyzLzeOeWBG1-I9bhYqXeZf07c;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryListPresenter$bvyzLzeOeWBG1-I9bhYqXeZf07c;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->addOnPermissionGrantedListener(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;)V

    .line 247
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->updateView()V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->selectedPosition:I

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBills()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 250
    iget v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->selectedPosition:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public onRowClicked(I)V
    .locals 3

    .line 319
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;

    .line 323
    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$ui$activity$LegacyTransactionsHistoryListPresenter$RowType:[I

    iget-object v2, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->type:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 p1, 0x2

    if-eq v1, p1, :cond_2

    const/4 p1, 0x3

    if-eq v1, p1, :cond_1

    goto :goto_0

    .line 329
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->requestPermission()V

    goto :goto_0

    .line 333
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->loadMore()V

    goto :goto_0

    .line 326
    :cond_3
    iget-object v0, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->onBillClicked(Lcom/squareup/billhistory/model/BillHistory;I)V

    :goto_0
    return-void
.end method

.method public onShowFullHistoryPermissionGranted()V
    .locals 0

    .line 262
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->updateView()V

    return-void
.end method

.method queryInstantDeposits()V
    .locals 2

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->checkIfEligibleForInstantDeposit:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method setLoadMoreState(Lcom/squareup/ui/LoadMoreState;)V
    .locals 1

    .line 578
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    if-eq v0, p1, :cond_0

    .line 579
    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    .line 580
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBills()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rebuildRows(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method setSelectedBill(Lcom/squareup/billhistory/model/BillHistory;I)V
    .locals 1

    .line 585
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/CurrentBill;->set(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 586
    iput p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->selectedPosition:I

    return-void
.end method

.method showInstantDepositRow()Z
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 290
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->showInstantDeposit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

    .line 291
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    .line 292
    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method updateInstantDepositRowView(Lcom/squareup/ui/activity/InstantDepositRowView;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 2

    .line 467
    sget-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    iget-object v1, p2, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 475
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->onNotEligible(Lcom/squareup/ui/activity/InstantDepositRowView;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    goto :goto_0

    .line 472
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->onCanMakeDeposit(Lcom/squareup/ui/activity/InstantDepositRowView;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    goto :goto_0

    .line 469
    :cond_2
    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/InstantDepositRowView;->setLoading(Z)V

    :goto_0
    return-void
.end method

.method public updateView()V
    .locals 3

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBills()Ljava/util/List;

    move-result-object v0

    .line 267
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->rebuildRows(Ljava/util/List;)V

    .line 269
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->calculateLoadMoreState()Lcom/squareup/ui/LoadMoreState;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->setLoadMoreState(Lcom/squareup/ui/LoadMoreState;)V

    .line 270
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 271
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->selectFirstBillIfNoneSelected(Ljava/util/List;)V

    :cond_0
    return-void
.end method
