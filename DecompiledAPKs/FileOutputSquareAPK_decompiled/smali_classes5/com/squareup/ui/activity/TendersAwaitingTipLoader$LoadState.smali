.class public final enum Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;
.super Ljava/lang/Enum;
.source "TendersAwaitingTipLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/TendersAwaitingTipLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LoadState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

.field public static final enum ERROR:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

.field public static final enum LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

.field public static final enum LOADING:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

.field public static final enum NOT_LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 55
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    const/4 v1, 0x0

    const-string v2, "NOT_LOADED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->NOT_LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    .line 56
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    const/4 v2, 0x1

    const-string v3, "LOADING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADING:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    .line 57
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    const/4 v3, 0x2

    const-string v4, "LOADED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    .line 58
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    const/4 v4, 0x3

    const-string v5, "ERROR"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->ERROR:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    .line 54
    sget-object v5, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->NOT_LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADING:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->ERROR:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->$VALUES:[Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;
    .locals 1

    .line 54
    const-class v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->$VALUES:[Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    invoke-virtual {v0}, [Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    return-object v0
.end method
