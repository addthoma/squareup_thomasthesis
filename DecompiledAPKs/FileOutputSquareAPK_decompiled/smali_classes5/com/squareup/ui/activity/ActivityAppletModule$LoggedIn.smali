.class public abstract Lcom/squareup/ui/activity/ActivityAppletModule$LoggedIn;
.super Ljava/lang/Object;
.source "ActivityAppletModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivityAppletModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LoggedIn"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/activity/ActivityAppletModule$LoggedIn;",
        "",
        "()V",
        "providePendingPayments",
        "Lcom/squareup/notifications/PendingPaymentNotifier;",
        "pendingPaymentStatusBarNotifier",
        "Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;",
        "providePendingPayments$android_release",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract providePendingPayments$android_release(Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;)Lcom/squareup/notifications/PendingPaymentNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
