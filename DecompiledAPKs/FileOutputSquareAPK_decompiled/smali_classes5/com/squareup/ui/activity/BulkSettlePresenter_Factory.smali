.class public final Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;
.super Ljava/lang/Object;
.source "BulkSettlePresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/BulkSettlePresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final bulkSettleRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;"
        }
    .end annotation
.end field

.field private final expiryCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tendersLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/TendersAwaitingTipLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/TendersAwaitingTipLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleRunner;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->timeFormatProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->tendersLoaderProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->employeesProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->bulkSettleRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/TendersAwaitingTipLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleRunner;",
            ">;)",
            "Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;"
        }
    .end annotation

    .line 92
    new-instance v13, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/Employees;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/activity/BulkSettleRunner;)Lcom/squareup/ui/activity/BulkSettlePresenter;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/activity/TendersAwaitingTipLoader;",
            "Lcom/squareup/activity/ExpiryCalculator;",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/permissions/Employees;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/activity/BulkSettleRunner;",
            ")",
            "Lcom/squareup/ui/activity/BulkSettlePresenter;"
        }
    .end annotation

    .line 101
    new-instance v13, Lcom/squareup/ui/activity/BulkSettlePresenter;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/activity/BulkSettlePresenter;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/Employees;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/activity/BulkSettleRunner;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/BulkSettlePresenter;
    .locals 13

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->timeFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->tendersLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/activity/ExpiryCalculator;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/EmployeeManagementSettings;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->employeesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/permissions/Employees;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->bulkSettleRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/Employees;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/activity/BulkSettleRunner;)Lcom/squareup/ui/activity/BulkSettlePresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter_Factory;->get()Lcom/squareup/ui/activity/BulkSettlePresenter;

    move-result-object v0

    return-object v0
.end method
