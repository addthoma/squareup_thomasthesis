.class public Lcom/squareup/ui/activity/RefundPinDialog;
.super Lcom/squareup/ui/activity/InIssueRefundScope;
.source "RefundPinDialog.java"

# interfaces
.implements Lcom/squareup/ui/buyer/PaymentExempt;
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogCardScreen;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;
    }
.end annotation


# instance fields
.field private final params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;Lcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 0

    .line 25
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/InIssueRefundScope;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/activity/RefundPinDialog;->params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public params()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundPinDialog;->params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    return-object v0
.end method
