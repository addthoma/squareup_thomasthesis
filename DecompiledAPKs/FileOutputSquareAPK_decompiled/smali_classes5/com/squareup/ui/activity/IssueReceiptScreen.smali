.class public final Lcom/squareup/ui/activity/IssueReceiptScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "IssueReceiptScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/IssueReceiptScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/IssueReceiptScreen$Module;,
        Lcom/squareup/ui/activity/IssueReceiptScreen$Component;
    }
.end annotation


# instance fields
.field public isFirstScreen:Z

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 45
    iput-boolean p2, p0, Lcom/squareup/ui/activity/IssueReceiptScreen;->isFirstScreen:Z

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->REISSUE_RECEIPT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    .line 62
    iget-boolean v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreen;->isFirstScreen:Z

    if-eqz v0, :cond_1

    .line 63
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 2

    .line 68
    const-class v0, Lcom/squareup/ui/activity/IssueReceiptScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/IssueReceiptScreen$Component;

    .line 69
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueReceiptScreen$Component;->issueReceiptCoordinatorFactory()Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;

    move-result-object v0

    .line 70
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueReceiptScreen$Component;->issueReceiptScreenRunner()Lcom/squareup/ui/activity/IssueReceiptScreenRunner;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->screenData()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->create(Lrx/Observable;Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;)Lcom/squareup/activity/ui/IssueReceiptCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 75
    const-class v0, Lcom/squareup/ui/activity/IssueReceiptScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/IssueReceiptScreen$Component;

    .line 76
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    .line 77
    invoke-interface {v0}, Lcom/squareup/ui/activity/IssueReceiptScreen$Component;->issueReceiptScreenRunner()Lcom/squareup/ui/activity/IssueReceiptScreenRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 97
    sget v0, Lcom/squareup/activity/R$layout;->activity_applet_issue_receipt_view:I

    return v0
.end method
