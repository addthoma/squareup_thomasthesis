.class public Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;
.super Ljava/lang/Object;
.source "IssueRefundScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/IssueRefundScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RefundContext"
.end annotation


# instance fields
.field final authorizedEmployeeToken:Ljava/lang/String;

.field final billHistory:Lcom/squareup/billhistory/model/BillHistory;

.field final itemizationMaxReturnableQuantity:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field public final refundData:Lcom/squareup/activity/refund/RefundData;

.field public final skipsRestock:Z


# direct methods
.method public constructor <init>(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ZLjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/activity/refund/RefundData;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;)V"
        }
    .end annotation

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->refundData:Lcom/squareup/activity/refund/RefundData;

    .line 193
    iput-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    .line 194
    iput-object p3, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->authorizedEmployeeToken:Ljava/lang/String;

    .line 195
    iput-boolean p4, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->skipsRestock:Z

    .line 196
    iput-object p5, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->itemizationMaxReturnableQuantity:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public isExchangePrepared()Z
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->refundData:Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->isExchange()Z

    move-result v0

    return v0
.end method

.method public prepareForExchange(Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;
    .locals 7

    .line 219
    new-instance v6, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->refundData:Lcom/squareup/activity/refund/RefundData;

    .line 220
    invoke-virtual {v0, p1}, Lcom/squareup/activity/refund/RefundData;->copyAsExchange(Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v3, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->authorizedEmployeeToken:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->skipsRestock:Z

    iget-object v5, p0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->itemizationMaxReturnableQuantity:Ljava/util/Map;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;-><init>(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ZLjava/util/Map;)V

    return-object v6
.end method
