.class public final Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;
.super Ljava/lang/Object;
.source "TendersAwaitingTipLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/TendersAwaitingTipLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderListerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderTipLister;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderTipLister;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;->tenderListerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;->resProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;->employeesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderTipLister;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;)",
            "Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/papersignature/TenderTipLister;Lcom/squareup/util/Res;Lcom/squareup/permissions/Employees;)Lcom/squareup/ui/activity/TendersAwaitingTipLoader;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;-><init>(Lcom/squareup/papersignature/TenderTipLister;Lcom/squareup/util/Res;Lcom/squareup/permissions/Employees;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/TendersAwaitingTipLoader;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;->tenderListerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/papersignature/TenderTipLister;

    iget-object v1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;->employeesProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/permissions/Employees;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;->newInstance(Lcom/squareup/papersignature/TenderTipLister;Lcom/squareup/util/Res;Lcom/squareup/permissions/Employees;)Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader_Factory;->get()Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    move-result-object v0

    return-object v0
.end method
