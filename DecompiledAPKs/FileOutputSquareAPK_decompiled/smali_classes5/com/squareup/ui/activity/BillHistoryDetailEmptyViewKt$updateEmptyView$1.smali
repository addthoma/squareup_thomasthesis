.class final Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$updateEmptyView$1;
.super Lkotlin/jvm/internal/Lambda;
.source "BillHistoryDetailEmptyView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;->updateEmptyView(Lcom/squareup/mosaic/core/Root;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBillHistoryDetailEmptyView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BillHistoryDetailEmptyView.kt\ncom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$updateEmptyView$1\n+ 2 BlueprintUiModel.kt\ncom/squareup/blueprint/mosaic/BlueprintUiModelKt\n+ 3 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt\n+ 4 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt$center$1\n*L\n1#1,57:1\n17#2,11:58\n26#2:69\n22#2,3:70\n28#2:102\n113#3,9:73\n119#3:82\n120#3:84\n141#3:85\n113#3,9:86\n119#3:95\n120#3,3:97\n142#3:100\n122#3:101\n113#4:83\n113#4:96\n*E\n*S KotlinDebug\n*F\n+ 1 BillHistoryDetailEmptyView.kt\ncom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$updateEmptyView$1\n*L\n26#1,11:58\n26#1:69\n26#1,3:70\n26#1:102\n26#1,9:73\n26#1:82\n26#1:84\n26#1:85\n26#1,9:86\n26#1:95\n26#1,3:97\n26#1:100\n26#1:101\n26#1:83\n26#1:96\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00010\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $labelResId:I


# direct methods
.method constructor <init>(I)V
    .locals 0

    iput p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$updateEmptyView$1;->$labelResId:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/mosaic/core/UiModelContext;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$updateEmptyView$1;->invoke(Lcom/squareup/mosaic/core/UiModelContext;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/mosaic/core/UiModelContext;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "$receiver"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    new-instance v1, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    .line 70
    invoke-interface/range {p1 .. p1}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v1

    .line 62
    invoke-direct/range {v2 .. v8}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;-><init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 27
    move-object v2, v1

    check-cast v2, Lcom/squareup/blueprint/BlueprintContext;

    sget-object v5, Lcom/squareup/blueprint/CenterBlock$Type;->VERTICALLY:Lcom/squareup/blueprint/CenterBlock$Type;

    .line 82
    invoke-interface {v2}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v4

    .line 78
    new-instance v9, Lcom/squareup/blueprint/CenterBlock;

    const/4 v7, 0x4

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    move-object v3, v9

    check-cast v3, Lcom/squareup/blueprint/BlueprintContext;

    .line 85
    new-instance v4, Lcom/squareup/blueprint/VerticalBlock;

    invoke-interface {v3}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x6

    const/4 v15, 0x0

    move-object v10, v4

    invoke-direct/range {v10 .. v15}, Lcom/squareup/blueprint/VerticalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 29
    move-object v5, v4

    check-cast v5, Lcom/squareup/blueprint/BlueprintContext;

    sget-object v12, Lcom/squareup/blueprint/CenterBlock$Type;->HORIZONTALLY:Lcom/squareup/blueprint/CenterBlock$Type;

    .line 95
    invoke-interface {v5}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v11

    .line 91
    new-instance v6, Lcom/squareup/blueprint/CenterBlock;

    const/4 v13, 0x0

    const/4 v14, 0x4

    move-object v10, v6

    invoke-direct/range {v10 .. v15}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 30
    move-object v7, v6

    check-cast v7, Lcom/squareup/blueprint/BlueprintContext;

    invoke-static {v7}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;->access$cardImageModel(Lcom/squareup/blueprint/BlueprintContext;)V

    .line 31
    check-cast v6, Lcom/squareup/blueprint/Block;

    .line 90
    invoke-interface {v5, v6}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 32
    move-object v6, v4

    check-cast v6, Lcom/squareup/blueprint/LinearBlock;

    invoke-static {v6}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;->access$smallSpacing(Lcom/squareup/blueprint/LinearBlock;)V

    move-object/from16 v6, p0

    .line 33
    iget v7, v6, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$updateEmptyView$1;->$labelResId:I

    invoke-static {v5, v7}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;->access$cardLabelModel(Lcom/squareup/blueprint/BlueprintContext;I)V

    .line 34
    check-cast v4, Lcom/squareup/blueprint/Block;

    .line 85
    invoke-interface {v3, v4}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 35
    check-cast v9, Lcom/squareup/blueprint/Block;

    .line 77
    invoke-interface {v2, v9}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 68
    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {v0, v1}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
