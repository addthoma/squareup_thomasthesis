.class public final Lcom/squareup/ui/activity/QuickTipEditor_MembersInjector;
.super Ljava/lang/Object;
.source "QuickTipEditor_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/QuickTipEditor;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/QuickTipEditorPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/QuickTipEditorPresenter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/QuickTipEditorPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/QuickTipEditor;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/activity/QuickTipEditor_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/QuickTipEditor_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/activity/QuickTipEditor;Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    iput-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/QuickTipEditor;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/QuickTipEditor_MembersInjector;->injectPresenter(Lcom/squareup/ui/activity/QuickTipEditor;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/QuickTipEditor_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/QuickTipEditor;)V

    return-void
.end method
