.class public Lcom/squareup/ui/activity/IssueRefundScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "IssueRefundScope.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/IssueRefundScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;,
        Lcom/squareup/ui/activity/IssueRefundScope$Component;,
        Lcom/squareup/ui/activity/IssueRefundScope$ParentComponent;
    }
.end annotation


# instance fields
.field final authorizedEmployeeToken:Ljava/lang/String;

.field final bill:Lcom/squareup/billhistory/model/BillHistory;

.field final inProgressRefundData:Lcom/squareup/activity/refund/RefundData;

.field final itemizationMaxReturnableQuantity:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field final skipsRestock:Z


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V
    .locals 6

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 74
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/activity/IssueRefundScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ZLjava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ZLjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;)V"
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 85
    iput-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScope;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 86
    iput-object p3, p0, Lcom/squareup/ui/activity/IssueRefundScope;->authorizedEmployeeToken:Ljava/lang/String;

    .line 87
    iput-boolean p4, p0, Lcom/squareup/ui/activity/IssueRefundScope;->skipsRestock:Z

    .line 88
    iput-object p5, p0, Lcom/squareup/ui/activity/IssueRefundScope;->itemizationMaxReturnableQuantity:Ljava/util/Map;

    const/4 p1, 0x0

    .line 89
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScope;->inProgressRefundData:Lcom/squareup/activity/refund/RefundData;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 97
    iget-object p1, p2, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScope;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 98
    iget-object p1, p2, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->authorizedEmployeeToken:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScope;->authorizedEmployeeToken:Ljava/lang/String;

    .line 99
    iget-boolean p1, p2, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->skipsRestock:Z

    iput-boolean p1, p0, Lcom/squareup/ui/activity/IssueRefundScope;->skipsRestock:Z

    .line 100
    iget-object p1, p2, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->itemizationMaxReturnableQuantity:Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScope;->itemizationMaxReturnableQuantity:Ljava/util/Map;

    .line 101
    iget-object p1, p2, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;->refundData:Lcom/squareup/activity/refund/RefundData;

    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScope;->inProgressRefundData:Lcom/squareup/activity/refund/RefundData;

    return-void
.end method


# virtual methods
.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScope;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 119
    const-class v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    .line 120
    invoke-interface {v0}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->issueRefundScopeRunner()Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
