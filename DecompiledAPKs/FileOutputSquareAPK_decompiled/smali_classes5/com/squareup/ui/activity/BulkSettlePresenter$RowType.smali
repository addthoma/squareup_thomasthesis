.class public final enum Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;
.super Ljava/lang/Enum;
.source "BulkSettlePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/BulkSettlePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RowType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

.field public static final enum ALL_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

.field public static final enum SORT:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

.field public static final enum TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

.field public static final enum VIEW_ALL:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

.field public static final enum YOUR_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 52
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    const/4 v1, 0x0

    const-string v2, "SORT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->SORT:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    .line 53
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    const/4 v2, 0x1

    const-string v3, "YOUR_TIPS_HEADER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->YOUR_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    .line 54
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    const/4 v3, 0x2

    const-string v4, "ALL_TIPS_HEADER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ALL_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    .line 55
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    const/4 v4, 0x3

    const-string v5, "TENDER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    .line 56
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    const/4 v5, 0x4

    const-string v6, "VIEW_ALL"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->VIEW_ALL:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    .line 51
    sget-object v6, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->SORT:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->YOUR_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ALL_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->VIEW_ALL:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->$VALUES:[Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;
    .locals 1

    .line 51
    const-class v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->$VALUES:[Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-virtual {v0}, [Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    return-object v0
.end method
