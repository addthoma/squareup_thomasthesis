.class public Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;
.super Lcom/squareup/ui/activity/InActivityAppletScope;
.source "RefundLearnMoreDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final messages:Lcom/squareup/register/widgets/LearnMoreMessages;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$RefundLearnMoreDialogScreen$LSp6z4UOR700QPorCDQN0gvSBVU;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$RefundLearnMoreDialogScreen$LSp6z4UOR700QPorCDQN0gvSBVU;

    .line 53
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/register/widgets/LearnMoreMessages;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/activity/InActivityAppletScope;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->messages:Lcom/squareup/register/widgets/LearnMoreMessages;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;)Lcom/squareup/register/widgets/LearnMoreMessages;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->messages:Lcom/squareup/register/widgets/LearnMoreMessages;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;
    .locals 1

    .line 54
    const-class v0, Lcom/squareup/register/widgets/LearnMoreMessages;

    .line 55
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/widgets/LearnMoreMessages;

    .line 56
    new-instance v0, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;-><init>(Lcom/squareup/register/widgets/LearnMoreMessages;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 48
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/activity/InActivityAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->messages:Lcom/squareup/register/widgets/LearnMoreMessages;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getMessages()Lcom/squareup/register/widgets/LearnMoreMessages;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->messages:Lcom/squareup/register/widgets/LearnMoreMessages;

    return-object v0
.end method
