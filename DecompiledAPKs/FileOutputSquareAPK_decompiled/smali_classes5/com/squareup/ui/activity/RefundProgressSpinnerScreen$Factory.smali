.class public Lcom/squareup/ui/activity/RefundProgressSpinnerScreen$Factory;
.super Ljava/lang/Object;
.source "RefundProgressSpinnerScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 36
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;

    .line 37
    invoke-static {v0}, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;->access$000(Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/squareup/dialog/GlassDialog;

    sget v1, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground_NoDim:I

    invoke-direct {v0, p1, v1}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 41
    :cond_0
    invoke-static {p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->inflate(Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->createDialog(Landroid/view/View;)Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
