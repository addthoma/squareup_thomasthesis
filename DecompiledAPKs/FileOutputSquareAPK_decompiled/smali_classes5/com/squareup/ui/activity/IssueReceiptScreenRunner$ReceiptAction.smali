.class final enum Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;
.super Ljava/lang/Enum;
.source "IssueReceiptScreenRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/IssueReceiptScreenRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ReceiptAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

.field public static final enum EMAIL:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

.field public static final enum NONE:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

.field public static final enum PRINT:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

.field public static final enum SMS:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 64
    new-instance v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    const/4 v1, 0x0

    const-string v2, "NONE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->NONE:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    .line 65
    new-instance v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    const/4 v2, 0x1

    const-string v3, "EMAIL"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->EMAIL:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    .line 66
    new-instance v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    const/4 v3, 0x2

    const-string v4, "SMS"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->SMS:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    .line 67
    new-instance v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    const/4 v4, 0x3

    const-string v5, "PRINT"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->PRINT:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    .line 63
    sget-object v5, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->NONE:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->EMAIL:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->SMS:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->PRINT:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->$VALUES:[Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;
    .locals 1

    .line 63
    const-class v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->$VALUES:[Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    invoke-virtual {v0}, [Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    return-object v0
.end method
