.class public interface abstract Lcom/squareup/ui/activity/ActivityAppletGateway;
.super Ljava/lang/Object;
.source "ActivityAppletGateway.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ActivityAppletGateway$NoActivityAppletGateway;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0001\u0008J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/activity/ActivityAppletGateway;",
        "",
        "activateInitialScreen",
        "",
        "goToDisallowInventoryApiDialog",
        "issueRefundScope",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "goToRestockOnItemizedRefundScreen",
        "NoActivityAppletGateway",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract activateInitialScreen()V
.end method

.method public abstract goToDisallowInventoryApiDialog(Lcom/squareup/ui/main/RegisterTreeKey;)V
.end method

.method public abstract goToRestockOnItemizedRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
.end method
