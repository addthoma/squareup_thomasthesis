.class public final Lcom/squareup/ui/activity/DeprecatedSearchBar;
.super Ljava/lang/Object;
.source "SearchBar.kt"

# interfaces
.implements Lcom/squareup/ui/activity/SearchBar;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSearchBar.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SearchBar.kt\ncom/squareup/ui/activity/DeprecatedSearchBar\n*L\n1#1,155:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u0008H\u0016J\n\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0010H\u0016J\u0008\u0010\u0012\u001a\u00020\u0010H\u0016J\u0010\u0010\u0013\u001a\u00020\u00082\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u00082\u0006\u0010 \u001a\u00020\u0010H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/activity/DeprecatedSearchBar;",
        "Lcom/squareup/ui/activity/SearchBar;",
        "viewGroup",
        "Landroid/view/ViewGroup;",
        "(Landroid/view/ViewGroup;)V",
        "searchBar",
        "Lcom/squareup/ui/XableEditText;",
        "addTextChangedListener",
        "",
        "listener",
        "Landroid/text/TextWatcher;",
        "clearFocus",
        "getText",
        "Landroid/text/Editable;",
        "isEnabled",
        "enabled",
        "",
        "isFocused",
        "requestFocus",
        "setHint",
        "hint",
        "",
        "setOnClickListener",
        "Landroid/view/View$OnClickListener;",
        "setOnEditorActionListener",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "setOnFocusChangeListener",
        "Landroid/view/View$OnFocusChangeListener;",
        "setText",
        "text",
        "",
        "setVisibleOrGone",
        "visible",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final searchBar:Lcom/squareup/ui/XableEditText;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    const-string/jumbo v0, "viewGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    check-cast p1, Landroid/view/View;

    sget v0, Lcom/squareup/billhistoryui/R$id;->search_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/XableEditText;

    iput-object p1, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    const/4 v0, 0x1

    .line 50
    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setSelectAllOnFocus(Z)V

    const/4 v0, 0x0

    .line 51
    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public clearFocus()V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->clearFocus()V

    return-void
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public isEnabled(Z)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    return-void
.end method

.method public isFocused()Z
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->isFocused()Z

    move-result v0

    return v0
.end method

.method public requestFocus()Z
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    move-result v0

    return v0
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 1

    const-string v0, "hint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVisibleOrGone(Z)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/activity/DeprecatedSearchBar;->searchBar:Lcom/squareup/ui/XableEditText;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
