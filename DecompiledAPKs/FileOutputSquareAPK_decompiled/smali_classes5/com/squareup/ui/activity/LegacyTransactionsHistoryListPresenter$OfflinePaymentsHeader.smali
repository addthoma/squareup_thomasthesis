.class Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;
.super Ljava/lang/Object;
.source "LegacyTransactionsHistoryListPresenter.java"

# interfaces
.implements Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Header;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OfflinePaymentsHeader"
.end annotation


# instance fields
.field private amount:Lcom/squareup/protos/common/Money;

.field private count:I

.field final synthetic this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x1

    .line 141
    iput p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->count:I

    .line 142
    iget-object p1, p2, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->amount:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method addBill(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 150
    iget v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->count:I

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->amount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public getHeaderText()Ljava/lang/CharSequence;
    .locals 3

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    iget v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->count:I

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$OfflinePaymentsHeader;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$200(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;ILcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
