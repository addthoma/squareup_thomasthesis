.class public final Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/RealCardPresentRefundKt;->nfcListenerOverrider(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1",
        "Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;",
        "setPaymentCompletionListener",
        "",
        "unsetPaymentCompletionListener",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field final synthetic $handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)V
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1;->$cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iput-object p2, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setPaymentCompletionListener()V
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1;->$cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-static {v1}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->access$paymentCompletionListener(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V

    return-void
.end method

.method public unsetPaymentCompletionListener()V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1;->$cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPaymentCompletionListener()V

    return-void
.end method
