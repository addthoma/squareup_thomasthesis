.class public Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;
.super Landroid/widget/LinearLayout;
.source "BillHistoryTaxBreakdownRow.java"


# instance fields
.field private final grossView:Landroid/widget/TextView;

.field private final labelView:Landroid/widget/TextView;

.field private final netView:Landroid/widget/TextView;

.field private final taxView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/squareup/text/Formatter;Ljava/lang/String;Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;",
            ")V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 25
    sget v0, Lcom/squareup/billhistoryui/R$layout;->bill_history_tax_breakdown_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 27
    sget p1, Lcom/squareup/billhistoryui/R$id;->label:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->labelView:Landroid/widget/TextView;

    .line 28
    sget p1, Lcom/squareup/billhistoryui/R$id;->net:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->netView:Landroid/widget/TextView;

    .line 29
    sget p1, Lcom/squareup/billhistoryui/R$id;->tax:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->taxView:Landroid/widget/TextView;

    .line 30
    sget p1, Lcom/squareup/billhistoryui/R$id;->gross:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->grossView:Landroid/widget/TextView;

    .line 34
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->labelView:Landroid/widget/TextView;

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->netView:Landroid/widget/TextView;

    iget-object p3, p4, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->appliedToAmount:Lcom/squareup/protos/common/Money;

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->taxView:Landroid/widget/TextView;

    iget-object p3, p4, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->appliedAmount:Lcom/squareup/protos/common/Money;

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;->grossView:Landroid/widget/TextView;

    iget-object p3, p4, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->afterApplicationTotalAmount:Lcom/squareup/protos/common/Money;

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
