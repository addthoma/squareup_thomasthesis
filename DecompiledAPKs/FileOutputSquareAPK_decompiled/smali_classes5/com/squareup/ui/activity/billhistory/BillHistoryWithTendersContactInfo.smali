.class public final Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;
.super Ljava/lang/Object;
.source "BillHistoryPresenterHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0080\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0008\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\n\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;",
        "",
        "billHistory",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "tendersWithCustomerInfo",
        "",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
        "(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)V",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final billHistory:Lcom/squareup/billhistory/model/BillHistory;

.field public final tendersWithCustomerInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;)V"
        }
    .end annotation

    const-string v0, "billHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tendersWithCustomerInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->tendersWithCustomerInfo:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->tendersWithCustomerInfo:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->copy(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->tendersWithCustomerInfo:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;)",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;"
        }
    .end annotation

    const-string v0, "billHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tendersWithCustomerInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;-><init>(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v1, p1, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->tendersWithCustomerInfo:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->tendersWithCustomerInfo:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->tendersWithCustomerInfo:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BillHistoryWithTendersContactInfo(billHistory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tendersWithCustomerInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->tendersWithCustomerInfo:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
