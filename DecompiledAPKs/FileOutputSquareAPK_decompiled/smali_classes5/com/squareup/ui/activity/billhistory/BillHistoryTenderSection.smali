.class public Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;
.super Landroid/widget/LinearLayout;
.source "BillHistoryTenderSection.java"


# instance fields
.field private final bill:Lcom/squareup/billhistory/model/BillHistory;

.field billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field clock:Lcom/squareup/util/Clock;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final dayFormatter:Ljava/text/DateFormat;

.field private final employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

.field employees:Lcom/squareup/permissions/Employees;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field locale:Ljava/util/Locale;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field resources:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final tender:Lcom/squareup/billhistory/model/TenderHistory;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 6

    const/4 v0, 0x0

    .line 78
    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->bill:Lcom/squareup/billhistory/model/BillHistory;

    const/4 v0, 0x1

    .line 80
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->setOrientation(I)V

    .line 82
    const-class v0, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;->inject(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;)V

    .line 83
    sget v0, Lcom/squareup/billhistoryui/R$layout;->bill_history_tender_section:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/billhistoryui/R$id;->tender_employee:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/billhistoryui/R$id;->tender_employee_deprecated:I

    :goto_0
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ShorteningTextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    .line 89
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->timeFormatter:Ljava/text/DateFormat;

    .line 90
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->dayFormatter:Ljava/text/DateFormat;

    .line 91
    iput-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 93
    sget p1, Lcom/squareup/billhistoryui/R$id;->tender_type:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    .line 94
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createTenderType(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    sget p1, Lcom/squareup/billhistoryui/R$id;->tender_timestamp:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    .line 97
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createTenderTimestamp(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    sget p1, Lcom/squareup/billhistoryui/R$id;->tenders_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p3, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    if-eqz v0, :cond_1

    .line 103
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createDeprecatedCustomerRow(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->resources:Lcom/squareup/util/Res;

    invoke-virtual {p3, v0}, Lcom/squareup/billhistory/model/TenderHistory;->getTypeName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 110
    invoke-virtual {p3}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 111
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_2

    .line 112
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createAmount(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 113
    invoke-virtual {p3}, Lcom/squareup/billhistory/model/TenderHistory;->getTipPercentage()Lcom/squareup/util/Percentage;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 116
    :cond_2
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createMethod(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v2, p2, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    iget-object v3, p3, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createReceiptRow(Landroid/content/Context;ZLjava/lang/String;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 121
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 124
    :cond_3
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createInvoiceNumber(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 126
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 129
    :cond_4
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createTabCustomer(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 131
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 134
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p2, Lcom/squareup/billhistory/model/BillHistory;->creatorName:Ljava/lang/String;

    .line 135
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 136
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_6

    .line 137
    iget-object p2, p2, Lcom/squareup/billhistory/model/BillHistory;->creatorName:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createDelegateRow(Ljava/lang/String;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 140
    :cond_6
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_7

    if-eqz p4, :cond_7

    .line 141
    invoke-direct {p0, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createCustomerRow(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 144
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide p1

    invoke-virtual {p3, p1, p2}, Lcom/squareup/billhistory/model/TenderHistory;->isPastRefundDate(J)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 145
    sget p1, Lcom/squareup/billhistoryui/R$id;->refund_past_deadline:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x0

    .line 146
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    return-void
.end method

.method private createAmount(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;
    .locals 3

    .line 262
    new-instance v0, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 263
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 264
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_DOLLAR_SIGN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 265
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->resources:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->amount:I

    .line 266
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 267
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->amountExcludingTip()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 268
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method

.method private createCustomerRow(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 4

    .line 228
    sget v0, Lcom/squareup/billhistoryui/R$id;->customer_row_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 230
    sget v1, Lcom/squareup/billhistoryui/R$id;->circle:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 231
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    .line 232
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->resources:Lcom/squareup/util/Res;

    invoke-static {p1, v2}, Lcom/squareup/crm/util/CustomerColorUtilsKt;->getCircleColor(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 234
    sget v1, Lcom/squareup/billhistoryui/R$id;->circle_initials_text:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketTextView;

    .line 235
    new-instance v2, Lcom/squareup/crm/util/CustomerInitialsHelper;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->resources:Lcom/squareup/util/Res;

    invoke-direct {v2, v3}, Lcom/squareup/crm/util/CustomerInitialsHelper;-><init>(Lcom/squareup/util/Res;)V

    .line 236
    invoke-virtual {v2, p1}, Lcom/squareup/crm/util/CustomerInitialsHelper;->getInitials(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    sget v1, Lcom/squareup/billhistoryui/R$id;->crm_customer_display_name:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketTextView;

    .line 239
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->resources:Lcom/squareup/util/Res;

    invoke-static {p1, v2}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullName(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 241
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void
.end method

.method private createDelegateRow(Ljava/lang/String;)Landroid/view/View;
    .locals 3

    .line 325
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 326
    new-instance v1, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-direct {v1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/billhistoryui/R$string;->collected_by:I

    .line 327
    invoke-static {v0, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v2, "name"

    .line 328
    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 329
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 327
    invoke-virtual {v1, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PERSON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 330
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 331
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method

.method private createDeprecatedCustomerRow(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;
    .locals 2

    .line 214
    instance-of v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    if-eqz v0, :cond_1

    .line 217
    check-cast p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    iget-object p1, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->buyerName:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 220
    :cond_0
    new-instance v0, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 221
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PERSON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 222
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    .line 223
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 224
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1

    .line 215
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Expected a CreditCardTenderHistory"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private createDeprecatedEmployeeFullName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;
    .locals 5

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 175
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/billhistoryui/R$string;->uppercase_receipt_detail_tender_payment_taker:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    .line 176
    invoke-virtual {v3, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "firstname"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    .line 177
    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "lastname"

    invoke-virtual {v2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 178
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo p1, "\u00a0"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private createDeprecatedEmployeeShortName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;
    .locals 5

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 190
    iget-object v1, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/squareup/billhistoryui/R$string;->uppercase_receipt_detail_tender_payment_taker:I

    invoke-static {v3, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    .line 192
    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "firstname"

    invoke-virtual {v3, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 193
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "lastname"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo p1, "\u00a0"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private createEmployeeFullName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;
    .locals 3

    .line 182
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$string;->bill_history_tender_section_cashier:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    const-string v2, "firstname"

    .line 183
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    const-string v1, "lastname"

    .line 184
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private createEmployeeShortName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;
    .locals 3

    .line 198
    iget-object v0, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    .line 199
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$string;->bill_history_tender_section_cashier:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    const-string v2, "firstname"

    .line 200
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 201
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "lastname"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 202
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private createInvoiceNumber(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;
    .locals 3

    .line 293
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->sequentialTenderNumber:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 297
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 298
    new-instance v1, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-direct {v1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 299
    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->INVOICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 300
    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$string;->receipt_detail_invoice_number:I

    .line 301
    invoke-static {v0, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->sequentialTenderNumber:Ljava/lang/String;

    const-string v2, "invoice_number"

    .line 302
    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 303
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 301
    invoke-virtual {v1, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 304
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method

.method private createMethod(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;
    .locals 4

    .line 272
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->getNote()Ljava/lang/String;

    move-result-object v0

    .line 273
    iget-object v1, p1, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v2, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    if-ne v1, v2, :cond_0

    .line 274
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/transaction/R$string;->offline_mode_declined:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/4 v1, 0x0

    .line 278
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v2}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v2

    if-nez v2, :cond_1

    .line 279
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 282
    :cond_1
    new-instance v2, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 283
    invoke-virtual {v2, v3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v2

    .line 284
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->resources:Lcom/squareup/util/Res;

    .line 285
    invoke-virtual {p1, v3}, Lcom/squareup/billhistory/model/TenderHistory;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 286
    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 287
    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 288
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setNote(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 289
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method

.method private createTabCustomer(Lcom/squareup/billhistory/model/TenderHistory;)Landroid/view/View;
    .locals 2

    .line 308
    instance-of v0, p1, Lcom/squareup/billhistory/model/TabTenderHistory;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 311
    :cond_0
    check-cast p1, Lcom/squareup/billhistory/model/TabTenderHistory;

    iget-object p1, p1, Lcom/squareup/billhistory/model/TabTenderHistory;->customerName:Ljava/lang/String;

    if-nez p1, :cond_1

    return-object v1

    .line 316
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 317
    new-instance v1, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-direct {v1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 318
    invoke-virtual {v1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PERSON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 319
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    .line 320
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 321
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method

.method private createTenderTimestamp(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/lang/CharSequence;
    .locals 3

    .line 206
    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$string;->uppercase_receipt_detail_tender_payment_timestamp:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->dayFormatter:Ljava/text/DateFormat;

    .line 208
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->timeFormatter:Ljava/text/DateFormat;

    .line 209
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private createTenderType(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/lang/CharSequence;
    .locals 2

    .line 168
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$string;->uppercase_receipt_detail_tender_payment_type:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->resources:Lcom/squareup/util/Res;

    .line 169
    invoke-virtual {p1, v1}, Lcom/squareup/billhistory/model/TenderHistory;->getTypeName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 170
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private createTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Landroid/view/View;
    .locals 3

    .line 245
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez p2, :cond_0

    .line 246
    sget p2, Lcom/squareup/activity/R$string;->tip:I

    .line 247
    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/billhistoryui/R$string;->tip_percentage:I

    .line 248
    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 249
    invoke-interface {v2, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v2, "percentage"

    invoke-virtual {v1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 250
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 252
    :goto_0
    new-instance v1, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-direct {v1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 253
    invoke-virtual {v1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 254
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 255
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    .line 256
    invoke-virtual {v0, p2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 257
    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 258
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public synthetic lambda$null$0$BillHistoryTenderSection(Lcom/squareup/permissions/Employee;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createEmployeeFullName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createEmployeeShortName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createDeprecatedEmployeeFullName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->createDeprecatedEmployeeShortName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    .line 163
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ShorteningTextView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$BillHistoryTenderSection()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->employees:Lcom/squareup/permissions/Employees;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/Employees;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryTenderSection$sXSdagNnLouzlE827HcHY9Ffpho;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryTenderSection$sXSdagNnLouzlE827HcHY9Ffpho;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;)V

    .line 155
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 151
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 153
    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryTenderSection$Uu80Y9npiyHwUgLhVplhbuCJZHg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryTenderSection$Uu80Y9npiyHwUgLhVplhbuCJZHg;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
