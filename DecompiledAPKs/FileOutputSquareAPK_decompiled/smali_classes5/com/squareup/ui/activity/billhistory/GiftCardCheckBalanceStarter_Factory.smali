.class public final Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;
.super Ljava/lang/Object;
.source "GiftCardCheckBalanceStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final giftCardByTokenCallPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;->giftCardByTokenCallPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;",
            ">;)",
            "Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;)Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;->giftCardByTokenCallPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;

    invoke-static {v0, v1}, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;)Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter_Factory;->get()Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    move-result-object v0

    return-object v0
.end method
