.class public final Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;
.super Ljava/lang/Object;
.source "SelectGiftReceiptTenderView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;Ljava/lang/Object;)V
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;

    iput-object p1, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;->presenter:Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;Lcom/squareup/text/Formatter;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;->injectPresenter(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;)V

    return-void
.end method
