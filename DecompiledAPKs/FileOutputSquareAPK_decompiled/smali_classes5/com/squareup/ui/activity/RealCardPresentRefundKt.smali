.class public final Lcom/squareup/ui/activity/RealCardPresentRefundKt;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u001a\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u001a\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u001a\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u001a \u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002\u001a \u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002\u001a \u0010\u0015\u001a\u00020\r2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002\u00a8\u0006\u0016"
    }
    d2 = {
        "nfcAuthDelegate",
        "Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;",
        "handler",
        "Lcom/squareup/ui/activity/CardPresentRefundEventHandler;",
        "nfcErrorHandler",
        "Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;",
        "nfcListenerOverrider",
        "Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;",
        "cardReaderListeners",
        "Lcom/squareup/cardreader/CardReaderListeners;",
        "paymentCompletionListener",
        "Lcom/squareup/cardreader/PaymentCompletionListener;",
        "restartRefundRequestOnReaders",
        "",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "amount",
        "",
        "nfcStatusDisplay",
        "Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;",
        "restartingMonitor",
        "startRefundRequestOnReaders",
        "android_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$nfcAuthDelegate(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->nfcAuthDelegate(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$nfcErrorHandler(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->nfcErrorHandler(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$nfcListenerOverrider(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->nfcListenerOverrider(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$paymentCompletionListener(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/cardreader/PaymentCompletionListener;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->paymentCompletionListener(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$restartRefundRequestOnReaders(Lcom/squareup/ui/NfcProcessor;JLcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->restartRefundRequestOnReaders(Lcom/squareup/ui/NfcProcessor;JLcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$startRefundRequestOnReaders(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;Lcom/squareup/ui/NfcProcessor;J)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->startRefundRequestOnReaders(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;Lcom/squareup/ui/NfcProcessor;J)Z

    move-result p0

    return p0
.end method

.method private static final nfcAuthDelegate(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;
    .locals 1

    .line 148
    new-instance v0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcAuthDelegate$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcAuthDelegate$1;-><init>(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)V

    check-cast v0, Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;

    return-object v0
.end method

.method private static final nfcErrorHandler(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;
    .locals 1

    .line 199
    new-instance v0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;-><init>(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)V

    check-cast v0, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    return-object v0
.end method

.method private static final nfcListenerOverrider(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;
    .locals 1

    .line 264
    new-instance v0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcListenerOverrider$1;-><init>(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)V

    check-cast v0, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;

    return-object v0
.end method

.method private static final paymentCompletionListener(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/cardreader/PaymentCompletionListener;
    .locals 1

    .line 284
    new-instance v0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;-><init>(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)V

    check-cast v0, Lcom/squareup/cardreader/PaymentCompletionListener;

    return-object v0
.end method

.method private static final restartRefundRequestOnReaders(Lcom/squareup/ui/NfcProcessor;JLcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)Z
    .locals 0

    .line 168
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/NfcProcessor;->startRefundOnAllContactlessReadersWithAmount(JLcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)Z

    move-result p0

    return p0
.end method

.method private static final restartingMonitor(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;Lcom/squareup/ui/NfcProcessor;J)Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;
    .locals 1

    .line 179
    new-instance v0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;

    invoke-direct {v0, p1, p2, p3, p0}, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;-><init>(Lcom/squareup/ui/NfcProcessor;JLcom/squareup/ui/activity/CardPresentRefundEventHandler;)V

    check-cast v0, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    return-object v0
.end method

.method private static final startRefundRequestOnReaders(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;Lcom/squareup/ui/NfcProcessor;J)Z
    .locals 0

    .line 160
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->restartingMonitor(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;Lcom/squareup/ui/NfcProcessor;J)Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    move-result-object p0

    .line 161
    invoke-virtual {p1, p2, p3, p0}, Lcom/squareup/ui/NfcProcessor;->startRefundOnAllContactlessReadersWithAmount(JLcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)Z

    move-result p0

    return p0
.end method
