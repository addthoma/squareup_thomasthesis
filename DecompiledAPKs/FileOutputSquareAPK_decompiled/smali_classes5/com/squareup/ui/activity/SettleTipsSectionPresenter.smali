.class public Lcom/squareup/ui/activity/SettleTipsSectionPresenter;
.super Lmortar/ViewPresenter;
.source "SettleTipsSectionPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/activity/SettleTipsSection;",
        ">;"
    }
.end annotation


# static fields
.field static final SETTLED_EVENT_QUICK_TIP_OPTION_NOT_USED:Ljava/lang/String; = "Quick Tip Option Not Used"

.field static final SETTLED_EVENT_QUICK_TIP_OPTION_USED:Ljava/lang/String; = "Quick Tip Option Used"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private bill:Lcom/squareup/billhistory/model/BillHistory;

.field private final billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final currentBill:Lcom/squareup/activity/CurrentBill;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

.field private final expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

.field private final flow:Lflow/Flow;

.field private final legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field private rowInFlight:Lcom/squareup/ui/activity/SettleTipRow;

.field final settleProgressPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final settleTipConnectivityUtils:Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

.field private final tenderSettler:Lcom/squareup/papersignature/TenderTipSettler;

.field private final tendersAwaitingTip:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/papersignature/TenderTipSettler;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/ui/activity/SettleTipConnectivityUtils;Lcom/squareup/activity/CurrentBill;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/EmployeeManagement;Lflow/Flow;Lcom/squareup/ui/activity/BillHistoryRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/papersignature/TenderTipSettler;",
            "Lcom/squareup/activity/ExpiryCalculator;",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            "Lcom/squareup/ui/activity/SettleTipConnectivityUtils;",
            "Lcom/squareup/activity/CurrentBill;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 86
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 88
    iput-object p2, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->res:Lcom/squareup/util/Res;

    .line 89
    iput-object p3, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 90
    iput-object p4, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tenderSettler:Lcom/squareup/papersignature/TenderTipSettler;

    .line 91
    iput-object p5, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    .line 92
    iput-object p6, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    .line 93
    iput-object p7, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleTipConnectivityUtils:Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

    .line 94
    iput-object p8, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 95
    iput-object p9, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 96
    iput-object p10, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 97
    iput-object p11, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 98
    iput-object p12, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 99
    iput-object p13, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->flow:Lflow/Flow;

    .line 100
    iput-object p14, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

    .line 102
    new-instance p1, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$1;-><init>(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;)V

    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleProgressPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 108
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tendersAwaitingTip:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/ui/activity/SettleTipRow;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->runSettle(Lcom/squareup/ui/activity/SettleTipRow;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->onSettleFailure(Lcom/squareup/billhistory/model/TenderHistory;)V

    return-void
.end method

.method private doSettle()V
    .locals 4

    .line 239
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->isUiLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleTipConnectivityUtils:Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->isOffline()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->unlockUi()V

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleTipConnectivityUtils:Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->getNoInternetPopupScreen(Z)Lcom/squareup/register/widgets/WarningDialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->receipt_detail_settle_tip_popup_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 250
    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleProgressPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    new-instance v2, Lcom/squareup/caller/ProgressPopup$Progress;

    invoke-direct {v2, v0}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->rowInFlight:Lcom/squareup/ui/activity/SettleTipRow;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/SettleTipRow;->getTenderId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->requireTenderState(Ljava/lang/String;)Lcom/squareup/ui/activity/TenderEditState;

    move-result-object v0

    .line 254
    iget-boolean v1, v0, Lcom/squareup/ui/activity/TenderEditState;->wasQuickTipUsed:Z

    invoke-direct {p0, v1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->logSettleClicked(Z)V

    .line 256
    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tenderSettler:Lcom/squareup/papersignature/TenderTipSettler;

    iget-object v2, v0, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;-><init>(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/ui/activity/TenderEditState;)V

    invoke-virtual {v1, v2, v3}, Lcom/squareup/papersignature/TenderTipSettler;->settleTips(Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void

    .line 240
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No settlement in flight."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getTitleColor(Lcom/squareup/billhistory/model/TenderHistory;)I
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/ExpiryCalculator;->isTipExpiringSoon(Lcom/squareup/billhistory/model/TenderHistory;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray:I

    :goto_0
    return p1
.end method

.method private isUiLocked()Z
    .locals 1

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->rowInFlight:Lcom/squareup/ui/activity/SettleTipRow;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private lockUi(Lcom/squareup/ui/activity/SettleTipRow;)V
    .locals 0

    .line 350
    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->rowInFlight:Lcom/squareup/ui/activity/SettleTipRow;

    return-void
.end method

.method private logSettleClicked(Z)V
    .locals 3

    if-eqz p1, :cond_0

    const-string p1, "Quick Tip Option Used"

    goto :goto_0

    :cond_0
    const-string p1, "Quick Tip Option Not Used"

    .line 282
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result v0

    .line 283
    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/papersignature/analytics/SettledTipsFromTransactionViewEvent;

    invoke-direct {v2, p1, v0}, Lcom/squareup/papersignature/analytics/SettledTipsFromTransactionViewEvent;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private onSettleFailure(Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleProgressPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/BillHistoryRunner;->onSettleFailure(Lcom/squareup/billhistory/model/TenderHistory;)V

    return-void
.end method

.method private rebuildRows()V
    .locals 7

    .line 304
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/SettleTipsSection;

    .line 305
    invoke-virtual {v0}, Lcom/squareup/ui/activity/SettleTipsSection;->removeAllViews()V

    .line 307
    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tendersAwaitingTip:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/activity/TenderEditState;

    .line 308
    iget-object v2, v2, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    .line 309
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->inflateRow()Lcom/squareup/ui/activity/SettleTipRow;

    move-result-object v3

    .line 311
    iget-object v4, v2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/activity/SettleTipRow;->setTenderId(Ljava/lang/String;)V

    .line 313
    invoke-virtual {v2}, Lcom/squareup/billhistory/model/TenderHistory;->hasAutoGratuity()Z

    move-result v4

    if-eqz v4, :cond_0

    sget v4, Lcom/squareup/billhistoryui/R$string;->receipt_detail_settle_additional_tip_title:I

    goto :goto_1

    :cond_0
    sget v4, Lcom/squareup/billhistoryui/R$string;->receipt_detail_settle_tip_title:I

    .line 316
    :goto_1
    iget-object v5, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v5, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->res:Lcom/squareup/util/Res;

    .line 317
    invoke-virtual {v2, v5}, Lcom/squareup/billhistory/model/TenderHistory;->getUppercaseDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v5

    const-string v6, "description"

    invoke-virtual {v4, v6, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 318
    invoke-virtual {v4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 320
    invoke-virtual {v3, v4}, Lcom/squareup/ui/activity/SettleTipRow;->setTitle(Ljava/lang/String;)V

    .line 323
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/activity/SettleTipRow;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 327
    invoke-direct {p0, v2}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->getTitleColor(Lcom/squareup/billhistory/model/TenderHistory;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/activity/SettleTipRow;->setTitleTextColor(I)V

    .line 329
    invoke-virtual {v3, v2}, Lcom/squareup/ui/activity/SettleTipRow;->updateQuickTipEditor(Lcom/squareup/billhistory/model/TenderHistory;)V

    .line 330
    invoke-virtual {v2}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v3, v2}, Lcom/squareup/ui/activity/SettleTipRow;->setSettleButtonEnabled(Z)V

    .line 332
    invoke-virtual {v0, v3}, Lcom/squareup/ui/activity/SettleTipsSection;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private removeSettledTenderRow()V
    .locals 2

    .line 337
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/SettleTipsSection;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->rowInFlight:Lcom/squareup/ui/activity/SettleTipRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/SettleTipsSection;->removeRow(Landroid/view/View;)V

    .line 338
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->unlockUi()V

    return-void
.end method

.method private requireTenderState(Ljava/lang/String;)Lcom/squareup/ui/activity/TenderEditState;
    .locals 3

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tendersAwaitingTip:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TenderEditState;

    if-eqz v0, :cond_0

    return-object v0

    .line 233
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid tender ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private runSettle(Lcom/squareup/ui/activity/SettleTipRow;)V
    .locals 1

    .line 201
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->isUiLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->lockUi(Lcom/squareup/ui/activity/SettleTipRow;)V

    .line 206
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->doSettle()V

    return-void

    .line 202
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Settlement is already in flight."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private showTipOptionsForTenders(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "tenders"

    .line 210
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 212
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tendersAwaitingTip:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 214
    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tendersAwaitingTip:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 216
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 217
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->shouldPrintTipEntryInput()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220
    iget-object v2, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/activity/TenderEditState;

    if-nez v2, :cond_1

    .line 221
    new-instance v2, Lcom/squareup/ui/activity/TenderEditState;

    invoke-direct {v2, v1}, Lcom/squareup/ui/activity/TenderEditState;-><init>(Lcom/squareup/billhistory/model/TenderHistory;)V

    .line 225
    :cond_1
    iget-object v3, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tendersAwaitingTip:Ljava/util/Map;

    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-void
.end method

.method private unlockUi()V
    .locals 1

    const/4 v0, 0x0

    .line 354
    iput-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->rowInFlight:Lcom/squareup/ui/activity/SettleTipRow;

    return-void
.end method

.method private updateView()V
    .locals 1

    .line 287
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 291
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->isUiLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 300
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->rebuildRows()V

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/activity/SettleTipsSection;)V
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 138
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->unlockUi()V

    .line 140
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/ui/activity/SettleTipsSection;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->dropView(Lcom/squareup/ui/activity/SettleTipsSection;)V

    return-void
.end method

.method getSettleButtonConfirmText(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 158
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->requireTenderState(Ljava/lang/String;)Lcom/squareup/ui/activity/TenderEditState;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->receipt_detail_settle_tip_confirm:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 161
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method inflateRow()Lcom/squareup/ui/activity/SettleTipRow;
    .locals 4

    .line 363
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/SettleTipsSection;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/SettleTipsSection;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$layout;->receipt_detail_settle_tip_row:I

    .line 364
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/SettleTipRow;

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$SettleTipsSectionPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 115
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->unlockUi()V

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/SettleTipsSection;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/SettleTipsSection;->removeTransitions()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$SettleTipsSectionPresenter(Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 124
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 125
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->isUiLocked()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 129
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->doSettle()V

    goto :goto_0

    .line 126
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempting to retry without a row currently in flight."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 131
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->unlockUi()V

    :goto_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 113
    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->onChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$SettleTipsSectionPresenter$dGgKk2D9io9Ahbc3jHN7TsQS42A;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$SettleTipsSectionPresenter$dGgKk2D9io9Ahbc3jHN7TsQS42A;-><init>(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;)V

    .line 114
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 112
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

    .line 122
    invoke-virtual {v0}, Lcom/squareup/ui/activity/BillHistoryRunner;->settleTipFailurePopupResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$SettleTipsSectionPresenter$Zd0jwy-7vdsB1Uds5QTwQurLGtw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$SettleTipsSectionPresenter$Zd0jwy-7vdsB1Uds5QTwQurLGtw;-><init>(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;)V

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 121
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method onSettleConfirmClicked(Lcom/squareup/ui/activity/SettleTipRow;)V
    .locals 3

    .line 180
    invoke-virtual {p1}, Lcom/squareup/ui/activity/SettleTipRow;->getTenderId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->requireTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v1}, Lcom/squareup/permissions/EmployeeManagement;->isEldmEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 183
    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isPaperSignatureFilteringAllowed()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 185
    invoke-interface {v1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->SETTLE_ALL_TIPS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$2;-><init>(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/ui/activity/SettleTipRow;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_1

    .line 186
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->runSettle(Lcom/squareup/ui/activity/SettleTipRow;)V

    :goto_1
    return-void
.end method

.method onSettleSuccess()V
    .locals 1

    .line 269
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->removeSettledTenderRow()V

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleProgressPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    return-void
.end method

.method onTipAmountChanged(Ljava/lang/String;Ljava/lang/Long;Z)V
    .locals 3

    .line 166
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->requireTenderState(Ljava/lang/String;)Lcom/squareup/ui/activity/TenderEditState;

    move-result-object v0

    if-nez p2, :cond_0

    .line 169
    iget-object p2, v0, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {p2}, Lcom/squareup/billhistory/model/TenderHistory;->withoutTip()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p2

    iput-object p2, v0, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    const/4 p2, 0x0

    .line 170
    iput-boolean p2, v0, Lcom/squareup/ui/activity/TenderEditState;->wasQuickTipUsed:Z

    goto :goto_0

    .line 172
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object p2, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v2, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 173
    iget-object v1, v0, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Lcom/squareup/billhistory/model/TenderHistory;->withTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p2

    iput-object p2, v0, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    .line 174
    iget-boolean p2, v0, Lcom/squareup/ui/activity/TenderEditState;->wasQuickTipUsed:Z

    or-int/2addr p2, p3

    iput-boolean p2, v0, Lcom/squareup/ui/activity/TenderEditState;->wasQuickTipUsed:Z

    .line 176
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->tendersAwaitingTip:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method requireTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 0

    .line 197
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->requireTenderState(Ljava/lang/String;)Lcom/squareup/ui/activity/TenderEditState;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    return-object p1
.end method

.method public setBill(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eq v0, p1, :cond_0

    .line 145
    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 146
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->showTipOptionsForTenders(Ljava/util/List;)V

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->updateView()V

    return-void
.end method
