.class Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;
.super Ljava/lang/Object;
.source "DiscountEntryPercentScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/DiscountEntryPercentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PercentWorkingDiscountState"
.end annotation


# instance fields
.field private authorizingEmployee:Lcom/squareup/protos/client/Employee;

.field private final authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

.field private final workingDiscountBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/BundleKey;Lcom/squareup/protos/client/Employee;Lcom/squareup/BundleKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;",
            "Lcom/squareup/protos/client/Employee;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;)V"
        }
    .end annotation

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->transaction:Lcom/squareup/payment/Transaction;

    .line 202
    iput-object p2, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    .line 203
    iput-object p3, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    .line 204
    iput-object p4, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->authorizingEmployee:Lcom/squareup/protos/client/Employee;

    .line 205
    iput-object p5, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    return-void
.end method

.method static synthetic access$1200(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;Ljava/lang/String;)V
    .locals 0

    .line 176
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->setPercentage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;Landroid/os/Bundle;)V
    .locals 0

    .line 176
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->load(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Ljava/lang/CharSequence;
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->getName()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;Landroid/os/Bundle;)V
    .locals 0

    .line 176
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->save(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Ljava/lang/String;
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->getPercentString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Lcom/squareup/util/Percentage;
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Lcom/squareup/configure/item/WorkingDiscount;
    .locals 0

    .line 176
    iget-object p0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;Z)V
    .locals 0

    .line 176
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->commit(Z)V

    return-void
.end method

.method static buildEmptyWorkingDiscountState(Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;)",
            "Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;"
        }
    .end annotation

    .line 194
    new-instance v6, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, v6

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/BundleKey;Lcom/squareup/protos/client/Employee;Lcom/squareup/BundleKey;)V

    return-object v6
.end method

.method static buildLoadedWorkingDiscountState(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/BundleKey;Lcom/squareup/protos/client/Employee;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;",
            "Lcom/squareup/protos/client/Employee;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;)",
            "Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;"
        }
    .end annotation

    .line 187
    new-instance v6, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/BundleKey;Lcom/squareup/protos/client/Employee;Lcom/squareup/BundleKey;)V

    return-object v6
.end method

.method private commit(Z)V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    invoke-virtual {v1, p1}, Lcom/squareup/configure/item/WorkingDiscount;->finish(Z)Lcom/squareup/checkout/Discount;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->authorizingEmployee:Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/payment/Transaction;->addDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method private getName()Ljava/lang/CharSequence;
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingDiscount;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPercentString()Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingDiscount;->percentageString:Ljava/lang/String;

    return-object v0
.end method

.method private getPercentage()Lcom/squareup/util/Percentage;
    .locals 2

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingDiscount;->percentageString:Ljava/lang/String;

    sget-object v1, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {v0, v1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v0

    return-object v0
.end method

.method private load(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/WorkingDiscount;

    iput-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/Employee;

    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->authorizingEmployee:Lcom/squareup/protos/client/Employee;

    :cond_0
    return-void
.end method

.method private save(Landroid/os/Bundle;)V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->authorizingEmployee:Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method private setPercentage(Ljava/lang/String;)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    iput-object p1, v0, Lcom/squareup/configure/item/WorkingDiscount;->percentageString:Ljava/lang/String;

    return-void
.end method
