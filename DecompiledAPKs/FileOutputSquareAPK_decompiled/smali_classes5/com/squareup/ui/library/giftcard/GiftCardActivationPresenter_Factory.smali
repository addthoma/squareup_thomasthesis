.class public final Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;
.super Ljava/lang/Object;
.source "GiftCardActivationPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final workingItemBundleKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p3, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p4, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p5, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p6, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p7, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p8, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p9, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p10, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 85
    iput-object p11, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 86
    iput-object p12, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    .line 87
    iput-object p13, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->workingItemBundleKeyProvider:Ljavax/inject/Provider;

    .line 88
    iput-object p14, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->giftCardServiceHelperProvider:Ljavax/inject/Provider;

    .line 89
    iput-object p15, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;"
        }
    .end annotation

    .line 109
    new-instance v16, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/BundleKey;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/badbus/BadBus;",
            "Lflow/Flow;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;"
        }
    .end annotation

    .line 119
    new-instance v16, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/BundleKey;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v16
.end method


# virtual methods
.method public get()Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;
    .locals 17

    move-object/from16 v0, p0

    .line 94
    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/giftcard/GiftCards;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/payment/SwipeHandler;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->workingItemBundleKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/BundleKey;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->giftCardServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/giftcard/GiftCardServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static/range {v2 .. v16}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/BundleKey;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter_Factory;->get()Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    move-result-object v0

    return-object v0
.end method
