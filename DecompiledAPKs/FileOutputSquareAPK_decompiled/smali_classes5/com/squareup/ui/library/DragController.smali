.class public Lcom/squareup/ui/library/DragController;
.super Lcom/mobeta/android/dslv/DragSortController;
.source "DragController.java"


# instance fields
.field private hitPosition:I

.field private final listView:Lcom/mobeta/android/dslv/DragSortListView;

.field private staticTopRows:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mobeta/android/dslv/DragSortListView;)V
    .locals 6

    .line 23
    sget v2, Lcom/squareup/widgets/recycler/R$id;->drag_handle:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/mobeta/android/dslv/DragSortController;-><init>(Lcom/mobeta/android/dslv/DragSortListView;IIII)V

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$color;->marin_window_background:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    const/high16 p2, 0x3f400000    # 0.75f

    .line 29
    invoke-static {p1, p2}, Lcom/squareup/util/Colors;->withAlpha(IF)I

    move-result p1

    .line 31
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/DragController;->setBackgroundColor(I)V

    const/4 p1, 0x0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/DragController;->setRemoveEnabled(Z)V

    return-void
.end method


# virtual methods
.method public dragHandleHitPosition(Landroid/view/MotionEvent;)I
    .locals 3

    .line 65
    invoke-super {p0, p1}, Lcom/mobeta/android/dslv/DragSortController;->dragHandleHitPosition(Landroid/view/MotionEvent;)I

    move-result p1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    return v0

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    invoke-virtual {v1}, Lcom/mobeta/android/dslv/DragSortListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/mobeta/android/dslv/DragSortListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 68
    sget v2, Lcom/squareup/widgets/recycler/R$id;->drag_handle:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 69
    invoke-virtual {v1}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    return p1

    :cond_1
    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/DragController;->startDragPosition(Landroid/view/MotionEvent;)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/library/DragController;->hitPosition:I

    .line 43
    iget v0, p0, Lcom/squareup/ui/library/DragController;->hitPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-super {p0, p1}, Lcom/mobeta/android/dslv/DragSortController;->onDown(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onDragFloatView(Landroid/view/View;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2

    .line 84
    iget-object p3, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    invoke-virtual {p3}, Lcom/mobeta/android/dslv/DragSortListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p3

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    invoke-virtual {v0}, Lcom/mobeta/android/dslv/DragSortListView;->getFirstVisiblePosition()I

    move-result v0

    .line 87
    invoke-interface {p3}, Landroid/widget/ListAdapter;->getCount()I

    move-result p3

    sub-int/2addr p3, v0

    iget-object v0, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    invoke-virtual {v0}, Lcom/mobeta/android/dslv/DragSortListView;->getHeaderViewsCount()I

    move-result v0

    add-int/2addr p3, v0

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    invoke-virtual {v0}, Lcom/mobeta/android/dslv/DragSortListView;->getDividerHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 90
    iget-object v1, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    invoke-virtual {v1, p3}, Lcom/mobeta/android/dslv/DragSortListView;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 92
    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result p3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    sub-int/2addr p3, p1

    sub-int/2addr p3, v0

    .line 93
    iget p1, p2, Landroid/graphics/Point;->y:I

    if-le p1, p3, :cond_0

    .line 94
    iput p3, p2, Landroid/graphics/Point;->y:I

    .line 98
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    iget p3, p0, Lcom/squareup/ui/library/DragController;->staticTopRows:I

    add-int/lit8 p3, p3, -0x1

    invoke-virtual {p1, p3}, Lcom/mobeta/android/dslv/DragSortListView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 100
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result p1

    add-int/2addr p1, v0

    .line 101
    iget p3, p2, Landroid/graphics/Point;->y:I

    if-ge p3, p1, :cond_1

    .line 102
    iput p1, p2, Landroid/graphics/Point;->y:I

    :cond_1
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 38
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/mobeta/android/dslv/DragSortController;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result p1

    return p1
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 49
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/DragController;->dragHandleHitPosition(Landroid/view/MotionEvent;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    iget v0, p0, Lcom/squareup/ui/library/DragController;->hitPosition:I

    invoke-virtual {p1, v0}, Lcom/mobeta/android/dslv/DragSortListView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    iget v1, p0, Lcom/squareup/ui/library/DragController;->hitPosition:I

    .line 55
    invoke-virtual {v0}, Lcom/mobeta/android/dslv/DragSortListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    iget v3, p0, Lcom/squareup/ui/library/DragController;->hitPosition:I

    invoke-interface {v2, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 54
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/mobeta/android/dslv/DragSortListView;->performItemClick(Landroid/view/View;IJ)Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public setStaticTopRows(I)V
    .locals 0

    .line 108
    iput p1, p0, Lcom/squareup/ui/library/DragController;->staticTopRows:I

    return-void
.end method

.method public startDrag(III)Z
    .locals 1

    .line 76
    invoke-super {p0, p1, p2, p3}, Lcom/mobeta/android/dslv/DragSortController;->startDrag(III)Z

    move-result p2

    const/4 p3, 0x0

    if-nez p2, :cond_0

    return p3

    .line 77
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/library/DragController;->listView:Lcom/mobeta/android/dslv/DragSortListView;

    invoke-virtual {p2}, Lcom/mobeta/android/dslv/DragSortListView;->getFirstVisiblePosition()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-virtual {p2, p1}, Lcom/mobeta/android/dslv/DragSortListView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_1

    return p3

    .line 79
    :cond_1
    sget p2, Lcom/squareup/widgets/recycler/R$id;->drag_handle:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 80
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p3, 0x1

    :cond_2
    return p3
.end method
