.class public Lcom/squareup/ui/library/BaseGlyphButtonEditText;
.super Landroid/widget/FrameLayout;
.source "BaseGlyphButtonEditText.java"

# interfaces
.implements Lcom/squareup/text/HasSelectableText;
.implements Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/widget/EditText;",
        ":",
        "Lcom/squareup/text/HasSelectableText;",
        ">",
        "Landroid/widget/FrameLayout;",
        "Lcom/squareup/text/HasSelectableText;",
        "Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;"
    }
.end annotation


# instance fields
.field private final inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

.field protected final textField:Landroid/widget/EditText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final touchRegion:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    invoke-static {p1, p3, p0}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 51
    sget p3, Lcom/squareup/widgets/pos/R$id;->glyph_edit_text_field:I

    invoke-static {p0, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/EditText;

    iput-object p3, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    .line 52
    sget p3, Lcom/squareup/widgets/pos/R$id;->inline_button:I

    invoke-static {p0, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p3, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    .line 53
    sget p3, Lcom/squareup/widgets/pos/R$id;->touch_region:I

    invoke-static {p0, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->touchRegion:Landroid/view/View;

    .line 55
    iget-object p3, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-static {p0, p3}, Lcom/squareup/util/Views;->expandTouchArea(Landroid/view/View;Landroid/view/View;)V

    .line 57
    sget-object p3, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 59
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_android_text:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->setText(Ljava/lang/CharSequence;)V

    .line 60
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_android_hint:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 62
    iget-object p2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 64
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_android_imeOptions:I

    const/4 v0, 0x1

    .line 65
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    .line 66
    invoke-virtual {p0, p2}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->setImeOptions(I)V

    .line 67
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_android_inputType:I

    .line 68
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    .line 69
    invoke-virtual {p0, p2}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->setInputType(I)V

    .line 72
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_buttonGlyph:I

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIGHT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {p1, p2, v0, v1}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 74
    sget v0, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_buttonGlyphColor:I

    const v1, 0x106000b

    .line 75
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 76
    sget v1, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_buttonGlyphBackground:I

    .line 77
    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$drawable;->marin_selector_glyph_button_inline:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 83
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v2, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 84
    iget-object p2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColorRes(I)V

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphView;->getPaddingLeft()I

    move-result p2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getPaddingRight()I

    move-result v0

    .line 88
    iget-object v2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 89
    iget-object v1, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1, p2, p3, v0, p3}, Lcom/squareup/glyph/SquareGlyphView;->setPadding(IIII)V

    .line 91
    iget-object p2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->touchRegion:Landroid/view/View;

    sget p3, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_buttonGlyphHint:I

    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 94
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_glyph:I

    sget-object p3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    const/4 v0, 0x0

    invoke-static {p1, p2, p3, v0}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz p2, :cond_2

    .line 96
    sget p3, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText_sq_glyphColor:I

    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p3

    if-nez p3, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    .line 98
    invoke-static {p3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p3

    .line 101
    :cond_1
    new-instance v1, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 102
    invoke-virtual {v1, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p2

    .line 103
    invoke-virtual {p2, p3}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorStateList(Landroid/content/res/ColorStateList;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p2

    .line 104
    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p2

    .line 105
    iget-object p3, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {p3, p2, v0, v0, v0}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 107
    :cond_2
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    check-cast v0, Lcom/squareup/text/HasSelectableText;

    invoke-interface {v0, p1}, Lcom/squareup/text/HasSelectableText;->addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    return-void
.end method

.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public append(Ljava/lang/CharSequence;)V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public getEditText()Landroid/widget/EditText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    return-object v0
.end method

.method public getSelectionEnd()I
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    return v0
.end method

.method public getSelectionStart()I
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    return v0
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getText()Ljava/lang/CharSequence;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onRestoreInstanceState$0$BaseGlyphButtonEditText()V
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 226
    check-cast p1, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;

    .line 227
    invoke-virtual {p1}, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-static {p1}, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->access$100(Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 230
    invoke-static {p1}, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->access$200(Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 231
    new-instance p1, Lcom/squareup/ui/library/-$$Lambda$BaseGlyphButtonEditText$ah6jYeL35By595PfqKkOxEyePNA;

    invoke-direct {p1, p0}, Lcom/squareup/ui/library/-$$Lambda$BaseGlyphButtonEditText$ah6jYeL35By595PfqKkOxEyePNA;-><init>(Lcom/squareup/ui/library/BaseGlyphButtonEditText;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .line 221
    new-instance v0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    .line 222
    invoke-virtual {v3}, Landroid/widget/EditText;->isFocused()Z

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;ZLcom/squareup/ui/library/BaseGlyphButtonEditText$1;)V

    return-object v0
.end method

.method public postRestartInput()V
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    check-cast v0, Lcom/squareup/text/HasSelectableText;

    invoke-interface {v0}, Lcom/squareup/text/HasSelectableText;->postRestartInput()V

    return-void
.end method

.method public removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    check-cast v0, Lcom/squareup/text/HasSelectableText;

    invoke-interface {v0, p1}, Lcom/squareup/text/HasSelectableText;->removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    return-void
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public requestEditFocus()V
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public setButtonEnabled(Z)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->touchRegion:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    return-void
.end method

.method public setButtonVisibility(I)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->inlineButtonView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 168
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 170
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->setButtonEnabled(Z)V

    return-void
.end method

.method public setHint(I)V
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setImeOptions(I)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setImeOptions(I)V

    return-void
.end method

.method public setInputType(I)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setInputType(I)V

    return-void
.end method

.method public setKeyListener(Landroid/text/method/KeyListener;)V
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->touchRegion:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public setSelection(II)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->setSelection(II)V

    return-void
.end method

.method public setText(I)V
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(I)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object p1, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText;->textField:Landroid/widget/EditText;

    invoke-virtual {v0, p1, p1}, Landroid/widget/EditText;->setSelection(II)V

    return-void
.end method
