.class public final Lcom/squareup/ui/buyerflow/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyerflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final checkout_card_min_height:I = 0x7f0700c7

.field public static final checkout_card_width:I = 0x7f0700c8

.field public static final checkout_top_margin:I = 0x7f0700cb

.field public static final crm_reward_progress_size:I = 0x7f0700ed

.field public static final crm_reward_status_box_height:I = 0x7f0700ee

.field public static final loyalty_cash_app_banner_height:I = 0x7f0701eb

.field public static final loyalty_cash_app_banner_margin:I = 0x7f0701ec

.field public static final loyalty_cash_app_banner_margin_half:I = 0x7f0701ed

.field public static final loyalty_cash_app_banner_text_size:I = 0x7f0701ee

.field public static final loyalty_cash_app_logo_height:I = 0x7f0701ef

.field public static final loyalty_contents_margin:I = 0x7f0701f0

.field public static final loyalty_contents_width:I = 0x7f0701f2

.field public static final loyalty_enrollment_slide_up:I = 0x7f0701f3

.field public static final loyalty_phone_edit_text_size:I = 0x7f0701f4

.field public static final loyalty_subtitle_text_size:I = 0x7f0701f6

.field public static final loyalty_subtitle_welcome_text_size:I = 0x7f0701f7

.field public static final loyalty_title_welcome_text_size:I = 0x7f0701f8

.field public static final sign_line_small_x_dimen:I = 0x7f0704bb

.field public static final signature_line_height:I = 0x7f0704bc

.field public static final text_checkout_headline:I = 0x7f07052c

.field public static final text_checkout_subtitle:I = 0x7f07052d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
