.class public interface abstract Lcom/squareup/ui/items/EditServiceVariationScreen$Component;
.super Ljava/lang/Object;
.source "EditServiceVariationScreen.kt"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;
.implements Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/items/EditServiceVariationScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/ErrorsBarPresenter$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/ui/items/DuplicateSkuValidator$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;,
        Lcom/squareup/ui/items/EditServiceVariationScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditServiceVariationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H&\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditServiceVariationScreen$Component;",
        "Lcom/squareup/ui/ErrorsBarView$Component;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;",
        "editServiceVariationCoordinator",
        "Lcom/squareup/ui/items/EditServiceVariationCoordinator;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract editServiceVariationCoordinator()Lcom/squareup/ui/items/EditServiceVariationCoordinator;
.end method
