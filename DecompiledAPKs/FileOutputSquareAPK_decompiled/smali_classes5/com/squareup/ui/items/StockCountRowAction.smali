.class public Lcom/squareup/ui/items/StockCountRowAction;
.super Ljava/lang/Object;
.source "StockCountRowAction.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/ui/items/EditInventoryState;",
        ">;"
    }
.end annotation


# instance fields
.field private final bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

.field private final editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

.field private final editItemState:Lcom/squareup/ui/items/EditItemState;

.field private final isNewVariation:Z

.field private final res:Lcom/squareup/util/Res;

.field private final rowClickedCallback:Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;

.field private final scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

.field private final stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

.field private variationId:Ljava/lang/String;

.field private final variationMerchantCatalogToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState;Lcom/squareup/ui/items/widgets/StockCountRow;Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;Lcom/squareup/util/Res;Ljavax/inject/Provider;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/EditItemState;",
            "Lcom/squareup/ui/items/widgets/StockCountRow;",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            "Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;I)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/items/StockCountRowAction;->variationId:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/squareup/ui/items/StockCountRowAction;->variationMerchantCatalogToken:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/squareup/ui/items/StockCountRowAction;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 49
    iput-object p4, p0, Lcom/squareup/ui/items/StockCountRowAction;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    .line 50
    iput-object p5, p0, Lcom/squareup/ui/items/StockCountRowAction;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    .line 51
    iput-object p6, p0, Lcom/squareup/ui/items/StockCountRowAction;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    .line 52
    iput-object p7, p0, Lcom/squareup/ui/items/StockCountRowAction;->rowClickedCallback:Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;

    .line 53
    iput-object p8, p0, Lcom/squareup/ui/items/StockCountRowAction;->res:Lcom/squareup/util/Res;

    if-nez p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 54
    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ui/items/StockCountRowAction;->isNewVariation:Z

    .line 55
    new-instance p1, Lcom/squareup/quantity/BigDecimalFormatter;

    sget-object p2, Lcom/squareup/quantity/BigDecimalFormatter$Format;->ROUNDING_SCALE:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    invoke-direct {p1, p9, p2}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;)V

    iput-object p1, p0, Lcom/squareup/ui/items/StockCountRowAction;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/items/StockCountRowAction;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-virtual {p1, p10}, Lcom/squareup/quantity/BigDecimalFormatter;->setMaxPaddedFractionDigits(I)V

    return-void
.end method

.method private showDisallowInventoryApi()V
    .locals 3

    .line 113
    iget-boolean v0, p0, Lcom/squareup/ui/items/StockCountRowAction;->isNewVariation:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/StockCountRowAction;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->inventory_receive_stock:I

    .line 114
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/StockCountRowAction;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->inventory_view_stock:I

    .line 115
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/StockCountRowAction;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$jPOeKq1CtMk-VprzxtjXIEt7U3c;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$jPOeKq1CtMk-VprzxtjXIEt7U3c;-><init>(Lcom/squareup/ui/items/StockCountRowAction;)V

    .line 117
    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    .line 116
    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/items/widgets/StockCountRow;->showViewStockCountField(Ljava/lang/String;Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private showNumberInStockOrReceiveStock(Lcom/squareup/ui/items/EditInventoryState;)V
    .locals 5

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/items/StockCountRowAction;->variationId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditInventoryState;->getCurrentStockCountForVariation(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/squareup/ui/items/StockCountRowAction;->variationId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/items/EditInventoryState;->getCurrentUnitCostForVariation(Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 92
    iget-object v2, p0, Lcom/squareup/ui/items/StockCountRowAction;->variationId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/squareup/ui/items/EditInventoryState;->hasServerStockCountForVariation(Ljava/lang/String;)Z

    move-result p1

    if-nez v0, :cond_0

    .line 96
    iget-object v2, p0, Lcom/squareup/ui/items/StockCountRowAction;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/edititem/R$string;->inventory_receive_stock:I

    .line 97
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/items/StockCountRowAction;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/edititem/R$string;->inventory_stock_count:I

    .line 98
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/StockCountRowAction;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    .line 99
    invoke-virtual {v3, v0}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "count"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 100
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 101
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 102
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/items/StockCountRowAction;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    new-instance v4, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;

    invoke-direct {v4, p0, p1, v0, v1}, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;-><init>(Lcom/squareup/ui/items/StockCountRowAction;ZLjava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    .line 103
    invoke-static {v4}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    .line 102
    invoke-virtual {v3, v2, p1}, Lcom/squareup/ui/items/widgets/StockCountRow;->showViewStockCountField(Ljava/lang/String;Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private showReloadStockCount()V
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/items/StockCountRowAction;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    iget-object v1, p0, Lcom/squareup/ui/items/StockCountRowAction;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->inventory_stock_count_reload:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$KP4iB5FdsvdG2DYdIFZ_wjiFT44;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$KP4iB5FdsvdG2DYdIFZ_wjiFT44;-><init>(Lcom/squareup/ui/items/StockCountRowAction;)V

    .line 109
    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    .line 108
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/widgets/StockCountRow;->showViewStockCountField(Ljava/lang/String;Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/ui/items/EditInventoryState;)V
    .locals 3

    .line 61
    sget-object v0, Lcom/squareup/ui/items/StockCountRowAction$1;->$SwitchMap$com$squareup$ui$items$EditInventoryStateController$InventoryStockCountsLoadStatus:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditInventoryState;->getInventoryStockCountsLoadStatus()Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/squareup/ui/items/StockCountRowAction;->showDisallowInventoryApi()V

    goto :goto_0

    .line 84
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal InventoryStockCountsLoadStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditInventoryState;->getInventoryStockCountsLoadStatus()Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/ui/items/StockCountRowAction;->isNewVariation:Z

    if-eqz v0, :cond_2

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/StockCountRowAction;->showNumberInStockOrReceiveStock(Lcom/squareup/ui/items/EditInventoryState;)V

    goto :goto_0

    .line 77
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/items/StockCountRowAction;->showReloadStockCount()V

    goto :goto_0

    .line 71
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/StockCountRowAction;->showNumberInStockOrReceiveStock(Lcom/squareup/ui/items/EditInventoryState;)V

    goto :goto_0

    .line 64
    :cond_4
    iget-boolean v0, p0, Lcom/squareup/ui/items/StockCountRowAction;->isNewVariation:Z

    if-eqz v0, :cond_5

    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/StockCountRowAction;->showNumberInStockOrReceiveStock(Lcom/squareup/ui/items/EditInventoryState;)V

    goto :goto_0

    .line 67
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/items/StockCountRowAction;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    invoke-virtual {p1}, Lcom/squareup/ui/items/widgets/StockCountRow;->showLoadingStockCountSpinner()V

    :goto_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/StockCountRowAction;->call(Lcom/squareup/ui/items/EditInventoryState;)V

    return-void
.end method

.method public synthetic lambda$showDisallowInventoryApi$2$StockCountRowAction(Landroid/view/View;)V
    .locals 0

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/items/StockCountRowAction;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->goToDisallowInventoryApiDialog()V

    return-void
.end method

.method public synthetic lambda$showNumberInStockOrReceiveStock$0$StockCountRowAction(ZLjava/math/BigDecimal;Lcom/squareup/protos/common/Money;Landroid/view/View;)V
    .locals 0

    .line 103
    iget-object p4, p0, Lcom/squareup/ui/items/StockCountRowAction;->rowClickedCallback:Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;

    invoke-interface {p4, p1, p2, p3}, Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;->viewStockCountFieldClicked(ZLjava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public synthetic lambda$showReloadStockCount$1$StockCountRowAction(Landroid/view/View;)V
    .locals 1

    .line 109
    iget-object p1, p0, Lcom/squareup/ui/items/StockCountRowAction;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    iget-object v0, p0, Lcom/squareup/ui/items/StockCountRowAction;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditInventoryStateController;->loadInventoryStockCountsData(Lcom/squareup/ui/items/EditItemState;)V

    return-void
.end method
