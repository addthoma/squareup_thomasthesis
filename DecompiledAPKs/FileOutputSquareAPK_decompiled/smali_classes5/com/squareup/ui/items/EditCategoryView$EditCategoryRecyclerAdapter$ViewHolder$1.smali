.class Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditCategoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->bindItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

.field final synthetic val$itemPosition:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;I)V
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    iput p2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;->val$itemPosition:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    iget v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;->val$itemPosition:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemSelected(I)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;->val$itemPosition:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->notifyItemChanged(I)V

    return-void
.end method
