.class public final Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;
.super Ljava/lang/Object;
.source "DetailSearchableListUnitBehavior.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J6\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0004H\u0002J:\u0010\u0011\u001a,\u0012(\u0012&\u0012\u000c\u0012\n \u0015*\u0004\u0018\u00010\u00140\u0014 \u0015*\u0012\u0012\u000c\u0012\n \u0015*\u0004\u0018\u00010\u00140\u0014\u0018\u00010\u00160\u00130\u00122\u0006\u0010\u0008\u001a\u00020\tH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;",
        "",
        "()V",
        "unitMatchRegexStr",
        "",
        "getLatestUnitsListData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "locale",
        "Ljava/util/Locale;",
        "res",
        "Lcom/squareup/util/Res;",
        "searchTerm",
        "readUnitsList",
        "Lio/reactivex/Single;",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "kotlin.jvm.PlatformType",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getLatestUnitsListData(Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 0

    .line 51
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;->getLatestUnitsListData(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$readUnitsList(Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;->readUnitsList(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final getLatestUnitsListData(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/badbus/BadBus;",
            "Ljava/util/Locale;",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    .line 61
    const-class v0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;

    .line 60
    invoke-virtual {p2, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p2

    .line 62
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p2

    .line 63
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$2;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p2

    .line 64
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p2

    .line 65
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;

    invoke-direct {v0, p1, p4, p3, p5}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Res;Ljava/util/Locale;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "badBus.events(\n        C\u2026  )\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final readUnitsList(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;>;"
        }
    .end annotation

    .line 96
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;

    check-cast v0, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {p1, v0}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs.asSingle { local ->\u2026ntUnit::class.java)\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
