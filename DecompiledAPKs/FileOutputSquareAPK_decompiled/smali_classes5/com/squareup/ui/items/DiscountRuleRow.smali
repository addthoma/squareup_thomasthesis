.class public Lcom/squareup/ui/items/DiscountRuleRow;
.super Lcom/squareup/marin/widgets/BorderedLinearLayout;
.source "DiscountRuleRow.java"


# instance fields
.field private final colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

.field private colorStripEnabled:Z

.field private final marginWithColorStrip:I

.field private rowKey:Lcom/squareup/marketfont/MarketTextView;

.field private rowVal:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRuleRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 30
    sget p2, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    .line 31
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 30
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/DiscountRuleRow;->setBorderWidth(I)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRuleRow;->setBordersToMultiply()V

    const/16 p2, 0x8

    .line 33
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/DiscountRuleRow;->addBorder(I)V

    .line 34
    new-instance p2, Lcom/squareup/marin/widgets/BorderPainter;

    sget v0, Lcom/squareup/edititem/R$dimen;->discount_rule_row_color_strip_thickness:I

    invoke-direct {p2, p0, v0}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;I)V

    iput-object p2, p0, Lcom/squareup/ui/items/DiscountRuleRow;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    .line 35
    iget-object p2, p0, Lcom/squareup/ui/items/DiscountRuleRow;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 36
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_thumbnail_content:I

    .line 37
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    sget v0, Lcom/squareup/marin/R$dimen;->marin_text_tile_color_block_width:I

    .line 38
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    add-int/2addr p2, p1

    iput p2, p0, Lcom/squareup/ui/items/DiscountRuleRow;->marginWithColorStrip:I

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 66
    iget-boolean v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->colorStripEnabled:Z

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorders(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 43
    invoke-super {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->onFinishInflate()V

    .line 44
    sget v0, Lcom/squareup/edititem/R$id;->rowKey:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->rowKey:Lcom/squareup/marketfont/MarketTextView;

    .line 45
    sget v0, Lcom/squareup/edititem/R$id;->rowValue:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->rowVal:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method public setColorStrip(I)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->setColor(I)V

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 58
    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ui/items/DiscountRuleRow;->colorStripEnabled:Z

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/items/DiscountRuleRow;->rowKey:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    .line 60
    iget-boolean v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->colorStripEnabled:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->marginWithColorStrip:I

    goto :goto_1

    :cond_1
    iget v0, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    :goto_1
    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->rowKey:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setRowKey(Ljava/lang/String;)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->rowKey:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setRowVal(Ljava/lang/String;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleRow;->rowVal:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
