.class public final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflowRunner_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final convertItemsUrlHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final editItemGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final factoryOfViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->factoryOfViewFactoryProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->editItemGatewayProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p10, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p11, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p12, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->convertItemsUrlHelperProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p13, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)",
            "Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;"
        }
    .end annotation

    .line 92
    new-instance v14, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;
    .locals 15

    .line 102
    new-instance v14, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;-><init>(Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;
    .locals 14

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->factoryOfViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->editItemGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/items/EditItemGateway;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->convertItemsUrlHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/util/BrowserLauncher;

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->newInstance(Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner_Factory_Factory;->get()Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;

    move-result-object v0

    return-object v0
.end method
