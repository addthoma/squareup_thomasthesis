.class public abstract Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;
.super Ljava/lang/Object;
.source "UnitSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "UnitType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerItem;,
        Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerService;,
        Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;,
        Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u000b\u000c\r\u000eB\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0004\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;",
        "",
        "screen",
        "Lcom/squareup/ui/items/unit/UnitSelectionScreen;",
        "id",
        "",
        "(Lcom/squareup/ui/items/unit/UnitSelectionScreen;Ljava/lang/String;)V",
        "getId",
        "()Ljava/lang/String;",
        "getScreen",
        "()Lcom/squareup/ui/items/unit/UnitSelectionScreen;",
        "PerDefaultStandardUnit",
        "PerItem",
        "PerService",
        "PerUnit",
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerItem;",
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerService;",
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;",
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final id:Ljava/lang/String;

.field private final screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;Ljava/lang/String;)V
    .locals 0

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;->screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;->id:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 203
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getId()Ljava/lang/String;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getScreen()Lcom/squareup/ui/items/unit/UnitSelectionScreen;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;->screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    return-object v0
.end method
