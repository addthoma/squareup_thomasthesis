.class final Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AssignUnitToVariationState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->toSnapshot()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    .line 61
    instance-of v1, v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->access$writeSelectUnit(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;Lokio/BufferedSink;Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V

    goto :goto_0

    .line 63
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    if-eqz v1, :cond_1

    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    invoke-virtual {v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->access$writeSelectUnit(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;Lokio/BufferedSink;Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V

    goto :goto_0

    .line 65
    :cond_1
    instance-of v1, v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;

    if-eqz v1, :cond_2

    .line 66
    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;

    invoke-virtual {v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->access$writeSelectUnit(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;Lokio/BufferedSink;Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;->getProposedUnitId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto :goto_0

    .line 70
    :cond_2
    instance-of v0, v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;

    if-eqz v0, :cond_3

    .line 71
    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;

    invoke-virtual {v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->access$writeSelectUnit(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;Lokio/BufferedSink;Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;->getDefaultStandardUnitToAdd()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_3
    :goto_0
    return-void
.end method
