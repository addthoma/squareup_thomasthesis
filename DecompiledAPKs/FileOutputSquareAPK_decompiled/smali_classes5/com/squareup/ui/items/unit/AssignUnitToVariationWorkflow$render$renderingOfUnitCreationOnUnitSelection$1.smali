.class final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AssignUnitToVariationWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->render(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "+",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAssignUnitToVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AssignUnitToVariationWorkflow.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,320:1\n704#2:321\n777#2,2:322\n*E\n*S KotlinDebug\n*F\n+ 1 AssignUnitToVariationWorkflow.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1\n*L\n214#1:321\n214#1,2:322\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        "output",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

.field final synthetic this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;Lcom/squareup/ui/items/unit/AssignUnitToVariationState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/EditUnitResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitResult$EditSaved;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 209
    check-cast p1, Lcom/squareup/items/unit/EditUnitResult$EditSaved;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitResult$EditSaved;->getSavedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v3

    const-string v4, "output.savedUnit.id"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    iget-object v4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v4, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    invoke-virtual {v4}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectableUnits()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    .line 211
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitResult$EditSaved;->getSavedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;->this$0:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

    invoke-static {v6}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->access$getRes$p(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;)Lcom/squareup/util/Res;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/squareup/items/unit/SelectableUnitKt;->toSelectableUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/items/unit/SelectableUnit;

    move-result-object v5

    .line 210
    invoke-static {v4, v5}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 212
    iget-object v5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v5, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    invoke-virtual {v5}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getCreatedUnits()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitResult$EditSaved;->getSavedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v6

    invoke-static {v5, v6}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 214
    iget-object v6, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v6, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    invoke-virtual {v6}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getDefaultStandardUnits()Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/lang/Iterable;

    .line 321
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    check-cast v7, Ljava/util/Collection;

    .line 322
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 215
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitResult$EditSaved;->getSavedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v10

    invoke-virtual {v10}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v10

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    if-eqz v9, :cond_0

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 323
    :cond_1
    check-cast v7, Ljava/util/List;

    .line 208
    new-instance p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-direct {p1, v3, v4, v5, v7}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 207
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 219
    :cond_2
    instance-of p1, p1, Lcom/squareup/items/unit/EditUnitResult$EditDiscarded;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/items/unit/EditUnitResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;->invoke(Lcom/squareup/items/unit/EditUnitResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
