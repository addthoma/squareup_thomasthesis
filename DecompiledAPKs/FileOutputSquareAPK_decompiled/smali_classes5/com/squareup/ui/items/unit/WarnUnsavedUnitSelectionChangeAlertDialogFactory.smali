.class public final Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory;
.super Ljava/lang/Object;
.source "WarnUnsavedUnitSelectionChangeAlertDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0005H\u0002R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "Landroid/app/AlertDialog;",
        "screen",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory;Landroid/content/Context;Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;)Landroid/app/AlertDialog;
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;)Landroid/app/AlertDialog;
    .locals 2

    .line 30
    new-instance v0, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory$createDialog$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory$createDialog$1;-><init>(Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;)V

    check-cast v0, Ljava/lang/Runnable;

    .line 31
    new-instance v1, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory$createDialog$2;

    invoke-direct {v1, p2}, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory$createDialog$2;-><init>(Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;)V

    check-cast v1, Ljava/lang/Runnable;

    .line 29
    invoke-static {p1, v0, v1}, Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;->createConfirmDiscardChangesAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "createConfirmDiscardChan\u2026screen.onEvent(Resume) })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 21
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory$create$1;-><init>(Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .take(1)\u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
