.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "AssignUnitToVariationWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Scope"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAssignUnitToVariationWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AssignUnitToVariationWorkflowRunner.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,225:1\n35#2:226\n1642#3,2:227\n*E\n*S KotlinDebug\n*F\n+ 1 AssignUnitToVariationWorkflowRunner.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope\n*L\n90#1:226\n114#1,2:227\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001c\u001dB=\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0014J\u0008\u0010\u001a\u001a\u00020\u001bH\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parent",
        "itemType",
        "Lcom/squareup/ui/items/unit/ItemType;",
        "initialSelectedUnitId",
        "",
        "selectableUnits",
        "",
        "Lcom/squareup/items/unit/SelectableUnit;",
        "doesVariationHaveStock",
        "",
        "shouldShowDefaultStandardUnits",
        "(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V",
        "getParent",
        "()Lcom/squareup/ui/main/RegisterTreeKey;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getParentKey",
        "",
        "Companion",
        "ParentComponent",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion;


# instance fields
.field private final doesVariationHaveStock:Z

.field private final initialSelectedUnitId:Ljava/lang/String;

.field private final itemType:Lcom/squareup/ui/items/unit/ItemType;

.field private final parent:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final selectableUnits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final shouldShowDefaultStandardUnits:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion;

    .line 130
    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026rdUnits\n        )\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Lcom/squareup/ui/items/unit/ItemType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "itemType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialSelectedUnitId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectableUnits"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->initialSelectedUnitId:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->selectableUnits:Ljava/util/List;

    iput-boolean p5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->doesVariationHaveStock:Z

    iput-boolean p6, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->shouldShowDefaultStandardUnits:Z

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 8

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    const-class v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 90
    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;

    .line 91
    invoke-interface {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;->assignUnitToVariationWorkflowRunner()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;

    move-result-object v0

    .line 94
    new-instance v7, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    .line 95
    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    .line 96
    iget-object v3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->initialSelectedUnitId:Ljava/lang/String;

    .line 97
    iget-object v4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->selectableUnits:Ljava/util/List;

    .line 98
    iget-boolean v5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->doesVariationHaveStock:Z

    .line 99
    iget-boolean v6, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->shouldShowDefaultStandardUnits:Z

    move-object v1, v7

    .line 94
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;-><init>(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V

    .line 93
    invoke-virtual {v0, v7}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->updateAssignUnitToVariationInput(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;)V

    .line 102
    invoke-super {p0, p1}, Lcom/squareup/ui/main/RegisterTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p1

    const-string v1, "it"

    .line 103
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string v0, "super.buildScope(parentS\u2026er.registerServices(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 111
    iget-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/ItemType;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->initialSelectedUnitId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    iget-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->selectableUnits:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    iget-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->selectableUnits:Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 227
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/unit/SelectableUnit;

    .line 115
    invoke-virtual {v0}, Lcom/squareup/items/unit/SelectableUnit;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v0}, Lcom/squareup/items/unit/SelectableUnit;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v0}, Lcom/squareup/items/unit/SelectableUnit;->getPrecision()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 118
    invoke-virtual {v0}, Lcom/squareup/items/unit/SelectableUnit;->getDisplayPrecision()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v0}, Lcom/squareup/items/unit/SelectableUnit;->getBackingUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0

    .line 121
    :cond_0
    iget-boolean p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->doesVariationHaveStock:Z

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeBooleanAsInt(Landroid/os/Parcel;Z)V

    .line 122
    iget-boolean p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->shouldShowDefaultStandardUnits:Z

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeBooleanAsInt(Landroid/os/Parcel;Z)V

    return-void
.end method

.method public final getParent()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method
