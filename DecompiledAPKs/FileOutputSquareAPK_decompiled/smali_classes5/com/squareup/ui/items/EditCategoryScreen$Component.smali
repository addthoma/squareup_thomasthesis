.class public interface abstract Lcom/squareup/ui/items/EditCategoryScreen$Component;
.super Ljava/lang/Object;
.source "EditCategoryScreen.java"

# interfaces
.implements Lcom/squareup/ui/items/AppliedLocationsBanner$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/items/EditCategoryScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/items/EditCategoryView;)V
.end method
