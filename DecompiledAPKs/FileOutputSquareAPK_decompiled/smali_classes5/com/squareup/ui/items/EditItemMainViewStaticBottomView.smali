.class public Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;
.super Lcom/squareup/widgets/ResponsiveLinearLayout;
.source "EditItemMainViewStaticBottomView.java"


# instance fields
.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private checkoutLink:Lcom/squareup/noho/NohoRow;

.field private checkoutLinkContainer:Landroid/view/View;

.field private checkoutLinkSettingsHelpText:Lcom/squareup/widgets/MessageView;

.field private checkoutLinkToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private createVariationButton:Landroid/view/View;

.field private delete:Lcom/squareup/ui/ConfirmButton;

.field private editableDescription:Landroid/widget/EditText;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final leftDivider:Landroid/graphics/drawable/Drawable;

.field private modifierListContainer:Landroid/view/ViewGroup;

.field private modifiersHeader:Landroid/view/View;

.field presenter:Lcom/squareup/ui/items/EditItemMainPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private priceHelpText:Lcom/squareup/widgets/MessageView;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final rightDivider:Landroid/graphics/drawable/Drawable;

.field private subscriptions:Lrx/subscriptions/CompositeSubscription;

.field private taxesContainer:Lcom/squareup/widgets/ColumnLayout;

.field private taxesHeader:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditItemMainScreen$Component;->inject(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;)V

    .line 83
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/noho/R$drawable;->noho_divider_horizontal_hairline:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->leftDivider:Landroid/graphics/drawable/Drawable;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/noho/R$drawable;->noho_divider_horizontal_hairline:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->rightDivider:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;Ljava/lang/String;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->onClickCheckoutLink(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$onFinishInflate$1(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .line 108
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/squareup/edititem/R$id;->edit_item_description:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 109
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 110
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    and-int/lit16 p1, p1, 0xff

    if-ne p1, v1, :cond_0

    .line 111
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    invoke-interface {p0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    return v2
.end method

.method private onClickCheckoutLink(Ljava/lang/String;)V
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLink:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->checkoutLinkClicked(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private setAlwaysEditableContentOnBottom(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditItemState$TaxState;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditItemState$ModifierState;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    const/16 v0, 0x8

    const/4 v1, 0x0

    if-eqz p4, :cond_0

    .line 199
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v2}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v2

    if-nez v2, :cond_0

    .line 200
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->createVariationButton:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 202
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->createVariationButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->updateHelpText()V

    .line 204
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    .line 205
    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemMainPresenter;->inclusiveTaxPossiblyChanged()Lrx/Observable;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$X5-X728ELKUWU7EfzZKhz1y7DXY;

    invoke-direct {v4, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$X5-X728ELKUWU7EfzZKhz1y7DXY;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;)V

    invoke-virtual {v3, v4}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v3

    .line 204
    invoke-virtual {v2, v3}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 208
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->modifierListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 209
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p4, :cond_1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 210
    invoke-interface {v2}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_2

    .line 214
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->modifiersHeader:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->modifierListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    const/4 v2, 0x0

    .line 216
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 217
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/items/EditItemState$ModifierState;

    .line 218
    new-instance v4, Lcom/squareup/ui/CheckBoxListRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/squareup/ui/CheckBoxListRow;-><init>(Landroid/content/Context;)V

    .line 219
    iget-object v5, v3, Lcom/squareup/ui/items/EditItemState$ModifierState;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->res:Lcom/squareup/util/Res;

    invoke-virtual {v3, v6}, Lcom/squareup/ui/items/EditItemState$ModifierState;->getOptionsCommaSeparatedList(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v6

    iget-boolean v7, v3, Lcom/squareup/ui/items/EditItemState$ModifierState;->enabled:Z

    invoke-virtual {v4, v5, v6, v7}, Lcom/squareup/ui/CheckBoxListRow;->showItem(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 220
    new-instance v5, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;

    invoke-direct {v5, p0, v4, v3}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;Lcom/squareup/ui/CheckBoxListRow;Lcom/squareup/ui/items/EditItemState$ModifierState;)V

    invoke-virtual {v4, v5}, Lcom/squareup/ui/CheckBoxListRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->modifierListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 211
    :cond_2
    :goto_2
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->modifiersHeader:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->modifierListContainer:Landroid/view/ViewGroup;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 230
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesContainer:Lcom/squareup/widgets/ColumnLayout;

    invoke-virtual {p2}, Lcom/squareup/widgets/ColumnLayout;->removeAllViews()V

    const/4 p2, 0x0

    const/4 v2, 0x0

    .line 233
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge p2, v3, :cond_4

    .line 234
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/items/EditItemState$TaxState;

    iget-boolean v3, v3, Lcom/squareup/ui/items/EditItemState$TaxState;->enabled:Z

    or-int/2addr v2, v3

    add-int/lit8 p2, p2, 0x1

    goto :goto_3

    .line 237
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_9

    if-eqz v2, :cond_9

    if-eqz p4, :cond_5

    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 239
    invoke-interface {p2}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result p2

    if-nez p2, :cond_5

    goto/16 :goto_6

    .line 243
    :cond_5
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    .line 244
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half_lollipop:I

    .line 245
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesContainer:Lcom/squareup/widgets/ColumnLayout;

    invoke-virtual {p2}, Lcom/squareup/widgets/ColumnLayout;->isTwoColumn()Z

    move-result p2

    if-eqz p2, :cond_6

    .line 247
    iget-object p4, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesContainer:Lcom/squareup/widgets/ColumnLayout;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->leftDivider:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->rightDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v0, v2}, Lcom/squareup/widgets/ColumnLayout;->setColumnDividers(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_6
    const/4 p4, 0x0

    .line 250
    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p4, v0, :cond_8

    .line 251
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemState$TaxState;

    .line 254
    iget-boolean v2, v0, Lcom/squareup/ui/items/EditItemState$TaxState;->enabled:Z

    if-nez v2, :cond_7

    goto :goto_5

    .line 260
    :cond_7
    sget v6, Lcom/squareup/noho/R$dimen;->noho_gap_0:I

    .line 261
    sget v7, Lcom/squareup/noho/R$dimen;->noho_gap_0:I

    .line 263
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesContainer:Lcom/squareup/widgets/ColumnLayout;

    invoke-virtual {v2}, Lcom/squareup/widgets/ColumnLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/ui/items/EditItemState$TaxState;->name:Ljava/lang/String;

    .line 264
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/squareup/configure/item/R$string;->percent:I

    invoke-static {v2, v5}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v5, v0, Lcom/squareup/ui/items/EditItemState$TaxState;->percentage:Ljava/lang/String;

    const-string v8, "content"

    .line 265
    invoke-virtual {v2, v8, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 266
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v5

    sget v8, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    .line 263
    invoke-static/range {v3 .. v8}, Lcom/squareup/widgets/list/ToggleButtonRow;->switchRowPreservedLabel(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object v2

    .line 270
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/list/ToggleButtonRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    iget-boolean v3, v0, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    invoke-virtual {v2, v3, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    .line 272
    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$KIwrlz5q5um3SfAWQSD5eT35IBE;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$KIwrlz5q5um3SfAWQSD5eT35IBE;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;Lcom/squareup/ui/items/EditItemState$TaxState;)V

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesContainer:Lcom/squareup/widgets/ColumnLayout;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/ColumnLayout;->addView(Landroid/view/View;)V

    :goto_5
    add-int/lit8 p4, p4, 0x1

    goto :goto_4

    .line 276
    :cond_8
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesHeader:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 277
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesContainer:Lcom/squareup/widgets/ColumnLayout;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/ColumnLayout;->setVisibility(I)V

    goto :goto_7

    .line 240
    :cond_9
    :goto_6
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesHeader:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 241
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesContainer:Lcom/squareup/widgets/ColumnLayout;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ColumnLayout;->setVisibility(I)V

    :goto_7
    if-eqz p3, :cond_a

    .line 281
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->delete:Lcom/squareup/ui/ConfirmButton;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->delete:Lcom/squareup/ui/ConfirmButton;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    .line 283
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->delete:Lcom/squareup/ui/ConfirmButton;

    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p3, Lcom/squareup/ui/items/-$$Lambda$jcwh4F2DeAU2Cj7SoQdiEls2Y6g;

    invoke-direct {p3, p2}, Lcom/squareup/ui/items/-$$Lambda$jcwh4F2DeAU2Cj7SoQdiEls2Y6g;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-virtual {p1, p3}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    :cond_a
    return-void
.end method

.method private updateCheckoutLinkUi(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 4

    .line 132
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showCheckoutLink:Z

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLinkContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLinkToggle:Lcom/squareup/noho/NohoCheckableRow;

    iget-boolean v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->checkoutLinkToggleState:Z

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLinkToggle:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$WZXmyArd4GASTglcJ_2QjM7_Nn0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$WZXmyArd4GASTglcJ_2QjM7_Nn0;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLink:Lcom/squareup/noho/NohoRow;

    iget-object v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->checkoutLink:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLink:Lcom/squareup/noho/NohoRow;

    iget-boolean v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->checkoutLinkToggleState:Z

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setEnabled(Z)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLink:Lcom/squareup/noho/NohoRow;

    new-instance v1, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$2;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLink:Lcom/squareup/noho/NohoRow;

    new-instance v1, Lcom/squareup/noho/NohoRow$Action$ActionIcon;

    sget v2, Lcom/squareup/vectoricons/R$drawable;->ui_share_24:I

    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$ZKkoKg0uQTdOROakoIV5EJg5Rq0;

    invoke-direct {v3, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$ZKkoKg0uQTdOROakoIV5EJg5Rq0;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    invoke-direct {v1, v2, v3}, Lcom/squareup/noho/NohoRow$Action$ActionIcon;-><init>(ILkotlin/jvm/functions/Function0;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setAction(Lcom/squareup/noho/NohoRow$Action;)V

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLinkSettingsHelpText:Lcom/squareup/widgets/MessageView;

    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/edititem/R$string;->checkout_link_settings_help_text:I

    const-string v2, "settings_checkout_links"

    .line 156
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/edititem/R$string;->checkout_link_settings_to_checkout_links_url:I

    .line 157
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/edititem/R$string;->checkout_link_settings:I

    .line 158
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 155
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 161
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLinkContainer:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private updateHelpText()V
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getPriceHelpText()Ljava/lang/String;

    move-result-object v0

    .line 289
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemMainPresenter;->isService()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 292
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->priceHelpText:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->priceHelpText:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_1

    .line 290
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->priceHelpText:Lcom/squareup/widgets/MessageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private useEditableItemDetailsOnBottom(Ljava/lang/String;)V
    .locals 1

    .line 189
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->editableDescription:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 192
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->editableDescription:Landroid/widget/EditText;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method getAllSubscriptionsOnTheRow()Lrx/Subscription;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$EditItemMainViewStaticBottomView(Landroid/view/View;)V
    .locals 1

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->isService()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_CREATE_VARIATION:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 95
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->addVariationClicked()V

    return-void
.end method

.method public synthetic lambda$setAlwaysEditableContentOnBottom$4$EditItemMainViewStaticBottomView(Lkotlin/Unit;)V
    .locals 0

    .line 205
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->updateHelpText()V

    return-void
.end method

.method public synthetic lambda$setAlwaysEditableContentOnBottom$5$EditItemMainViewStaticBottomView(Lcom/squareup/ui/items/EditItemState$TaxState;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 273
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemState$TaxState;->id:Ljava/lang/String;

    invoke-virtual {p2, p1, p3}, Lcom/squareup/ui/items/EditItemMainPresenter;->taxSwitchChanged(Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic lambda$updateCheckoutLinkUi$2$EditItemMainViewStaticBottomView(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 1

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->checkoutLinksToggleChanged(Z)V

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLink:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setEnabled(Z)V

    .line 139
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public synthetic lambda$updateCheckoutLinkUi$3$EditItemMainViewStaticBottomView(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)Lkotlin/Unit;
    .locals 0

    .line 151
    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->checkoutLink:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->onClickCheckoutLink(Ljava/lang/String;)V

    .line 152
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 89
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveLinearLayout;->onFinishInflate()V

    .line 90
    sget v0, Lcom/squareup/edititem/R$id;->create_variation_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->createVariationButton:Landroid/view/View;

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->createVariationButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$MFaa8UDaWZXtxxgTxenzLkkiMFk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$MFaa8UDaWZXtxxgTxenzLkkiMFk;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_price_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->priceHelpText:Lcom/squareup/widgets/MessageView;

    .line 100
    sget v0, Lcom/squareup/edititem/R$id;->modifier_list_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->modifiersHeader:Landroid/view/View;

    .line 101
    sget v0, Lcom/squareup/edititem/R$id;->modifier_list_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->modifierListContainer:Landroid/view/ViewGroup;

    .line 103
    sget v0, Lcom/squareup/edititem/R$id;->taxes_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesHeader:Landroid/view/View;

    .line 104
    sget v0, Lcom/squareup/edititem/R$id;->taxes_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ColumnLayout;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->taxesContainer:Lcom/squareup/widgets/ColumnLayout;

    .line 106
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_description:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->editableDescription:Landroid/widget/EditText;

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->editableDescription:Landroid/widget/EditText;

    sget-object v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$he0w3XW2VzoUMcdUyxSeaxi73ac;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticBottomView$he0w3XW2VzoUMcdUyxSeaxi73ac;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->editableDescription:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$1;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 122
    sget v0, Lcom/squareup/edititem/R$id;->delete:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->delete:Lcom/squareup/ui/ConfirmButton;

    .line 124
    sget v0, Lcom/squareup/edititem/R$id;->checkout_link_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLinkContainer:Landroid/view/View;

    .line 125
    sget v0, Lcom/squareup/edititem/R$id;->checkout_link_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLinkToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 126
    sget v0, Lcom/squareup/edititem/R$id;->checkout_link:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLink:Lcom/squareup/noho/NohoRow;

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLink:Lcom/squareup/noho/NohoRow;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1}, Lcom/squareup/noho/NohoRowKt;->singleLineLabel(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V

    .line 128
    sget v0, Lcom/squareup/edititem/R$id;->checkout_link_settings_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->checkoutLinkSettingsHelpText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method showContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 4

    .line 172
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 174
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemType:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemType:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_1

    .line 175
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->description:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->useEditableItemDetailsOnBottom(Ljava/lang/String;)V

    .line 178
    :cond_1
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->taxes:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->modifierLists:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->deleteButtonText:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->setAlwaysEditableContentOnBottom(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Z)V

    .line 181
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->updateCheckoutLinkUi(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    return-void
.end method
