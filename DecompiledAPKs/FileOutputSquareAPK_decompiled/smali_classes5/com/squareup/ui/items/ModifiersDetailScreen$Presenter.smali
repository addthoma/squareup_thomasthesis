.class public Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;
.super Lcom/squareup/ui/items/DetailSearchableListPresenter;
.source "ModifiersDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ModifiersDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
        "Lcom/squareup/ui/items/ModifiersDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    .line 60
    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/DetailSearchableListPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    move-object v1, p2

    .line 62
    iput-object v1, v0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    return-void
.end method

.method public static synthetic lambda$9Gn9HEBkIS8WMniX6yuUid8gBMg(Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method private onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 110
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/ModifiersDetailView;

    iput-boolean v2, p1, Lcom/squareup/ui/items/ModifiersDetailView;->useTemporaryOrdinal:Z

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->forceRefresh()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected getActionBarTitle()Ljava/lang/CharSequence;
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/itemsapplet/R$string;->items_applet_modifiers_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getButtonCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method getButtonText(I)Ljava/lang/String;
    .locals 1

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/registerlib/R$string;->create_modifier_set:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 102
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object v0
.end method

.method getItemTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic lambda$setOrdinal$0$ModifiersDetailScreen$Presenter(ILcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 118
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 119
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->updateOrSame(Ljava/lang/String;I)Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    move-result-object p1

    if-eq p1, p2, :cond_0

    .line 121
    iget-object p2, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/cogs/CogsTasks;->write()Lcom/squareup/cogs/WriteBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cogs/WriteBuilder;->update(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    :cond_0
    return-void
.end method

.method onButtonClicked(I)V
    .locals 1

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->startNewModifierSetFlow()V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_MODIFIER_SET:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 66
    invoke-super {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$ModifiersDetailScreen$Presenter$9Gn9HEBkIS8WMniX6yuUid8gBMg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$ModifiersDetailScreen$Presenter$9Gn9HEBkIS8WMniX6yuUid8gBMg;-><init>(Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method onRowClicked(I)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->startEditModifierSetFlow(Ljava/lang/String;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_MODIFIER_SET:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method positionChanged(II)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    .line 129
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->setOrdinal(Ljava/lang/String;I)V

    return-void
.end method

.method public rowsHaveThumbnails()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected setOrdinal(Ljava/lang/String;I)V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    const-class v1, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    invoke-static {v1, p1}, Lcom/squareup/cogs/CogsTasks;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/CatalogTask;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$ModifiersDetailScreen$Presenter$ngU1hPREklD_AWF6nb8Z1LB0_7Q;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/items/-$$Lambda$ModifiersDetailScreen$Presenter$ngU1hPREklD_AWF6nb8Z1LB0_7Q;-><init>(Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;I)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public usesDraggableListView()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
