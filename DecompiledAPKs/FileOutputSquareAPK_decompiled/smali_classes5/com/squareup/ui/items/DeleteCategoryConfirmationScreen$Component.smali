.class public interface abstract Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Component;
.super Ljava/lang/Object;
.source "DeleteCategoryConfirmationScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract analytics()Lcom/squareup/analytics/Analytics;
.end method

.method public abstract libraryDeleter()Lcom/squareup/ui/items/LibraryDeleter;
.end method
