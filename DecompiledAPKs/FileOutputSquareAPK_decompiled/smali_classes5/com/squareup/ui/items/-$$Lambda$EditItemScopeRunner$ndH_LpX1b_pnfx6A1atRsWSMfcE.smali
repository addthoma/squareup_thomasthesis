.class public final synthetic Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

.field private final synthetic f$1:Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;

.field private final synthetic f$2:Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;->f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;->f$1:Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;

    iput-object p3, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;->f$2:Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;->f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;->f$1:Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;

    iget-object v2, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;->f$2:Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;

    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->lambda$saveItemOptionsAndValues$6$EditItemScopeRunner(Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
