.class public Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;
.super Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;
.source "EditModifierSetRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter<",
        "Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;",
        "Lcom/squareup/api/items/ItemModifierOption$Builder;",
        ">;"
    }
.end annotation


# static fields
.field private static final DELETE_BUTTON:I = 0x2

.field private static final OPTION_ROW:I = 0x1

.field private static final STATIC_TOP_ROWS:I = 0x1

.field private static final STATIC_TOP_ROW_CONTENT:I


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final showDelete:Z

.field private final useMultiUnitUI:Z


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 273
    invoke-direct {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;-><init>()V

    .line 274
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    .line 275
    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 276
    iput-object p3, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->res:Lcom/squareup/util/Res;

    .line 277
    iput-object p4, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 278
    iput-object p5, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 280
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->isNewObject()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    iput-boolean p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->showDelete:Z

    .line 281
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->useMultiUnitEditingUI()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->useMultiUnitUI:Z

    .line 282
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->list:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 2

    .line 348
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticTopRowsCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 350
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->showDelete:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x2

    return p1

    :cond_1
    return v1
.end method

.method public getStableIdForIndex(I)J
    .locals 2

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/ItemModifierOption$Builder;

    iget-object p1, p1, Lcom/squareup/api/items/ItemModifierOption$Builder;->id:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getStaticBottomRowsCount()I
    .locals 1

    .line 359
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->showDelete:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public getStaticTopRowsCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;I)V
    .locals 5

    .line 316
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->getViewType()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p2, 0x2

    if-eq v0, p2, :cond_0

    goto :goto_1

    .line 340
    :cond_0
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->showDelete:Z

    if-eqz p2, :cond_6

    .line 341
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->bindDelete()V

    goto :goto_1

    .line 321
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getItemCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticBottomRowsCount()I

    move-result v2

    sub-int/2addr v0, v2

    const/4 v2, 0x0

    if-ne p2, v0, :cond_3

    .line 324
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getItemCount()I

    move-result p2

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticTopRowsCount()I

    move-result v0

    sub-int/2addr p2, v0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticBottomRowsCount()I

    move-result v0

    sub-int/2addr p2, v0

    if-nez p2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 325
    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->bindStaticNewOptionRow(Z)V

    goto :goto_1

    .line 328
    :cond_3
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 329
    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->bindModifierOption(Lcom/squareup/api/items/ItemModifierOption$Builder;I)V

    .line 330
    iget-wide v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->draggingRowId:J

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticTopRowsCount()I

    move-result v3

    sub-int/2addr p2, v3

    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStableIdForIndex(I)J

    move-result-wide v3

    cmp-long p2, v0, v3

    if-nez p2, :cond_4

    .line 331
    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$600(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->draggingRowView:Landroid/view/View;

    .line 332
    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$600(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 334
    :cond_4
    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$600(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 318
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->bindStaticTopRowContent()V

    :cond_6
    :goto_1
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 42
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;
    .locals 10

    .line 290
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    const/4 v2, 0x1

    if-eq p2, v2, :cond_1

    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    .line 306
    sget v2, Lcom/squareup/itemsapplet/R$layout;->items_applet_delete_button_row:I

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 309
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "viewType not supported"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 298
    :cond_1
    sget v2, Lcom/squareup/itemsapplet/R$layout;->modifier_option_row:I

    .line 299
    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/ModifierOptionRow;

    .line 300
    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierOptionRow;->getPriceView()Lcom/squareup/widgets/SelectableEditText;

    move-result-object v0

    .line 301
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;)V

    .line 302
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    sget-object v2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    goto :goto_0

    .line 294
    :cond_2
    sget v2, Lcom/squareup/itemsapplet/R$layout;->edit_modifier_set_static_content_top:I

    .line 295
    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :goto_0
    move-object v1, p1

    .line 311
    new-instance p1, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    iget-object v3, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v4, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    iget-object v5, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->res:Lcom/squareup/util/Res;

    iget-object v6, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v8, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-boolean v9, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->useMultiUnitUI:Z

    move-object v0, p1

    move v2, p2

    move-object v7, p0

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;ILcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;Lcom/squareup/text/Formatter;Z)V

    return-object p1
.end method
