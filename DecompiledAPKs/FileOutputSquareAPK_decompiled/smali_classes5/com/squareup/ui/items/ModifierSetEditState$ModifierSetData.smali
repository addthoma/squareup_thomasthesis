.class Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;
.super Ljava/lang/Object;
.source "ModifierSetEditState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ModifierSetEditState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ModifierSetData"
.end annotation


# instance fields
.field private final assignedItemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

.field private final modifierOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemModifierOption$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private modifierSetItemCount:I

.field private final removedOptionIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    new-instance v0, Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierList$Builder;-><init>()V

    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemModifierList$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    .line 205
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    .line 206
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removedOptionIds:Ljava/util/Set;

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->setMultipleSelection()V

    return-void
.end method

.method constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemModifierList;)V
    .locals 0

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierList;->newBuilder()Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    .line 212
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    .line 213
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    .line 214
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removedOptionIds:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;)Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 0

    .line 193
    iget-object p0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    return-object p0
.end method

.method private decrementCount()V
    .locals 1

    .line 317
    iget v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierSetItemCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierSetItemCount:I

    return-void
.end method

.method private incrementCount()V
    .locals 1

    .line 313
    iget v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierSetItemCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierSetItemCount:I

    return-void
.end method


# virtual methods
.method addAssignedItemId(Ljava/lang/String;)V
    .locals 1

    .line 287
    invoke-direct {p0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->incrementCount()V

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method addItemItemModifierListMemberships(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;)V"
        }
    .end annotation

    .line 278
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    .line 279
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getItemId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 281
    invoke-direct {p0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->incrementCount()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method addModifierOption(Lcom/squareup/api/items/ItemModifierOption$Builder;)V
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method addRemovedOptionId(Ljava/lang/String;)V
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removedOptionIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method cloneModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;
    .locals 4

    .line 218
    new-instance v0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-direct {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;-><init>()V

    .line 219
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierList;->newBuilder()Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    .line 220
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 221
    iget-object v3, v0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-virtual {v2}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/api/items/ItemModifierOption;->newBuilder()Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 223
    :cond_0
    iget-object v1, v0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    iget-object v2, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 224
    iget-object v1, v0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removedOptionIds:Ljava/util/Set;

    iget-object v2, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removedOptionIds:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 230
    :cond_0
    instance-of v1, p1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 231
    :cond_1
    check-cast p1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    .line 232
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object v1

    iget-object v3, p1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-virtual {v3}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/api/items/ItemModifierList;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    .line 233
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v3, p1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v1, v3, :cond_3

    return v2

    :cond_3
    const/4 v1, 0x0

    .line 234
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 235
    iget-object v3, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-virtual {v3}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v3

    iget-object v4, p1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-virtual {v4}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/api/items/ItemModifierOption;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    return v2

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 237
    :cond_5
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v2

    .line 238
    :cond_6
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removedOptionIds:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removedOptionIds:Ljava/util/Set;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    return v2

    .line 239
    :cond_7
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->isMultipleSelection()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->isMultipleSelection()Z

    move-result p1

    if-eq v1, p1, :cond_8

    return v2

    :cond_8
    return v0
.end method

.method getAssignedItemIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    return-object v0
.end method

.method getModifierOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemModifierOption$Builder;",
            ">;"
        }
    .end annotation

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    return-object v0
.end method

.method public getModifierSet()Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
    .locals 3

    .line 244
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    iget-object v1, v1, Lcom/squareup/api/items/ItemModifierList$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    .line 245
    invoke-virtual {v2}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object v2

    .line 244
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    return-object v0
.end method

.method getModifierSetItemCount()I
    .locals 1

    .line 301
    iget v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierSetItemCount:I

    return v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->name(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getRemovedOptionIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removedOptionIds:Ljava/util/Set;

    return-object v0
.end method

.method hasNoModifierOptions()Z
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method isMultipleSelection()Z
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->isMultipleSelection(Lcom/squareup/api/items/ItemModifierList$SelectionType;)Z

    move-result v0

    return v0
.end method

.method removeAssignedItem(Ljava/lang/String;)Z
    .locals 1

    .line 292
    invoke-direct {p0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->decrementCount()V

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->assignedItemIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method setMultipleSelection()V
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    sget-object v1, Lcom/squareup/api/items/ItemModifierList$SelectionType;->MULTIPLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    return-void
.end method

.method setName(Ljava/lang/String;)V
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->modifierListBuilder:Lcom/squareup/api/items/ItemModifierList$Builder;

    iput-object p1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->name:Ljava/lang/String;

    return-void
.end method
