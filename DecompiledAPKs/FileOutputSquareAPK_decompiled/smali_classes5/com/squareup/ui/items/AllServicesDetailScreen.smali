.class public final Lcom/squareup/ui/items/AllServicesDetailScreen;
.super Lcom/squareup/ui/items/InItemsAppletScope;
.source "AllServicesDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/AllServicesDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/AllServicesDetailScreen$Component;,
        Lcom/squareup/ui/items/AllServicesDetailScreen$Module;,
        Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/AllServicesDetailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/items/AllServicesDetailScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/ui/items/AllServicesDetailScreen;

    invoke-direct {v0}, Lcom/squareup/ui/items/AllServicesDetailScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/AllServicesDetailScreen;->INSTANCE:Lcom/squareup/ui/items/AllServicesDetailScreen;

    .line 137
    sget-object v0, Lcom/squareup/ui/items/AllServicesDetailScreen;->INSTANCE:Lcom/squareup/ui/items/AllServicesDetailScreen;

    .line 138
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/AllServicesDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/items/InItemsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 48
    const-class v0, Lcom/squareup/ui/items/ItemsAppletSection$AllServices;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 141
    sget v0, Lcom/squareup/itemsapplet/R$layout;->all_items_or_services_detail_view:I

    return v0
.end method
