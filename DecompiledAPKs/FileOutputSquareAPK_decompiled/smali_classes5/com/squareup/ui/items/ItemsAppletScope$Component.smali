.class public interface abstract Lcom/squareup/ui/items/ItemsAppletScope$Component;
.super Ljava/lang/Object;
.source "ItemsAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/items/unit/EditUnitModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract allItemsDetail()Lcom/squareup/ui/items/AllItemsDetailScreen$Component;
.end method

.method public abstract allServicesDetail()Lcom/squareup/ui/items/AllServicesDetailScreen$Component;
.end method

.method public abstract categoriesList()Lcom/squareup/ui/items/CategoriesListScreen$Component;
.end method

.method public abstract categoryDetail()Lcom/squareup/ui/items/CategoryDetailScreen$Component;
.end method

.method public abstract deleteCategoryDialog()Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Component;
.end method

.method public abstract editCategory()Lcom/squareup/ui/items/EditCategoryScope$Component;
.end method

.method public abstract editModifierSet()Lcom/squareup/ui/items/EditModifierSetScope$Component;
.end method

.method public abstract itemsAppletMaster()Lcom/squareup/ui/items/ItemsAppletMasterScreen$Component;
.end method

.method public abstract modifiersDetail()Lcom/squareup/ui/items/ModifiersDetailScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/items/ItemsAppletScopeRunner;
.end method
