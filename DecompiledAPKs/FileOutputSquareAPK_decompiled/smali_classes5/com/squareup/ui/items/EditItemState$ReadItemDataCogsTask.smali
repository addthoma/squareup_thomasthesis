.class Lcom/squareup/ui/items/EditItemState$ReadItemDataCogsTask;
.super Ljava/lang/Object;
.source "EditItemState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ReadItemDataCogsTask"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static readItemDataFromCogs(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)Lcom/squareup/ui/items/EditItemState$ItemData;
    .locals 5

    .line 671
    new-instance v0, Lcom/squareup/ui/items/EditItemState$ItemData;

    invoke-direct {v0, p2}, Lcom/squareup/ui/items/EditItemState$ItemData;-><init>(Lcom/squareup/api/items/Item$Type;)V

    .line 672
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p2

    invoke-interface {p0, p2}, Lcom/squareup/shared/catalog/Catalog$Local;->readItems(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p2

    .line 673
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 674
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-direct {v1, p2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;)V

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 675
    invoke-interface {p0, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemVariations(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 677
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    .line 679
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 680
    new-instance v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-direct {v4, v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    .line 682
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getOrdinal()I

    move-result v3

    if-eq v3, v2, :cond_0

    .line 683
    invoke-virtual {v4, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 685
    :cond_0
    iget-object v3, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 689
    :cond_1
    const-class v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    invoke-interface {p0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllCatalogConnectV2Objects(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 691
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 692
    iget-object v3, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 696
    :cond_2
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 697
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    new-instance v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 700
    :cond_3
    invoke-interface {p0}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllTaxes()Ljava/util/List;

    move-result-object p1

    .line 701
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    .line 702
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 703
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 704
    iget-object v2, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 708
    :cond_5
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasMenuCategory()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 709
    const-class p1, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 710
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getMenuCategoryId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    if-eqz p0, :cond_6

    .line 712
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->access$402(Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/lang/String;)Ljava/lang/String;

    .line 713
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/squareup/ui/items/EditItemState$ItemData;->access$502(Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/lang/String;)Ljava/lang/String;

    :cond_6
    return-object v0
.end method
