.class public Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "NewCategoryNameScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/NewCategoryNameScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/items/NewCategoryNameView;",
        ">;"
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private currentCategoryName:Ljava/lang/String;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final state:Lcom/squareup/ui/items/EditItemState;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/ui/items/EditItemState;Lflow/Flow;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/ui/items/EditItemState;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p2, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 61
    iput-object p3, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->flow:Lflow/Flow;

    .line 62
    iput-object p4, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public synthetic lambda$onLoad$0$NewCategoryNameScreen$Presenter()V
    .locals 0

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->saveCategory()V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->finish()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 66
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/edititem/R$string;->category_create:I

    .line 70
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$NewCategoryNameScreen$Presenter$RkvMTb4X6QQj1NEZkkZTB_1rvf0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$NewCategoryNameScreen$Presenter$RkvMTb4X6QQj1NEZkkZTB_1rvf0;-><init>(Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;)V

    .line 71
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 69
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public saveCategory()V
    .locals 4

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->currentCategoryName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 82
    :cond_0
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object v0

    .line 83
    new-instance v1, Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/MenuCategory$Builder;-><init>()V

    invoke-virtual {v1, v0}, Lcom/squareup/api/items/MenuCategory$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->currentCategoryName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/api/items/MenuCategory$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/items/MenuCategory$Builder;->build()Lcom/squareup/api/items/MenuCategory;

    move-result-object v1

    .line 84
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v2, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 85
    iget-object v2, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cogs/Cogs;

    .line 86
    invoke-static {}, Lcom/squareup/cogs/CogsTasks;->write()Lcom/squareup/cogs/WriteBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/squareup/cogs/WriteBuilder;->update(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object v1

    invoke-static {v2}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    .line 88
    iget-object v1, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->currentCategoryName:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->setItemCategory(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public updateCategoryName(Ljava/lang/String;)V
    .locals 1

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->currentCategoryName:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
