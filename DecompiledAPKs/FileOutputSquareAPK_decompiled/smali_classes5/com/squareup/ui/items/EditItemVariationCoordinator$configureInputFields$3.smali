.class public final Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditItemVariationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemVariationCoordinator;->configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $skuValidationRunnable:Lkotlin/jvm/functions/Function0;

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0;",
            ")V"
        }
    .end annotation

    .line 188
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->$view:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->$skuValidationRunnable:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    const-string v0, "editable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getEditItemVariationRunner$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getSkuEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;->variationSkuChanged(Ljava/lang/String;)V

    .line 193
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->$view:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->$skuValidationRunnable:Lkotlin/jvm/functions/Function0;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/ui/items/EditItemVariationCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->$view:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;->$skuValidationRunnable:Lkotlin/jvm/functions/Function0;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/squareup/ui/items/EditItemVariationCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object v0, v1

    :cond_1
    check-cast v0, Ljava/lang/Runnable;

    const-wide/16 v1, 0xc8

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
