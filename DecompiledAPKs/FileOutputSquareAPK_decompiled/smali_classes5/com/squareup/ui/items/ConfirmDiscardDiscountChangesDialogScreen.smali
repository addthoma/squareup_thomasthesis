.class public Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;
.super Lcom/squareup/ui/items/InEditDiscountScope;
.source "ConfirmDiscardDiscountChangesDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;

    invoke-direct {v0}, Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;-><init>()V

    .line 26
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/ui/items/InEditDiscountScope;-><init>()V

    return-void
.end method
