.class Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditModifierSetRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->bindStaticNewOptionRow(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

.field final synthetic val$nameView:Landroid/widget/EditText;

.field final synthetic val$priceView:Lcom/squareup/widgets/SelectableEditText;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Lcom/squareup/widgets/SelectableEditText;Landroid/widget/EditText;)V
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->val$priceView:Lcom/squareup/widgets/SelectableEditText;

    iput-object p3, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->val$nameView:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 185
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$100(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 187
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$200(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->val$priceView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$000(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->val$nameView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->createNewModifierOption(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    .line 189
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getItemCount()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {v1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticBottomRowsCount()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->notifyItemChanged(I)V

    .line 190
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    .line 191
    invoke-static {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getItemCount()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {v1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticBottomRowsCount()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x1

    add-int/2addr v0, v1

    .line 190
    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->notifyItemInserted(I)V

    .line 193
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->val$priceView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->hasFocus()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$402(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Z)Z

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->val$nameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$502(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Z)Z

    .line 195
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {p1, v1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$102(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Z)Z

    return-void
.end method
