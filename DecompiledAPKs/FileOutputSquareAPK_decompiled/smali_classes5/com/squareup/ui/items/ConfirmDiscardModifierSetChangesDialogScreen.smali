.class public Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;
.super Lcom/squareup/ui/items/InEditModifierSetScope;
.source "ConfirmDiscardModifierSetChangesDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$ConfirmDiscardModifierSetChangesDialogScreen$ZiEzWTsTTCTmRz3cxLt3ROThFzg;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$ConfirmDiscardModifierSetChangesDialogScreen$ZiEzWTsTTCTmRz3cxLt3ROThFzg;

    .line 38
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditModifierSetScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;
    .locals 1

    .line 39
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 40
    new-instance v0, Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 34
    iget-object p2, p0, Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;->modifierSetId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
