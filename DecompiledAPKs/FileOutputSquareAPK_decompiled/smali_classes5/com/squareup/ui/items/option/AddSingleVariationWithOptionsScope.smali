.class public final Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "AddSingleVariationWithOptionsScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$ParentComponent;,
        Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$CREATOR;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddSingleVariationWithOptionsScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddSingleVariationWithOptionsScope.kt\ncom/squareup/ui/items/option/AddSingleVariationWithOptionsScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,45:1\n35#2:46\n*E\n*S KotlinDebug\n*F\n+ 1 AddSingleVariationWithOptionsScope.kt\ncom/squareup/ui/items/option/AddSingleVariationWithOptionsScope\n*L\n20#1:46\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00102\u00020\u0001:\u0002\u0010\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;",
        "Lcom/squareup/ui/items/InEditItemScope;",
        "parent",
        "Lcom/squareup/ui/items/EditItemScope;",
        "(Lcom/squareup/ui/items/EditItemScope;)V",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "CREATOR",
        "ParentComponent",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$CREATOR;


# instance fields
.field private final parent:Lcom/squareup/ui/items/EditItemScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$CREATOR;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$CREATOR;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;->CREATOR:Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$CREATOR;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    iput-object p1, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;->parent:Lcom/squareup/ui/items/EditItemScope;

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    const-class v0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$ParentComponent;

    .line 21
    invoke-interface {v0}, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$ParentComponent;->addSingleVariationWithOptionsWorkflowRunner()Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;

    move-result-object v0

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p1

    const-string v1, "it"

    .line 23
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string v0, "super.buildScope(parentS\u2026er.registerServices(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;->parent:Lcom/squareup/ui/items/EditItemScope;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
