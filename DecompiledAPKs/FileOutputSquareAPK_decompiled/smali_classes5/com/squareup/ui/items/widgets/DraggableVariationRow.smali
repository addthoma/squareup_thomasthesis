.class public Lcom/squareup/ui/items/widgets/DraggableVariationRow;
.super Lcom/squareup/widgets/ResponsiveLinearLayout;
.source "DraggableVariationRow.java"


# instance fields
.field protected detailsContainer:Landroid/view/ViewGroup;

.field protected dragHandle:Lcom/squareup/glyph/SquareGlyphView;

.field protected name:Lcom/squareup/marketfont/MarketTextView;

.field protected topDivider:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public configureTopDivider(Z)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->topDivider:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public getDetailsContainer()Landroid/view/ViewGroup;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->detailsContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public hideDragHandle()V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 30
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveLinearLayout;->onFinishInflate()V

    .line 32
    sget v0, Lcom/squareup/edititem/R$id;->draggable_variation_row_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->name:Lcom/squareup/marketfont/MarketTextView;

    .line 33
    sget v0, Lcom/squareup/edititem/R$id;->draggable_variation_row_details_container:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->detailsContainer:Landroid/view/ViewGroup;

    .line 34
    sget v0, Lcom/squareup/edititem/R$id;->draggable_variation_row_top_divider:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->topDivider:Landroid/view/View;

    .line 35
    sget v0, Lcom/squareup/edititem/R$id;->draggable_item_variation_row_drag_handle:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->name:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
