.class Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditItemCategorySelectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter$1;->this$1:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter$1;->this$1:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->this$0:Lcom/squareup/ui/items/EditItemCategorySelectionView;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->onNewCategoryClicked()V

    return-void
.end method
