.class public abstract Lcom/squareup/ui/items/EditItemScope$VariationRunnerModule;
.super Ljava/lang/Object;
.source "EditItemScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "VariationRunnerModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideEditItemVariationRunner(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEditServiceVariationRunner(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
