.class Lcom/squareup/ui/items/CategoriesListView$CategoriesListAdapter;
.super Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;
.source "CategoriesListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/CategoriesListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CategoriesListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListView<",
        "Lcom/squareup/ui/items/CategoriesListView;",
        ">.DetailSearchable",
        "ListAdapter;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/CategoriesListView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/CategoriesListView;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/items/CategoriesListView$CategoriesListAdapter;->this$0:Lcom/squareup/ui/items/CategoriesListView;

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;-><init>(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method


# virtual methods
.method protected buildAndBindItemRow(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 0

    if-nez p1, :cond_0

    .line 59
    sget p1, Lcom/squareup/itemsapplet/R$layout;->items_applet_category_list_row:I

    .line 60
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    :cond_0
    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 62
    iget-object p2, p0, Lcom/squareup/ui/items/CategoriesListView$CategoriesListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    return-object p1
.end method
