.class public final Lcom/squareup/ui/items/EditModifierSetScope;
.super Lcom/squareup/ui/items/InItemsAppletScope;
.source "EditModifierSetScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditModifierSetScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditModifierSetScope$Component;,
        Lcom/squareup/ui/items/EditModifierSetScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditModifierSetScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final NEW_MODIFIER_SET:Ljava/lang/String;


# instance fields
.field final modifierSetId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetScope$M2e8jJiYNqcR2wqIIWZ7ZeY9_Io;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditModifierSetScope$M2e8jJiYNqcR2wqIIWZ7ZeY9_Io;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditModifierSetScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/items/InItemsAppletScope;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScope;->modifierSetId:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditModifierSetScope;
    .locals 1

    .line 64
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 65
    new-instance v0, Lcom/squareup/ui/items/EditModifierSetScope;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditModifierSetScope;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 33
    iget-object p2, p0, Lcom/squareup/ui/items/EditModifierSetScope;->modifierSetId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/ui/items/EditModifierSetScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditModifierSetScope$Component;

    .line 29
    invoke-interface {v0}, Lcom/squareup/ui/items/EditModifierSetScope$Component;->scopeRunner()Lcom/squareup/ui/items/EditModifierSetScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
