.class Lcom/squareup/ui/items/EditCategoryScreen$Presenter;
.super Lcom/squareup/ui/items/BaseEditObjectViewPresenter;
.source "EditCategoryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/BaseEditObjectViewPresenter<",
        "Lcom/squareup/ui/items/EditCategoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

.field private categoryNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private initiallyEmptyCategory:Z

.field private itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private final libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

.field private screen:Lcom/squareup/ui/items/EditCategoryScreen;

.field private final state:Lcom/squareup/ui/items/EditCategoryState;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lflow/Flow;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/ui/items/EditCategoryState;Lcom/squareup/ui/items/EditCategoryScopeRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/ui/items/EditCategoryState;",
            "Lcom/squareup/ui/items/EditCategoryScopeRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 92
    invoke-direct {p0, p6, p5, p3}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V

    .line 93
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    .line 94
    iput-object p3, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 95
    iput-object p4, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

    .line 96
    iput-object p6, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    .line 97
    iput-object p2, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 98
    iput-object p7, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->flow:Lflow/Flow;

    .line 99
    iput-object p8, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 100
    iput-object p9, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    .line 101
    iput-object p10, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    return-void
.end method

.method private entryInCurrentCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z
    .locals 1

    .line 277
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getCategoryId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemOriginallyInCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->getRemovedItems()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->getAddedItems()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private finish()V
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method private isDefaultAbbreviation(Ljava/lang/String;)Z
    .locals 1

    .line 261
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private itemOriginallyInCategory(Ljava/lang/String;)Z
    .locals 1

    .line 285
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->screen:Lcom/squareup/ui/items/EditCategoryScreen;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryScreen;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private showItemsLoaded()V
    .locals 2

    .line 311
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 314
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditCategoryView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditCategoryView;->updateCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    .line 315
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->updatePrimaryButtonState()V

    return-void
.end method


# virtual methods
.method deleteClicked()V
    .locals 4

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 320
    sget-object v0, Lcom/squareup/catalog/event/CatalogFeature;->CATEGORY_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->screen:Lcom/squareup/ui/items/EditCategoryScreen;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryScreen;->categoryId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->screen:Lcom/squareup/ui/items/EditCategoryScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditCategoryScreen;->categoryId:Ljava/lang/String;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$Presenter$lT3hMs-rd9usBPVenDw7RthZ6fQ;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$Presenter$lT3hMs-rd9usBPVenDw7RthZ6fQ;-><init>(Lcom/squareup/ui/items/EditCategoryScreen$Presenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/LibraryDeleter;->deleteCategoryById(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void

    .line 327
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->initiallyEmptyCategory:Z

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->screen:Lcom/squareup/ui/items/EditCategoryScreen;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryScreen;->categoryId:Ljava/lang/String;

    sget v3, Lcom/squareup/editbaseobject/R$string;->item_editing_category_delete_confirmation_maybe_empty:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->screen:Lcom/squareup/ui/items/EditCategoryScreen;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryScreen;->categoryId:Ljava/lang/String;

    sget v3, Lcom/squareup/editbaseobject/R$string;->item_editing_category_delete_confirmation_with_items:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method editTileClicked()V
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->onClickCategoryTile()V

    return-void
.end method

.method getAbbreviation()Ljava/lang/String;
    .locals 2

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    .line 212
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCurrentName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 211
    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method getAbbreviationOrDefault()Ljava/lang/String;
    .locals 2

    .line 224
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCurrentName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCategoryName()Ljava/lang/String;
    .locals 1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCurrentName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCategoryNameForId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->categoryNameMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method getCategoryNameOrDefault()Ljava/lang/String;
    .locals 3

    .line 228
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    .line 229
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isNewObject()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->new_category:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 228
    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentName()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method getLabelColorOrDefault()Ljava/lang/String;
    .locals 3

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    .line 217
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->getColor()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    .line 218
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Colors;->toHex(I)Ljava/lang/String;

    move-result-object v0

    .line 219
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    .line 220
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-static {v1}, Lcom/squareup/util/Colors;->toHex(I)Ljava/lang/String;

    move-result-object v1

    .line 219
    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNewObject()Z
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->screen:Lcom/squareup/ui/items/EditCategoryScreen;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryScreen;->categoryId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isTextTileMode()Z
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v0

    return v0
.end method

.method itemInCurrentCategory(I)Z
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 272
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    .line 273
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->entryInCurrentCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p1

    return p1
.end method

.method itemSelected(I)V
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 290
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    .line 291
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    .line 292
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->entryInCurrentCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryState;->getAddedItems()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 297
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getCategoryId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemOriginallyInCategory(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 298
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryState;->getRemovedItems()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryState;->getRemovedItems()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 304
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getCategoryId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemOriginallyInCategory(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 305
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryState;->getAddedItems()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$deleteClicked$3$EditCategoryScreen$Presenter()V
    .locals 2

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/items/CategoriesListScreen;->INSTANCE:Lcom/squareup/ui/items/CategoriesListScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$0$EditCategoryScreen$Presenter(Lcom/squareup/ui/items/EditCategoryState;)V
    .locals 1

    .line 114
    iget-object v0, p1, Lcom/squareup/ui/items/EditCategoryState;->categoryDataFromLibrary:Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->libraryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 115
    iget-object v0, p1, Lcom/squareup/ui/items/EditCategoryState;->categoryDataFromLibrary:Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->categoryNameMap:Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->categoryNameMap:Ljava/util/Map;

    .line 116
    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryState;->categoryDataFromLibrary:Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;

    iget-boolean p1, p1, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->initiallyEmptyCategory:Z

    iput-boolean p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->initiallyEmptyCategory:Z

    .line 118
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->showItemsLoaded()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$EditCategoryScreen$Presenter()Lrx/Subscription;
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$Presenter$al-d7J3P_2qBs3Z4yUbvGvZUHlo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$Presenter$al-d7J3P_2qBs3Z4yUbvGvZUHlo;-><init>(Lcom/squareup/ui/items/EditCategoryScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$saveCategoryAndAssignments$2$EditCategoryScreen$Presenter(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 6

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryState;->getCategory()Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object v1

    .line 163
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v2

    .line 165
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isNewObject()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 166
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 168
    :cond_0
    sget-object v3, Lcom/squareup/catalog/event/CatalogFeature;->CATEGORY_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v4, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v3, v4, v2}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 169
    iget-object v3, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditCategoryState;->hasCategoryChanged()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 170
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_1
    :goto_0
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    .line 176
    new-instance v2, Ljava/util/HashSet;

    iget-object v3, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditCategoryState;->getAddedItems()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 177
    iget-object v3, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditCategoryState;->getRemovedItems()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 178
    invoke-interface {p1, v2}, Lcom/squareup/shared/catalog/Catalog$Local;->readItems(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v2

    .line 179
    iget-object v3, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditCategoryState;->getAddedItems()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 180
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 181
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/squareup/api/items/Item;

    .line 182
    invoke-virtual {v5}, Lcom/squareup/api/items/Item;->newBuilder()Lcom/squareup/api/items/Item$Builder;

    move-result-object v5

    .line 183
    invoke-virtual {v5, v1}, Lcom/squareup/api/items/Item$Builder;->menu_category(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Item$Builder;

    move-result-object v5

    .line 184
    invoke-virtual {v5}, Lcom/squareup/api/items/Item$Builder;->build()Lcom/squareup/api/items/Item;

    move-result-object v5

    .line 185
    invoke-virtual {v4, v5}, Lcom/squareup/shared/catalog/models/CatalogItem;->copy(Lcom/squareup/api/items/Item;)Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v4

    .line 186
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 189
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryState;->getRemovedItems()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 190
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 191
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v5

    check-cast v5, Lcom/squareup/api/items/Item;

    .line 192
    invoke-virtual {v5}, Lcom/squareup/api/items/Item;->newBuilder()Lcom/squareup/api/items/Item$Builder;

    move-result-object v5

    .line 193
    invoke-virtual {v5, v4}, Lcom/squareup/api/items/Item$Builder;->menu_category(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Item$Builder;

    move-result-object v4

    .line 194
    invoke-virtual {v4}, Lcom/squareup/api/items/Item$Builder;->build()Lcom/squareup/api/items/Item;

    move-result-object v4

    .line 195
    invoke-virtual {v3, v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->copy(Lcom/squareup/api/items/Item;)Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v3

    .line 196
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 199
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v4
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 105
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditCategoryScreen;

    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->screen:Lcom/squareup/ui/items/EditCategoryScreen;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 109
    invoke-super {p0, p1}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditCategoryView;

    .line 112
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$Presenter$FSfLlybvzknUQ2_9oErTNy1w_hQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$Presenter$FSfLlybvzknUQ2_9oErTNy1w_hQ;-><init>(Lcom/squareup/ui/items/EditCategoryScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 121
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isNewObject()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/itemsapplet/R$string;->items_applet_create_category:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/itemsapplet/R$string;->items_applet_edit_category:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 126
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isNewObject()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->useMultiUnitEditingUI()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/items/EditCategoryView;->initializeAdapter(ZZ)V

    .line 127
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->updatePrimaryButtonState()V

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->secondary_button_save:I

    .line 130
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$1VnOUTbJ94He1SQvzH0_PX1Eduk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$1VnOUTbJ94He1SQvzH0_PX1Eduk;-><init>(Lcom/squareup/ui/items/EditCategoryScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$JWnGE1YT02U146VPEp6u13KqUhg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$JWnGE1YT02U146VPEp6u13KqUhg;-><init>(Lcom/squareup/ui/items/EditCategoryScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz p1, :cond_1

    .line 135
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->showItemsLoaded()V

    :cond_1
    return-void
.end method

.method saveCategoryAndAssignments()V
    .locals 3

    .line 148
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/registerlib/R$string;->category_name_required_warning_title:I

    sget v2, Lcom/squareup/registerlib/R$string;->category_name_required_warning_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 156
    :cond_0
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->CATEGORY:Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v0}, Lsquareup/items/merchant/CatalogObjectType;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->logEditCatalogObjectEvent(Ljava/lang/String;Z)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    .line 159
    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$Presenter$enlSi08W3-elUWcIbAVLHiXfozI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$Presenter$enlSi08W3-elUWcIbAVLHiXfozI;-><init>(Lcom/squareup/ui/items/EditCategoryScreen$Presenter;)V

    .line 201
    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v2

    .line 159
    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    .line 203
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->finish()V

    return-void
.end method

.method showConfirmDiscardDialogOrFinish()V
    .locals 3

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->hasEditCategoryStateChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->screen:Lcom/squareup/ui/items/EditCategoryScreen;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryScreen;->categoryId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 346
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->finish()V

    :goto_0
    return-void
.end method

.method updateAbbreviation(Ljava/lang/String;)V
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditCategoryState;->setAbbreviation(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method updateCategoryName(Ljava/lang/String;)V
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->state:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditCategoryState;->setName(Ljava/lang/String;)V

    .line 242
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->updatePrimaryButtonState()V

    :cond_0
    return-void
.end method

.method updateCategoryNameAndAbbreviation(Ljava/lang/String;)V
    .locals 1

    .line 233
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isDefaultAbbreviation(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-static {p1}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->updateAbbreviation(Ljava/lang/String;)V

    .line 236
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->updateCategoryName(Ljava/lang/String;)V

    return-void
.end method
