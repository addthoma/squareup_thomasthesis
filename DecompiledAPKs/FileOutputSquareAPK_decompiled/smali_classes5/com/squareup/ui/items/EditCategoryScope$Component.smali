.class public interface abstract Lcom/squareup/ui/items/EditCategoryScope$Component;
.super Ljava/lang/Object;
.source "EditCategoryScope.java"


# annotations
.annotation runtime Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/catalog/online/CatalogOnlineModule;,
        Lcom/squareup/ui/items/EditCategoryScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract editCategory()Lcom/squareup/ui/items/EditCategoryScreen$Component;
.end method

.method public abstract editCategoryLabel()Lcom/squareup/ui/items/EditCategoryLabelScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/items/EditCategoryScopeRunner;
.end method
