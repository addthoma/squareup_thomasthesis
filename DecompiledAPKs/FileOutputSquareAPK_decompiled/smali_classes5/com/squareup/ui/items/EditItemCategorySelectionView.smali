.class public Lcom/squareup/ui/items/EditItemCategorySelectionView;
.super Lcom/squareup/widgets/PairLayout;
.source "EditItemCategorySelectionView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;
    }
.end annotation


# static fields
.field private static final CATEGORY_ROW:I = 0x0

.field private static final EMPTY_VIEW_ROW:I = 0x2

.field private static final NEW_CATEGORY_ROW:I = 0x1


# instance fields
.field private adapter:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

.field private categorySelectionListView:Lcom/squareup/marin/widgets/HorizontalRuleListView;

.field presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Component;->inject(Lcom/squareup/ui/items/EditItemCategorySelectionView;)V

    return-void
.end method


# virtual methods
.method public getSelectedCategoryIndex()I
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->getSelectedCategoryIndex()I

    move-result v0

    return v0
.end method

.method public synthetic lambda$onFinishInflate$0$EditItemCategorySelectionView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->adapter:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->getItemViewType(I)I

    move-result p1

    if-nez p1, :cond_0

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    add-int/lit8 p3, p3, -0x3

    invoke-virtual {p1, p3}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->onCategoryIndexClicked(I)V

    :cond_0
    return-void
.end method

.method public notifyAdapter()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->adapter:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->screenDismissed()V

    .line 64
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 46
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 47
    new-instance v0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;-><init>(Lcom/squareup/ui/items/EditItemCategorySelectionView;Lcom/squareup/ui/items/EditItemCategorySelectionView$1;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->adapter:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

    .line 49
    sget v0, Lcom/squareup/edititem/R$id;->item_category_selection_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/HorizontalRuleListView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->categorySelectionListView:Lcom/squareup/marin/widgets/HorizontalRuleListView;

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->categorySelectionListView:Lcom/squareup/marin/widgets/HorizontalRuleListView;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionView$IRgbRur_qiiRSuQYpETqR1GzaIc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionView$IRgbRur_qiiRSuQYpETqR1GzaIc;-><init>(Lcom/squareup/ui/items/EditItemCategorySelectionView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/HorizontalRuleListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setCategories(Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            ">;I)V"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->adapter:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

    iput-object p1, v0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->categories:Ljava/util/List;

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->setSelectedCategoryIndex(I)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->categorySelectionListView:Lcom/squareup/marin/widgets/HorizontalRuleListView;

    iget-object p2, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->adapter:Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/HorizontalRuleListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
