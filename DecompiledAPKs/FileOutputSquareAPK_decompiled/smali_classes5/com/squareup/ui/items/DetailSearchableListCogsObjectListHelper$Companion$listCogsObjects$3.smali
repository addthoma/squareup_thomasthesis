.class final Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;
.super Ljava/lang/Object;
.source "DetailSearchableListCogsObjectListHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->listCogsObjects(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Single;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $analytics:Lcom/squareup/analytics/Analytics;

.field final synthetic $cogs:Lcom/squareup/cogs/Cogs;

.field final synthetic $discountCursorDecoration:Lkotlin/jvm/functions/Function2;

.field final synthetic $itemTypes:Ljava/util/List;

.field final synthetic $objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field final synthetic $searchTerm:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/cogs/Cogs;Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;Lcom/squareup/analytics/Analytics;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$cogs:Lcom/squareup/cogs/Cogs;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$searchTerm:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$itemTypes:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$analytics:Lcom/squareup/analytics/Analytics;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$discountCursorDecoration:Lkotlin/jvm/functions/Function2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Unit;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$cogs:Lcom/squareup/cogs/Cogs;

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;)V

    check-cast v0, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {p1, v0}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    .line 63
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs.asSingle { local ->\u2026        )\n              }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->apply(Lkotlin/Unit;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
