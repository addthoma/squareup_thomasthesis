.class public Lcom/squareup/ui/items/EditItemMainViewStaticTopView;
.super Landroid/widget/LinearLayout;
.source "EditItemMainViewStaticTopView.java"


# instance fields
.field private banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

.field private editableDetailsSection:Lcom/squareup/ui/items/EditItemEditDetailsView;

.field private manageOptionButton:Landroid/widget/Button;

.field private manageOptionHelpText:Lcom/squareup/widgets/MessageView;

.field private optionSection:Landroid/view/View;

.field private optionsContainer:Landroid/widget/LinearLayout;

.field presenter:Lcom/squareup/ui/items/EditItemMainPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private readOnlyDetailsSection:Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditItemMainScreen$Component;->inject(Lcom/squareup/ui/items/EditItemMainViewStaticTopView;)V

    return-void
.end method

.method private setShowTextTile(Z)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->editableDetailsSection:Lcom/squareup/ui/items/EditItemEditDetailsView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemEditDetailsView;->setShowTextTile(Z)V

    return-void
.end method

.method private useEditableItemDetailsOnTop(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Ljava/lang/String;ZZ)V
    .locals 8

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->editableDetailsSection:Lcom/squareup/ui/items/EditItemEditDetailsView;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/ui/items/EditItemEditDetailsView;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Ljava/lang/String;ZZ)V

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->editableDetailsSection:Lcom/squareup/ui/items/EditItemEditDetailsView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemEditDetailsView;->setVisibility(I)V

    return-void
.end method

.method private useReadOnlyItemDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->readOnlyDetailsSection:Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;

    invoke-virtual {v0, p4, p1, p2, p3}, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->readOnlyDetailsSection:Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$showContent$0$EditItemMainViewStaticTopView(Landroid/view/View;)V
    .locals 0

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->manageItemOptionClicked()V

    return-void
.end method

.method public synthetic lambda$showContent$1$EditItemMainViewStaticTopView()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->onLearnMoreOnItemOptionsClicked()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 42
    sget v0, Lcom/squareup/edititem/R$id;->banner:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/AppliedLocationsBanner;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

    .line 44
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_edit_details_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemEditDetailsView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->editableDetailsSection:Lcom/squareup/ui/items/EditItemEditDetailsView;

    .line 45
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_read_only_details_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->readOnlyDetailsSection:Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;

    .line 46
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_option_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->optionSection:Landroid/view/View;

    .line 47
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_options_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->optionsContainer:Landroid/widget/LinearLayout;

    .line 48
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_manage_option_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->manageOptionButton:Landroid/widget/Button;

    .line 49
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_manage_option_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->manageOptionHelpText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method showContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 11

    .line 53
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showLocationBanner:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/AppliedLocationsBanner;->setVisibility(I)V

    .line 57
    :cond_0
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showTextTile:Z

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->setShowTextTile(Z)V

    .line 59
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showOptionSection:Z

    if-eqz v0, :cond_3

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->optionSection:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->optionsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 63
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemOptionsAndValuesList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 64
    new-instance v2, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->optionsContainer:Landroid/widget/LinearLayout;

    .line 65
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;-><init>(Landroid/content/Context;)V

    .line 66
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->setOptionName(Ljava/lang/String;)V

    .line 67
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->setOptionValues(Ljava/lang/String;)V

    .line 68
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->optionsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 74
    :cond_1
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemOptionsAndValuesList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 75
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->manageOptionButton:Landroid/widget/Button;

    if-eqz v0, :cond_2

    sget v0, Lcom/squareup/edititem/R$string;->edit_item_add_item_options_button_label:I

    goto :goto_1

    :cond_2
    sget v0, Lcom/squareup/edititem/R$string;->edit_item_edit_item_options_button_label:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->manageOptionButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticTopView$pFLAObU5P9wpu41t4zRbleH-HtY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticTopView$pFLAObU5P9wpu41t4zRbleH-HtY;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticTopView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->manageOptionHelpText:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/edititem/R$string;->edit_item_item_option_help_text:I

    const-string v3, "learn_more"

    .line 81
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/items/editoption/R$string;->item_options_learn_more_url:I

    .line 82
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->learn_more:I

    .line 83
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$color;->noho_text_help_link:I

    .line 84
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticTopView$qXsM8JnGi6To0KAyIBbZL5n2JyQ;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewStaticTopView$qXsM8JnGi6To0KAyIBbZL5n2JyQ;-><init>(Lcom/squareup/ui/items/EditItemMainViewStaticTopView;)V

    .line 85
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->onClick(Ljava/lang/Runnable;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->optionSection:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 91
    :goto_2
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemType:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    if-eq v0, v1, :cond_4

    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    if-nez v0, :cond_4

    .line 92
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->categoryName:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->description:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->detailsHeader:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->useReadOnlyItemDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 95
    :cond_4
    iget-object v4, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemName:Ljava/lang/String;

    iget-object v5, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->labelColor:Ljava/lang/String;

    iget-object v6, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->labelText:Ljava/lang/String;

    iget-object v7, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    iget-object v8, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->categoryName:Ljava/lang/String;

    iget-boolean v9, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->shouldShowInlineVariation:Z

    iget-boolean v10, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    move-object v3, p0

    invoke-direct/range {v3 .. v10}, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->useEditableItemDetailsOnTop(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Ljava/lang/String;ZZ)V

    :goto_3
    return-void
.end method
