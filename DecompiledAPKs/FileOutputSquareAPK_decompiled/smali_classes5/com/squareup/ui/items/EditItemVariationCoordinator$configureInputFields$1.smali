.class public final Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "EditItemVariationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemVariationCoordinator;->configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditItemVariationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditItemVariationCoordinator.kt\ncom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1\n*L\n1#1,261:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doAfterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 146
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 2

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 149
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getActionBar$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$shouldEnabledPrimaryButton(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getEditItemVariationRunner$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;->variationNameChanged(Ljava/lang/String;)V

    return-void

    .line 149
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
