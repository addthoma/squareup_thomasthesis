.class final enum Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;
.super Ljava/lang/Enum;
.source "EditInventoryStateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditInventoryStateController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "InventoryStockCountsLoadStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

.field public static final enum DISALLOWED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

.field public static final enum ERROR:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

.field public static final enum LOADING:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

.field public static final enum NOT_REQUESTED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

.field public static final enum SUCCESS:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 264
    new-instance v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    const/4 v1, 0x0

    const-string v2, "NOT_REQUESTED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->NOT_REQUESTED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    new-instance v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    const/4 v2, 0x1

    const-string v3, "LOADING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->LOADING:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    new-instance v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    const/4 v3, 0x2

    const-string v4, "SUCCESS"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->SUCCESS:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    new-instance v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    const/4 v4, 0x3

    const-string v5, "ERROR"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->ERROR:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    new-instance v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    const/4 v5, 0x4

    const-string v6, "DISALLOWED"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->DISALLOWED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    .line 263
    sget-object v6, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->NOT_REQUESTED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->LOADING:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->SUCCESS:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->ERROR:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->DISALLOWED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->$VALUES:[Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 263
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;
    .locals 1

    .line 263
    const-class v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;
    .locals 1

    .line 263
    sget-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->$VALUES:[Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    return-object v0
.end method
