.class public final enum Lcom/squareup/ui/items/ItemEditingStringIds;
.super Ljava/lang/Enum;
.source "ItemEditingStringIds.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/ItemEditingStringIds;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/ItemEditingStringIds;

.field public static final enum APPOINTMENTS_SERVICE:Lcom/squareup/ui/items/ItemEditingStringIds;

.field public static final enum CATEGORY:Lcom/squareup/ui/items/ItemEditingStringIds;

.field public static final enum DISCOUNT:Lcom/squareup/ui/items/ItemEditingStringIds;

.field public static final enum ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

.field public static final enum MODIFIER_SET:Lcom/squareup/ui/items/ItemEditingStringIds;

.field public static final enum RESTAURANT_ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

.field public static final enum RETAIL_ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

.field public static final enum TAX:Lcom/squareup/ui/items/ItemEditingStringIds;


# instance fields
.field final bannerErrorResId:I

.field final bannerInactiveResId:I

.field final bannerNotSharedResId:I

.field final bannerOneOtherResId:I

.field final bannerSharedResId:I

.field final deleteMultiunitResId:I

.field final deleteResId:I

.field final readOnlyDetailsId:I

.field final screenTitleResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 32

    .line 15
    new-instance v12, Lcom/squareup/ui/items/ItemEditingStringIds;

    sget v3, Lcom/squareup/editbaseobject/R$string;->item_editing_changes_not_available_service:I

    sget v4, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_service:I

    sget v5, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_not_shared_service:I

    sget v6, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_one_other_location_service:I

    sget v7, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_inactive_service:I

    sget v8, Lcom/squareup/editbaseobject/R$string;->edit_service:I

    sget v9, Lcom/squareup/editbaseobject/R$string;->item_editing_read_only_details_service:I

    sget v10, Lcom/squareup/editbaseobject/R$string;->item_editing_delete_service:I

    sget v11, Lcom/squareup/editbaseobject/R$string;->item_editing_delete_from_location_service:I

    const-string v1, "APPOINTMENTS_SERVICE"

    const/4 v2, 0x0

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIIIIIII)V

    sput-object v12, Lcom/squareup/ui/items/ItemEditingStringIds;->APPOINTMENTS_SERVICE:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 22
    new-instance v0, Lcom/squareup/ui/items/ItemEditingStringIds;

    sget v16, Lcom/squareup/editbaseobject/R$string;->item_editing_changes_not_available_category:I

    sget v17, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_category:I

    sget v18, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_not_shared_category:I

    sget v19, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_one_other_location_category:I

    sget v20, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_inactive_category:I

    const-string v14, "CATEGORY"

    const/4 v15, 0x1

    move-object v13, v0

    invoke-direct/range {v13 .. v20}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->CATEGORY:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 27
    new-instance v0, Lcom/squareup/ui/items/ItemEditingStringIds;

    sget v4, Lcom/squareup/editbaseobject/R$string;->item_editing_changes_not_available_discount:I

    sget v5, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_discount:I

    sget v6, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_not_shared_discount:I

    sget v7, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_one_other_location_discount:I

    sget v8, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_inactive_discount:I

    const-string v2, "DISCOUNT"

    const/4 v3, 0x2

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->DISCOUNT:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 32
    new-instance v0, Lcom/squareup/ui/items/ItemEditingStringIds;

    sget v12, Lcom/squareup/editbaseobject/R$string;->item_editing_changes_not_available_product:I

    sget v13, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_product:I

    sget v14, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_not_shared_product:I

    sget v15, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_one_other_location_product:I

    sget v16, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_inactive_product:I

    sget v17, Lcom/squareup/editbaseobject/R$string;->edit_retail_item:I

    sget v18, Lcom/squareup/editbaseobject/R$string;->item_editing_read_only_details_product:I

    sget v19, Lcom/squareup/editbaseobject/R$string;->item_editing_delete_product:I

    sget v20, Lcom/squareup/editbaseobject/R$string;->item_editing_delete_from_location_product:I

    const-string v10, "RETAIL_ITEM"

    const/4 v11, 0x3

    move-object v9, v0

    invoke-direct/range {v9 .. v20}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIIIIIII)V

    sput-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->RETAIL_ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 39
    new-instance v0, Lcom/squareup/ui/items/ItemEditingStringIds;

    sget v24, Lcom/squareup/editbaseobject/R$string;->item_editing_changes_not_available_item:I

    sget v25, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_item:I

    sget v26, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_not_shared_item:I

    sget v27, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_one_other_location_item:I

    sget v28, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_inactive_item:I

    sget v29, Lcom/squareup/editbaseobject/R$string;->edit_item:I

    sget v30, Lcom/squareup/editbaseobject/R$string;->delete_item:I

    sget v31, Lcom/squareup/editbaseobject/R$string;->item_editing_delete_from_location_item:I

    const-string v22, "ITEM"

    const/16 v23, 0x4

    move-object/from16 v21, v0

    invoke-direct/range {v21 .. v31}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIIIIII)V

    sput-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 45
    new-instance v0, Lcom/squareup/ui/items/ItemEditingStringIds;

    sget v4, Lcom/squareup/editbaseobject/R$string;->item_editing_changes_not_available_mod_set:I

    sget v5, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_mod_set:I

    sget v6, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_not_shared_mod_set:I

    sget v7, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_one_other_location_mod_set:I

    sget v8, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_inactive_mod_set:I

    const-string v2, "MODIFIER_SET"

    const/4 v3, 0x5

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->MODIFIER_SET:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 50
    new-instance v0, Lcom/squareup/ui/items/ItemEditingStringIds;

    sget v12, Lcom/squareup/editbaseobject/R$string;->item_editing_changes_not_available_menu_item:I

    sget v13, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_menu_item:I

    sget v14, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_not_shared_menu_item:I

    sget v15, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_one_other_location_menu_item:I

    sget v16, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_inactive_menu_item:I

    sget v17, Lcom/squareup/editbaseobject/R$string;->edit_restaurant_item:I

    sget v18, Lcom/squareup/editbaseobject/R$string;->item_editing_read_only_details_menu_item:I

    sget v19, Lcom/squareup/editbaseobject/R$string;->item_editing_delete_menu_item:I

    sget v20, Lcom/squareup/editbaseobject/R$string;->item_editing_delete_from_location_menu_item:I

    const-string v10, "RESTAURANT_ITEM"

    const/4 v11, 0x6

    move-object v9, v0

    invoke-direct/range {v9 .. v20}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIIIIIII)V

    sput-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->RESTAURANT_ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 57
    new-instance v0, Lcom/squareup/ui/items/ItemEditingStringIds;

    sget v4, Lcom/squareup/editbaseobject/R$string;->item_editing_changes_not_available_tax:I

    sget v5, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_tax:I

    sget v6, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_not_shared_tax:I

    sget v7, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_one_other_location_tax:I

    sget v8, Lcom/squareup/editbaseobject/R$string;->item_editing_banner_inactive_tax:I

    const-string v2, "TAX"

    const/4 v3, 0x7

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->TAX:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 13
    sget-object v1, Lcom/squareup/ui/items/ItemEditingStringIds;->APPOINTMENTS_SERVICE:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/ItemEditingStringIds;->CATEGORY:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/ItemEditingStringIds;->DISCOUNT:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/ItemEditingStringIds;->RETAIL_ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/ItemEditingStringIds;->ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/ItemEditingStringIds;->MODIFIER_SET:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/ItemEditingStringIds;->RESTAURANT_ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/ItemEditingStringIds;->TAX:Lcom/squareup/ui/items/ItemEditingStringIds;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->$VALUES:[Lcom/squareup/ui/items/ItemEditingStringIds;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII)V"
        }
    .end annotation

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v10, -0x1

    const/4 v11, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    .line 74
    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIIIIIII)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIIIII)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIIIII)V"
        }
    .end annotation

    const/4 v9, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v10, p9

    move/from16 v11, p10

    .line 81
    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/items/ItemEditingStringIds;-><init>(Ljava/lang/String;IIIIIIIIII)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIIIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIIIIII)V"
        }
    .end annotation

    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 88
    iput p3, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerErrorResId:I

    .line 89
    iput p4, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerSharedResId:I

    .line 90
    iput p5, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerNotSharedResId:I

    .line 91
    iput p6, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerOneOtherResId:I

    .line 92
    iput p7, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerInactiveResId:I

    .line 93
    iput p8, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->screenTitleResId:I

    .line 94
    iput p9, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->readOnlyDetailsId:I

    .line 95
    iput p10, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->deleteResId:I

    .line 96
    iput p11, p0, Lcom/squareup/ui/items/ItemEditingStringIds;->deleteMultiunitResId:I

    return-void
.end method

.method public static getStringsForCatalogItemType(Lcom/squareup/api/items/Item$Type;)Lcom/squareup/ui/items/ItemEditingStringIds;
    .locals 1

    .line 100
    sget-object v0, Lcom/squareup/ui/items/ItemEditingStringIds$1;->$SwitchMap$com$squareup$api$items$Item$Type:[I

    invoke-virtual {p0}, Lcom/squareup/api/items/Item$Type;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 106
    sget-object p0, Lcom/squareup/ui/items/ItemEditingStringIds;->APPOINTMENTS_SERVICE:Lcom/squareup/ui/items/ItemEditingStringIds;

    return-object p0

    .line 108
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Strings have not been specified for the provided item type"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 104
    :cond_1
    sget-object p0, Lcom/squareup/ui/items/ItemEditingStringIds;->ITEM:Lcom/squareup/ui/items/ItemEditingStringIds;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/ItemEditingStringIds;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/ui/items/ItemEditingStringIds;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/ItemEditingStringIds;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/ItemEditingStringIds;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/ui/items/ItemEditingStringIds;->$VALUES:[Lcom/squareup/ui/items/ItemEditingStringIds;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/ItemEditingStringIds;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/ItemEditingStringIds;

    return-object v0
.end method
