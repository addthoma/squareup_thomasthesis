.class public final Lcom/squareup/ui/items/EditCategoryLabelScreen;
.super Lcom/squareup/ui/items/InEditCategoryScope;
.source "EditCategoryLabelScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditCategoryLabelScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditCategoryLabelScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditCategoryLabelScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditCategoryLabelScreen$h_NsvAkHenKIpRCKFK4VWSD4ub4;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditCategoryLabelScreen$h_NsvAkHenKIpRCKFK4VWSD4ub4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditCategoryLabelScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditCategoryScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditCategoryLabelScreen;
    .locals 1

    .line 43
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 44
    new-instance v0, Lcom/squareup/ui/items/EditCategoryLabelScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditCategoryLabelScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 39
    iget-object p2, p0, Lcom/squareup/ui/items/EditCategoryLabelScreen;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditCategoryLabelScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/items/EditCategoryLabelCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/items/EditCategoryLabelCoordinator;
    .locals 1

    .line 29
    const-class v0, Lcom/squareup/ui/items/EditCategoryLabelScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditCategoryLabelScreen$Component;

    invoke-interface {p1}, Lcom/squareup/ui/items/EditCategoryLabelScreen$Component;->getCoordinator()Lcom/squareup/ui/items/EditCategoryLabelCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 25
    sget v0, Lcom/squareup/itemsapplet/R$layout;->edit_category_label_view:I

    return v0
.end method
