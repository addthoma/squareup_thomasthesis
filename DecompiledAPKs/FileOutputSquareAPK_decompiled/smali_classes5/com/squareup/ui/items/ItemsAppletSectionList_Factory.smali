.class public final Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;
.super Ljava/lang/Object;
.source "ItemsAppletSectionList_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/ItemsAppletSectionList;",
        ">;"
    }
.end annotation


# instance fields
.field private final allItemsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$AllItems;",
            ">;"
        }
    .end annotation
.end field

.field private final allServicesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$AllServices;",
            ">;"
        }
    .end annotation
.end field

.field private final categoriesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Categories;",
            ">;"
        }
    .end annotation
.end field

.field private final discountsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Discounts;",
            ">;"
        }
    .end annotation
.end field

.field private final gatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final modifiersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;",
            ">;"
        }
    .end annotation
.end field

.field private final prefsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final unitsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Units;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$AllItems;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$AllServices;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Categories;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Discounts;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Units;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->prefsProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->gatekeeperProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->allItemsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->allServicesProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->categoriesProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->modifiersProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->discountsProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p8, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->unitsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p9, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$AllItems;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$AllServices;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Categories;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Discounts;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSection$Units;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;"
        }
    .end annotation

    .line 69
    new-instance v10, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletSection$AllItems;Lcom/squareup/ui/items/ItemsAppletSection$AllServices;Lcom/squareup/ui/items/ItemsAppletSection$Categories;Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;Lcom/squareup/ui/items/ItemsAppletSection$Discounts;Lcom/squareup/ui/items/ItemsAppletSection$Units;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/ItemsAppletSectionList;
    .locals 11

    .line 77
    new-instance v10, Lcom/squareup/ui/items/ItemsAppletSectionList;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/ItemsAppletSectionList;-><init>(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletSection$AllItems;Lcom/squareup/ui/items/ItemsAppletSection$AllServices;Lcom/squareup/ui/items/ItemsAppletSection$Categories;Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;Lcom/squareup/ui/items/ItemsAppletSection$Discounts;Lcom/squareup/ui/items/ItemsAppletSection$Units;Lcom/squareup/util/Res;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/ItemsAppletSectionList;
    .locals 10

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->prefsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->gatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->allItemsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/items/ItemsAppletSection$AllItems;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->allServicesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/items/ItemsAppletSection$AllServices;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->categoriesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/items/ItemsAppletSection$Categories;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->modifiersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->discountsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/items/ItemsAppletSection$Discounts;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->unitsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/items/ItemsAppletSection$Units;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->newInstance(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletSection$AllItems;Lcom/squareup/ui/items/ItemsAppletSection$AllServices;Lcom/squareup/ui/items/ItemsAppletSection$Categories;Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;Lcom/squareup/ui/items/ItemsAppletSection$Discounts;Lcom/squareup/ui/items/ItemsAppletSection$Units;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/ItemsAppletSectionList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemsAppletSectionList_Factory;->get()Lcom/squareup/ui/items/ItemsAppletSectionList;

    move-result-object v0

    return-object v0
.end method
