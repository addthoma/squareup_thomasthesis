.class public final Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;
.super Ljava/lang/Object;
.source "EditItemVariationCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditItemVariationCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final duplicateSkuValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DuplicateSkuValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final editInventoryStateControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;"
        }
    .end annotation
.end field

.field private final editItemVariationRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final errorsBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DuplicateSkuValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->editInventoryStateControllerProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->duplicateSkuValidatorProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->editItemVariationRunnerProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p9, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DuplicateSkuValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/items/EditItemVariationCoordinator;
    .locals 11

    .line 81
    new-instance v10, Lcom/squareup/ui/items/EditItemVariationCoordinator;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/EditItemVariationCoordinator;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditItemVariationCoordinator;
    .locals 10

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/EditItemScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->editInventoryStateControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/items/EditInventoryStateController;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/money/PriceLocaleHelper;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->duplicateSkuValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/items/DuplicateSkuValidator;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->editItemVariationRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->newInstance(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/items/EditItemVariationCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemVariationCoordinator_Factory;->get()Lcom/squareup/ui/items/EditItemVariationCoordinator;

    move-result-object v0

    return-object v0
.end method
