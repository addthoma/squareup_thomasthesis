.class public final Lcom/squareup/ui/items/EditItemMainPresenter_Factory;
.super Ljava/lang/Object;
.source "EditItemMainPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditItemMainPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appliedLocationCountFetcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutLinkShareSheetProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final duplicateSkuValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DuplicateSkuValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final durationPickerRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final editInventoryStateControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;"
        }
    .end annotation
.end field

.field private final editOptionEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final editVariationRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditVariationRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeTextHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EmployeeTextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final errorPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final intermissionHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final itemSuggestionsManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/ItemSuggestionsManager;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryDeleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineStoreAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineStoreRestrictionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private final servicesCustomizationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/ServicesCustomization;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final stateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation
.end field

.field private final stringIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemEditingStringIds;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final unsupportedItemOptionActionDialogRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final weeblySquareSyncServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemEditingStringIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DuplicateSkuValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/ServicesCustomization;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditVariationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EmployeeTextHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/ItemSuggestionsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionEventLogger;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 137
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->stateProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 138
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 139
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->libraryDeleterProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 140
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->errorPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->stringIdsProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->duplicateSkuValidatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->durationPickerRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->servicesCustomizationProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->editInventoryStateControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->editVariationRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->eventLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->employeeTextHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->itemSuggestionsManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->unsupportedItemOptionActionDialogRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->weeblySquareSyncServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 165
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->checkoutLinkShareSheetProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 166
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->serverProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->onlineStoreRestrictionsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->intermissionHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->onlineStoreAnalyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p34

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->editOptionEventLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditItemMainPresenter_Factory;
    .locals 36
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemEditingStringIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DuplicateSkuValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/ServicesCustomization;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditVariationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EmployeeTextHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/ItemSuggestionsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionEventLogger;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemMainPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    .line 207
    new-instance v35, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;

    move-object/from16 v0, v35

    invoke-direct/range {v0 .. v34}, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v35
.end method

.method public static newInstance(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/items/ItemEditingStringIds;Lflow/Flow;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/ui/items/EditVariationRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/ui/items/EmployeeTextHelper;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/librarylist/ItemSuggestionsManager;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/http/Server;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/items/editoption/EditOptionEventLogger;)Lcom/squareup/ui/items/EditItemMainPresenter;
    .locals 36

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    .line 228
    new-instance v35, Lcom/squareup/ui/items/EditItemMainPresenter;

    move-object/from16 v0, v35

    invoke-direct/range {v0 .. v34}, Lcom/squareup/ui/items/EditItemMainPresenter;-><init>(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/items/ItemEditingStringIds;Lflow/Flow;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/ui/items/EditVariationRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/ui/items/EmployeeTextHelper;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/librarylist/ItemSuggestionsManager;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/http/Server;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/items/editoption/EditOptionEventLogger;)V

    return-object v35
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditItemMainPresenter;
    .locals 36

    move-object/from16 v0, p0

    .line 175
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->stateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/items/EditItemState;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/items/EditItemScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->libraryDeleterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/items/LibraryDeleter;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->errorPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->stringIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/items/ItemEditingStringIds;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->duplicateSkuValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/items/DuplicateSkuValidator;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->durationPickerRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->servicesCustomizationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/appointmentsapi/ServicesCustomization;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->editInventoryStateControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/items/EditInventoryStateController;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->editVariationRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/items/EditVariationRunner;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/cogs/Cogs;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->eventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/ui/items/EditItemEventLogger;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->employeeTextHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/ui/items/EmployeeTextHelper;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/catalogapi/CatalogIntegrationController;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->itemSuggestionsManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/librarylist/ItemSuggestionsManager;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->unsupportedItemOptionActionDialogRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->weeblySquareSyncServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->checkoutLinkShareSheetProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/http/Server;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->onlineStoreRestrictionsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->intermissionHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v33, v1

    check-cast v33, Lcom/squareup/intermission/IntermissionHelper;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->onlineStoreAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v34, v1

    check-cast v34, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->editOptionEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v35, v1

    check-cast v35, Lcom/squareup/items/editoption/EditOptionEventLogger;

    invoke-static/range {v2 .. v35}, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->newInstance(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/items/ItemEditingStringIds;Lflow/Flow;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/ui/items/EditVariationRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/ui/items/EmployeeTextHelper;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/librarylist/ItemSuggestionsManager;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/http/Server;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/items/editoption/EditOptionEventLogger;)Lcom/squareup/ui/items/EditItemMainPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter_Factory;->get()Lcom/squareup/ui/items/EditItemMainPresenter;

    move-result-object v0

    return-object v0
.end method
