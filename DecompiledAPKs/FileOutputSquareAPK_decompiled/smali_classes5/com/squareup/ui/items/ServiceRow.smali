.class public final Lcom/squareup/ui/items/ServiceRow;
.super Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;
.source "DetailSearchableListRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0007H\u00c2\u0003J\t\u0010\u001a\u001a\u00020\tH\u00c2\u0003J\t\u0010\u001b\u001a\u00020\u000bH\u00c2\u0003J\t\u0010\u001c\u001a\u00020\rH\u00c2\u0003J\t\u0010\u001d\u001a\u00020\u000fH\u00c2\u0003J\t\u0010\u001e\u001a\u00020\u0011H\u00c2\u0003J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0016JY\u0010#\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0011H\u00c6\u0001J\u0013\u0010$\u001a\u00020%2\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u00d6\u0003J\t\u0010(\u001a\u00020)H\u00d6\u0001J\t\u0010*\u001a\u00020+H\u00d6\u0001R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/ui/items/ServiceRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;",
        "screen",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "service",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "tileAppearanceSettings",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)V",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getScreen",
        "()Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "configureRow",
        "",
        "row",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow;",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

.field private final service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "service"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    move-object v0, p3

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    iput-object p1, p0, Lcom/squareup/ui/items/ServiceRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/ServiceRow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/ServiceRow;->service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    iput-object p4, p0, Lcom/squareup/ui/items/ServiceRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p5, p0, Lcom/squareup/ui/items/ServiceRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p6, p0, Lcom/squareup/ui/items/ServiceRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p7, p0, Lcom/squareup/ui/items/ServiceRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iput-object p8, p0, Lcom/squareup/ui/items/ServiceRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method private final component3()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    return-object v0
.end method

.method private final component4()Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-object v0
.end method

.method private final component5()Lcom/squareup/text/DurationFormatter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-object v0
.end method

.method private final component6()Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-object v0
.end method

.method private final component7()Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-object v0
.end method

.method private final component8()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/ServiceRow;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;ILjava/lang/Object;)Lcom/squareup/ui/items/ServiceRow;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/ui/items/ServiceRow;->service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/ui/items/ServiceRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/ui/items/ServiceRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/items/ServiceRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/items/ServiceRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/ui/items/ServiceRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/ui/items/ServiceRow;->copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/items/ServiceRow;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/util/Res;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method

.method public configureRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 13

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;->getService()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v2

    .line 170
    iget-object v3, p0, Lcom/squareup/ui/items/ServiceRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    .line 171
    iget-object v4, p0, Lcom/squareup/ui/items/ServiceRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 172
    iget-object v6, p0, Lcom/squareup/ui/items/ServiceRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    .line 173
    iget-object v7, p0, Lcom/squareup/ui/items/ServiceRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v9

    .line 175
    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v10

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/16 v11, 0x48

    const/4 v12, 0x0

    move-object v1, p1

    .line 168
    invoke-static/range {v1 .. v12}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindCatalogItem$default(Lcom/squareup/librarylist/LibraryItemListNohoRow;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;ILjava/lang/Object;)V

    return-void
.end method

.method public final copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/items/ServiceRow;
    .locals 10

    const-string v0, "screen"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "service"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/ServiceRow;

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/items/ServiceRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/ServiceRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/ServiceRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/ServiceRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/ServiceRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    iget-object v1, p1, Lcom/squareup/ui/items/ServiceRow;->service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p1, Lcom/squareup/ui/items/ServiceRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v1, p1, Lcom/squareup/ui/items/ServiceRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, p1, Lcom/squareup/ui/items/ServiceRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v1, p1, Lcom/squareup/ui/items/ServiceRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object p1, p1, Lcom/squareup/ui/items/ServiceRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/items/ServiceRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ServiceRow;->service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ServiceRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ServiceRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ServiceRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ServiceRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ServiceRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ServiceRow(screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ServiceRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", service="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ServiceRow;->service:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", priceFormatter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ServiceRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", durationFormatter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ServiceRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemPhotos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ServiceRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tileAppearanceSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ServiceRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currencyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ServiceRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
