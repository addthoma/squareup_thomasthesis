.class public final Lcom/squareup/ui/items/ItemsDeepLinkHandler$CreateItemHistoryFactory;
.super Ljava/lang/Object;
.source "ItemsDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsDeepLinkHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "CreateItemHistoryFactory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0001\n\u0000\u0008\u0080\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0004H\u0016J\n\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/items/ItemsDeepLinkHandler$CreateItemHistoryFactory;",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "(Lcom/squareup/ui/items/ItemsDeepLinkHandler;)V",
        "createHistory",
        "Lflow/History;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "currentHistory",
        "getPermissions",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/ItemsDeepLinkHandler;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/ItemsDeepLinkHandler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/items/ItemsDeepLinkHandler$CreateItemHistoryFactory;->this$0:Lcom/squareup/ui/items/ItemsDeepLinkHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 3

    const-string v0, "home"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/ui/items/EditItemScope;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/ui/items/EditItemScope;-><init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    new-instance v1, Lcom/squareup/ui/items/EditItemMainScreen;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/EditItemMainScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    .line 37
    invoke-static {p1, p2}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p1

    .line 38
    invoke-virtual {p1, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    const-string p2, "home.buildUpon(currentHi\u2026creen)\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getPermissions()Ljava/lang/Void;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getPermissions()Ljava/util/Set;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemsDeepLinkHandler$CreateItemHistoryFactory;->getPermissions()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
