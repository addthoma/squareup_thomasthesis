.class final enum Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;
.super Ljava/lang/Enum;
.source "EditItemScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SaveState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

.field public static final enum COMPLETE:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

.field public static final enum DEFAULT:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

.field public static final enum ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

.field public static final enum SAVE_IN_PROGRESS:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1114
    new-instance v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    const/4 v1, 0x0

    const-string v2, "DEFAULT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->DEFAULT:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    .line 1115
    new-instance v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    const/4 v2, 0x1

    const-string v3, "SAVE_IN_PROGRESS"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->SAVE_IN_PROGRESS:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    .line 1116
    new-instance v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    const/4 v3, 0x2

    const-string v4, "ERROR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    .line 1117
    new-instance v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    const/4 v4, 0x3

    const-string v5, "COMPLETE"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->COMPLETE:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    .line 1113
    sget-object v5, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->DEFAULT:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->SAVE_IN_PROGRESS:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->COMPLETE:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->$VALUES:[Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;
    .locals 1

    .line 1113
    const-class v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;
    .locals 1

    .line 1113
    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->$VALUES:[Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    return-object v0
.end method
