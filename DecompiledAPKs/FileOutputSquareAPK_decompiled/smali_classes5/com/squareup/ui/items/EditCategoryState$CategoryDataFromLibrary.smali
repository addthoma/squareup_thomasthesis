.class public Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;
.super Ljava/lang/Object;
.source "EditCategoryState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CategoryDataFromLibrary"
.end annotation


# instance fields
.field public final category:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

.field public final categoryNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final initiallyEmptyCategory:Z

.field public final libraryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemCategory;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/Map;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->category:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 196
    iput-object p2, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->libraryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 197
    iput-object p3, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->categoryNameMap:Ljava/util/Map;

    .line 198
    iput-boolean p4, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->initiallyEmptyCategory:Z

    return-void
.end method
