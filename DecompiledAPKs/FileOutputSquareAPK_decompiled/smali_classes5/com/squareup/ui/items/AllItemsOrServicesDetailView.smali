.class public Lcom/squareup/ui/items/AllItemsOrServicesDetailView;
.super Lcom/squareup/ui/items/DetailSearchableListView;
.source "AllItemsOrServicesDetailView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/AllItemsOrServicesDetailView$AllItemsDetailViewComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListView<",
        "Lcom/squareup/ui/items/AllItemsOrServicesDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field presenter:Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method getPresenter()Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->presenter:Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;

    return-object v0
.end method

.method bridge synthetic getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->getPresenter()Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;

    move-result-object v0

    return-object v0
.end method

.method protected inject()V
    .locals 2

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/items/AllItemsOrServicesDetailView$AllItemsDetailViewComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/AllItemsOrServicesDetailView$AllItemsDetailViewComponent;

    invoke-interface {v0, p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailView$AllItemsDetailViewComponent;->inject(Lcom/squareup/ui/items/AllItemsOrServicesDetailView;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 23
    invoke-super {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->onFinishInflate()V

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->searchBar:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->presenter:Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->getSearchHint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->presenter:Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->getNullTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->presenter:Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;

    invoke-virtual {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->getNullHint(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;->setNullState(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method
