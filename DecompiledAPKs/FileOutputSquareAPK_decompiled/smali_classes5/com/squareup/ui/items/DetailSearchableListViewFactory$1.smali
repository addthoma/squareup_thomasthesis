.class final Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DetailSearchableListViewFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListViewFactory;-><init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;Ljava/lang/Class;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/ui/items/DetailSearchableListCoordinator;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0010\u0000\u001a\u00020\u00012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/items/DetailSearchableListCoordinator;",
        "it",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field final synthetic $currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field final synthetic $device:Lcom/squareup/util/Device;

.field final synthetic $durationFormatter:Lcom/squareup/text/DurationFormatter;

.field final synthetic $itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field final synthetic $mainScheduler:Lio/reactivex/Scheduler;

.field final synthetic $percentageFormatter:Lcom/squareup/text/Formatter;

.field final synthetic $priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field final synthetic $recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field final synthetic $res:Lcom/squareup/util/Res;

.field final synthetic $tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field final synthetic $tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Device;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$device:Lcom/squareup/util/Device;

    iput-object p10, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iput-object p11, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p12, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$mainScheduler:Lio/reactivex/Scheduler;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lio/reactivex/Observable;)Lcom/squareup/ui/items/DetailSearchableListCoordinator;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/ui/items/DetailSearchableListCoordinator;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "it"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    .line 56
    sget-object v3, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListViewFactory$1$1;

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    const-string v1, "it.map { it.unwrapV2Screen }"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iget-object v6, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$res:Lcom/squareup/util/Res;

    iget-object v7, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v8, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 57
    iget-object v9, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v10, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v11, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v12, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v13, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$device:Lcom/squareup/util/Device;

    .line 58
    iget-object v14, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v15, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->$mainScheduler:Lio/reactivex/Scheduler;

    move-object v3, v2

    move-object/from16 v16, v1

    .line 55
    invoke-direct/range {v3 .. v16}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Device;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)V

    return-object v2
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;->invoke(Lio/reactivex/Observable;)Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    move-result-object p1

    return-object p1
.end method
