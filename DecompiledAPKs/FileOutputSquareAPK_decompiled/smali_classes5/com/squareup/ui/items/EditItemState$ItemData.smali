.class final Lcom/squareup/ui/items/EditItemState$ItemData;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ItemData"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemState$ItemData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field allEnabledTaxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation
.end field

.field private categoryId:Ljava/lang/String;

.field private categoryName:Ljava/lang/String;

.field item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

.field measurementUnits:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;"
        }
    .end annotation
.end field

.field variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 921
    new-instance v0, Lcom/squareup/ui/items/EditItemState$ItemData$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditItemState$ItemData$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$ItemData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 745
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    .line 746
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    .line 747
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    return-void
.end method

.method constructor <init>(Lcom/squareup/api/items/Item$Type;)V
    .locals 1

    .line 740
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;-><init>(Lcom/squareup/api/items/Item$Type;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemState$ItemData;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem$Builder;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItem$Builder;)V
    .locals 1

    .line 750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 751
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 752
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    .line 753
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 754
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Ljava/lang/String;)V

    .line 755
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 756
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    .line 757
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItem$Builder;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem$Builder;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;)V"
        }
    .end annotation

    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 764
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 765
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    .line 766
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    .line 767
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    .line 768
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    .line 769
    iput-object p6, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItem$Builder;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/squareup/ui/items/EditItemState$1;)V
    .locals 0

    .line 731
    invoke-direct/range {p0 .. p6}, Lcom/squareup/ui/items/EditItemState$ItemData;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem$Builder;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$402(Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 731
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 731
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public addItemVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)V
    .locals 1

    .line 808
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 809
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public cloneItemData()Lcom/squareup/ui/items/EditItemState$ItemData;
    .locals 5

    .line 773
    new-instance v0, Lcom/squareup/ui/items/EditItemState$ItemData;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;-><init>()V

    .line 775
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;)V

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 776
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 777
    iget-object v3, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    new-instance v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 780
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 781
    iget-object v3, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    new-instance v4, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    invoke-direct {v4, v2}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogTax;)V

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 784
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    .line 785
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 908
    :cond_0
    instance-of v1, p1, Lcom/squareup/ui/items/EditItemState$ItemData;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 909
    :cond_1
    check-cast p1, Lcom/squareup/ui/items/EditItemState$ItemData;

    .line 910
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v1

    iget-object v3, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/shared/catalog/models/CatalogItem;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    .line 911
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v3, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v1, v3, :cond_3

    return v2

    :cond_3
    const/4 v1, 0x0

    .line 912
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 913
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v3

    iget-object v4, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    return v2

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 915
    :cond_5
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v2

    .line 916
    :cond_6
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    return v2

    .line 917
    :cond_7
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    return v2

    :cond_8
    return v0
.end method

.method public getCategoryId()Ljava/lang/String;
    .locals 1

    .line 800
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryName()Ljava/lang/String;
    .locals 1

    .line 804
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 3

    .line 845
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 846
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 848
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-object v0
.end method

.method public getItemOptionSetIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 883
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAllItemOptionIds()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getItemOptionValueIdsByOptionIdsUsedInItem()Ljava/util/LinkedHashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 886
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 888
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 890
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v2

    .line 891
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 892
    iget-object v4, v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    .line 893
    iget-object v3, v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    .line 895
    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 896
    new-instance v5, Ljava/util/LinkedHashSet;

    invoke-direct {v5}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-virtual {v0, v4, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 899
    :cond_1
    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/LinkedHashSet;

    invoke-virtual {v4, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public getVariationIdsByMerchantCatalogTokens()Ljava/util/LinkedHashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 852
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 853
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 854
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v3

    .line 855
    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 856
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getVariationMerchantCatalogTokensByIds()Ljava/util/LinkedHashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 863
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 864
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 865
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v3

    .line 866
    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 867
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public hasItemOptions()Z
    .locals 1

    .line 880
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAllItemOptionIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public removeItemVariationById(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 815
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 816
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 817
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 822
    :cond_1
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 824
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 827
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->removeAllOptions()Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    :cond_2
    return v0
.end method

.method public reorderVariation(II)V
    .locals 1

    .line 875
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 876
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void
.end method

.method public replaceItemVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 834
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 835
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 836
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 837
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {p1, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public setItemCategory(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 790
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    .line 791
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    .line 792
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 793
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {p2, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setCategoryId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    goto :goto_0

    .line 795
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setCategoryId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    :goto_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 968
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->toByteArray()[B

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 969
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 970
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 971
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0

    .line 973
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 974
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 975
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_1

    .line 977
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 978
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->categoryName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 979
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 980
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 981
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 982
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_2

    :cond_2
    return-void
.end method
