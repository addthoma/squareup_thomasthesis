.class public Lcom/squareup/ui/items/LibraryDeleter;
.super Ljava/lang/Object;
.source "LibraryDeleter.java"


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/items/LibraryDeleter;->cogs:Lcom/squareup/cogs/Cogs;

    return-void
.end method

.method private deleteCategoryTask(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/shared/catalog/CatalogTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 66
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$1jECfQmMhCRHqC0Yg1JMB3l4y4w;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$1jECfQmMhCRHqC0Yg1JMB3l4y4w;-><init>(Lcom/squareup/shared/catalog/models/CatalogObject;)V

    return-object v0
.end method

.method private deleteDiscountTask(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/shared/catalog/CatalogTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 126
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$zmj3j31OoMh_DhxVanoPXJGH8oo;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$zmj3j31OoMh_DhxVanoPXJGH8oo;-><init>(Lcom/squareup/shared/catalog/models/CatalogObject;)V

    return-object v0
.end method

.method private deleteItemTask(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/shared/catalog/CatalogTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 106
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$toMPIIEjv_X7iNVbZFINffYkg1k;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$toMPIIEjv_X7iNVbZFINffYkg1k;-><init>(Lcom/squareup/shared/catalog/models/CatalogObject;)V

    return-object v0
.end method

.method private deleteModifierListTask(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/shared/catalog/CatalogTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 89
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$Y9y2JZy5QQTx80g8Wc1afsb3Odo;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$Y9y2JZy5QQTx80g8Wc1afsb3Odo;-><init>(Lcom/squareup/shared/catalog/models/CatalogObject;)V

    return-object v0
.end method

.method static synthetic lambda$deleteCategoryById$0(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;
    .locals 1

    .line 44
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    invoke-interface {p1, v0, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    return-object p0
.end method

.method static synthetic lambda$deleteCategoryTask$2(Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 5

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 69
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findCategoryItems(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 70
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 72
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/Item;

    invoke-virtual {v4}, Lcom/squareup/api/items/Item;->newBuilder()Lcom/squareup/api/items/Item$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/squareup/api/items/Item$Builder;->menu_category(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Item$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/api/items/Item$Builder;->build()Lcom/squareup/api/items/Item;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogItem;->copy(Lcom/squareup/api/items/Item;)Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v2

    .line 73
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 77
    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object p0

    sget-object v2, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    invoke-interface {p1, p0, v2}, Lcom/squareup/shared/catalog/Catalog$Local;->findTilePageMemberships(Ljava/lang/String;Lcom/squareup/api/items/Type;)Ljava/util/List;

    move-result-object p0

    .line 81
    invoke-interface {v1, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 83
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v3
.end method

.method static synthetic lambda$deleteDiscountTask$5(Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 3

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 132
    sget-object v1, Lcom/squareup/api/items/Type;->DISCOUNT:Lcom/squareup/api/items/Type;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object p0

    sget-object v2, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    .line 133
    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 132
    invoke-interface {p1, v1, p0, v2}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectIdsForRelatedObjects(Lcom/squareup/api/items/Type;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    .line 134
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 135
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/sync/ObjectId;

    .line 136
    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 138
    :cond_0
    const-class p0, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-interface {p1, p0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 139
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    invoke-interface {p1, p0, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic lambda$deleteItemTask$4(Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 2

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 112
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v1

    .line 113
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoObjectType()Lcom/squareup/api/items/Type;

    move-result-object p0

    invoke-interface {p1, v1, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findTilePageMemberships(Ljava/lang/String;Lcom/squareup/api/items/Type;)Ljava/util/List;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 115
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemFeeMemberships(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 116
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemVariations(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 117
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemModifierMemberships(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 120
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    invoke-interface {p1, p0, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic lambda$deleteModifierListTask$3(Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findModifierItemMemberships(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 96
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findModifierOptions(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 98
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 100
    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    invoke-interface {p1, p0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method private syncWhenFinishedWithPostHook(Ljava/lang/Runnable;)Lcom/squareup/shared/catalog/CatalogCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 145
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$MswzCJwNM4J_v6OK6VuriHzrSmU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$MswzCJwNM4J_v6OK6VuriHzrSmU;-><init>(Lcom/squareup/ui/items/LibraryDeleter;Ljava/lang/Runnable;)V

    return-object v0
.end method


# virtual methods
.method public delete(Lcom/squareup/shared/catalog/models/CatalogObject;Ljava/lang/Runnable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .line 50
    instance-of v0, p1, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    if-eqz v0, :cond_0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/LibraryDeleter;->deleteCategoryTask(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/shared/catalog/CatalogTask;

    move-result-object p1

    goto :goto_0

    .line 52
    :cond_0
    instance-of v0, p1, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    if-eqz v0, :cond_1

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/LibraryDeleter;->deleteModifierListTask(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/shared/catalog/CatalogTask;

    move-result-object p1

    goto :goto_0

    .line 54
    :cond_1
    instance-of v0, p1, Lcom/squareup/shared/catalog/models/CatalogItem;

    if-eqz v0, :cond_2

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/LibraryDeleter;->deleteItemTask(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/shared/catalog/CatalogTask;

    move-result-object p1

    goto :goto_0

    .line 56
    :cond_2
    instance-of v0, p1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    if-eqz v0, :cond_3

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/LibraryDeleter;->deleteDiscountTask(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/shared/catalog/CatalogTask;

    move-result-object p1

    .line 62
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/LibraryDeleter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-direct {p0, p2}, Lcom/squareup/ui/items/LibraryDeleter;->syncWhenFinishedWithPostHook(Ljava/lang/Runnable;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 59
    :cond_3
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Deletion of CatalogObject with type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " is unsupported."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public deleteCategoryById(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/items/LibraryDeleter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$q012l2LqrKRHAw-DADbLTX7VC44;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$q012l2LqrKRHAw-DADbLTX7VC44;-><init>(Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$4JUJlzr-H_BJwikUYk608S8CjMo;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/items/-$$Lambda$LibraryDeleter$4JUJlzr-H_BJwikUYk608S8CjMo;-><init>(Lcom/squareup/ui/items/LibraryDeleter;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public synthetic lambda$deleteCategoryById$1$LibraryDeleter(Ljava/lang/Runnable;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 45
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogObject;

    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/items/LibraryDeleter;->delete(Lcom/squareup/shared/catalog/models/CatalogObject;Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$syncWhenFinishedWithPostHook$6$LibraryDeleter(Ljava/lang/Runnable;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/items/LibraryDeleter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    .line 147
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void
.end method
