.class public final Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;
.super Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;
.source "CompactFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "THOUSAND"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;",
        "()V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 115
    new-instance v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;

    invoke-direct {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;-><init>()V

    sput-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x3e8

    .line 115
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    const-string v1, "BigDecimal.valueOf(1000)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;-><init>(Ljava/math/BigDecimal;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
