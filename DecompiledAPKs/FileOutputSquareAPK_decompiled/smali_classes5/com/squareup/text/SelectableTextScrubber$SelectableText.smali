.class public Lcom/squareup/text/SelectableTextScrubber$SelectableText;
.super Ljava/lang/Object;
.source "SelectableTextScrubber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/SelectableTextScrubber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SelectableText"
.end annotation


# instance fields
.field public final selectionEnd:I

.field public final selectionStart:I

.field public final text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, -0x1

    .line 28
    invoke-direct {p0, p1, v0, v0}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    .line 33
    iput p2, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    .line 34
    iput p3, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    return-void
.end method


# virtual methods
.method public cursorAtEndOfText()Z
    .locals 2

    .line 48
    iget v0, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget v1, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", selectionStart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", selectionEnd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
