.class public Lcom/squareup/text/Fonts;
.super Ljava/lang/Object;
.source "Fonts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/Fonts$FittedTextInfo;
    }
.end annotation


# static fields
.field private static final TMP_RECT:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/squareup/text/Fonts;->TMP_RECT:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addSectionBreaks(Landroid/content/res/Resources;II)Ljava/lang/CharSequence;
    .locals 1

    .line 25
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 26
    invoke-static {p0, p2}, Lcom/squareup/text/LineHeightSpan;->getLineHeightCharSequence(Landroid/content/res/Resources;I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string p2, "\n"

    .line 27
    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 28
    invoke-static {p0, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "section_break"

    .line 29
    invoke-virtual {p0, p1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 30
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFF)V
    .locals 6

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 52
    invoke-static/range {v0 .. v5}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFFF)V

    return-void
.end method

.method public static autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFFF)V
    .locals 4

    .line 57
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 59
    invoke-virtual {p0, v0}, Landroid/text/TextPaint;->setTextScaleX(F)V

    const/high16 v0, 0x42c80000    # 100.0f

    .line 60
    invoke-virtual {p0, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    const/4 v1, 0x0

    .line 61
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sget-object v3, Lcom/squareup/text/Fonts;->TMP_RECT:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v1, v2, v3}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 63
    sget-object p1, Lcom/squareup/text/Fonts;->TMP_RECT:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    int-to-float p1, p1

    int-to-float p2, p2

    mul-float p2, p2, p5

    div-float/2addr p2, p1

    mul-float p2, p2, v0

    .line 66
    invoke-static {p2, p3}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-static {p1, p4}, Ljava/lang/Math;->min(FF)F

    move-result p1

    float-to-double p1, p1

    .line 67
    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide p1

    double-to-int p1, p1

    int-to-float p1, p1

    .line 68
    invoke-virtual {p0, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    return-void
.end method

.method public static autoFitText(Landroid/text/TextPaint;Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 43
    invoke-static {p0, p1, p2, v0}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;Landroid/graphics/Rect;F)V

    return-void
.end method

.method public static autoFitText(Landroid/text/TextPaint;Ljava/lang/String;Landroid/graphics/Rect;F)V
    .locals 6

    .line 47
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    const/4 v3, 0x0

    const/high16 v4, 0x4f000000

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFFF)V

    return-void
.end method

.method public static buildFittedTextInfo(Landroid/text/TextPaint;Ljava/lang/CharSequence;IFF)Lcom/squareup/text/Fonts$FittedTextInfo;
    .locals 9

    .line 92
    new-instance v8, Landroid/text/TextPaint;

    invoke-direct {v8, p0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 93
    invoke-static {v8, p1, p2, p3, p4}, Lcom/squareup/text/Fonts;->getFittedTextSize(Landroid/text/TextPaint;Ljava/lang/CharSequence;IFF)I

    move-result p0

    int-to-float p0, p0

    .line 94
    invoke-virtual {v8, p0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 97
    new-instance p3, Landroid/text/StaticLayout;

    mul-int/lit16 v3, p2, 0x3e8

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p3

    move-object v1, p1

    move-object v2, v8

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 100
    invoke-virtual {p3}, Landroid/text/StaticLayout;->getHeight()I

    move-result p2

    .line 101
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p3

    const/4 p4, 0x0

    invoke-virtual {v8, p1, p4, p3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result p1

    float-to-double p3, p1

    invoke-static {p3, p4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p3

    double-to-int p1, p3

    .line 102
    new-instance p3, Lcom/squareup/text/Fonts$FittedTextInfo;

    int-to-float p1, p1

    int-to-float p2, p2

    invoke-direct {p3, p0, p1, p2}, Lcom/squareup/text/Fonts$FittedTextInfo;-><init>(FFF)V

    return-object p3
.end method

.method public static getFittedTextSize(Landroid/text/TextPaint;Ljava/lang/CharSequence;IFF)I
    .locals 3

    if-gtz p2, :cond_0

    float-to-int p0, p3

    return p0

    .line 78
    :cond_0
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, p0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    const/high16 p0, 0x3f800000    # 1.0f

    .line 79
    invoke-virtual {v0, p0}, Landroid/text/TextPaint;->setTextScaleX(F)V

    const/high16 p0, 0x42c80000    # 100.0f

    .line 80
    invoke-virtual {v0, p0}, Landroid/text/TextPaint;->setTextSize(F)V

    const/4 v1, 0x0

    .line 83
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result p1

    int-to-float p2, p2

    div-float/2addr p2, p1

    mul-float p2, p2, p0

    .line 87
    invoke-static {p2, p3}, Ljava/lang/Math;->max(FF)F

    move-result p0

    invoke-static {p0, p4}, Ljava/lang/Math;->min(FF)F

    move-result p0

    float-to-double p0, p0

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide p0

    double-to-int p0, p0

    return p0
.end method

.method public static getYForCenteredText(Landroid/text/TextPaint;FF)F
    .locals 2

    .line 38
    invoke-virtual {p0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object p0

    add-float/2addr p2, p1

    const/high16 p1, 0x40000000    # 2.0f

    div-float/2addr p2, p1

    .line 39
    iget v0, p0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v1, p0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v0, v1

    div-float/2addr v0, p1

    add-float/2addr p2, v0

    iget p0, p0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    sub-float/2addr p2, p0

    return p2
.end method
