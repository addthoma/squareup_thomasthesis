.class public Lcom/squareup/text/QuantityScrubber;
.super Ljava/lang/Object;
.source "QuantityScrubber.java"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# static fields
.field private static final LEADING_ZEROS_AND_NON_DIGITS:Ljava/util/regex/Pattern;

.field private static final MAX_VALUE:J


# instance fields
.field private allowZero:Z

.field private maxValue:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string v0, "(^[^1-9]+)|([^0-9])"

    .line 17
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/QuantityScrubber;->LEADING_ZEROS_AND_NON_DIGITS:Ljava/util/regex/Pattern;

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    const-wide/high16 v2, 0x4032000000000000L    # 18.0

    .line 18
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-long v0, v0

    sput-wide v0, Lcom/squareup/text/QuantityScrubber;->MAX_VALUE:J

    return-void
.end method

.method public constructor <init>(J)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/squareup/text/QuantityScrubber;->setMaxValue(J)V

    return-void
.end method


# virtual methods
.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 46
    sget-object v0, Lcom/squareup/text/QuantityScrubber;->LEADING_ZEROS_AND_NON_DIGITS:Ljava/util/regex/Pattern;

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p1

    .line 48
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    iget-boolean p1, p0, Lcom/squareup/text/QuantityScrubber;->allowZero:Z

    if-eqz p1, :cond_0

    const-string p1, "0"

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1

    .line 51
    :cond_1
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/text/QuantityScrubber;->maxValue:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 52
    invoke-static {}, Lcom/squareup/currency_db/NumberFormats;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public setAllowZero(Z)V
    .locals 0

    .line 42
    iput-boolean p1, p0, Lcom/squareup/text/QuantityScrubber;->allowZero:Z

    return-void
.end method

.method public setMaxValue(J)V
    .locals 4

    const-wide/16 v0, 0x1

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 34
    sget-wide v0, Lcom/squareup/text/QuantityScrubber;->MAX_VALUE:J

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    .line 38
    iput-wide p1, p0, Lcom/squareup/text/QuantityScrubber;->maxValue:J

    return-void

    .line 35
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 36
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    sget-wide v2, Lcom/squareup/text/QuantityScrubber;->MAX_VALUE:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, v1, p1

    const-string p1, "maxValue=%d, max is %d"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
