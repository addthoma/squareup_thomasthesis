.class public interface abstract Lcom/squareup/text/HasSelectableText;
.super Ljava/lang/Object;
.source "HasSelectableText.java"


# virtual methods
.method public abstract addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
.end method

.method public abstract addTextChangedListener(Landroid/text/TextWatcher;)V
.end method

.method public abstract getSelectionEnd()I
.end method

.method public abstract getSelectionStart()I
.end method

.method public abstract getText()Landroid/text/Editable;
.end method

.method public abstract postRestartInput()V
.end method

.method public abstract removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
.end method

.method public abstract removeTextChangedListener(Landroid/text/TextWatcher;)V
.end method

.method public abstract setKeyListener(Landroid/text/method/KeyListener;)V
.end method

.method public abstract setSelection(II)V
.end method
