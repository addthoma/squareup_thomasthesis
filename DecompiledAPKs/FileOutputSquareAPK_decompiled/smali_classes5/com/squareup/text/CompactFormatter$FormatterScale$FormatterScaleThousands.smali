.class public abstract Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;
.super Lcom/squareup/text/CompactFormatter$FormatterScale;
.source "CompactFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/CompactFormatter$FormatterScale;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "FormatterScaleThousands"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;,
        Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;,
        Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0007\u0008\tB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0003\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;",
        "Lcom/squareup/text/CompactFormatter$FormatterScale;",
        "magnitude",
        "Ljava/math/BigDecimal;",
        "(Ljava/math/BigDecimal;)V",
        "getMagnitude",
        "()Ljava/math/BigDecimal;",
        "BILLION",
        "MILLION",
        "THOUSAND",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final magnitude:Ljava/math/BigDecimal;


# direct methods
.method private constructor <init>(Ljava/math/BigDecimal;)V
    .locals 1

    const/4 v0, 0x0

    .line 114
    invoke-direct {p0, p1, v0}, Lcom/squareup/text/CompactFormatter$FormatterScale;-><init>(Ljava/math/BigDecimal;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;->magnitude:Ljava/math/BigDecimal;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/math/BigDecimal;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;-><init>(Ljava/math/BigDecimal;)V

    return-void
.end method


# virtual methods
.method public getMagnitude()Ljava/math/BigDecimal;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;->magnitude:Ljava/math/BigDecimal;

    return-object v0
.end method
