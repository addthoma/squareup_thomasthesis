.class public final Lcom/squareup/text/Cards;
.super Ljava/lang/Object;
.source "Cards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/Cards$CardOwnerName;
    }
.end annotation


# static fields
.field public static final CARD_NAME_SEPARATOR:Ljava/lang/String; = "/"

.field public static final CARD_TITLE_SEPARATOR:Ljava/lang/String; = "."

.field private static final jsonStringToBrand:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/Card$Brand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/squareup/text/Cards;->jsonStringToBrand:Ljava/util/Map;

    .line 111
    invoke-static {}, Lcom/squareup/Card$Brand;->values()[Lcom/squareup/Card$Brand;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 112
    invoke-virtual {v3}, Lcom/squareup/Card$Brand;->getJsonRepresentation()Ljava/lang/String;

    move-result-object v4

    .line 113
    sget-object v5, Lcom/squareup/text/Cards;->jsonStringToBrand:Ljava/util/Map;

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static brandFromJson(Ljava/lang/String;)Lcom/squareup/Card$Brand;
    .locals 1

    .line 122
    sget-object v0, Lcom/squareup/text/Cards;->jsonStringToBrand:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/Card$Brand;

    if-nez p0, :cond_0

    .line 123
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    :cond_0
    return-object p0
.end method

.method public static formattedBrandAndUnmaskedDigits(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 188
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "####"

    .line 189
    :cond_0
    sget v0, Lcom/squareup/common/card/R$string;->saved_card:I

    invoke-static {p0, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 190
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    const-string p1, "card_brand"

    invoke-virtual {v0, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string/jumbo p1, "unmasked_digits"

    .line 191
    invoke-virtual {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 192
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formattedBrandAndUnmaskedDigits(Landroid/content/res/Resources;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 175
    invoke-static {p1}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object p1

    iget p1, p1, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    invoke-static {p0, p1, p2}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formattedBrandAndUnmaskedDigits(Landroid/content/res/Resources;Lcom/squareup/Card;)Ljava/lang/String;
    .locals 1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    iget v0, v0, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    .line 163
    invoke-virtual {p1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    .line 162
    invoke-static {p0, v0, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 198
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "####"

    .line 199
    :cond_0
    sget v0, Lcom/squareup/common/card/R$string;->saved_card:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 200
    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    const-string p1, "card_brand"

    invoke-virtual {v0, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string/jumbo p1, "unmasked_digits"

    .line 201
    invoke-virtual {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 202
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 169
    invoke-static {p1}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object p1

    iget p1, p1, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    invoke-static {p0, p1, p2}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card;)Ljava/lang/String;
    .locals 1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    iget v0, v0, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    .line 157
    invoke-virtual {p1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    .line 156
    invoke-static {p0, v0, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    .line 213
    :try_start_0
    invoke-static {p1}, Lcom/squareup/text/Cards;->parseCardOwnerName(Ljava/lang/String;)Lcom/squareup/text/Cards$CardOwnerName;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    .line 220
    invoke-virtual {p1, p0}, Lcom/squareup/text/Cards$CardOwnerName;->format(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    return-object v0

    :catch_0
    move-exception p0

    .line 215
    invoke-static {p1}, Lcom/squareup/util/Strings;->redactAlpha(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error parsing card name like "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-object v0
.end method

.method public static formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 229
    new-instance v0, Lcom/squareup/text/Cards$CardOwnerName;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/text/Cards$CardOwnerName;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lcom/squareup/text/Cards$CardOwnerName;->format(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formattedGiftCard(Lcom/squareup/util/Res;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 0

    if-eqz p2, :cond_0

    .line 180
    sget p2, Lcom/squareup/common/card/R$string;->electronic_gift_card:I

    goto :goto_0

    :cond_0
    sget p2, Lcom/squareup/common/card/R$string;->gift_card:I

    :goto_0
    invoke-static {p0, p2, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formattedMask(Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    sget-object v1, Lcom/squareup/text/Cards$1;->$SwitchMap$com$squareup$Card$Brand:[I

    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const-string/jumbo v2, "\u2022\u2022\u2022\u2022 "

    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    .line 137
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\u2022\u2022\u2022\u2022"

    .line 140
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 131
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\u2022\u2022\u2022\u2022\u2022\u2022 "

    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\u2022\u2022\u2022\u2022\u2022"

    .line 133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    if-eqz p1, :cond_1

    .line 144
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->unmaskedDigits()I

    move-result p0

    invoke-static {v1, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 146
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, p0

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 148
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    sub-int/2addr v1, p0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result p0

    invoke-virtual {v0, v1, p0, p1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static parseCardOwnerName(Ljava/lang/String;)Lcom/squareup/text/Cards$CardOwnerName;
    .locals 5

    .line 234
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "/"

    .line 240
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, -0x1

    const-string v3, ""

    if-eq v0, v2, :cond_1

    .line 242
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v0, v0, 0x1

    .line 243
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    move-object v4, v3

    :goto_0
    const-string v0, "."

    .line 245
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 247
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    .line 248
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 250
    :cond_2
    invoke-static {p0}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 253
    :goto_1
    new-instance p0, Lcom/squareup/text/Cards$CardOwnerName;

    invoke-direct {p0, v3, v1, v4}, Lcom/squareup/text/Cards$CardOwnerName;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method
