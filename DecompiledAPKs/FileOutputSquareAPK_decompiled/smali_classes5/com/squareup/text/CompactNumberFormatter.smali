.class public final Lcom/squareup/text/CompactNumberFormatter;
.super Lcom/squareup/text/CompactFormatter;
.source "CompactNumberFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/CompactNumberFormatter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/text/CompactFormatter<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCompactNumberFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CompactNumberFormatter.kt\ncom/squareup/text/CompactNumberFormatter\n*L\n1#1,62:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0002\u0008\u0003\u0018\u0000 \u00102\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0002H\u0016R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\t\u001a\u00020\n*\u00020\u00028TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/text/CompactNumberFormatter;",
        "Lcom/squareup/text/CompactFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lcom/squareup/util/Res;Ljavax/inject/Provider;)V",
        "asBigDecimal",
        "Ljava/math/BigDecimal;",
        "getAsBigDecimal",
        "(Ljava/lang/Number;)Ljava/math/BigDecimal;",
        "format",
        "",
        "number",
        "Companion",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/text/CompactNumberFormatter$Companion;

.field private static final DEFAULT_COMPACT_FRACTIONAL_DIGITS:I = 0x2


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/text/CompactNumberFormatter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/text/CompactNumberFormatter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/text/CompactNumberFormatter;->Companion:Lcom/squareup/text/CompactNumberFormatter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/text/CompactFormatter;-><init>(Lcom/squareup/util/Res;)V

    iput-object p2, p0, Lcom/squareup/text/CompactNumberFormatter;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public format(Ljava/lang/Number;)Ljava/lang/CharSequence;
    .locals 3

    if-nez p1, :cond_0

    const-string p1, ""

    .line 31
    check-cast p1, Ljava/lang/CharSequence;

    return-object p1

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/CompactNumberFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 36
    invoke-virtual {p0, p1}, Lcom/squareup/text/CompactNumberFormatter;->getAsBigDecimal(Ljava/lang/Number;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/text/CompactNumberFormatter;->needsTruncation(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x2

    if-eqz v1, :cond_1

    .line 40
    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 41
    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 43
    sget-object v1, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 48
    :cond_1
    invoke-virtual {p0, p1, v2}, Lcom/squareup/text/CompactNumberFormatter;->truncatedValueAndScale(Ljava/lang/Object;I)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    .line 47
    check-cast v1, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    .line 50
    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "numberFormat.format(valueTruncated)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, v0, p1}, Lcom/squareup/text/CompactNumberFormatter;->abbreviate(Ljava/lang/String;Lcom/squareup/text/CompactFormatter$FormatterScale;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 24
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p0, p1}, Lcom/squareup/text/CompactNumberFormatter;->format(Ljava/lang/Number;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method protected getAsBigDecimal(Ljava/lang/Number;)Ljava/math/BigDecimal;
    .locals 1

    const-string v0, "$this$asBigDecimal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1}, Ljava/lang/Number;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic getAsBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .locals 0

    .line 24
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p0, p1}, Lcom/squareup/text/CompactNumberFormatter;->getAsBigDecimal(Ljava/lang/Number;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method
