.class public final Lcom/squareup/text/phone/number/RealPhoneNumberHelper;
.super Ljava/lang/Object;
.source "RealPhoneNumberHelper.kt"

# interfaces
.implements Lcom/squareup/text/PhoneNumberHelper;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPhoneNumberHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPhoneNumberHelper.kt\ncom/squareup/text/phone/number/RealPhoneNumberHelper\n+ 2 _Strings.kt\nkotlin/text/StringsKt___StringsKt\n*L\n1#1,102:1\n1004#2,3:103\n*E\n*S KotlinDebug\n*F\n+ 1 RealPhoneNumberHelper.kt\ncom/squareup/text/phone/number/RealPhoneNumberHelper\n*L\n87#1,3:103\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0016J\u0014\u0010\u000f\u001a\u0004\u0018\u00010\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0016J\u0012\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u00068BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/text/phone/number/RealPhoneNumberHelper;",
        "Lcom/squareup/text/PhoneNumberHelper;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "(Lcom/squareup/CountryCode;)V",
        "util",
        "Lcom/google/i18n/phonenumbers/PhoneNumberUtil;",
        "kotlin.jvm.PlatformType",
        "getUtil",
        "()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;",
        "util$delegate",
        "Lkotlin/Lazy;",
        "format",
        "",
        "rawText",
        "formatPartial",
        "isValid",
        "",
        "input",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;

.field private final util$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Lcom/squareup/CountryCode;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->countryCode:Lcom/squareup/CountryCode;

    .line 16
    sget-object p1, Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;->INSTANCE:Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->util$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private final getUtil()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;
    .locals 1

    iget-object v0, p0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->util$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    return-object v0
.end method


# virtual methods
.method public format(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 41
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->getUtil()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v1

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v0}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :try_end_0
    .catch Lcom/google/i18n/phonenumbers/NumberParseException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 48
    sget-object v0, Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->INTERNATIONAL:Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    goto :goto_1

    .line 50
    :cond_1
    sget-object v0, Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->NATIONAL:Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    .line 54
    :goto_1
    :try_start_1
    invoke-direct {p0}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->getUtil()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v1

    .line 55
    invoke-direct {p0}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->getUtil()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v2

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v4}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v2

    .line 54
    invoke-virtual {v1, v2, v0}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->format(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Lcom/google/i18n/phonenumbers/NumberParseException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-object p1
.end method

.method public formatPartial(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 82
    :cond_0
    invoke-direct {p0}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->getUtil()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getAsYouTypeFormatter(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;->clear()V

    .line 86
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->normalizeDiallableCharsOnly(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "PhoneNumberUtil.normaliz\u2026allableCharsOnly(rawText)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const-string v2, ""

    .line 104
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 87
    invoke-virtual {v0, v2}, Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;->inputDigit(C)Ljava/lang/String;

    move-result-object v2

    const-string v3, "asYouTypeFormatter.inputDigit(char)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public isValid(Ljava/lang/String;)Z
    .locals 3

    .line 96
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->getUtil()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->getUtil()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v1

    check-cast p1, Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v2}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->isValidNumber(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;)Z

    move-result p1
    :try_end_0
    .catch Lcom/google/i18n/phonenumbers/NumberParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
