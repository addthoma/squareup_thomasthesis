.class public final Lcom/squareup/text/NumberFormatter_Factory;
.super Ljava/lang/Object;
.source "NumberFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/NumberFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final maxFractionalDigitsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/text/NumberFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/text/NumberFormatter_Factory;->maxFractionalDigitsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/text/NumberFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/text/NumberFormatter_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/text/NumberFormatter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/text/NumberFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Ljava/lang/Integer;)Lcom/squareup/text/NumberFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/squareup/text/NumberFormatter;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/text/NumberFormatter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/text/NumberFormatter;-><init>(Ljavax/inject/Provider;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/text/NumberFormatter;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/text/NumberFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/text/NumberFormatter_Factory;->maxFractionalDigitsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/text/NumberFormatter_Factory;->newInstance(Ljavax/inject/Provider;Ljava/lang/Integer;)Lcom/squareup/text/NumberFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/text/NumberFormatter_Factory;->get()Lcom/squareup/text/NumberFormatter;

    move-result-object v0

    return-object v0
.end method
