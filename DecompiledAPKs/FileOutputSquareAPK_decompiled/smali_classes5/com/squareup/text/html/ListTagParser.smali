.class public Lcom/squareup/text/html/ListTagParser;
.super Ljava/lang/Object;
.source "ListTagParser.java"

# interfaces
.implements Lcom/squareup/text/html/TagParser;


# static fields
.field private static final LIST_MARGIN:I = 0x19

.field private static final LI_TAG:Ljava/lang/String; = "li"

.field private static final OL_TAG:Ljava/lang/String; = "ol"

.field private static final UL_TAG:Ljava/lang/String; = "ul"


# instance fields
.field private final densityDpi:I

.field private final lists:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final nextOlItem:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/squareup/text/html/ListTagParser;->lists:Ljava/util/Stack;

    .line 42
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/squareup/text/html/ListTagParser;->nextOlItem:Ljava/util/Stack;

    const/16 v0, 0xa0

    .line 47
    iput v0, p0, Lcom/squareup/text/html/ListTagParser;->densityDpi:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/squareup/text/html/ListTagParser;->lists:Ljava/util/Stack;

    .line 42
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/squareup/text/html/ListTagParser;->nextOlItem:Ljava/util/Stack;

    .line 52
    iput p1, p0, Lcom/squareup/text/html/ListTagParser;->densityDpi:I

    return-void
.end method

.method private handleListTag(Landroid/text/Editable;)V
    .locals 6

    .line 89
    iget-object v0, p0, Lcom/squareup/text/html/ListTagParser;->lists:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 93
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-interface {p1, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    const/16 v3, 0xa

    if-ne v1, v3, :cond_0

    return-void

    :cond_0
    const-string v1, "\n"

    .line 97
    invoke-interface {p1, v1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 99
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    .line 101
    array-length v4, v1

    if-le v4, v2, :cond_1

    .line 102
    array-length v3, v1

    sub-int/2addr v3, v2

    aget-object v1, v1, v3

    .line 103
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v3, v1

    sub-int/2addr v3, v2

    :cond_1
    const/high16 v1, 0x41c80000    # 25.0f

    .line 106
    iget v2, p0, Lcom/squareup/text/html/ListTagParser;->densityDpi:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result v1

    const-string/jumbo v2, "ul"

    .line 107
    invoke-static {v2}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    const/16 v4, 0x21

    if-eqz v2, :cond_2

    .line 108
    new-instance v0, Lcom/squareup/text/BulletListItemSpan;

    invoke-direct {v0, v1}, Lcom/squareup/text/BulletListItemSpan;-><init>(I)V

    .line 109
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {p1, v0, v3, v1, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    const-string v2, "ol"

    .line 110
    invoke-static {v2}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/squareup/text/html/ListTagParser;->nextOlItem:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 112
    iget-object v2, p0, Lcom/squareup/text/html/ListTagParser;->nextOlItem:Ljava/util/Stack;

    add-int/lit8 v5, v0, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    new-instance v2, Lcom/squareup/text/NumberedListItemSpan;

    invoke-direct {v2, v0, v1}, Lcom/squareup/text/NumberedListItemSpan;-><init>(II)V

    .line 115
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-interface {p1, v2, v3, v0, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V
    .locals 0

    const-string/jumbo p4, "ul"

    .line 69
    invoke-static {p4}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p4

    if-eqz p4, :cond_1

    if-eqz p1, :cond_0

    .line 71
    iget-object p1, p0, Lcom/squareup/text/html/ListTagParser;->lists:Ljava/util/Stack;

    invoke-virtual {p1, p2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    :cond_0
    iget-object p1, p0, Lcom/squareup/text/html/ListTagParser;->lists:Ljava/util/Stack;

    invoke-virtual {p1, p2}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string p4, "ol"

    .line 75
    invoke-static {p4}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p4

    if-eqz p4, :cond_3

    if-eqz p1, :cond_2

    .line 77
    iget-object p1, p0, Lcom/squareup/text/html/ListTagParser;->lists:Ljava/util/Stack;

    invoke-virtual {p1, p2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object p1, p0, Lcom/squareup/text/html/ListTagParser;->nextOlItem:Ljava/util/Stack;

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 80
    :cond_2
    iget-object p1, p0, Lcom/squareup/text/html/ListTagParser;->lists:Ljava/util/Stack;

    invoke-virtual {p1, p2}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    .line 81
    iget-object p1, p0, Lcom/squareup/text/html/ListTagParser;->nextOlItem:Ljava/util/Stack;

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    :cond_3
    const-string p4, "li"

    .line 83
    invoke-static {p4}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_4

    if-nez p1, :cond_4

    .line 84
    invoke-direct {p0, p3}, Lcom/squareup/text/html/ListTagParser;->handleListTag(Landroid/text/Editable;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public tagsToHandle()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 56
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const-string v1, "ol"

    .line 57
    invoke-static {v1}, Lcom/squareup/text/html/HtmlText;->openTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/text/html/HtmlText;->openTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v2, "ul"

    .line 58
    invoke-static {v2}, Lcom/squareup/text/html/HtmlText;->openTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/text/html/HtmlText;->openTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "li"

    .line 59
    invoke-static {v3}, Lcom/squareup/text/html/HtmlText;->openTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/squareup/text/html/HtmlText;->openTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-static {v1}, Lcom/squareup/text/html/HtmlText;->closeTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/text/html/HtmlText;->closeTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-static {v2}, Lcom/squareup/text/html/HtmlText;->closeTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/text/html/HtmlText;->closeTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-static {v3}, Lcom/squareup/text/html/HtmlText;->closeTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Lcom/squareup/text/html/HtmlText;->customTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/text/html/HtmlText;->closeTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
