.class public final Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;
.super Ljava/lang/Object;
.source "StoreCardResultParcelable.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStoreCardResultParcelable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StoreCardResultParcelable.kt\ncom/squareup/sdk/reader/crm/StoreCardResultParcelable\n*L\n1#1,97:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u000c2\u00020\u0001:\u0001\u000cB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;",
        "Landroid/os/Parcelable;",
        "result",
        "Lcom/squareup/sdk/reader/checkout/Card;",
        "(Lcom/squareup/sdk/reader/checkout/Card;)V",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "dest",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "reader-sdk_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;

.field private static final STORE_CARD_RESULT_EXTRA_KEY:Ljava/lang/String; = "com.squareup.sdk.reader.STORE_CARD_RESULT"


# instance fields
.field private final result:Lcom/squareup/sdk/reader/checkout/Card;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->Companion:Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;

    .line 41
    new-instance v0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Card;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Card;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;-><init>(Lcom/squareup/sdk/reader/checkout/Card;)V

    return-void
.end method

.method public static final synthetic access$getResult$p(Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;)Lcom/squareup/sdk/reader/checkout/Card;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    return-object p0
.end method

.method public static final readStoredCardFromIntent(Landroid/content/Intent;)Lcom/squareup/sdk/reader/checkout/Card;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->Companion:Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;->readStoredCardFromIntent(Landroid/content/Intent;)Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object p0

    return-object p0
.end method

.method public static final writeStoredCardToIntent(Lcom/squareup/sdk/reader/checkout/Card;Landroid/content/Intent;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->Companion:Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;->writeStoredCardToIntent(Lcom/squareup/sdk/reader/checkout/Card;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "dest"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object p2, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Card;->getBrand()Lcom/squareup/sdk/reader/checkout/Card$Brand;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Card$Brand;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24
    iget-object p2, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Card;->getLastFourDigits()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25
    iget-object p2, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Card;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 26
    iget-object p2, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Card;->getCardholderName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 27
    new-instance p2, Lkotlin/ranges/IntRange;

    const/4 v0, 0x1

    const/16 v1, 0xc

    invoke-direct {p2, v0, v1}, Lkotlin/ranges/IntRange;-><init>(II)V

    iget-object v1, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Card;->getExpirationMonth()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v1}, Lkotlin/ranges/IntRange;->contains(I)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 28
    iget-object p2, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Card;->getExpirationMonth()Ljava/lang/Integer;

    move-result-object p2

    if-nez p2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v0, "result.expirationMonth!!"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    iget-object p2, p0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Card;->getExpirationYear()Ljava/lang/Integer;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v0, "result.expirationYear!!"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    .line 27
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Check failed."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
