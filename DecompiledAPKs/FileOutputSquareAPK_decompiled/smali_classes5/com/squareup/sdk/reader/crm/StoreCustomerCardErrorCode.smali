.class public final enum Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;
.super Ljava/lang/Enum;
.source "StoreCustomerCardErrorCode.java"

# interfaces
.implements Lcom/squareup/sdk/reader/core/ErrorCode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;",
        ">;",
        "Lcom/squareup/sdk/reader/core/ErrorCode;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

.field public static final enum CANCELED:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

.field public static final enum INVALID_CUSTOMER_ID:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

.field public static final enum NO_NETWORK:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

.field public static final enum SDK_NOT_AUTHORIZED:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

.field public static final enum USAGE_ERROR:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 13
    new-instance v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    const/4 v1, 0x0

    const-string v2, "CANCELED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->CANCELED:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    .line 16
    new-instance v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    const/4 v2, 0x1

    const-string v3, "NO_NETWORK"

    invoke-direct {v0, v3, v2}, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->NO_NETWORK:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    .line 19
    new-instance v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    const/4 v3, 0x2

    const-string v4, "SDK_NOT_AUTHORIZED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->SDK_NOT_AUTHORIZED:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    .line 22
    new-instance v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    const/4 v4, 0x3

    const-string v5, "INVALID_CUSTOMER_ID"

    invoke-direct {v0, v5, v4}, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->INVALID_CUSTOMER_ID:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    .line 29
    new-instance v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    const/4 v5, 0x4

    const-string v6, "USAGE_ERROR"

    invoke-direct {v0, v6, v5}, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    .line 7
    sget-object v6, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->CANCELED:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->NO_NETWORK:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->SDK_NOT_AUTHORIZED:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->INVALID_CUSTOMER_ID:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->$VALUES:[Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->$VALUES:[Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    invoke-virtual {v0}, [Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    return-object v0
.end method


# virtual methods
.method public isUsageError()Z
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/crm/StoreCustomerCardErrorCode;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
