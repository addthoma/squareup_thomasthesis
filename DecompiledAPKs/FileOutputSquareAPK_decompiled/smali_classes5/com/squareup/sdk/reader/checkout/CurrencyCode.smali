.class public final enum Lcom/squareup/sdk/reader/checkout/CurrencyCode;
.super Ljava/lang/Enum;
.source "CurrencyCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/reader/checkout/CurrencyCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum AED:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum AFN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ALL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum AMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ANG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum AOA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ARS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum AUD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum AWG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum AZN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BAM:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BDT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BGN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BHD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BIF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BOB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BOV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BRL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BSD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BTN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BWP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BYR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum BZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CDF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CHE:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CHF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CHW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CLF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CLP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CNY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum COP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum COU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CRC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CUC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CUP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CVE:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum CZK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum DJF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum DKK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum DOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum DZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum EGP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ERN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ETB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum EUR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum FJD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum FKP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum GBP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum GEL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum GHS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum GIP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum GMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum GNF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum GTQ:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum GYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum HKD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum HNL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum HRK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum HTG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum HUF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum IDR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ILS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum INR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum IQD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum IRR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ISK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum JMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum JOD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum JPY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KES:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KGS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KHR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KMF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KPW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KRW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KWD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum KZT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum LAK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum LBP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum LKR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum LRD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum LSL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum LTL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum LVL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum LYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MDL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MGA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MKD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MMK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MNT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MRO:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MUR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MVR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MWK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MXN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MXV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MYR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum MZN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum NAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum NGN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum NIO:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum NOK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum NPR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum NZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum OMR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum PAB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum PEN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum PGK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum PHP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum PKR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum PLN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum PYG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum QAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum RON:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum RSD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum RUB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum RWF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SCR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SDG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SEK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SGD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SHP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SLL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SOS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SRD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SSP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum STD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SVC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SYP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum SZL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum THB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum TJS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum TMT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum TND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum TOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum TRY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum TTD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum TWD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum TZS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum UAH:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum UGX:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum USD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum USN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum USS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum UYI:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum UYU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum UZS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum VEF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum VND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum VUV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum WST:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XAF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XAG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XAU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XBA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XBB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XBC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XCD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XDR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XOF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XPD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XPF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XPT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XTS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum XXX:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum YER:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ZAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ZMK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field public static final enum ZMW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v1, 0x0

    const-string v2, "AED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AED:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 12
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v2, 0x1

    const-string v3, "AFN"

    invoke-direct {v0, v3, v2}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AFN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 13
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v3, 0x2

    const-string v4, "ALL"

    invoke-direct {v0, v4, v3}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ALL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 14
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v4, 0x3

    const-string v5, "AMD"

    invoke-direct {v0, v5, v4}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 15
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v5, 0x4

    const-string v6, "ANG"

    invoke-direct {v0, v6, v5}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ANG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 16
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v6, 0x5

    const-string v7, "AOA"

    invoke-direct {v0, v7, v6}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AOA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 17
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v7, 0x6

    const-string v8, "ARS"

    invoke-direct {v0, v8, v7}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ARS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 18
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v8, 0x7

    const-string v9, "AUD"

    invoke-direct {v0, v9, v8}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AUD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 19
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v9, 0x8

    const-string v10, "AWG"

    invoke-direct {v0, v10, v9}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AWG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 20
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v10, 0x9

    const-string v11, "AZN"

    invoke-direct {v0, v11, v10}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AZN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 21
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v11, 0xa

    const-string v12, "BAM"

    invoke-direct {v0, v12, v11}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BAM:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 22
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v12, 0xb

    const-string v13, "BBD"

    invoke-direct {v0, v13, v12}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 23
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v13, 0xc

    const-string v14, "BDT"

    invoke-direct {v0, v14, v13}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BDT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 24
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v14, 0xd

    const-string v15, "BGN"

    invoke-direct {v0, v15, v14}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BGN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 25
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v15, 0xe

    const-string v14, "BHD"

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BHD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 26
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BIF"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BIF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 27
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BMD"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 28
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BND"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 29
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BOB"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BOB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 30
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BOV"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BOV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 31
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BRL"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BRL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 32
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BSD"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BSD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 33
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BTN"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BTN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 34
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BWP"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BWP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 35
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BYR"

    const/16 v15, 0x18

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BYR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 36
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "BZD"

    const/16 v15, 0x19

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 37
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CAD"

    const/16 v15, 0x1a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 38
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CDF"

    const/16 v15, 0x1b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CDF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 39
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CHE"

    const/16 v15, 0x1c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CHE:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 40
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CHF"

    const/16 v15, 0x1d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CHF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 41
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CHW"

    const/16 v15, 0x1e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CHW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 42
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CLF"

    const/16 v15, 0x1f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CLF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 43
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CLP"

    const/16 v15, 0x20

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CLP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 44
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CNY"

    const/16 v15, 0x21

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CNY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 45
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "COP"

    const/16 v15, 0x22

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->COP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 46
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "COU"

    const/16 v15, 0x23

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->COU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 47
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CRC"

    const/16 v15, 0x24

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CRC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 48
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CUC"

    const/16 v15, 0x25

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CUC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 49
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CUP"

    const/16 v15, 0x26

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CUP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 50
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CVE"

    const/16 v15, 0x27

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CVE:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 51
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "CZK"

    const/16 v15, 0x28

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CZK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 52
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "DJF"

    const/16 v15, 0x29

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->DJF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 53
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "DKK"

    const/16 v15, 0x2a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->DKK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 54
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "DOP"

    const/16 v15, 0x2b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->DOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 55
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "DZD"

    const/16 v15, 0x2c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->DZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 56
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "EGP"

    const/16 v15, 0x2d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->EGP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 57
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "ERN"

    const/16 v15, 0x2e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ERN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 58
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "ETB"

    const/16 v15, 0x2f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ETB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 59
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "EUR"

    const/16 v15, 0x30

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->EUR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 60
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "FJD"

    const/16 v15, 0x31

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->FJD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 61
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "FKP"

    const/16 v15, 0x32

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->FKP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 62
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "GBP"

    const/16 v15, 0x33

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GBP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 63
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "GEL"

    const/16 v15, 0x34

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GEL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 64
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "GHS"

    const/16 v15, 0x35

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GHS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 65
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "GIP"

    const/16 v15, 0x36

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GIP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 66
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "GMD"

    const/16 v15, 0x37

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 67
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "GNF"

    const/16 v15, 0x38

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GNF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 68
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "GTQ"

    const/16 v15, 0x39

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GTQ:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 69
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "GYD"

    const/16 v15, 0x3a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 70
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "HKD"

    const/16 v15, 0x3b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HKD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 71
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "HNL"

    const/16 v15, 0x3c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HNL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 72
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "HRK"

    const/16 v15, 0x3d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HRK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 73
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "HTG"

    const/16 v15, 0x3e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HTG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 74
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "HUF"

    const/16 v15, 0x3f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HUF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 75
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "IDR"

    const/16 v15, 0x40

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->IDR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 76
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "ILS"

    const/16 v15, 0x41

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ILS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 77
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "INR"

    const/16 v15, 0x42

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->INR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 78
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "IQD"

    const/16 v15, 0x43

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->IQD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 79
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "IRR"

    const/16 v15, 0x44

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->IRR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 80
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "ISK"

    const/16 v15, 0x45

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ISK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 81
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "JMD"

    const/16 v15, 0x46

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->JMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 82
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "JOD"

    const/16 v15, 0x47

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->JOD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 83
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "JPY"

    const/16 v15, 0x48

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->JPY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 84
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KES"

    const/16 v15, 0x49

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KES:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 85
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KGS"

    const/16 v15, 0x4a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KGS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 86
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KHR"

    const/16 v15, 0x4b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KHR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 87
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KMF"

    const/16 v15, 0x4c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KMF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 88
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KPW"

    const/16 v15, 0x4d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KPW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 89
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KRW"

    const/16 v15, 0x4e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KRW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 90
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KWD"

    const/16 v15, 0x4f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KWD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 91
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KYD"

    const/16 v15, 0x50

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 92
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "KZT"

    const/16 v15, 0x51

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KZT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 93
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "LAK"

    const/16 v15, 0x52

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LAK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 94
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "LBP"

    const/16 v15, 0x53

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LBP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 95
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "LKR"

    const/16 v15, 0x54

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LKR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 96
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "LRD"

    const/16 v15, 0x55

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LRD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 97
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "LSL"

    const/16 v15, 0x56

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LSL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 98
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "LTL"

    const/16 v15, 0x57

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LTL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 99
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "LVL"

    const/16 v15, 0x58

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LVL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 100
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "LYD"

    const/16 v15, 0x59

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 101
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MAD"

    const/16 v15, 0x5a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 102
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MDL"

    const/16 v15, 0x5b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MDL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 103
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MGA"

    const/16 v15, 0x5c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MGA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 104
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MKD"

    const/16 v15, 0x5d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MKD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 105
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MMK"

    const/16 v15, 0x5e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MMK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 106
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MNT"

    const/16 v15, 0x5f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MNT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 107
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MOP"

    const/16 v15, 0x60

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 108
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MRO"

    const/16 v15, 0x61

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MRO:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 109
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MUR"

    const/16 v15, 0x62

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MUR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 110
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MVR"

    const/16 v15, 0x63

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MVR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 111
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MWK"

    const/16 v15, 0x64

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MWK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 112
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MXN"

    const/16 v15, 0x65

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MXN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 113
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MXV"

    const/16 v15, 0x66

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MXV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 114
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MYR"

    const/16 v15, 0x67

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MYR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 115
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "MZN"

    const/16 v15, 0x68

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MZN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 116
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "NAD"

    const/16 v15, 0x69

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 117
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "NGN"

    const/16 v15, 0x6a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NGN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 118
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "NIO"

    const/16 v15, 0x6b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NIO:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 119
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "NOK"

    const/16 v15, 0x6c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NOK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 120
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "NPR"

    const/16 v15, 0x6d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NPR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 121
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "NZD"

    const/16 v15, 0x6e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 122
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "OMR"

    const/16 v15, 0x6f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->OMR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 123
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "PAB"

    const/16 v15, 0x70

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PAB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 124
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "PEN"

    const/16 v15, 0x71

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PEN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 125
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "PGK"

    const/16 v15, 0x72

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PGK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 126
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "PHP"

    const/16 v15, 0x73

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PHP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 127
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "PKR"

    const/16 v15, 0x74

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PKR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 128
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "PLN"

    const/16 v15, 0x75

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PLN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 129
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "PYG"

    const/16 v15, 0x76

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PYG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 130
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "QAR"

    const/16 v15, 0x77

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->QAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 131
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "RON"

    const/16 v15, 0x78

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->RON:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 132
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "RSD"

    const/16 v15, 0x79

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->RSD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 133
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "RUB"

    const/16 v15, 0x7a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->RUB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 134
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "RWF"

    const/16 v15, 0x7b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->RWF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 135
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SAR"

    const/16 v15, 0x7c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 136
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SBD"

    const/16 v15, 0x7d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 137
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SCR"

    const/16 v15, 0x7e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SCR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 138
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SDG"

    const/16 v15, 0x7f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SDG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 139
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SEK"

    const/16 v15, 0x80

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SEK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 140
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SGD"

    const/16 v15, 0x81

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SGD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 141
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SHP"

    const/16 v15, 0x82

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SHP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 142
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SLL"

    const/16 v15, 0x83

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SLL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 143
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SOS"

    const/16 v15, 0x84

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SOS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 144
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SRD"

    const/16 v15, 0x85

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SRD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 145
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SSP"

    const/16 v15, 0x86

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SSP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 146
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "STD"

    const/16 v15, 0x87

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->STD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 147
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SVC"

    const/16 v15, 0x88

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SVC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 148
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SYP"

    const/16 v15, 0x89

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SYP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 149
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "SZL"

    const/16 v15, 0x8a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SZL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 150
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "THB"

    const/16 v15, 0x8b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->THB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 151
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "TJS"

    const/16 v15, 0x8c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TJS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 152
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "TMT"

    const/16 v15, 0x8d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TMT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 153
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "TND"

    const/16 v15, 0x8e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 154
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "TOP"

    const/16 v15, 0x8f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 155
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "TRY"

    const/16 v15, 0x90

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TRY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 156
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "TTD"

    const/16 v15, 0x91

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TTD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 157
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "TWD"

    const/16 v15, 0x92

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TWD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 158
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "TZS"

    const/16 v15, 0x93

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TZS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 159
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "UAH"

    const/16 v15, 0x94

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UAH:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 160
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "UGX"

    const/16 v15, 0x95

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UGX:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 161
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "USD"

    const/16 v15, 0x96

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->USD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 162
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "USN"

    const/16 v15, 0x97

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->USN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 163
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "USS"

    const/16 v15, 0x98

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->USS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 164
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "UYI"

    const/16 v15, 0x99

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UYI:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 165
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "UYU"

    const/16 v15, 0x9a

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UYU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 166
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "UZS"

    const/16 v15, 0x9b

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UZS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 167
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "VEF"

    const/16 v15, 0x9c

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->VEF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 168
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "VND"

    const/16 v15, 0x9d

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->VND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 169
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "VUV"

    const/16 v15, 0x9e

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->VUV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 170
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "WST"

    const/16 v15, 0x9f

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->WST:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 171
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XAF"

    const/16 v15, 0xa0

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XAF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 172
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XAG"

    const/16 v15, 0xa1

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XAG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 173
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XAU"

    const/16 v15, 0xa2

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XAU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 174
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XBA"

    const/16 v15, 0xa3

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XBA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 175
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XBB"

    const/16 v15, 0xa4

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XBB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 176
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XBC"

    const/16 v15, 0xa5

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XBC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 177
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XBD"

    const/16 v15, 0xa6

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 178
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XCD"

    const/16 v15, 0xa7

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XCD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 179
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XDR"

    const/16 v15, 0xa8

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XDR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 180
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XOF"

    const/16 v15, 0xa9

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XOF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 181
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XPD"

    const/16 v15, 0xaa

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XPD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 182
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XPF"

    const/16 v15, 0xab

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XPF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 183
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XPT"

    const/16 v15, 0xac

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XPT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 184
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XTS"

    const/16 v15, 0xad

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XTS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 185
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "XXX"

    const/16 v15, 0xae

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XXX:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 186
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "YER"

    const/16 v15, 0xaf

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->YER:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 187
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "ZAR"

    const/16 v15, 0xb0

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ZAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 188
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "ZMK"

    const/16 v15, 0xb1

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ZMK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 189
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string v14, "ZMW"

    const/16 v15, 0xb2

    invoke-direct {v0, v14, v15}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ZMW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v0, 0xb3

    new-array v0, v0, [Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 9
    sget-object v14, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AED:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AFN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ALL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ANG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AOA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ARS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AUD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AWG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->AZN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BAM:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BDT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BGN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BHD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BIF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BOB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BOV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BRL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BSD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BTN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BWP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BYR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->BZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CDF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CHE:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CHF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CHW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CLF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CLP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CNY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->COP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->COU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CRC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CUC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CUP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CVE:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->CZK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->DJF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->DKK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->DOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->DZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->EGP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ERN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ETB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->EUR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->FJD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->FKP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GBP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GEL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GHS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GIP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GNF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GTQ:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->GYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HKD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HNL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HRK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HTG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->HUF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->IDR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ILS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->INR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->IQD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->IRR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ISK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->JMD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->JOD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->JPY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KES:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KGS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KHR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KMF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KPW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KRW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KWD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->KZT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LAK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LBP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LKR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LRD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LSL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LTL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LVL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->LYD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MDL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MGA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MKD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MMK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MNT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MRO:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MUR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MVR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MWK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MXN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MXV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MYR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->MZN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NAD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NGN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NIO:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NOK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NPR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->NZD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->OMR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PAB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PEN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PGK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PHP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PKR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PLN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->PYG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->QAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->RON:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->RSD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->RUB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->RWF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SCR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SDG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SEK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SGD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SHP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SLL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SOS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SRD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SSP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->STD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SVC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SYP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->SZL:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->THB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TJS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TMT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TOP:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TRY:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TTD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TWD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->TZS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UAH:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UGX:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->USD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->USN:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->USS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UYI:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UYU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->UZS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->VEF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->VND:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->VUV:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->WST:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XAF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XAG:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XAU:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XBA:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XBB:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XBC:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XBD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XCD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XDR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XOF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XPD:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XPF:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XPT:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XTS:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->XXX:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->YER:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ZAR:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ZMK:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->ZMW:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->$VALUES:[Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static current()Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 203
    invoke-static {}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->currentCurrency()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->$VALUES:[Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-virtual {v0}, [Lcom/squareup/sdk/reader/checkout/CurrencyCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-object v0
.end method
