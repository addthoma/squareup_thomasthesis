.class public Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;
.super Ljava/lang/Object;
.source "CardReceiptDetails.java"


# instance fields
.field private final applicationIdentifier:Ljava/lang/String;

.field private final applicationPreferredName:Ljava/lang/String;

.field private final authorizationCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->authorizationCode:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationIdentifier:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationPreferredName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_5

    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 48
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    .line 50
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->authorizationCode:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->authorizationCode:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 53
    :cond_2
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationIdentifier:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationIdentifier:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 56
    :cond_3
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationPreferredName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationPreferredName:Ljava/lang/String;

    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    return v1

    :cond_4
    return v0

    :cond_5
    :goto_0
    return v1
.end method

.method public getApplicationIdentifier()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public getApplicationPreferredName()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationPreferredName:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthorizationCode()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->authorizationCode:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 64
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->authorizationCode:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 65
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationIdentifier:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 66
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationPreferredName:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 67
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardReceiptDetails{authorizationCode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->authorizationCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", applicationIdentifier=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationIdentifier:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", applicationPreferredName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->applicationPreferredName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
