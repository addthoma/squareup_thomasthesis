.class public final enum Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;
.super Ljava/lang/Enum;
.source "CheckoutErrorCode.java"

# interfaces
.implements Lcom/squareup/sdk/reader/core/ErrorCode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;",
        ">;",
        "Lcom/squareup/sdk/reader/core/ErrorCode;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

.field public static final enum CANCELED:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

.field public static final enum SDK_NOT_AUTHORIZED:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

.field public static final enum USAGE_ERROR:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 14
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    const/4 v1, 0x0

    const-string v2, "CANCELED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->CANCELED:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    .line 17
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    const/4 v2, 0x1

    const-string v3, "SDK_NOT_AUTHORIZED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->SDK_NOT_AUTHORIZED:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    .line 24
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    const/4 v3, 0x2

    const-string v4, "USAGE_ERROR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    .line 11
    sget-object v4, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->CANCELED:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->SDK_NOT_AUTHORIZED:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->$VALUES:[Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->$VALUES:[Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    invoke-virtual {v0}, [Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    return-object v0
.end method


# virtual methods
.method public isUsageError()Z
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/checkout/CheckoutErrorCode;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
