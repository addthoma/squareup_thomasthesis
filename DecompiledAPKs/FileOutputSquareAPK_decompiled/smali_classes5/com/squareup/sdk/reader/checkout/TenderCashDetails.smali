.class public final Lcom/squareup/sdk/reader/checkout/TenderCashDetails;
.super Ljava/lang/Object;
.source "TenderCashDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;
    }
.end annotation


# instance fields
.field private final buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private final changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)V
    .locals 3

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v0

    .line 29
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 37
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 38
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-void

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "buyerTenderedMoney and changeBackMoney must have the same currency. The buyerTenderedMoney currency is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " but the changeBackMoney currency is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;Lcom/squareup/sdk/reader/checkout/TenderCashDetails$1;)V
    .locals 0

    .line 10
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;-><init>(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public static newBuilder(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;
    .locals 2

    const-string v0, "buyerTenderedMoney"

    .line 20
    invoke-static {p0, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/TenderCashDetails$1;)V

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails$1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 68
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    .line 70
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 71
    :cond_2
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2, p1}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    :cond_3
    return v0

    :cond_4
    :goto_0
    return v1
.end method

.method public getBuyerTenderedMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public getChangeBackMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 78
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TenderCashDetails{buyerTenderedMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", changeBackMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
