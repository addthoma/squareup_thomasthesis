.class public final Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;
.super Ljava/lang/Object;
.source "ReaderSdkConversionUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReaderSdkConversionUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReaderSdkConversionUtils.kt\ncom/squareup/sdk/reader/ReaderSdkConversionUtils\n*L\n1#1,135:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u001a\u000e\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u000e\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b\u001a\u000e\u0010\u0008\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e\u001a\u000e\u0010\u0008\u001a\u00020\u000f2\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u000e\u0010\u0008\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012\u001a\u000e\u0010\u0008\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015\u001a\u000e\u0010\u0008\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018\u001a\u0018\u0010\u0008\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u0015\u001a\u0014\u0010\u0008\u001a\u00020\u001a2\n\u0010\u001b\u001a\u00060\u001cj\u0002`\u001dH\u0002\u001a\u0012\u0010\u0008\u001a\u00020\u001e2\n\u0010\u001f\u001a\u00060 j\u0002`!\u001a\u000e\u0010\u0008\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u0013\u001a\u000e\u0010\u0008\u001a\u00020\u00182\u0006\u0010\u0017\u001a\u00020\u0016\u001a\u000e\u0010\"\u001a\u00020\t2\u0006\u0010#\u001a\u00020$\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003*\u000c\u0008\u0002\u0010%\"\u00020\u001c2\u00020\u001c*\u000c\u0008\u0002\u0010&\"\u00020 2\u00020 \u00a8\u0006\'"
    }
    d2 = {
        "ISO_8601",
        "Ljava/text/SimpleDateFormat;",
        "getISO_8601",
        "()Ljava/text/SimpleDateFormat;",
        "builderFrom",
        "Lcom/squareup/sdk/reader/checkout/Card$Builder;",
        "card",
        "Lcom/squareup/protos/client/bills/CardTender$Card;",
        "from",
        "Ljava/util/Date;",
        "date",
        "Lcom/squareup/protos/client/ISO8601Date;",
        "Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;",
        "cardTender",
        "Lcom/squareup/protos/client/bills/CardTender;",
        "Lcom/squareup/sdk/reader/checkout/Card;",
        "Lcom/squareup/sdk/reader/checkout/Tender$Type;",
        "type",
        "Lcom/squareup/protos/client/bills/Tender$Type;",
        "Lcom/squareup/sdk/reader/checkout/CurrencyCode;",
        "code",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "Lcom/squareup/sdk/reader/checkout/Money;",
        "money",
        "Lcom/squareup/protos/common/Money;",
        "defaultCurrencyCode",
        "Lcom/squareup/sdk/reader/checkout/Card$Brand;",
        "brand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "Lcom/squareup/sdk/reader/ProtoBrand;",
        "Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;",
        "entryMethod",
        "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        "Lcom/squareup/sdk/reader/ProtoEntryMethod;",
        "parseISO8601Date",
        "dateString",
        "",
        "ProtoBrand",
        "ProtoEntryMethod",
        "reader-sdk_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ISO_8601:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 34
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->ISO_8601:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public static final builderFrom(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/sdk/reader/checkout/Card$Builder;
    .locals 2

    const-string v0, "card"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v1, "card.brand"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/sdk/reader/checkout/Card$Brand;

    move-result-object v0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/squareup/sdk/reader/checkout/Card;->newCardBuilder(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    move-result-object p0

    const-string v0, "Card.newCardBuilder(from\u2026.brand), card.pan_suffix)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final from(Lcom/squareup/sdk/reader/checkout/CurrencyCode;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    const-string v0, "code"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/protos/common/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p0

    return-object p0
.end method

.method public static final from(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/protos/common/Money;
    .locals 3

    const-string v0, "money"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/Money;->getAmount()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p0

    const-string v2, "money.currencyCode"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/sdk/reader/checkout/CurrencyCode;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method

.method private static final from(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/sdk/reader/checkout/Card$Brand;
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 117
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->OTHER_BRAND:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 116
    :pswitch_1
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->VISA:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 115
    :pswitch_2
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->OTHER_BRAND:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 114
    :pswitch_3
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->SQUARE_GIFT_CARD:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 113
    :pswitch_4
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->OTHER_BRAND:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 111
    :pswitch_5
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->MASTERCARD:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 110
    :pswitch_6
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->JCB:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 109
    :pswitch_7
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->INTERAC:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 108
    :pswitch_8
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->OTHER_BRAND:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 106
    :pswitch_9
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->EFTPOS:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 105
    :pswitch_a
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 104
    :pswitch_b
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->DISCOVER:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 103
    :pswitch_c
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 102
    :pswitch_d
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->OTHER_BRAND:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 101
    :pswitch_e
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    goto :goto_0

    .line 100
    :pswitch_f
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Card$Brand;->OTHER_BRAND:Lcom/squareup/sdk/reader/checkout/Card$Brand;

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final from(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/sdk/reader/checkout/Card;
    .locals 3

    const-string v0, "card"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Card;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v2, "card.brand"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/sdk/reader/checkout/Card$Brand;

    move-result-object v1

    iget-object p0, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    invoke-direct {v0, v1, p0}, Lcom/squareup/sdk/reader/checkout/Card;-><init>(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final from(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;
    .locals 4

    const-string v0, "cardTender"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 131
    :goto_0
    iget-object v3, p0, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    if-eqz v3, :cond_1

    iget-object v3, v3, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, v2

    .line 132
    :goto_1
    iget-object p0, p0, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    if-eqz p0, :cond_2

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    .line 129
    :cond_2
    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final from(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 1

    const-string v0, "code"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p0

    return-object p0
.end method

.method public static final from(Lcom/squareup/protos/common/Money;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 4

    const-string v0, "money"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Money;

    iget-object v1, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v2, "money.amount"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v3, "money.currency_code"

    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p0

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    return-object v0
.end method

.method public static final from(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 2

    const-string v0, "defaultCurrencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    .line 79
    invoke-static {p0}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/Money;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    .line 80
    :cond_0
    new-instance p0, Lcom/squareup/sdk/reader/checkout/Money;

    const-wide/16 v0, 0x0

    invoke-static {p1}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    :goto_0
    return-object p0
.end method

.method public static final from(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/sdk/reader/checkout/Tender$Type;
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$Type;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 124
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Tender$Type;->OTHER:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    goto :goto_0

    .line 123
    :cond_0
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CASH:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    goto :goto_0

    .line 122
    :cond_1
    sget-object p0, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CARD:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    :goto_0
    return-object p0
.end method

.method public static final from(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;
    .locals 1

    const-string v0, "entryMethod"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 60
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Card on file transactions are not supported in Reader SDK."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 57
    :pswitch_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "The entry method for the payment should never be unknown."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 56
    :pswitch_2
    sget-object p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->CONTACTLESS:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    goto :goto_0

    .line 55
    :pswitch_3
    sget-object p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->CHIP:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    goto :goto_0

    .line 54
    :pswitch_4
    sget-object p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->SWIPE:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    goto :goto_0

    .line 53
    :pswitch_5
    sget-object p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->MANUALLY_ENTERED:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final from(Lcom/squareup/protos/client/ISO8601Date;)Ljava/util/Date;
    .locals 1

    const-string v0, "date"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    const-string v0, "date.date_string"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->parseISO8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method

.method public static final getISO_8601()Ljava/text/SimpleDateFormat;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->ISO_8601:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method public static final parseISO8601Date(Ljava/lang/String;)Ljava/util/Date;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "dateString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Z"

    const-string v3, "-0000"

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    .line 40
    invoke-static/range {v1 .. v6}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 43
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    .line 45
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3a

    if-ne v1, v2, :cond_2

    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const-string v3, "null cannot be cast to non-null type java.lang.String"

    if-eqz p0, :cond_1

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz p0, :cond_0

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 49
    :cond_2
    :goto_0
    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->ISO_8601:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    if-nez p0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    return-object p0
.end method
