.class public final enum Lcom/squareup/sdk/pos/TransactionRequest$TenderType;
.super Ljava/lang/Enum;
.source "TransactionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/TransactionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TenderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/pos/TransactionRequest$TenderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

.field public static final enum CARD_FROM_READER:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

.field public static final enum CARD_ON_FILE:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

.field public static final enum CASH:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

.field public static final enum KEYED_IN_CARD:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

.field public static final enum OTHER:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

.field public static final enum SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;


# instance fields
.field apiExtraName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 511
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    const/4 v1, 0x0

    const-string v2, "CARD_FROM_READER"

    const-string v3, "com.squareup.pos.TENDER_CARD_FROM_READER"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->CARD_FROM_READER:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    .line 514
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    const/4 v2, 0x1

    const-string v3, "CARD_ON_FILE"

    const-string v4, "com.squareup.pos.TENDER_CARD_ON_FILE"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->CARD_ON_FILE:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    .line 517
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    const/4 v3, 0x2

    const-string v4, "SQUARE_GIFT_CARD"

    const-string v5, "com.squareup.pos.TENDER_GIFT_CARD"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    .line 520
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    const/4 v4, 0x3

    const-string v5, "KEYED_IN_CARD"

    const-string v6, "com.squareup.pos.TENDER_KEYED_IN_CARD"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->KEYED_IN_CARD:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    .line 523
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    const/4 v5, 0x4

    const-string v6, "CASH"

    const-string v7, "com.squareup.pos.TENDER_CASH"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->CASH:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    .line 529
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    const/4 v6, 0x5

    const-string v7, "OTHER"

    const-string v8, "com.squareup.pos.TENDER_OTHER"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->OTHER:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    .line 508
    sget-object v7, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->CARD_FROM_READER:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->CARD_ON_FILE:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->KEYED_IN_CARD:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->CASH:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->OTHER:Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->$VALUES:[Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 533
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 534
    iput-object p3, p0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->apiExtraName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$TenderType;
    .locals 1

    .line 508
    const-class v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/pos/TransactionRequest$TenderType;
    .locals 1

    .line 508
    sget-object v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->$VALUES:[Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    invoke-virtual {v0}, [Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    return-object v0
.end method
