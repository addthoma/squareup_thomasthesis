.class public final enum Lcom/squareup/sdk/pos/transaction/Card$Brand;
.super Ljava/lang/Enum;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Brand"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/pos/transaction/Card$Brand;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum AMERICAN_EXPRESS:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum CHINA_UNIONPAY:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum DISCOVER:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum JCB:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum MASTERCARD:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum OTHER_BRAND:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field public static final enum VISA:Lcom/squareup/sdk/pos/transaction/Card$Brand;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 28
    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/4 v1, 0x0

    const-string v2, "VISA"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->VISA:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/4 v2, 0x1

    const-string v3, "MASTERCARD"

    invoke-direct {v0, v3, v2}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->MASTERCARD:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/4 v3, 0x2

    const-string v4, "AMERICAN_EXPRESS"

    invoke-direct {v0, v4, v3}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/4 v4, 0x3

    const-string v5, "DISCOVER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->DISCOVER:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/4 v5, 0x4

    const-string v6, "DISCOVER_DINERS"

    invoke-direct {v0, v6, v5}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/4 v6, 0x5

    const-string v7, "JCB"

    invoke-direct {v0, v7, v6}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->JCB:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/4 v7, 0x6

    const-string v8, "CHINA_UNIONPAY"

    invoke-direct {v0, v8, v7}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    .line 29
    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/4 v8, 0x7

    const-string v9, "SQUARE_GIFT_CARD"

    invoke-direct {v0, v9, v8}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/16 v9, 0x8

    const-string v10, "OTHER_BRAND"

    invoke-direct {v0, v10, v9}, Lcom/squareup/sdk/pos/transaction/Card$Brand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->OTHER_BRAND:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/sdk/pos/transaction/Card$Brand;

    .line 27
    sget-object v10, Lcom/squareup/sdk/pos/transaction/Card$Brand;->VISA:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Card$Brand;->MASTERCARD:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Card$Brand;->DISCOVER:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Card$Brand;->JCB:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Card$Brand;->SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Card$Brand;->OTHER_BRAND:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->$VALUES:[Lcom/squareup/sdk/pos/transaction/Card$Brand;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Card$Brand;
    .locals 1

    .line 27
    const-class v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/pos/transaction/Card$Brand;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/pos/transaction/Card$Brand;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/sdk/pos/transaction/Card$Brand;->$VALUES:[Lcom/squareup/sdk/pos/transaction/Card$Brand;

    invoke-virtual {v0}, [Lcom/squareup/sdk/pos/transaction/Card$Brand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/pos/transaction/Card$Brand;

    return-object v0
.end method
