.class final Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;
.super Lcom/squareup/sdk/pos/transaction/Order$Builder;
.source "$$AutoValue_Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

.field private totalTaxMoney:Lcom/squareup/sdk/pos/transaction/Money;

.field private totalTipMoney:Lcom/squareup/sdk/pos/transaction/Money;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/Order$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/pos/transaction/Order;
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    const-string v1, ""

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " totalMoney"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalTipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " totalTipMoney"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalTaxMoney:Lcom/squareup/sdk/pos/transaction/Money;

    if-nez v0, :cond_2

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " totalTaxMoney"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 120
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Order;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalTipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    iget-object v3, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalTaxMoney:Lcom/squareup/sdk/pos/transaction/Money;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/sdk/pos/transaction/AutoValue_Order;-><init>(Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;)V

    return-object v0

    .line 121
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public totalMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Order$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 89
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object p0

    .line 87
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null totalMoney"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public totalTaxMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Order$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 105
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalTaxMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object p0

    .line 103
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null totalTaxMoney"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public totalTipMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Order$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 97
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;->totalTipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object p0

    .line 95
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null totalTipMoney"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
