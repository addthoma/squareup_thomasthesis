.class final Lcom/squareup/sdk/pos/transaction/AutoValue_Order;
.super Lcom/squareup/sdk/pos/transaction/$AutoValue_Order;
.source "AutoValue_Order.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/sdk/pos/transaction/AutoValue_Order;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Order$1;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Order$1;-><init>()V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Order;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order;-><init>(Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Order;->totalMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Order;->totalTipMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Order;->totalTaxMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
