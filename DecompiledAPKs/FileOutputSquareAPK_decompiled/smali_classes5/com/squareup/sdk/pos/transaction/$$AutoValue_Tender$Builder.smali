.class final Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;
.super Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.source "$$AutoValue_Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

.field private cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

.field private clientId:Ljava/lang/String;

.field private createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

.field private customerId:Ljava/lang/String;

.field private serverId:Ljava/lang/String;

.field private tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

.field private totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

.field private type:Lcom/squareup/sdk/pos/transaction/Tender$Type;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 173
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/Tender$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/pos/transaction/Tender;
    .locals 12

    .line 238
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->clientId:Ljava/lang/String;

    const-string v1, ""

    if-nez v0, :cond_0

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " clientId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    if-nez v0, :cond_1

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " createdAt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 244
    :cond_1
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    if-nez v0, :cond_2

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " totalMoney"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    if-nez v0, :cond_3

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " tipMoney"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 250
    :cond_3
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->type:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    if-nez v0, :cond_4

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 253
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 256
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;

    iget-object v3, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->clientId:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->serverId:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    iget-object v6, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    iget-object v7, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    iget-object v8, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->customerId:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->type:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    iget-object v10, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    iget-object v11, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/Tender$Type;Lcom/squareup/sdk/pos/transaction/TenderCardDetails;Lcom/squareup/sdk/pos/transaction/TenderCashDetails;)V

    return-object v0

    .line 254
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cardDetails(Lcom/squareup/sdk/pos/transaction/TenderCardDetails;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    return-object p0
.end method

.method public cashDetails(Lcom/squareup/sdk/pos/transaction/TenderCashDetails;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    return-object p0
.end method

.method public clientId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 180
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->clientId:Ljava/lang/String;

    return-object p0

    .line 178
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null clientId"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public createdAt(Lcom/squareup/sdk/pos/transaction/DateTime;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 193
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    return-object p0

    .line 191
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null createdAt"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public customerId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->customerId:Ljava/lang/String;

    return-object p0
.end method

.method public serverId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->serverId:Ljava/lang/String;

    return-object p0
.end method

.method public tipMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 209
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object p0

    .line 207
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null tipMoney"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public totalMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 201
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object p0

    .line 199
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null totalMoney"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public type(Lcom/squareup/sdk/pos/transaction/Tender$Type;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 222
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;->type:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    return-object p0

    .line 220
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null type"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
