.class public final enum Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;
.super Ljava/lang/Enum;
.source "TenderCardDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/TenderCardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EntryMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

.field public static final enum CONTACTLESS:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

.field public static final enum EMV:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

.field public static final enum KEYED:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

.field public static final enum ON_FILE:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

.field public static final enum SWIPED:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

.field public static final enum UNKNOWN:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 30
    new-instance v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->UNKNOWN:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    .line 33
    new-instance v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    const/4 v2, 0x1

    const-string v3, "KEYED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->KEYED:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    .line 36
    new-instance v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    const/4 v3, 0x2

    const-string v4, "SWIPED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->SWIPED:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    .line 39
    new-instance v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    const/4 v4, 0x3

    const-string v5, "EMV"

    invoke-direct {v0, v5, v4}, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->EMV:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    .line 42
    new-instance v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    const/4 v5, 0x4

    const-string v6, "ON_FILE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->ON_FILE:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    .line 45
    new-instance v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    const/4 v6, 0x5

    const-string v7, "CONTACTLESS"

    invoke-direct {v0, v7, v6}, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->CONTACTLESS:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    .line 28
    sget-object v7, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->UNKNOWN:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->KEYED:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->SWIPED:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->EMV:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->ON_FILE:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->CONTACTLESS:Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->$VALUES:[Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->$VALUES:[Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    invoke-virtual {v0}, [Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    return-object v0
.end method
