.class public Lcom/squareup/settings/StringSetLocalSetting;
.super Lcom/squareup/settings/AbstractLocalSetting;
.source "StringSetLocalSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/settings/AbstractLocalSetting<",
        "Ljava/util/Set<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/settings/StringSetPreferenceAdapter;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/AbstractLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 21
    new-instance p1, Lcom/squareup/settings/StringSetPreferenceAdapter;

    invoke-direct {p1, p3}, Lcom/squareup/settings/StringSetPreferenceAdapter;-><init>(Lcom/google/gson/Gson;)V

    iput-object p1, p0, Lcom/squareup/settings/StringSetLocalSetting;->adapter:Lcom/squareup/settings/StringSetPreferenceAdapter;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doGet()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/settings/StringSetLocalSetting;->doGet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected doGet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/settings/StringSetLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/StringSetLocalSetting;->key:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 30
    :cond_0
    iget-object v1, p0, Lcom/squareup/settings/StringSetLocalSetting;->adapter:Lcom/squareup/settings/StringSetPreferenceAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/settings/StringSetPreferenceAdapter;->deserialize(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/squareup/settings/StringSetLocalSetting;->set(Ljava/util/Set;)V

    return-void
.end method

.method public set(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/settings/StringSetLocalSetting;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/settings/StringSetLocalSetting;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/settings/StringSetLocalSetting;->adapter:Lcom/squareup/settings/StringSetPreferenceAdapter;

    invoke-virtual {v2, p1}, Lcom/squareup/settings/StringSetPreferenceAdapter;->serialize(Ljava/util/Set;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 36
    invoke-virtual {p0, v0}, Lcom/squareup/settings/StringSetLocalSetting;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method
