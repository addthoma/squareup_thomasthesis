.class public Lcom/squareup/settings/server/EmployeeManagementSettingChanged;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "EmployeeManagementSettingChanged.java"


# instance fields
.field public final enabled:Z


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/EventNamedAction;Z)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 14
    iput-boolean p2, p0, Lcom/squareup/settings/server/EmployeeManagementSettingChanged;->enabled:Z

    return-void
.end method

.method public static guestModeEnabled(Z)Lcom/squareup/settings/server/EmployeeManagementSettingChanged;
    .locals 2

    .line 18
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettingChanged;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_EMPLOYEE_MANAGEMENT_GUEST_MODE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/settings/server/EmployeeManagementSettingChanged;-><init>(Lcom/squareup/analytics/EventNamedAction;Z)V

    return-object v0
.end method
