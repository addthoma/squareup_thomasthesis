.class public final Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;
.super Ljava/lang/Object;
.source "EmployeeManagementSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/server/EmployeeManagementSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/GuestModeDefault;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/GuestModeDefault;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 52
    iput-object p8, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p9, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p10, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg9Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/passcode/GuestModeDefault;",
            ">;)",
            "Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;"
        }
    .end annotation

    .line 69
    new-instance v11, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;Lcom/squareup/settings/server/passcode/GuestModeDefault;)Lcom/squareup/settings/server/EmployeeManagementSettings;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;",
            "Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;",
            "Lcom/squareup/settings/server/passcode/GuestModeDefault;",
            ")",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;"
        }
    .end annotation

    .line 76
    new-instance v11, Lcom/squareup/settings/server/EmployeeManagementSettings;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/settings/server/EmployeeManagementSettings;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;Lcom/squareup/settings/server/passcode/GuestModeDefault;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/settings/server/EmployeeManagementSettings;
    .locals 11

    .line 59
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/accountstatus/AccountStatusProvider;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/settings/server/passcode/GuestModeDefault;

    invoke-static/range {v1 .. v10}, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;Lcom/squareup/settings/server/passcode/GuestModeDefault;)Lcom/squareup/settings/server/EmployeeManagementSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings_Factory;->get()Lcom/squareup/settings/server/EmployeeManagementSettings;

    move-result-object v0

    return-object v0
.end method
