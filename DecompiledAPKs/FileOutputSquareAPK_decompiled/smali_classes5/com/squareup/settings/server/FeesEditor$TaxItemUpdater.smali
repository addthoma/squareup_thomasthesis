.class public interface abstract Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;
.super Ljava/lang/Object;
.source "FeesEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/FeesEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TaxItemUpdater"
.end annotation


# virtual methods
.method public abstract findRelationsToUpdate(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;Ljava/util/List;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)",
            "Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;"
        }
    .end annotation
.end method
