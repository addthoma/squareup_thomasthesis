.class public final Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault_PasscodeEmployeeManagementEnabled_Factory;
.super Ljava/lang/Object;
.source "PasscodeEmployeeManagementDefault_PasscodeEmployeeManagementEnabled_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault_PasscodeEmployeeManagementEnabled_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementEnabled;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault_PasscodeEmployeeManagementEnabled_Factory;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault_PasscodeEmployeeManagementEnabled_Factory$InstanceHolder;->access$000()Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault_PasscodeEmployeeManagementEnabled_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementEnabled;
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementEnabled;

    invoke-direct {v0}, Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementEnabled;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementEnabled;
    .locals 1

    .line 13
    invoke-static {}, Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault_PasscodeEmployeeManagementEnabled_Factory;->newInstance()Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementEnabled;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault_PasscodeEmployeeManagementEnabled_Factory;->get()Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementEnabled;

    move-result-object v0

    return-object v0
.end method
