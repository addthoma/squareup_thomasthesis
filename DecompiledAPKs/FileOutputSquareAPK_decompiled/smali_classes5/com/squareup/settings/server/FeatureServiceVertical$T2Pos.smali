.class public abstract Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos;
.super Lcom/squareup/settings/server/FeatureServiceVertical;
.source "FeatureServiceVertical.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/FeatureServiceVertical;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "T2Pos"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos$T2SPos;,
        Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos$T2TerminalApi;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0005\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos;",
        "Lcom/squareup/settings/server/FeatureServiceVertical;",
        "()V",
        "T2SPos",
        "T2TerminalApi",
        "Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos$T2SPos;",
        "Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos$T2TerminalApi;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, v0}, Lcom/squareup/settings/server/FeatureServiceVertical;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos;-><init>()V

    return-void
.end method
