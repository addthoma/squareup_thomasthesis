.class public final Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;
.super Ljava/lang/Object;
.source "InstallationIdModule_ProvideInstallationIdFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final generatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/InstallationIdGenerator;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/InstallationIdGenerator;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;->generatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/InstallationIdGenerator;",
            ">;)",
            "Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideInstallationId(Landroid/content/SharedPreferences;Lcom/squareup/settings/InstallationIdGenerator;)Ljava/lang/String;
    .locals 0

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/settings/InstallationIdModule;->provideInstallationId(Landroid/content/SharedPreferences;Lcom/squareup/settings/InstallationIdGenerator;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;->generatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/InstallationIdGenerator;

    invoke-static {v0, v1}, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;->provideInstallationId(Landroid/content/SharedPreferences;Lcom/squareup/settings/InstallationIdGenerator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
