.class public final Lcom/squareup/settings/cashmanagement/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/cashmanagement/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final cash_management:I = 0x7f120379

.field public static final cash_management_auto_email_enable_hint:I = 0x7f12037a

.field public static final cash_management_auto_email_enable_label:I = 0x7f12037b

.field public static final cash_management_auto_print_enable_hint:I = 0x7f12037c

.field public static final cash_management_auto_print_enable_label:I = 0x7f12037d

.field public static final cash_management_default_starting_cash_label_uppercase:I = 0x7f12037e

.field public static final cash_management_disabled_message_subtitle:I = 0x7f12037f

.field public static final cash_management_disabled_message_title:I = 0x7f120380

.field public static final cash_management_email_recipient_label_uppercase:I = 0x7f120382

.field public static final cash_management_enable_hint:I = 0x7f120383

.field public static final cash_management_enable_label:I = 0x7f120384

.field public static final cash_management_settings_save:I = 0x7f120387


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
