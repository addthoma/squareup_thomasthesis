.class public final Lcom/squareup/tenderworkflow/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderworkflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final PaymentPromptLayout:I = 0x7f13014f

.field public static final PaymentPromptLayout_Card:I = 0x7f130150

.field public static final PaymentPromptLayout_CardSlot:I = 0x7f130151

.field public static final PaymentPromptLayout_Content:I = 0x7f130152

.field public static final PaymentPromptLayout_Content_Icon:I = 0x7f130153

.field public static final PaymentPromptLayout_Content_PaymentType:I = 0x7f130154

.field public static final PaymentPromptLayout_Content_Total:I = 0x7f130155

.field public static final PaymentPromptLayout_Content_TransactionType:I = 0x7f130156

.field public static final PaymentPromptLayout_Header:I = 0x7f130157

.field public static final PaymentPromptLayout_Header_BuyerLanguageButton:I = 0x7f130158

.field public static final PaymentPromptLayout_Header_UpButton:I = 0x7f130159


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
