.class public final Lcom/squareup/transactionhistory/TimeInterval;
.super Ljava/lang/Object;
.source "Configuration.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transactionhistory/TimeInterval$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u001b\u0008\u0002\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005J\u000b\u0010\t\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J!\u0010\u000b\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/TimeInterval;",
        "",
        "startTimeInclusive",
        "Ljava/util/Date;",
        "endTimeExclusive",
        "(Ljava/util/Date;Ljava/util/Date;)V",
        "getEndTimeExclusive",
        "()Ljava/util/Date;",
        "getStartTimeInclusive",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/transactionhistory/TimeInterval$Companion;


# instance fields
.field private final endTimeExclusive:Ljava/util/Date;

.field private final startTimeInclusive:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/transactionhistory/TimeInterval$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/transactionhistory/TimeInterval$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/transactionhistory/TimeInterval;->Companion:Lcom/squareup/transactionhistory/TimeInterval$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/util/Date;Ljava/util/Date;)V
    .locals 7

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    iput-object p2, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    .line 85
    iget-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    const/4 p2, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    if-eqz p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 86
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Either both of startTimeInclusive="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "endTimeExclusive="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    iget-object v2, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " must be null or both must be non-null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    if-eqz p1, :cond_5

    .line 92
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    iget-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    cmp-long p1, v3, v5

    if-lez p1, :cond_3

    goto :goto_1

    :cond_3
    cmp-long p1, v1, v5

    if-lez p1, :cond_4

    goto :goto_2

    :cond_4
    :goto_1
    const/4 p2, 0x0

    .line 93
    :goto_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "startTimeInclusive="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " must be within "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "[0, endTimeExclusive="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ")."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 91
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    :cond_5
    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Date;Ljava/util/Date;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/TimeInterval;-><init>(Ljava/util/Date;Ljava/util/Date;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/transactionhistory/TimeInterval;Ljava/util/Date;Ljava/util/Date;ILjava/lang/Object;)Lcom/squareup/transactionhistory/TimeInterval;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/transactionhistory/TimeInterval;->copy(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/transactionhistory/TimeInterval;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    return-object v0
.end method

.method public final component2()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    return-object v0
.end method

.method public final copy(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/transactionhistory/TimeInterval;
    .locals 1

    new-instance v0, Lcom/squareup/transactionhistory/TimeInterval;

    invoke-direct {v0, p1, p2}, Lcom/squareup/transactionhistory/TimeInterval;-><init>(Ljava/util/Date;Ljava/util/Date;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/transactionhistory/TimeInterval;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transactionhistory/TimeInterval;

    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    iget-object v1, p1, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    iget-object p1, p1, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEndTimeExclusive()Ljava/util/Date;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    return-object v0
.end method

.method public final getStartTimeInclusive()Ljava/util/Date;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TimeInterval(startTimeInclusive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/TimeInterval;->startTimeInclusive:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", endTimeExclusive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/TimeInterval;->endTimeExclusive:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
