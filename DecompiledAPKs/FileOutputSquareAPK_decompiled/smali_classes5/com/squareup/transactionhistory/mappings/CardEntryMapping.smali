.class public final enum Lcom/squareup/transactionhistory/mappings/CardEntryMapping;
.super Ljava/lang/Enum;
.source "CardEntryMapping.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/transactionhistory/mappings/CardEntryMapping;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/mappings/CardEntryMapping;",
        "",
        "protoEntryMethod",
        "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        "transactionEntryMethod",
        "Lcom/squareup/transactionhistory/CardEntryMethod;",
        "(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/transactionhistory/CardEntryMethod;)V",
        "getProtoEntryMethod$public_release",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        "getTransactionEntryMethod$public_release",
        "()Lcom/squareup/transactionhistory/CardEntryMethod;",
        "UNKNOWN",
        "SWIPED",
        "KEYED",
        "EMV",
        "CONTACTLESS",
        "ON_FILE",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

.field public static final enum CONTACTLESS:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

.field public static final enum EMV:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

.field public static final enum KEYED:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

.field public static final enum ON_FILE:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

.field public static final enum SWIPED:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

.field public static final enum UNKNOWN:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;


# instance fields
.field private final protoEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field private final transactionEntryMethod:Lcom/squareup/transactionhistory/CardEntryMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    .line 17
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 18
    sget-object v3, Lcom/squareup/transactionhistory/CardEntryMethod;->UNKNOWN:Lcom/squareup/transactionhistory/CardEntryMethod;

    const/4 v4, 0x0

    const-string v5, "UNKNOWN"

    .line 16
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/transactionhistory/CardEntryMethod;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->UNKNOWN:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    .line 21
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 22
    sget-object v3, Lcom/squareup/transactionhistory/CardEntryMethod;->SWIPED:Lcom/squareup/transactionhistory/CardEntryMethod;

    const/4 v4, 0x1

    const-string v5, "SWIPED"

    .line 20
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/transactionhistory/CardEntryMethod;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->SWIPED:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    .line 25
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 26
    sget-object v3, Lcom/squareup/transactionhistory/CardEntryMethod;->KEYED:Lcom/squareup/transactionhistory/CardEntryMethod;

    const/4 v4, 0x2

    const-string v5, "KEYED"

    .line 24
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/transactionhistory/CardEntryMethod;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->KEYED:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    .line 29
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 30
    sget-object v3, Lcom/squareup/transactionhistory/CardEntryMethod;->EMV:Lcom/squareup/transactionhistory/CardEntryMethod;

    const/4 v4, 0x3

    const-string v5, "EMV"

    .line 28
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/transactionhistory/CardEntryMethod;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->EMV:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    .line 33
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 34
    sget-object v3, Lcom/squareup/transactionhistory/CardEntryMethod;->CONTACTLESS:Lcom/squareup/transactionhistory/CardEntryMethod;

    const/4 v4, 0x4

    const-string v5, "CONTACTLESS"

    .line 32
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/transactionhistory/CardEntryMethod;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->CONTACTLESS:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    .line 37
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 38
    sget-object v3, Lcom/squareup/transactionhistory/CardEntryMethod;->ON_FILE:Lcom/squareup/transactionhistory/CardEntryMethod;

    const/4 v4, 0x5

    const-string v5, "ON_FILE"

    .line 36
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/transactionhistory/CardEntryMethod;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->ON_FILE:Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/transactionhistory/CardEntryMethod;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
            "Lcom/squareup/transactionhistory/CardEntryMethod;",
            ")V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->protoEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object p4, p0, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->transactionEntryMethod:Lcom/squareup/transactionhistory/CardEntryMethod;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/transactionhistory/mappings/CardEntryMapping;
    .locals 1

    const-class v0, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    return-object p0
.end method

.method public static values()[Lcom/squareup/transactionhistory/mappings/CardEntryMapping;
    .locals 1

    sget-object v0, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    invoke-virtual {v0}, [Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/transactionhistory/mappings/CardEntryMapping;

    return-object v0
.end method


# virtual methods
.method public final getProtoEntryMethod$public_release()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->protoEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object v0
.end method

.method public final getTransactionEntryMethod$public_release()Lcom/squareup/transactionhistory/CardEntryMethod;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/CardEntryMapping;->transactionEntryMethod:Lcom/squareup/transactionhistory/CardEntryMethod;

    return-object v0
.end method
