.class public interface abstract Lcom/squareup/transactionhistory/processed/ProcessedTransactionsStore;
.super Ljava/lang/Object;
.source "ProcessedTransactionsStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H&J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00072\u0006\u0010\u000b\u001a\u00020\u000cH&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/processed/ProcessedTransactionsStore;",
        "",
        "configureWith",
        "",
        "configuration",
        "Lcom/squareup/transactionhistory/Configuration;",
        "fetchNextSummaries",
        "Lio/reactivex/Single;",
        "Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult;",
        "fetchTransaction",
        "Lcom/squareup/transactionhistory/processed/FetchTransactionResult;",
        "transactionServerId",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract configureWith(Lcom/squareup/transactionhistory/Configuration;)Z
.end method

.method public abstract fetchNextSummaries()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract fetchTransaction(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transactionhistory/processed/FetchTransactionResult;",
            ">;"
        }
    .end annotation
.end method
