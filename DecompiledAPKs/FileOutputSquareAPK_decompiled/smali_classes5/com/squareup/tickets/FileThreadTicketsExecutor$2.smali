.class Lcom/squareup/tickets/FileThreadTicketsExecutor$2;
.super Ljava/lang/Object;
.source "FileThreadTicketsExecutor.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tickets/FileThreadTicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/FileThreadTicketsExecutor;

.field final synthetic val$task:Lcom/squareup/tickets/TicketsTask;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/FileThreadTicketsExecutor;Lcom/squareup/tickets/TicketsTask;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$2;->this$0:Lcom/squareup/tickets/FileThreadTicketsExecutor;

    iput-object p2, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$2;->val$task:Lcom/squareup/tickets/TicketsTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$2;->val$task:Lcom/squareup/tickets/TicketsTask;

    iget-object v1, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$2;->this$0:Lcom/squareup/tickets/FileThreadTicketsExecutor;

    invoke-static {v1}, Lcom/squareup/tickets/FileThreadTicketsExecutor;->access$000(Lcom/squareup/tickets/FileThreadTicketsExecutor;)Lcom/squareup/tickets/TicketDatabase;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsTask;->perform(Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
