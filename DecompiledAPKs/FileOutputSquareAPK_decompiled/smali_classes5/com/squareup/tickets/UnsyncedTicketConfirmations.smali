.class public Lcom/squareup/tickets/UnsyncedTicketConfirmations;
.super Ljava/lang/Object;
.source "UnsyncedTicketConfirmations.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildMessage(ILcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-le p0, v0, :cond_0

    .line 18
    sget v0, Lcom/squareup/opentickets/R$string;->open_tickets_syncing_message_plural:I

    .line 19
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string/jumbo v0, "ticket_count"

    .line 20
    invoke-virtual {p1, v0, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 22
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget p0, Lcom/squareup/opentickets/R$string;->open_tickets_syncing_message:I

    .line 23
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static disableConfirmation(ILcom/squareup/util/Res;)Lcom/squareup/register/widgets/ConfirmationStrings;
    .locals 4

    .line 11
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationStrings;

    sget v1, Lcom/squareup/opentickets/R$string;->open_tickets_syncing:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 12
    invoke-static {p0, p1}, Lcom/squareup/tickets/UnsyncedTicketConfirmations;->buildMessage(ILcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    sget v2, Lcom/squareup/widgets/R$string;->disable:I

    .line 13
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/squareup/widgets/R$string;->cancel:I

    .line 14
    invoke-interface {p1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p0, v2, p1}, Lcom/squareup/register/widgets/ConfirmationStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
