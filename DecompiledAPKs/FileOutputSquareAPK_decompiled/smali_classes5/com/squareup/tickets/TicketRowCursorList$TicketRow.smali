.class public interface abstract Lcom/squareup/tickets/TicketRowCursorList$TicketRow;
.super Ljava/lang/Object;
.source "TicketRowCursorList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/TicketRowCursorList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TicketRow"
.end annotation


# virtual methods
.method public abstract getEmployeeName()Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEmployeeToken()Ljava/lang/String;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getOpenTicket()Lcom/squareup/tickets/OpenTicket;
.end method

.method public abstract getPriceAmount()J
.end method

.method public abstract getTimeUpdated()Ljava/util/Date;
.end method
