.class public interface abstract Lcom/squareup/tickets/TicketsExecutor;
.super Ljava/lang/Object;
.source "TicketsExecutor.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Lcom/squareup/tickets/TicketDatabase;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Executor;"
    }
.end annotation


# virtual methods
.method public abstract execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/tickets/TicketsTask<",
            "TT;TD;>;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract execute(Ljava/lang/Runnable;)V
.end method
