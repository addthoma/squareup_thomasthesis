.class public interface abstract Lcom/squareup/tickets/Tickets;
.super Ljava/lang/Object;
.source "Tickets.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/Tickets$MergeResults;,
        Lcom/squareup/tickets/Tickets$CompResults;,
        Lcom/squareup/tickets/Tickets$CompHandler;,
        Lcom/squareup/tickets/Tickets$VoidResults;,
        Lcom/squareup/tickets/Tickets$VoidHandler;,
        Lcom/squareup/tickets/Tickets$InternalTickets;
    }
.end annotation


# virtual methods
.method public abstract addEmptyTicketAndLock(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;
.end method

.method public abstract addTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)V
.end method

.method public abstract addTicketAndLock(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;
.end method

.method public abstract additiveMergeToTicket(Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract closeTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V
.end method

.method public abstract compTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$CompHandler;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/Tickets$CompHandler;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$CompResults;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract debugCloseAllTickets()V
.end method

.method public abstract deleteTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V
.end method

.method public abstract deleteTickets(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getBundler()Lmortar/bundler/Bundler;
.end method

.method public abstract getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getRemoteUnsyncedFixableTicketCount()J
.end method

.method public abstract getRemoteUnsyncedUnfixableTicketCount()J
.end method

.method public abstract getTicketAndLock(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/OpenTicket;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getTicketCounts(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketCounter;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getUnsyncedTicketCount(Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract mergeTicketsToExisting(Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract mergeTicketsToNew(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onLocalTicketsUpdated()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/LocalTicketUpdateEvent;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract transferOwnership(Ljava/util/List;Lcom/squareup/permissions/EmployeeInfo;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/permissions/EmployeeInfo;",
            ")V"
        }
    .end annotation
.end method

.method public abstract unlock(Ljava/lang/String;)V
.end method

.method public abstract updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V
.end method

.method public abstract updateTicketAndMaintainLock(Lcom/squareup/tickets/OpenTicket;)V
.end method

.method public abstract updateTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V
.end method

.method public abstract voidTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$VoidHandler;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/Tickets$VoidHandler;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$VoidResults;",
            ">;)V"
        }
    .end annotation
.end method
