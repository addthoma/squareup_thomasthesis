.class abstract Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;
.super Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;
.source "AbstractTicketsLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "CancelableTicketCursorCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback<",
        "Lcom/squareup/tickets/TicketRowCursorList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;


# direct methods
.method private constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)V
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V
    .locals 0

    .line 243
    invoke-direct {p0, p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/tickets/TicketsResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    .line 247
    iget-boolean v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;->canceled:Z

    if-nez v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tickets/TicketRowCursorList;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->setTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;Z)V

    .line 249
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;->onCompleted(Lcom/squareup/tickets/TicketsResult;)V

    goto :goto_0

    .line 252
    :cond_0
    :try_start_0
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;->onAborted(Lcom/squareup/tickets/TicketsResult;)V

    :goto_0
    return-void

    :catch_0
    move-exception p1

    .line 254
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
