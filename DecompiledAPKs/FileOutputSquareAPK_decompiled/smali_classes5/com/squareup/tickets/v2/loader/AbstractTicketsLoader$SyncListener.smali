.class abstract Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;
.super Ljava/lang/Object;
.source "AbstractTicketsLoader.java"

# interfaces
.implements Lcom/squareup/ui/ticket/TicketsListSchedulerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "SyncListener"
.end annotation


# instance fields
.field private final employeeAccessMode:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

.field private final employeeToken:Ljava/lang/String;

.field private final excludedTicketIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sortType:Lcom/squareup/tickets/TicketSort;

.field final synthetic this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketSort;",
            ")V"
        }
    .end annotation

    .line 354
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355
    iput-object p2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->excludedTicketIds:Ljava/util/List;

    .line 356
    iput-object p3, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->employeeToken:Ljava/lang/String;

    .line 357
    iput-object p4, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->employeeAccessMode:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    .line 358
    iput-object p5, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->sortType:Lcom/squareup/tickets/TicketSort;

    return-void
.end method


# virtual methods
.method protected abstract fetchTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;",
            ")V"
        }
    .end annotation
.end method

.method public final onUpdate(Z)V
    .locals 7

    .line 363
    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$500(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$600(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    move-object v3, p1

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v3, p1, v0

    const/4 v0, 0x1

    .line 364
    iget-object v1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->sortType:Lcom/squareup/tickets/TicketSort;

    aput-object v1, p1, v0

    const-string v0, "[Tickets_Loader] Hitting local store from sync callback with filter of \"%s\"+%s."

    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    iget-object v1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->excludedTicketIds:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->sortType:Lcom/squareup/tickets/TicketSort;

    iget-object v4, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->employeeToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->employeeAccessMode:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    .line 367
    invoke-static {p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$800(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;

    move-result-object v6

    move-object v0, p0

    .line 366
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;->fetchTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;)V

    return-void
.end method
